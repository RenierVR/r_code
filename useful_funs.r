
old_wd <- getwd()
setwd("~/R_code/")


gg_color_hue <- function(n) {
  hues = seq(15, 375, length = n + 1)
  hcl(h = hues, l = 65, c = 100)[1:n]
}



############################################################################
####    SOURCES OF USEFUL FUNTIONS    ######################################
############################################################################

source("tiaan_specgram.r")
source("zPlot.r")
source("IIRfilters.r")
source("FIRfilters.r")





############################################################################
####    WHICH OF ONE SEQUENCE IN ANOTHER              ######################
############################################################################

which_in <- function(x, y, type = c("which", "bool")[1])
{
  ## Return bool vector of length of sequence 'x', or 
  ##  indices of sequence 'x' that is in the range given
  ##  by sequence 'y'.
  ## Eg which_in(c(11,12,13,14,15), c(13,14), "which") 
  ##  should return c(3, 4)
  
  xx <- (x >= y[1]) & (x <= tail(y, 1))
  
  if(type == "which")
    return(which(xx))
  if(type == "bool")
    return(xx)
  
}



############################################################################
####    MOVING AVERAGE (WRAPPER FOR stats::filter)    ######################
############################################################################

movingAve <- function(x, m, endpoint_behaviour = 1, allow_na_prop = 0.8)
{
  ## Moving Average filter with order 'm'
  ## 'endpoint_behaviour' sets the behaviour for endpoints used for
  ##   first and last results, can have values:
  ##   1 - return original values
  ##   2 - return NAs
  ##   3 - remove and return sequence with reduced length
  ##  (note that if 'x' contains NAs, this will be influenced as well).
  ## If missing values in 'x', a max proportion of 'allow_na_prop'
  ##   of each window's data is used in the calculation of the mean.
  
  if(length(x) < m)
    stop("Time series length shorter than filter order.")
  
  # If no NAs, use builtin function for efficiency
  if(!any(is.na(x)))
    x_fil <- as.numeric(stats::filter(x, rep(1/m, m)))
  # Otherwise, assess NA proportion in each window
  else { 
    x_fil <- rep(NA, len(x))
    mn <- c(floor((m-1)/2), ceiling((m-1)/2))
    for(i in (mn[1]+1):(len(x)-mn[2])) {
      xwin <- x[(i-mn[1]):(i+mn[2])]
      if((1 - sum(is.na(xwin))/len(xwin)) >= allow_na_prop)
        x_fil[i] <- mean(xwin, na.rm=T)
    }
  }
  
  
  if(endpoint_behaviour == 1){
    x_fil[is.na(x_fil)] <- x[is.na(x_fil)]
    return(x_fil)
  }
  
  else if(endpoint_behaviour == 2)
    return(x_fil)
  
  else if(endpoint_behaviour == 3)
    return(as.numeric(na.omit(x_fil)))
  
}




############################################################################
####    EXPONENTIAL SMOOTHER     ###########################################
############################################################################

expSmooth <- function(x, alpha=0, method=c("iir", "fir")[1])
{ 
  x_alpha_c <- (1-alpha)*x
  
  if(method == "fir") {
    x_alpha <- alpha*x
    y <- c(x[1], apply(cb(x_alpha[-len(x_alpha)], x_alpha_c[-1]), 1, sum))
  }
  else if(method == "iir") {
    y <- c(x[1], rep(0, len(x)-1))
    for(i in 2:len(x))
      y[i] <- alpha*y[i-1] + x_alpha_c[i]
  }
  
  return(y)
}



############################################################################
####    RUNNING STANDARD DEVIATION    ######################################
############################################################################

runningSD <- function(x, bandwidth=length(x))
{
  n <- len(x)
  
  if(bandwidth > n){
    warning("Bandwidth parameter larger than vector length.")
    return(0)
  }
  
  rsd <- NULL
  for(i in 1:(len(x) - bandwidth + 1))  
    rsd <- c(rsd, sd(x[i:(i + bandwidth - 1)]))
  
  return(rsd)
}




############################################################################
####    JUMP DISCONTINUITY REMOVAL FUNCTION    #############################
############################################################################

removeAGCJumps <- function(xx, threshold=500)
{
  xx_adjust <- numeric(length(xx))
  running_DC <- 0
  
  for(i in 2:length(xx)){
    previousSample <- xx[i-1]
    grn1 <- xx[i]
    if(abs(previousSample - grn1) > threshold)
      running_DC <- running_DC + (grn1 - previousSample);
    xx_adjust[i] <- grn1 - running_DC;
  }
  
  return(xx_adjust)
}



############################################################################
####    DATA SEARCH IN DIRECTORY    ########################################
############################################################################


getDataFromDir <- function(fpath, ext="csv", dataExtract=TRUE, ret=c("full", "base")[1], ...)
{
  files <- list.files(fpath)
  # fext <- sapply(files, function(x) substring(x, nchar(x)-2, nchar(x)))
  fext <- unlist(lapply(strsplit(files, "[.]"), tail, 1))
  
  files <- files[fext == ext]
  
  if(!dataExtract){
    if(ret == "base")
      return(files)
    else
      return(paste0(fpath, files))
  }
  
  n <- len(files)
  flist <- vector("list", n)
  names(flist) <- sapply(files, function(x) substring(x, 1, nchar(x)-4))
  
  for(i in 1:n)
    flist[[i]] <- read.table(paste0(fpath, files[i]), sep=",", ...)
  
  return(flist)
}




############################################################################
####    PRINT HDF5 FILE TABLE NAMES    #####################################
############################################################################


h5contents <- function(fpath_h5)
{ ## Print the contents (table names) of a HDF5 file.
  
  require(rhdf5)
  h5_contents <- h5ls(fpath_h5)
  return( h5_contents[h5_contents$group != "/", c(1, 5)] )
  
}


############################################################################
####    HEAD FUNCTION FOR LISTS    #########################################
############################################################################


list_head <- function(x, n=10)
{
  if(!is.list(x)) stop("Input 'x' needs to be a list.")
  
  if(is.null(names(x)))
    nms <- paste0("[[", 1:len(x), "]]")
  else
    nms <- paste0("$", names(x))
  
  for(i in 1:len(x)) {
    cat(nms[i], "\n")
    print(head(x[[i]], n))
    cat("\n")
  }
}



############################################################################
####    SELECT ITEM FROM LIST OF DFS    ####################################
############################################################################


lsel <- function(x, i)
{ ## Given list 'x' with each element a DF with similar
  ##   col dimension, select a specified col from each DF.
  ## Indicator 'i' can be either the col index or the col name.
  
  if(!is.list(x)) stop("Input 'x' needs to be a list.")
  
  if(!is.num(i)) {
    wi <- which(i == names(x[[1]]))
    
    # Use partial pattern matching if necessary
    if(len(wi) < 1) {
      gi <- grep(i, names(x[[1]]))
      if(len(gi) > 1) 
        warning(paste0("More than one name matches specified pattern, using '", names(x[[1]])[gi[1]], "'."))
      i <- gi[1]
    }
    else
      i <- wi
  }
  
  lapply(x, function(y) y[[i]])
}




############################################################################
####    CURRENT CONVERSION FUNCTIONS   #####################################
############################################################################



convertToCurrent3 <-  function(xx, Rf, Isub, c1=0.016061056, c2=0.809626)
{
  temp32bitVal <- xx - 32768
  
  tempVal <- c1 * temp32bitVal
  tempVal <- tempVal / (Rf+1)
  tempVal <- tempVal - c2 * (Isub/10.0)
  return (1000000.0 * tempVal)
}





removeCurrentDiscont <- function(xx, rf, isub, threshold=50000)
{
  n <- length(xx)
  
  xx_adjust <- numeric(n)
  running_DC <- 0
  
  if(missing(rf))
    rf <- numeric(n)
  
  if(missing(isub))
    isub <- numeric(n)
  
  for(i in 2:n){
    previousSample <- xx[i-1]
    grn1 <- xx[i]
    
    if((rf[i]-rf[i-1] != 0) | (isub[i]-isub[i-1] != 0))
      agc_changed <- TRUE
    else
      agc_changed <- FALSE
    
    if((abs(previousSample - grn1) > threshold) | agc_changed)
      running_DC <- running_DC + (grn1 - previousSample)
    xx_adjust[i] <- grn1 - running_DC
  }
  
  return(xx_adjust)
}



removeCurrentDiscont2 <- function(xx, rf, isub, led, threshold=1e6)
{
  n <- length(xx)
  
  xx_d <- diff(xx)
  
  if(missing(rf))
    rf <- numeric(n)
  
  if(missing(isub))
    isub <- numeric(n)
  
  if(missing(led))
    led <- numeric(n)
  
  xx_agc <- cbind(rf, isub, led)
  xx_agc <- apply(xx_agc, 2, diff)
  xx_agc <- apply(xx_agc, 1, function(x) any(x != 0))
  xx_agc <- xx_agc | (abs(xx_d) > threshold)
  
  run_DC <- xx_d
  run_DC[!xx_agc] <- 0
  run_DC <- c(0, cumsum(run_DC))
  
  return(xx - run_DC)
  
  
  #   xx_adjust <- numeric(n)
  #   running_DC <- 0
  #   for(i in 2:n){
  #     previousSample <- xx[i-1]
  #     grn1 <- xx[i]
  #     
  #     if((rf[i]-rf[i-1] != 0) | (isub[i]-isub[i-1] != 0) | (led[i]-led[i-1] != 0))
  #       agc_changed <- TRUE
  #     else
  #       agc_changed <- FALSE
  #     
  #     if((abs(previousSample - grn1) > threshold) | agc_changed)
  #       running_DC <- running_DC + (grn1 - previousSample)
  #     
  #     xx_adjust[i] <- grn1 - running_DC
  #   }
  #   
  #   return(xx_adjust)
}



removeCurrentDisconts <- function(xx, rf1, rf2, isub1, isub2, led, threshold=1e6)
{
  n <- length(xx)
  
  xx_d <- diff(xx)
  
  if(missing(rf1) || missing(rf2)) {
    rf1 <- numeric(n)
    rf2 <- numeric(n)
  }
  
  if(missing(isub1) || missing(isub2)) {
    isub1 <- numeric(n)
    isub2 <- numeric(n)
  }
  
  if(missing(led))
    led <- numeric(n)
  
  xx_agc <- cbind(rf1, rf2, isub1, isub2, led)
  xx_agc <- apply(xx_agc, 2, diff)
  xx_agc <- apply(xx_agc, 1, function(x) any(x != 0))
  xx_agc <- xx_agc | (abs(xx_d) > threshold)
  
  run_DC <- xx_d
  run_DC[!xx_agc] <- 0
  run_DC <- c(0, cumsum(run_DC))
  
  return(xx - run_DC)
  
  
  #   xx_adjust <- numeric(n)
  #   running_DC <- 0
  #   for(i in 2:n){
  #     previousSample <- xx[i-1]
  #     grn1 <- xx[i]
  #     
  #     if((rf[i]-rf[i-1] != 0) | (isub[i]-isub[i-1] != 0) | (led[i]-led[i-1] != 0))
  #       agc_changed <- TRUE
  #     else
  #       agc_changed <- FALSE
  #     
  #     if((abs(previousSample - grn1) > threshold) | agc_changed)
  #       running_DC <- running_DC + (grn1 - previousSample)
  #     
  #     xx_adjust[i] <- grn1 - running_DC
  #   }
  #   
  #   return(xx_adjust)
}




removeCurrentDiscont_m <- function(x, rf, isub, threshold=50000)
{
  fac <- 1
  n <- length(x)
  y <- numeric(n)
  
  fvec <- numeric(n)
  
  if(missing(rf))
    rf <- numeric(n)
  
  if(missing(isub))
    isub <- numeric(n)
  
  y[1] <- x[1]
  for(i in 2:n){
    
    if((rf[i]-rf[i-1] != 0) | (isub[i]-isub[i-1] != 0))
      agc_changed <- TRUE
    else
      agc_changed <- FALSE
    
    if((abs(x[i] - x[i-1]) > threshold) | agc_changed)
      fac <- (x[i-1]*fac)/x[i]
    
    y[i] <- x[i]*fac
    fvec[i] <- fac
  }
  
  return(list(y=y, fac=fvec))
}


removeCurrentDiscont2_m <- function(x, rf, isub, led, threshold=1e6)
{
  n <- length(x)
  
  if(missing(rf))
    rf <- numeric(n)
  
  if(missing(isub))
    isub <- numeric(n)
  
  if(missing(led))
    led <- numeric(n)
  
  xx_agc <- cbind(rf, isub, led)
  xx_agc <- apply(xx_agc, 2, diff)
  xx_agc <- apply(xx_agc, 1, function(y) any(y != 0))
  xx_agc <- xx_agc | (abs(diff(x)) > threshold)
  
  
  fac <- 1
  fvec <- rep(1,n)
  for(i in 2:n){
    if( xx_agc[i-1] )
      fac <- (x[i-1]*fac)/x[i]
    
    fvec[i] <- fac
  }
  
  return(list(y=x*fvec, fac=fvec))
}



############################################################################
####    READ XLSX FILE WITH MULTIPLE SHEETS    #############################
############################################################################


read_all_excel_sheets <- function(filename) {
  require(readxl)
  sheets <- readxl::excel_sheets(filename)
  x <-    lapply(sheets, function(X) readxl::read_excel(filename, sheet = X))
  names(x) <- sheets
  x
}




############################################################################
####    GENERATE OBSERVATIONS USING SMOTHE    ##############################
############################################################################

balance_data_with_SMOTE <- function(dat, i_target = 1, n_bal)
{ ## Balance a data set by artificially creating new observations
  ##   for under-represented classes using SMOTE.
  ## 'i_target' is the column index of the factor variable.
  ## 'n_bal' is the amount of observations to aim for in each class
  ##   (this is taken to be n of observations in majority class by default).
  
  require(DMwR)
  
  # Assert that input is a data.frame
  if(!is.data.frame(dat)) stop("Input needs to be a data frame.")
  
  # Change name of target for ease of use
  name_target <- names(dat)[i_target]
  names(dat)[i_target] <- "target"
  
  # Correctly format target variable
  target <- as.factor(dat$target)
  tab <- table(target)
  m <- len(tab)
  
  m_major <- which.max(tab) 
  
  # Set up new n for each class
  if(missing(n_bal)) n_bal <- as.numeric(rep(tab[m_major], m))
  else if(len(n_bal) == 1) n_bal <- rep(n_bal, m)
  
  if(any(n_bal > tab[m_major])) stop("Majority class cannot be oversampled.")
  
  
  # Select desired amount of observations from majority class
  df_out <- dat[which(dat$target == levels(target)[m_major])[1:n_bal[m_major]],]
  
  
  # Loop through each under-rep class and generate new observations
  for(i in (1:m)[-m_major]) {
    
    dat_tmp <- dat[(dat[, i_target] == levels(target)[m_major]) | (dat[, i_target] == levels(target)[i]), ]
    dat_tmp$target <- as.factor(as.character(dat_tmp$target))
    perc_aim <- as.numeric(100*ceiling((n_bal[i]-tab[i])/tab[i]))
    dat_tmp <- SMOTE(target ~ ., data=dat_tmp, perc.over=perc_aim, perc.under=100)
    
    
    df_out <- rbind(df_out, dat_tmp[which(dat_tmp$target == levels(target)[i])[1:n_bal[i]], ])
    
  }
  
  names(df_out)[i_target] <- name_target
  
  return(df_out)
  
}






############################################################################
####    CALCULATE COHEN'S KAPPA STATISTIC FOR CLASSIFICATION MODELS    #####
############################################################################


calc_cohen_kappa <- function(conf_mat)
{
  ## calculate Cohen's Kappa
  
  # sum of all values in confusion matrix
  m <- sum(conf_mat)
  
  # observed proportionate agreement
  p_o <- sum(diag(conf_mat))/m
  
  # prob of overall random agreement
  p_rows <- rowSums(conf_mat)
  p_cols <- colSums(conf_mat)
  p_e <- sum(p_rows * p_cols)/(m^2)
  
  # calculate Cohen's Kappa
  kap <- (p_o - p_e)/(1 - p_e)
  
  return(kap)
}



############################################################################
####    PERFORMANCE MEASURES: PREDICTED VS TRUE    #########################
############################################################################


perf <- function(x_true, x_pred)
{ ## calculates performance measures between true values and predicted response
  ## measures: root mean squared error, mean absolute error,
  ##   mean percentage error, mean absolute percentage error
  ## if vectors of unequal length, shorter length is used
  
  if(length(x_true) != length(x_pred)){
    warning("lengths of input vectors not equal, smallest one used")
    min_len <- min(length(x_true), length(x_pred))
    x_true <- x_true[1:min_len]
    x_pred <- x_pred[1:min_len]
  }
  
  y <- x_true-x_pred
  p_vec <- c(rMSE=sqrt(mean(y^2)), MAE=mean(abs(y)), 
             MPE=100*mean(y/x_true), MAPE=100*mean(abs(y/x_true)) ) 
  
  return(p_vec)
}



#################################################################
####    SUMMARY OF DATAFRAME COLUMNS    #########################
#################################################################

poke <- function(dat, r=2)
{ ## Function to quickly glean summary of variables,
  ##   columns of data.frame 'dat'.
  ## Output consists of col name, mode and fivenum() summary
  ##   if numeric, otherwise the first 3 unique character values.
  ## Numerical values rounded to 'r'.
  ## Similar to function 'str' but adjusted to personal taste.
  
  out <- matrix("", ncol(dat), 4)
  
  for(i in 1:ncol(dat)) {
    
    ri <- dat[,i]
    ri_name <- paste0('"', names(dat)[i], '"')
    ri_class <- class(ri)
    ri_distinct <- len(unique(ri))
    
    if(ri_class == "numeric") {
      ri_class <- "num"
      ri_range <- paste0("[", paste(round(fivenum(ri), r), collapse=", "), "]")
    }
    else if(ri_class == "character") {
      ri_class <- "char"
      ri_range <- paste0("[", paste(head(unique(ri), 3), collapse=", "), "...]")
    }
    else if(ri_class == "factor")
    {
      m <- len(levels(ri))
      ri_table <- table(ri) 
      ri_table <- ri_table[order(ri_table, decreasing=T)]
      ri_table <- ri_table[1:min(5, m)]
      
      ri_range <- paste0('["', names(ri_table)[1], '" (', ri_table[1], ")")
      for(j in 2:len(ri_table))
        ri_range <- paste0(ri_range, ', "', names(ri_table)[j], '" (', ri_table[j], ")")
      
      if(m > 5)
        ri_range <- paste0(ri_range, ", ...]")
      else
        ri_range <- paste0(ri_range, "]")
    }
    
    cat(paste0(i, ")"), ri_name, "\n")
    cat("\t", ri_class, " distinct=", ri_distinct, "\n", sep="")
    cat("\t", ri_range, "\n", sep="")
    
    
    out[i,] <- c(ri_name, ri_class, ri_distinct, ri_range)
  }
  
  invisible(out)
}



############################################################
####    CHECK IF VALUE(S) WITHIN BOUNDS    #################
############################################################


isIn <- function(x, bounds, low_include = TRUE, up_include = TRUE, excl_all = FALSE)
{ ## Returns logical vector of length similar to 'x'
  ##   specifying whether value(s) within 'bounds'.
  ## If 'bounds' is only one value, say y, it is assumed to be (-y, y).
  ## 'bounds' consist of lower and upper bound values, and inclusion/exlcusion of
  ##   these bounds are set with 'low_include', 'up_include', or 'excl_all'.
  
  if(excl_all) {
    low_include <- FALSE
    up_include <- FALSE
  }
  
  if(len(bounds) == 1)
    bounds <- sort(bounds*c(-1, 1))
  
  is_in <- numeric(len(x))
  
  if(low_include) 
    is_in <- (x >= bounds[1])
  else 
    is_in <- (x > bounds[1])
  
  if(up_include) 
    is_in <- is_in & (x <= bounds[2])
  else 
    is_in <- is_in & (x < bounds[2])
  
  return (is_in)
}



############################################################
####    PRINT PROGRESS IN LOOPS    #########################
############################################################


catProgress <- function(i, n, r = 0, t_start = 0, SCALE = 1.3)
{ ## prints the progress after iteration i of n
  ## rounds to nearest r digits
  ## if estimated time remaining desired, create variable outside loop
  ##   with value 0, use it as the fourth argument and set it
  ##   equal to the catProgress() function call, e.g.
  ##   cPt <- catProgress(i, n, 0, cPt)
  
  if((i == 1) || ((i > 1) && ( round(100*i/n, r) != round(100*(i-1)/n, r) ))) {
    prog <- round(100*i/n, r)
    cat(prog, "%") 
    
    
    # estimate time remaining
    if(!missing(t_start)) {
      
      time_cur <- proc.time()[3]
      time_diff <- time_cur - t_start
      
      if(t_start == 0)
        t_start <- time_cur
      else if(time_diff > 0) {
        if((prog >= 10) && (prog < 100) )
          cat("\t\t\t", round(((time_diff * n / i) - time_diff)*SCALE), "s")
        else if(prog == 100) {
          cat("\n", time_diff, "s total")
        }
      }
      
    }
    
    cat("\n")
    if(!missing(t_start)) return(t_start)
  }
}



catProgress2 <- function(i, n)
{ ## Prints the progress after iteration i of n.
  ## Differs from original 'catProgress()' in that printing
  ##   is optimised for a single line of output.
  
  if(i == n)
    cat("100%\n")
  else if((i == 1) || ((i > 1) && ( round(100*i/n, -1) != round(100*(i-1)/n, -1) ))) {
    prog <- round(100*i/n, -1)
    if(prog != 100)
      cat(prog, "%... ", sep="")
  }
}





############################################################################

## prints progress in a for/while loop
# catProgress <- function(i, n, r=0){
#   if((i == 1) || ((i > 1) && ( round(100*i/n, r) != round(100*(i-1)/n, r) )))
#     cat(round(100*i/n, r), "%\n")
# }


############################################################################
####    PRINT OBJECT NAMES WITH ENUMERAITON    #############################
############################################################################

cnames <- function(x)
{
  nms <- names(x)
  if(is.matrix(x)) nms <- colnames(x)
  
  for(i in 1:len(nms)) {
    cat(i, ":", sep="")
    cat('"', nms[i], '" | ', sep="")
    if(i%%2 == 0) cat("\n")
  }
  
}


############################################################################
####    CBIND TWO VECTORS OF UNEQUAL LENGTH    #############################
############################################################################

cb_eq <- function(x, y)
{ ## cbind two vectors/matrices into new matrix
  ##   with nrow the minimum nr of rows
  
  n_min <- min(nrow(cbind(x)), nrow(cbind(y)))
  return( cbind(cbind(x)[1:n_min, ], cbind(y)[1:n_min, ]) )
}



############################################################################
####    ROUND NUMERIC ELEMENTS OF DATA.FRAME OR LIST    ####################
############################################################################

round_df <- function(x, d)
{ ## round all numeric columns in a dataframe x to d digits
  
  if(class(x) != "data.frame") stop("Input needs to be a data.frame.")
  
  which_num <- which(unlist(lapply(x, is.numeric)))
  x_round <- round(x[, which_num], d)
  x[, which_num] <- x_round
  
  return(x)
}



############################################################################
####    Compare columns of two objects    ##################################
############################################################################

colcompare <- function(x, y)
{ ## Compare columns of 'x' and 'y' for equality.
  
  if(!all(dim(x) == dim(y))) stop("Dimension of inputs differ.")
  
  m <- ncol(x)
  
  col_match <- rep(FALSE, m)
  for(i in 1:m)
    col_match[i] <- all(x[,i] == y[,i])
  
  return(col_match)
}



############################################################################
####    Compare two lists    ##################################
############################################################################

listcompare <- function(x, y)
{ ## Compare columns of 'x' and 'y' for equality.
  
  if(!((mode(x) == "list") & (mode(y) == "list"))) stop("Input must be lists.")
  if(len(x) != len(y)) stop("Input lists must have equal lengths.")
  
  m <- len(x)
  
  l_match <- rep(FALSE, m)
  for(i in 1:m)
    l_match[i] <- all(x[[i]] == y[[i]])
  
  return(l_match)
}




############################################################################
####    CONVERT LIST WITH DATA.FRAMES TO SINGLE DATA.FRAME    ##############
############################################################################


df_from_list <- function(xlist)
{ ## Function to convert input list to single data.frame.
  ## List needs to consist of data.frames with equal col dimension.
  
  # Get dimension of output DF and ensure correct dimensionality
  xdim <- c(sum(unl(lapply(xlist, nrow))), ncol(xlist[[1]]))
  
  if(!(unl(lapply(xlist, ncol)) %=% xdim[2])) 
    stop("All component data.frames must have equal col dim.")
  
  # Reformat 'xlist' into columns
  xdf <- vector("list", xdim[2])
  for(i in 1:xdim[2])
    xdf[[i]] <- unl(lapply(xlist, function(x) x[,i]))
  
  # Convert reformatted list into DF
  xdf <- as.data.frame(xdf)
  names(xdf) <- names(xlist[[1]])
  
  return(xdf)
  
}





############################################################################
####    SEGMENTATION WITH REGARDS TO DIFF    ###############################
############################################################################

split_diff_into_list <- function(x, diff_max = 1, abs_diff = FALSE)
{ ## Function which splits a vector into distinct list elements,
  ##  where the difference between consecutive values is larger
  ##  than 'diff_max'.
  ## If no such differences, 'x' returned as a list
  
  if(!abs_diff)
    x_d <- c(0, which(diff(x) > diff_max), len(x)) + 1
  else
    x_d <- c(0, which(abs(diff(x)) > diff_max), len(x)) + 1
  
  if(len(x_d) == 2) return(list(x))
  
  out_list <- vector("list", len(x_d)-1)
  for(i in 1:(len(x_d)-1))
    out_list[[i]] <- x[x_d[i]:(x_d[i+1]-1)]
  
  return(out_list)
}





############################################################################
####    SEGMENTATION WITH REGARDS TO DIFF    ###############################
############################################################################

segment_on_diff <- function(x, diff_max=1, segment_length_min)
{ ## Function to segment a vector 'x' according to discontinuities
  ##  in successive values, i.e. a vector of timestamps. Differences
  ##   in successive 'x' values larger than 'diff_max' are marked as
  ##   separators of segments, and a matrix is returned with row dimension
  ##   equal to amount of segments, and two columns indicated start and
  ##   end indices of each segment.
  ## If 'segment_length_min' specified, only indices of segments that
  ##   adhere to minimum length requirement are included in matrix.
  
  idx <- which(abs(diff(x)) > diff_max)
  
  if(len(idx) < 1) {
    warning("No specified diffs detected.")
    return(NA)
  }
  
  idx_mat <- cb(c(0, idx) + 1, c(idx, len(x)))
  
  if(!missing(segment_length_min)) {
    idx_mat <- idx_mat[apply(idx_mat, 1, diff) >= segment_length_min, ]
    
    if(nrow(idx_mat) < 1) {
      warning("All segments shorter than min specified length.")
      return(NA)
    }
  }
  
  return(idx_mat)
}




############################################################################
####    ROW/COLUMN MISSING VALUE BREAKDOWN    ##############################
############################################################################


count_NA <- function(x, dimension=2)
{ # Count the nr of missing values per row ('dimension = 1')
  #   or column ('dimension = 2')
  
  na_sum <- apply(x, dimension, function(y) sum(is.na(y)))
  return(na_sum)
}




############################################################################
####    CALCULATE COEFFICIENT OF VARIATION    ##############################
############################################################################


cvar <- function(x)
{ # Calculate the coefficient of variation
  # Note that this is only defined for sample with mean(x) > 0
  
  if(mean(x) <= 0) stop("Coeff. of variation only defined when mean is positive.")
  
  return( sd(x)/mean(x) )
}




############################################################################
####    COLOURED BG PLOT    ################################################
############################################################################


plot_g <- function(..., type="p", pch=19, lwd=2, col="blue",
                   yaxt="s", xaxt="s", ann=T, ann_ax=T, bg_col="gray80",
                   return_ax_ticks=FALSE)
{
  # Plotting preamble to create coloured background specified by 'bg_col'.
  # Can use argument 'ann_ax' to disable all plot annotations.
  # Use function 'lplot_g' for shorthand to set type to line plot.
  
  plot(..., type="n", yaxt="n", xaxt="n", ann=F)#, bty="n")
  rect(par("usr")[1], par("usr")[3], par("usr")[2], par("usr")[4], col=bg_col, border=NA)
  
  par_x <- par("xaxp")
  par_y <- par("yaxp")
  ticks_x <- seq(par_x[1], par_x[2], len=(par_x[3]+1))
  ticks_y <- seq(par_y[1], par_y[2], len=(par_y[3]+1))
  abline(v=ticks_x, lty=3, col="white")
  abline(h=ticks_y, lty=3, col="white")
  
  if(!ann_ax) {
    yaxt <- "n"
    xaxt <- "n"
    ann <- F
  }
  
  par(new=T)
  if(type == "p") plot(..., type="p", pch=pch, col=col, yaxt=yaxt, xaxt=xaxt, ann=ann)
  if(type == "l") plot(..., type="l", lwd=lwd, col=col, yaxt=yaxt, xaxt=xaxt, ann=ann)
  if(type == "n") plot(..., type="n", yaxt=yaxt, xaxt=xaxt, ann=ann)
  
  if(return_ax_ticks)
    return(list("axis1"=ticks_x, "axis2"=ticks_y))
}

lplot_g <- function(...) plot_g(..., type="l")




############################################################################
####    SCROLL PLOT    #####################################################
############################################################################


scrollPlot <- function(start=1, window=TRUE, winsize, overlap=winsize/2, n, 
                       plotfun=function(xi){ plot(xi, xi) },
                       fig_path, ...)
{ ## Function to scroll through plotting windows of size 'winsize' with
  ##  specified overlap, with user-defined start and end points.
  ## Plotting code for each window specified as a function 'plotfun', which
  ##  takes one argument: the x-values of each respective window.
  ## Specifying 'fig_path' overrides the windowed plots, saving each
  ##  window instead using the 'png()' function with in format
  ##  'paste0(fig_path, j, ".png"), where j is the window iterator.
  ## Additional arguments '...' for function 'png()' if necessary.
  
  if(missing(fig_path)) {
    if(window) w()
    par(ask = TRUE)
  }
  
  j <- 0
  last <- FALSE
  
  if(winsize > n) winsize <- n
  if(overlap > winsize) overlap <- winsize
  
  winsize <- as.integer(winsize)
  overlap <- as.integer(overlap)
  
  while(TRUE) {
    j = j+1
    xi <- (overlap*(j-1)+start):(start-1+winsize + overlap*(j-1))
    
    if(max(xi) >= floor(n)) {
      last <- TRUE
      xi <- xi[1]:n
    }
    
    ####################
    
    if(!missing(fig_path))
      png(paste0(fig_path, format_ind(j, 4), ".png"), width=1400, height=900, ...)
    
    plotfun(xi)
    
    if(!missing(fig_path)) {
      invisible(dev.off())
      
      if(j == 1) cat("Processing", floor(n/overlap), "total windows...\n")
      catProgress(j, floor(n/overlap))
    }
    
    ####################
    
    if(last) break
    
  }
}




############################################################################
####    HDF5 FILE READING SHORTHAND    #####################################
############################################################################

h5read2 <- function(..., compoundAsDataFrame = TRUE, bit64conversion = "double", 
                    tableObject = TRUE, convert_index = 9)
{ ## wrapper to read H5 files with "rhdf5" package
  
  require(rhdf5)
  
  if(tableObject) 
    dat <- h5read(..., bit64conversion=bit64conversion, compoundAsDataFrame=compoundAsDataFrame)$table
  else
    dat <- h5read(..., bit64conversion=bit64conversion, compoundAsDataFrame=compoundAsDataFrame)
  
  if(convert_index > 0)
    dat$index <- dat$index/(10^convert_index)
  
  return(dat)
}




############################################################################
####    FORMAT INDEX FOR FILENAMES     #####################################
############################################################################

format_ind <- function(i, digits = 2)
{ ## Format index 'i' for use in file name by pasting
  ##  leading "0"s based on max nr of digits spec by 'digits'.
  
  fi <- as.char(i)
  while(nchar(fi) < digits)
    fi <- paste0("0", fi)
  
  return(fi)
}



############################################################################
####    CONVERT LONG DATA FORMAT TO WIDE     ###############################
############################################################################

widen <- function(dat, index_col=1, var_cols=2:ncol(dat))
{ ## Converts DF 'dat' in long format to wide.
  ## Repeated values (such as timestamps) specified in 'index_col', while
  ##   variables to be used in output specified in 'var_cols'. Note that
  ##   both arguments can be either a numeric index or referred to by name.
  
  is_DT <- is.data.table(dat)
  dat <- as.data.frame(dat)
  
  # Convert index and variable col specifiers to numeric
  if(mode(index_col) != "numeric")
    index_col <- which(index_col == names(dat))
  
  if(mode(var_cols) != "numeric")
    var_cols <- match(var_cols, names(dat))
  
  # Create output DF
  df_out <- setNames(as.data.frame(matrix(NA, nrow=nrow(dat)*len(var_cols), ncol=3)),
                     c("index", "value", "variable"))
  
  # Convert 'dat' to wide format
  df_out[[1]] <- rep(dat[[index_col]], len(var_cols))
  df_out$value <- unl(dat[, var_cols])
  df_out$variable <- as.factor(rep(names(dat)[var_cols], each=nrow(dat)))
  
  # Return as original data format
  if(is_DT)
    return(as.data.table(df_out))
  return(df_out)
}




############################################################################
####    SPLIT DATAFRAME ON A DATETIME INDEX COLUMN     #####################
############################################################################

split_on_datetime <- function(dat, index_col=1, freq=24, offset=12)
{ ## Split DF/DT 'dat' into groups formed by column 'index_col'.
  ## Column 'index_col' is required to be in POSIXct format.
  ## Groups are split into non-overlapping time durations specified
  ##   'freq' (hours), with a baseline of 00:00 in the day to which
  ##   a specifiable offset 'offset' (hours) can be added.
  ##   (E.g. the default parameters of 'freq=24' and 'offset=12' would
  ##   split the data into daily groups, starting at 12:00.)
  
  
  idx <- dat[[index_col]]
  if(!any(class(idx) == "POSIXct")) stop("Index column needs to be POSIXct format.")
  
  # Determine initial break
  idx_day1 <- strptime(as.char(as.Date(idx[1])), format="%Y-%m-%d") + offset*3600
  
  # Form sequence of breaks
  idx_seq <- seq(idx_day1, tail(idx, 1), by=freq*3600)
  if(idx[1] < idx_seq[1])
    idx_seq <- c(idx[1]-1, idx_seq)
  if(tail(idx, 1) > tail(idx_seq, 1))
    idx_seq <- c(idx_seq, tail(idx, 1)+1)
  
  # Create labels correspondings with break points and split data into list
  idx_labels <- cut(idx, breaks=idx_seq)
  
  return( split(dat, idx_labels) )
  
}




############################################################################
####    OPERATOR OVERLOADING    ############################################
############################################################################

## compares all elements of two objects
"%=%" <- function(x, y) all(x == y)




############################################################################
####    OTHER USEFUL FUNCTION DECLARATIONS    ##############################
############################################################################

## shorthand for function 'windows()'
# w <- function(width=14, height=7, ...) windows(width, height, ...)
w <- function(width=10, height=7, ...) x11(width=width, height=height, ...)

############################################################################

## shorthand for function 'length()'
len <- function(...) length(...)

############################################################################

## shorthand for function 'cbind()'
cb <- function(...) cbind(...)

############################################################################

## shorthand for function 'unlist()'
unl <- function(...) unlist(...)

############################################################################

## shorthand for function numeric typing functions '
##  'numeric', as.numeric()' and 'is.numeric()'
num <- function(...) numeric(...)
as.num <- function(...) as.numeric(...)
is.num <- function(...) is.numeric(...)

############################################################################

## shorthand for function character typing functions 
##   'character()', 'as.character()' and 'is.character()'
char <- function(...) character(...)
as.char <- function(...) as.character(...)
is.char <- function(...) is.character(...)

############################################################################

## line plot
lplot <- function(..., col="forestgreen") plot(..., type="l", col=col)

############################################################################

## line-and-point plot
lpplot <- function(..., colL="forestgreen", colP=colL, pch=19, cex=0.8){ 
  plot(..., type="l", col=colL) ; points(..., col=colP, pch=pch, cex=cex)
}

############################################################################

## Gaussian smoother of numeric vector
ksmooth2 <- function(z, bw=5) 
  return( ksmooth(1:len(z), z,  kernel="normal", bandwidth=bw)$y )

############################################################################

## calculates resultant acceleration vector
acc_res <- function(mat, scaling=12)
  return(scaling*sqrt(rowSums(((mat - 2^15) / (2^16))^2)) )

############################################################################

## wrapper to write CSV file with no quotes, row names or column names
write.table2 <- function(xx, file="", sep=",", row.names=FALSE, col.names=FALSE, quote=FALSE, ...)
  write.table(xx, file=file, sep=sep, row.names=row.names, col.names=col.names, quote=quote, ...)

############################################################################

## wrapper to automatically insert standard POSIXct origin data
as.POSIXct2 <- function(...) as.POSIXct(..., origin="1970-01-01")

############################################################################

## Overload 'head' function with custom behaviour for lists
head <- function(x, ...) 
{ 
  if(is.list(x) && !is.data.frame(x))
    list_head(x, ...)
  else
    utils::head(x, ...)
}

############################################################################

## Convenience function to print full correlation output
cor_full <- function(x, y=NULL, ...)
{
  if(ncol(cb(x)) > 1) {
    y <- x[,2]
    x <- x[,1]
  }
  
  cr1 <- cor(x, y, method="pearson")
  cr2 <- cor(x, y, method="spearman")
  cat("Pearson: ", round(cr1, 4), "\n")
  cat("Spearman:", round(cr2, 4), "\n")
  
  invisible(c("pearson"=cr1, "spearman"=cr2))
}

############################################################################



############################################################################
############################################################################
############################################################################

setwd(old_wd)
rm(old_wd)