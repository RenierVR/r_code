zPlot <- function(y, x=1:nrow(cbind(y)), ynames, scl_in=0.5, scl_out=1.5, col_f, col_z, scroll_prop=0.1,
                  par_new=FALSE, optimise_speed=TRUE, optimise_maxlen=2500, 
                  win=TRUE, win_w=17, win_h=9.5, lty_z,
                  points_coord_list, col_points, pch_z=19)
{ 
  #######################################################################
  ## Function to dynamically zoom in/out or scroll through signal(s)
  ## y: input vector or matrix of which each column represents signal
  ##    with a common x-axis
  ## scl_in: proportion by which to zoom in
  ## scl_out: proportion by which to zoom out
  ## col_f: colour(s) of full line plot
  ## col_z: colour(s) of zoomed line plot
  ## scroll_prop: proportion on left and right of x-axis dedicated to
  ##              "scrolling" rather than zooming
  ## par_new: if FALSE, zoomed signals plotted on common y-axis
  ##          if TRUE, par("new") is used to overlay signals (the y-axis
  ##          of the second signal will be drawn on RHS)
  ## optimise_speed: if specified, plotting is made efficient and fast
  ##                 (mainly by downsampling very long signals)
  ## optimise_maxlen: the maximum allowable length of signals before
  ##                  optimisation occurs
  ## win: opens a external window for the figure
  ## win_w: width of window if win=T
  ## win_h: height of window if win=T
  ## points_coord_list: list of matrices which contain x-y coordinates
  ##                    of points to be included in (zoomed) plot
  ## col_points: vector of colours of length equal to length of
  ##             points_coord_list
  ## pch_z: the plotting character to be used for all points
  #######################################################################
  
  
  ## detect OS and set window function accorindgly
  if(Sys.info()[1] == "Windows")
    w <- function(...) windows(...)
  else
    w <- function(...) x11(...)
  
  
  lplot <- function(...) plot(..., type="l")
  len   <- function(...) length(...)
  
  zoomfn <- function(scl = 1, l.coord)
  { 
    usr <- par("usr")
    xrange <- usr[2] - usr[1]
    yrange <- usr[4] - usr[3]
    return(c(l.coord$x - 0.5 * scl * xrange, l.coord$x + 0.5 * scl * xrange,
             l.coord$y - 0.5 * scl * yrange, l.coord$y + 0.5 * scl * yrange))
  }
  
  
  if(!is.matrix(y))
    y <- cbind(y)
  
  n <- nrow(y)
  m <- ncol(y)
  
  x <- cbind(x)
  if(ncol(x) == 1) x <- matrix(x, nrow(x), ncol(y))
  
  
  ## open window and set layout
  if(win) w(win_w, win_h)
  par(mar = c(0, 4, 2, 3) + 0.1, oma=c(4,0,0,0))
  layout(matrix(c(1,2), 2, 1, byrow = TRUE), heights=c(1,2))
  
  ## control line type
  if(missing(lty_z))
    lty_z <- rep(1,m)
  
  ## specify optional plotting of points
  n_pointsets <- 0
  if(!missing(points_coord_list)) {
    if(class(points_coord_list) == "list")
      n_pointsets <- len(points_coord_list)
    else {
      points_coord_list <- list(points_coord_list)
      n_pointsets <- 1
    }
    
    if(missing(col_points))
      col_points <- suppressWarnings(matrix(c("red", "orange", "brown"), nr=n_pointsets, nc=1))[,1]
  }
  
  
  #######################################################################
  ## initial plot
  #######################################################################
  
  
  y_plot <- y
  x_plot <- 1:n
  if(optimise_speed){
    if(n > optimise_maxlen){
      y_plot <- y[seq(1, n, floor(n/optimise_maxlen)), , drop=F]
      x_plot <- x_plot[seq(1, n, floor(n/optimise_maxlen))]
    }    
  }
  
  ## organise plotting colours
  if(missing(col_f))
    col_f <- c("blue", "mediumseagreen", "red", "orange", "purple")
  if(missing(col_z))
    col_z <- col_f
  
  ## organise signal names
  if(missing(ynames)){
    if(is.null(colnames(y)))
      ynames <- as.character(1:ncol(y))
    else
      ynames <- colnames(y)
  }
  
  
  ## plot initial (fully zoomed out) signals
  y_plot_ylim <- range(y_plot)
  lplot(x_plot, y_plot[,1], ylim=y_plot_ylim, xlab="", ylab="", lwd=2, col=col_f[1])
  if(m > 1)
    for(i in 2:m)
      lines(x_plot, y_plot[,i], lwd=2, col=col_f[i])
  
  if(ncol(y) > 1)
    legend("bottomright", lwd=2, col=col_f, leg=ynames, cex=0.75, bg="white")
  
  
  if(par_new)
    y_plot_ylim <- range(y_plot[,1])
  
  lplot(x_plot, y_plot[,1], ylim=y_plot_ylim, xlab="", ylab="", lwd=2, col=col_z[1], lty=lty_z[1])
  if(m > 1)
    for(i in 2:m){
      if(par_new){
        par(new=T) ; lplot(x_plot, y_plot[,i], lwd=2, col=col_z[i], ann=F, axes=F, lty=lty_z[i])
      }
      else  
        lines(x_plot, y_plot[,i], lwd=2, col=col_z[i], lty=lty_z[i])
    }
  
  y_plot_ylim <- range(y_plot)
  z_xlim     <- c(1, n)
  scroll_lim <- z_xlim
  
  
  ## allow zooming in/out indefinitely
  while(1){
    
    lco <- locator(1)
    if(is.null(lco))
      break
    
    
    #######################################################################
    ## calculate new axes limits based on user selected location
    #######################################################################
    
    is_outside   <- FALSE
    is_scrolling <- FALSE
    
    z_usr <- par("usr")
    ## check if user location outside plotting area
    if((lco$x < z_usr[1]) || (lco$x > z_usr[2]) || (lco$y < z_usr[3]) || (lco$y > z_usr[4]))
      is_outside <- TRUE
    
    
    if(is_outside){
      ## if outside, zoom out
      scl_use <- scl_out
      
      ## special case: if location on original signal, scroll to that point
      if((lco$x > z_xlim[1]) & (lco$x < z_xlim[2]) & (lco$y > z_usr[4])){
        lco_p <- (lco$x - z_xlim[1])/diff(range(z_xlim))
        lco$x <- lco_p*n
        is_scrolling <- TRUE
      }
    }
    else{
      ## if inside, check if location in "scrolling limits"
      if((lco$x < scroll_lim[1]) || (lco$x > scroll_lim[2])){
        scl_use <- 1
        is_scrolling <- TRUE
      }
      else
        scl_use <- scl_in
    }
    
    #cat("IS_OUTSIDE: ", is_outside, "\n")
    #cat("IS_SCROLLING: ", is_scrolling, "\n\n")
    
    
    ## calculate updated x-axis range
    if(!is_scrolling){
      nr <- zoomfn(scl_use, lco)
      xr <- round(nr[1:2],0)
      if(xr[1] < 1) 
        xr[1] <- 1
      if(xr[2] > n) 
        xr[2] <- n
    }
    else{
      xr <- round(lco$x + c(-1,1)*round(diff(range(z_xlim))/2,0), 0)
    }
    
    
    ## ensure stable behaviour at signal limits
    if(xr[2] < 1)
      xr <- z_xlim
    if(xr[1] > n)
      xr <- z_xlim
    
    z_xlim <- xr[1:2]
    
    
    ## define the "scrolling limit"
    scroll_lim <- z_xlim + c(1,-1)*diff(z_xlim)*scroll_prop
    
    
    
    #######################################################################
    ## plot updated figure after user selection
    #######################################################################
    
    a_lines <- xr[1]:xr[2]
    if(optimise_speed){
      if(len(a_lines) > optimise_maxlen)
        a_lines <- seq(xr[1], xr[2], floor(len(a_lines)/optimise_maxlen))
    }
    
    plot(x_plot, y_plot[,1], ylim=y_plot_ylim, xlab = "", ylab = "", type="n")
    abline(v=a_lines, col="yellow")
    for(i in 1:m)
      lines(x_plot, y_plot[,i], lwd=2, col=col_f[i])
    
    if(ncol(y) > 1)
      legend("bottomright", lwd=2, col=col_f, leg=ynames, cex=0.75, bg="white")
    
    
    
    ## plot current zoom selection
    zx <- z_xlim
    if(z_xlim[1] < 1)
      zx[1] <- 1
    if(z_xlim[2] > n)
      zx[2] <- n
    y_zplot <- y[zx[1]:zx[2], , drop=F]
    x_zplot <- zx[1]:zx[2]
    
    if(optimise_speed){
      if(nrow(y_zplot) > optimise_maxlen){
        y_zplot <- y_zplot[seq(1, nrow(y_zplot), floor(nrow(y_zplot)/optimise_maxlen)), , drop=F]
        x_zplot <- x_zplot[seq(1, len(x_zplot), floor(len(x_zplot)/optimise_maxlen))]
      }
    }
    
    if(par_new)
      y_zplot_ylim <- range(y_zplot[,1])
    else
      y_zplot_ylim <- range(y_zplot)
    
    lplot(x_zplot, y_zplot[,1], xlab="", ylab="", ylim=y_zplot_ylim, lwd=2, col=col_z[1], lty=lty_z[1]) 
    if(m > 1)
      for(i in 2:m){
        if(par_new){
          par(new=T) ; lplot(x_zplot, y_zplot[,i], lwd=2, col=col_z[i], ann=F, axes=F, lty=lty_z[i])
          if(i == 2) axis(side=4)
        }
        else  
          lines(x_zplot, y_zplot[,i], lwd=2, col=col_z[i], lty=lty_z[i])
      }
    abline(v=scroll_lim, lty=2, col="grey")
    
    
    ## (optionally) plot points
    if(n_pointsets > 0) {
      for(j in 1:n_pointsets) {
        
        pksmat <- points_coord_list[[j]]
        pks_zplot <- pksmat[(pksmat[,1] >= x_zplot[1]) & (pksmat[,1] <= x_zplot[len(x_zplot)]),]
        
        points(pks_zplot, col=col_points[j], pch=pch_z)
        
      }
    }
    
  }
}
