library(ggplot2)


############################################################################
####    DENSITY PLOT    ####################################################
############################################################################

wgg_density <- function(x, fill="mediumseagreen", xrange, xlb="x", win=TRUE, ...)
{
  ggdat <- data.frame(x=x)
  gg <- ggplot(ggdat, aes(x=x)) + xlab(xlb)
  gg <- gg + geom_density(fill=fill, ...)
  
  if(!missing(xrange))
    gg <- gg + coord_cartesian(xlim = c(xrange[1], xrange[2]))
  
  if(win) w()
  gg
}


############################################################################
####    MULTIVARIATE DENSITY PLOT    #######################################
############################################################################

wgg_density_mv <- function(xdf, xlb="x", main, win=TRUE, leg_pos="right", alpha=0.5, ...)
{
  if(class(xdf) == "matrix") xdf <- as.data.frame(xdf)
  fnames <- names(xdf)
  if(is.null(fnames)) fnames <- paste0("Element", 1:ncol(xdf))
  
  ggdat <- data.frame("score" = xdf[[1]], "name"=fnames[1], stringsAsFactors=F)
  for(i in 2:len(fnames))
    ggdat <- rbind(ggdat, data.frame("score" = xdf[[i]], "name"=fnames[i], stringsAsFactors=F))
  
  gg <- ggplot(data=ggdat, aes(x=score, fill=name)) + geom_density(alpha=alpha, ...) + xlab(xlb)
  
  if(leg_pos == "right")
    gg <- gg + theme(legend.title=element_blank(), legend.justification = c(1, 1), legend.position = c(1, 1))
  else
    gg <- gg + theme(legend.title=element_blank(), legend.justification = c(0, 1), legend.position = c(0, 1))
  
  if(!missing(main))
    gg <- gg + ggtitle(main) + theme(plot.title = element_text(hjust = 0.5))
  
  if(win) w()
  gg
}

wgg_hist_mv <- function(xdf, xlb="x", main, win=TRUE, alpha=0.5, ...)
{
  if(class(xdf) == "matrix") xdf <- as.data.frame(xdf)
  fnames <- names(xdf)
  if(is.null(fnames)) fnames <- paste0("Element", 1:ncol(xdf))
  
  ggdat <- data.frame("score" = xdf[[1]], "name"=fnames[1], stringsAsFactors=F)
  for(i in 2:len(fnames))
    ggdat <- rbind(ggdat, data.frame("score" = xdf[[i]], "name"=fnames[i], stringsAsFactors=F))
  
  gg <- ggplot(data=ggdat, aes(x=score, fill=name)) + geom_histogram(alpha=alpha, ...) + xlab(xlb)
  gg <- gg + theme(legend.title=element_blank(), legend.justification = c(1, 1), legend.position = c(1, 1))
  
  if(!missing(main))
    gg <- gg + ggtitle(main) + theme(plot.title = element_text(hjust = 0.5))
  
  if(win) w()
  gg
}



############################################################################
####    CORRELATION PLOT    ################################################
############################################################################

# Reference:
# http://www.sthda.com/english/wiki/ggplot2-quick-correlation-matrix-heatmap-r-software-and-data-visualization

wgg_corr <- function(dat, win = TRUE, 
                     cluster_cor_matrix=TRUE,
                     add_text_to_tiles = TRUE,
                     tile_border_col = NA,
                     show_legend = TRUE,
                     legend_lim = NULL)
{
  
  # Calucate Pearson correlation matrix
  dat_cor <- cor(dat)
  
  # Use hierarchical clustering to order correlation matrix
  if(cluster_cor_matrix) {
    hc <- hclust(as.dist(1-dat_cor))
    dat_cor <- dat_cor[hc$order, hc$order]
  }
  
  # Use only upper-tri part of cor matrix to form plot data
  dat_cor[lower.tri(dat_cor)] <- NA
  ggdat <- melt(round(dat_cor, 2), na.rm=T)
  
  # Determine limits of plot legend
  if(is.null(legend_lim))
    legend_lim <- range(ggdat$value)
  
  #  Create ggplot object
  gg <- ggplot(data=ggdat, aes(x=Var2, y=Var1, fill=value)) + geom_tile(color=tile_border_col)
  gg <- gg + scale_fill_gradient(low = "blue", high = "red", name="Pearson\nCorrelation", limits=legend_lim)
  gg <- gg + theme_minimal() + theme(axis.text.x=element_text(angle=45, vjust=1, size=12, hjust=1))
  gg <- gg + coord_fixed()
  
  if(show_legend) {
  gg <- gg + theme(axis.title.x = element_blank(), axis.title.y = element_blank(),
                                   legend.justification = c(1, 0),
                                   legend.position = c(0.35, 0.75),
                                   legend.direction = "horizontal")
  gg <- gg + guides(fill = guide_colorbar(barwidth=7, barheight=1, title.position="top", title.hjust=0.5)) 
  }
  else
    gg <- gg + theme(axis.title.x = element_blank(), axis.title.y = element_blank(), legend.position="none")
  
  if(add_text_to_tiles)
    gg <- gg + geom_text(aes(Var2, Var1, label = value), color = "black", size = 4)
  
  # Output 
  if(win) { 
    w()
    print(gg)
  }
  
  return(gg)
  
}



# Concern: it appears that the new operational flow was conceived to primarily solve for ideal problem cases that we have historically rarely encountered.
# 
# Elaboration: the new flow seems very well suited for cases where (a) the Discovery department develops a new idea with potential market value or  