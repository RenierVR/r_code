tiaan_specgram <- function (data, windowSize=2^8, overlap= windowSize-1, Fs=50, output=FALSE,
                            graph=TRUE, normal=1, blankFactor=3, maxfreq=1, minfreq=0,
                            dBTrans=FALSE, logTrans=TRUE, logTransBase=1.3, logAddFactor=1,
                            n_xlabels=8, 
                            colMap=colorRampPalette(c("black", "black", "blue", "green", "yellow", "red")),
                            line_overlay, col_overlay_l="white", points_overlay, col_overlay_p="white")
{
  #overlap spesified as number of samples (< windowSize)
  #blankFactor: number of times the window size, which will be used to add blanks
  #normal: if 2, subtract mean and divide by stdev, if 1 scale to [0,1], otherwise don't scale
  #maxfreq: maximum frequency on y-axis in Hz
  #dBTrans: if output should be transformed to decibel scale
  #logTrans: if output should be transformed to log scale
  # NOTE: if both dBTrans and logTrans is set to true, then dBTrans will be used
  #Output: whether spectogram data should be printed or not
  #Graph: whether spectrogram should be plotted or not
  
  normalize <- function(x) 
  { 
    x <- sweep(x, 2, apply(x, 2, min)) 
    sweep(x, 2, apply(x, 2, max), "/") 
  }
  
  n<-length(data)
  out<-NULL
  for(i in 0:(floor((n-windowSize)/(windowSize-overlap))-1))
  {   
    tempdat<-c(data[1:windowSize+(windowSize-overlap)*i],rep(0,blankFactor*(windowSize))) # zero padding
    out<-cbind(out,(abs(fft(tempdat)[1:(length(tempdat)/2)+1]))^2)
  }
  
  if(logTrans==TRUE) {out<-log(out+logAddFactor, base=logTransBase)}
  if(dBTrans==TRUE)  {out<-log(out,10)*10}
  
  y1=seq(0,(Fs/2),((Fs/2))/nrow(out))[-1]
  
  if(normal==1){out2<-normalize(t(out))}
  else if(normal==2) {out2<-matrix(as.numeric(scale(t(out))),ncol=length(y1))}
  else out2<-(t(out))
  
  if(graph==TRUE){
#     filled.contour(y=y1,out2,ylim=c(minfreq,maxfreq),col = colMap, nlevels=25)
    
    if(!missing(line_overlay) && !missing(points_overlay))
      filled.contour(y=y1, out2, ylim=c(minfreq,maxfreq), color.palette=colMap,
                     plot.axes={ axis(1, at=pretty(1:n, n_xlabels)/n, lab=pretty(1:n, n_xlabels)) ; axis(2) ; 
                                 lines(1:n/n, line_overlay, lwd=3, col=col_overlay_l) ; 
                                 points(points_overlay[,1]/n, points_overlay[,2], pch=19, cex=1, col=col_overlay_p)} )
      
    else if(!missing(line_overlay))
      filled.contour(y=y1, out2, ylim=c(minfreq,maxfreq), color.palette=colMap,
                     plot.axes={ axis(1, at=pretty(1:n, n_xlabels)/n, lab=pretty(1:n, n_xlabels)) ; axis(2) ; 
                                 lines(1:n/n, line_overlay, lwd=3, col=col_overlay_l)} )
    
    else if(!missing(points_overlay))
      filled.contour(y=y1, out2, ylim=c(minfreq,maxfreq), color.palette=colMap,
                     plot.axes={ axis(1, at=pretty(1:n, n_xlabels)/n, lab=pretty(1:n, n_xlabels)) ; axis(2) ; 
                                 points(points_overlay[,1]/n, points_overlay[,2], pch=19, cex=1, col=col_overlay_p)} )
    
    else
      filled.contour(y=y1, out2, ylim=c(minfreq,maxfreq), color.palette=colMap,
                     plot.axes={ axis(1, at=pretty(1:n, n_xlabels)/n, lab=pretty(1:n, n_xlabels)) ; axis(2)} )
  }
  
  if(output==TRUE){list(out=out2[,which(y1>minfreq & y1<maxfreq)],y=y1)}
  
}




#########################################################################################################
#########################################################################################################
#########################################################################################################
#########################################################################################################





tiaan_specgram_opt <- function (data, windowSize=2^8, overlap= windowSize-1, Fs=50, 
                                graph=TRUE, normal=1, blankFactor=3, maxfreq=1, minfreq=0,
                                logTrans=TRUE, logTransBase=1.3, logAddFactor=1,
                                nXlabels=8, optPlot=F, optFactor=1000,
                                colMap=colorRampPalette(c("black", "black", "blue", "green", "yellow", "red")),
                                lineOverlay, lcolOverlay="white", pointsOverlay, pcolOverlay="white")
{
  
  # data: numeric vector
  # windowSize: the size of overlapping windows on which FFTs will be taken
  # overlap: specified as number of samples that overlaps (< windowSize)
  # Fs: sampling frequency
  # graph: toggles plotting of spectrogram
  # normal: normalisation, if 1 scale to [0,1], if 2, subtract mean and divide by stdev, otherwise don't scale
  # blankFactor: number of times the window size of blanks to be added to each window (zero padding)
  # maxfreq: maximum frequency on y-axis in Hz
  # minfreq: minimum frequency on y-axis in Hz
  # logTrans: toggles log transformation
  # logTransBase: base of log transformation
  # logAddFactor: additive factor in log term (to prevent taking log of 0)
  # nXlabels: controls number of labels on x-axis
  # optPlot: toggles optimisation of plotting time (by reducing output to be plotted)
  # optFactor: factor by which output data reduced before plotting
  # colMap: the function which determines the colour scheme
  # lineOverlay: if specified, a vector (with length equal to data) which will be overlayed on plot as a line
  # lcolOverlay: colour of overlayed line
  # pointsOverlay: if specified, a 2-column coordinate matrix with rows equal to length of data, to be overlayed on plot
  # pcolOverlay: colour of overlayed points
  


  normalize <- function(x) 
  { ## function which normalises to [0,1] range
    x <- sweep(x, 2, apply(x, 2, min)) 
    sweep(x, 2, apply(x, 2, max), "/") 
  }
  
  ###############################################
  
  n   <- length(data)
  out <- NULL
  
  i_max <- floor((n-windowSize)/(windowSize-overlap))
  tempmat <- matrix(0, nr=windowSize*(1+blankFactor), nc=i_max) 
  
  ## constructing matrix with overlapping windows and zero padding
  for(i in 1:i_max)
    tempmat[,i] <- c(data[1:windowSize + (windowSize-overlap)*(i-1)], rep(0, blankFactor*(windowSize)))
  
  rm(data)

  ## perform multivariate FFT and obtain power spectrum
  out <- mvfft(tempmat)[2:(nrow(tempmat)/2+1),]
  rm(tempmat)
  
  out <- apply(out, 2, abs)
  out <- out^2
  
  
  if(logTrans)
    out <- log(out+logAddFactor, base=logTransBase)
  
  y1 <- seq(0, (Fs/2), by=((Fs/2))/nrow(out))[-1]
  
  
  ## normalization of FFT matrix
  if(normal == 1)
    out2 <- normalize(t(out))
  else if(normal == 2)
    out2 <- matrix(as.numeric(scale(t(out))), ncol=length(y1))
  else 
    out2 <- t(out)
  
  
  
  ## plot spectrogram if specified
  if(graph){
  
    if(optPlot)
      if(nrow(out2) > optFactor)
        out2 <- out2[seq(1, nrow(out2), by=floor(nrow(out2)/optFactor)), ]
    
    
    
    ## if line and points to be overlayed:
    if(!missing(lineOverlay) && !missing(pointsOverlay))
      filled.contour(y=y1, out2, ylim=c(minfreq,maxfreq), color.palette=colMap,
                     plot.axes={ axis(1, at=pretty(1:n, nXlabels)/n, lab=format(pretty(1:n, nXlabels), digits=12)) ; 
                                 axis(2) ; lines(1:n/n, lineOverlay, lwd=3, col=lcolOverlay) ; 
                                 points(pointsOverlay[,1]/n, pointsOverlay[,2], pch=19, cex=1, col=pcolOverlay)} )
    
    ## if line to be overlayed:
    else if(!missing(lineOverlay))
      filled.contour(y=y1, out2, ylim=c(minfreq,maxfreq), color.palette=colMap,
                     plot.axes={ axis(1, at=pretty(1:n, nXlabels)/n, lab=format(pretty(1:n, nXlabels), digits=12)) ; 
                                 axis(2) ; lines(1:n/n, lineOverlay, lwd=3, col=lcolOverlay)} )
    
    ## if points to be overlayed:
    else if(!missing(pointsOverlay))
      filled.contour(y=y1, out2, ylim=c(minfreq,maxfreq), color.palette=colMap,
                     plot.axes={ axis(1, at=pretty(1:n, nXlabels)/n, lab=format(pretty(1:n, nXlabels), digits=12)) ; 
                                 axis(2) ; 
                                 points(pointsOverlay[,1]/n, pointsOverlay[,2], pch=19, cex=1, col=pcolOverlay)} )
    
    ## if no overlay:
    else
      filled.contour(y=y1[which(y1>minfreq & y1<maxfreq)], out2[,which(y1>minfreq & y1<maxfreq)], ylim=c(minfreq,maxfreq), color.palette=colMap,
                     plot.axes={ axis(1, at=pretty(1:n, nXlabels)/n, lab=format(pretty(1:n, nXlabels), digits=12)) ; 
                                 axis(2)} )
#       filled.contour(y=y1, out2, ylim=c(minfreq,maxfreq), color.palette=colMap,
#                      plot.axes={ axis(1, at=pretty(1:n, nXlabels)/n, lab=format(pretty(1:n, nXlabels), digits=12)) ; 
#                                  axis(2)} )
    
  }
  
  invisible(list(out=out2[,which(y1>minfreq & y1<maxfreq)], y=y1))
  
}




