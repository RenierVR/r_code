source("../useful_funs.r")

fpath <- "~/Data/PPG_ECG/"
datlist <- getDataFromDir(fpath, dataE=F)



ppg_sat_prop <- matrix(0, nrow=len(datlist), ncol=3)
# for(i in 1:len(datlist)) {
for(i in 5) {
  
  dat <- read.table(datlist[[i]], head=T, sep=",")
  
  # Set up single time delta column
  dat_t <- dat[dat[,1] == 13, 3:4]
  
  # which(is.na(dat_t[,1])) %=% which(!is.na(dat_t[,2]))
  # which(is.na(dat_t[,2])) %=% which(!is.na(dat_t[,1]))
  
  dat_t[is.na(dat_t[,1]), 1] <- dat_t[is.na(dat_t[,1]), 2]
  dat_t <- cumsum(dat_t[,1]/1000)
  
  
  # Set up ECG and PPG data columns 
  dat_ecg <- cb("t"=dat_t, dat[dat[,1] == 13, c(2, 6)])
  dat_ecg <- dat_ecg[dat_ecg$SUBTOKEN == 9, c(1, 3)]
  
  dat_ppg <- cb("t"=dat_t, dat[dat[,1] == 13, c(2, 5, 7)])
  dat_ppg <- dat_ppg[dat_ppg$SUBTOKEN == 10, c(1, 3:4)]
  
  
  # w() ; lplot_g(dat_ppg[,3], ylim=c(-32693, 32683))
  # zPlot(dat_ecg[,2])
  
  
  # Count PPG saturations
  ppg_min <- min(dat_ppg[,2])
  ppg_max <- max(dat_ppg[,2])
  
  if(ppg_min != ppg_max)
    ppg_sat_prop[i,] <- c(nrow(dat_ppg), sum(dat_ppg[,2] == ppg_min), sum(dat_ppg[,2] == ppg_max))
  else
    ppg_sat_prop[i,] <- c(nrow(dat_ppg), 0, sum(dat_ppg[,2] == ppg_max))
  
  catProgress(i, len(datlist))
}



8-13



min_sat <- colSums(ppg_sat_prop)[2]/colSums(ppg_sat_prop)[1]
max_sat <- colSums(ppg_sat_prop)[3]/colSums(ppg_sat_prop)[1]

aa <- ppg_sat_prop[,]
(colSums(aa)[2] + colSums(aa)[3])/colSums(aa)[1]




# Plot PPG against ECG
scrollPlot(start=1, winsize=10, n=tail(dat_ppg[,1], 1), window=F, plotfun=function(xi){
  x_ppg <- (dat_ppg[,1] >= xi[1]) & (dat_ppg[,1] <= tail(xi,1))
  x_ecg <- (dat_ecg[,1] >= xi[1]) & (dat_ecg[,1] <= tail(xi,1))
  
  lplot_g(dat_ecg[x_ecg, 1], dat_ecg[x_ecg, 2], col="red", lwd=3, 
          xlim=range(xi), ann=F, ann_ax=F)
  
  # par(new=T)
  # lplot(dat_ppg[x_ppg, 1], dat_ppg[x_ppg, 3], lwd=1, col="yellow",
  #       xlim=range(xi), ylim=range(dat_ppg[,3]), ann=F, yaxt="n", xaxt="n")
  # axis(4)
  
  par(new=T)
  lplot(dat_ppg[x_ppg, 1], dat_ppg[x_ppg, 2], lwd=3, xlim=range(xi),
        main = "ECG-PPG device demo", ylab="ECG", xlab="time units (samples)")
  axis(4)

  legend("bottomright", bg="white", lwd=2, col=c("red", "mediumseagreen"),
         leg=c("ECG", "PPG green"))  
  
})







