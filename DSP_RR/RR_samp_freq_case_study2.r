source("../useful_funs.r")
source("../ARCHIVE/HRV/ERMADT_funs.r")
source("../ARCHIVE/HRV/alignToRefRR.r")
source("../ARCHIVE/HRV/alignToRefRR_single.r")

library(signal)


rr_resample <- function(x, Fs_new, Fs_old=50)
{
  x_new <- Fs_old*x/1000
  x_new <- (1000*x_new)/Fs_new
  return(x_new)
}


###################################################################################################
##########################       HRV BATCH PROCESSING - NOMOTION       ############################
###################################################################################################

#########################################
## consolidation of no motion HRV data ##
#########################################

# fpath <- "D:/HealthQ/Data/HRV/HRV BATCH NOMOTION/"
fpath <- "/media/reniervr/Data/HealthQ/Data/HRV/HRV BATCH NOMOTION/"

datlist3 <- getDataFromDir(paste0(fpath, "201502 validation/"), head=T)
reflist3 <- getDataFromDir(paste0(fpath, "201502 validation/reference/"), ext="txt")
reflist3 <- lapply(reflist3, function(x) cbind(NA, diff(x[,1])))
reflist3 <- c(reflist3, getDataFromDir(paste0(fpath, "201502 validation/reference/"), head=T))
reflist3 <- reflist3[order(names(reflist3))]
reflist3 <- lapply(reflist3, function(x) x[,2])


datlist <- c(datlist3)
reflist <- c(reflist3)

###################################################################################################

###################################################################################################

i_used <- c(1, 4, 5, 9, 11, 17, 18, 20, 21, 22, 24, 29, 34)
i_used <- c(i_used, 2, 8, 13, 25, 26, 27, 28, 32, 33)


i_factor <- matrix(c(1, 2, 4, 5, 8, 9, 11, 13, 17, 18, 20, 21, 22, 
                     24, 25, 26, 27, 28, 29, 32, 33, 34,
                     49.7, 50.1, 50, 49.2, 49, 49.9, 49.3, 50, 
                     50.1, 49.5, 50.4, 49.6, 49.7, 49.8, 50.1, 
                     49.7, 49, 49.2, 49.6, 50, 49.4, 49.8),
                   nrow = 2, byrow = T)
fs_factor <- rep(50, max(i_used))
fs_factor[i_factor[1,]] <- i_factor[2,]

###################################################################################################

# HOW TO UPSAMPLE (WITH PLOT)
# grn50 <- grn
# x100 <- 1:(2*len(grn50))
# x50 <- x100[seq(1, 2*len(grn50), 2)]
# grn100 <- interp1(x=x50, y=grn50, 
#                   xi=x100, method="linear")
# 
# w() ; lplot_g(x50, grn50, lwd=3, xlim=c(6000, 6300))
# lines(x100, grn100, lwd=2, col="red")


# for(k in 1) {

fs_seq <- round(c(100:3), 0)
# fs_seq <- 0

out_MAE_all <- matrix(0, len(i_used), len(fs_seq))
out_MAPE_all <- matrix(0, len(i_used), len(fs_seq))
colnames(out_MAE_all) <- colnames(out_MAPE_all) <- paste0("FS_", fs_seq)

for(m in 1:len(fs_seq)) {
  
  
  RRlist_R <- vector("list", len(datlist))
  
  for(i in 1:len(datlist)) {
    
    Fs <- 50
    dat <- datlist[[i]]
    
    dat <- dat[dat[,1] != 0,]
    dat <- dat[10:nrow(dat),]
    curr1 <- curr2 <- matrix(0, nr=nrow(dat), nc=2)
    for(j in 1:1){
      if(j < 3){
        Rf <- dat[,8]
        led <- dat[,14]
      }
      else{
        Rf <- dat[,9]
        led <- dat[,j+12]
      }
      
      safe_term <- 0
      if(j == 2){
        safe_term <- 1
        led <- numeric(nrow(dat))
      }
      
      dat_tmp <- dat[,j] 
      isub_tmp <- dat[,9+j]
      curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
      curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e8)  
    }
    
    grn <- curr2[,1]
    
    grn100 <- interp1(x=seq(1, 2*len(grn), 2), y=grn,
                      xi=1:(2*len(grn)), method="cubic")
    grn100 <- na.omit(grn100)
    
    # grn_rs <- interp1(x=seq(1, len(grn100)), y=grn100,
    #                   xi=seq(1, len(grn100), len=(fs_seq[m]/100)*len(grn100)), method="cubic")
    
    grn_rs <- resample(grn100, 1, 100/fs_seq[m])
    
    grn_rs_25 <- interp1(x=seq(1, len(grn_rs)), y=grn_rs,
                         xi=seq(1, len(grn_rs), len=(25/fs_seq[m])*len(grn_rs)), method="linear")
    
    
    Fs <- 25
    
    hrv_out <- ER_MA_DT_peaks_C(grn_rs_25, filt_t="FIRHAM", clip=4*Fs, output=T, interp=T, 
                                bf_fs=Fs, bf_fs_actual=Fs,
                                check_mult_peaks_above_zero=T, post_filt_running_mean=F)
    rr1 <- hrv_out$RR
    xf1 <- hrv_out$filt_signal
    
    
    RRlist_R[[i]] <- rr1
  }
  
  
  
  
  ###################################################################################################
  
  
  
  # w(14, 7) 
  j <- 0
  out_mat <- matrix(0, len(i_used), 2)
  rr_good_all <- NULL
  for(i in sort(i_used)) {
    
    # i <- sort(i_used)[1]
    j <- j+1
    
    
    ref <- reflist[[i]]
    rr1 <- RRlist_R[[i]]
    
    ref_outl <- NA
    
    if(i == 2)
      ref_outl <- c(52, 53, 54)
    if(i == 8) {
      ref <- ref[-c(1:12)]
    }
    if(i == 9) {
      ref <- ref[-c(1:15)]
    }
    if(i == 10) {
      ref <- ref[-c(1:25)]
    }
    if(i == 13) {
      ref <- ref[-c(1:85)]
      rr1 <- rr1[-c(1:80)]
    }
    if(i == 20)
      ref <- ref[10:130]
    if(i == 21) {
      ref <- ref[-c(1:19)]
    }
    if(i == 25) {
      ref <- ref[-c(1:7)]
      rr1 <- rr1[-c(1:6)]
    }
    if(i == 26)
      ref_outl <- c(108, 109)
    if(i == 27) {
      ref_outl <- c(28, 29)
      ref <- ref[-c(1:60)]
      rr1 <- rr1[-c(1:40)]
    }
    if(i == 28) {
      ref_outl <- c(28, 29)
      ref <- ref[-c(1:55)]
      rr1 <- rr1[-c(1:50)]
    }
    
    nid <- as.character(m)
    if(m < 10)
      nid <- paste0("0", nid)
    
    
    rr1 <- rr_resample(rr1, fs_factor[i])
    
    align_out <- alignToRefRR_single(ref = ref, rr1 = rr1, ref_o = ref_outl, SHOW_PLOT=F)
    # title(main = paste(names(datlist)[i], "| Fs =", fs_seq[m],
    #                    "\nMAE =", round(align_out[[1]],2), "ms | MAPE =", 
    #                    round(align_out[[2]],2), "%"))
    
    # savePlot(paste0("./results/RR noise sensitivity/plots/",
    #                 paste0(names(datlist)[i], "_fs", nid, "_", fs_seq[m], ".png")), "png")
    
    # graphics.off()
    
    out_mat[j,] <- c(align_out[[1]], align_out[[2]])
    rr1 <- align_out[[4]]
    # write.table2(round(rr1, 4), 
    #              file = paste0("./results/RR noise sensitivity/",
    #                            paste0(names(datlist)[i], "_RR_noise", nid, "_", fs_seq[m]), ".txt"))
    # 
    # catProgress(j, len(i_used))
  }
  
  out_MAE_all[,m] <- out_mat[,1]
  out_MAPE_all[,m] <- out_mat[,2]
  
  # write.table2(round(out_MAE_all,4),
  #              file = paste0("results/RR noise sensitivity/stats/stats_MAE_all_run", k, ".txt"), 
  #              sep = ", ", col.names = T)
  # 
  # write.table2(round(out_MAPE_all,4),
  #              file = paste0("results/RR noise sensitivity/stats/stats_MAPE_all_run", k, ".txt"), 
  #              sep = ", ", col.names = T)
  
  
  
  catProgress(m, len(fs_seq))
  graphics.off()
}

# }


# out_MAE_all_b <- out_MAE_all
# out_MAPE_all_b <- out_MAPE_all


out1 <- colMeans(out_MAE_all)
out2 <- colMeans(out_MAPE_all)

w() ; lplot_g(fs_seq, out1, ylab="MAE (ms)", xlab="fs (Hz)", col="blue",
              main="Effect of sampling frequency of RR error")
abline(v=25, col="red", lwd=2, lty=2)
savePlot("plot1.png", "png")


w() ; lplot_g(fs_seq[which_in(fs_seq, c(10, 50))], out1[which_in(fs_seq, c(10, 50))], ylab="MAE (ms)", xlab="fs (Hz)", 
              main="Effect of sampling frequency of RR error")
abline(v=25, col="red", lwd=2, lty=2)
savePlot("plot2.png", "png")


