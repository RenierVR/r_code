source("../useful_funs.r")
# source("../DSP_RR/amendConfidencePostProcess.r")
library(rhdf5)

# enable viewing of 64-bit format timestamps
# options(digits = 22)

# out_metric_heartrate$index <- out_metric_heartrate$index/10^6
# as.POSIXlt(out_metric_heartrate$index[1]/1000, origin = "1970-01-01")

############################
##  READ HDF5 FILE CONTENTS
############################

h5 <- "~/HealthQ/DATA/Silabs RR testing/20170927/TPID0301_tmp.h5"

h5_contents <- h5ls(h5)
print( h5_contents[h5_contents$group != "/", c(1, 5)] )


# s_raw <- h5read(h5, "/raw_converted", bit64conversion="double", compoundAsDataFrame=T)$table
# s_trange <- out_raw[c(1, nrow(out_raw)), 1]

s_acc <- h5read(h5, "/metric_acc_energy", bit64conversion="double", compoundAsDataFrame=T)$table
s_events <- h5read(h5, "/event", bit64conversion="double", compoundAsDataFrame=T)$table

s_rr <- h5read(h5, "/metric_rr", bit64conversion="double", compoundAsDataFrame=T)$table
s_rrv <- h5read(h5, "/metric_rr_isvalid", bit64conversion="double", compoundAsDataFrame=T)$table
s_rrcm <- h5read(h5, "/metric_rr_conf", bit64conversion="double", compoundAsDataFrame=T)$table

ref_rr <- h5read(h5, "/metric_reference_rr", bit64conversion="double", compoundAsDataFrame=T)$table



############################
##  READ MOJO HDF5 FILE 
############################

h5 <- "~/HealthQ/DATA/Silabs RR testing/20170927/mojo/TPID0301_tmp.h5"

h5_contents <- h5ls(h5)
print( h5_contents[h5_contents$group != "/", c(1, 5)] )


# m_raw <- h5read(h5, "/raw_converted", bit64conversion="double", compoundAsDataFrame=T)$table
# m_trange <- out_raw[c(1, nrow(out_raw)), 1]

m_acc <- h5read(h5, "/metric_acc_energy", bit64conversion="double", compoundAsDataFrame=T)$table
m_events <- h5read(h5, "/event", bit64conversion="double", compoundAsDataFrame=T)$table

m_rr <- h5read(h5, "/metric_rr", bit64conversion="double", compoundAsDataFrame=T)$table
m_rrv <- h5read(h5, "/metric_rr_isvalid", bit64conversion="double", compoundAsDataFrame=T)$table
m_rrcm <- h5read(h5, "/metric_rr_conf", bit64conversion="double", compoundAsDataFrame=T)$table

ref_rr2 <- h5read(h5, "/metric_reference_rr", bit64conversion="double", compoundAsDataFrame=T)$table




############################
##  PLOTS
############################

mrr <- m_rr ; srr <- s_rr ; rrr <- ref_rr ; sacc <- s_acc

mrr[,1] <- mrr[,1]/1e6 - 6780
srr[,1] <- srr[,1]/1e6
rrr[,1] <- rrr[,1]/1e6
sacc[,1] <- sacc[,1]/1e6



scrollPlot(start = head(rrr[,1], 1), winsize=100000, n=tail(rrr[,1], 1), plotfun=function(xi){
  
  par(mfrow = c(2,1), mar=c(2, 3, 1, 1))
  
  xr <- which_in(rrr[,1], xi)
  xs <- which_in(srr[,1], xi)
  xm <- which_in(mrr[,1], xi)
  xsa <- which_in(sacc[,1], xi)
  
  lplot_g(rrr[xr, 1], rrr[xr, 2], col="red", lwd=3, ylim=c(500, 1200))
  lines(srr[xs, 1], srr[xs, 2], col="blue", lwd=2)
  lines(mrr[xm, 1], mrr[xm, 2], col="mediumseagreen", lwd=2)
  
  lplot_g(sacc[xsa, 1], sacc[xsa, 2], col="purple", lwd=3, ylim=range(sacc[,2]))
      
})





############################
##  Benchmarking
############################

# RR from Mojo
rrmat <- cbind(m_rr, m_rrcm[,2])
rrmat[,1] <- rrmat[,1]/1e9
rrmat <- rrmat[-c(1:30),]
dev_events <- m_events

# RR from Silabs
# rrmat <- cbind(s_rr, s_rrcm[,2])
# rrmat[,1] <- rrmat[,1]/1e9
# rrmat <- rrmat[-c(1:10),]
# dev_events <- s_events


refmat <- ref_rr
refmat[,1] <- refmat[,1]/1e9


m_bench <- benchmark_RR(rrmat, refmat, LAG_WIDTH = 2)

# Accuracy by confidence metric category
mb2 <- cb(m_bench$clean_out, "cm_cat"=cut(m_bench$clean_out$cm, c(1, seq(20, 100, 20))))
round(tapply(abs(mb2[,2]-mb2[,3]), mb2[,7], mean), 2)
round(100*table(mb2$cm_cat)/nrow(mb2), 1)

# Accuracy by protocol event
mb3 <- m_bench$all_out
mb3 <- mb3[!apply(mb3, 1, function(x) any(is.na(x))), ]
mb3_e <- rep("none", nrow(mb3))

dev_events[,1] <- dev_events[,1]/1e9 - m_bench$initial_lag

for(i in seq(1, nrow(dev_events), 2)) {
  trange <- dev_events[c(i, i+1), 1]
  mb3_e[which_in(mb3[,1], trange)] <- substr(dev_events[i,2], 1, nchar(dev_events[i,2])-6)
}




mb3 <- cb(mb3, "event"=as.factor(mb3_e))
lvls <- unique(as.char(mb3$event))

res_tab <- matrix(0, ncol = 7, nrow = len(lvls))
rownames(res_tab) <- lvls
colnames(res_tab) <- c("prop_0_conf", "mean_conf", "mean_non0_conf", 
                       "prop_outliers_ref", "prop_outliers_ppg",
                       "overall_MAD", "non0_conf_MAD")

for(lvl in lvls) {
  mb3i <- mb3[which(mb3$event == lvl), ]
  
  prop_conf0 <- sum(mb3i$cm == 0) / nrow(mb3i)
  mean_conf <- mean(mb3i$cm)
  mean_nzconf <- mean(mb3i$cm[mb3i$cm != 0]) 
  
  prop_out_ref <- sum(mb3i$outlier1 == 1) / nrow(mb3i)
  prop_out_rr <- sum(mb3i$outlier2 == 1) / nrow(mb3i)
  
  mad_score_all <- mean(abs( apply(mb3i[(mb3i$outlier1 == 0) & (mb3i$outlier2 == 0), 2:3], 1, diff) ))
  mad_score_conf <- mean(abs( apply(mb3i[(mb3i$outlier1 == 0) & (mb3i$outlier2 == 0) & (mb3i$cm > 0), 2:3], 1, diff) ))
  
  res_tab[which(lvl == rownames(res_tab)), ] <- round(c(prop_conf0, mean_conf, mean_nzconf, 
                                                        prop_out_ref, prop_out_rr,
                                                        mad_score_all, mad_score_conf), 3)
  
  # cat("Results for protocol event:", lvl, "\n_________________________________________\n")
  # cat("Proportion zero-confidence =", round(prop_conf0 ,3), "\n")
  # cat("Mean confidence =", round(mean_conf, 3), "\n")
  # cat("Mean non-zero confidence =", round(mean_nzconf, 3), "\n\n")
  # cat("Proportion of outliers (reference) =", round(prop_out_ref, 3), "\n")
  # cat("Proportion of outliers (PPG device) =", round(prop_out_rr, 3), "\n\n")
  # cat("Overall MAD score (outliers removed) =", round(mad_score_all, 3), "\n")
  # cat("Non-zero confidence MAD score (outl. rem.) =", round(mad_score_conf, 3), "\n\n\n")
  
}


write.table2(res_tab, "20171003_benchmarking.txt", col.names=T, row.names=T)









