source("../../R files/HRV/ERMADT_funs.r")



# rrmat <- cbind(m_rr, m_rrcm[,2])
# rrmat[,1] <- rrmat[,1]/1e9
# rrmat <- rrmat[-c(1:30),]
# 
# refmat <- ref_rr
# refmat[,1] <- refmat[,1]/1e9
# 
# winsize <- 60


benchmark_RR <- function(rrmat, refmat, start, end, 
                         winsize = 60, print_progress = TRUE,
                         LAG_WIDTH = 10,
                         X_BOUND_DELTA = 0.5,
                         PROP_CONF_THRESH = 0.5,
                         PLOT_STYLE = c("save", "ask")[1],
                         PLOT_SAVE_DIR = "~/HealthQ/R_scripts/DSP_RR/benchmark_output/")
{
  ## Set default configurations
  ##############################
  
  if(missing(start))
    start <- refmat[1, 1]
  
  if(missing(end))
    end <- tail(refmat[,1], 1)
  
  if(ncol(rrmat) == 2)
    rrmat <- cb(rrmat, rep(100, nrow(rrmat)))
  
  
  ## Define working variables
  ############################
  
  rr1 <- refmat[, 1:2]
  rr2 <- rrmat[, 1:3]
  
  lag_running <- 0
  lag_ave <- NA
  
  comp_df_all <- NULL
  
  wi <- 1
  
  
  ## Outlier analysis
  ####################
  rr1_outl <- detectRROutliers(rr1[,2])[,1]
  rr2_outl <- detectRROutliers(rr2[,2])[,1]
  
  
  
  ## Calculate initial lag (2 minute window)
  ###########################################
  win <- c(start, start + 120 - 1)
  w1 <- which_in(rr1[,1], win)
  xp1 <- rr1[w1, 1] ; yp1 <- rr1[w1, 2]
  
  w2 <- which_in(rr2[,1], win)
  xp2 <- rr2[w2, 1] ; yp2 <- rr2[w2, 2]
  
  lags <- round(seq(-100, 100, by=0.5), 1)
  msd_lags <- sapply(lags, function(k) {
    msd_vec <- ( yp1 - yp2[sapply(xp1 + k, function(x) which.min(abs(x - xp2)))] )
    return( median(abs(msd_vec)) )
  })
  
  lag_running <- lags[which.min(msd_lags)]
  
  init_lag <- lag_running

  
  ## Iterate over windows
  ########################
  
  if(PLOT_STYLE == "ask") {
    w() ; par(ask = T, mfrow=c(2,1), mar=c(2.5,2.5,1,1)) 
  }
  
  i <- start
  while(i <= end) {
    
    # Select relevant indices in ref signal
    win <- c(i, i + winsize - 1)
    w1 <- which_in(rr1[,1], win)
    xp1 <- rr1[w1, 1] ; yp1 <- rr1[w1, 2] ; outlp1 <- rr1_outl[w1]
    
    # Select indices and correct for running lag
    w2 <- which_in(rr2[,1] - lag_running, win)
    if(len(w2) == 0) break
    
    xp2 <- rr2[w2, 1] - lag_running ; yp2 <- rr2[w2, 2]
    outlp2 <- rr2_outl[w2] ; rrcmp <- rr2[w2, 3]
    
    
    # w() ; par(mfrow=c(2,1), mar=c(2.5,2.5,1,1))
    # lplot_g(xp1, yp1, lwd=2, col="red") ; lines(xp2, yp2, lwd=2, col="blue")
    # lplot_g(xp2, rrcmp, ylim=c(0,100), xlim=range(xp1), col="purple", lwd=2)
    
    
    
    # Determine lag relative to ref signal
    k_min_lag <- lag_ave
    if(sum(rrcmp > 0)/len(rrcmp) >= PROP_CONF_THRESH) {
      lags <- round(seq(-LAG_WIDTH, LAG_WIDTH, by=0.1), 1)
      
      msd_lags <- sapply(lags, function(k) {
        
        msd_vec <- ( yp1 - yp2[sapply(xp1 + k, function(x) which.min(abs(x - xp2)))] )
        return( median(abs(msd_vec)) )
        
      })
      
      k_min <- which(msd_lags == min(msd_lags))
      k_min_lag <- lags[k_min[ceiling(len(k_min)/2)]]
      
      
      if(is.na(lag_ave))
        lag_ave <- k_min_lag
      else if(!is.na(lag_ave))
        lag_ave <- 0.67*lag_ave + 0.33*k_min_lag
      
    }
    
    xp2 <- xp2 - k_min_lag
    lag_running <- lag_running + k_min_lag
    
    # Determine bounds for comparison
    x_bounds <- c(max(xp1[1], xp2[1]), min(tail(xp1, 1), tail(xp2, 1))) + X_BOUND_DELTA*c(-1, 1)
    
    w1 <- which_in(xp1, x_bounds)
    xp1 <- xp1[w1] ; yp1 <- yp1[w1] ; outlp1 <- outlp1[w1]
    
    w2 <- which_in(xp2, x_bounds)
    xp2 <- xp2[w2] ; yp2 <- yp2[w2] ; outlp2 <- outlp2[w2] ; rrcmp <- rrcmp[w2]
    
    
    # Debugging
    ###########################################################
    # xp1b <- xp1 ; xp2b <- xp2 ; yp1b <- yp1 ; yp2b <- yp2
    # outlp1b <- outlp1 ; outlp2b <- outlp2 ; rrcmpb <- rrcmp
    # 
    # xp1 <- xp1b ; xp2 <- xp2b ; yp1 <- yp1b ; yp2 <- yp2b
    # outlp1 <- outlp1b ; outlp2 <- outlp2b ; rrcmp <- rrcmpb
    # 
    # xp1 <- xp1[-c(10)] ; yp1 <- yp1[-c(10)] ; outlp1 <- outlp1[-c(10)]
    # xp2 <- xp2[-c(10, 15, 16)] ; yp2 <- yp2[-c(10, 15, 16)] ; outlp2 <- outlp2[-c(10, 15, 16)] ; rrcmp <- rrcmpb[-c(10, 15, 16)]
    
    
    # Find corresponding RR values (closest in time)
    index_closest <- sapply(xp1, function(x) which.min(abs(x - xp2)))
    
    if(len(index_closest) > len(unique(index_closest))) {
      
      idxs <- which(diff(index_closest) == 0)
      for(j in 1:len(idxs)) {
        ix <- index_closest[idxs[j]]
        i_min <- which.min(abs(xp1[which(index_closest == ix)] - xp2[ix]))
        index_closest[which(index_closest == ix)] <- NA
        index_closest[idxs[j] + i_min - 1] <- ix
      }
    }
    
    # Set up data frame to compare corresponding RR values
    comp_df0 <- data.frame("x"=xp1, "rr1"=yp1, "rr2_closest"=yp2[index_closest], 
                           "outlier1"=outlp1, "outlier2"=outlp2[index_closest],
                           "cm"=rrcmp[index_closest])
    
    comp_df <- comp_df0[!apply(comp_df0, 1, function(x) any(is.na(x))), ]
    comp_df <- comp_df[(comp_df$outlier1 == 0) & (comp_df$outlier2 == 0), ]
    comp_df <- comp_df[comp_df$cm > 0, ]
    
    diffs_win <- comp_df[,2] - comp_df[,3]
    
    comp_df_all <- rbind(comp_df_all, comp_df0)
    
    # Plot the window comparison as output
    if(PLOT_STYLE == "save") {
      figname <- paste0(PLOT_SAVE_DIR, "fig_win_", ifelse(wi < 10, paste0("0", wi), as.char(wi)), ".png")
      png(figname, 1200, 700, pointsize=16)
      par(mfrow=c(2,1), mar=c(2.5,2.5,1,1))
    }
    
    lplot_g(xp1, yp1, lwd=2, col="red") ; lines(xp2, yp2, lwd=2, col="blue")
    abline(v = x_bounds, lty=2)
    abline(v = xp1[outlp1 == 1], lwd=3, col="orange")
    abline(v = xp2[outlp2 == 1], lwd=1, col="yellow")
    legend("topleft", leg=paste("MAD =", round(mean(abs(diffs_win)), 2)), bg="white", cex=0.75)
    lplot_g(xp2, rrcmp, ylim=c(0,100), xlim=range(xp1), col="mediumseagreen", lwd=2)
    legend("bottomleft", leg=paste("window lag =", round(ifelse(i != start, k_min_lag, lag_running), 1)), 
           bg="white", cex=0.75)

    if(PLOT_STYLE == "save") {
      invisible(dev.off())
      wi <- wi + 1
    }
    
    
    
    # Update the iteration index
    i <- x_bounds[2]
    
    if(print_progress)
      cat(round(100*(i - start)/(end - start), 0), "%\n", sep="")

    
  }
  
  # Compile output (all and "cleaned" without outliers, NAs and zero conf)
  comp_df_all_clean <- comp_df_all[!apply(comp_df_all, 1, function(x) any(is.na(x))), ]
  comp_df_all_clean <- comp_df_all_clean[(comp_df_all_clean$outlier1 == 0) & (comp_df_all_clean$outlier1 == 0), ]
  comp_df_all_clean <- comp_df_all_clean[comp_df_all_clean$cm > 0, ]
  
  mad_score <- mean(abs(comp_df_all_clean[,2] - comp_df_all_clean[,3]))
  cat("Overall MAD =", round(mad_score, 2), "\n")
  
  return(list("all_out" = comp_df_all,
              "clean_out"= comp_df_all_clean,
              "mad_score" = mad_score,
              "initial_lag" = init_lag))
  
}

