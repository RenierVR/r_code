source("../useful_funs.r")
library(rhdf5)

# enable viewing of 64-bit format timestamps
options(digits = 22)


##  VIEW HDF5 FILE CONTENTS
############################

# path to h5 file
h5_fpath <- "/.../..."

# print group names with corresponding dimension
h5_contents <- h5ls(h5_fpath)
print( h5_contents[h5_contents$group != "/", c(1, 5)] )



## ACCESS METADATA
###################

# out_meta_rhr <- h5read(h5_fpath, "/meta_rhr", bit64conversion="double", compoundAsDataFrame=T)$table 

## ACCESS TIME SERIES DATA
###########################

# out_metric_heartrate <- h5read(h5_fpath, "/metric_heartrate", bit64conversion="double", compoundAsDataFrame=T)$table

## TIME STAMP CONVERSION 
#########################

# out_metric_heartrate$index <- out_metric_heartrate$index/10^6
# as.POSIXlt(out_metric_heartrate$index[1]/1000, origin = "1970-01-01")

