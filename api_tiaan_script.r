###----------------------------------------------------------------------------------------------------------------------------------------------------
### CONNECTING TO API ###
###----------------------------------------------------------------------------------------------------------------------------------------------------

# Packages required:
library(jsonlite)
library(httr)
library(RCurl)
library(zoo)

###----------------------------------------------------------------------------------------------------------------------------------------------------
# AUXILIARY FUNCTIONs
###----------------------------------------------------------------------------------------------------------------------------------------------------
{
  timeToSeconds_ISO8601<-function(timestamp_string)
  { # This function changes timestamp in ISO 8601 to pure seconds
    
    Y <- substring(timestamp_string, 1, 4)
    M <- substring(timestamp_string, 6, 7)
    D <- substring(timestamp_string, 9, 10)
    h <- substring(timestamp_string, 12, 13)
    m <- substring(timestamp_string, 15, 16)
    s <- substring(timestamp_string, 18, 19)
    
    return(list(Y, M, D, h, m, s))
  }
  
  
  generateDateTime_Res_sec <- function(start_time, end_time, offset, limit)
  { # Generate date-time values in seconds resolution.
    
    # replace T with space to get in ISO format
    start_time <- sub("T", " ", start_time) 
    end_time <- sub("T", " ", end_time) 
    
    #date_vec <- seq(as.POSIXlt(start_time),as.POSIXlt(end_time),"sec")
    date_vec <- seq(as.POSIXlt(start_time), by = "sec", len=limit)
    
    return(date_vec)
  }
  
  
  dailySecond<-function(start_time, end_time, offset, limit, tz = "Africa/Johannesburg")
  { 
    #date_vec <- seq(as.POSIXlt(start_time),as.POSIXlt(end_time),"sec")
    date_vec <- seq(as.POSIXlt(start_time,tz = tz), by = "sec", len =limit)
    return(date_vec)
  }
}

###----------------------------------------------------------------------------------------------------------------------------------------------------
### SECURITY, USERID, ETC.
###----------------------------------------------------------------------------------------------------------------------------------------------------

{
  # userId "949c8678-ee66-4267-a9bb-bdd281facd80"
  # Call on command line:
  # curl --user "1T0XXVTY1FFELGBOKE0K2G8GA:eQxA9Br6VCKv73OKFKjg5TQwqMYw/5rIuLf+VRfgsBY" --data "grant_type=client_credentials" https://link.healthq.co/api/oauth/token
  
  urlbase = 'https://link.healthq.co'
  secret = 'MVQwWFhWVFkxRkZFTEdCT0tFMEsyRzhHQTplUXhBOUJyNlZDS3Y3M09LRktqZzVUUXdxTVl3LzVySXVMZitWUmZnc0JZ'
}


###----------------------------------------------------------------------------------------------------------------------------------------------------
### AUTHORIZATION- GET ACCESS TOKEN ###
###----------------------------------------------------------------------------------------------------------------------------------------------------

{
  # POST
  req <- POST("https://link.healthq.co/api/oauth/token?grant_type=client_credentials",
              add_headers("Authorization" = paste("Basic", secret),
                          "Content-Type" = "application/x-www-form-urlencoded;charset=UTF-8"))
  
  res <- fromJSON(rawToChar(req$content))
  
  # Create token for API authorization
  token <- paste("Bearer", res$access_token)
}


###----------------------------------------------------------------------------------------------------------------------------------------------------
# API REQUEST - 1HZ HR
###----------------------------------------------------------------------------------------------------------------------------------------------------

# TIME FILTER 
{
  startTime_api <- "2016-02-01T00:00:01Z"
  endTime_api <-   "2016-02-01T00:00:59Z"
  TimeZone_offset <- 2*60*60 # 2 hours to get GMT+2, in seconds
  startTime <-as.POSIXlt(sub("Z","", sub("T", " ", startTime_api))) + TimeZone_offset # replace T with space to get in ISO format
  endTime <- as.POSIXlt(sub("Z","", sub("T", " ", endTime_api))) + TimeZone_offset
}

# SOME SETTINGS
{
  limit <-10000
  offset <- 0
  resolution <- "second"
}

# API CALL- HR
{
  url = paste0("https://link.healthq.co/api/userDataSets/64a4b5d6-9151-4edc-b8b0-73eda6926ce1/metrics/heartRate?startTime=",
               startTime_api, "&endTime=", endTime_api, "&resolution=", resolution, "&limit=", limit,
               "&offset=", as.integer(offset))
  call1 <- GET(url, add_headers("Authorization" = token))
  obj1 <- fromJSON(rawToChar(call1$content))
  
  #number of the consequtive 10000-sample API calls
  n <- 1 
}

# LOOP
{
  HR_1hz <- obj1$metrics
  offset <- 0
  
  # approx number of calls for progress indicator
  num_calls <- (difftime(endTime_api, startTime_api ,units="days")[[1]]*24*60*60)/limit 
  
  while(obj1$more==TRUE) {
    n <- n + 1
    offset <- offset + limit # NOTE, you must you as.integer to avoid scientific notation
    
    # Print progress %
    cat(round(n/(num_calls+1)*100,digits = 1),"% \n",sep="")
    
    url = paste0("https://link.healthq.co/api/userDataSets/64a4b5d6-9151-4edc-b8b0-73eda6926ce1/metrics/heartRate?startTime=",
                 startTime_api, "&endTime=", endTime_api, "&resolution=", resolution, "&limit=", limit,
                 "&offset=", as.integer(offset))
    
    call1   <- GET(url, add_headers("Authorization" = token))
    obj1    <- fromJSON(rawToChar(call1$content))
    HR_1hz  <- rbind(HR_1hz, obj1$metrics)
  }
}

Global <- data.frame(time = dailySecond(startTime, endTime, 0, dim(HR_1hz)[1]), HR = HR_1hz[,1])