source("R_code/useful_funs.r")
library(rhdf5)
library(readxl)


########################################
##  Read in master MMI info document  ##
########################################

dat_idmap <- read.table("~/Data/MMI_general/MMI_cohort_DBN-ANONID_mappings_updated_20180815.csv", sep=",", head=T)


#########################################################
##  Read in daily data from Francesco (baseline file)  ##
#########################################################

dat <- read.table("~/Data/MMI_general/Francesco - LifeQ daily heart data from pilot.csv", sep=",", head=T)
dat <- dat[, c(2, 6:11)]

id_match_dat <- match(dat$external_id, dat_idmap$dbn_id)
dat <- cb(dat, "guid"=as.char(dat_idmap$anon_id[id_match_dat]))

dat$Date <- unl(lapply(strsplit(unl(lapply(strsplit(as.char(dat$ts), " "), function(x) x[1])), "/"), function(y) paste(y[c(3,1,2)], collapse="-")))
dat$Date <- unl(lapply(strsplit(dat$Date, "-"), function(x) paste(sapply(x, format_ind), collapse="-")))
dat$map <- paste0(dat$guid, "_", dat$Date)


################################
##  Process HHA model output  ##
################################

dat_hha_prelim <- read.table("~/Data/MMI_general/HHA_output_feat_mmi_vld_20180814.csv", sep=",", head=T)
dat_hha_prelim <- dat_hha_prelim[, c(1:4, 79:86)]

# dat_hha <- read.table("~/Downloads/mmi_daily.csv", head=T, sep=",")
dat_hha <- read.table("~/Data/MMI_general/HHA_output_buckets_scores_20180815.csv", head=T, sep=",")
dat_hha$map <- paste0(as.char(dat_hha$AnonId), "_", as.char(dat_hha$timestamp))

## Add gender and age
dat_hha <- cb(dat_hha, dat_hha_prelim[match(as.char(dat_hha$AnonId), as.char(dat_hha_prelim$guid)), c(3,4)])

###########################################
##  Merge HHA output with baseline data  ##
###########################################


id_match <- match(dat$map, dat_hha$map)

dat_all <- cb(dat, dat_hha[id_match, ])
dat_all <- dat_all[, match(c("external_id", "Date", "gender", "age", "status", "HHS",
                             "restingHeartRate", "minHeartRate", "maxHeartRate", "averageHeartRate",
                             "bucket_hr", "bucket_hrv", "bucket_pwf", "bucket_hha", "score_hha"), names(dat_all))]

names(dat_all) <- c("External_ID", "Date", "Gender", "Age", "MMI_Status", "Momentum_HHS", "restingHeartRate", "minHeartRate",
                    "maxHeartRate", "averageHeartRate", "Tier2_HR_bucket", "Tier2_HRV_bucket", "Tier2_PWF_bucket", 
                    "HHA_bucket", "HHA_score" )


dat_all_narm <- dat_all[-which(apply(dat_all, 1, function(x) any(is.na(x)))), ]


write.table2(dat_all, "~/Data/MMI_general/LifeQ_daily-heart_data_HHAmerged_ALL_20180815.csv", col.names=T)
write.table2(dat_all_narm, "~/Data/MMI_general/LifeQ_daily-heart_data_HHAmerged_NA-REMOVED_20180815.csv", col.names=T)


# aa <- unique(as.char(dat_hha$AnonId))
# bb <- unique(as.char(dat$guid))
# 
# len(na.omit(match(bb, aa))) == 864
# 
# unique(as.char(dat_all$external_id))
# 
# nn <- num(len(aa))
# for(i in 1:len(aa)) {
#   t1 <- as.char(dat_hha$timestamp[dat_hha$AnonId == aa[i]])
#   t2 <- dat$Date[dat$guid == aa[i]]
#   
#   nn[i] <- sum(!is.na(match(t2, t1)))
# }


##################################################
##  Process PWV and AIx output from MMI cohort  ##
##################################################

## Find files
fp <- "~/Data/MMI_ppgwave_subset/"
fls_h5 <- list.files(fp)
fls_h5 <- fls_h5[unl(lapply(strsplit(fls_h5, "[.]"), function(x) x[2] == "h5"))]
fls_h5 <- paste0(fp, fls_h5)
fls_h5_ids <- unl(lapply(strsplit(unl(lapply(strsplit(fls_h5, "/"), function(x) tail(x, 1))), "[.]"), function(y) y[1]))
n <- len(fls_h5)


## ID matching
id_match_ppgw <- match(fls_h5_ids, dat_idmap$anon_id)
id_match_ppgw <- cb(fls_h5_ids, dat_idmap[id_match_ppgw,])

## Read in data
ppgw_list <- vector("list", n)
for(i in 1:n) {
  ppgw_list[[i]] <- cb(h5read2(fls_h5[i], "/metric/ppgwave/aix"),
                       "pwv" = h5read2(fls_h5[i], "/metric/ppgwave/pwv")[,-1],
                       "DBNID"=id_match_ppgw$dbn_id[i])
  
  catProgress(i, n)
}


## Flatten PPGW list
N <- sum(unl(lapply(ppgw_list, nrow)))
ppgw_df <- data.frame("External_ID"=char(N),
                      "TimeStamp"=as.POSIXct2(num(N)),
                      "pwv"=num(N),
                      "aix"=num(N),
                      stringsAsFactors=FALSE)

j <- 1
for(i in 1:n) {
  ppgw <- ppgw_list[[i]]
  idx <- j:(nrow(ppgw) + j -1)
  ppgw_df$External_ID[idx] <- as.char(ppgw$DBNID)
  ppgw_df$TimeStamp[idx] <- as.POSIXct2(ppgw$index)
  ppgw_df$pwv[idx] <- ppgw$pwv
  ppgw_df$aix[idx] <- ppgw$value
  
  j <- j + nrow(ppgw)
  catProgress(i, n)
}

## Write full DF to file
write.table2(ppgw_df, "~/Data/MMI_general/MMIcohort_ppgwave_output_20180815.csv", col.names=T)


## Create condensed version
ppgw_df$Date <- format(ppgw_df$TimeStamp, format="%Y-%m-%d")
ppgw_df$TimeStamp <- NULL

ppgw_ids <- unique(ppgw_df$External_ID)

out_df <- NULL
for(i in 1:len(ppgw_ids)) {
  ppgw <- ppgw_df[ppgw_df$External_ID == ppgw_ids[i], -1]
  ppgw_split <- split(ppgw, ppgw$Date)
  
  out <- t(as.data.frame(lapply(ppgw_split, function(x){
    c("n"=nrow(x), 
      "pwv_mean"=mean(x$pwv),
      "pwv_median"=median(x$pwv),
      "pwv_sd"=sd(x$pwv),
      "pwv_iqr"=IQR(x$pwv),
      "aix_mean"=mean(x$aix),
      "aix_median"=median(x$aix),
      "aix_sd"=sd(x$aix),
      "aix_iqr"=IQR(x$aix))
  }) ))
  
  rownames(out) <- NULL
  out <- as.data.frame(out)
  out <- cb("External_ID"=ppgw_ids[i],
            "Date"=names(ppgw_split),
            out)
  
  
  if(i == 1)
    out_df <- out
  else
    out_df <- rbind(out_df, out)
  
  catProgress(i, len(ppgw_ids))
}


write.table2(out_df, "~/Data/MMI_general/MMIcohort_ppgwave_output_daily_20180815.csv", col.names=T)
