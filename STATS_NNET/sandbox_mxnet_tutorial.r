###############################################################
#  http://mxnet.io/tutorials/r/fiveMinutesNeuralNetwork.html  #
###############################################################

# install.packages("drat", repos="https://cran.rstudio.com")
# drat:::addRepo("dmlc")
# install.packages("mxnet")

source("../useful_funs.r")

library(mlbench)
library(mxnet)


####################
#  Classification  #
####################


# load data
data(Sonar, package="mlbench")



# split data into test and train partitions
Sonar[,61] = as.numeric(Sonar[,61])-1
train_i = c(1:50, 100:150)
x_train = data.matrix(Sonar[train_i, 1:60])
y_train = Sonar[train_i, 61]
x_test = data.matrix(Sonar[-train_i, 1:60])
y_test = Sonar[-train_i, 61]



# train multiple layer perceptron
#   function 'mx.mlp' requires the following parameters:
#     - Training data and label
#     - Number of hidden nodes in each hidden layer
#     - Number of nodes in the output layer
#     - Type of the activation
#     - Type of the output loss
#     - The device to train (GPU or CPU)
#     - Other parameters for 'mx.model.FeedForward.create'

mx.set.seed(0)
mlp_out <- mx.mlp(data=x_train, label=y_train, hidden_node=10, out_node=2, out_activation="softmax",
                  num.round=20, array.batch.size=15, learning.rate=0.07, momentum=0.9,
                  eval.metric=mx.metric.accuracy)


# plot computation graph
graph.viz(model$symbol)#$as.json())

# predict test data output
preds = predict(mlp_out, x_test)

# construct confusion matrix from test predictions
y_pred = max.col(t(preds))-1
conf_mat <- table(y_pred, y_test)



################
#  Regression  #
################

# load data of Boston housing values
data(BostonHousing, package="mlbench")

# split into test and train partitions
train_i = seq(1, 506, 3)
x_train = data.matrix(BostonHousing[train_i, -14])
y_train = BostonHousing[train_i, 14]
x_test = data.matrix(BostonHousing[-train_i, -14])
y_test = BostonHousing[-train_i, 14]

# define the input data
data <- mx.symbol.Variable("data")
label <- mx.symbol.Variable("label")


# A fully connected hidden layer
# data: input source
# num_hidden: number of neurons in this hidden layer
fc1 <- mx.symbol.FullyConnected(data, num_hidden=1)

# Use linear regression for the output layer
lro <- mx.symbol.LinearRegressionOutput(fc1)
# lro <- mx.symbol.MAERegressionOutput(fc1)
# lro <- mx.symbol.MakeLoss(mx.symbol.abs(mx.symbol.Reshape(fc1, shape = 0) - label))

# Train neural net
mx.set.seed(0)
mlp_model <- mx.model.FeedForward.create(lro, X=x_train, y=y_train,
                                         ctx=mx.cpu(),     num.round=50, array.batch.size=20,
                                         optimizer="rmsprop")
                                         # learning.rate=2e-6, momentum=0.9,)

# Make prediction form test data and evaluate with MSE
preds <- predict(mlp_model, x_test)
(mse_preds <- sqrt(mean((c(preds) - y_test)^2)))


