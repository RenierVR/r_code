source("../useful_funs.r")

library(rhdf5)
library(mlbench)
library(mxnet)




# Read HDF5
# h5_fpath <- "C:/Users/reniervr/Downloads/colated_data_v4(1).h5"
h5_fpath <- "C:/Users/reniervr/Downloads/colated_data_trained_all_v3.h5"
# h5_fpath <- "C:/Users/reniervr/Downloads/colated_data.h5"
# h5_fpath <- "C:/Users/reniervr/Downloads/colated_data_first_backup_with_entropy.h5"

h5_contents <- h5ls(h5_fpath)
print( h5_contents[h5_contents$group != "/", c(1, 5)] )


# Load data into R
dat_all <- h5read(h5_fpath, "/colated_data", bit64conversion="double", compoundAsDataFrame=T)$table



#####################

# dat_LE <- read.table("C:/Users/reniervr/Downloads/franco_le_data_take2.csv", sep=",", head=T)
dat_LE <- read.table("C:/Users/reniervr/Downloads/life_expectancy_more_values.csv", sep=",", head=T)
# dat_LE <- dat_LE[-which(dat_LE$doh_id == ""),]

dat_LE <- dat_LE[match(dat_all$lifeq_id, dat_LE$lifeq_id), ]

dat <- cb(dat_all, "lifeq_Life_Expectancy"=dat_LE$lifeq_Life_Expectancy)[!is.na(dat_LE[,1]), ]



#####################


h5_fpath <- "C:/Users/reniervr/Downloads/colated_data.h5"
dat_ent <- h5read(h5_fpath, "/colated_data", bit64conversion="double", compoundAsDataFrame=T)$table

dat_ent <- dat_ent[,c(grep("lifeq_id", names(dat_ent)), grep("entropy", names(dat_ent)))]



dat <- cb(dat, dat_ent[match(dat$lifeq_id, dat_ent$lifeq_id), -1])

#####################

dat <- dat[which(dat$enough_data == 1), ]

# Select data rows of standard and substandard subjects
dat <- dat[dat$Decision_group_overall %in% c(0, 1), ]


# Remove rows which doesn't have life expectancy output
dat <- dat[which(!is.na(dat$lifeq_Life_Expectancy)),]



# Remove rows which doesn't have entropy output
dat <- dat[!apply(dat[,grep("entropy", names(dat))], 1, function(x) any(is.na(x))), ]




dat_model <- dat[,c(30, 148:ncol(dat))]

#####################

cols_na <- apply(dat_model[,-1], 2, function(z) sum(is.na(z)))

# Remove columns with significant amount of NAs
################################################
if(sum(cols_na >= 5) > 0)
  dat_model <- dat_model[, -as.numeric(which(cols_na >= 5) + 1)]

# Remove rows with NA values
##############################
rows_na <- as.numeric(which(apply(dat_model, 1, function(z) any(is.na(z)))))
if(len(rows_na) > 0)
  dat_model <- dat_model[-rows_na,]


#####################

library(minerva)


dat_mi <- apply(dat_model[,-1], 2, function(x) mine(x, dat_model$Decision_group_overall)$MIC)



dat_mi_norm <- scale(dat_mi, center=min(dat_mi), scale=F)
dat_mi_norm <- scale(dat_mi_norm, center=F, scale=max(dat_mi_norm))
names(dat_mi_norm) <- names(dat_mi)

dat_mi_norm <- sort(dat_mi_norm, decreasing=T)

vi_part <- cb(seq(1, ncol(dat_model), 30), c(seq(1, ncol(dat_model), 30)[-1]-1, ncol(dat_model)))

for(i in 1:nrow(vi_part)) {

  w(14,14)
  vi_part_seq <- vi_part[i,1]:vi_part[i,2]
  bars <- barplot(rev(dat_mi_norm[vi_part_seq]), horiz=T, col="yellow", yaxt="n", xlim=c(0,1))
  text(rev(dat_mi_norm[vi_part_seq])/2, bars, rev(names(dat_mi_norm[vi_part_seq])))
  title(main=paste0("Maximal Information Value wrt Decision_Group [", vi_part[i,1], ", ", vi_part[i,2], "]"))

  savePlot(paste0("mic_wrt_decisiongroup", i, ".png"), "png")
  # graphics.off()

}








conf_mat <- matrix(c(1613,    2629,    4096,   1515, 1053335,    1531,    167,    8044,   20074), 3, 3, byrow=T)




