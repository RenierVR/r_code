source("../useful_funs.r")

library(rhdf5)
library(mlbench)
library(mxnet)




# Read HDF5
h5_fpath <- "C:/Users/reniervr/Downloads/colated_data_v4(1).h5"
h5_fpath <- "C:/Users/reniervr/Downloads/colated_data_trained_all_v3.h5"

h5_contents <- h5ls(h5_fpath)
print( h5_contents[h5_contents$group != "/", c(1, 5)] )


# Load data into R
dat <- h5read(h5_fpath, "/colated_data", bit64conversion="double", compoundAsDataFrame=T)$table

# Exploratory analysis and data partitioning
gleam(dat[,1:150], r=1)
gleam(dat, r=1)

col_dependent <- c(29:30)
col_demo <- c(13:15, 17:27)
col_sleep_habits1 <- c(134:138)
col_enough <- c(145:146)
col_sleep_habits2 <- c(152:158)
col_feature_means <- c(159:232)
col_feature_stds <- c(233:306)
col_pwv <- c(307:319)
col_clf <- c(320:329)

dat_all <- dat[which(dat$enough_data == 1), c(col_dependent,
                                              col_demo, 
                                              col_sleep_habits2,
                                              col_feature_means,
                                              col_feature_stds,
                                              col_pwv,
                                              col_clf)]


# rm(dat)               

# Select rows to use in modelling
# col_model <- c(7:10, 17:184)
col_model <- c(17:60, 65:134, 139:184)
col_clf <- c(185:194)

# Select data rows of standard, loaded and declined subjects
# dat_all <- dat_all[dat_all$Decision_type_overall %in% c(0, 1, 3), ]

# Select data rows of standard and substandard subjects
dat_all <- dat_all[dat_all$Decision_group_overall %in% c(0, 1), ]

dat_model <- dat_all[,c(2, col_model, col_clf)]
# dat_model <- dat_all[,c(2, col_clf)]




######################################################
##### Data Handling 
######################################################


# Identify features with most NA values
########################################
cols_na <- apply(dat_model[,-1], 2, function(z) sum(is.na(z)))

# Remove columns with significant amount of NAs
################################################
if(sum(cols_na >= 5) > 0)
  dat_model <- dat_model[, -as.numeric(which(cols_na >= 5) + 1)]

# Remove rows with NA values
##############################
rows_na <- as.numeric(which(apply(dat_model, 1, function(z) any(is.na(z)))))
if(len(rows_na) > 0)
  dat_model <- dat_model[-rows_na,]

dat_model <- cb(FOLDS=rep(1, nrow(dat_model)), dat_model)




######################################################
##### MCI 
######################################################



library(minerva)

# dat_mi <- apply(dat_model[,-c(1:2)], 2, function(x) mutinformation(round(1000*x, 0), dat_model$Decision_group_overall))
dat_mi <- apply(dat_model[,-c(1:2)], 2, function(x) mine(x, dat_model$Decision_group_overall)$MIC)
dat_mi_full <- mine(dat_model[-c(1:2)]) 



# dat_mi_norm <- scale(dat_mi, center=min(dat_mi), scale=F)
# dat_mi_norm <- scale(dat_mi_norm, center=F, scale=max(dat_mi_norm))
# names(dat_mi_norm) <- names(dat_mi)
# 
# dat_mi_norm <- sort(dat_mi_norm, decreasing=T)
# 
# vi_part <- cb(seq(1, ncol(dat_model), 30), c(seq(1, ncol(dat_model), 30)[-1]-1, ncol(dat_model)))
# 
# for(i in 1:nrow(vi_part)) {
#   
#   w(14,14)
#   vi_part_seq <- vi_part[i,1]:vi_part[i,2]
#   bars <- barplot(rev(dat_mi_norm[vi_part_seq]), horiz=T, col="yellow", yaxt="n", xlim=c(0,1))
#   text(rev(dat_mi_norm[vi_part_seq])/2, bars, rev(names(dat_mi_norm[vi_part_seq])))
#   title(main=paste0("Maximal Information Value wrt Decision_Group [", vi_part[i,1], ", ", vi_part[i,2], "]"))
#   
#   savePlot(paste0("mic_wrt_decisiongroup", i, ".png"), "png")
#   graphics.off()
# 
# }





######################################################
##### PRUNE 'dat_model' FEATURES
######################################################


col_remove <- c(3, # wake_up_time_mean
                4, # wake_up_time_std
                5, # apnea
                9, # api_maxHeartRate_mean
                11, # api_restingHeartRate_mean
                12, # api_caloriesMeasured_mean *
                13, # api_caloriesEstimated_mean *
                14, # api_caloriesTotal_mean *
                15, # api_durationMeasured_mean
                16, # api_totalDeepSleep_mean
                17, # api_totalSleep_mean
                18, # api_totalLightSleep_mean
                20, # api_totalRemSleep_mean
                22, # sedentary_mean_hr_mean
                36, # sleep_no_motion_hr_amplitude_mean
                37, # sleep_no_motion_hr_median_mean
                47, # sleep_quality_mean
                53, # deep_hrv_conf_mean_mean
                72, # breathing_rate_max_mean
                73, # breathing_rate_amp_mean,
                75, # api_minHeartRate_std
                76, # api_maxHeartRate_std
                78, # api_restingHeartRate_std
                79, # api_caloriesMeasured_std
                80, # api_caloriesEstimated_std
                81, # api_caloriesTotal_std,
                83, # api_totalDeepSleep_std
                84, # api_totalSleep_std
                85, # api_totalLightSleep_std
                87, # api_totalRemSleep_std
                114, # sleep_quality_std
                141, # weekday_std
                142, # pwd_gradient
                143, # pwd_c
                144, # pwd_75
                146, # pwv_gradient
                147, # pwv_c
                148, # pwv_75
                150, # pwa_negative
                152, # aix_c
                153, # aix_75
                154) # pwf_amount


dat_model <- dat_model[, -(col_remove+2)]


######################################################
##### CROSS-VALIDATION 
######################################################

runCrossValTraining <- function(CV_K = 5,
                                CV_SEED = 0,
                                CV_STRATIFY_FOLDS = TRUE,
                                CV_TRAIN_RESAMPLE = TRUE,
                                CV_APPLY_SCALING = TRUE,
                                MLP_ACTIVATION_FUN = "tanh",
                                MLP_ARRAY_BATCH_SIZE = 30,
                                MLP_NUM_ROUND = 100,
                                MLP_EARLY_STOP_THRESH = 0.99,
                                MLP_HIDDEN_NODE = 1,
                                MLP_INITIALIZER = mx.init.uniform(1))
{

  
  # Partition data into folds
  ############################
  set.seed(CV_SEED)
  
  if(!CV_STRATIFY_FOLDS) {
    folds <- sample(rep(1:CV_K, each=ceiling(nrow(dat_model)/CV_K)))[1:nrow(dat_model)]
    dat_model$FOLDS <- folds
  }
  if(CV_STRATIFY_FOLDS) {
    nclass <- table(dat_model[,2])
    folds0 <- sample(rep(1:CV_K, each=ceiling(nclass[1]/CV_K)))[1:nclass[1]]
    folds1 <- sample(rep(1:CV_K, each=ceiling(nclass[2]/CV_K)))[1:nclass[2]]
    
    dat_model$FOLDS[dat_model[,2] == 0] <- folds0
    dat_model$FOLDS[dat_model[,2] == 1] <- folds1
  }
  
  
  # for(i in 1:5) print(table(dat_model[dat_model$FOLDS == i, 2])/nrow(dat_model[dat_model$FOLDS == i,]))
  
  
  # Iterate over CV folds
  ########################
  model_list <- vector("list", CV_K)
  res_list <- vector("list", CV_K)
  for(k in 1:CV_K) {
    
    # Split into test and train based on fold 'k'
    dat_train <- dat_model[dat_model$FOLD != k, -1]
    dat_test <- dat_model[dat_model$FOLD == k, -1]
    
    if(CV_TRAIN_RESAMPLE) {
      dat_train_new <- dat_train[sample(which(dat_train[,1] == 0), diff(table(dat_train[,1])), replace=T), ]
      dat_train <- rbind(dat_train, dat_train_new)   
    }
    
    # Ensure randomised observations
    dat_train <- dat_train[sample(1:nrow(dat_train)),]
    
    if(CV_APPLY_SCALING) {
      # Scale training data
      scale_min <- apply(dat_train[,-1], 2, min)
      dat_x_scaled1 <- as.data.frame(scale(dat_train[,-1], center=scale_min, scale=F))
      scale_max <- apply(dat_x_scaled1, 2, max)
      dat_x_scaled <- as.data.frame(scale(dat_x_scaled1, center=F, scale=scale_max))
      
      dat_train <- cb(dat_train[,1], dat_x_scaled)
      
      # Scale testing data (with training data mins and maxes)
      dat_test_scaled <- as.data.frame(scale(dat_test[,-1], center=scale_min, scale=F))
      dat_test_scaled <- as.data.frame(scale(dat_test_scaled, center=F, scale=scale_max))
      
      dat_test <- cb(dat_test[,1], dat_test_scaled)
    }
    
    dat_train <- data.matrix(dat_train)
    dat_test <- data.matrix(dat_test)
    
    
    # Define callback function for early stopping
    mx.callback.early.stop2 <- function (train.metric = NULL, verbose = FALSE) 
    {
      function(iteration, nbatch, env, verbose = verbose) {
        if (!is.null(env$metric)) {
          if (!is.null(train.metric)) {
            result <- env$metric$get(env$train.metric)
            if(result$value >= train.metric)
              return(FALSE)
          }
        }
        return(TRUE)
      }
    }
    
    # Fit model
    cat("\nTRAINING MODEL (FOLD !=", k, ")...\n")
    mlp_out <- mx.mlp(data=dat_train[,-1], label=dat_train[,1], 
                      array.layout="rowmajor", hidden_node=MLP_HIDDEN_NODE,
                      activation=MLP_ACTIVATION_FUN,
                      out_node=2, out_activation="softmax", dropout=0.5,
                      num.round=MLP_NUM_ROUND, array.batch.size=MLP_ARRAY_BATCH_SIZE,
                      optimizer="adam", initializer=MLP_INITIALIZER,
                      eval.metric=mx.metric.accuracy,
                      epoch.end.callback=mx.callback.early.stop2(MLP_EARLY_STOP_THRESH))
    
    
    # Predict test cases
    preds <- predict(mlp_out, dat_test[,-1], array.layout="rowmajor")
    
    y_pred = max.col(t(preds))-1
    conf_mat <- table(y_pred, dat_test[,1])
    
    # Store results in lists
    model_list[[k]] <- mlp_out
    res_list[[k]] <- list("CONFUSION_MATRIX"=conf_mat,
                          "COHEN_KAPPA"=calc_cohen_kappa(conf_mat))
    
  }
  
  return(list("model_list"=model_list,
              "res_list"=res_list))
}
  


# Calculate aggregated results
###############################


# cv_out <- runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 100)$res_list
# cv_kappa <- round(unlist(lapply(cv_out, function(x) x[[2]])), 3)
# 
# cat(names(cv_out_list)[i], ":\n")
# cat("OVERALL KAPPA = ", mean(cv_kappa), " (SD = ", round(sd(cv_kappa), 3), ")\n",
#     "RANGE = [", min(cv_kappa), ", ", max(cv_kappa), "]\n\n", sep="")


{
cv_out_list <- list("hidden_node=5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(5))$res_list,
                    "hidden_node=10"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(10))$res_list,
                    "hidden_node=25"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(25))$res_list,
                    "hidden_node=50"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(50))$res_list,
                    "hidden_node=75"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(75))$res_list,
                    "hidden_node=125"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(125))$res_list,
                    "hidden_node=10,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(10,5))$res_list,
                    "hidden_node=25,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(25,5))$res_list,
                    "hidden_node=50,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(50,5))$res_list,
                    "hidden_node=75,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(75,5))$res_list,
                    "hidden_node=100,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(100,5))$res_list,
                    "hidden_node=125,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(125,5))$res_list,
                    "hidden_node=25,10"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(25,10))$res_list,
                    "hidden_node=50,10"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(50,10))$res_list,
                    "hidden_node=75,10"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(75,10))$res_list,
                    "hidden_node=100,10"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(100,10))$res_list,
                    "hidden_node=125,10"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(125,10))$res_list,
                    "hidden_node=50,25"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(50,25))$res_list,
                    "hidden_node=75,25"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(75,25))$res_list,
                    "hidden_node=100,25"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(100,25))$res_list,
                    "hidden_node=125,25"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(125,25))$res_list,
                    "hidden_node=75,50"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(75,50))$res_list,
                    "hidden_node=100,50"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(100,50))$res_list,
                    "hidden_node=125,50"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(125,50))$res_list,
                    "hidden_node=100,75"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(100,75))$res_list,
                    "hidden_node=125,75"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(125,75))$res_list,
                    "hidden_node=125,100"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(125,100))$res_list,
                    "hidden_node=25,10,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(25,10,5))$res_list,
                    "hidden_node=50,10,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(50,10,5))$res_list,
                    "hidden_node=75,10,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(75,10,5))$res_list,
                    "hidden_node=100,10,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(100,10,5))$res_list,
                    "hidden_node=125,10,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(125,10,5))$res_list,
                    "hidden_node=50,25,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(50,25,5))$res_list,
                    "hidden_node=75,25,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(75,25,5))$res_list,
                    "hidden_node=100,25,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(100,25,5))$res_list,
                    "hidden_node=125,25,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(125,25,5))$res_list,
                    "hidden_node=75,50,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(75,50,5))$res_list,
                    "hidden_node=100,50,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(100,50,5))$res_list,
                    "hidden_node=125,50,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(125,50,5))$res_list,
                    "hidden_node=100,75,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(100,75,5))$res_list,
                    "hidden_node=125,75,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(125,75,5))$res_list,
                    "hidden_node=125,100,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(125,100,5))$res_list,
                    "hidden_node=50,25,10"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(50,25,10))$res_list,
                    "hidden_node=75,25,10"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(75,25,10))$res_list,
                    "hidden_node=100,25,10"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(100,25,10))$res_list,
                    "hidden_node=125,25,10"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(125,25,10))$res_list,
                    "hidden_node=75,50,10"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(75,50,10))$res_list,
                    "hidden_node=100,50,10"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(100,50,10))$res_list,
                    "hidden_node=125,50,10"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(125,50,10))$res_list,
                    "hidden_node=100,75,10"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(100,75,10))$res_list,
                    "hidden_node=125,75,10"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(125,75,10))$res_list,
                    "hidden_node=125,100,10"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(125,100,10))$res_list,
                    "hidden_node=75,50,25"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(75,50,25))$res_list,
                    "hidden_node=100,50,25"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(100,50,25))$res_list,
                    "hidden_node=125,50,25"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(125,50,25))$res_list,
                    "hidden_node=100,75,25"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(100,75,25))$res_list,
                    "hidden_node=125,75,25"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(125,75,25))$res_list,
                    "hidden_node=125,100,25"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(125,100,25))$res_list,
                    "hidden_node=100,75,50"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(100,75,50))$res_list,
                    "hidden_node=125,75,50"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(125,75,50))$res_list,
                    "hidden_node=125,100,50"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(125,100,50))$res_list,
                    "hidden_node=125,100,75"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_HIDDEN_NODE = c(125,100,75))$res_list)

}



cv_out_list <- list("hidden_node=5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_ACTIVATION_FUN = "sigmoid", MLP_HIDDEN_NODE = c(5))$res_list,
                    "hidden_node=10"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(10))$res_list,
                    "hidden_node=125"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(125))$res_list,
                    "hidden_node=25,10"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(25,10))$res_list,
                    "hidden_node=50,10"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(50,10))$res_list,
                    "hidden_node=75,10"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(75,10))$res_list,
                    "hidden_node=125,10"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(125,10))$res_list,
                    "hidden_node=125,25"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(125,25))$res_list,
                    "hidden_node=75,50"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(75,50))$res_list,
                    "hidden_node=50,10,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(50,10,5))$res_list,
                    "hidden_node=100,10,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(100,10,5))$res_list,
                    "hidden_node=125,10,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(125,10,5))$res_list,
                    "hidden_node=75,25,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(75,25,5))$res_list,
                    "hidden_node=100,25,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(100,25,5))$res_list,
                    "hidden_node=100,50,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(100,50,5))$res_list,
                    "hidden_node=100,75,5"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(100,75,5))$res_list,
                    "hidden_node=75,25,10"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(75,25,10))$res_list,
                    "hidden_node=75,50,10"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 30, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(75,50,10))$res_list)




cv_out_list <- list("batch=100,layers=75,50"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 100, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(75,50), MLP_NUM_ROUND = 300)$res_list,
                    "batch=100,layers=75,50"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 100, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(75,50), MLP_NUM_ROUND = 300)$res_list,
                    "batch=100,layers=75,50"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 100, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(75,50), MLP_NUM_ROUND = 300)$res_list,
                    "batch=100,layers=75,50"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 100, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(75,50), MLP_NUM_ROUND = 300)$res_list,
                    "batch=100,layers=75,50"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 100, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(75,50), MLP_NUM_ROUND = 300)$res_list,
                    "batch=100,layers=75,50"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 100, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(75,50), MLP_NUM_ROUND = 300)$res_list,
                    "batch=100,layers=75,50"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 100, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(75,50), MLP_NUM_ROUND = 300)$res_list,
                    "batch=100,layers=75,50"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 100, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(75,50), MLP_NUM_ROUND = 300)$res_list,
                    "batch=100,layers=75,50"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 100, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(75,50), MLP_NUM_ROUND = 300)$res_list,
                    "batch=100,layers=75,50"=runCrossValTraining(MLP_ARRAY_BATCH_SIZE = 100, MLP_ACTIVATION_FUN = "sigmoid",  MLP_HIDDEN_NODE = c(75,50), MLP_NUM_ROUND = 300)$res_list)



# Print Kappa averaged over folds
##################################

# for(i in 1:len(cv_out_list)) {
#   cv_out <- cv_out_list[[i]]
#   cv_kappa <- round(unlist(lapply(cv_out, function(x) x[[2]])), 2)
#   
#   cat(names(cv_out_list)[i], ":\n")
#   cat("OVERALL KAPPA = ", mean(cv_kappa), " (SD=", round(sd(cv_kappa), 2), 
#       "; RANGE=[", min(cv_kappa), ", ", max(cv_kappa), "])\n\n", sep="")
# }


# Plot Kappa averaged over folds
#################################


cv_kappa_mat <- matrix(unlist(lapply(cv_out_list, 
                                     function(cv_out) round(unlist(lapply(cv_out, 
                                                                          function(x) x[[2]])), 2))), 
                       ncol=5, byrow=T)

# cv_kappa_plot_labels <- paste0("(", unlist(lapply(strsplit(names(cv_out_list), "="), function(x) x[[2]])), ")")
cv_kappa_plot_labels <- names(cv_out_list)

w() ; par(mar=c(7,4,4,2)) 
plot_g(1:nrow(cv_kappa_mat), 1:nrow(cv_kappa_mat), ylim=c(-0.05, 0.5),
       type="n", ylab="Kappa", xlab="", xaxt="n", main="activation = 'sigmoid'")
axis(1, at=1:nrow(cv_kappa_mat), labels=cv_kappa_plot_labels, las=2, cex.axis=0.7)

for(i in 1:nrow(cv_kappa_mat)) {
  cv_kappa <- cv_kappa_mat[i,][is.finite(cv_kappa_mat[i,])]
  
  # points(cb(i, cv_kappa), pch=2, col="blue")
  text(cb(i, cv_kappa), labels=1:5, col="blue", cex=0.75)
  points(cb(i, mean(cv_kappa)), pch=19, col="red", cex=1.25)
}



# Exploratory feature importance
#################################

cv_out_list <- list(runCrossValTraining(CV_SEED = as.integer(runif(1, 0, 99999)), 
                                        MLP_ARRAY_BATCH_SIZE = 100, 
                                        MLP_ACTIVATION_FUN = "sigmoid",  
                                        MLP_HIDDEN_NODE = c(75,50), 
                                        MLP_NUM_ROUND = 1000,
                                        MLP_EARLY_STOP_THRESH = 0.95),
                    runCrossValTraining(CV_SEED = as.integer(runif(1, 0, 99999)), 
                                        MLP_ARRAY_BATCH_SIZE = 100, 
                                        MLP_ACTIVATION_FUN = "sigmoid",  
                                        MLP_HIDDEN_NODE = c(75,50), 
                                        MLP_NUM_ROUND = 1000,
                                        MLP_EARLY_STOP_THRESH = 0.95),
                    runCrossValTraining(CV_SEED = as.integer(runif(1, 0, 99999)), 
                                        MLP_ARRAY_BATCH_SIZE = 100, 
                                        MLP_ACTIVATION_FUN = "sigmoid",  
                                        MLP_HIDDEN_NODE = c(75,50), 
                                        MLP_NUM_ROUND = 1000,
                                        MLP_EARLY_STOP_THRESH = 0.95),
                    runCrossValTraining(CV_SEED = as.integer(runif(1, 0, 99999)), 
                                        MLP_ARRAY_BATCH_SIZE = 100, 
                                        MLP_ACTIVATION_FUN = "sigmoid",  
                                        MLP_HIDDEN_NODE = c(75,50), 
                                        MLP_NUM_ROUND = 1000,
                                        MLP_EARLY_STOP_THRESH = 0.95),
                    runCrossValTraining(CV_SEED = as.integer(runif(1, 0, 99999)), 
                                        MLP_ARRAY_BATCH_SIZE = 100, 
                                        MLP_ACTIVATION_FUN = "sigmoid",  
                                        MLP_HIDDEN_NODE = c(75,50), 
                                        MLP_NUM_ROUND = 1000,
                                        MLP_EARLY_STOP_THRESH = 0.95),
                    runCrossValTraining(CV_SEED = as.integer(runif(1, 0, 99999)), 
                                        MLP_ARRAY_BATCH_SIZE = 100, 
                                        MLP_ACTIVATION_FUN = "sigmoid",  
                                        MLP_HIDDEN_NODE = c(75,50), 
                                        MLP_NUM_ROUND = 1000,
                                        MLP_EARLY_STOP_THRESH = 0.95),
                    runCrossValTraining(CV_SEED = as.integer(runif(1, 0, 99999)), 
                                        MLP_ARRAY_BATCH_SIZE = 100, 
                                        MLP_ACTIVATION_FUN = "sigmoid",  
                                        MLP_HIDDEN_NODE = c(75,50), 
                                        MLP_NUM_ROUND = 1000,
                                        MLP_EARLY_STOP_THRESH = 0.95),
                    runCrossValTraining(CV_SEED = as.integer(runif(1, 0, 99999)), 
                                        MLP_ARRAY_BATCH_SIZE = 100, 
                                        MLP_ACTIVATION_FUN = "sigmoid",  
                                        MLP_HIDDEN_NODE = c(75,50), 
                                        MLP_NUM_ROUND = 1000,
                                        MLP_EARLY_STOP_THRESH = 0.95),
                    runCrossValTraining(CV_SEED = as.integer(runif(1, 0, 99999)), 
                                        MLP_ARRAY_BATCH_SIZE = 100, 
                                        MLP_ACTIVATION_FUN = "sigmoid",  
                                        MLP_HIDDEN_NODE = c(75,50), 
                                        MLP_NUM_ROUND = 1000,
                                        MLP_EARLY_STOP_THRESH = 0.95),
                    runCrossValTraining(CV_SEED = as.integer(runif(1, 0, 99999)), 
                                        MLP_ARRAY_BATCH_SIZE = 100, 
                                        MLP_ACTIVATION_FUN = "sigmoid",  
                                        MLP_HIDDEN_NODE = c(75,50), 
                                        MLP_NUM_ROUND = 1000,
                                        MLP_EARLY_STOP_THRESH = 0.95))



for(i in 1:len(cv_out_list)) {

  cv_out <- cv_out_list[[i]]

  aa <- unlist(lapply(cv_out$res_list, function(x) x$COHEN_KAPPA))
  aa
  mean(aa)
  sd(aa)

}

# 
# 
# for(j in 1:len(cv_out_list)) {
#   out_input_weights <- lapply(cv_out_list[[j]]$model_list, function(x) x$arg.params[[1]])
#   out_input_weights <- lapply(out_input_weights, function(x) apply(as.matrix(x), 1, function(z) mean(abs(z))))
#   
#   
#   cols <- rainbow(164)
#   w() ; plot_g(cb(1:164, 0), ylim=range(unlist(out_input_weights)), type="n", main=paste0("cv_out_list ", j))
#   
#   points(rowMeans(matrix(unlist(out_input_weights), ncol=5, byrow=T)), pch=19, col=cols)
#   # for(i in 1:len(out_input_weights)) {
#   #   points(out_input_weights[[i]], pch=19, col= cols)
#   # }
# }




out_input_weights_list <- lapply(cv_out_list, function(z) lapply(z$model_list, function(x) x$arg.params[[1]]))
var_imp_list <- lapply(unlist(out_input_weights_list), function(x) apply(as.matrix(x), 1, function(z) mean(abs(z))))
var_imp_mat <- matrix(unlist(var_imp_list), nrow=len(var_imp_list), byrow=T)
var_imp <- colMeans(var_imp_mat)
var_names <- names(dat_model[,-c(1:2)])

var_imp_cat <- rep(1, len(var_imp))
var_imp_cat[var_imp >= 0.56] <- 3
var_imp_cat[(var_imp < 0.56) & (var_imp >= 0.53)] <- 2
var_imp_cat[var_imp < 0.508] <- 0

View(data.frame(name=var_names, importance=var_imp_cat, ord=order(order(var_imp, decreasing=T))))




