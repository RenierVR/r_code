source("../useful_funs.r")

library(rhdf5)
library(mlbench)
library(mxnet)




# Read HDF5
# h5_fpath <- "C:/Users/reniervr/Downloads/colated_data_v4(1).h5"
h5_fpath <- "C:/Users/reniervr/Downloads/colated_data_trained_all_v3.h5"
# h5_fpath <- "C:/Users/reniervr/Downloads/colated_data.h5"
# h5_fpath <- "C:/Users/reniervr/Downloads/colated_data_first_backup_with_entropy.h5"

h5_contents <- h5ls(h5_fpath)
print( h5_contents[h5_contents$group != "/", c(1, 5)] )


# Load data into R
dat_all <- h5read(h5_fpath, "/colated_data", bit64conversion="double", compoundAsDataFrame=T)$table



#####################

# dat_LE <- read.table("C:/Users/reniervr/Downloads/franco_le_data_take2.csv", sep=",", head=T)
dat_LE <- read.table("C:/Users/reniervr/Downloads/life_expectancy_more_values.csv", sep=",", head=T)
# dat_LE <- dat_LE[-which(dat_LE$doh_id == ""),]

dat_LE <- dat_LE[match(dat_all$lifeq_id, dat_LE$lifeq_id), ]



dat <- cb(dat_all, 
          "lifeq_Life_Expectancy"=dat_LE$lifeq_Life_Expectancy,
          "LE_bmi"=dat_LE$BMI,
          "LE_rhr"=dat_LE$RHR,
          "LE_age"=dat_LE$Age,
          "LE_vo2max"=dat_LE$VO2max)[!is.na(dat_LE[,1]), ]


dat <- dat[which(dat$enough_data == 1), ]

# Select data rows of standard and substandard subjects
dat <- dat[dat$Decision_group_overall %in% c(0, 1), ]
# dat <- dat[dat$Decision_type_overall %in% c(0, 1, 3), ]


# Remove rows which doesn't have life expectancy output
dat <- dat[which(!is.na(dat$lifeq_Life_Expectancy)),]



#####################


cols_na <- apply(dat[,-1], 2, function(z) sum(is.na(z)))

# Remove columns with significant amount of NAs
################################################
if(sum(cols_na >= 5) > 0)
  dat <- dat[, -as.numeric(which(cols_na >= 5) + 1)]

# Remove rows with NA values
##############################
rows_na <- as.numeric(which(apply(dat, 1, function(z) any(is.na(z)))))
if(len(rows_na) > 0)
  dat <- dat[-rows_na,]


####################


# dat_le <- cb(dat$Decision_type_overall,
#              dat$lifeq_Life_Expectancy,
#              dat$LE_bmi,
#              dat$LE_rhr,
#              dat$LE_age,
#              dat$LE_vo2max)
# 
# dat_le[dat_le[,1] == 3,1] <- 2
# 
# 
# 
# w(12,10) ; plot_g(dat_le[,c(3,2)], col=c("blue", "orange", "red")[(dat_le[,1]+1)],
#              xlab="BMI", ylab="LifeQ Life Expectancy")
# legend("topright", col=c("blue", "orange", "red"), pch=19,
#        leg=c("standard", "loaded", "declined"), bg="white")
# 
# 
# w() ; plot_g(dat_le[,c(2,4)], col=c("blue", "orange", "red")[(dat_le[,1]+1)])
# w() ; plot_g(cb(dat_le[,2], dat_le[,2]-dat_le[,5]), col=c("blue", "orange", "red")[(dat_le[,1]+1)])
# w() ; plot_g(dat_le[,c(2,6)], col=c("blue", "orange", "red")[(dat_le[,1]+1)])



####################


dat_use <- data.frame("UW"=dat$Decision_group_overall,
                      "LE"=dat$lifeq_Life_Expectancy,
                      "BMI"=dat$LE_bmi)

library(randomForest)
library(ROCR)




runRandomForest <- function(dat,
                            CV_K = 5,
                            CV_SEED = 0,
                            CV_STRATIFY_FOLDS = TRUE,
                            CV_TRAIN_RESAMPLE = TRUE,
                            RF_model_eqn = as.factor(UW) ~.,
                            ...)
{
  # Partition data into folds
  ############################
  set.seed(CV_SEED)
  
  if(!CV_STRATIFY_FOLDS) {
    folds <- sample(rep(1:CV_K, each=ceiling(nrow(dat)/CV_K)))[1:nrow(dat)]
    dat$FOLDS <- folds
  }
  if(CV_STRATIFY_FOLDS) {
    nclass <- table(dat[,1])
    folds0 <- sample(rep(1:CV_K, each=ceiling(nclass[1]/CV_K)))[1:nclass[1]]
    folds1 <- sample(rep(1:CV_K, each=ceiling(nclass[2]/CV_K)))[1:nclass[2]]
    
    dat$FOLDS[dat[,1] == 0] <- folds0
    dat$FOLDS[dat[,1] == 1] <- folds1
  }
  
  
  # for(i in 1:5) print(table(dat[dat$FOLDS == i, 2])/nrow(dat[dat$FOLDS == i,]))
  
  
  # Iterate over CV folds
  ########################
  model_list <- vector("list", CV_K)
  res_list <- vector("list", CV_K)
  for(k in 1:CV_K) {
    
    # Split into test and train based on fold 'k'
    i_f <- grep("FOLDS", names(dat))
    dat_train <- dat[dat$FOLD != k, -i_f]
    dat_test <- dat[dat$FOLD == k, -i_f]
    
    
    if(CV_TRAIN_RESAMPLE) {
      dat_train_new <- dat_train[sample(which(dat_train[,1] == 0), diff(table(dat_train[,1])), replace=T), ]
      dat_train <- rbind(dat_train, dat_train_new)   
    }
    
    # Ensure randomised observations
    dat_train <- dat_train[sample(1:nrow(dat_train)),]
    
    rf_out <- randomForest(RF_model_eqn, data=dat_train, 
                           importance=T, proximity=T, ...)
    
    # kappa <- calc_cohen_kappa(rf_out$confusion[,-3])
    
    # Performance
    preds <- predict(rf_out, dat_test[, -1, drop=F])
    pred_probs <-predict(rf_out, dat_test[, -1, drop=F], type="prob")
    y_pred <- as.numeric(preds)-1
    conf_mat <- table(y_pred, dat_test[,1])
    
    auc <- performance(prediction(pred_probs[,2], as.factor(dat_test[,1])), measure="auc")@y.values[[1]]
     
    
    model_list[[k]] <- rf_out
    res_list[[k]] <- list("CONFUSION_MATRIX"=conf_mat,
                          "PREDICTION_PROBS"=cb("pred"=pred_probs[,2], "true"=dat_test[,1]),
                          "COHEN_KAPPA"=calc_cohen_kappa(conf_mat),
                          "AUC"=auc)
    
    
    
    
    # catProgress(k, CV_K)
    
  }
  
  return(list("model_list"=model_list,
              "res_list"=res_list))
    

}




############################################################

cv_out <- runRandomForest(dat_use, mtry=2,
                          ntree=500, nodesize=250)

kappa <- unlist(lapply(cv_out$res_list, function(x) x$COHEN_KAPPA))
cat("LE:  mean = ", round(mean(kappa), 2), " | SD = ", round(sd(kappa), 2), 
    " | range = [", round(min(kappa),2), ", ", round(max(kappa),2), "]\n", sep="")


pred_probs_list <- lapply(cv_out$res_list, function(x) x$PREDICTION_PROBS)
roc_pred <- prediction(matrix(unlist(lapply(pred_probs_list, function(x) x[,1])), ncol=5),
                       matrix(unlist(lapply(pred_probs_list, function(x) x[,2])), ncol=5))
roc_plot <- performance(roc_pred, "tpr", "fpr")

w(12, 10)
plot(roc_plot, lty=3)
plot(roc_plot, avg="vertical", add=T, lwd=2, col="red")
plot(roc_plot, avg="horizontal", add=T, lwd=2, col="blue")
plot(roc_plot, avg="threshold", add=T, lwd=2, col="mediumseagreen")






####################

cv_out_all <- runRandomForest(dat_use, CV_SEED = 1234)
cv_out_bmi <- runRandomForest(dat_use[,-grep("LE", names(dat_use))], CV_SEED = 5678)
cv_out_le <- runRandomForest(dat_use[,-grep("BMI", names(dat_use))], CV_SEED = 9090)


kappa_all <- unlist(lapply(cv_out_all$res_list, function(x) x$COHEN_KAPPA))
kappa_bmi <- unlist(lapply(cv_out_bmi$res_list, function(x) x$COHEN_KAPPA))
kappa_le <- unlist(lapply(cv_out_le$res_list, function(x) x$COHEN_KAPPA))

cat("ALL: mean = ", round(mean(kappa_all), 2), " | SD = ", round(sd(kappa_all), 2), "\n",
    "BMI: mean = ", round(mean(kappa_bmi), 2), " | SD = ", round(sd(kappa_bmi), 2), "\n",
    "LE:  mean = ", round(mean(kappa_le), 2), " | SD = ", round(sd(kappa_le), 2), "\n", sep="")





################################################################################




cv_out_all <- vector("list", 100)
cv_out_bmi <- vector("list", 100)
cv_out_le <- vector("list", 100)
for(i in 1:100) {
  
  cv_seed <- round(runif(1, 0, 99999), 0)
  cv_out_all[[i]] <- runRandomForest(dat_use, CV_SEED = cv_seed, ntree=500, nodesize=250)$res_list
  cv_out_bmi[[i]] <- runRandomForest(dat_use[,-grep("LE", names(dat_use))], CV_SEED = cv_seed, ntree=500, nodesize=250)$res_list
  cv_out_le[[i]] <- runRandomForest(dat_use[,-grep("BMI", names(dat_use))], CV_SEED = cv_seed, ntree=500, nodesize=250)$res_list
  
  
  catProgress(i, 100)
}






kappa_all <- unlist(lapply(cv_out_all, function(y) unlist(lapply(y, function(x) x$COHEN_KAPPA))))
kappa_bmi <- unlist(lapply(cv_out_bmi, function(y) unlist(lapply(y, function(x) x$COHEN_KAPPA))))
kappa_le <- unlist(lapply(cv_out_le, function(y) unlist(lapply(y, function(x) x$COHEN_KAPPA))))


cat("ALL: mean = ", round(mean(kappa_all), 2), " | SD = ", round(sd(kappa_all), 2), "\n",
    "BMI: mean = ", round(mean(kappa_bmi), 2), " | SD = ", round(sd(kappa_bmi), 2), "\n",
    "LE:  mean = ", round(mean(kappa_le), 2), " | SD = ", round(sd(kappa_le), 2), "\n", sep="")



