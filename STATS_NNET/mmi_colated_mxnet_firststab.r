source("../useful_funs.r")

library(rhdf5)
library(mlbench)
library(mxnet)




# Read HDF5
h5_fpath <- "C:/Users/reniervr/Downloads/colated_data_v4(1).h5"
h5_fpath <- "C:/Users/reniervr/Downloads/colated_data_trained_all_v3.h5"

h5_contents <- h5ls(h5_fpath)
print( h5_contents[h5_contents$group != "/", c(1, 5)] )


# Load data into R
dat <- h5read(h5_fpath, "/colated_data", bit64conversion="double", compoundAsDataFrame=T)$table

# Exploratory analysis and data partitioning
gleam(dat[,1:150], r=1)
gleam(dat, r=1)

col_dependent <- c(29:30)
col_demo <- c(13:15, 17:27)
col_sleep_habits1 <- c(134:138)
col_enough <- c(145:146)
col_sleep_habits2 <- c(152:158)
col_feature_means <- c(159:232)
col_feature_stds <- c(233:306)
col_pwv <- c(307:319)
col_clf <- c(320:329)

dat_all <- dat[which(dat[,145] == 1), c(col_dependent,
                                        col_demo, 
                                        col_sleep_habits2,
                                        col_feature_means,
                                        col_feature_stds,
                                        col_pwv,
                                        col_clf)]


# rm(dat)               
               
# Select rows to use in modelling
col_model <- c(7:10, 17:184)
col_clf <- c(185:194)

# Select data rows of standard, loaded and declined subjects
# dat_all <- dat_all[dat_all$Decision_type_overall %in% c(0, 1, 3), ]

# Select data rows of standard and substandard subjects
dat_all <- dat_all[dat_all$Decision_group_overall %in% c(0, 1), ]

# dat_model <- dat_all[,c(2, col_model)]
dat_model <- dat_all[,c(2, col_clf)]



######################################################
##### Data Handling 
######################################################


# Identify features with most NA values
########################################
cols_na <- apply(dat_model[,-1], 2, function(z) sum(is.na(z)))

# Remove columns with significant amount of NAs
################################################
if(sum(cols_na >= 5) > 0)
  dat_model <- dat_model[, -as.numeric(which(cols_na >= 5) + 1)]

# Remove rows with NA values
##############################
rows_na <- as.numeric(which(apply(dat_model, 1, function(z) any(is.na(z)))))
if(len(rows_na) > 0)
  dat_model <- dat_model[-rows_na,]


# Create under-represented class by resampling with replacement
################################################################
# dat_new <- dat_model[sample(which(dat_model[,1] == 0), diff(table(dat_model[,1])), replace=T), ]
# dat_model <- rbind(dat_model, dat_new)



# Final data partition into dependent variable and features and testing/training
#################################################################################

n_train_groups <- round(table(dat_model[,1])/nrow(dat_model) * round(0.75*nrow(dat_model), 0), 0)

set.seed(0)
i_train <- sample(c(sample(which(dat_model[,1] == 0), n_train_groups[1]), 
                    sample(which(dat_model[,1] == 1), n_train_groups[2])))

dat_train <- dat_model[i_train,]
dat_test <- dat_model[-i_train,]

# rm(dat_model)


# Create under-represented class by resampling with replacement
################################################################
dat_train_new <- dat_train[sample(which(dat_train[,1] == 0), diff(table(dat_train[,1])), replace=T), ]
dat_train <- rbind(dat_train, dat_train_new)


# Data scaling to 0-1
################################################################
APPLY_SCALING <- TRUE

if(APPLY_SCALING) {
  # Scale training data
  scale_min <- apply(dat_train[,-1], 2, min)
  dat_x_scaled1 <- as.data.frame(scale(dat_train[,-1], center=scale_min, scale=F))
  scale_max <- apply(dat_x_scaled1, 2, max)
  dat_x_scaled <- as.data.frame(scale(dat_x_scaled1, center=F, scale=scale_max))
  
  dat_train <- cb(dat_train[,1], dat_x_scaled)
  
  # Scale testing data (with training data mins and maxes)
  dat_test_scaled <- as.data.frame(scale(dat_test[,-1], center=scale_min, scale=F))
  dat_test_scaled <- as.data.frame(scale(dat_test_scaled, center=F, scale=scale_max))
  
  dat_test <- cb(dat_test[,1], dat_test_scaled)
}


# aa <- t(apply(dat_x_scaled, 1, function(x) x*scale_max))
# bb <- t(apply(aa, 1, function(x) x+scale_min))


# Reformat data for NN training
################################
dat_train <- data.matrix(dat_train)
dat_test <- data.matrix(dat_test)


# Neural net
#############
mlp_out <- mx.mlp(data=dat_train[,-1], label=dat_train[,1], 
                  array.layout="rowmajor", hidden_node=c(8:4), 
                  out_node=2, out_activation="softmax",
                  num.round=25, array.batch.size=10,
                  optimizer="adam", initializer=mx.init.normal(1),# mx.init.uniform(1),
                  eval.metric=mx.metric.accuracy)


# Predict test data output
###########################
(preds <- predict(mlp_out, dat_test[,-1]))

y_pred = max.col(t(preds))-1
(conf_mat <- table(y_pred, dat_test[,1]))
calc_cohen_kappa(conf_mat)


