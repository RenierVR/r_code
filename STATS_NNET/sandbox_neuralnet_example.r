# https://datascienceplus.com/fitting-neural-network-in-r/

source("../useful_funs.r")

library(neuralnet)
library(MASS)


set.seed(500)

# use data of Boston housing values
# in order to predict mean value of homes ('medv')
dat <- Boston

# check if missing data present
any(apply(apply(dat, 2, is.na), 2, any))

# split into test and train
idx_train <- sample(1:nrow(dat), round(0.75*nrow(dat)))
dat_train <- dat[idx_train, ]
dat_test <- dat[-idx_train, ]

# fit linear model for comparison
lm_out <- glm(medv ~ ., data=dat_train)
pr_lm <- predict(lm_out, dat_test)
mse_lm <- mean((pr_lm - dat_test$medv)^2)

# scale data to [0, 1] range
scale_min <- apply(dat, 2, min)
dat_scaled <- as.data.frame(scale(dat, center=scale_min, scale=F))
scale_max <- apply(dat_scaled, 2, max)
dat_scaled <- as.data.frame(scale(dat_scaled, center=F, scale=scale_max))

dat_train_sc <- dat_scaled[idx_train, ]
dat_test_sc <- dat_scaled[-idx_train, ]

# fit the neural net ('linear.output=T' specifies regression)
nms <- names(dat_train_sc)
net_out <- neuralnet(as.formula(paste("medv ~", paste(nms[!nms %in% "medv"], collapse = " + "))),
                     act.fct="logistic", err.fct="sse",
                     data=dat_train_sc, hidden=c(5,3), linear.output=T)

w() ; plot(net_out)


# predict test data
pr_net_test <- compute(net_out, dat_test[,-14])
pr_net2_test <- compute(net_out, dat_test_sc[,-14])

pr_net2_tes
# Test the preictions





cran <- getOption("repos")
cran["dmlc"] <- "https://s3-us-west-2.amazonaws.com/apache-mxnet/R/CRAN/"
options(repos = cran)
install.packages("mxnet")#,dependencies = T)
library(mxnet)
