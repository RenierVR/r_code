# Introduction to SMOTE smart oversampling of under-represented class in unbalanced data
# https://amunategui.github.io/smote/
#########################################################################################

source("../useful_funs.r")


hyper <-read.csv('http://archive.ics.uci.edu/ml/machine-learning-databases/thyroid-disease/hypothyroid.data', header=F)
names <- read.csv('http://archive.ics.uci.edu/ml/machine-learning-databases/thyroid-disease/hypothyroid.names', header=F, sep='\t')[[1]]
names <- gsub(pattern =":|[.]",x = names, replacement="")
colnames(hyper) <- names


colnames(hyper) <-c("target", "age", "sex", "on_thyroxine", "query_on_thyroxine",
                    "on_antithyroid_medication", "thyroid_surgery", "query_hypothyroid",
                    "query_hyperthyroid", "pregnant", "sick", "tumor", "lithium",
                    "goitre", "TSH_measured", "TSH", "T3_measured", "T3", "TT4_measured",
                    "TT4", "T4U_measured", "T4U", "FTI_measured", "FTI", "TBG_measured",
                    "TBG")

hyper$target <- ifelse(hyper$target=='negative',0,1)

ind <- sapply(hyper, is.factor)
hyper[ind] <- lapply(hyper[ind], as.character)

hyper[ hyper == "?" ] = NA
hyper[ hyper == "f" ] = 0
hyper[ hyper == "t" ] = 1
hyper[ hyper == "n" ] = 0
hyper[ hyper == "y" ] = 1
hyper[ hyper == "M" ] = 0
hyper[ hyper == "F" ] = 1

hyper[ind] <- lapply(hyper[ind], as.numeric)

repalceNAsWithMean <- function(x) {replace(x, is.na(x), mean(x[!is.na(x)]))}
hyper <- repalceNAsWithMean(hyper)


train_i <- sample(1:nrow(hyper), round(0.7*nrow(hyper), 0))

trainsplit <- hyper[train_i, ] 
trainsplit$target <- as.factor(trainsplit$target)



dat1 <- data.frame(target=as.factor(rep(c(0, 1, 2), c(500, 40, 10))),
                   var1=round(rnorm(550, 10, 5), 3),
                   var2=round(rnorm(550, 100, 10), 3),
                   var3=round(rnorm(550, 1000, 50), 3))



dat2 <- SMOTE(target ~ ., data=dat2, perc.over = 100, perc.under=200)


table(dat1$target)
table(dat2$target)


i_match <- numeric(nrow(dat2))
for(i in 1:nrow(dat2)) {
  
  tmprow <- dat2[i,]
  
  i_m <- which(apply(dat1, 1, function(x) 
    (as.numeric(x[2]) == as.numeric(tmprow[2])) & (as.numeric(x[3]) == as.numeric(tmprow[3])) & (as.numeric(x[3]) == as.numeric(tmprow[3]))))
  
  if(len(i_m) > 0) i_match[i] <- i_m
  
  catProgress(i, nrow(dat2))
}


balance_data_with_SMOTE <- function(dat, i_target = 1, n_bal)
{ ## Balance a data set by artificially creating new observations
  ##   for under-represented classes using SMOTE.
  ## 'i_target' is the column index of the factor variable.
  ## 'n_bal' is the amount of observations to aim for in each class
  ##   (this is taken to be n of observations in majority class by default).
  
  require(DMwR)
  
  # Assert that input is a data.frame
  if(!is.data.frame(dat)) stop("Input needs to be a data frame.")
  
  # Change name of target for ease of use
  name_target <- names(dat)[i_target]
  names(dat)[i_target] <- "target"
  
  # Correctly format target variable
  target <- as.factor(dat$target)
  tab <- table(target)
  m <- len(tab)
  
  m_major <- which.max(tab) 
  
  # Set up new n for each class
  if(missing(n_bal)) n_bal <- as.numeric(rep(tab[m_major], m))
  else if(len(n_bal) == 1) n_bal <- rep(n_bal, m)
  
  if(any(n_bal > tab[m_major])) stop("Majority class cannot be oversampled.")
  
  
  # Select desired amount of observations from majority class
  df_out <- dat[which(dat$target == levels(target)[m_major])[1:n_bal[m_major]],]
  
  
  # Loop through each under-rep class and generate new observations
  for(i in (1:m)[-m_major]) {
    
    dat_tmp <- dat[(dat[, i_target] == levels(target)[m_major]) | (dat[, i_target] == levels(target)[i]), ]
    dat_tmp$target <- as.factor(as.character(dat_tmp$target))
    perc_aim <- as.numeric(round(100*(n_bal[i]-tab[i])/tab[i], -2))
    dat_tmp <- SMOTE(target ~ ., data=dat_tmp, perc.over=perc_aim, perc.under=100)
    
    
    df_out <- rbind(df_out, dat_tmp[which(dat_tmp$target == levels(target)[i])[1:n_bal[i]], ])
    
  }
  
  names(df_out)[i_target] <- name_target
  
  return(df_out)
  
}


names(dat1)[1] <- "depvar"
dat2 <- balance_data_with_SMOTE(dat1)


dat2[1001:1010, ] %=% dat1[541:550,]








