source("../useful_funs.r")
source("../useful_funs_ggplot.r")
library(rhdf5)
library(diptest)

# enable viewing of 64-bit format timestamps
# options(digits = 22)

## TIME STAMP CONVERSION 
#########################

# out_metric_heartrate$index <- out_metric_heartrate$index/10^6
# as.POSIXlt(out_metric_heartrate$index[1]/1000, origin = "1970-01-01")

##  view HDF5 file contents
############################

h5_fpath = "~/Data/HRV/concat_daily_hrv_with_lnrmssd.h5"
h5_contents <- h5ls(h5_fpath)
print( h5_contents[h5_contents$group != "/", c(1, 5)] )


## Extract and rework data
###########################

out_hrv <- h5read2(h5_fpath, "/daily_hrv_features")

# Only keep IDs with more than 21 days of data
days_per_id <- tapply(out_hrv$index, out_hrv$anon_id, function(x) len(unique(x)))
out_hrv <- out_hrv[-unl(lapply(as.list(names(which(days_per_id < 25))), function(x) grep(x, out_hrv$anon_id))), ]

# Variable for age grouping
out_hrv$AgeBand <- cut(out_hrv$meta_age, c(seq(20, 60, 10), 80), include.lowest=T)

# Standardise gender levels
out_hrv$meta_gender[out_hrv$meta_gender == "F"] <- "female"
out_hrv$meta_gender[out_hrv$meta_gender == "M"] <- "male"

# Ignore 60-80 age group
out_hrv <- out_hrv[out_hrv$AgeBand != "(60,80]",]
out_hrv$AgeBand <- as.factor(as.char(out_hrv$AgeBand))

# Add weekday indicator wearable
out_hrv$wdays <- rep("week", nrow(out_hrv))
out_hrv$wdays[weekdays(as.POSIXct(out_hrv$index, origin = "1970-01-01")) %in% c("Saturday", "Sunday")] <- "weekend"
 
outlist_hrv <- lapply(as.list(unique(out_hrv$anon_id)), function(x) out_hrv[which(!is.na(match(out_hrv$anon_id, x))), ])
names(outlist_hrv) <- unl(lapply(outlist_hrv, function(x) unique(x$anon_id)))

out2 <- cb(out_hrv[match(unique(out_hrv$anon_id), out_hrv$anon_id), ],
           "xx" = unl(lapply(outlist_hrv, function(x) mean(x$ln_rmssd))))

out3 <- cb(out_hrv, "xx" = unl(lapply(outlist_hrv, function(x) movingAve(x$ln_rmssd, 7, 2))) )
out3 <- na.omit(out3)

out4 <- cb(out_hrv, "xx" = unl(lapply(outlist_hrv, function(x) movingAve(x$ln_rmssd, 25, 2))) )
out4 <- na.omit(out4)


out5 <- lapply(outlist_hrv, function(x) {
  i_full <- match(seq(x$index[1], tail(x$index, 1), 86400), x$index)
  return( cb(x[i_full, ], "xx"=movingAve(x$ln_rmssd[i_full], 7, 2, 0.75)) ) })
out5 <- na.omit(df_from_list(out5))

out6 <- lapply(outlist_hrv, function(x) {
  i_full <- match(seq(x$index[1], tail(x$index, 1), 86400), x$index)
  return( cb(x[i_full, ], "xx"=movingAve(x$ln_rmssd[i_full], 25, 2, 0.75)) ) })
out6 <- na.omit(df_from_list(out6))



out7 <- lapply(outlist_hrv, function(x) {
  i_full <- match(seq(x$index[1], tail(x$index, 1), 86400), x$index)
  return( cb(x[i_full, ], 
             "xx_ma7" = movingAve(x$ln_rmssd[i_full], 7, 2, 0.5),
             "xx_ma25" = movingAve(x$ln_rmssd[i_full], 25, 2, 0.5)) ) })



## Plot densities across bands
###################################

# All daily values
gg <- ggplot(out_hrv, aes(x=ln_rmssd, fill=AgeBand)) + facet_wrap(~meta_gender)
gg <- gg + geom_density(alpha=0.4, bw=0.06)
gg <- gg + coord_cartesian(xlim = c(2, 5))
w(14, 7) ; gg

# One value per ID
gg <- ggplot(out2, aes(x=xx, fill=AgeBand)) + facet_wrap(~meta_gender)
gg <- gg + geom_density(alpha=0.4, bw=0.1)
gg <- gg + coord_cartesian(xlim = c(2, 5))
w(14, 7) ; gg

# Monthly MA values
gg <- ggplot(out4, aes(x=xx, fill=AgeBand)) + facet_wrap(~meta_gender)
gg <- gg + geom_density(alpha=0.4, bw=0.05)
gg <- gg + coord_cartesian(xlim = c(2, 5))
w(14, 7) ; gg

# Weekly MA values
gg <- ggplot(out3, aes(x=xx, fill=AgeBand)) + facet_wrap(~meta_gender)
gg <- gg + geom_density(alpha=0.4, bw=0.05)
gg <- gg + coord_cartesian(xlim = c(2, 5))
w(14, 7) ; gg

# Weekly MA values - weekdays
gg <- ggplot(out3[out3$wdays == "week",], aes(x=xx, fill=AgeBand)) + facet_wrap(~meta_gender)
gg <- gg + geom_density(alpha=0.4)#, bw=0.025)
gg <- gg + coord_cartesian(xlim = c(2, 5))
w(14, 7) ; gg

# Weekly MA values - weekdays
gg <- ggplot(out3[out3$wdays == "weekend",], aes(x=xx, fill=AgeBand)) + facet_wrap(~meta_gender)
gg <- gg + geom_density(alpha=0.4)#, bw=0.025)
gg <- gg + coord_cartesian(xlim = c(2, 5))
w(14, 7) ; gg

# Weekly MA values - weekdays
gg <- ggplot(out4[out4$wdays == "week",], aes(x=xx, fill=AgeBand)) + facet_wrap(~meta_gender)
gg <- gg + geom_density(alpha=0.4)#, bw=0.025)
gg <- gg + coord_cartesian(xlim = c(2, 5))
w(14, 7) ; gg

# Weekly MA values - weekdays
gg <- ggplot(out4[out4$wdays == "weekend",], aes(x=xx, fill=AgeBand)) + facet_wrap(~meta_gender)
gg <- gg + geom_density(alpha=0.4)#, bw=0.025)
gg <- gg + coord_cartesian(xlim = c(2, 5))
w(14, 7) ; gg


# Weekly MA values (intelligent smoothing)
gg <- ggplot(out5, aes(x=xx, fill=AgeBand)) + facet_wrap(~meta_gender)
gg <- gg + geom_density(alpha=0.4)#, bw=0.025)
gg <- gg + coord_cartesian(xlim = c(2, 5))
w(14, 7) ; gg


# Monthly MA values (intelligent smoothing)
gg <- ggplot(out6, aes(x=xx, fill=AgeBand)) + facet_wrap(~meta_gender)
gg <- gg + geom_density(alpha=0.4)#, bw=0.025)
gg <- gg + coord_cartesian(xlim = c(2, 5))
w(14, 7) ; gg




## Plot densities across individuals
###################################

for(i in 1:len(outlist_hrv)) {
  
  dip_res <- dip.test(outlist_hrv[[i]]$ln_rmssd)
  fcol <- ifelse(dip_res$p.value <= 0.05, "red", "mediumseagreen")
  
  gg <- ggplot(outlist_hrv[[i]], aes(x=ln_rmssd))
  gg <- gg + geom_density(fill=fcol) + coord_cartesian(xlim = c(2, 5))
  gg <- gg + geom_label(label=paste("dip test p-val =", round(dip_res$p.value, 2)), x=-Inf, y=Inf, hjust=0, vjust=1)
  # w() ; gg
  
  png(paste0("output/fig/", format_ind(i, 3), ".png"), width=800, height=600)
  print(gg) ; invisible(dev.off())
  
  catProgress(i, len(outlist_hrv))
}



## Investigate multimodality
#############################

dip_res <- numeric(len(outlist_hrv))
for(i in 1:len(outlist_hrv))
  dip_res[i] <- dip.test(outlist_hrv[[i]]$ln_rmssd)$p.value


for(i in which(dip_res < 0.5)) {
  
  gg <- ggplot(outlist_hrv[[i]], aes(x=ln_rmssd))
  gg <- gg + geom_density(fill="mediumseagreen") + coord_cartesian(xlim = c(2, 5))
  # gg <- gg + geom_label(label=paste("dip test p-val =", round(dip_res$p.value, 2)), x=-Inf, y=Inf, hjust=0, vjust=1)
  # w() ; gg
  
  png(paste0("output/fig/", format_ind(i, 3), ".png"), width=800, height=600)
  print(gg) ; invisible(dev.off())
  
  catProgress(i, len(outlist_hrv))
}


 
## Individual HRV patterns
###########################

iseq <- 1:len(out7)
iseq <- sort(c(1, 28, 34, 53, 29, 56, 47, 64, 3))

for(i in iseq) {
  out <- out7[[i]]
  which_weekend <- which(out$wdays %in% "weekend")
  
  png(paste0("output/fig/", format_ind(i, 3), ".png"), width=1800, height=800)
  
  # w(16, 7)
  layout(matrix(c(1, 2), nc=2), c(0.6, 0.4))
  plot_g(out$ln_rmssd, type="n", ylim=c(2, 5), ylab="log RMSSD", xlab="time index",
         main=paste0("AnonID: ", names(out7)[i], " | ", out$cohort[1]))
  abline(v=which_weekend, lwd=5, col="lightgoldenrod1")
  lines(out$ln_rmssd, lwd=3, col="red")
  points(out$ln_rmssd, pch=19, col="red")
  lines(out$xx_ma7, lwd=3, col="blue")
  points(out$xx_ma7, pch=19, col="blue")
  lines(out$xx_ma25, lwd=3, col="mediumseagreen")
  points(out$xx_ma25, pch=19, col="mediumseagreen")
  
  xx_all <- as.num(na.omit(as.num(out$ln_rmssd)))
  xx_week <- as.num(na.omit(as.num(out$xx_ma7)))
  xx_month <- as.num(na.omit(as.num(out$xx_ma25)))
  
  dens_all <- density(xx_all)
  dens_week <- dens_month <- NA
  if(len(xx_week) > 2) dens_week <- density(xx_week)
  if(len(xx_month) > 2) dens_month <- density(xx_month)
  
  xlm <- range(xx_all) + 0.5*c(-1, 1)
  hist(xx_all, breaks=len(xx_all)/4, xlim=xlm, border="grey",
       main="Histogram", xlab="log RMSSD", prob=T)
  lines(dens_all, col="red", lwd=3)
  
  if(!is.na(dens_week)) {
    par(new = T)
    plot(dens_week, type="l", col="blue", lwd=3, xlim=xlm, bty="n", yaxt="n", xaxt="n", ann=F)
  }
  
  if(!is.na(dens_month)) {
    par(new = T)
    plot(dens_month, type="l", col="mediumseagreen", lwd=3, xlim=xlm, bty="n", yaxt="n", xaxt="n", ann=F)
  }
  
  invisible(dev.off())
  
  catProgress(i, len(out7))
}


## bimodal ex: 1, 28, 34, 53
## unimodal ex: 29, 56
## 47, 64




#######################################################################################

