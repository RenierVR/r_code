source("../useful_funs.r")
source("../useful_funs_ggplot.r")

library(rhdf5)
library(dtwclust)


##########################################################################
#####  Load data and reformat aggregated data
##########################################################################

out_hrv <- h5read2("~/Data/HRV/concat_daily_hrv_with_lnrmssd.h5", "/daily_hrv_features")

# Only keep IDs with more than 25 days of data
days_per_id <- tapply(out_hrv$index, out_hrv$anon_id, function(x) len(unique(x)))
out_hrv <- out_hrv[-unl(lapply(as.list(names(which(days_per_id < 25))), function(x) grep(x, out_hrv$anon_id))), ]

# Variable for age grouping
out_hrv$AgeBand <- cut(out_hrv$meta_age, c(seq(20, 60, 10), 80), include.lowest=T)

# Standardise gender levels
out_hrv$meta_gender[out_hrv$meta_gender == "F"] <- "female"
out_hrv$meta_gender[out_hrv$meta_gender == "M"] <- "male"

# Ignore 60-80 age group
out_hrv <- out_hrv[out_hrv$AgeBand != "(60,80]",]
out_hrv$AgeBand <- as.factor(as.char(out_hrv$AgeBand))

# Add weekday indicator wearable
out_hrv$wdays <- rep("week", nrow(out_hrv))
out_hrv$wdays[weekdays(as.POSIXct(out_hrv$index, origin = "1970-01-01")) %in% c("Saturday", "Sunday")] <- "weekend"

outlist_hrv <- lapply(as.list(unique(out_hrv$anon_id)), function(x) out_hrv[which(!is.na(match(out_hrv$anon_id, x))), ])
names(outlist_hrv) <- unl(lapply(outlist_hrv, function(x) unique(x$anon_id)))

# DF with mean value of 'ln_rmssd' per ID
out2 <- cb(out_hrv[match(unique(out_hrv$anon_id), out_hrv$anon_id), ],
           "xx" = unl(lapply(outlist_hrv, function(x) mean(x$ln_rmssd))))

# DF with weekly MA values of 'ln_rmssd' per ID
out3 <- lapply(outlist_hrv, function(x) {
  i_full <- match(seq(x$index[1], tail(x$index, 1), 86400), x$index)
  return( cb(x[i_full, ], "xx"=movingAve(x$ln_rmssd[i_full], 7, 2, 0.75)) ) })
out3 <- na.omit(df_from_list(out3))

# DF with monthly MA values of 'ln_rmssd' per ID
out4 <- lapply(outlist_hrv, function(x) {
  i_full <- match(seq(x$index[1], tail(x$index, 1), 86400), x$index)
  return( cb(x[i_full, ], "xx"=movingAve(x$ln_rmssd[i_full], 25, 2, 0.75)) ) })
out4 <- na.omit(df_from_list(out4))

# per-ID list with weekly and monthly MA values
out5 <- lapply(outlist_hrv, function(x) {
  i_full <- match(seq(x$index[1], tail(x$index, 1), 86400), x$index)
  return( cb(x[i_full, ], 
             "xx_ma7" = movingAve(x$ln_rmssd[i_full], 7, 2, 0.5),
             "xx_ma25" = movingAve(x$ln_rmssd[i_full], 25, 2, 0.5)) ) })



##########################################################################
#####  Clustering
##########################################################################

# Extract 'ln_rmssd' in seperate list
xlist <- lapply(outlist_hrv, function(x) x$ln_rmssd)

# Only keep RGA participants
xlist <- xlist[which(unl(lapply(outlist_hrv, function(x) x[1, 12])) == "RGA")]

xlist_dens <- lapply(xlist, function(x) density(x))
xlist_dens_mm <- c(min(unl(lapply(xlist_dens, function(x) min(x$x)))), 
                   max(unl(lapply(xlist_dens, function(x) max(x$x)))))

xlist_dens_padded <- lapply(xlist_dens, function(x) {
  x_pre <- rev(seq(x$x[1], xlist_dens_mm[1], by=-diff(x$x)[1]))
  x_post <- seq(tail(x$x, 1), xlist_dens_mm[2], by=diff(x$x)[1])
  
  xnew <- rbind(cb(x_pre, 0),
                cb(x$x, x$y)[2:(len(x$x)-1),],
                cb(x_post, 0))
  
  return(xnew)
})


xlist_dens_padded <- lapply(xlist_dens_padded, function(x) x[,2])
xlist_dens_padded <- reinterpolate(xlist_dens_padded, 512)#max(lengths(xlist_dens_padded)))

# Plot individual densities
# for(i in 1:len(xlist_dens_padded)) {
#   png(paste0("output/fig/", format_ind(i, 3), "_2.png"), width=800, height=600)
# 
#   lplot_g(xlist_dens_padded[[i]], xlim=c(2, 5))
#   invisible(dev.off())
# 
#   catProgress(i, len(xlist_dens_padded))
# }



#####  Hierarchical clustering of densities
############################################

hc_sbd <- tsclust(xlist_dens_padded, type = "h", k = 10, preproc = zscore,
                  distance = "sbd", centroid = shape_extraction,
                  control = hierarchical_control(method = "average"))

hc_dtw <- tsclust(xlist_dens_padded, type = "h", k = 10, preproc = zscore,
                  distance = "dtw_basic", centroid = DBA,
                  control = hierarchical_control(method = "average"))


w() ; plot(hc_dtw)
w() ; plot(hc_dtw, type = "sc")


#####  Partitional clustering
############################################

xlist_dens_padded_zs <- zscore( xlist_dens_padded )

pc_dtw <- tsclust(xlist_dens_padded_zs, k = 5, type = "p",
                  distance = "dtw_basic", centroid = "dba",
                  trace = TRUE, norm = "L2", window.size = 50,
                  args = tsclust_args(cent = list(trace = TRUE)))

pc_ks <- tsclust(xlist_dens_padded_zs, k = 5, type = "p",
                 distance = "sbd", centroid = "shape", trace = TRUE)


pc_tp <-tsclust(xlist_dens_padded_zs, k = 5, type = "t",
                seed = 8, trace = TRUE,
                control = tadpole_control(dc = 1.5, window.size = 20))


w() ; plot(pc_dtw)#, type ="c")
w() ; plot(pc_ks)#, type="c")
w() ; plot(pc_tp)#, type ="c")



