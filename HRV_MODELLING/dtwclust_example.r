source("../useful_funs.r")
library(dtwclust)

##########################################################################
#####  Hierarchical clustering

hc_sbd <- tsclust(CharTraj, type = "h", k = 20,
                  preproc = zscore, seed = 899,
                  distance = "sbd", centroid = shape_extraction,
                  control = hierarchical_control(method = "average"))

w() ; plot(hc_sbd, type = "sc")

##########################################################################
#####  Partitional clustering

data <- zscore( reinterpolate(CharTraj, new.length = max(lengths(CharTraj))) )

pc_dtw <- tsclust(data, k = 4, type = "p",
                  distance = "dtw_basic", centroid = "dba",
                  trace = TRUE, seed = 8,
                  norm = "L2", window.size = 20,
                  args = tsclust_args(cent = list(trace = TRUE)))

pc_ks <- tsclust(data, k = 4, type = "p",
                 distance = "sbd", centroid = "shape",
                 seed = 8, trace = TRUE)

pc_tp <-tsclust(data, k = 4, type = "t",
                seed = 8, trace = TRUE,
                control = tadpole_control(dc = 1.5, window.size = 20))

sapply(list(DTW = pc_dtw, kShape = pc_ks, TADPole = pc_tp),
       cvi, b = CharTrajLabels[60L:100L], type = "VI")



##########################################################################
#####  Fuzzy clustering

acf_fun <- function(dat, ...)
{
  lapply(dat, function(x) as.numeric(acf(x, lag.max = 50, plot = FALSE)$acf) )
}

fc <- tsclust(CharTraj[1:20], type = "f", k = 4,
              preproc = acf_fun, distance = "L2",
              seed = 42)

w() ; plot(fc, series = CharTraj[1:20], type = "series")








