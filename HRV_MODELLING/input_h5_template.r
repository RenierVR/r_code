source("../useful_funs.r")
library(rhdf5)

# enable viewing of 64-bit format timestamps
# options(digits = 22)

## TIME STAMP CONVERSION 
#########################

# out_metric_heartrate$index <- out_metric_heartrate$index/10^6
# as.POSIXlt(out_metric_heartrate$index[1]/1000, origin = "1970-01-01")

##  VIEW HDF5 FILE CONTENTS
############################

h5_fpath = ""
h5_contents <- h5ls(h5_fpath)
print( h5_contents[h5_contents$group != "/", c(1, 5)] )



## ACCESS TIME SERIES DATA
###########################

out_metric_heartrate <- h5read(h5_fpath, "/metric_heartrate", bit64conversion="double", compoundAsDataFrame=T)$table


