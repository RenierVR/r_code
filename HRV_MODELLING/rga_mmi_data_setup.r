source("../useful_funs.r")


#################################################
####  Read in and prepare bio/UW data  ##########
#################################################



dat_df <- t(sapply(readLines(file("~/Data/MMI_RGA/RGApilot_UW_Bio_CompactResults_new.csv")), 
                   function(x) unlist(strsplit(x, ","))))
dimnames(dat_df) <- list(NULL, dat_df[1,])
dat_df <- dat_df[-1,]


# Check that anon ID mix up is corrected
if(dat_df[dat_df[,1] == "1406b144-eed7-45be-9052-9610fdab12f2", 3] != "b3ae4d0c-4aef-455a-b5e6-d5deaa157b5c" )
  cat("ID inconsistency detected.\n")

# Add cholesterol
dat_chol <- read.table("~/Data/MMI_RGA/RGApilot_All_cholesterol_results.csv", head=T, sep=",")[,3]


# Configure df used for modelling
dat_use <- data.frame(dat_df[,3], dat_df[,5], apply(dat_df[,6:12], 2, as.num), dat_chol, dat_df[,13], dat_df[,23:25])
names(dat_use) <- c("ID", paste0("BIO_", c("SEX","AGE", "HEIGHT", "WEIGHT", "BP_SYS", "BP_DIA", "RHR", "PEFR", "CHOL", "SMOKE")), paste0("UW_", c("OV", "PREF", "PREF_NOFAM")))

# Add BMI
dat_use <- cb(dat_use[,1:5], "BIO_BMI"=dat_use$BIO_WEIGHT/(0.01*dat_use$BIO_HEIGHT)^2, dat_use[,6:ncol(dat_use)])

# Cast SEX as integer
dat_use$BIO_SEX <- (as.num(dat_use$BIO_SEX) - 1)

# Add summary UW columns for convenience
dat_uwk4 <- as.num(as.char(dat_use$UW_PREF_NOFAM))
dat_uwk4[which(dat_use$UW_PREF_NOFAM == 2 & dat_use$UW_OV == 0)] <- 2
dat_uwk4[which(dat_use$UW_OV == 1)] <- 3
dat_uwk4[which(dat_use$UW_OV == 99)] <- 99

dat_uwk3 <- dat_uwk4
dat_uwk3[dat_uwk4 == 3] <- 2

dat_use <- cb(dat_use, "UW_K4"=dat_uwk4, "UW_K3"=dat_uwk3)

# Omit defers from working data set
dat_use <- dat_use[dat_use$UW_K3 != 99, ]

# Make binary classification categories
dat_use$UW_K2_sp <- dat_use$UW_K3
dat_use$UW_K2_sp[dat_use$UW_K2_sp == 2] <- 1

dat_use$UW_K2_p <- dat_use$UW_K3
dat_use$UW_K2_p[dat_use$UW_K2_p == 1] <- 0
dat_use$UW_K2_p[dat_use$UW_K2_p == 2] <- 1

dat_use$UW_SS <- dat_use$UW_K4
dat_use$UW_SS[dat_use$UW_SS %in% c(1, 2)] <- 0
dat_use$UW_SS[dat_use$UW_SS == 3] <- 1




#################################################
####  Read in and prepare concatenated data  ####
#################################################

library(rhdf5)
H5close()

# h5_fpath <- "~/HealthQ/RGA/AGGREGATED/RGA_colated_data_v6.h5"
# h5_fpath <- "~/HealthQ/RGA/AGGREGATED/ALL_colated_data_with_ae.h5"
h5_fpath <- "~/Data/MMI_RGA/ALL_colated_data.h5"
h5_contents <- h5ls(h5_fpath)
# print( h5_contents[h5_contents$group != "/", c(1, 5)] )

dat_h5_map <- h5read(h5_fpath, "/mappings", bit64conversion="double", compoundAsDataFrame=T)$table[,-1]


dat_h5_list <- list("ACTIVITY"=h5read(h5_fpath, "/activity", bit64conversion="double", compoundAsDataFrame=T)$table[,-1],
                    "HR"=h5read(h5_fpath, "/hr", bit64conversion="double", compoundAsDataFrame=T)$table[,-1],
                    "HRV"=h5read(h5_fpath, "/hrv", bit64conversion="double", compoundAsDataFrame=T)$table[,-1],
                    "PWF"=h5read(h5_fpath, "/pwf", bit64conversion="double", compoundAsDataFrame=T)$table[,-1],
                    "SLEEP"=h5read(h5_fpath, "/sleep", bit64conversion="double", compoundAsDataFrame=T)$table[,-1],
                    "SLEEP_HRV"=h5read(h5_fpath, "/sleep_hrv", bit64conversion="double", compoundAsDataFrame=T)$table[,-1],
                    "TRENDS"=h5read(h5_fpath, "/trends", bit64conversion="double", compoundAsDataFrame=T)$table[,-1])

dat_h5_ae_act <- h5read(h5_fpath, "/aevals_activity", bit64conversion="double", compoundAsDataFrame=T)$table
dat_h5_ae_act <- dat_h5_ae_act[match(0:(nrow(dat_h5_list[[1]])-1), dat_h5_ae_act[,1]), -1]

dat_h5_all <- as.data.frame(dat_h5_list)
dat_h5_all <- cb(dat_h5_all, "AEVALS_ACT"=dat_h5_ae_act)

act_ratio_i <- grep("ACTIVITY.activity_ratio_mean", names(dat_h5_all))
dat_h5_all[is.infinite(dat_h5_all[,act_ratio_i]), act_ratio_i] <- NaN
# apply(dat_h5_all, 2, function(x) which(is.infinite(x)))

#################################################################
####  Amend concatenated data with missing RGA participants  ####
#################################################################


# h5_fpath <- "~/HealthQ/RGA/AGGREGATED/RGA_colated_data_v6.h5"
# h5_contents <- h5ls(h5_fpath)
# # print( h5_contents[h5_contents$group != "/", c(1, 5)] )
# 
# dat_h5_list <- list("ACTIITY"=h5read(h5_fpath, "/activity", bit64conversion="double", compoundAsDataFrame=T)$table[,-1],
#                     "HR"=h5read(h5_fpath, "/hr", bit64conversion="double", compoundAsDataFrame=T)$table[,-1],
#                     "HRV"=h5read(h5_fpath, "/hrv", bit64conversion="double", compoundAsDataFrame=T)$table[,-1],
#                     "PWF"=h5read(h5_fpath, "/pwf", bit64conversion="double", compoundAsDataFrame=T)$table[,-1],
#                     "SLEEP"=h5read(h5_fpath, "/sleep", bit64conversion="double", compoundAsDataFrame=T)$table[,-1],
#                     "SLEEP_HRV"=h5read(h5_fpath, "/sleep_hrv", bit64conversion="double", compoundAsDataFrame=T)$table[,-1],
#                     "TRENDS"=h5read(h5_fpath, "/trends", bit64conversion="double", compoundAsDataFrame=T)$table[,-1])
# 
# dat_h5_rga <- as.data.frame(dat_h5_list)
# 
# ## only keep IDs that are not in `dat_h5_all`
# dat_h5_rga <- dat_h5_rga[is.na(match(dat_h5_rga[,1], dat_h5_all[,1])),]
# 
# 
# # dat_h5_merged <- merge(dat_h5_all, dat_h5_rga, all=T, sort=F)
# dat_h5_all <- merge(dat_h5_all, dat_h5_rga, all=T, sort=F)


######################################################
####  Read in and append additional data sources  ####
######################################################

## Dirk's Auto Encoders

# datlist_ae <- getDataFromDir("~/HealthQ/RGA/AGGREGATED/AE_converted/", head=T)
# datlist_ae <- lapply(datlist_ae, function(x) x[match(0:247, x[,1]), ])
# names(datlist_ae) <- toupper(names(datlist_ae))
# datlist_ae <- lapply(datlist_ae, function(x){ for(i in 1:67){ x <- rbind(x, NA) } ; return(x) })

h5_fpath <- "~/Data/MMI_RGA/ALL_colated_data_with_ae.h5"

h5_ae_anonids <- h5read(h5_fpath, "/activity", bit64conversion="double", compoundAsDataFrame=T)$table[,2]

datlist_ae <- list("AEVALS_HR"=h5read(h5_fpath, "/aevals_hr", bit64conversion="double", compoundAsDataFrame=T)$table,
                   "AEVALS_PWF"=h5read(h5_fpath, "/aevals_pwf", bit64conversion="double", compoundAsDataFrame=T)$table,
                   "AEVALS_SLEEP"=h5read(h5_fpath, "/aevals_sleep", bit64conversion="double", compoundAsDataFrame=T)$table,
                   "AEVALS_SLEEPHRV"=h5read(h5_fpath, "/aevals_sleep_hrv", bit64conversion="double", compoundAsDataFrame=T)$table)

datlist_ae <- lapply(datlist_ae, function(x) x[match(0:(len(h5_ae_anonids)-1), x[,1]), ])

h5_id_match <- match(dat_h5_all[,1], h5_ae_anonids)

dat_h5_all <- cb(dat_h5_all, as.data.frame(datlist_ae)[h5_id_match, ])




## Remove unnecessary ID columns
dat_h5_all <- dat_h5_all[, -c(grep(".anon_id", names(dat_h5_all)), grep(".index", names(dat_h5_all)))[-1]]
names(dat_h5_all)[1] <- "ANON_ID"



## Add cohort indicator column
map_match <- match(dat_h5_all$ANON_ID, dat_h5_map$anon_id)
dat_h5_all <- cb("COHORT"=dat_h5_map$cohort[map_match], dat_h5_all)



## Antoinette's sleep score features
dat_hraf <- read.table("~/Data/MMI_RGA/HAF_sleep_score_mean_sd_v1.csv", sep=",", head=T)
names(dat_hraf)[2:3] <- paste0("HRAF.sleep_score", c("_mean", "_std"))
id_match <- match(dat_h5_all$ANON_ID, dat_hraf$anon_id)
# dat_hraf <- dat_hraf[id_match, c(4,2,3)]

dat_h5_all <- cb(dat_h5_all, dat_hraf[id_match, 2:3])




########################################
####  Franco's HR entropy features  ####
########################################

dat_ent_rga <- h5read("~/Data/MMI_RGA/RGApilot_HRentropy_colated_data.h5", "/entropy", bit64conversion="double", compoundAsDataFrame=T)$table[,-1]

dat_ent_mmi <- h5read("~/Data/MMI_RGA/MMIpilot_HRentropy_colated_data.h5", "/colated_data", bit64conversion="double", compoundAsDataFrame=T)$table[,-1]
dat_mmi_idmap <- dat_ent_mmi[, c(grep("anon_id", names(dat_ent_mmi)), grep("dbn_id", names(dat_ent_mmi)))]
dat_ent_mmi <- dat_ent_mmi[, c(grep("anon_id", names(dat_ent_mmi)), grep("entropy", names(dat_ent_mmi)))]

dat_ent <- merge(dat_ent_mmi, dat_ent_rga, all=T, sort=F)
names(dat_ent) <- paste0("HR_ENTROPY.", names(dat_ent))

dat_h5_all <- cb(dat_h5_all, dat_ent[match(dat_h5_all$ANON_ID, dat_ent[,1]), -1])

# dat_h5_all$ANON_ID[is.na(match(dat_h5_all$ANON_ID, dat_ent[,1]))]
# aa <- dat_h5_all$ANON_ID[is.na(match(dat_h5_all$ANON_ID, dat_ent[,1]))]
# bb <- dat_ent$HR_ENTROPY.anon_id[is.na(match(dat_ent[,1], dat_h5_all$ANON_ID))]




#######################################
####  Franco's Life Expt features  ####
#######################################


dat_le_rga <- read.table("~/Data/MMI_RGA/RGApilot_LE_based_on_profile_VO2max_and_wearable_RHR.csv", sep=",", head=T)
dat_le_rga$ANON_ID <- dat_df[match(dat_le_rga$dbn_id, dat_df[,2]), 3]
dat_le_rga <- dat_le_rga[, match(c("ANON_ID", "VO2max", "lifeq_Life_Expectancy"), names(dat_le_rga))]

dat_le_mmi <- read.table("~/Data/MMI_RGA/MMIpilot_LE_more_values.csv", head=T, sep=",")
dat_le_mmi$ANON_ID <- dat_mmi_idmap$anon_id[match(dat_le_mmi$dbn_id, dat_mmi_idmap$dbn_id)]
dat_le_mmi <- dat_le_mmi[, match(c("ANON_ID", "VO2max", "lifeq_Life_Expectancy"), names(dat_le_mmi))]

dat_le <- merge(dat_le_rga, dat_le_mmi, all=T, sort=F)
names(dat_le)[2:3] <- c("BIO_DEFAULT_VO2MAX", "LIFE_EXP.lifeq_life_expectancy")


dat_h5_all <- cb(dat_h5_all, dat_le[match(dat_h5_all$ANON_ID, dat_le$ANON_ID), 2:3])
dat_h5_all <- dat_h5_all[, c(grep("BIO_DEFAULT_VO2MAX", names(dat_h5_all)), (1:ncol(dat_h5_all))[-grep("BIO_DEFAULT_VO2MAX", names(dat_h5_all))])]


#########################################
####  MMI John-Hancock underwriting  ####
#########################################

dat_mmi_jh <- read.table("~/Data/MMI_RGA/UW_JH_MMI.csv", head=T, sep=",")

# relevelling to standardised convention
levels(dat_mmi_jh$BIO_SEX) <- c("Female", "Male")
levels(dat_mmi_jh$BIO_SMOKE) <- c("No", "Ex2", "Ex1", "Yes")
levels(dat_mmi_jh$UW_outcome) <- c(2, 1, 0)

names(dat_mmi_jh)[grep("UW_outcome", names(dat_mmi_jh))] <- "UW_K3"
names(dat_mmi_jh)[grep("AnonID", names(dat_mmi_jh))] <- "ID"

# Omit defers
dat_mmi_jh <- dat_mmi_jh[dat_mmi_jh$DecisionType %in% c("Decline", "Loading", "Standard rates"), ]
dat_mmi_jh$DecisionType <- factor(as.char(dat_mmi_jh$DecisionType))


# Make binary classification categories
dat_mmi_jh$UW_K2_sp <- as.num(as.char(dat_mmi_jh$UW_K3))
dat_mmi_jh$UW_K2_sp[dat_mmi_jh$UW_K2_sp == 2] <- 1

dat_mmi_jh$UW_K2_p <- as.num(as.char(dat_mmi_jh$UW_K3))
dat_mmi_jh$UW_K2_p[dat_mmi_jh$UW_K2_p == 1] <- 0
dat_mmi_jh$UW_K2_p[dat_mmi_jh$UW_K2_p == 2] <- 1

dat_mmi_jh$UW_SS <- as.char(dat_mmi_jh$DecisionType)
dat_mmi_jh$UW_SS[dat_mmi_jh$UW_SS == "Standard rates"] <- 0
dat_mmi_jh$UW_SS[dat_mmi_jh$UW_SS %in% c("Loading", "Decline")] <- 1
dat_mmi_jh$UW_SS <- as.num(dat_mmi_jh$UW_SS)


# Cast SEX as integer
dat_mmi_jh$BIO_SEX <- (as.num(dat_mmi_jh$BIO_SEX) - 1)



####################################################
####  Open reformatted questionnaire responses  ####
####################################################

dat_q <- read.table("~/Data/MMI_RGA/all_reformatted_questionnaire.csv", head=T, sep=",")

match_rga <- match(dat_use$ID, dat_q$Anon_ID)
match_mmi <- match(dat_mmi_jh$ID, dat_q$Anon_ID)

# append questionnaire data to RGA profile data
dat_use <- cb(dat_use, dat_q[match_rga, -1])
dat_use <- cb(dat_use[, -grep("UW", names(dat_use))], dat_use[, grep("UW", names(dat_use))])
names(dat_use)[grep("SMOKE", names(dat_use))] <- "QUESTION_SMOKE"


# append questionnaire data to MMI profile data
dat_mmi_jh <- cb(dat_mmi_jh, dat_q[match_mmi, -1])
dat_mmi_jh <- cb(dat_mmi_jh[, -grep("UW", names(dat_mmi_jh))], dat_mmi_jh[, grep("UW", names(dat_mmi_jh))])
names(dat_mmi_jh)[grep("SMOKE", names(dat_mmi_jh))] <- "QUESTION_SMOKE"




################################################
####  Merge data for use in model building  ####
################################################

## Create data subsets with no profile dependent features
prof_dependent_idxs <- sapply(c("mmi_active_days", 
                                "lifeq_active_days",
                                "energy_expenditure_30min_mean_mean",
                                "energy_expenditure_30min_mean_std",
                                "energy_expenditure_30min_max_mean",
                                "energy_expenditure_30min_max_std",
                                "energy_expenditure_30min_std_mean",
                                "energy_expenditure_30min_std_std",
                                "pythline_calories_total_mean",
                                "pythline_calories_total_std",
                                "pythline_calories_measured_mean",
                                "pythline_calories_measured_std",
                                "pythline_calories_estimated_mean",
                                "pythline_calories_estimated_std",
                                "auto_vo2_mean",
                                "auto_vo2_std",
                                "activity_clf_healthy",
                                "activity_clf_unhealthy",
                                "pwv_gradient_mean",
                                "pwv_gradient_std",
                                "pwv_c_mean",
                                "pwv_c_std",
                                "pwv_75_mean",
                                "pwv_75_std",
                                "aix_gradient_mean",
                                "aix_gradient_std",
                                "aix_c_mean",
                                "aix_c_std",
                                "aix_75_mean",
                                "aix_75_std",
                                "pwf_clf_healthy",
                                "pwf_clf_unhealthy",
                                "lifeq_life_expectancy",
                                "AEVALS_ACT",
                                "AEVALS_PWF"), grep, names(dat_h5_all)) # "sleep_quality_mean", "sleep_quality_std", "hrr_mean", "hrr_std"

dat_h5_noprof <- dat_h5_all[, -unl(prof_dependent_idxs)]


dat_match_rga <- match(dat_use$ID, dat_h5_all$ANON_ID)
dat_rga <- cb(dat_use[!is.na(dat_match_rga), ],
              dat_h5_all[na.omit(dat_match_rga), ])
dat_rga_noprof <- cb(dat_use[!is.na(dat_match_rga), ], 
                     dat_h5_noprof[na.omit(dat_match_rga), ])

dat_all_use <- merge(dat_use, dat_mmi_jh, all=T, sort=F)[,1:len(intersect(names(dat_use), names(dat_mmi_jh)))]
dat_match_all <- match(dat_all_use$ID, dat_h5_all$ANON_ID)
dat_all <- cb(dat_all_use[!is.na(dat_match_all), ],
              dat_h5_all[na.omit(dat_match_all), ])
dat_all_noprof <- cb(dat_all_use[!is.na(dat_match_all), ], 
                     dat_h5_noprof[na.omit(dat_match_all), ])


id_idxs <- match(c("COHORT", "ANON_ID"), names(dat_rga))
dat_rga <- dat_rga[, c(id_idxs, (1:ncol(dat_rga))[-c(1, id_idxs)])]

id_idxs <- match(c("COHORT", "ANON_ID"), names(dat_rga_noprof))
dat_rga_noprof <- dat_rga_noprof[, c(id_idxs, (1:ncol(dat_rga_noprof))[-c(1, id_idxs)])]

id_idxs <- match(c("COHORT", "ANON_ID"), names(dat_all))
dat_all <- dat_all[, c(id_idxs, (1:ncol(dat_all))[-c(1, id_idxs)])]

id_idxs <- match(c("COHORT", "ANON_ID"), names(dat_all_noprof))
dat_all_noprof <- dat_all_noprof[, c(id_idxs, (1:ncol(dat_all_noprof))[-c(1, id_idxs)])]





# dat_all <- dat_all[,c(11:12, 2:10, 13:ncol(dat_all))]
write.table2(dat_all, "~/Data/MMI_RGA/MMI_RGA_combined_dat_for_models.csv", col.names=T)



#######################################
####  Cleanup unnecessary globals  ####
#######################################

rm(dat_df)
rm(dat_h5_list, dat_h5_ae_act)
rm(dat_use, dat_all_use)
rm(dat_h5_map, datlist_ae, dat_hraf, dat_mmi_jh)
rm(dat_ent_rga, dat_ent_mmi, dat_le_rga, dat_le_mmi, dat_ent, dat_le)
rm(dat_uwk4, dat_uwk3, map_match, id_match, prof_dependent_idxs, dat_match_rga, dat_match_all, dat_mmi_idmap, id_idxs, h5_ae_anonids, h5_id_match)


