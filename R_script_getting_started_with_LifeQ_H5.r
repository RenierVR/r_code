#######################################
####  PREAMBLE                     ####
#######################################

# source("http://bioconductor.org/biocLite.R")
# biocLite("rhdf5")

library(rhdf5)

# enable viewing of 64-bit format timestamps
options(digits = 22)


#######################################
####  VIEW HDF5 FILE CONTENTS      ####
#######################################

# path to h5 file
# h5_fpath <- "/.../..."

h5_fpath <- "D:/HealthQ/Data/RRI/Test/parsed_hdf5/20170117_095304_20170117_TPID0404_Garmin_Ingestion_FREELIVING.h5"
h5_contents <- h5ls(h5_fpath)

# print all contents
print( h5_contents )

# print group names with corresponding dimension
print( h5_contents[h5_contents$group != "/", c(1, 5)] )



#######################################
####  ACCESS HDF5 FILE CONTENTS    ####
#######################################

# The following code serves as an example to access data stored in
# the HDF5 file using the so-called group name. This will usually
# be in the form of either meta-information (such as demographic or 
# profile data) or the output of some metric. 
#
# The group name is used to access relevant data in the HDF5 file.
#
# The output data is usually in the form of two columns, with the
# first, 'index', containing the time stamps, and the second, 'value'
# the output value.


## NOTE ON WARNINGS
####################

# The code below would induce warnings regarding the bit-conversion setting.
# THis is due to the time stamps, which are formatted with a 64-bit python
# package upon HDF5 file construction.
#
# While one can use the R package "bit64" as per the warning suggestions, it
# is not done here -- this package invokes data structures not compatible with
# vectorised code and can cause problems with already existing code for analysis.
#
# So far, the autor of this script has not yet run into problems by ignoring
# this problem and handling the time stamps as in 'Example 3' below.


## EXAMPLE 1: METADATA
#######################

# Accessing the resting heart rate profile data from the HDF5 file
out_meta_rhr <- h5read(h5_fpath, "/meta_rhr", bit64conversion="double", compoundAsDataFrame=T)$table
print( out_meta_rhr )


## EXAMPLE 2: HEART RATE OUTPUT
################################

# Accessing the resting HR output data from the HDF5 file
out_metric_heartrate <- h5read(h5_fpath, "/metric_heartrate", bit64conversion="double", compoundAsDataFrame=T)$table

# Plot heart rate for visual inspection
w()
plot_g(out_metric_heartrate$value, type="l", lwd=2, col="blue")


## EXAMPLE 3: DEALING WITH TIME STAMPS
######################################

# You will notice that the format of the timestamps are in Unix time, but
# with many trailing zeros. To convert to Unix time in milliseconds, divide
# the time stamp from the HDF5 file by 10^7:

out_metric_heartrate$index <- out_metric_heartrate$index/10^6

# To convert this to standard unix time, you would need to divide by a further 10^3.
# This allows you to use the 'as.POSIXlt()' function to convert into dates, eg:
as.POSIXlt(out_metric_heartrate$index[1]/1000, origin = "1970-01-01")



