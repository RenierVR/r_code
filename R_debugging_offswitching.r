source('../useful_funs.r')

nfiles <- 2


p_rr_fnames <- paste0("~/HealthQ/Python/rr_debug_out", 0:(nfiles-1), ".csv")
p_rrhr_fnames <- paste0("~/HealthQ/Python/rr_hr_debug_out", 0:(nfiles-1), ".csv")
p_deb_fnames <- paste0("~/HealthQ/Python/rr_debug_deb", 0:(nfiles-1), ".csv")

p_rr_list <- vector("list", nfiles)
p_hr_list <- vector("list", nfiles)
p_deb_list <- vector("list", nfiles)
for(i in 0:(nfiles-1)) {
  p_rr_list[[i+1]] <- read.table(p_rr_fnames[i+1], head=T, sep=",")[,-1]
  p_hr_list[[i+1]] <- read.table(p_rrhr_fnames[i+1], head=T, sep=",")[,-1]
  p_deb_list[[i+1]] <- read.table(p_deb_fnames[i+1], head=T, sep=",")[,-1]
  
  catProgress((i+1), nfiles)
  # break
}


# p_rr_list_b <- p_rr_list
# p_hr_list_b <- p_hr_list
# p_list_b <- list(p_rr_list_b, p_hr_list_b)
# save(p_list_b, file="p_list_b.Rdata")

# load("p_list_b.Rdata")
# p_rr_list_b <- p_list_b[[1]]
# p_hr_list_b <- p_list_b[[2]]

###########################################################################


# p_rr_list[[1]] == p_rr_list_b[[1]]

p_rr_list[[1]] %=% p_rr_list_b[[1]]
p_rr_list[[2]] %=% p_rr_list_b[[2]]
# p_rr_list[[3]] %=% p_rr_list_b2[[3]]


p_hr_list[[1]] %=% p_hr_list_b[[1]]
p_hr_list[[2]] %=% p_hr_list_b[[2]]
# p_hr_list[[3]] %=% p_hr_list_b2[[3]]




###########################################################################

i <- 2
hr <- p_hr_list[[i]] ; rr <- p_rr_list[[i]]
hr_b <- p_hr_list_b[[i]] ; rr_b <- p_rr_list_b[[i]]
# deb <- p_deb_list[[i]]

j <- 1 ; zPlot(cb(hr[,j],  hr_b[,j]))
# zPlot(cb_eq(rr[,1], rr_b[,1]))


w() ; lplot_g(hr_b[seq(1, nrow(hr_b), 25),1], lwd=4, 
              xlab="time (s)", ylab="HR (bpm)", main="TPID0019")
lines(hr[seq(1, nrow(hr_b), 25),1], col="red", lwd=2)
legend("bottomright", bg="white", lwd=2, col=c("blue", "red"),
       leg=c("old HR from device (continuous RR)",
             "new HR from device (RR switching off)"))



rr1 <- cb(0.04*rr[,4], rr[,1], rr[,5])
rr2 <- cb(0.04*rr_b[,4], rr_b[,1], rr_b[,5])
# rr_reset <- 0.04*deb[deb[,2]==0, 1]
rr_notrun <- 0.04*which(hr$pp_rr_isrunning == 0)
scrollPlot(start=40000, winsize=500, n=floor(tail(rr2, 1)[1]), plotfun=function(xi){
  
  which_rr1 <- which((rr1[,1] >= xi[1]) & (rr1[,1] <= tail(xi, 1)))
  which_rr2 <- which((rr2[,1] >= xi[1]) & (rr2[,1] <= tail(xi, 1)))
  
  plot_g(rr2[which_rr2, 1], rr2[which_rr2, 2], type="n", xlim=range(xi),
         ylab="RR interval (ms)", xlab="time (s)")
  abline(v=rr_notrun, col="yellow")
  # abline(v=rr_reset, col="orange", lwd=2)
  # abline(v=rr1[which_rr1, 1][rr1[which_rr1,3] == 0], col="orange", lwd=3)
  # abline(v=rr2[which_rr2, 1][rr2[which_rr2,3] == 0], col="seagreen", lwd=2)
  lines(rr2[which_rr2, 1], rr2[which_rr2, 2], col="mediumseagreen", lwd=4)
  lines(rr1[which_rr1, 1], rr1[which_rr1, 2], lwd=2, col="red")
  
  hrseq <- (25*xi[1]):(25*tail(xi,1))
  par(new=T)
  lplot(hrseq*0.04, hr[hrseq, 1], col="black", lwd=2, xlim=range(xi),
        ann=F, yaxt="n", xaxt="n", ylim= (c(-20,5) + range(c(hr[hrseq, 1], hr_b[hrseq, 1]))))
  lines(hrseq*0.04, hr_b[hrseq, 1], col="blue", lwd=2)
  axis(4)
  
  legend("bottomright", bg="white", lwd=2, col=c("mediumseagreen", "red", "blue", "black"),
         leg=c("old RR (continuous)", "new RR (switching off)", "old HR", "new HR"), cex=0.75)
  
})





###########################################################################



### Scroll plot with RR confidence

# rr1 <- cb(0.04*rr[,4], rr[,1], rr[,5], rr[,2])
# rr2 <- cb(0.04*rr_b[,4], rr_b[,1], rr_b[,5], rr_b[,2])
# # rr_reset <- 0.04*deb[deb[,2]==0, 1]
# rr_notrun <- 0.04*which(hr$pp_rr_isrunning == 0)
# scrollPlot(start=50000, winsize=500, n=floor(tail(rr2, 1)[1]), plotfun=function(xi){
#   
#   which_rr1 <- which((rr1[,1] >= xi[1]) & (rr1[,1] <= tail(xi, 1)))
#   which_rr2 <- which((rr2[,1] >= xi[1]) & (rr2[,1] <= tail(xi, 1)))
#   
#   plot_g(rr2[which_rr2, 1], rr2[which_rr2, 2], type="n", xlim=range(xi),
#          ylab="RR interval (ms)", xlab="time (s)")
#   abline(v=rr_notrun, col="yellow")
#   # abline(v=rr_reset, col="orange", lwd=2)
#   abline(v=rr1[which_rr1, 1][rr1[which_rr1,3] == 0], col="red", lwd=3)
#   abline(v=rr2[which_rr2, 1][rr2[which_rr2,3] == 0], col="mediumseagreen", lwd=2)
#   lines(rr2[which_rr2, 1], rr2[which_rr2, 2], col="mediumseagreen", lwd=4)
#   lines(rr1[which_rr1, 1], rr1[which_rr1, 2], lwd=2, col="red")
#   
#   par(new=T)
#   lplot(rr2[which_rr2, 1], rr2[which_rr2, 4], col="black", lwd=2, xlim=range(xi),
#         ann=F, yaxt="n", xaxt="n", ylim= (c(-20,5) + range(c(rr2[which_rr2, 4], rr1[which_rr1, 4]))))
#   lines(rr1[which_rr1, 1], rr1[which_rr1, 4], col="blue", lwd=2)
#   axis(4)
#   
#   legend("bottomright", bg="white", lwd=2, col=c("mediumseagreen", "red", "blue", "black"),
#          leg=c("old RR (continuous)", "new RR (switching off)", "old HR", "new HR"), cex=0.75)
#   
# })






