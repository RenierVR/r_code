source("../useful_funs.r")
library(ggplot2)
library(plotly)

###################################################################################################
#####  Get data and trim to keep only entries after 2017-01-01
###################################################################################################

datlist <- getDataFromDir("~/Documents/Personal/BTC_DATA/", dataE = F)
datlist <- as.list(datlist[sapply(c("bitstampUSD", "coinbaseUSD"), grep, datlist)])

for(i in 1:len(datlist)) {
  datlist[[i]] <- read.table(datlist[[i]], sep = ",", head = T)
  
  ts <- as.POSIXct(datlist[[i]][,1], origin = "1970-01-01")
  datlist[[i]] <- datlist[[i]][which(unl(lapply(strsplit(as.char(ts), "-"), function(x) x[[1]])) == "2017")[1]:nrow(datlist[[i]]), ]
}


dat1 <- datlist[[1]]
dat2 <- datlist[[2]]
dat2 <- dat2[match(dat1[,1], dat2[,1]), ]



###################################################################################################
#####  Choose sample section with highs/lows 
###################################################################################################

xs <- 311400:405500
dat <- dat1[xs, match(c("Timestamp", "Close"), names(dat1))]

dat$Timestamp <- as.POSIXct2(dat$Timestamp)

gg <- ggplot(data=dat, aes(x=Timestamp, y=Close))
gg <- gg + geom_line() + geom_point()
ggplotly(gg)




###################################################################################################
#####  Basic categorisation of rise/fall based on previous signed diffs
###################################################################################################

dat$Smoothed <- expSmooth(dat$Close, 0.5)

sm <- dat$Close[-1]
sm_diff <- sign(diff(dat$Smoothed))
sm_diff_count <- numeric(len(sm_diff))
sign_current <- sm_diff[1]
for(i in 2:len(sm_diff)) {
  if(sm_diff[i] == sign_current)
    sm_diff_count[i] <- sm_diff_count[i-1] + sign_current
  else if((sm_diff[i] != sign_current) && (sm_diff[i-1] == sign_current))
    sm_diff_count[i] <- sm_diff_count[i-1] + sign_current
  else
    sign_current <- sm_diff[i]
    
}

sm_diff_cat <- rep("stable", len(sm_diff_count))
sm_diff_cat[sm_diff_count > 2] <- "rise"
sm_diff_cat[sm_diff_count < -2] <- "fall"



scrollPlot(window = F, winsize = 120, n = len(sm_diff), plotfun = function(xi) {
  
  plot_g(xi, sm[xi], type="n")
  abline(v = which(sm_diff_cat == "rise"), col = "green")
  abline(v = which(sm_diff_cat == "fall"), col = "orange")
  lines(xi, sm[xi], col="black")
  
})




###################################################################################################
#####  Basic categorisation of rise/fall based on previous signed diffs
###################################################################################################


dat$Smoothed <- expSmooth(dat$Close, 0.5)
# dat$BoundUpper <- dat$Close + 0.05*dat$Smoothed
# dat$BoundLower <- dat$Close - 0.05*dat$Smoothed


gain_calc <- function(xmat, wts)
{
  gain <- xmat[nrow(xmat),]$Close / xmat[-nrow(xmat),]$Smoothed
  return(100*(1 - as.num(gain %*% wts)))
}



gain <- numeric(nrow(dat))
for(i in 5:nrow(dat)) {
  
  # run_mean <- mean(dat$Smoothed[(i-2):(i-1)])
  # gain[i] <- 100*(dat$Smoothed[i] - run_mean)/run_mean
  gain[i] <- gain_calc(dat[(i-4):i, match(c("Close", "Smoothed"), names(dat))], 
                       wts = c(0.5, 0.35, 0.1, 0.05))
  
  catProgress(i, nrow(dat))
}

scrollPlot(window = F, winsize = 720, n = nrow(dat), plotfun = function(xi) {
  
  lplot_g(xi, dat$Close[xi], col="black")
  lines(xi, dat$Smoothed[xi], col="blue", lwd=2)
  par(new = T)
  lplot(xi, gain[xi], col="red", ylim=c(-2.5, 2.5), yaxt="n", xaxt="n", ann=F)
  abline(h=0, col="red", lty=2)
})





