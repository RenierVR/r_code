source("../useful_funs.r")


# dat_fixed <- read.table("~/python_code/pybitx/sandbox/test.txt", head=T, sep=",")
# i <- 10

btcRealTimeVisualiser <- function(out_fpath="~/python_code/pybitx/sandbox/test3.txt", win=TRUE, 
                                  PLOT_NEW_WIN_SIZE=60*30,
                                  PLOT_NEW_FILLED_PROP=0.925,
                                  USD_TO_ZAR=11.90,
                                  LUNO_USD_ADJUST=0)
{ ## Real-time BTC visualiser to track API tickers of 3 exchanges (Luno, Coindesk and Bitfinex).
  ## Reads from output path 'out_fpath' and creates seperate window when 'win' is TRUE.
  
  datUseLastSecs <- function(x, win_len=60)
  { ## Reshapes df to only use last 'win_len' seconds
    ##  (assuming that 'x' has a col named 'timestamp' in POSIXct format).
    
    ts2 = tail(x$timestamp,1) - win_len <= x$timestamp
    return(x[ts2,])
  }
  
  
  if(win) w()
  
  new_plot <- TRUE
  n_dat <- 0
  while(T) {
    
    par(mar=c(4, 4, 1.5, 1.5))
    # layout(matrix(c(1, 1, 2, 3), nrow=2), widths=c(0.6, 0.4))
    
    ## Read API data collected by python script and update
    ## visualiser only if a new row was added
    ###########################################################
    
    dat <- read.table(out_fpath, head=T, sep=",")
    # dat <- dat_fixed[1:i,] ; i <- i + 1
    
    if(nrow(dat) == n_dat)
      next
    else
      n_dat <- nrow(dat)
    
    dat$timestamp <- as.POSIXct(dat$timestamp/1000, origin="1970-01-01")
    
    ## Truncate data to specified window size
    ###########################################
    
    if(new_plot)
      dat_p <- datUseLastSecs(dat, PLOT_NEW_WIN_SIZE/2)
    else
      dat_p <- datUseLastSecs(dat, PLOT_NEW_WIN_SIZE)
    
    ## Convert prices to one unit (USD) and 
    ## concatenate them for range calculations
    #############################################
    
    dat_p$luno_USD_last_trade <- dat_p$luno_ZAR_last_trade/USD_TO_ZAR - LUNO_USD_ADJUST
    dat_vals <- dat_p[, match(c("luno_USD_last_trade", "coindesk_USD_rate", "bitfinex_USD_last_price"), names(dat_p))]
    
    ## Update the plot window if necessary 
    #############################################
    
    if(new_plot) {
      lplot_g(dat_p$timestamp, dat_p$luno_USD_last_trade, ylim=c(0.99, 1.01)*range(dat_vals), 
              xlim = as.POSIXct(dat_p$timestamp[c(1, 1)] + c(0, PLOT_NEW_WIN_SIZE), origin="1970-01-01"),
              xlab="", ylab="price (USD)", col="blue", xaxt="n")
      
      axis.POSIXct(1, x=as.POSIXct2(seq(par("xaxp")[1], par("xaxp")[2], length=2+par("xaxp")[3])), 
                   format="%m/%d %H:%M")
      
      legend("bottomright", lwd=1, col=c("blue", "red", "forestgreen"), bty="n",
             cex=0.75, leg=c("LUNO last trade***", paste(c("COINDESK", "BITFINEX"), "last trade")))
      
      new_plot <- FALSE
      par_current <- list("xlims"=c(range(dat_p$timestamp), dat_p$timestamp[1] + PLOT_NEW_WIN_SIZE), 
                          "ylim"=c(0.99, 1.01)*range(dat_vals))
    }
    else {
      ## If not new plot, only plot new data points
      ###############################################
      
      dat_p <- tail(dat_p, 2)
      lines(dat_p$timestamp, dat_p$luno_USD_last_trade, col="blue", lwd=2)
    }
    
    lines(dat_p$timestamp, dat_p$coindesk_USD_rate, col="red", lwd=2)
    lines(dat_p$timestamp, dat_p$bitfinex_USD_last_price, col="forestgreen", lwd=2)
    
    
    ## Determine if new plot is necessary by
    ## checking x and y axes range limits
    ###############################################
    
    if(!new_plot) {
      par_current$xlims[2] <-tail(dat_p$timestamp, 1)
      
      difft1 <- difftime(par_current$xlims[2], par_current$xlims[1], units="sec")
      difft2 <- difftime(par_current$xlims[3], par_current$xlims[1], units="sec")
      propt <- as.num(difft1)/as.num(difft2)
      
      if(propt > PLOT_NEW_FILLED_PROP)
        new_plot <- TRUE
      
      if((min(dat_vals) <= par_current$ylim[1]) | (max(dat_vals) >= par_current$ylim[2]))
        new_plot <- TRUE
    }
    
    Sys.sleep(5)
    
  }
}


btcRealTimeVisualiser(LUNO_USD_ADJUST=500)