source("../useful_funs.r")
library(mclust)

load("CAST_rr_list_compensated.rdata")

cols <- c("blue", "purple", "forestgreen", "red")
i <- 1

splitInto10Min <- function(x)
{
  t_max <- max(x$t)
  t_breaks <- cut(x$t, c(seq(0, t_max, 60*10), ceiling(t_max)))
  return( split(x, t_breaks) )
}



for(i in 1:20) {
  
  ## Set up RR df  
  rr_df <- rr_list[[i]]
  rr_df <- rr_df[rr_df[,2] %in% c("N", "A", "V"), ]
  which_ndash <- which(rr_df[,2] != "N") + 1
  which_ndash <- which_ndash[which_ndash <= nrow(rr_df)]
  which_ndash <- which_ndash[rr_df[which_ndash, 2] == "N"]
  rr_df[which_ndash, 2] <- "T"
  rr_df$t <- cumsum(rr_df$RRI)/1e3
  
  # rr_10_list <- splitInto10Min(rr_df)
  # rr_poinc_list <- lapply(rr_10_list, function(z) data.frame(x=z[1:(nrow(z)-1), 1], y=z[-1, 1]))
  
  # w()
  # for(i in 1:len(rr_poinc_list)) {
  #   rr_poinc_df <- rr_poinc_list[[i]]
  #   rr_df0 <- rr_10_list[[i]]
  #   p_range <- p_range <- c(max(200, min(rr_df0[,1])), min(2000, max(rr_df0[,1])))
  #   
  #   png(paste0("./output/CAST_poincare_gmm/", format_ind(i, 4), ".png"), width=1000, height=1000)
  #   plot_g(rr_poinc_df$x, rr_poinc_df$y, cex=0.75,
  #          xlim = p_range, ylim = p_range,
  #          col = cols[match(rr_df0[,2], c("N", "T", "A", "V"))], ann=F)
  #   title(main=paste0(strsplit(names(rr_list)[i], "_")[[1]][1], " | ", round(sum(rr_df0[,1])/1000/3600, 1), " hrs"),
  #         xlab="RR(t)", ylab="RR(t+1)")
  #   invisible(dev.off())
  #   catProgress(i, len(rr_poinc_list))
  # }
  # 
  ## Poincare plot
  rr_poinc_df <- data.frame(x=rr_df$RRI[1:(nrow(rr_df)-1)], y=rr_df$RRI[-1])
  rr_poinc_cov <- sqrt(cov(rr_poinc_df$x, rr_poinc_df$y))
  
  
  png(paste0("./output/CAST_poincare_gmm/", format_ind(i, 4), ".png"), width=1000, height=1000)
  
  # w()
  plot_g(rr_poinc_df$x, rr_poinc_df$y, cex=0.75,
         xlim = p_range, ylim = p_range,
         col = cols[match(rr_df[,2], c("N", "T", "A", "V"))], ann=F)
  title(main=paste0(strsplit(names(rr_list)[i], "_")[[1]][1], " | ", round(sum(rr_df[,1])/1000/3600, 1), " hrs"),
        xlab="RR(t)", ylab="RR(t+1)")
  
  y1 <- 1000 + c(2.5, -2.5)*rr_poinc_cov
  y2 <- 100 + c(1, -1)*rr_poinc_cov
  b <- (y1 - y2)/(1000-100)
  a <- y1 - 1000*b
  abline(a=a[1], b=b[1], col="black", lwd=2)
  abline(a=a[2], b=b[2], col="black", lwd=2)
  
  invisible(dev.off())

  
  # ## GMM decomposition
  # gmm_out_g1 <- Mclust(data=rr_poinc_df, G=1)
  # gmm_out_g2 <- Mclust(data=rr_poinc_df, G=2)
  # # gmm_out_g3 <- Mclust(data=rr_poinc_df, G=3, modelNames=c("EVV"))
  # 
  # 
  # ## Plot control
  # # png(paste0("./output/CAST_poincare_gmm/", format_ind(i, 4), ".png"), width=2000, height=1000)
  # w(14,7)
  # par(mfrow=c(1, 2), mar=c(3, 3, 1, 1))
  # 
  # plot(gmm_out_g1, what="class")
  # plot(gmm_out_g2, what="class")
  
  # invisible(dev.off())
  
  catProgress(i, 20)
}



## Create diagonal elliptal multivariate Gaussian
####################################################

# library(MASS)
# xy <- mvrnorm(10000, mu=c(0,0), Sigma=matrix(c(10, 8,
#                                                8, 10), nrow=2, byrow=T))
# 
# w(8,8) ; plot_g(xy[,1], xy[,2])
# abline(h=0, v=0, lty=2) ; abline(a=0, b=1, lty=2)






