source("../useful_funs.r")
source("../useful_funs_ggplot.r")
library(rhdf5)

options(digits.secs=3)

load("CAST_rr_list_compensated.rdata")


##########################################################################
#####  Summary stats of arrhythmia prevalence  ###########################
##########################################################################

wake_list <- vector("list", len(rr_list))
sleep_list <- vector("list", len(rr_list))
for(i in 1:len(rr_list)) {
  fname_h5 <- paste0("~/Data/CAST/", substring(names(rr_list)[i], 1, 4), ".h5")
  tabname <- c("baseline", "suppression")[match(substring(names(rr_list)[i], 5, 5), c("a", "b"))]
  meta <- h5read(fname_h5, paste0("meta_", tabname), compoundAsDataFrame=TRUE)$table[,-1]

  z_sleep_start <- strptime(paste(meta[3, 2], "23:30:00"), "%d/%m/%Y %H:%M:%OS")
  z_sleep_stop <- z_sleep_start + 7*3600
  
  rr_df <- rr_list[[i]]
  rr_dt <- round(cumsum(rr_df[,1])/1000, 3)
  rr_dt <- rr_dt + strptime(paste(meta[3:4, 2], collapse=' '), "%d/%m/%Y %H:%M:%OS")
  
  which_sleep <- which((rr_dt > z_sleep_start) & (rr_dt < z_sleep_stop))
  if(len(which_sleep) > 0) {
    ann_wake <- rr_df[-which_sleep, 2]
    ann_sleep <- rr_df[which_sleep, 2]
  }
  else {
    ann_wake <- rr_df[,2]
    ann_sleep <- NA
  }
  
  wake_list[[i]] <- table(as.factor(c(c("|", "A", "N", "Q", "V"), ann_wake))[-c(1:5)])
  sleep_list[[i]] <- table(as.factor(c(c("|", "A", "N", "Q", "V"), ann_sleep))[-c(1:5)])
  
  catProgress(i, len(rr_list))
}


## Calculate distributional and hourly stats
###############################################

calcTableStats <- function(tab, hrs=24)
{
  tab_prop <- tab[-which("|" == names(tab))]/sum(tab[-which("|" == names(tab))])
  
  wa <- match(c("A", "V"), names(tab))
  phrs <- c(tab[wa[1]], tab[wa[2]], "combined"=sum(tab[wa]))/hrs
  
  return(list("DISTRIBUTION"=round(100*tab_prop, 2),
              "PREV_PER_HOUR"=round(phrs, 1)))
}

# Get session lengths
session_lens <- unl(lapply(rr_list, function(x) sum(x[,1])/(36e5)))

# Per-subject stats
wake_df <- unl(calcTableStats(wake_list[[1]], session_lens[1]-6.5))
sleep_df <- unl(calcTableStats(sleep_list[[1]], 6.5))
for(i in 2:len(rr_list)) {
  wake_df <- rbind(wake_df, unl(calcTableStats(wake_list[[i]], session_lens[i]-6.5)))
  if(!(sleep_list[[i]] %=% 0))
    sleep_df <- rbind(sleep_df, unl(calcTableStats(sleep_list[[i]], 6.5)))
  else
    sleep_df <- rbind(sleep_df, NA)
}

# Average per subject
round(apply(wake_df, 2, mean), 2)
round(apply(na.omit(sleep_df), 2, mean), 2)

which_na <- which(apply(sleep_df, 1, function(x) any(is.na(x))))

w() ; boxplot(cb("wake"=wake_df[-which_na,1], "sleep"=sleep_df[-which_na,1]), 
              col="lightgreen", ylim=c(0,2.5), ylab="%")#,
              # main="Distribution of atrial premature beats per session")
savePlot("./output/dist_atrial_per_session.png", "png")

w() ; boxplot(cb("wake"=wake_df[,4], "sleep"=sleep_df[,4]), 
              col="pink", ylim=c(0,5), ylab="%")#,
              # main="Distribution of premature ventricular contractions per session")
savePlot("./output/dist_ventricular_per_session.png", "png")

w() ; boxplot(cb("wake"=wake_df[,7], "sleep"=sleep_df[,7]), 
              col="lightblue", ylim=c(0,150), ylab="occurences per hour")#,
              # main="Distribution of all arrhythmia occurences per session")
savePlot("./output/dist_all_per_session.png", "png")

# Correlations
for(j in c(1, 4:7))
  print(cor(wake_df[-which_na, j], sleep_df[-which_na, j]))


t.test(x=wake_df[-which_na, 2], y=sleep_df[-which_na, 2], 
       alternative="greater", paired=T)


##########################################################################
#####  Plotting tachograms of recorded sessions  #########################
##########################################################################


for(i in 1:len(rr_list)) {
  fname_h5 <- paste0("~/Data/CAST/", substring(names(rr_list)[i], 1, 4), ".h5")
  tabname <- c("baseline", "suppression")[match(substring(names(rr_list)[i], 5, 5), c("a", "b"))]
  meta <- h5read(fname_h5, paste0("meta_", tabname), compoundAsDataFrame=TRUE)$table[,-1]
  z <- strptime(paste(meta[3:4, 2], collapse=' '), "%d/%m/%Y %H:%M:%OS")
  
  z_sleep_start <- strptime(paste(meta[3, 2], "23:30:00"), "%d/%m/%Y %H:%M:%OS")
  z_sleep_stop <- z_sleep_start + 7*3600
  
  rr_df <- rr_list[[i]]
  
  rr_dt <- round(cumsum(rr_df[,1])/1000, 3)
  rr_dt <- z + rr_dt
  
  png(paste0("./output/CAST_daily/", format_ind(i, 4), ".png"), width=2000, height=800)
  
  plot_g(rr_dt, rr_df[,1], ylim=c(0, 2500), type="n", ylab="RRI (ms)", xlab="",
         main=substring(names(rr_list[i]), 1, 5))
  abline(v=rr_dt[(rr_dt > z_sleep_start) & (rr_dt < z_sleep_stop)], col="gray50")
  abline(v=rr_dt[rr_df[,2] %in% c("A")], col="forestgreen", lwd=2)
  abline(v=rr_dt[rr_df[,2] %in% c("V")], col="red", lwd=2)
  lines(rr_dt, rr_df[,1], col="blue", lwd=2)
  axis(3, at=as.num(c(z_sleep_start, z_sleep_stop)), labels=FALSE)
  axis(3, line=-1, at=as.num(z_sleep_start + 3.5*3600), tick=FALSE, labels="normal sleep hours")
  legend("topleft", lwd=1, col=c("forestgreen", "red"), bg="white",
         leg=c("atrial prem beat", "prem ventricular contr"), cex=0.75)
  
  invisible(dev.off())
  
  catProgress(i, len(rr_list))
}



