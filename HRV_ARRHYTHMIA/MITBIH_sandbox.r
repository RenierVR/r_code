source("../useful_funs.r")
library(rhdf5)

h5list <- getDataFromDir("~/Downloads/MIT-BIH/", ext="h5", dataE=FALSE)


h5contents(h5list[1])

fs <- 360



######################################
##  Test characteristics of sample  ##
######################################


res <- vector("list", len(h5list))
names(res) <- basename(h5list)
for(i in 1:len(h5list)) {
  dat_sig <- h5read(h5list[i], "signals_ECG_d", compoundAsDataFrame=T)$table[, -1]
  dat_ann <- cb(h5read(h5list[i], "annotation_sample_locations", compoundAsDataFrame=T)$table[, -1],
                h5read(h5list[i], "annotation_symbols", compoundAsDataFrame=T)$table[, -1],
                h5read(h5list[i], "annotation_aux_notes", compoundAsDataFrame=T)$table[, -1])
  dat_peak <- list("peaks" = h5read(h5list[i], "peaks_locations", compoundAsDataFrame=T)$table[, -1],
                   "peaks_corrected" = h5read(h5list[i], "peaks_locations_corrected", compoundAsDataFrame=T)$table[, -1])
  

  i_ann <- as.num(dat_ann[,1])
  i_ann_mat <- t(sapply(i_ann, function(x) (x-50):(x+50)))
  i_pks <- dat_peak$peaks_corrected
  
  res[[i]] <- list("i_pks" = i_pks, "i_ann" = i_ann, "i_ann_mat" = i_ann_mat)
  
  catProgress(i, len(h5list))
  
}



# for(r in 1:len(res)) {
#   i_pks <- res[[r]]$i_pks
#   i_ann_mat <- res[[r]]$i_ann_mat
# 
#   i_match <- numeric(len(i_pks))
#   for(i in 1:len(i_pks)) {
#     is_in_mat <- apply(i_ann_mat, 1, function(x) i_pks[i] %in% x)
#     if(!any(is_in_mat))
#       i_match[i] <- NA
#     else if(len(which(is_in_mat)) > 1) {
#       i_match[i] <- which(is_in_mat)[1]
#       cat("Multiple matches: row", r, "peak", i, "\n")
#     }
#     else
#       i_match[i] <- which(is_in_mat)
# 
#   }
# 
#   res[[r]] <- c(res[[r]], "i_match"=list(i_match))
#   catProgress(r, len(res))
# }

# Multiple matches: row 15 peak 1776
# res_list <- res
# save(res_list, file="./res_list.rdata")
load("res_list.rdata")


# res <- res_list[[2]]
# 
# res$i_pks[which(is.na(res$i_match))]/fs
# round(res$i_ann[-na.omit(res$i_match)]/fs,5)


###########################################
##  Plot signals, annotations and peaks  ##
###########################################

i <- 10

## Read data from H5 file
dat_sig <- h5read(h5list[i], "signals_ECG_d", compoundAsDataFrame=T)$table[, -1]
dat_ann <- cb(h5read(h5list[i], "annotation_sample_locations", compoundAsDataFrame=T)$table[, -1],
              h5read(h5list[i], "annotation_symbols", compoundAsDataFrame=T)$table[, -1],
              h5read(h5list[i], "annotation_aux_notes", compoundAsDataFrame=T)$table[, -1])
dat_peak <- list("peaks" = h5read(h5list[i], "peaks_locations", compoundAsDataFrame=T)$table[, -1],
                 "peaks_corrected" = h5read(h5list[i], "peaks_locations_corrected", compoundAsDataFrame=T)$table[, -1])


## Create structure to store peaks, RRIs and annotations
res <- res_list[[i]]
rr_df <- data.frame("peak"=res$i_pks/fs, 
                    "ann_x"=res$i_ann[res$i_match]/fs,
                    "ann"=dat_ann[res$i_match, 2], stringsAsFactors=FALSE)

## Check that peak annotation locations are negligible
cat(res$i_ann %=% as.num(dat_ann[,1]), "\n")
w() ; lplot_g(rr_df$peak - rr_df$ann_x)

## Find peaks with no annotations, and vice versa
( pks_sans_ann <- res$i_pks[which(is.na(res$i_match))] )
( ann_sans_pks <- res$i_ann[-na.omit(res$i_match)] )

## Try to find matching peaks for 'pks_sans_ann'
which_pk_match <- NA
if(len(pks_sans_ann) > 0) {
  which_pk_match <- apply(sapply(pks_sans_ann, function(x) abs(x - ann_sans_pks)), 2, 
                          function(y) c(min(y), which.min(y)))
  cat("Diff-index matrix pre-match:\n")
  print(which_pk_match)
  
  for(j in 1:ncol(which_pk_match))
    if(which_pk_match[1, j] < fs) {
      rr_df[which(res$i_pks == pks_sans_ann[j]), ]$ann_x <- ann_sans_pks[which_pk_match[2, j]]/fs
      rr_df[which(res$i_pks == pks_sans_ann[j]), ]$ann <- dat_ann[which(res$i_ann == ann_sans_pks[which_pk_match[2, j]]), 2]
      which_pk_match[1, j] <- 0
    }
  
  cat("\nDiff-index matrix post-match:\n")
  print(which_pk_match)
}

## Review peaks matched to annotations in second stage
print(rr_df[match(pks_sans_ann, res$i_pks), ])


## Review remaining annotations not corresponding to peaks
if(!all(is.na(which_pk_match)))
  ann_sans_pks <- ann_sans_pks[-which_pk_match[2, which_pk_match[1,] == 0]]
ann_other_df <- data.frame("ann_x"=ann_sans_pks/fs, "ann"=dat_ann[match(ann_sans_pks, res$i_ann), 2])
print(ann_other_df)


## Amend any remaing unannotated peaks
if(sum(apply(rr_df, 1, function(x) any(is.na(x)))) > 0) {
  w_na <- which(apply(rr_df, 1, function(x) any(is.na(x))))
  rr_df$ann_x[w_na] <- rr_df$peak[w_na]
  rr_df$ann[w_na] <- "?"
}

## Add RR to finalised DF
rr_df$RRI <- c(0, 1000*diff(rr_df$peak))


## Plot data
scrollPlot(start=1, winsize=fs*25, overlap=fs*25*3/4, n=nrow(dat_sig), 
           plotfun=function(xi){

  # w()             
  # Current plot window logistics
  w_idx <- which(round(rr_df$peak*fs, 0) %in% xi)
  w_idx_ann <- which(round(ann_other_df$ann_x*fs, 0) %in% xi)
  
  par(mar=c(0.5,5,0,1), oma=c(5,0,4,0))
  layout(c(1, 2), heights=c(2/3, 1/3))
  
  # First plot (ECG, peaks, annotations)
  plot_g(xi/fs, dat_sig[xi, 1], xaxt="n", ylab="ECG", type="n")
  
  abline(v=rr_df$peak[w_idx], lwd=2, lty=1, col="mediumseagreen")
  # abline(v=pks_sans_ann, lwd=2, col="blue")
  
  cols <- rep("orange", len(w_idx))
  cols[rr_df$ann[w_idx] != "N"] <- "red"
  mtext(rr_df$ann[w_idx], side=3, at=rr_df$ann_x[w_idx], col=cols)
  abline(v=rr_df$ann_x[w_idx], lwd=2, lty=2, col=cols)
  
  if(len(w_idx_ann) > 0) {
    mtext(ann_other_df$ann[w_idx_ann], side=3, at=ann_other_df$ann_x[w_idx_ann], col="blue")
    abline(v=ann_other_df$ann_x[w_idx_ann], lwd=3, col="blue")
  }
  
  lines(xi/fs, dat_sig[xi, 1], col="black", lwd=2)
  
  # Second plot (RRIs)
  plot_g(xi/fs, rep(0, len(xi)), ylab="RRI (ms)", ylim=range(rr_df$RRI[w_idx]), type="n")
  
  abline(v=rr_df$ann_x[w_idx], lwd=2, lty=2, col=cols)
  if(len(w_idx_ann) > 0)
    abline(v=ann_other_df$ann_x[w_idx_ann], lwd=3, col="blue")
  
  points(rr_df$peak[w_idx], rr_df$RRI[w_idx], pch=19, cex=0.75)
  lines(rr_df$peak[w_idx], rr_df$RRI[w_idx])
  title(xlab="time (s)", xpd=NA)
  
}, fig_path=paste0("output/fig/patient", format_ind(i), "/ecg_rr_patient", format_ind(i), "_"))

print(ann_other_df)


##########################################################################
##########################################################################
# patient02:  175s (misclassified beat)
#             320s (missed beat)
#
# patient04:  1095s (missed beat)
#
# patient05:  855s (missed beat)
#             1010s (missed beat)
#             1470s (missed beat)
#             1690s (missed beat)
#
# patient06:  630s (misclassified beat / missed beat)
#             1030s (missed beat)
#             1205s (misclassified beat / missed beat)
#             1315s (misclassified beat / missed beat)
#             1655s (misclassified beat / missed beat)
#
# patient07:  1000s (missed beat)
#
# patient08:  755s (missed beat)
#             1735s (missed beat)
#             1795s (missed beat)
#
# patient09:  820s (missed beat)
#             930s (missed beat)
#             1215s (missed beat)
#             1655s (missed beat)
#             1680s (missed beat)
#             1705s (missed beat)
#             1720s (missed beat)
#
# patient10:  1455s (missed beat)
#             1805s (missed beat)


##########################################################################
# OTHER NOTES:
# - remove non-beat peaks before RR calc
# - wean pks_sans_ann matched, perhaps use timestamp of anno, not beat?

