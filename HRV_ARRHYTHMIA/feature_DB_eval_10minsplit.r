source("../useful_funs.r")

load("MITBIHNSR_rr_list.rdata")
load("CAST_rr_list_compensated.rdata")
CAST_rr_list <- rr_list

valid_RR_symbols <- c("N", "L", "R", "B", "A", "a", "J", "S", "V", "r", "F", "e", "j", "n", "E", "/", "f")
valid_RR_bounds <- c(100, 3000)

##########################################################################

CAST_rr_list <- lapply(CAST_rr_list, function(x){ x$t <- cumsum(x$RRI)/1000 ; return(x) })
# CAST_rr_list <- lapply(CAST_rr_list, function(x) x[x$ann %in% c(valid_RR_symbols, "Q"), ])
MITBIHNSR_rr_list <- lapply(MITBIHNSR_rr_list, function(x){ x$t <- x$t - x$t[1] + x$RRI[1]/1000 ; return(x) })

rr_list <- c(MITBIHNSR_rr_list, CAST_rr_list)
rm(MITBIHNSR_rr_list, CAST_rr_list) ; gc()

##########################################################################

splitInto10Min <- function(x)
{
  t_max <- max(x$t)
  t_breaks <- cut(x$t, c(seq(floor(min(x$t)), t_max, 60*10), ceiling(t_max)))
  return( split(x, t_breaks) )
}



generateFeature_DB <- function(rr, rrv, const_var,
                               BOUND_X1=100, BOUND_X2=1000, BOUND_X3=2000,
                               BOUND_SD1=1, BOUND_SD2=3, BOUND_SD3=6)
{ ## Generates "decision bounds" feature from vector of RRIs 'rr'
  ##   by constructing linear bounds based on covariance in 
  ##   Poincare plot and naively counting points outside
  ##   these bounds as anomalous.
  
  rr_poinc_df <- data.frame(x=rr[1:(len(rr)-1)], y=rr[-1])
  rr_poinc_df_invalid <- sort(c(which(rrv == 0), which(rrv == 0)-1))
  if(len(rr_poinc_df_invalid) > 0)
    rr_poinc_df <- rr_poinc_df[-sort(c(which(rrv == 0), which(rrv == 0)-1)), ]
  
  if(missing(const_var)) 
    rr_poinc_mad <- sqrt(median((rr_poinc_df$x-mean(rr_poinc_df$x))^2))
  else
    rr_poinc_mad <- const_var
  
  getCorrespondingPoints <- function(x1, a)
  {
    y1 <- x1 - a
    x2 <- (y1 - (x1+a))/2 + x1
    return(cb(c(x1, y1), c(x2, x1)))
  }
  
  getLineFromPoints <- function(xy1, xy2)
  {
    b <- (xy2[2]-xy1[2])/(xy2[1]-xy1[1])
    a <- xy1[2] - b*xy1[1]
    return(c(a, b))
  }
  
  p1 <- getCorrespondingPoints(BOUND_X1, BOUND_SD1*rr_poinc_mad)
  p2 <- getCorrespondingPoints(BOUND_X2, BOUND_SD2*rr_poinc_mad)
  p3 <- getCorrespondingPoints(BOUND_X3, BOUND_SD3*rr_poinc_mad)
  
  lower_ab1 <- getLineFromPoints(p1[,1], p2[,1])
  lower_ab2 <- getLineFromPoints(p2[,1], p3[,1])
  upper_ab1 <- getLineFromPoints(p1[,2], p2[,2])
  upper_ab2 <- getLineFromPoints(p2[,2], p3[,2])
  
  if(upper_ab2[2] < 0)
    upper_ab2 <- c(max(valid_RR_bounds), 0)
  
  f_lower <- function(x)
  {
    if(x <= p2[1,1])
      return( lower_ab1[1] + x*lower_ab1[2])
    else
      return( lower_ab2[1] + x*lower_ab2[2])
  }
  
  f_upper <- function(x)
  {
    if(x <= p2[1,2])
      return( upper_ab1[1] + x*upper_ab1[2])
    else
      return( upper_ab2[1] + x*upper_ab2[2])
  }
  
  xx <- seq(0, 3000, 1)
  
  return(list("poincare_df"=rr_poinc_df, "poincare_var"=sqrt(median((rr_poinc_df$x-mean(rr_poinc_df$x))^2)),
              "lines"=cb(xx, sapply(xx, f_upper), sapply(xx, f_lower)),
              "in_DB"=apply(rr_poinc_df, 1, function(x) as.num(isIn(x[2], c(f_lower(x[1]), f_upper(x[1]))))) ))
}





##########################################################################
#####  Apply feature generation  #########################################
##########################################################################

DB_eval_list <- vector("list", len(rr_list))
rng <- 1:len(rr_list)
for(i in rng) {
  # for(i in c(1:20, 221:223)) {
  
  rr_df_split10 <- splitInto10Min(rr_list[[i]])
  
  featureDB_seg_list <- vector("list", len(rr_df_split10))
  for(j in 1:len(featureDB_seg_list)) {
    rr_df <- rr_df_split10[[j]]
    
    if(nrow(rr_df) < 10)
      next
    
    ## Indicate validity based on annotation or RRI magnitude
    rr_df$RRV <- as.num(rr_df$ann %in% valid_RR_symbols)
    rr_df$RRV[rr_df$RRI > max(valid_RR_bounds)] <- 0
    rr_df$RRV[sum(rr_df$RRI < min(valid_RR_bounds))] <- 0
    
    which_rrv0 <- which(rr_df$RRV == 0)
    rr_df$RRV[which_rrv0[which_rrv0 < nrow(rr_df)] + 1] <- 0
    
    ## Adjust annotations of RRIs after arrhythmias
    which_ndash <- which(rr_df$ann != "N") + 1
    which_ndash <- which_ndash[which_ndash <= nrow(rr_df)]
    which_ndash <- which_ndash[rr_df[which_ndash, 2] == "N"]
    rr_df[which_ndash, 2] <- "N-"
    
    ## Generate Poincare Decision Bound feature
    feature_DB <- generateFeature_DB(rr=rr_df$RRI, rrv=rr_df$RRV, 65)
    
    featureDB_seg_list[[j]] <- list("ann_df"=rr_df[, -match(c("RRI", "t"), names(rr_df))],
                                    "feature_DB"=table(feature_DB$in_DB))
    
    
    ## Plot
    # png(paste0("./output/feature_DB_eval/", format_ind(i, 4), "_", format_ind(j, 4), ".png"), width=1000, height=1000)
    # 
    # cols <- rep("blue", nrow(rr_df))
    # cols[rr_df$ann == "N-"] <- "forestgreen"
    # cols[!(rr_df$ann %in% c("N", "N-"))] <- "red"
    # 
    # cols_inv <- c(which(rr_df$RRV == 0), which(rr_df$RRV == 0)+1)
    # if(len(cols_inv) > 0) cols <- cols[-cols_inv]
    # 
    # # w(8,8)
    # plot_g(feature_DB$poincare_df$x, feature_DB$poincare_df$y, cex=0.75, col=cols,
    #        xlim=c(200, 2000), ylim=c(200, 2000), pch=c(1, 19)[feature_DB$in_DB+1])
    # abline(a=0, b=1, lty=2)
    # lines(feature_DB$lines[,1], feature_DB$lines[,2], lwd=2, col="black")
    # lines(feature_DB$lines[,1], feature_DB$lines[,3], lwd=2, col="black")
    # mtext(paste("variation =", round(feature_DB$poincare_var, 2)))
    # 
    # invisible(dev.off())
    
    
  }
  
  res_agg <- lapply(featureDB_seg_list, function(x) {
    if(is.null(x))
      return( c("abn"=0, "all"=0, "DB_0"=0, "DB_1"=0) )
    
    val <- x$ann_df$ann[x$ann_df$RRV == 1]
    if(is.null(names(x$feature_DB)))
      x2 <- c(0, 0)
    else if(len(x$feature_DB) == 2)
      x2 <- as.num(x$feature_DB)
    else if(names(x$feature_DB) == "1")
      x2 <- c(0, as.num(x$feature_DB))
    else if(names(x$feature_DB) == "0")
      x2 <- c(as.num(x$feature_DB), 0)
    else
      stop("something is wrong here")
    
    return( c("abn"=sum(val != "N"), "all"=len(val), 
              "DB_0"=x2[1], "DB_1"=x2[2]) )
  })
  
  res_agg <- matrix(unl(res_agg), ncol=4, byrow=T)
  colnames(res_agg) <- c("abn", "all", "DB_0", "DB_1")
  
  DB_eval_list[[i]] <- res_agg
  
  catProgress(i+1-min(rng), max(rng))
}



##########################################################################
#####  Evaluate feature  #################################################
##########################################################################


res_mat <- matrix(unl(lapply(lapply(DB_eval_list, colSums), 
                             function(x) 100*c(x[1]/x[2], x[3]/x[4]) )), ncol=2, byrow=T)

cor_full(res_mat)





