source("../useful_funs.r")
source("../useful_funs_ggplot.r")


###############################################################################
####  Load and format new output data
###############################################################################

fpath <- "~/Data/MMI_2018_05_11/"
flist <- list.files(fpath, rec=T, full=T)
flist <- flist[grep("output", flist)]
flist_raw <- gsub("_output", "", flist)

# Read output data in list
out_list <- list("vector", len(flist))
for(i in 1:len(flist))
  out_list[[i]] <- list("HRV_output" = h5read2(flist[i], "/metric/health_assessment/heart/hrv_features"),
                       "PDB_output" = h5read2(flist[i], "/metric/health_assessment/heart/poincaredb_features"))

names(out_list) <- unl(lsel(strsplit(unl(strsplit(basename(flist), ".h5")), "_"), 1))




###############################################################################
####  Get raw RR data
###############################################################################

i <- 1

# Read and reformat input data for subject 'i'
dat <- cb("a"=h5read2(flist_raw[i], "/metric/on_device/rr"),
          "b"=h5read2(flist_raw[i], "/metric/on_device/rr_isvalid")[,-1])
colnames(dat) <- c("index", "RR", "RRV")

# Continuous segment calculation
t_diffs <- 1000*diff(dat$index)
rr_diffs <- as.integer(dat$RR[-nrow(dat)] - t_diffs)

rr_seg_ind <- c(0, abs(rr_diffs) < 3000)

# Construct matrix of segment starts/ends
seg_mat <- data.frame("start" = which(rr_seg_ind == 0),
                      "end" = c(which(rr_seg_ind == 0)[-1] - 1, len(rr_seg_ind)))
seg_mat <- seg_mat[apply(seg_mat, 1, function(x) diff(dat$index[as.num(x)])) >= 5, ]
seg_mat$col <- rep(c("darkolivegreen2", "darkolivegreen1"), ceiling(nrow(seg_mat)/2))[1:nrow(seg_mat)]

rm(t_diffs, rr_diffs, rr_seg_ind)

# Reformat output data for subject 'i'
out <- out_list[[i]]
out$PDB_output$ratio <- out$PDB_output$db_within/out$PDB_output$db_all


format_pdb <- function(out_pdb_row)
{
  f1 <- paste0(out_pdb_row$db_within, "/", out_pdb_row$db_all)
  f2 <- paste0(f1, " (", as.char(round(out_pdb_row$ratio, 2)), ")")
  return( paste0("PDB: ", f2) )
}

format_hrv <- function(out_hrv_row)
{
  f1 <- paste0("HRV: mnn=", round(out_hrv_row$mnn, 0))
  f2 <- paste0("pnn20=", round(out_hrv_row$pnn20, 1), "; pnn50=", round(out_hrv_row$pnn50, 1))
  f3 <- paste0("rmssd=", round(out_hrv_row$rmssd, 0), "; sdnn=", round(out_hrv_row$sdnn, 0))
  paste(f1, f2, f3, sep="\n")
}



###############################################################################
####  Scroll plotting of RR values
###############################################################################

HRV_winsize = 120

# Set plotting parameters
winsize <- 15*60
overlap <- winsize/2
fig_path="./output/preprocessor_debugging/"
start=dat$index[1]
n=dat$index[1] + 1*24*3600

# Start plotting routine
j <- 0
last <- FALSE

if(winsize > n) winsize <- n
if(overlap > winsize) overlap <- winsize

winsize <- as.integer(winsize)
overlap <- as.integer(overlap)

while(TRUE) {
  j = j+1
  xi <- (overlap*(j-1)+start):(start-1+winsize + overlap*(j-1))
  
  if(max(xi) >= floor(n)) {
    last <- TRUE
    xi <- xi[1]:n
  }
  
  
  which_i <- which((dat$index >= xi[1]) & (dat$index <= tail(xi, 1)))
  if(len(which_i) > 0) {
    
    png(paste0(fig_path, format_ind(j, 4), ".png"), width=1400, height=900)
    
    ## Ready data for plot
    #######################  
    
    pdat <- data.frame("x"=as.POSIXct2(dat$index[which_i]), 
                       "y"=dat$RR[which_i],
                       "v"=dat$RRV[which_i])
    
    which_seg1 <- which(seg_mat$start > which_i[1])[1] - 1
    which_seg2 <- tail(which(seg_mat$start < tail(which_i, 1)), 1)
    pdat_seg <- seg_mat[which_seg1:which_seg2, ]
    
    which_pdb <- which((out$PDB_output$index >= xi[1]) & (out$PDB_output$index <= tail(xi, 1)))
    which_hrv <- which((out$HRV_output$index >= xi[1]) & (out$HRV_output$index <= tail(xi, 1)))
    
    
    ## Plotting code
    ####################    
    
    # w()
    par(mar=c(5, 4, 7, 2))
    plot(pdat$x, pdat$y, type="n", ylim=c(300, 2300), xlim=as.POSIXct2(range(xi)),
         xlab=gsub(" ", " | ", pdat$x[1]), ylab="RR (ms)")
    for(seg in 1:nrow(pdat_seg))
      rect(xleft=as.POSIXct2(dat$index[as.num(pdat_seg[seg, 1])]), ybottom=par("usr")[3],
           xright=as.POSIXct2(dat$index[as.num(pdat_seg[seg, 2])]), ytop=par("usr")[4],
           col=pdat_seg$col[seg], border=NA)
    
    if(len(which_pdb) > 0)
      for(wpdb in which_pdb) {
        mtext(format_pdb(out$PDB_output[wpdb,]), at=out$PDB_output$index[wpdb])
        abline(v=out$PDB_output$index[wpdb], lty=4, lwd=2)
      }
    
    if(len(which_hrv) > 0)
      for(whrv in which_hrv) {
        mtext(format_hrv(out$HRV_output[whrv,]), at=out$HRV_output$index[whrv] + HRV_winsize/2, line=2)
        rect(xleft=out$HRV_output$index[whrv], ybottom=par("usr")[3],
             xright=out$HRV_output$index[whrv] + HRV_winsize, ytop=par("usr")[4],
             density=5, col="grey50")
      }
    
    lines(pdat$x, pdat$y, col="blue", lty=3)
    points(pdat$x, pdat$y, pch=19, col=c("red", "blue")[pdat$v + 1])
  
    
    ####################
    
    invisible(dev.off())
    
  }
  
  if(j == 1) cat("Processing", floor((n - start)/overlap), "total windows...\n")
  catProgress(j, floor((n - start)/overlap))
  
  if(last) break
  
}





###############################################################################
####  Scroll plotting of HRV values
###############################################################################

hrv_temp <- out$HRV_output
hrv_pdb <- out$PDB_output

# Set plotting parameters
winsize <- 2*60*60
overlap <- winsize/2
fig_path <- "./output/preprocessor_debugging/"
start <- min(hrv_temp$index[1], hrv_pdb$index[1])
n = start + 1*24*3600


# Function to calculate hours in plot window
get_hour_inds <- function(xi)
{
  t0 <- as.POSIXct2(xi[1])
  t0_split <- strsplit(as.char(t0), " ")[[1]]
  t0n <- as.num(as.POSIXct2(paste(t0_split[1], paste0(format(t0, "%H"), ":00:00"))))
  return(seq(t0n, tail(xi, 1), 3600))
}

# Function to fill plot bg and draw ticklines
plot_fill_ticklines <- function(xi, bg_col="grey20")
{
  rect(par("usr")[1], par("usr")[3], par("usr")[2], par("usr")[4], col=bg_col, border=NA)
  par_y <- par("yaxp")
  abline(h=seq(par_y[1], par_y[2], len=(par_y[3]+1)), lty=3, col="white")
  abline(v=get_hour_inds(xi), lty=3, col="white")
}


# Determine y-limits
ylm_temp_nn <- range(c(hrv_temp$mnn))
ylm_temp_sd <- range(c(hrv_temp$rmssd, hrv_temp$sdnn))
ylm_temp_pnn <- range(c(hrv_temp$pnn20, hrv_temp$pnn50))
ylm_pdb_freq <- range(c(hrv_pdb$db_all, hrv_pdb$db_within))


# Start plotting routine
j <- 0
last <- FALSE

if(winsize > n) winsize <- n
if(overlap > winsize) overlap <- winsize

winsize <- as.integer(winsize)
overlap <- as.integer(overlap)

while(TRUE) {
  j = j+1
  xi <- (overlap*(j-1)+start):(start-1+winsize + overlap*(j-1))
  
  if(max(xi) >= floor(n)) {
    last <- TRUE
    xi <- xi[1]:n
  }
  
  
  which_i_temp <- which((hrv_temp$index >= xi[1]) & (hrv_temp$index <= tail(xi, 1)))
  which_i_pdb <- which((hrv_pdb$index >= xi[1]) & (hrv_pdb$index <= tail(xi, 1)))
  if(max(len(which_i_temp), len(which_i_pdb)) > 0) {
    
    png(paste0(fig_path, format_ind(j, 4), ".png"), width=1400, height=900)
    
    ## Ready data for plot
    #######################  
    
    pdat_temp <- data.frame("x"=as.POSIXct2(hrv_temp$index[which_i_temp]), 
                            "y_mnn"=hrv_temp$mnn[which_i_temp],
                            "y_pnn20"=hrv_temp$pnn20[which_i_temp],
                            "y_pnn50"=hrv_temp$pnn20[which_i_temp],
                            "y_rmssd"=hrv_temp$rmssd[which_i_temp],
                            "y_sdnn"=hrv_temp$sdnn[which_i_temp])
    
    pdat_pdb <- data.frame("x"=as.POSIXct2(hrv_pdb$index[which_i_pdb]), 
                           "y_dball"=hrv_pdb$db_all[which_i_pdb],
                           "y_dbwithin"=hrv_pdb$db_within[which_i_pdb],
                           "y_dbratio"=hrv_pdb$ratio[which_i_pdb])
    
    
    ## Plotting code
    ####################    
    
    w()
    par(mfrow=c(3, 1), mar=c(2,5,0,4), oma=c(5,0,1,0))
    
    plot(pdat_temp$x, pdat_temp$y_mnn, type="n", xlim=as.POSIXct2(range(xi)),
         xlab="", xaxt="n", ylab="RR (ms)", ylim=ylm_temp_nn)
    plot_fill_ticklines(xi)
    points(pdat_temp$x, pdat_temp$y_mnn, col="green", pch=19)
    lines(pdat_temp$x, pdat_temp$y_mnn, col="green", lty=2)
    par(new=T)
    plot(pdat_temp$x, pdat_temp$y_rmssd, type="n", xlim=as.POSIXct2(range(xi)),
         ann=F, yaxt="n", xaxt="n", ylim=ylm_temp_sd)
    points(pdat_temp$x, pdat_temp$y_rmssd, pch=19, col="mediumseagreen")
    lines(pdat_temp$x, pdat_temp$y_rmssd, pch=19, col="mediumseagreen", lty=2)
    points(pdat_temp$x, pdat_temp$y_sdnn, pch=19, col="seagreen")
    lines(pdat_temp$x, pdat_temp$y_sdnn, pch=19, col="seagreen", lty=2)
    axis(4)
    mtext("SD RR (ms)", 4, line=3, cex=par()$cex)
    
    plot(pdat_temp$x, pdat_temp$y_pnn20, type="n", xlim=as.POSIXct2(range(xi)),
         xlab="", xaxt="n", ylab="PNN %", ylim=ylm_temp_pnn)
    plot_fill_ticklines(xi)
    points(pdat_temp$x, pdat_temp$y_pnn20, col="green", pch=19)
    lines(pdat_temp$x, pdat_temp$y_pnn20, col="green", lty=2)
    points(pdat_temp$x, pdat_temp$y_pnn50, col="green", pch=19)
    lines(pdat_temp$x, pdat_temp$y_pnn50, col="green", lty=2)
    
    plot(pdat_pdb$x, pdat_pdb$y_dball, type="n", xlim=as.POSIXct2(range(xi)),
         xlab=gsub(" ", " | ", as.POSIXct2(xi[1])), ylab="Poincare DB freq", ylim=ylm_pdb_freq)
    plot_fill_ticklines(xi)
    points(pdat_pdb$x, pdat_pdb$y_dball, col="green", pch=19)
    lines(pdat_pdb$x, pdat_pdb$y_dball, col="green", lty=2)
    points(pdat_pdb$x, pdat_pdb$y_dbwithin, col="green", pch=19)
    lines(pdat_pdb$x, pdat_pdb$y_dbwithin, col="green", lty=2)
    par(new=T)
    plot(pdat_pdb$x, pdat_pdb$y_dbratio, type="n", xlim=as.POSIXct2(range(xi)),
         ann=F, yaxt="n", xaxt="n", ylim=ylm_pdb_ratio)
    points(pdat_temp$x, pdat_temp$y_dbratio, pch=19, col="mediumseagreen")
    lines(pdat_temp$x, pdat_temp$y_dbratio, pch=19, col="mediumseagreen", lty=2)
    axis(4)
    mtext("Poincare DB Ratio", 4, line=3, cex=par()$cex)
    
    # mtext()
    
    
    
    
    ####################
#     
#     invisible(dev.off())
#     
#   }
#   
#   if(j == 1) cat("Processing", floor((n - start)/overlap), "total windows...\n")
#   catProgress(j, floor((n - start)/overlap))
#   
#   if(last) break
#   
# }




