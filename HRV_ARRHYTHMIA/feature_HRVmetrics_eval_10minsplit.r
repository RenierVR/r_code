source("../useful_funs.r")

load("MITBIHNSR_rr_list.rdata")
load("CAST_rr_list_compensated.rdata")
CAST_rr_list <- rr_list

valid_RR_symbols <- c("N", "L", "R", "B", "A", "a", "J", "S", "V", "r", "F", "e", "j", "n", "E", "/", "f")
valid_RR_bounds <- c(100, 3000)

##########################################################################

CAST_rr_list <- lapply(CAST_rr_list, function(x){ x$t <- cumsum(x$RRI)/1000 ; return(x) })
# CAST_rr_list <- lapply(CAST_rr_list, function(x) x[x$ann %in% c(valid_RR_symbols, "Q"), ])
MITBIHNSR_rr_list <- lapply(MITBIHNSR_rr_list, function(x){ x$t <- x$t - x$t[1] + x$RRI[1]/1000 ; return(x) })

rr_list <- c(MITBIHNSR_rr_list, CAST_rr_list)
rm(MITBIHNSR_rr_list, CAST_rr_list) ; gc()

##########################################################################

splitInto10Min <- function(x)
{
  t_max <- max(x$t)
  t_breaks <- cut(x$t, c(seq(floor(min(x$t)), t_max, 60*10), ceiling(t_max)))
  return( split(x, t_breaks) )
}


