source("../useful_funs.r")

##########################################################################

# load("CAST_rr_list.rdata")
# rr_list <- CAST_rr_list[grep("COMPENSATED", names(CAST_rr_list))]
# save(rr_list, file="CAST_rr_list_compensated.rdata")
load("CAST_rr_list_compensated.rdata")




##########################################################################
#####  Compare baseline vs suppression with Poincare plots  ##############
##########################################################################


cols <- c("blue", "purple", "forestgreen", "red")

i <- 1
while(i <= len(rr_list)) {
  
  if(substring(names(rr_list)[i], 1, 4) != substring(names(rr_list)[i+1], 1, 4)) {
    i <- i + 1
    next
  }
  
  png(paste0("./output/CAST_poincare/", format_ind(i, 4), ".png"), width=2000, height=1000)
  par(mfrow=c(1, 2))
  
  rr_df <- rr_list[[i]]
  p_range <- c(max(200, min(rr_df[,1])), min(2000, max(rr_df[,1])))
  rr_df <- rr_df[rr_df[,2] %in% c("N", "A", "V"), ]
  which_ndash <- which(rr_df[,2] != "N") + 1
  which_ndash <- which_ndash[which_ndash <= nrow(rr_df)]
  which_ndash <- which_ndash[rr_df[which_ndash, 2] == "N"]
  rr_df[which_ndash, 2] <- "T"
  
  plot_g(rr_df[1:(nrow(rr_df)-1), 1], rr_df[-1, 1], cex=0.75,
         xlim = p_range, ylim = p_range, 
         col = cols[match(rr_df[,2], c("N", "T", "A", "V"))], ann=F)
  title(main=paste0(strsplit(names(rr_list)[i], "_")[[1]][1], " | ", round(sum(rr_df[,1])/1000/3600, 1), " hrs"), 
        xlab="RR(t)", ylab="RR(t+1)")
  
  rr_df <- rr_list[[i+1]]
  rr_df <- rr_df[rr_df[,2] %in% c("N", "A", "V"), ]
  plot_g(rr_df[1:(nrow(rr_df)-1), 1], rr_df[-1, 1], cex=0.75,
         xlim = p_range, ylim = p_range, 
         col = cols[match(rr_df[,2], c("N", "A", "V"))], ann=F)
  title(main=paste0(strsplit(names(rr_list)[i+1], "_")[[1]][1], " | ", round(sum(rr_df[,1])/1000/3600, 1), " hrs"), 
        xlab="RR(t)", ylab="RR(t+1)")
  legend("bottomright", bg="white", pch=19, col=cols, 
         leg=c("normal beat", "atrial prem beat", "prem ventricular contraction"))
  
  invisible(dev.off())
    
  i <- i + 2
  catProgress(i, len(rr_list))
}




##########################################################################
#####  Visual representation of "decision boundaries"  ###################
##########################################################################


for(i in 1:len(rr_list)) {
  
  ## Set up RR df  
  rr_df <- rr_list[[i]]
  rr_df <- rr_df[rr_df[,2] %in% c("N", "A", "V"), ]
  which_ndash <- which(rr_df[,2] != "N") + 1
  which_ndash <- which_ndash[which_ndash <= nrow(rr_df)]
  which_ndash <- which_ndash[rr_df[which_ndash, 2] == "N"]
  rr_df[which_ndash, 2] <- "T"
  rr_df$t <- cumsum(rr_df$RRI)/1e3
  
  
  ## Poincare plot
  rr_poinc_df <- data.frame(x=rr_df$RRI[1:(nrow(rr_df)-1)], y=rr_df$RRI[-1])
  rr_poinc_cov <- sqrt(cov(rr_poinc_df$x, rr_poinc_df$y))
  
  
  png(paste0("./output/CAST_poincare_gmm/", format_ind(i, 4), ".png"), width=1000, height=1000)
  
  # w()
  plot_g(rr_poinc_df$x, rr_poinc_df$y, cex=0.75,
         xlim = p_range, ylim = p_range,
         col = cols[match(rr_df[,2], c("N", "T", "A", "V"))], ann=F)
  title(main=paste0(strsplit(names(rr_list)[i], "_")[[1]][1], " | ", round(sum(rr_df[,1])/1000/3600, 1), " hrs"),
        xlab="RR(t)", ylab="RR(t+1)")
  
  y1 <- 1000 + c(2.5, -2.5)*rr_poinc_cov
  y2 <- 100 + c(1, -1)*rr_poinc_cov
  b <- (y1 - y2)/(1000-100)
  a <- y1 - 1000*b
  abline(a=a[1], b=b[1], col="black", lwd=2)
  abline(a=a[2], b=b[2], col="black", lwd=2)
  
  invisible(dev.off())
  
  catProgress(i, 20)
}


