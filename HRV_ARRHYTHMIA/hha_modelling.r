source("../useful_funs.r")

library(data.table)

dat <- fread("~/Documents/HealthQ/HHA/features_mmi_all.csv")
folds <- fread("~/Documents/HealthQ/HHA/folds.csv")


## Use predefined folds from Albert
id_match <- match(dat$guid, folds$V1)
dat <- cb(dat[which(!is.na(id_match)),], "fold"=folds$V2[na.omit(id_match)])

## Scale numerical data
# dat2 <- as.data.frame(dat)
# dat2 <- cb(dat2[, 1:3], apply(dat2[, -c(1:3, grep("QUESTION", names(dat2)))], 2, scale))

# N_FOLDS <- 5
# folds <- rep(99, nrow(dat))
# all_i <- 1:nrow(dat)
# for(i in 1:N_FOLDS) {
#   fi <- sample(all_i, floor(nrow(dat)/N_FOLDS))
#   folds[fi] <- i-1
#   all_i <- all_i[-match(fi, all_i)]
# }
# folds[all_i] <- floor(runif(len(all_i), 0, N_FOLDS - 0.0000001))
# 
# dat <- cb("fold"=folds, dat)


###############################################################################
######  Function to select data subset based on desired modelling
###############################################################################


buildLM <- function(dat, dep_var = c("Framingham", "BIO_AGE", "BIO_BMI")[1], 
                    predictors = c("gam_coeff", "rr_ave", "rr_stdev", "Poincare", "mnn", "pnn20", "pnn50", 
                                   "rmssd", "sdsd", "sdnn", "entropy", "heartrate", "lowres", "accfeat"),
                    n_folds = len(unique(dat$fold)))
{
  i_dep_var <- match(dep_var, names(dat))
  i_pred <- unl(as.list(sapply(predictors, grep, names(dat))))
  
  res_list <- vector("list", n_folds)
  for(i in 1:n_folds) {
    
    if(is.data.table(dat))
      dat_m <- dat[fold != (i-1), c(i_dep_var, i_pred), with=F]
    else
      dat_m <- dat[fold != (i-1), c(i_dep_var, i_pred)]
    # dat_m <- data.table(apply(dat_m, 2, scale))
    
    if(dep_var == "Framingham")
      lm_out <- lm(I(log(Framingham)) ~ ., data=dat_m)
    else if(dep_var == "BIO_AGE")
      lm_out <- lm(BIO_AGE ~ ., data=dat_m)
    else if(dep_var == "BIO_BMI")
      lm_out <- lm(BIO_BMI ~ ., data=dat_m)
    
    pred_trn <- predict(lm_out, dat_m[, which(names(dat_m) != dep_var), with=F])
    pred_test <- predict(lm_out, dat[fold == (i-1), i_pred, with=F])
    
    res_list[[i]] <- list("train"=cb(pred_trn, dat_m[, dep_var, with=F]),
                          "test"=cb(pred_test, dat[fold == (i-1), dep_var, with=F]))
  }
  
  return(list("all_results"=res_list,
              "train_cor"=unl(lapply(res_list, function(x) cor(x$train[,1], x$train[,2]))),
              "test_cor"=unl(lapply(res_list, function(x) cor(x$test[,1], x$test[,2])))))
}




buildLM_sep <- function(dat, dep_var = c("Framingham", "BIO_AGE", "BIO_BMI")[1], pred_sep = "BIO_SEX",
                        predictors = c("gam_coeff", "rr_ave", "rr_stdev", "Poincare", "mnn", "pnn20", "pnn50", 
                                       "rmssd", "sdsd", "sdnn", "entropy", "heartrate", "lowres", "accfeat"),
                        n_folds = len(unique(dat$fold)))
{
  i_dep_var <- match(dep_var, names(dat))
  i_pred <- unl(as.list(sapply(predictors, grep, names(dat))))
  i_predsep <- match(pred_sep, names(dat))
  
  i_fold <- match("fold", names(dat))
  
  if(is.data.table(dat))
    dat_split <- split(dat, dat[, i_predsep, with=F])
  else
    dat_split <- split(dat, dat[, i_predsep])
  dat_split <- lapply(dat_split, as.data.frame)
  dat_id_all <- lapply(dat_split, function(x) x[, match(c("fold", "guid"), names(x))])
  
  res_list <- vector("list", n_folds)
  for(i in 1:n_folds) {
    
    dat_m_split <- lapply(dat_split, function(x) x[x[,i_fold] != (i-1), c(i_dep_var, i_pred)])
    dat_id <- lapply(dat_split, function(x) x$guid[x[,i_fold] != (i-1)])
    
    if(dep_var == "Framingham")
      lm_out <- lapply(dat_m_split, function(x) lm(I(log(Framingham)) ~ ., data=x))
    else if(dep_var == "BIO_AGE")
      lm_out <- lapply(dat_m_split, function(x) lm(BIO_AGE ~ ., data=x))
    else if(dep_var == "BIO_BMI")
      lm_out <- lapply(dat_m_split, function(x) lm(BIO_BMI ~ ., data=x))
    
    pred_trn <- c(predict(lm_out[[1]], dat_m_split[[1]][, which(names(dat_m_split[[1]]) != dep_var)]),
                  predict(lm_out[[2]], dat_m_split[[2]][, which(names(dat_m_split[[2]]) != dep_var)]))
    pred_test <- c(predict(lm_out[[1]], dat_split[[1]][dat_split[[1]][,i_fold] == (i-1), i_pred]),
                   predict(lm_out[[2]], dat_split[[2]][dat_split[[2]][,i_fold] == (i-1), i_pred]))
    
    res_list[[i]] <- list("train"=data.frame("guid"=unl(dat_id), pred_trn, 
                                             c(dat_m_split[[1]][, dep_var], dat_m_split[[2]][, dep_var])),
                          "test"=data.frame("guid"=unl(lapply(dat_id_all, function(x) x$guid[x$fold == (i-1)])), 
                                            pred_test, 
                                            c(dat_split[[1]][dat_split[[1]][,i_fold] == (i-1), dep_var], 
                                              dat_split[[2]][dat_split[[2]][,i_fold] == (i-1), dep_var])))
  }
  
  return(list("all_results"=res_list,
              "train_cor"=unl(lapply(res_list, function(x) cor(x$train[,2], x$train[,3]))),
              "test_cor"=unl(lapply(res_list, function(x) cor(x$test[,2], x$test[,3])))))
}




###############################################################################
######  'Simple' features
###############################################################################

targets <- c("Framingham", "BIO_AGE", "BIO_BMI")

res <- NULL
for(target in targets) {
  out <- buildLM(dat, dep_var=target, predictors = c("BIO_SEX", "gam_coeff", "rr_ave", "rr_stdev", "Poincare", "mnn", "pnn20", "pnn50", 
                                                "rmssd", "sdsd", "sdnn", "entropy", "heartrate", "lowres", "accfeat"))
  
  
  out2 <- buildLM_sep(dat, dep_var=target, pred_sep = "BIO_SEX", 
                      predictors = c("gam_coeff", "rr_ave", "rr_stdev", "Poincare", "mnn", "pnn20", "pnn50", 
                                     "rmssd", "sdsd", "sdnn", "entropy", "heartrate", "lowres", "accfeat"))
  
  
  cat("Target =", target, "\n")
  cat("CONTROLLING FOR GENDER:\nmean train cor = ", round(mean(out$train_cor), 3),
      "\nmean test cor = ", round(mean(out$test_cor), 3), "\n\nSEPERATED BY GENDER:\n",
      "mean train cor = ", round(mean(out2$train_cor), 3), "\nmean test cor = ",
      round(mean(out2$test_cor), 3), "\n\n\n", sep="")
  
  res <- c(res, mean(out2$test_cor))
}

names(res) <- targets


###############################################################################
######  Investigate performance of HRV features only
###############################################################################


targets <- c("Framingham", "BIO_AGE", "BIO_BMI")

res <- NULL
for(target in targets) {
  
  out1 <- buildLM_sep(dat, dep_var=target, pred_sep = "BIO_SEX", 
                      predictors = c("mnn", "pnn20", "pnn50", "rmssd", "sdnn"))
  
  res <- c(res, mean(out1$test_cor))
}

names(res) <- targets
res



###############################################################################
######  Investigate consistency of monthly measurements
###############################################################################


out <- buildLM_sep(dat, dep_var="Framingham", pred_sep = "BIO_SEX", 
                   predictors = c("gam_coeff", "rr_ave", "rr_stdev", "Poincare", "mnn", "pnn20", "pnn50", 
                                  "rmssd", "sdsd", "sdnn", "entropy", "heartrate", "lowres", "accfeat"))



out_df <- out$all_results[[1]]$test
for(i in 2:len(out$all_results))
  out_df <- rbind(out_df, out$all_results[[i]]$test)
names(out_df) <- c("giud", "pred", "true")
out_df[,2] <- exp(out_df[,2])

out_split <- split(out_df[,2:3], out_df[,1])
# out_split_diff <- lapply(out_split[lapply(out_split, nrow) > 1], apply, 1, diff)
# out_mad <- mapply(function(x, y){ abs(x)/y }, 
#                   out_split_diff, lapply(out_split[lapply(out_split, nrow) > 1], function(x) x[1,2]))

out_cvar <- unl(lapply(out_split[lapply(out_split, nrow) > 1], function(x) cvar(x[,1])))

wgg_density(out_cvar)



