MITBIH_pks_corrections <- list(
  "patient01" = NULL,
  "patient02" = list("add_ann_other" = c(175.875, 318.625)),
  "patient03" = NULL,
  "patient04" = list("add_ann_other" = c(1095.353)),
  "patient05" = list("add_ann_other" = c(854.55, 1012.017, 1468.306, 1693.131)),
  "patient06" = list("add_ann_other" = c(631.739, 1032.525, 1205.556, 1313.517, 1652.775)),
  "patient07" = list("add_ann_other" = c(998.897)),
  "patient08" = list("add_ann_other" = c(755.581, 1737.292, 1794.892)),
  "patient09" = list("add_ann_other" = c(819.553, 931.483, 1217.775, 1654.828, 1678.631, 1705.878, 1721.231)),
  "patient10" = list("add_ann_other" = c(1457.814, 1805.364)),
  "patient11" = NULL,
  "patient12" = NULL,
  "patient13" = NULL,
  "patient14" = NULL,
  "patient15" = NULL,
  "patient16" = NULL,
  "patient17" = NULL,
  "patient18" = NULL,
  "patient19" = NULL,
  "patient20" = NULL
)

##########################################################################
##########################################################################



# patient10:  1455s (missed beat)
#             1805s (missed beat)