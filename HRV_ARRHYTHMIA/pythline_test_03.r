source("../useful_funs.r")
source("../useful_funs_ggplot.r")
library(ggplot2)
library(data.table)
library(pls)
library(glmnet)


###############################################################################
####  Gleam ranges of raw HRV features
###############################################################################

# fpath <- "~/Data/MMI_2018-03-13_Calculated-Concatenated/renier_out_summaries/"
# dlist <- getDataFromDir(fpath, dataE=F, ext="h5")
# 
# summ_list <- vector("list", len(dlist))
# for(i in 1:len(dlist)) {
#   
#   dat_hrv <- h5read2(dlist[i], "/dev_hrv_metrics_timedomain")
#   dat_pdb <- h5read2(dlist[i], "/dev_poincaredb_10min_count")
#   
#   summ_list[[i]] <- cb(apply(dat_hrv[,-1], 2, fivenum), apply(dat_pdb[,-1], 2, fivenum))
#   catProgress(i, len(dlist))  
# }
# 
# 
# j <- 1
# summ_col <- lapply(summ_list, function(x) x[,j])
# 
# print(c(round(min(unl(lapply(summ_col, function(x) x[1]))), 0), 
#         round(max(unl(lapply(summ_col, function(x) x[5]))), 0)))
# 
# cols <- brewer.pal(5, "Set1")
# wgg_density(unl(lapply(summ_col, function(x) x[1])), fill=cols[1])




###############################################################################
####  Load data
###############################################################################

# Read Framingham target and CV folds
dat_fram <- fread("~/Documents/HealthQ/HHA/features_mmi_fram.csv")
dat_fram <- dat_fram[match(unique(dat_fram$guid), dat_fram$guid), .(guid, Framingham, BIO_SEX, BIO_BMI, BIO_AGE)]
folds <- fread("~/Documents/HealthQ/HHA/folds.csv")

# Read old summary data
dat_old <- fread("~/Documents/HealthQ/HHA/features_mmi_all.csv")
dat_old <- dat_old[, c(unl(as.list(sapply(c("mnn", "pnn20", "pnn50", "rmssd", "sdnn"), grep, names(dat_old)))), 
                       match(c("guid", "Framingham", "BIO_SEX"), names(dat_old))), with=F]

# Read daily, weekly and monthly summary data
fls <- getDataFromDir("~/Data/MMI_2018-03-13_Calculated-Concatenated/renier_out_summaries/output/", dataE=F)
dlist_raw <- lapply(as.list(fls[grep("daily", fls)]), fread)
dlist_w <- lapply(as.list(fls[grep("weekly", fls)]), fread)
dlist_m <- lapply(as.list(fls[grep("monthly", fls)]), fread)

# Assign TPIDs and concat into single DT
dlist_names <- unl(lapply(strsplit(unl(strsplit(basename(fls[grep("monthly", fls)]), ".csv")), "_"), function(x) x[1]))
for(i in 1:len(dlist_m)) {
  dlist_m[[i]]$guid <- dlist_names[i]
  dlist_w[[i]]$guid <- dlist_names[i]
}

dat_m <- dlist_m[[1]][, -1]
dat_w <- dlist_w[[1]][, -1]
for(i in 2:len(dlist_m)) {
  dat_m <- rbind(dat_m, dlist_m[[i]][, -1])
  dat_w <- rbind(dat_w, dlist_w[[i]][, -1])
  catProgress(i, len(dlist_m))
}

# names(dat_w)[1] <- "index"
# write.table2(dat_w, "hrv_agg_by_sleep_weekly_summaries.csv", col.names=T)


# Merge targets with features
guid_match <- match(dat_m$guid, dat_fram$guid)
dat_model <- cb(dat_m[which(!is.na(guid_match)),], 
                "Framingham"=dat_fram$Framingham[na.omit(guid_match)],
                "BIO_AGE"=dat_fram$BIO_AGE[na.omit(guid_match)],
                "BIO_SEX"=dat_fram$BIO_SEX[na.omit(guid_match)],
                "BIO_BMI"=dat_fram$BIO_BMI[na.omit(guid_match)])

# Add folds to DT
guid_match <- match(dat_model$guid, folds$V1)
dat_model <- cb(dat_model[which(!is.na(guid_match)),], "fold"=folds$V2[na.omit(guid_match)])

guid_match_old <- match(dat_old$guid, folds$V1)
dat_old <- cb(dat_old[which(!is.na(guid_match_old)),], "fold"=folds$V2[na.omit(guid_match_old)])

# Change name of 'WAKE_DAY' event to 'DAY' for easier grep
names(dat_model) <- gsub("WAKE_DAY", "DAY", names(dat_model))


# Formulate data subsets
dat_feat <- dat_model[, unl(as.list(sapply(c("mnn", "pnn20", "pnn50", "rmssd", "sdnn"), grep, names(dat_model)))), with=F]
dat_lm <- cb(dat_feat, dat_model[, match(c("Framingham", "fold", "BIO_SEX"), names(dat_model)), with=F])
dat_lm_old <- dat_old[, -which(names(dat_old) == "guid"), with=F]


###############################################################################
####  Modelling
###############################################################################


targets <- c("Framingham", "BIO_AGE", "BIO_BMI")
res <- NULL

for(target in targets) {
  
  # out1 <- buildLM_sep(dat_model, dep_var=target, pred_sep = "BIO_SEX", 
                      # predictors = c("mnn", "pnn20", "pnn50", "rmssd", "sdnn"))
  
  # out1 <- buildLM_sep(dat_model, dep_var=target, pred_sep = "BIO_SEX", 
                      # predictors = c("DAY"))
  
  out1 <- buildLM_sep(dat_model, dep_var=target, pred_sep = "BIO_SEX", 
                      predictors = names(out2$coefficients))
  
  res <- c(res, mean(out1$test_cor))
}

names(res) <- targets
res


# Framingham    BIO_AGE    BIO_BMI 
# 0.4885559  0.5212519  0.3780997 



###############################################################################
####  Correlations
###############################################################################

wgg_corr(dat_feat, add_text_to_tiles = FALSE)


dat_cor <- cor(dat_feat)
hc <- hclust(as.dist(1-dat_cor))
dat_cor <- dat_cor[hc$order, hc$order]

outmat <- NULL
for(i in 1:nrow(dat_cor)) {
  for(j in 1:nrow(dat_cor))
    if(!is.na(dat_cor[i,j]) & (i != j) & dat_cor[i,j] > 0.9)
      outmat <- rbind(outmat, c(i, j, dat_cor[i,j]))
}

outmat <- as.data.frame(outmat)
outmat[,1] <- rownames(dat_cor)[outmat[,1]]
outmat[,2] <- colnames(dat_cor)[outmat[,2]]

(outmat <- as.data.table(outmat))




###############################################################################
####  Features stability (at weekly level)
###############################################################################

# Create weekly feature vector and seperate guid
dat_w <- dat_w[, -grep("sdsd", names(dat_w)), with=F]
dat_w_guid <- dat_w$guid
dat_w <- dat_w[, -match("guid", names(dat_w)), with=F]

# Scale globally to 0-1
# dat_w_scaled <- apply(dat_w, 2, function(x) x-min(x))
# dat_w_scaled <- apply(dat_w_scaled, 2, function(x) x/max(x))
dat_w_scaled <- as.data.table(scale(dat_w))

dat_w_groupby_guid <- split(dat_w_scaled, dat_w_guid)
dat_w_guid_mean <- lapply(dat_w_groupby_guid, apply, 2, mean)

for(i in 1:len(dat_w_groupby_guid)) {
  dat_gi <- dat_w_groupby_guid[[i]]
  dat_w_groupby_guid[[i]] <- as.data.frame(dat_gi - matrix(as.num(dat_w_guid_mean[[i]]), byrow=T, nrow=nrow(dat_gi), ncol=ncol(dat_gi)))
  catProgress(i, len(dat_w_groupby_guid))
}

dat_w2 <- df_from_list(dat_w_groupby_guid)
dat_w2_isv <- data.frame("intra-subject variance"=round(apply(dat_w2, 2, sd), 4))
dat_w2_isv[order(dat_w2_isv),,drop=F]



###############################################################################
####  Ad-hoc modelling
###############################################################################


dat_use <- dat_lm
dat_use$Framingham <- log(dat_use$Framingham)

# dat_use <- as.data.table(scale(dat_use))
glm_alpha <- 1
glm_s <- 55


res <- NULL
res_m <- NULL
res_f <- NULL
for(k in 1:len(unique(dat_use$fold))) {
  # Split into test/train depending on CV fold
  i_train <- which(dat_use$fold != (k-1))
  dat_lm_train <- dat_use[i_train, -which(names(dat_use) == "fold"), with=F]
  dat_lm_test <- dat_use[-i_train, -which(names(dat_use) == "fold"), with=F]
  
  # Split into genders
  dat_lm_train_m <- dat_lm_train[dat_lm_train$BIO_SEX == 1, -which(names(dat_lm_train) == "BIO_SEX"), with=F]
  dat_lm_test_m <- dat_lm_test[dat_lm_test$BIO_SEX == 1, -which(names(dat_lm_test) == "BIO_SEX"), with=F]
  dat_lm_train_f <- dat_lm_train[dat_lm_train$BIO_SEX == 0, -which(names(dat_lm_train) == "BIO_SEX"), with=F]
  dat_lm_test_f <- dat_lm_test[dat_lm_test$BIO_SEX == 0, -which(names(dat_lm_test) == "BIO_SEX"), with=F]
  
  # Linear model
  out_lm_m <- lm(Framingham ~., data=dat_lm_train_m)
  out_lm_f <- lm(Framingham ~., data=dat_lm_train_f)
  out_lm_pred_m <- predict(out_lm_m, dat_lm_test_m[, -which(names(dat_lm_test_m) == "Framingham"), with=F])
  out_lm_pred_f <- predict(out_lm_f, dat_lm_test_f[, -which(names(dat_lm_test_f) == "Framingham"), with=F])
  
  out_lm_cor <- cor(c(out_lm_pred_m, out_lm_pred_f), c(dat_lm_test_m$Framingham, dat_lm_test_f$Framingham))
  out_lm_rmse <- sqrt(mean((c(out_lm_pred_m, out_lm_pred_f) - c(dat_lm_test_m$Framingham, dat_lm_test_f$Framingham))^2))
  
  # Elastic net
  out_glm_m <- glmnet(x=as.matrix(dat_lm_train_m[, -which(names(dat_lm_train_m) == "Framingham"), with=F]),
                    y=dat_lm_train_m$Framingham, family="gaussian", alpha=glm_alpha)
  out_glm_f <- glmnet(x=as.matrix(dat_lm_train_f[, -which(names(dat_lm_train_f) == "Framingham"), with=F]),
                      y=dat_lm_train_f$Framingham, family="gaussian", alpha=glm_alpha)
  out_glm_pred_m <- predict(out_glm_m, as.matrix(dat_lm_test_m[, -which(names(dat_lm_test_m) == "Framingham"), with=F]))[,glm_s]
  out_glm_pred_f <- predict(out_glm_f, as.matrix(dat_lm_test_f[, -which(names(dat_lm_test_f) == "Framingham"), with=F]))[,glm_s]
  
  out_glm_cor <- cor(c(out_glm_pred_m, out_glm_pred_f), c(dat_lm_test_m$Framingham, dat_lm_test_f$Framingham))
  out_glm_rmse <- sqrt(mean((c(out_glm_pred_m, out_glm_pred_f) - c(dat_lm_test_m$Framingham, dat_lm_test_f$Framingham))^2))
  
  res <- rbind(res, c("LM_cor"=out_lm_cor, "GLM_cor"=out_glm_cor,
                      "LM_rmse"=out_lm_rmse, "GLM_rmse"=out_glm_rmse))
  
  res_m <- rbind(res_m, cb(out_glm_pred_m, dat_lm_test_m$Framingham))
  res_f <- rbind(res_f, cb(out_glm_pred_f, dat_lm_test_f$Framingham))
}
  
colMeans(res)


# ggdat <- rbind(data.frame("fitted"=res_m[,1], "true"=res_m[,2], "sex"="M\t(p=0.602; RMSE=0.702)"),
#                data.frame("fitted"=res_f[,1], "true"=res_f[,2], "sex"="F\t(p=0.548; RMSE=0.661)"))
# 
# gg <- ggplot(data=ggdat, aes(x=true, y=fitted, col=sex)) + ggtitle("New HRV model to predict log Framingham")
# gg <- gg + geom_point() + coord_cartesian(ylim=c(-5.7, -0.7))
# gg <- gg + theme(legend.position = c(0.15, 0.9), legend.title=element_blank(), plot.title=element_text(hjust=0.5))
# w() ; print(gg)
# ggsave("./output/pythline/new_hrv.png", width=10, height=8)
# 
# sqrt(mean(apply(res_f, 1, diff)^2))




