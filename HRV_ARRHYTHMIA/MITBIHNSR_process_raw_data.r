source("../useful_funs.r")
library(rhdf5)

h5list <- getDataFromDir("~/Data/MIT-BIH_NSR//", ext="h5", dataE=FALSE)

valid_RR_symbols <- c("N", "L", "R", "B", "A", "a", "J", "S", "V", "r", "F", "e", "j", "n", "E", "/", "f", "Q")



##########################################################################
#####  Process raw data  #################################################
##########################################################################



rr_list <- vector("list", len(h5list))
for(i in 1:len(h5list)) {
  
  dat_ann <- cb(h5read(h5list[i], "annotation_sample_locations", compoundAsDataFrame=T)$table[, -1],
                h5read(h5list[i], "annotation_symbols", compoundAsDataFrame=T)$table[, -1],
                h5read(h5list[i], "annotation_aux_notes", compoundAsDataFrame=T)$table[, -1])
  
  dat_fs <- h5read(h5list[i], "meta", compoundAsDataFrame=T)$table
  
  pks_df <- data.frame("t"=as.num(dat_ann[,1])/dat_fs[1,2],
                       "ann"=dat_ann[, 2], stringsAsFactors=FALSE)
  
  print(table(pks_df$ann))
  
  # rr_df <- pks_df[pks_df$ann %in% valid_RR_symbols, ]
  rr_df <- pks_df[-which(pks_df$ann == "~"), ]
  rr_df$RRI <- c(0, 1000*diff(rr_df$t))
  rr_df <- rr_df[-1,]
  
  rr_list[[i]] <- rr_df
  
  catProgress(i, len(h5list))
}

names(rr_list) <- paste0("MIT-BIH_NSR_", unl(lapply(strsplit(h5list, "//"), function(x) strsplit(x[[2]], ".h5")[[1]])))
MITBIHNSR_rr_list <- rr_list
save(MITBIHNSR_rr_list, file="MITBIHNSR_rr_list.rdata")


##########################################################################
#####  Poincare plots  ###################################################
##########################################################################


cols <- c("blue", "forestgreen", "purple", "red", "orange")

for(i in 1:len(rr_list)) {

  ## Set up RR df  
  rr_df <- rr_list[[i]]
  which_ndash <- which(rr_df$ann != "N") + 1
  which_ndash <- which_ndash[which_ndash <= nrow(rr_df)]
  which_ndash <- which_ndash[rr_df$ann[which_ndash] == "N"]
  rr_df$ann[which_ndash] <- "T"
  rr_df$ann[!rr_df$ann %in% c('N', 'S', 'T', 'V')] <- "O"
  
  # rr_10_list <- splitInto10Min(rr_df)
  # rr_poinc_list <- lapply(rr_10_list, function(z) data.frame(x=z[1:(nrow(z)-1), 1], y=z[-1, 1]))
  
  rr_poinc_df <- data.frame(x=rr_df$RRI[1:(nrow(rr_df)-1)], y=rr_df$RRI[-1])
  
  png(paste0("./output/MITBIHNSR_poincare/", format_ind(i, 4), ".png"), width=1000, height=1000)
  plot_g(rr_poinc_df$x, rr_poinc_df$y, cex=0.75, xlim=c(200, 2000), ylim=c(200, 2000),
         col=cols[match(rr_df$ann, c("N", "S", "V", "T", "O"))])
  invisible(dev.off())

  catProgress(i, len(rr_list))  
}



##########################################################################
#####  Try to find geometric bounds  #####################################
##########################################################################



cols <- c("blue", "forestgreen", "purple", "red", "orange")

for(i in 1:len(rr_list)) {
  
  ## Set up RR df  
  rr_df <- rr_list[[i]]
  which_ndash <- which(rr_df$ann != "N") + 1
  which_ndash <- which_ndash[which_ndash <= nrow(rr_df)]
  which_ndash <- which_ndash[rr_df$ann[which_ndash] == "N"]
  rr_df$ann[which_ndash] <- "T"
  rr_df$ann[!rr_df$ann %in% c('N', 'S', 'T', 'V')] <- "O"
  
  # rr_10_list <- splitInto10Min(rr_df)
  # rr_poinc_list <- lapply(rr_10_list, function(z) data.frame(x=z[1:(nrow(z)-1), 1], y=z[-1, 1]))
  
  rr_poinc_df <- data.frame(x=rr_df$RRI[1:(nrow(rr_df)-1)], y=rr_df$RRI[-1])
  rr_poinc_cov <- sqrt(cov(rr_poinc_df$x, rr_poinc_df$y))
  
  
  png(paste0("./output/MITBIHNSR_poincare/", format_ind(i, 4), ".png"), width=1000, height=1000)
  
  plot_g(rr_poinc_df$x, rr_poinc_df$y, cex=0.75, xlim=c(200, 2000), ylim=c(200, 2000),
         col=cols[match(rr_df$ann, c("N", "S", "V", "T", "O"))])
  abline(a=0, b=1, lty=2)
  
  y1 <- 1000 + c(2.5, -2.5)*rr_poinc_cov
  y2 <- 100 + c(1, -1)*rr_poinc_cov
  b <- (y1 - y2)/(1000-100)
  a <- y1 - 1000*b
  abline(a=a[1], b=b[1], col="red", lwd=2)
  abline(a=a[2], b=b[2], col="red", lwd=2)
  
  invisible(dev.off())
  
  catProgress(i, len(rr_list))  
}


