source("../useful_funs.r")
library(rhdf5)

h5list <- getDataFromDir("~/Data/CAST/", ext="h5", dataE=FALSE)

NOTE_VALID <- "recording with a timing signal, flutter-compensated"

##########################################################################

res <- NULL
for(i in 1:len(h5list)) {
  
  h5_tables <- invisible(h5contents(h5list[i])[,1])
  if(len(grep("RR_base", h5_tables)) > 0) {
    rr_base <- h5read(h5list[i], "RR_baseline", compoundAsDataFrame=T)$table[, -1]
    tab <- table(rr_base$annotation)
    res <- c(res, names(tab)[!(names(tab) %in% c("N", "A", "V", "Q"))])
  }
  
  if(len(grep("RR_supp", h5_tables)) > 0) {
    rr_supp <- h5read(h5list[i], "RR_suppression", compoundAsDataFrame=T)$table[, -1]
    tab <- table(rr_supp$annotation)
    res <- c(res, names(tab)[!(names(tab) %in% c("N", "A", "V", "Q"))])
  }
  
  res <- unique(res)
  
  catProgress(i, len(h5list))
}


##########################################################################

# incrementCount <- function(count_all, tab)
# {
#   for(t in names(tab))
#     if(t == "N") 
#       count_all[1] <- count_all[1] + tab[names(tab) == "N"]
#     else if(t == "A") 
#       count_all[2] <- count_all[2] + tab[names(tab) == "A"]
#     else if(t == "V") 
#       count_all[3] <- count_all[3] + tab[names(tab) == "V"]
#     else if(t == "Q") 
#       count_all[4] <- count_all[4] + tab[names(tab) == "Q"]
#     else 
#       count_all[5] <- count_all[5] + sum(tab[!(names(tab) %in% names(count_all)[1:4])])
#     
#     return(count_all)
# }
# 
# count_all <- count_valid <- numeric(5)
# names(count_all) <- names(count_valid) <- c("N", "A", "V", "Q", "other")
# for(i in 1:len(h5list)) {
#   
#   h5_tables <- invisible(h5contents(h5list[i])[,1])
#   
#   if(len(grep("RR_base", h5_tables)) > 0) {
#     rr_base <- h5read(h5list[i], "RR_baseline", compoundAsDataFrame=T)$table[, -1]
#     meta_base <- h5read(h5list[i], "meta_baseline", compoundAsDataFrame=T)$table[, -1]
#     count_all <- incrementCount(count_all, table(rr_base$annotation))
#     
#     if(meta_base[7, 2] == NOTE_VALID)
#       count_valid <- incrementCount(count_valid, table(rr_base$annotation))
#   }
#   
#   if(len(grep("RR_supp", h5_tables)) > 0) {
#     rr_supp <- h5read(h5list[i], "RR_suppression", compoundAsDataFrame=T)$table[, -1]
#     meta_supp <- h5read(h5list[i], "meta_suppression", compoundAsDataFrame=T)$table[, -1]
#     count_all <- incrementCount(count_all, table(rr_supp$annotation))
#     
#     if(meta_supp[7, 2] == NOTE_VALID)
#       count_valid <- incrementCount(count_valid, table(rr_supp$annotation))
#   }
#   
#   catProgress(i, len(h5list))
# }


# > count_all
# N         A         V         Q     other 
# 151492959    891122   2378149   1911412   7419714 
# > count_valid
# N        A        V        Q    other 
# 65985561   299352   906093   582181  3591109 



##########################################################################

h5_cont_all <- sapply(h5list, function(x) h5contents(x)[,1])


res_list <- vector("list", len(unl(h5_cont_all))/2)
j <- 1
for(i in 1:len(h5list)) {
  
  h5_tables <- invisible(h5contents(h5list[i])[,1])
  
  if(len(grep("RR_base", h5_tables)) > 0) {
    rr_base <- h5read(h5list[i], "RR_baseline", compoundAsDataFrame=T)$table[, -1]
    meta_base <- h5read(h5list[i], "meta_baseline", compoundAsDataFrame=T)$table[, -1]
    
    res_list[[j]] <- data.frame("RRI" = 1000*diff(as.num(rr_base$x))/as.num(meta_base[2,2]),
                                "ann" = rr_base$annotation[-1], stringsAsFactors=FALSE)
    
    names(res_list)[j] <- meta_base[1,2]
    if(meta_base[8, 2] == NOTE_VALID)
      names(res_list)[j] <- paste0(names(res_list)[j], "_COMPENSATED")
    
    j <- j+1
  }
  
  if(len(grep("RR_supp", h5_tables)) > 0) {
    rr_supp <- h5read(h5list[i], "RR_suppression", compoundAsDataFrame=T)$table[, -1]
    meta_supp <- h5read(h5list[i], "meta_suppression", compoundAsDataFrame=T)$table[, -1]

    res_list[[j]] <- data.frame("RRI" = 1000*diff(as.num(rr_supp$x))/as.num(meta_supp[2,2]),
                                "ann" = rr_supp$annotation[-1], stringsAsFactors=FALSE)
    
    names(res_list)[j] <- meta_supp[1,2]
    if(meta_supp[8, 2] == NOTE_VALID)
      names(res_list)[j] <- paste0(names(res_list)[j], "_COMPENSATED")
    
    j <- j+1
  }
  
  catProgress(i, len(h5list))
}

# save(res_list, file="CAST_rr_list.rdata")


