library(glmnet)
library(doMC)
library(ROCR)
library(caTools)
library(ggplot2)


###############################################################################
####    Feature generation functions    #######################################
###############################################################################

generateRRWinMat <- function(rr, rrv, winsize=5)
{
  n <- len(rr)
  m <- ceiling(winsize/2)
  
  matConstr <- function(x)
  {
    xmat <- cb(x[1:n], 
               c(x[2:n], rep(NA, 1)),
               c(x[3:n], rep(NA, 2)),
               c(x[4:n], rep(NA, 3)),
               c(x[5:n], rep(NA, 4)))
    
    xmat <- rbind(matrix(NA, ncol=winsize, nrow=m-1), 
                  xmat[1:(n-winsize+1),],
                  matrix(NA, ncol=winsize, nrow=m-1))
  }
  
  ## Create feature matrix with raw RR values
  rr_mat <- matConstr(rr)
  
  ## Remove feature row if principal RR invalid
  rrv_mat <- matConstr(rrv)
  rr_mat[which(rrv_mat[, m] == 0),] <- NA
  rr_mat[which(rowSums(rrv_mat) < winsize),] <- NA
  
  ## Keep track of NA rows
  which_na <- which(apply(rr_mat, 1, function(x) any(is.na(x))))
  
  return(list("RR_window_mat"=rr_mat, "RRV_window_mat"=rrv_mat, "which_rows_na"=which_na))
}


generateFeatures_RRwindow <- function(rr, rrv, winsize=5)
{
  ## Generate raw RR feature matrix
  rr_mat_out <- generateRRWinMat(rr, rrv, winsize)
  rr_mat <- rr_mat_out$RR_window_mat
  colnames(rr_mat) <- paste0("feature_RRraw_t", c("_m2", "_m1", "", "_p1", "_p2"))
  
  ## Append feature matrix with diff'd RR values
  rr_diff_mat <- t(apply(rr_mat, 1, diff))
  colnames(rr_diff_mat) <- paste0("feature_RRdiff_t", c("_m1", "", "_p1", "_p2"))
  
  ## Calculate mean and sd features
  rr_stats_mat <- cb(apply(rr_mat, 1, mean),
                     apply(rr_diff_mat, 1, mean),
                     apply(rr_mat, 1, sd),
                     apply(rr_diff_mat, 1, sd))
  colnames(rr_stats_mat) <- paste0("feature_RRstats_", c("mean", "mean_diff", "sd", "sd_diff"))
  
  ## Calculate RR_t deviation from mean feature
  rr_dev_t <- rr_stats_mat[,1] - rr_mat[, 2:4]
  colnames(rr_dev_t) <- paste0("feature_RRdevmean_t", c("_m1", "", "_p1"))
  
  return(list("features_RR_df"=data.frame(rr_mat, rr_diff_mat, rr_stats_mat, rr_dev_t), 
              "feature_RR_NArows"=rr_mat_out$which_rows_na))
}





###############################################################################
####    Modelling functions    ################################################
###############################################################################

# CV_K=10; CV_STRATIFY=TRUE; CV_SEED=NULL;
# GLMNET_FIXED_LAM=TRUE; GLMNET_MULTINOMIAL_TYPE="ungrouped"; GLMNET_NLAMBDA=50;
# GLMNET_CV=FALSE; GLMNET_CV_NFOLDS=5;
# GLMNET_CV_PARALLEL_CORES=6; GLMNET_CV_PLOT_PATH=NULL;
# OUTPUT_FPATH="./output/modeldat/"


modelBuild_logistic_glmnet <- function(xfeat, y, 
                                       CV_K=10, CV_STRATIFY=TRUE, CV_SEED=NULL, CV_1FOLD_PROP=0.6,
                                       GLMNET_FIXED_LAM=TRUE, GLMNET_MULTINOMIAL_TYPE="ungrouped", GLMNET_NLAMBDA=50,
                                       GLMNET_CV=TRUE, GLMNET_CV_NFOLDS=5, 
                                       GLMNET_CV_PARALLEL_CORES=6, GLMNET_CV_PLOT_PATH=NULL,
                                       OUTPUT_FPATH="./output/modeldat/")
{
  # Ensure response variable is a factor
  y <- as.factor(y)
  y_lev <- levels(y)
  n <- len(y)
  m <- len(y_lev)
  
  # Configure glmnet family argument appropriately
  glmnet_fam <- "binomial"
  if(m > 2) glmnet_fam <- "multinomial"
  
  # Set seed for reproducibility (if specified)
  if(!is.null(CV_SEED)) set.seed(CV_SEED)
  
  #####################################
  ##  Split data into train/test folds
  #####################################
  
  if(CV_K > 1) {
    if(!CV_STRATIFY)
      folds <- sample(rep(1:CV_K, each=ceiling(n/CV_K)))[1:n]
    
    # Construct stratified CV folds based on prop representation (if specified)
    if(CV_STRATIFY) {
      nclass <- table(y)
      
      folds <- num(n)
      for(i in 1:m) 
        folds[y == names(nclass)[i]] <- sample(rep(1:CV_K, each=ceiling(nclass[i]/CV_K)))[1:nclass[i]]
    }
  }
  
  # If CV_K is 1, cross-validation is not performed, rather, only 1 "fold" is
  #  created, i.e. a classic single train/test split base on 'CV_1FOLD_PROP'
  if(CV_K == 1) {
    if(!CV_STRATIFY) {
      n_fold <- ceiling(n*CV_1FOLD_PROP)
      folds <- sample( rep(0:1, c(n_fold, n - n_fold)) )
    }
    
    # Stratify test/train split (if specified)
    if(CV_STRATIFY) {
      nclass <- table(y)
      
      folds <- num(n)
      for(i in 1:m) {
        n_fold <- ceiling(nclass[i]*CV_1FOLD_PROP)
        folds[y == names(nclass)[i]] <- sample( rep(0:1, c(n_fold, nclass[i] - n_fold)) )
      }
    }
  }
  
  # Validate CV folds selected as intended
  # for(i in 1:m) print(table(folds[y == names(table(y))[i]]))
    
  ##########################
  ##  Iterate over CV folds
  ##########################
  
  lam_fixed <- NULL
  lam_min <- NULL
  lam_1se <- NULL
  
  cv_out_list <- vector("list", CV_K)
  cv_itrain_list <- vector("list", CV_K)
  for(k in 1:CV_K) {
  # for(k in 1:3) {
    
    # Partition data into test/train
    x_train <- xfeat[folds != k,]
    y_train <- y[folds != k]
    x_test <- xfeat[folds == k,]
    y_test <- y[folds == k]
    
    # Ensure randomised observations
    i_train <- sample(1:len(y_train))
    x_train <- x_train[i_train, ]
    y_train <- y_train[i_train]
    
    #############################
    ##  Modelling with glmnet()
    #############################
    
    if(!GLMNET_CV) {
      # Run glmnet logistic regression model and store initial lambda sequence
      if(is.null(lam_fixed)) {
        out <- glmnet(x=as.matrix(x_train), y=y_train, family=glmnet_fam, 
                      type.multinomial=GLMNET_MULTINOMIAL_TYPE, nlambda=GLMNET_NLAMBDA)
        lam_fixed <- out$lambda
      }
      # Use fixed lambda sequence in glmnet modelling (if specified)
      else if(GLMNET_FIXED_LAM) {
        out <- glmnet(x=as.matrix(x_train), y=y_train, family=glmnet_fam, 
                      type.multinomial=GLMNET_MULTINOMIAL_TYPE, lambda=lam_fixed)
      }
    }
    
    # If specified, use glmnet()'s internal CV routine to find optimal lambda
    if(GLMNET_CV) {
      stop("Here be dragons... comment me out to proceed.")
      
      # Run glmnet logistic regression model and store initial lambda sequence
      if(is.null(lam_fixed)) {
        registerDoMC(cores=GLMNET_CV_PARALLEL_CORES)
        out <- cv.glmnet(x=as.matrix(x_train), y=y_train, family=glmnet_fam, 
                            type.multinomial=GLMNET_MULTINOMIAL_TYPE, nfolds=GLMNET_CV_NFOLDS,
                            parallel=TRUE, nlambda=GLMNET_NLAMBDA)
        lam_fixed <- out_cv$glmnet.fit$lambda
      }
      
      # Use fixed lambda sequence in glmnet modelling (if specified)
      else if(GLMNET_FIXED_LAM) {
        registerDoMC(cores=GLMNET_CV_PARALLEL_CORES)
        out <- cv.glmnet(x=as.matrix(x_train), y=y_train, family=glmnet_fam, 
                            type.multinomial=GLMNET_MULTINOMIAL_TYPE, lambda=lam_fixed, nfolds=GLMNET_CV_NFOLDS,
                            parallel=TRUE, nlambda=GLMNET_NLAMBDA)
      }
      
      # Save cv.glmnet() deviance output
      # if(!is.null(GLMNET_CV_PLOT_PATH)) {
      #   png(file=paste0(GLMNET_CV_PLOT_PATH, "glmnetcv_lambda-vs-deviance_fold", format_ind(k, 3), ".png"),
      #       width=1200, height=800)
      #   plot(out)
      #   invisible(dev.off())
      # }
    }
    
    # Save model output to files
    # NOTE: changing this filename string would result in errors using 'modelTest_()'
    save(out, file=paste0(OUTPUT_FPATH, "modeltest-glmnet-logistic_model_out_cv_fold", format_ind(k, 3), ".rdata"))
    
    cv_itrain_list[[k]] <- i_train
    cv_out_list[[k]] <- out

    # Track CV progress
    catProgress2(k, CV_K)
  }
  
  return(list("FOLDS"=folds, "I_TRAIN"=cv_itrain_list, "MODEL_OUT"=cv_out_list))
  
}


###############################################################################
###############################################################################



modelTest_logistic_glmnet <- function(xfeat, y, build_out, output_fpath="./output/modeldat/")
{
  # Ensure 'folds' matches data dim
  folds <- build_out$FOLDS
  if(len(folds) != len(y))
    stop("Input data dimension mismatched with folds.")
  
  cv_k <- sum(unique(folds) > 0)
  if(len(build_out$I_TRAIN) != cv_k)
    stop("Input data dimension mismatched with folds.")
  
  # Ensure response variable is a factor
  y <- as.factor(y)
  y_lev <- levels(y)
  n <- len(y)
  m <- len(y_lev)
  
  # Get list of model output files and ensure compatibility with 'fold'
  f_out <- list.files(output_fpath)
  f_out <- f_out[grep("modeltest-glmnet-logistic_model_out", f_out)]
  f_out <- f_out[grep("rdata", f_out)]
  if(len(f_out) != cv_k) stop("Input file list mismatched with folds.")
  
  f_list <- as.list(paste0(output_fpath, f_out))
  
  #############################################
  ##  Test whether lambda sequences were fixed
  #############################################
  lam_fixed <- FALSE
  
  # Extract lambda sequences from model outputs
  lambda <- lapply(f_list, function(x) {
    load(x)
    return(out$lambda)
  })
  
  # Test that all lambda seqs are similar
  if(unl(lapply(lambda, len)) %=% len(lambda[[1]]))
    if(all(unl(lapply(lambda[-1], function(x) x %=% lambda[[1]]))))
      lam_fixed <- TRUE
  
  if(!lam_fixed) stop("Varying lambda sequences not yet supported :(")
  
  lambda <- lambda[[1]]
  
  ##############################################################
  ##  Function to calculate confusion matrix, prediction probs, 
  ##    and relevant results based on these measures
  ##############################################################
  
  res_fun_confusion <- function(model_out, x_new, y_true, y_lv)
  {
    # Predict new classes and form confusion matrix
    pred <- predict(model_out, newx=x_new, type="class")
    confmat <- apply(pred, 2, function(x) {
        list(table(factor(y_true, levels=y_lv), factor(x, levels=y_lv))) })
    
    # Calculate Cohen's Kappa, accuracy, recall and precision for all confusion mats
    res <- lapply(confmat, function(x) c("KAPPA"=calc_cohen_kappa(x[[1]]),
                                         "ACC"=sum(diag(x[[1]]))/sum(x[[1]]),
                                         "RECALL"=diag(x[[1]])/rowSums(x[[1]]),
                                         "PRECSN"=diag(x[[1]])/colSums(x[[1]])) )
    
    # Append means recall and precision means
    res <- lapply(res, function(x) c(x, "RECALL_mean"=mean(x[grep("RECALL", names(x))])))
    res <- lapply(res, function(x) c(x, "PRECSN_mean"=mean(x[grep("PRECSN", names(x))])))
    
    # Reformat results into matrix
    return( as.data.frame(matrix(unl(res), nrow=len(res), byrow=T, dimnames=list(NULL, names(res[[1]])))) )
  }
  
  res_fun_probs <- function(model_out, x_new, y_true, y_lv)
  {
    if(len(y_lv) == 3) {
      # Calculate AUCs from prediction probabilities
      aucs <- sapply(lambda, function(x) {
        pred <- predict(model_out, newx=x_new, type="response", s=x)[,,1]
        return( rowMeans(colAUC(pred, y_true)) )
      })
      
      # Reformat AUC output to make it nicer
      aucs <- t(as.data.frame(aucs))
      colnames(aucs) <- unl(lapply(strsplit(colnames(aucs), " "), function(x) paste0("AUC_", x[1], ".vs.", x[3])))
      aucs <- cb(aucs, "AUC_mean"=apply(aucs, 1, mean))
    }

    # if(m == 2) {
    # calc_auc_ROCR <- function(prob, true)
    #   return(performance(prediction(prob, as.factor(true)), measure="auc")@y.values[[1]])
    #   aucs <- lapply(x[[3]], function(y) calc_auc_ROCR(y[,2], y[,1]))
    # }
    
    return(aucs)
  }
  
  
  ########################################################################
  ##  Calculate error statistics for training and test per model output
  ########################################################################
  
  res_list_train <- vector("list", cv_k)
  res_list_test <- vector("list", cv_k)
  
  # Gather results over CVed model output separately
  for(k in 1:cv_k) {
    # load(f_list[[k]])
    out <- build_out$MODEL_OUT[[k]]
    
    # Re-partition data into test/train
    x_train <- xfeat[folds != k,]
    y_train <- y[folds != k]
    x_test <- xfeat[folds == k,]
    y_test <- y[folds == k]
    
    # Ensure correct order of previously randomised obs
    # i_train <- build_out$I_TRAIN[[k]]
    # x_train <- x_train[i_train, ]
    # y_train <- y_train[i_train]
    # 
    # Process and collect all results 
    res_list_train[[k]] <- cb(res_fun_confusion(out, as.matrix(x_train), y_train, y_lev),
                              res_fun_probs(out, as.matrix(x_train), y_train, y_lev))
    
    res_list_test[[k]] <- cb(res_fun_confusion(out, as.matrix(x_test), y_test, y_lev),
                             res_fun_probs(out, as.matrix(x_test), y_test, y_lev))
    
    # Update progress
    catProgress2(k, cv_k)
    gc()
  }
  
  names(res_list_train) <- names(res_list_test) <- paste0("CV_fold", sapply(1:cv_k, format_ind, 2))
  
  ####################################
  ##  Aggregate results over CV folds
  ####################################
  
  if(lam_fixed) {
    measure_names <- names(res_list_test[[1]])
    res_agg_train <- as.data.frame(lapply(as.list(measure_names), function(y) {
      agg <- t(apply(as.data.frame(lsel(res_list_train, y)), 1, function(x) c(mean(x), sd(x)/(sqrt(len(x))) )))
      colnames(agg) <- paste0(y, c("_CVM", "_CVSE"))
      return(agg) }))
    
    res_agg_test <- as.data.frame(lapply(as.list(measure_names), function(y) {
      agg <- t(apply(as.data.frame(lsel(res_list_test, y)), 1, function(x) c(mean(x), sd(x)/(sqrt(len(x))) )))
      colnames(agg) <- paste0(y, c("_CVM", "_CVSE"))
      return(agg) }))
    
  }
  
  return(list("lambda"=lambda, 
              "aggregated_results_train"=res_agg_train, 
              "aggregated_results_test"=res_agg_test))
}


# ggdat <- cb(x=1:nrow(res_agg_train), res_agg_train[, grep("KAPPA_", names(res_agg_train))])
# ggdat[,3] <- ggdat[,3]/sqrt(10)
# gg <- ggplot(data=ggdat, aes(x=x, y=KAPPA_CVM)) + geom_line()
# gg <- gg + geom_errorbar(aes(ymin=KAPPA_CVM-KAPPA_CVSD, ymax=KAPPA_CVM+KAPPA_CVSD), width=0.25)
# w() ; print(gg)



###############################################################################
###############################################################################


modelPlot_logistic_glmnet <- function(res_list)
{
  # Assign lambda values
  lambda <- res_list$lambda
  if(is.null(lambda)) stop("Lambda values missing.")
  
  # Assign result lists separate names for convenience
  res_train <- res_list$aggregated_results_train
  res_test <- res_list$aggregated_results_test
  
  ##############################
  ##  Plot of overall summaries
  ##############################
  
  ggdat <- data.frame("log_lambda" = rep(log(lambda), 10),
                      "value" = c(c(as.matrix(res_train[, match(c("KAPPA_CVM", "ACC_CVM", "AUC_mean_CVM", "RECALL_mean_CVM", "PRECSN_mean_CVM"), names(res_train))])),
                                  c(as.matrix(res_test[, match(c("KAPPA_CVM", "ACC_CVM", "AUC_mean_CVM", "RECALL_mean_CVM", "PRECSN_mean_CVM"), names(res_test))]))),
                      "measure" = rep(rep(c("Kappa", "accuracy", "AUC_mean", "recall_mean", "precision_mean"), each=len(lambda)), 2),
                      "data_source" = factor(rep(c("training set", "test set"), each=len(lambda)*5), levels=c("training set", "test set")))
  
  gg <- ggplot(data=ggdat, aes(x=log_lambda, y=value, col=measure))
  gg <- gg + geom_line(size=1.5, alpha=0.75, na.rm=T) + facet_wrap(~data_source)
  gg <- gg + coord_cartesian(ylim=c(0.5, 1))
  
  gg1 <- gg
  # w(12, 6) ; print(gg)
  # return(gg)
 
  
  #################
  ##  Plot of AUCs
  #################
  
  ggdat <- data.frame("log_lambda" = rep(log(lambda), 6),
                      "value" = c(c(as.matrix(res_train[, grep("AUC_N.*CVM", names(res_train))])),
                                  c(as.matrix(res_test[, grep("AUC_N.*CVM", names(res_test))]))),
                      "measure" = rep(rep(c("AUC_NN.vs.NX", "AUC_NN.vs.XN", "AUC_NX.vs.XN"), each=len(lambda)), 2),
                      "data_source" = factor(rep(c("training set", "test set"), each=len(lambda)*3), levels=c("training set", "test set")))
  
  ggdat2 <- data.frame("log_lambda" = rep(log(lambda), 2),
                       "value" = c(c(as.matrix(res_train[, match("AUC_mean_CVM", names(res_train))])),
                                   c(as.matrix(res_test[, match("AUC_mean_CVM", names(res_test))]))),
                       "measure" = rep(rep(c("AUC_mean"), each=len(lambda)), 2),
                       "data_source" = factor(rep(c("training set", "test set"), each=len(lambda)*1), levels=c("training set", "test set")))
  
  gg <- ggplot(data=ggdat, aes(x=log_lambda, y=value, col=measure))
  gg <- gg + geom_line(alpha=0.75, na.rm=T) + facet_wrap(~data_source)
  gg <- gg + geom_line(data=ggdat2, aes(x=log_lambda, y=value), size=1.5, alpha=0.75)
  gg <- gg + coord_cartesian(ylim=c(0.5, 1))
  
  gg2 <- gg
  # w(12, 6) ; print(gg)
  
  
  #################################
  ##  Plot of precision and recall
  #################################
    
  ggdat_r1 <- data.frame("log_lambda" = rep(log(lambda), 6),
                      "value" = c(c(as.matrix(res_train[, grep("RECALL.*[XN]_CVM", names(res_train))])),
                                  c(as.matrix(res_test[, grep("RECALL.*[XN]_CVM", names(res_test))]))),
                      "measure" = rep(rep(c("recall.NN", "recall.NX", "recall.XN"), each=len(lambda)), 2),
                      "data_source" = factor(rep(c("training set", "test set"), each=len(lambda)*3), levels=c("training set", "test set")))
  
  ggdat_r2 <- data.frame("log_lambda" = rep(log(lambda), 2),
                         "value" = c(c(as.matrix(res_train[, match("RECALL_mean_CVM", names(res_train))])),
                                     c(as.matrix(res_test[, match("RECALL_mean_CVM", names(res_test))]))),
                         "measure" = rep(rep(c("recall_mean"), each=len(lambda)), 2),
                         "data_source" = factor(rep(c("training set", "test set"), each=len(lambda)*1), levels=c("training set", "test set")))
  
  ggdat_p1 <- data.frame("log_lambda" = rep(log(lambda), 6),
                         "value" = c(c(as.matrix(res_train[, grep("PRECSN.*[XN]_CVM", names(res_train))])),
                                     c(as.matrix(res_test[, grep("PRECSN.*[XN]_CVM", names(res_test))]))),
                         "measure" = rep(rep(c("precision_NN", "precision_NX", "precision_XN"), each=len(lambda)), 2),
                         "data_source" = factor(rep(c("training set", "test set"), each=len(lambda)*3), levels=c("training set", "test set")))
  
  ggdat_p2 <- data.frame("log_lambda" = rep(log(lambda), 2),
                         "value" = c(c(as.matrix(res_train[, match("PRECSN_mean_CVM", names(res_train))])),
                                     c(as.matrix(res_test[, match("PRECSN_mean_CVM", names(res_test))]))),
                         "measure" = rep(rep(c("precision_mean"), each=len(lambda)), 2),
                         "data_source" = factor(rep(c("training set", "test set"), each=len(lambda)*1), levels=c("training set", "test set")))
  
  gg <- ggplot(data=ggdat_r1, aes(x=log_lambda, y=value, col=measure))
  gg <- gg + geom_line(alpha=0.75, na.rm=T) + facet_wrap(~data_source)
  gg <- gg + geom_line(data=ggdat_r2, aes(x=log_lambda, y=value), size=1.5, alpha=0.75)
  gg <- gg + geom_line(data=ggdat_p1, aes(x=log_lambda, y=value), alpha=0.75, na.rm=T) 
  gg <- gg + geom_line(data=ggdat_p2, aes(x=log_lambda, y=value), size=1.5, alpha=0.75, na.rm=T)
  gg <- gg + coord_cartesian(ylim=c(0.5, 1))
  gg <- gg + scale_colour_manual(values=c(colorRampPalette(c("darkgreen", "yellow"))(6)[2:5], 
                                          colorRampPalette(c("darkblue", "lightblue"))(6)[2:5]))
  
  gg3 <- gg
  # w(12, 6) ; print(gg)
  
  
  #############################################################################
  
  w(12, 6) ; print(gg3)
  w(12, 6) ; print(gg2)
  w(12, 6) ; print(gg1)
  
  return(list("summary_ggplot"=gg1, "auc_ggplot"=gg2, "recall_precision_ggplot"=gg3))

}










