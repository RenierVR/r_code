source("../useful_funs.r")
library(data.table)

source("feature_ArrClass_functions.r")

load("MITBIHNSR_rr_list.rdata")
load("CAST_rr_list_compensated.rdata")
CAST_rr_list <- rr_list

valid_RR_symbols <- c("N", "L", "R", "B", "A", "a", "J", "S", "V", "r", "F", "e", "j", "n", "E", "/", "f")
valid_RR_bounds <- c(100, 3000)

###############################################################################
####    Reconfigure data    ###################################################
###############################################################################

# Add timestamp column to CAST data
CAST_rr_list <- lapply(CAST_rr_list, function(x){ x$t <- cumsum(x$RRI)/1000 ; return(x) })
# CAST_rr_list <- lapply(CAST_rr_list, function(x) x[x$ann %in% c(valid_RR_symbols, "Q"), ])
# MITBIHNSR_rr_list <- lapply(MITBIHNSR_rr_list, function(x){ x$t <- x$t - x$t[1] + x$RRI[1]/1000 ; return(x) })

# Create data subset for modelling
set.seed(2114097)
i_MITBIHNSR <- sample(1:len(MITBIHNSR_rr_list), len(MITBIHNSR_rr_list))
i_CAST <- sample(1:len(CAST_rr_list), 200)
rr_list <- c(MITBIHNSR_rr_list[i_MITBIHNSR], CAST_rr_list[i_CAST])
# rm(MITBIHNSR_rr_list, CAST_rr_list) ; gc()

# Check freq of arrhythmias
sum(unl(lapply(rr_list, function(x) sum(x$ann %in% c("A", "V")))))
sum(unl(lapply(rr_list, function(x) sum(x$ann %in% c("N")))))



###############################################################################
####    Adjust annotations and indicate validity    ###########################
###############################################################################

rr_adjann_list <- lapply(rr_list, function(x) {
  ## Indicate validity based on annotation or RRI magnitude
  x$RRV <- as.num(x$ann %in% valid_RR_symbols)
  x$RRV[x$RRI > max(valid_RR_bounds)] <- 0
  x$RRV[sum(x$RRI < min(valid_RR_bounds))] <- 0
  
  which_rrv0 <- which(x$RRV == 0)
  x$RRV[which_rrv0[which_rrv0 < nrow(x)] + 1] <- 0
  
  ## Create dual annotations for RRIs from original peak anns
  x$ann[x$ann == "N"] <- "NN"
  x$ann[x$ann != "NN"] <- paste0("N", x$ann[x$ann != "NN"])
  
  ## Group annotations by type
  ann_class_normal <- c("NF", "NJ")
  x$ann[which(x$ann %in% ann_class_normal)] <- "NN"
  
  ann_class_arrthm <- c("NS", "NV", "NA")
  x$ann[x$ann %in% ann_class_arrthm] <- "NX"
  
  ## Adjust annotations of RRIs after arrhythmias
  which_after_arrthm <- which(x$ann == "NX") + 1
  which_after_arrthm <- which_after_arrthm[which_after_arrthm <= nrow(x)]
  which_after_arrthm <- which_after_arrthm[x$ann[which_after_arrthm] == "NN"]
  x$ann[which_after_arrthm] <- "XN"
  
  return(x)
})


rm(rr_list) ; gc()



###############################################################################
####    Create feature DF    ##################################################
###############################################################################

p_MITBIHSNR <- 0.33

# t1 <- Sys.time()
rr_feat_list <- vector("list", len(rr_adjann_list))
# for(i in 1:len(rr_adjann_list)) {
for(i in 19:len(rr_adjann_list)) {
  
  rr_df <- rr_adjann_list[[i]]
  
  # Generate features
  rr_feat_out <- generateFeatures_RRwindow(rr_df$RRI, rr_df$RRV, winsize=5)
  rr_feat_out <- data.table("class"=rr_df$ann, rr_feat_out$features_RR)[-rr_feat_out$feature_RR_NArows, ]
  
  ##  Balance data by undersampling
  ##################################
  
  which_nn <- which(rr_feat_out$class == "NN")
  
  # MITBIHSNR data: drop predefined proportion of NN data obs
  if(unl(lapply(strsplit(names(rr_adjann_list), "_"), function(x) x[[1]]))[i] == "MIT-BIH")
    p_NN <- p_MITBIHSNR
  # CAST data: keep amount of NN obs proportional to amount of arrhythmias
  else 
    p_NN <- 1 - len(which_nn)/nrow(rr_feat_out)
    
  # Determine amount of NN obs to keep
  n_NN <- ceiling(p_NN*len(which_nn))
  if(n_NN < 100) n_NN <- 100
    
  # Undersample feature matrix
  rr_feat_out <- rr_feat_out[c(sample(which_nn, n_NN), which(rr_feat_out$class != "NN")), ]
    
  rr_feat_list[[i]] <- rr_feat_out
  catProgress(i, len(rr_adjann_list))
}
# t2 <- Sys.time()


save(rr_feat_list, file="rr_feat_list.rdata")


# Total N of obs
( n_all <- sum(unl(lapply(rr_feat_list, nrow))) )

# Class representation
table(unl(lsel(rr_feat_list, "class")))/n_all





