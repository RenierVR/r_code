source("../useful_funs.r")

load("MITBIHNSR_rr_list.rdata")
load("CAST_rr_list_compensated.rdata")
CAST_rr_list <- rr_list

valid_RR_symbols <- c("N", "L", "R", "B", "A", "a", "J", "S", "V", "r", "F", "e", "j", "n", "E", "/", "f")
valid_RR_bounds <- c(100, 3000)

##########################################################################

CAST_rr_list <- lapply(CAST_rr_list, function(x){ x$t <- cumsum(x$RRI)/1000 ; return(x) })
# CAST_rr_list <- lapply(CAST_rr_list, function(x) x[x$ann %in% c(valid_RR_symbols, "Q"), ])
MITBIHNSR_rr_list <- lapply(MITBIHNSR_rr_list, function(x){ x$t <- x$t - x$t[1] + x$RRI[1]/1000 ; return(x) })

rr_list <- c(MITBIHNSR_rr_list, CAST_rr_list)
rm(MITBIHNSR_rr_list, CAST_rr_list) ; gc()

##########################################################################


generateFeature_DB <- function(rr, rrv,
                               THRESH_X_UPPER = 1000, THRESH_X_LOWER = 100,
                               THRESH_SD_UPPER = 3, THRESH_SD_LOWER = 1)
{ ## Generates "decision bounds" feature from vector of RRIs 'rr'
  ##   by constructing linear bounds based on covariance in 
  ##   Poincare plot and naively counting points outside
  ##   these bounds as anomalous.
  
  rr_poinc_df <- data.frame(x=rr[1:(len(rr)-1)], y=rr[-1])
  rr_poinc_df_invalid <- sort(c(which(rrv == 0), which(rrv == 0)-1))
  if(len(rr_poinc_df_invalid) > 0)
    rr_poinc_df <- rr_poinc_df[-sort(c(which(rrv == 0), which(rrv == 0)-1)), ]
  
  # rr_poinc_cov <- sqrt(abs(cov(rr_poinc_df$x, rr_poinc_df$y)))
  # rr_poinc_cov <- sqrt(var(rr_poinc_df$x))
  rr_poinc_cov <- sqrt(median((rr_poinc_df$x-mean(rr_poinc_df$x))^2))
  
  y1_1 <- THRESH_X_UPPER + THRESH_SD_UPPER*rr_poinc_cov
  y2_1 <- THRESH_X_LOWER + THRESH_SD_LOWER*rr_poinc_cov
  
  y1d <- THRESH_X_UPPER - THRESH_SD_UPPER*rr_poinc_cov
  x1_2 <- (y1_1-y1d)/2 + THRESH_X_UPPER
  y1_2 <- x1_2 - THRESH_SD_UPPER*rr_poinc_cov
  
  y2d <-  THRESH_X_LOWER - THRESH_SD_LOWER*rr_poinc_cov
  x2_2 <- (y2_1-y2d)/2 + THRESH_X_LOWER
  y2_2 <- x2_2 - THRESH_SD_LOWER*rr_poinc_cov
  
  b <- c((y1_1 - y2_1)/(THRESH_X_UPPER-THRESH_X_LOWER),
         (y1_2 - y2_2)/(x1_2 - x2_2))
  a <- c(y1_1 - THRESH_X_UPPER*b[1],
         y1_2 - x1_2*b[2])
  
  f1 <- function(x) return( b[1]*x + a[1])
  f2 <- function(x) return( b[2]*x + a[2])
  
  return(list("poincare_df"=rr_poinc_df, "a_vec"=a, "b_vec"=b,
              "in_DB"=apply(rr_poinc_df, 1, function(x) as.num(isIn(x[2], c(f2(x[1]), f1(x[1]))))) ))
}


generateFeature_DB <- function(rr, rrv, const_var,
                               BOUND_X1=100, BOUND_X2=1000, BOUND_X3=2000,
                               BOUND_SD1=1, BOUND_SD2=3, BOUND_SD3=6)
{ ## Generates "decision bounds" feature from vector of RRIs 'rr'
  ##   by constructing linear bounds based on covariance in 
  ##   Poincare plot and naively counting points outside
  ##   these bounds as anomalous.
  
  rr_poinc_df <- data.frame(x=rr[1:(len(rr)-1)], y=rr[-1])
  rr_poinc_df_invalid <- sort(c(which(rrv == 0), which(rrv == 0)-1))
  if(len(rr_poinc_df_invalid) > 0)
    rr_poinc_df <- rr_poinc_df[-sort(c(which(rrv == 0), which(rrv == 0)-1)), ]
  
  if(missing(const_var)) 
    rr_poinc_mad <- sqrt(median((rr_poinc_df$x-mean(rr_poinc_df$x))^2))
  else
    rr_poinc_mad <- const_var
  
  getCorrespondingPoints <- function(x1, a)
  {
    y1 <- x1 - a
    x2 <- (y1 - (x1+a))/2 + x1
    return(cb(c(x1, y1), c(x2, x1)))
  }
  
  getLineFromPoints <- function(xy1, xy2)
  {
    b <- (xy2[2]-xy1[2])/(xy2[1]-xy1[1])
    a <- xy1[2] - b*xy1[1]
    return(c(a, b))
  }
  
  p1 <- getCorrespondingPoints(BOUND_X1, BOUND_SD1*rr_poinc_mad)
  p2 <- getCorrespondingPoints(BOUND_X2, BOUND_SD2*rr_poinc_mad)
  p3 <- getCorrespondingPoints(BOUND_X3, BOUND_SD3*rr_poinc_mad)
  
  lower_ab1 <- getLineFromPoints(p1[,1], p2[,1])
  lower_ab2 <- getLineFromPoints(p2[,1], p3[,1])
  upper_ab1 <- getLineFromPoints(p1[,2], p2[,2])
  upper_ab2 <- getLineFromPoints(p2[,2], p3[,2])
  
  if(upper_ab2[2] < 0)
    upper_ab2 <- c(max(valid_RR_bounds), 0)
  
  f_lower <- function(x)
  {
    if(x <= p2[1,1])
      return( lower_ab1[1] + x*lower_ab1[2])
    else
      return( lower_ab2[1] + x*lower_ab2[2])
  }
  
  f_upper <- function(x)
  {
    if(x <= p2[1,2])
      return( upper_ab1[1] + x*upper_ab1[2])
    else
      return( upper_ab2[1] + x*upper_ab2[2])
  }

  xx <- seq(0, 3000, 1)

  return(list("poincare_df"=rr_poinc_df, "poincare_var"=sqrt(median((rr_poinc_df$x-mean(rr_poinc_df$x))^2)),
              "lines"=cb(xx, sapply(xx, f_upper), sapply(xx, f_lower)),
              "in_DB"=apply(rr_poinc_df, 1, function(x) as.num(isIn(x[2], c(f_lower(x[1]), f_upper(x[1]))))) ))
}



##########################################################################
#####  Apply feature generation  #########################################
##########################################################################

# candidate_var_consts <- c(55, 65, 75)
# cand_res <- NULL
# for(cand in candidate_var_consts) {

DB_eval_list <- vector("list", len(rr_list))
for(i in 1:len(rr_list)) {
# for(i in c(1:20, 221:223)) {
  
  rr_df <- rr_list[[i]]
  
  ## Indicate validity based on annotation or RRI magnitude
  rr_df$RRV <- as.num(rr_df$ann %in% valid_RR_symbols)
  rr_df$RRV[rr_df$RRI > max(valid_RR_bounds)] <- 0
  rr_df$RRV[sum(rr_df$RRI < min(valid_RR_bounds))] <- 0
  
  which_rrv0 <- which(rr_df$RRV == 0)
  rr_df$RRV[which_rrv0[which_rrv0 < nrow(rr_df)] + 1] <- 0
  
  ## Adjust annotations of RRIs after arrhythmias
  which_ndash <- which(rr_df$ann != "N") + 1
  which_ndash <- which_ndash[which_ndash <= nrow(rr_df)]
  which_ndash <- which_ndash[rr_df[which_ndash, 2] == "N"]
  rr_df[which_ndash, 2] <- "N-"
  
  ## Generate Poincare Decision Bound feature
  feature_DB <- generateFeature_DB(rr=rr_df$RRI, rrv=rr_df$RRV, 65)
  
  DB_eval_list[[i]] <- list("ann_df"=rr_df[, -match(c("RRI", "t"), names(rr_df))],
                            "poincare_var"=feature_DB$poincare_var,
                            "feature_DB"=table(feature_DB$in_DB))
  
  ## Plot
  # png(paste0("./output/feature_DB_eval/", format_ind(i, 4), ".png"), width=1000, height=1000)
  # 
  # cols <- rep("blue", nrow(rr_df))
  # cols[rr_df$ann == "N-"] <- "forestgreen"
  # cols[!(rr_df$ann %in% c("N", "N-"))] <- "red"
  # 
  # cols_inv <- c(which(rr_df$RRV == 0), which(rr_df$RRV == 0)+1)
  # if(len(cols_inv) > 0) cols <- cols[-cols_inv]
  # 
  # # w(8,8)
  # plot_g(feature_DB$poincare_df$x, feature_DB$poincare_df$y, cex=0.75, col=cols,
  #        xlim=c(200, 2000), ylim=c(200, 2000), pch=c(1, 19)[feature_DB$in_DB+1])
  # abline(a=0, b=1, lty=2)
  # lines(feature_DB$lines[,1], feature_DB$lines[,2], lwd=2, col="black")
  # lines(feature_DB$lines[,1], feature_DB$lines[,3], lwd=2, col="black")
  # mtext(paste("variation =", round(feature_DB$poincare_var, 2)))
  # # abline(a=feature_DB$a_vec[1], b=feature_DB$b_vec[1], col="orange", lwd=1)
  # # abline(a=feature_DB$a_vec[2], b=feature_DB$b_vec[2], col="orange", lwd=1)
  # 
  # invisible(dev.off())
  
  catProgress(i, len(rr_list))
}

# save(DB_eval_list, file="feature_DB_eval_list.rdata")




##########################################################################
#####  Evaluate feature  #################################################
##########################################################################

res_agg <- lapply(DB_eval_list, function(x) {
  val <- x$ann_df$ann[x$ann_df$RRV == 1]
  if(len(x$feature_DB) == 2)
    x2 <- as.num(x$feature_DB)
  else
    x2 <- c(0, as.num(x$feature_DB))
  
  return( c("abn"=sum(val != "N"), "all"=len(val), 
            "DB_0"=x2[1], "DB_1"=x2[2]) )
})

res_mat <- matrix(unl(lapply(res_agg, function(x) 100*c(x[1]/x[2], x[3]/x[4]) )), ncol=2, byrow=T)

# cand_res <- rbind(cand_res, cor_full(res_mat))

# }



## With old function (just two SD points)
# Pearson:  0.8435 
# Spearman: 0.8832 

## With new function and fixed var=100
# Pearson:  0.7557 
# Spearman: 0.9268 



# cb(candidate_var_consts, cand_res)
# > cb(candidate_var_consts, cand_res)
# candidate_var_consts   pearson  spearman
# [1,]                   40 0.8149563 0.8504811
# [2,]                   45 0.8307040 0.8775721
# [1,]                   50 0.8424000 0.8963861
# [1,]                   55 0.8483224 0.9094771
# [2,]                   60 0.8509390 0.9193106
# [2,]                   65 0.8497467 0.9256211 *
# [3,]                   70 0.8455038 0.9293706
# [3,]                   75 0.8384983 0.9309859
# [4,]                   80 0.8274881 0.9316644
# [5,]                   90 0.7963918 0.9315465
# [6,]                  100 0.7557228 0.9268148
# [7,]                  110 0.7135022 0.9220980
# [8,]                  120 0.6788867 0.9127274
# [9,]                  130 0.6701986 0.9005329
# [10,]                  140 0.6792670 0.8835430
# [11,]                  150 0.6817739 0.8680676



# x <- CAST_rr_list[[1]]
# x$t <- cumsum(x$RRI)/1000
# 
# scrollPlot(start=0, window=F, winsize=60, n=max(x$t), plotfun=function(xi){
#   
#   which_rr <- which_in(x$t, xi)
#   which_ann <- which(x$ann[which_rr] != "N")
#   
#   lplot_g(x$t[which_rr], x$RRI[which_rr], ylim=c(400,1200))
#   points(x$t[which_rr], x$RRI[which_rr], pch=19, cex=0.75)
#   if(len(which_ann) > 0) {
#     abline(v=x$t[which_rr][which_ann], col="red", lty=2)
#     mtext(x$ann[which_rr][which_ann], at=x$t[which_rr][which_ann], col="red")
#   }
#   
# })

w()
par(mfrow=c(2,1), mar=c(4, 4, 1, 1))

plot_g(res_mat[,1], res_mat[,2], 
             ylab="true arrhythmias", xlab="Poincare DB feature")


plot_g(log(res_mat[,1]), log(res_mat[,2]), col="red", 
             ylab="LOG true arrhythmias", xlab="LOG Poincare DB feature")


savePlot("output/pythline/mmi_results.png", "png")


