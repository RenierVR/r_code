source("../useful_funs.r")
library(rhdf5)

h5list <- getDataFromDir("~/Data/MIT-BIH/", ext="h5", dataE=FALSE)
load("pks_ann_list.rdata")

fs <- 360


################################
##  Process data iteratively  ##
################################

process_MITBIH_data <- function(i, 
                                VALID_RR_SYMBOLS = c("N", "A", "V", "/", "f", "F", "j", "L"), 
                                VALID_BEAT_SYMBOLS = c("Q")) 
{

  # i <- 14
  
  ## Read raw signals and annotations from H5 file
  dat_sig <- h5read(h5list[i], "signals_ECG_d", compoundAsDataFrame=T)$table[, -1]
  dat_ann <- cb(h5read(h5list[i], "annotation_sample_locations", compoundAsDataFrame=T)$table[, -1],
                h5read(h5list[i], "annotation_symbols", compoundAsDataFrame=T)$table[, -1],
                h5read(h5list[i], "annotation_aux_notes", compoundAsDataFrame=T)$table[, -1])
  
  
  ## Create structure to store peaks, RRIs and annotations
  res <- pks_ann_list[[i]]
  pks_df <- data.frame("peak"=res$i_pks/fs, 
                      "ann_x"=res$i_ann[res$i_match]/fs,
                      "ann"=dat_ann[res$i_match, 2], stringsAsFactors=FALSE)
  
  
  ## Check that peak annotation locations are negligible
  # cat(res$i_ann %=% as.num(dat_ann[,1]), "\n")
  # w() ; lplot_g(pks_df$peak - pks_df$ann_x)
  
  
  ## Find peaks with no annotations, and vice versa
  pks_sans_ann <- res$i_pks[which(is.na(res$i_match))]
  ann_sans_pks <- res$i_ann[-na.omit(res$i_match)] 
  cat("Peaks/no anno count =", len(pks_sans_ann), "\nAnno/no peaks count =", len(ann_sans_pks), "\n\n")
  
  
  ## Try to find matching peaks for 'pks_sans_ann'
  which_pk_match <- NA
  if(len(pks_sans_ann) > 0) {
    which_pk_match <- apply(rbind(sapply(pks_sans_ann, function(x) abs(x - ann_sans_pks))), 2, 
                            function(y) c(min(y), which.min(y)))
    # cat("Diff-index matrix pre-match:\n")
    # print(which_pk_match)
    
    for(j in 1:ncol(which_pk_match))
      if(which_pk_match[1, j] < fs) {
        pks_df[which(res$i_pks == pks_sans_ann[j]), ]$ann_x <- ann_sans_pks[which_pk_match[2, j]]/fs
        pks_df[which(res$i_pks == pks_sans_ann[j]), ]$ann <- dat_ann[which(res$i_ann == ann_sans_pks[which_pk_match[2, j]]), 2]
        which_pk_match[1, j] <- 0
      }
    
    # cat("\nDiff-index matrix post-match:\n")
    # print(which_pk_match)
    
    cat("Unmatched peaks after second-stage matching =", sum(which_pk_match[1,] > 0), "\n")
  }
  
  
  ## Review peaks matched to annotations in second stage
  print(pks_df[match(pks_sans_ann, res$i_pks), ])
  
  
  ## Review remaining annotations not corresponding to peaks
  if(!all(is.na(which_pk_match)))
    ann_sans_pks <- ann_sans_pks[-which_pk_match[2, which_pk_match[1,] == 0]]
  ann_other_df <- data.frame("ann_x"=ann_sans_pks/fs, "ann"=dat_ann[match(ann_sans_pks, res$i_ann), 2])
  cat("Unmatched annotations after second-stage:\n")
  print(ann_other_df)
  
  
  ## Amend any remaing unannotated peaks
  if(sum(apply(pks_df, 1, function(x) any(is.na(x)))) > 0) {
    w_na <- which(apply(pks_df, 1, function(x) any(is.na(x))))
    pks_df$ann_x[w_na] <- pks_df$peak[w_na]
    pks_df$ann[w_na] <- "?"
  }
  
  
  
  
  ## Manual correction of peak detection missed beats & missed classifications
  corrections <- ann_other_df$ann_x[ann_other_df$ann %in% c(VALID_RR_SYMBOLS, VALID_BEAT_SYMBOLS)]
  corrections <- corrections[corrections > pks_df$peak[pks_df$ann %in% c(VALID_RR_SYMBOLS, VALID_BEAT_SYMBOLS)][1]]
  # source("MITBIH_correct_raw_data.r")
  # corr_list <- MITBIH_pks_corrections[[which(names(MITBIH_pks_corrections) == paste0("patient", format_ind(i)))]]
  # if(!is.null(corr_list$add_ann_other))
  if(len(corrections) > 0)
    for(j in 1:len(corrections)) {
      which_pk_correct <- which.min(abs(ann_other_df[,1] - corrections[j]))
      pk_correct <- ann_other_df[which_pk_correct, ]
      pk_correct <- cb("peak"=pk_correct$ann_x, pk_correct)
      i_slice <- which(pks_df[,1] > pk_correct$ann_x)[1]
      
      pks_df <- rbind(pks_df[1:(i_slice-1),], pk_correct, pks_df[i_slice:nrow(pks_df),])
      ann_other_df <- ann_other_df[-which_pk_correct,]
    }
  cat(len(corrections), "missed beats corrected")
  
  
  cat("\nAll annotations:\n") ; print(table(dat_ann[,2]))
  
  
  ## Create validity indicators for beats and RRs
  pks_df$valid_RR <- pks_df$ann %in% VALID_RR_SYMBOLS
  pks_df$valid_beat <- pks_df$valid_RR | (pks_df$ann %in% VALID_BEAT_SYMBOLS)
  # pks_df$peak <- pks_df$ann_x
  
  rr_df <- pks_df[pks_df$valid_beat, -which(names(pks_df) == "valid_beat")]
  rr_df$RRI <- c(0, 1000*diff(rr_df$peak))
  
  
  ## Combine remaining non-beat annotations
  ann_other_df <- rbind(ann_other_df,
                        pks_df[!pks_df$valid_beat, -match(c("peak", "valid_beat", "valid_RR"), names(pks_df))])
  ann_other_df <- ann_other_df[order(ann_other_df$ann_x), ]
  
  
  ## Plot data
  scrollPlot(start=1, winsize=fs*5, overlap=fs*5*3/4, n=nrow(dat_sig), 
             plotfun=function(xi){
               
               # w()
               # Current plot window logistics
               w_idx <- which(round(rr_df$peak*fs, 0) %in% xi)
               w_idx_ann <- which(round(ann_other_df$ann_x*fs, 0) %in% xi)
               
               par(mar=c(0.5,5,0,1), oma=c(5,0,4,0))
               layout(c(1, 2), heights=c(2/3, 1/3))
               
               # First plot (ECG, peaks, annotations)
               plot_g(xi/fs, dat_sig[xi, 1], xaxt="n", ylab="ECG", type="n")
               
               abline(v=rr_df$peak[w_idx], lwd=2, lty=1, col="mediumseagreen")
               # abline(v=pks_sans_ann, lwd=2, col="blue")
               
               cols <- rep("orange", len(w_idx))
               cols[rr_df$ann[w_idx] != "N"] <- "red"
               mtext(rr_df$ann[w_idx], side=3, at=rr_df$ann_x[w_idx], col=cols)
               abline(v=rr_df$ann_x[w_idx], lwd=2, lty=2, col=cols)
               
               if(len(w_idx_ann) > 0) {
                 mtext(ann_other_df$ann[w_idx_ann], side=3, at=ann_other_df$ann_x[w_idx_ann], col="blue")
                 abline(v=ann_other_df$ann_x[w_idx_ann], lwd=2, col="blue")
               }
               
               lines(xi/fs, dat_sig[xi, 1], col="black", lwd=2)
               
               # Second plot (RRIs)
               plot_g(xi/fs, rep(0, len(xi)), ylab="RRI (ms)", ylim=range(rr_df$RRI[w_idx]), type="n")
               
               abline(v=rr_df$ann_x[w_idx], lwd=2, lty=2, col=cols)
               if(len(w_idx_ann) > 0)
                 abline(v=ann_other_df$ann_x[w_idx_ann], lwd=2, col="blue")
               
               points(rr_df$peak[w_idx], rr_df$RRI[w_idx], pch=19, cex=0.75)
               lines(rr_df$peak[w_idx], rr_df$RRI[w_idx])
               title(xlab="time (s)", xpd=NA)
               
             }, fig_path=paste0("output/fig/patient", format_ind(i), "/ecg_rr_patient", format_ind(i), "_"))
  
  
  print(ann_other_df)
  
}


i <- 14
dat_ann <- h5read(h5list[i], "annotation_symbols", compoundAsDataFrame=T)$table[, -1]
table(dat_ann)

process_MITBIH_data(i, 
                    VALID_RR_SYMBOLS = c("N", "A", "V", "/", "f", "F", "j", "L", "a", "J"), 
                    VALID_BEAT_SYMBOLS = c("Q")) 


##########################################################################
# OTHER NOTES:
# - wean pks_sans_ann matched, perhaps use timestamp of anno, not beat?

