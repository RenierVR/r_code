source("../useful_funs.r")
library(rhdf5)

h5list <- getDataFromDir("~/Data/MIT-BIH/", ext="h5", dataE=FALSE)
# load("pks_ann_list.rdata")

fs <- 360


################################
##  Process data iteratively  ##
################################

valid_RR_symbols <- c("N", "L", "R", "B", "A", "a", "J", "S", "V", "r", "F", "e", "j", "n", "E", "/", "f")

process_MITBIH_data <- function(i, WIN_SIZE = 25,
                                VALID_RR_SYMBOLS = c("N", "A", "V", "/", "f", "F", "j", "L"), 
                                VALID_BEAT_SYMBOLS = c("Q")) 
{
  
  # i <- 1
  
  ## Read raw signals and annotations from H5 file
  dat_sig <- h5read(h5list[i], "signals_ECG_d", compoundAsDataFrame=T)$table[, -1]
  dat_ann <- cb(h5read(h5list[i], "annotation_sample_locations", compoundAsDataFrame=T)$table[, -1],
                h5read(h5list[i], "annotation_symbols", compoundAsDataFrame=T)$table[, -1],
                h5read(h5list[i], "annotation_aux_notes", compoundAsDataFrame=T)$table[, -1])
  
  
  ## Create structure to store peaks, RRIs and annotations
  pks_df <- data.frame("ann_x"=as.num(dat_ann[,1])/fs,
                       "ann"=dat_ann[, 2], stringsAsFactors=FALSE)
  
  
  cat("\nAll annotations:\n") ; print(table(dat_ann[,2])) ; cat("\n")
  
  
  ## Create validity indicators for beats and RRs
  pks_df$valid_RR <- pks_df$ann %in% VALID_RR_SYMBOLS
  pks_df$valid_beat <- pks_df$valid_RR | (pks_df$ann %in% VALID_BEAT_SYMBOLS)

  rr_df <- pks_df[pks_df$valid_beat, -which(names(pks_df) == "valid_beat")]
  rr_df$RRI <- c(0, 1000*diff(rr_df$ann_x))
  
  
  ## Combine remaining non-beat annotations
  ann_other_df <- pks_df[!pks_df$valid_beat, -match(c("valid_beat", "valid_RR"), names(pks_df))]
  ann_other_df <- ann_other_df[order(ann_other_df$ann_x), ]
  
  
  ## Plot data
  scrollPlot(start=1, winsize=fs*WIN_SIZE, overlap=fs*WIN_SIZE*3/4, n=nrow(dat_sig), 
             plotfun=function(xi){
               
               # w()
               # Current plot window logistics
               w_idx <- which(round(rr_df$ann_x*fs, 0) %in% xi)
               w_idx_ann <- which(round(ann_other_df$ann_x*fs, 0) %in% xi)
               
               par(mar=c(0.5,5,0,1), oma=c(5,0,4,0))
               layout(c(1, 2), heights=c(2/3, 1/3))
               
               # First plot (ECG, peaks, annotations)
               plot_g(xi/fs, dat_sig[xi, 1], xaxt="n", ylab="ECG", type="n")
               
               if(len(w_idx) > 0) {
                 cols <- rep("orange", len(w_idx))
                 cols[rr_df$ann[w_idx] != "N"] <- "red"
                 mtext(rr_df$ann[w_idx], side=3, at=rr_df$ann_x[w_idx], col=cols)
                 abline(v=rr_df$ann_x[w_idx], lwd=2, lty=2, col=cols)
               }
               
               if(len(w_idx_ann) > 0) {
                 mtext(ann_other_df$ann[w_idx_ann], side=3, at=ann_other_df$ann_x[w_idx_ann], col="blue")
                 abline(v=ann_other_df$ann_x[w_idx_ann], lwd=2, col="blue")
               }
               
               lines(xi/fs, dat_sig[xi, 1], col="black", lwd=2)
               
               # Second plot (RRIs)
               yr <- c(0, 1000)
               if(len(w_idx) > 0)
                  yr <- range(rr_df$RRI[w_idx])
               
               plot_g(xi/fs, rep(0, len(xi)), ylab="RRI (ms)", ylim=yr, type="n")
               
               if(len(w_idx) > 0)
                 abline(v=rr_df$ann_x[w_idx], lwd=2, lty=2, col=cols)
               if(len(w_idx_ann) > 0)
                 abline(v=ann_other_df$ann_x[w_idx_ann], lwd=2, col="blue")
               
               if(len(w_idx) > 0) {
                 points(rr_df$ann_x[w_idx], rr_df$RRI[w_idx], pch=19, cex=0.75)
                 lines(rr_df$ann_x[w_idx], rr_df$RRI[w_idx])
               }
               title(xlab="time (s)", xpd=NA)
               
             }, fig_path=paste0("output/fig/patient", format_ind(i), "/ecg_rr_patient", format_ind(i), "_"))
  
  
  print(ann_other_df)
  
  invisible(return(list("RR_DF"=rr_df, "ANN_OTHER_DF"=ann_other_df)))
  
}


i <- 29
dat_ann <- h5read(h5list[i], "annotation_symbols", compoundAsDataFrame=T)$table[, -1]
table(dat_ann)

# rr_res_list <- vector("list", len(h5list))
# save(rr_res_list, file="rr_res_list.rdata")

for(i in 29) {
  cat("Processing file '", h5list[i], "'...\n")
  rr_res_list[[i]] <- process_MITBIH_data(i, WIN_SIZE = 25,
                                          VALID_RR_SYMBOLS = valid_RR_symbols, 
                                          VALID_BEAT_SYMBOLS = c("Q")) 
  cat("\n\n")
}


