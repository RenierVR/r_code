source("../useful_funs.r")
source("../useful_funs_ggplot.r")
library(ggplot2)
library(data.table)

dat_events <- fread("~/Data/MMI_2018-03-13_Calculated-Concatenated/temp/out_events.csv")
dat_tm <- fread("~/Data/MMI_2018-03-13_Calculated-Concatenated/temp/out_tablemerged.csv")

colnames(dat_events) <- c("index", "ss_events")
colnames(dat_tm) <- c("index", colnames(dat_tm)[-1])

dat_events$index <- as.POSIXct2(dat_events$index/1e9)
dat_tm$index <- as.POSIXct2(dat_tm$index/1e9)

###############################################################################
###############################################################################

ggdat <- widen(dat_tm[, -ncol(dat_tm), with=F])
gg <- ggplot(ggdat, aes(x=index, y=value, col=variable))
gg <- gg + geom_line()
w() ; plot(gg)



###############################################
## Investigate correlations
###############################################

# Daily correlations for single subject
dat_tm_split <- split_on_datetime(dat_tm)
for(i in 1:len(dat_tm_split)) {
  dat <- dat_tm_split[[i]]
  if(nrow(dat) == 0) next
  
  gg <- wgg_corr(dat[, mnn:sdsd], win=F, cluster_cor_matrix=F)
  ggsave(paste0("./output/pythline/daily_cormat_", format_ind(i, 3), ".png"), gg, scale=2)
  
  catProgress(i, len(dat_tm_split))
}

# Correlation plots for all subjects
dat_list <- getDataFromDir("~/Data/MMI_2018-03-13_Calculated-Concatenated/temp/out/", dataE=F)
dat_list <- dat_list[grep("tablemerged", dat_list)]

for(i in 1:len(dat_list)) {
  dat_tm <- fread(dat_list[i])
  colnames(dat_tm) <- c("index", colnames(dat_tm)[-1])
  dat_tm$index <- as.POSIXct2(dat_tm$index/1e9)
  
  gg <- wgg_corr(dat_tm[, mnn:sdsd], win=F, cluster_cor_matrix=F)
  ggsave(paste0("./output/pythline/cormat_", format_ind(i, 3), ".png"), gg, scale=2)
  
  catProgress(i, len(dat_list))
}


###############################################
## Plot daily events
###############################################

dat_e_split <- split_on_datetime(dat_events)
dat_tm_split <- split_on_datetime(dat_tm)

i <- 1
for(i in 1:len(dat_tm_split)) {
  dat <- dat_tm_split[[i]]
  which_dat_e <- which(labels(dat_tm_split)[i] == labels(dat_e_split))
  dat_e <- dat_e_split[[which_dat_e]]
  
  # evnt_start <- c(1, which(diff(as.num(as.factor(dat_e$ss_events))) != 0) + 1)
  # evnt_stop <- c(evnt_start[-1] -1, nrow(dat))
  
  dat_e <- data.table("xmin"=dat_e$index[-nrow(dat_e)],
                      "xmax"=dat_e$index[-1], "ss"=dat_e$ss_events[-nrow(dat_e)])
  dat_e <- dat_e[ss != "WAKE_DAY"]
  
  ggdat <- widen(dat[, -ncol(dat), with=F])
  gg <- ggplot()
  # gg <- gg + geom_line(data=ggdat, aes(x=index, y=value, col=variable))
  gg <- gg + geom_point(data=ggdat, aes(x=index, y=value, col=variable))
  gg <- gg + scale_color_brewer(palette="Dark2")
  # gg <- gg + scale_color_gradient(low="yellow", high="darkred")
  
  gg <- gg + geom_rect(data = dat_e, alpha=0.3, aes(xmin=xmin, xmax=xmax, ymin=-Inf, ymax=Inf, fill=ss))
  gg <- gg + scale_fill_brewer(palette="YlOrRd")
  # gg <- gg + scale_fill_gradient(low="yellow", high="darkred", guide="legend")
  
  ggsave(paste0("./output/pythline/daily_hrv_", format_ind(i, 3), ".png"), gg,
         width=6, height=3, scale=2)
  
}


###############################################
## Investigate SS events and wake events
###############################################

# wake_starts <- NULL
# for(i in 1:(nrow(dat_events)-1)) {
#   if(tail(unl(strsplit(dat_events$ss_events[i], "_")), 1) == "STOP") {
#     stop_diff <- diff(as.num(dat_events$index[i:(i+1)]))
#     if(stop_diff > 0)
#       wake_starts <- c(wake_starts, i)
#   }
# }
# 
# wake_events <- data.table(index=dat_events$index[wake_starts], ss_events="WAKE_DAY")
# dat_e_all <- dat_events[which(unl(lapply(strsplit(dat_events$ss_events, "_"), function(x) tail(x,1) == "START"))), ]
# dat_e_all$ss_events <- unl(lapply(strsplit(dat_e_all$ss_events, "_"), function(x) x[4]))
# dat_e_all$ss_events[dat_e_all$ss_events == "WAKE"] <- "AWAKENING"
# dat_e_all <- rbind(dat_e_all, wake_events)
# dat_e_all <- dat_e_all[order(dat_e_all$index), ]
