source("../useful_funs.r")
library(data.table)

source("feature_ArrClass_functions.r")

valid_RR_symbols <- c("N", "L", "R", "B", "A", "a", "J", "S", "V", "r", "F", "e", "j", "n", "E", "/", "f")
valid_RR_bounds <- c(100, 3000)



###############################################################################
####    Loading data    #######################################################
###############################################################################

load("rr_feat_list.rdata")
rr_feat_all <- df_from_list(lapply(rr_feat_list, as.data.frame))
# rm(rr_feat_list) ; gc()


###############################################################################
####    Modelling    ##########################################################
###############################################################################


j <- 60
xfeat <- rr_feat_list[[j]][, -which(names(rr_feat_list[[j]]) == "class"), with=F]
y <- rr_feat_list[[j]]$class

# lapply(rr_feat_list, function(x) table(x$class))


# xfeat <- rr_feat_all[, -which("class" == names(rr_feat_all))]
# y <- rr_feat_all$class

# Build and test GLMNET log reg model with 10-fold CV
build_out <- modelBuild_logistic_glmnet(xfeat, y, GLMNET_CV=F, GLMNET_NLAMBDA=25, CV_SEED=1234)
res_list <- modelTest_logistic_glmnet(xfeat, y, build_out)
modelPlot_logistic_glmnet(res_list)

# Build and test GLMNET log reg model with single split
build_out <- modelBuild_logistic_glmnet(xfeat, y, CV_K=1, GLMNET_CV=F, GLMNET_NLAMBDA=75, CV_SEED=1234)
res_list <- modelTest_logistic_glmnet(xfeat, y, build_out)
gg_out <- modelPlot_logistic_glmnet(res_list)
gg_out_b <- modelPlot_logistic_glmnet(res_list_b)


w() ; print(gg_out$summary_ggplot)
w() ; print(gg_out_b$summary_ggplot)


# Build and test GLMNET log reg model with single split

