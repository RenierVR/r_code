source("../useful_funs.r")

dat_list <- list.files("~/Data/MMI_2018-03-13_Calculated-Concatenated/temp/10min_split", full=T)
dat_id <- as.num(unl(lapply(strsplit(dat_list, "_"), function(x) x[[6]])))
dat_list <- dat_list[order(dat_id)]

valid_RR_bounds <- c(100, 3000)

##########################################################################
##########################################################################

generateFeature_DB <- function(rr, rrv, rr_seg_ind, const_var,
                               BOUND_X1=100, BOUND_X2=1000, BOUND_X3=2000,
                               BOUND_SD1=1, BOUND_SD2=3, BOUND_SD3=6)
{ ## Generates "decision bounds" feature from vector of RRIs 'rr'
  ##   by constructing linear bounds based on covariance in 
  ##   Poincare plot and naively counting points outside
  ##   these bounds as anomalous.
  
  if(missing(rr_seg_ind))
    rr_seg_ind <- rep(1, len(rr))
  
  rr_poinc_df <- data.frame(x=rr[1:(len(rr)-1)], y=rr[-1])
  
  rr_poinc_df_invalid <- unique(c(which(rrv == 0), which(rrv == 0)-1))
  rr_poinc_df_invalid <- sort(c(rr_poinc_df_invalid, which(rr_seg_ind == 0) - 1))
  rr_poinc_df_invalid <- rr_poinc_df_invalid[rr_poinc_df_invalid > 0]
  if(len(rr_poinc_df_invalid) > 0)
    rr_poinc_df <- rr_poinc_df[-rr_poinc_df_invalid, ]
  
  if(missing(const_var)) 
    rr_poinc_mad <- sqrt(median((rr_poinc_df$x-mean(rr_poinc_df$x))^2))
  else
    rr_poinc_mad <- const_var
  
  getCorrespondingPoints <- function(x1, a)
  {
    y1 <- x1 - a
    x2 <- (y1 - (x1+a))/2 + x1
    return(cb(c(x1, y1), c(x2, x1)))
  }
  
  getLineFromPoints <- function(xy1, xy2)
  {
    b <- (xy2[2]-xy1[2])/(xy2[1]-xy1[1])
    a <- xy1[2] - b*xy1[1]
    return(c(a, b))
  }
  
  p1 <- getCorrespondingPoints(BOUND_X1, BOUND_SD1*rr_poinc_mad)
  p2 <- getCorrespondingPoints(BOUND_X2, BOUND_SD2*rr_poinc_mad)
  p3 <- getCorrespondingPoints(BOUND_X3, BOUND_SD3*rr_poinc_mad)
  
  lower_ab1 <- getLineFromPoints(p1[,1], p2[,1])
  lower_ab2 <- getLineFromPoints(p2[,1], p3[,1])
  upper_ab1 <- getLineFromPoints(p1[,2], p2[,2])
  upper_ab2 <- getLineFromPoints(p2[,2], p3[,2])
  
  if(upper_ab2[2] < 0)
    upper_ab2 <- c(max(valid_RR_bounds), 0)
  
  f_lower <- function(x)
  {
    if(x <= p2[1,1])
      return( lower_ab1[1] + x*lower_ab1[2])
    else
      return( lower_ab2[1] + x*lower_ab2[2])
  }
  
  f_upper <- function(x)
  {
    if(x <= p2[1,2])
      return( upper_ab1[1] + x*upper_ab1[2])
    else
      return( upper_ab2[1] + x*upper_ab2[2])
  }
  
  xx <- seq(0, 3000, 1)
  
  return(list("poincare_df"=rr_poinc_df, "poincare_var"=sqrt(median((rr_poinc_df$x-mean(rr_poinc_df$x))^2)),
              "lines"=cb(xx, sapply(xx, f_upper), sapply(xx, f_lower)),
              "in_DB"=apply(rr_poinc_df, 1, function(x) as.num(isIn(x[2], c(f_lower(x[1]), f_upper(x[1]))))) ))
}


calcSegLen <- function(x)
{
  if(!any(names(x) == "seg_ind"))
    stop("Segment indicator column required.")
  
  start_ind <- which(x$seg_ind == 0)
  t_diffs <- apply(cb(x$index[start_ind], x$index[c(start_ind[-1] - 1, nrow(x))]), 1, diff)
  
  return(t_diffs/1e3)
}

##########################################################################
##########################################################################

res_mat <- matrix(0, nrow=len(dat_list), nc=2)
for(i in 1:len(dat_list)) {
  
  # Read and format data
  dat <- read.table(dat_list[i], sep=",")
  names(dat) <- c("index", "RRI", "RRV", "RRconf", "seg_ind")
  dat$index <- dat$index/1e6
  
  # Adjust RRV
  dat$RRV[dat$RRI > max(valid_RR_bounds)] <- 0
  dat$RRV[dat$RRI < min(valid_RR_bounds)] <- 0
  
  # Calculate length of continuous segments
  seg_lens <- calcSegLen(dat)
  
  rr <- dat$RRI ; rrv <- dat$RRV ; rr_seg_ind <- dat$seg_ind
  # cb(rr_poinc_df, data.frame(xv=rrv[1:(len(rr)-1)], yv=rrv[-1]), 
  #    si=dat$seg_ind[1:(len(rr)-1)], vvv=1:(len(rr)-1) %in% rr_poinc_df_invalid)
  
  
  # Create Poincare
  DBout <- generateFeature_DB(dat$RRI, dat$RRV, dat$seg_ind, const_var=65)
  res_mat[i,] <- c(sum(DBout$in_DB), len(DBout$in_DB))
  
  catProgress(i, len(dat_list))
}


py_res <- read.table("~/Data/MMI_2018-03-13_Calculated-Concatenated/temp/10min_split/feature_db_results.csv", sep=",")

w() ; lplot_g(res_mat[,1] - py_res[,1])
res_mat %=% py_res
