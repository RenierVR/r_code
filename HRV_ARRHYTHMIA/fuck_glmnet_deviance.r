# https://stats.stackexchange.com/questions/134694/what-deviance-is-glmnet-using-to-compare-values-of-lambda?rq=1
# https://github.com/cran/glmnet/blob/master/R/cv.lognet.R

set.seed(4567)
N       <- 500
P       <- 100
coefs   <- NULL

for(p in 1:P){
  coefs[p]    <- (-1)^p*100*2^(-p)
}

inv.logit <- function(x) exp(x)/(1+exp(x))
X   <- matrix(rnorm(N*P), ncol=P, nrow=N)
Y   <- rbinom(N, size=1, p=inv.logit(cbind(1, X)%*%c(-4, coefs)))

test   <- cv.glmnet(x=X, y=Y, family="binomial", nfolds=10, alpha=0.8)

plot(test)
plot(log(test$glmnet.fit$lambda), deviance.glmnet(test$glmnet.fit))

###############################################################################

y = as.factor(Y)
ntab = table(y)
nc = as.integer(length(ntab))
y = diag(nc)[as.numeric(y), ]

predmat <- predict(test$glmnet.fit, newx=X, type="response")

prob_min = 1e-05
prob_max = 1 - prob_min
predmat=pmin(pmax(predmat, prob_min), prob_max)
lp=y[,1]*log(1-predmat)+y[,2]*log(predmat)
ly=log(y)
ly[y==0]=0
ly=drop((y*ly)%*%c(1,1))
cvraw <- 2*(ly-lp)

cvm = apply(cvraw, 2, mean, na.rm = TRUE)

w() ; lplot(log(test$glmnet.fit$lambda), cvm)



###############################################################################

aaa = function (x, sign.lambda = 1, ...) 
{
  cvobj = x
  xlab = "log(Lambda)"
  if (sign.lambda < 0) 
    xlab = paste("-", xlab, sep = "")
  plot.args = list(x = sign.lambda * log(cvobj$lambda), y = cvobj$cvm, 
                   ylim = range(cvobj$cvup, cvobj$cvlo), xlab = xlab, ylab = cvobj$name, 
                   type = "n")
  new.args = list(...)
  if (length(new.args)) 
    plot.args[names(new.args)] = new.args
  do.call("plot", plot.args)
  # error.bars(sign.lambda * log(cvobj$lambda), cvobj$cvup, cvobj$cvlo, 
  #            width = 0.01, col = "darkgrey")
  points(sign.lambda * log(cvobj$lambda), cvobj$cvm, pch = 20, 
         col = "red")
  axis(side = 3, at = sign.lambda * log(cvobj$lambda), labels = paste(cvobj$nz), 
       tick = FALSE, line = 0)
  abline(v = sign.lambda * log(cvobj$lambda.min), lty = 3)
  abline(v = sign.lambda * log(cvobj$lambda.1se), lty = 3)
  invisible()
}


w() ; aaa(test)



