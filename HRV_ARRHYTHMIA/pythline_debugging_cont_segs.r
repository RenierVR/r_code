source("../useful_funs.r")


###############################################################################
####  Load and format data
###############################################################################


hrv_df <- read.table("~/Data/MMI_2018_05_11/out_pdb_0.csv", head=T, sep=",")
index <- as.num(as.POSIXct(as.char(hrv_df$index), origin="1970-01-01 00:00:00.000"))
index <- index + abs(index[1])

##  Calculations as in pythline
#################################

t_diffs <- 1000*diff(index)
rr_diffs <- hrv_df$metric_rr[-1] - t_diffs

EPSILON <- 30000
rr_seg_ind <- c(0, abs(rr_diffs) < EPSILON)

##  New approach
##################

t_diffs <- 1000*diff(index)
rr_diffs <- hrv_df$metric_rr[-nrow(hrv_df)] - t_diffs

EPSILON <- 5000
rr_seg_ind2 <- c(0, abs(rr_diffs) < EPSILON)



###############################################################################
####  Scroll plot with segment indicator
###############################################################################

scrollPlot(start=1, window=F, winsize=100, n=tail(index,1), plotfun=function(xi){
  
  # print(range(xi))
  which_i <- which((index >= xi[1]) & (index <= tail(xi, 1)))
  
  if(len(which_i) > 0) {
    which_si1 <- index[which_i][which(rr_seg_ind[which_i] == 0)]
    which_si2 <- index[which_i][which(rr_seg_ind2[which_i] == 0)]
    
    plot_g(index[which_i]/3600, hrv_df$metric_rr[which_i])
    if(len(which_si1) > 0) abline(v=which_si1/3600, col="forestgreen", lwd=2)
    if(len(which_si2) > 0) abline(v=which_si2/3600, col="red", lwd=2, lty=2)
  }
  
}, fig_path="./output/preprocessor_debugging/")




###############################################################################
####  Scroll plot with corresponding time diffs
###############################################################################

t_diffs <- 1000*diff(index)
rrs <- data.frame("index"=index, "rr1"=hrv_df$metric_rr, "rr2"=c(t_diffs, 0))

scrollPlot(start=1, window=F, winsize=1200, n=tail(index,1), plotfun=function(xi){
  
  which_i <- which((index >= xi[1]) & (index <= tail(xi, 1)))
  
  if(len(which_i) > 0) {
    
    plot_g(index[which_i]/3600, rrs$rr1[which_i])
    points(index[which_i]/3600, rrs$rr2[which_i], pch=19, col="red", cex=0.75)
    
    lines(index[which_i]/3600, rrs$rr1[which_i], col="blue", lty=2)
    lines(index[which_i]/3600, rrs$rr2[which_i], col="red", lty=3)
    
    which_si <- index[which_i][which(rr_seg_ind2[which_i] == 0)]
    if(len(which_si) > 0) abline(v=which_si/3600, col="red", lwd=2)
  }
  
}, fig_path="./output/preprocessor_debugging/")






