source("../useful_funs.r")
library(rhdf5)
library(data.table)
library(readxl)

dat_list <- getDataFromDir("~/Data/MMI_2018-03-13_Calculated-Concatenated/renier_out_summaries/", ext="h5", dataE=F)
dat_id <- unl(strsplit(unl(lapply(strsplit(dat_list, "/"), function(x) tail(x, 1))), ".h5"))

h5_groups <- h5contents(dat_list[1])$group
h5_devgroups <- h5_groups[startsWith(h5_groups, "/dev_")]

valid_RR_bounds <- c(100, 3000)

###############################################################################
######  Function declaration                                             
###############################################################################


###############################################################################
######  Processing feature: Poincare decision bounds                                            
###############################################################################

for(i in 1:len(dat_list)) {
  
  feat_pdb_raw <- data.table(h5read(dat_list[i], "/dev_poincaredb_10min_count",  bit64conversion="double", compoundAsDataFrame=T)$table)
  feat_pdb_sum <- data.table(h5read(dat_list[i], "/dev_poincaredb_10min_count_sum_M",  bit64conversion="double", compoundAsDataFrame=T)$table)
  
  mn <- feat_pdb_raw$feature_db_within/feat_pdb_raw$feature_db_all
  mn <- mn[!is.na(mn)]
  
  png(paste0("./output/pythline/feature_db_raw_hist_", format_ind(i, 4), ".png"), width=1000, height=1000)
  print(wgg_density(mn, win=F))
  invisible(dev.off())
  
  catProgress(i, len(dat_list))
}


###############################################################################
######  Processing feature: HRV time domain metrics
###############################################################################

for(i in 1:len(dat_list)) {
  
  feat_hrv_raw <- h5read(dat_list[i], "/dev_hrv_metrics_timedomain",  bit64conversion="double", compoundAsDataFrame=T)$table
  feat_hrv_med <- h5read(dat_list[i], "/dev_hrv_metrics_timedomain_median_M",  bit64conversion="double", compoundAsDataFrame=T)$table
  feat_hrv_std <- h5read(dat_list[i], "/dev_hrv_metrics_timedomain_std_M",  bit64conversion="double", compoundAsDataFrame=T)$table
  
  for(key in names(feat_hrv_raw)[-which(names(feat_hrv_raw) == "index")]) {
    png(paste0("./output/pythline/feature_hrv_med_", key, "_hist_", format_ind(i, 4), ".png"), width=1000, height=1000)
    print(wgg_density(feat_hrv_med[, names(feat_hrv_med) == key], win=F))
    invisible(dev.off())
  }
  
  catProgress(i, len(dat_list))
}



###############################################################################
######  Results investigation
###############################################################################


meta_all <- read.table("~/Downloads/combined_dat_for_models.csv", head=T, sep=",")

fdat <- read_excel("~/Downloads/MMI and LifeQ Pilot_Analytics data set_DOH_RUB_HA_UW_25042017_V7 - import.xlsx",
                   sheet = "IMPORT")
fdat <- fdat[-c(1, which(is.na(fdat[,1]))), ]

id_match <- match(fdat$AnonID, dat_id)
cb(fdat$AnonID[!is.na(id_match)], dat_id[na.omit(id_match)])


dat <- fdat[!is.na(id_match), match(c("AnonID", "DecisionType", "Gender", "Age", "Height", "Weight", "BMI", "Cholesterol", 
                                      "BpSystolic", "BpDiastolic", "SmokingHabitsLast6Mths", "CardiacArythmiaSuspectedOrDiagnosed"), names(fdat))]


feat_hrv_df <- setNames(data.frame(matrix(0, nrow=len(dat_list), ncol=13)),
                        c("pdb_prop", paste(rep(c("mnn", "pnn20", "pnn50", "rmssd", "sdnn", "sdsd")), rep(c("med", "std"), each=6), sep="_")))
for(i in 1:len(dat_list)) {
  
  feat_pdb_prop <- h5read(dat_list[i], "/dev_poincaredb_10min_count_prop_M",  bit64conversion="double", compoundAsDataFrame=T)$table
  feat_hrv_med <- h5read(dat_list[i], "/dev_hrv_metrics_timedomain_median_M",  bit64conversion="double", compoundAsDataFrame=T)$table
  feat_hrv_std <- h5read(dat_list[i], "/dev_hrv_metrics_timedomain_std_M",  bit64conversion="double", compoundAsDataFrame=T)$table
  
  med_means <- colMeans(feat_hrv_med)
  std_means <- colMeans(feat_hrv_std)
  pdb_mean <- mean(feat_pdb_prop$values)
  
  feat_hrv_df[i,] <- c(pdb_mean, med_means[-1], std_means[-1])
  
  catProgress(i, len(dat_list))
}

feat_hrv_df <- cb("AnonID"=dat_id, feat_hrv_df)
feat_hrv_df2 <- feat_hrv_df[na.omit(id_match),]


###############################################################################
######  Results investigation
###############################################################################

aspect <- "BMI"
for(i in 2:ncol(feat_hrv_df2)) {
  
  pdat <- cb(feat_hrv_df2[,i], as.num(dat[[which(names(dat) == aspect)]]))
  pdat <- pdat[!apply(pdat, 1, function(x) any(is.na(x))), ]
  
  if(aspect == "BMI") pdat <- pdat[!(pdat[,2] == 0),]
  
  cor_out <- cor.test(pdat[,1], pdat[,2])
  
  # w(8, 8)
  png(paste0("./output/pythline/cor_", aspect, "-", names(feat_hrv_df2)[i], ".png"), width=1000, height=1000)
  plot_g(pdat[,1], pdat[,2], xlab=names(feat_hrv_df2)[i], ylab=aspect,
         main=paste("Pearson cor =", round(cor_out$estimate, 3), ", p-val =", round(cor_out$p.value, 3)))
  abline(lm(pdat[,2] ~ pdat[,1]), lty=2)
  invisible(dev.off())
  
}


aa <- data.frame("DiagnosedOrSuspectedArrhythmia"=as.factor(dat$CardiacArythmiaSuspectedOrDiagnosed), 
                 "Feature_PoincareDB"=feat_hrv_df2$pdb_prop)
aa <- aa[!apply(aa, 1, function(x) any(is.na(x))),]


png(paste0("./output/pythline/feature_pdb_boxplot.png"), width=800, height=800)
boxplot(split(aa[,2], aa[,1]), ylab="Feature_PoincareDB", col=c("pink", "mediumseagreen"),
              main="Boxplots wrt DiagnosedOrSuspectedArrhythmia response")
legend("bottomleft", leg=c("Response:\tNo = 860 / Yes = 17",
                           "Means:\t\tNo = 0.981 / Yes = 0.939",
                           "SD:\t\t\tNo = 0.021 / Yes = 0.104"))
invisible(dev.off())

png(paste0("./output/pythline/feature_pdb_density.png"), width=1000, height=800)
gg <- ggplot(aa, aes(x=Feature_PoincareDB, fill=DiagnosedOrSuspectedArrhythmia))
gg <- gg + geom_density(alpha=0.4) + theme(legend.title = element_blank(), legend.justification = c(0, 1), legend.position = c(0, 1))
print(gg)
invisible(dev.off())



