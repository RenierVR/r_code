source("R_code/useful_funs.r")
source("R_code/useful_funs_ggplot.r")
library(rhdf5)

## Obtain data lists
fp_old <- "~/Data/MMI_RR_oldvsnew/MMI-6-Default-RHR-Through-LEL-v1.16.3_0_oldrr_wqioff-HHAPv0.1.3rc11-HHAv0.1.2rc6/"
fp_new <- "~/Data/MMI_RR_oldvsnew/MMI-6-Default-RHR-Through-LEL-v1.16.1_newrr_wqioff-HHAPv0.1.3rc11-HHAv0.1.2rc6/"
fl_old <- getDataFromDir(fp_old, ext="h5", dataE=F, ret="base")
fl_new <- getDataFromDir(fp_new, ext="h5", dataE=F, ret="base")

f_ids <- unl(strsplit(list(fl_old, fl_new)[[which.min(c(len(fl_old), len(fl_new)))]], ".h5"))
n_ids <- len(f_ids)



#########################
##  Raw RR comparison  ##
#########################

res_list <- vector("list", n_ids)
for(i in 1:n_ids) {
  
  res <- NA
  if(len(grep(f_ids[i], fl_old)) > 0 & len(grep(f_ids[i], fl_new)) > 0) {
    f_old <- paste0(fp_old, fl_old[grep(f_ids[i], fl_old)])
    f_new <- paste0(fp_new, fl_new[grep(f_ids[i], fl_new)])
    
    rr_old <- cb(h5read2(f_old, "/metric/on_device/rr"),
                 rrv=h5read2(f_old, "/metric/on_device/rr_isvalid")[,-1])
    rr_new <- h5read2(f_new, "/metric/on_device/rr")
    
    wrrv <- rr_old$rrv == 1
    
    res <- c("RRold_count"=nrow(rr_old), "RRold_count_valid"=sum(wrrv),
             "RRold_time"=sum(rr_old$value/1000), "RRold_time_valid"=sum(rr_old$value[wrrv]/1000),
             "RRnew_count"=nrow(rr_new), "RRnew_time"=sum(rr_new$value/1000))
  }
  
  res_list[[i]] <- res
  names(res_list)[i] <- f_ids[i]
  
  H5close()
  H5garbage_collect()
  catProgress(i, n_ids)  
}

# save(res_list, file="~/R_code/misc_objects/mmi_hha_rrcomp_dims_rr.Rdata")
load("~/R_code/misc_objects/mmi_hha_rrcomp_dims_rr.Rdata")

## Remove all NA results from list
res_list <- res_list[!unl(lapply(res_list, function(x) any(is.na(x))))]

## Comparitive summary stats
rr_list_hours <- lapply(res_list, function(x) c(x[3], x[4], x[6])/3600)
rr_diffs_mat <- matrix(unl(lapply(rr_list_hours, function(x) as.num(x[3]-x[1:2]))), ncol=2, byrow=T, dimnames=list(NULL, c("RR_new-vs-old", "RR_new-vs-oldvalid")))

w() ; boxplot(as.data.frame(rr_diffs_mat), ylab="hours difference", 
              main="Difference between total RR measurment time: old vs new algorithms")
savePlot("diff_RR_total_time.png", "png")


##########################
##  HHA output lengths  ##
##########################

res_list <- vector("list", n_ids)
for(i in 1:n_ids) {
  
  res_mat <- NA
  if(len(grep(f_ids[i], fl_old)) > 0 & len(grep(f_ids[i], fl_new)) > 0) {
    f_old <- paste0(fp_old, fl_old[grep(f_ids[i], fl_old)])
    f_new <- paste0(fp_new, fl_new[grep(f_ids[i], fl_new)])
    
    res_mat <- matrix(0, nc=2, nrow=5)  
    res_mat[1,] <- c(nrow(h5read2(f_old, "/metric/health_assessment/heart/hr_features")),
                     nrow(h5read2(f_new, "/metric/health_assessment/heart/hr_features")))
    res_mat[2,] <- c(nrow(h5read2(f_old, "/metric/health_assessment/heart/hrv_features")),
                     nrow(h5read2(f_new, "/metric/health_assessment/heart/hrv_features")))
    res_mat[3,] <- c(nrow(h5read2(f_old, "/metric/health_assessment/heart/poincaredb_features")),
                     nrow(h5read2(f_new, "/metric/health_assessment/heart/poincaredb_features")))
    res_mat[4,] <- c(nrow(h5read2(f_old, "/metric/health_assessment/heart/pwf_features")),
                     nrow(h5read2(f_new, "/metric/health_assessment/heart/pwf_features")))
    
    ndim_hha_old <- 0
    ndim_hha_new <- 0
    if("/metric/health_assessment/heart/score" %in% h5contents(f_old)[,1])
      ndim_hha_old <-nrow(h5read2(f_old, "/metric/health_assessment/heart/score"))
    if("/metric/health_assessment/heart/score" %in% h5contents(f_new)[,1])
      ndim_hha_new <-nrow(h5read2(f_new, "/metric/health_assessment/heart/score"))
    
    res_mat[5,] <- c(ndim_hha_old, ndim_hha_new)
    
  }
  
  res_list[[i]] <- res_mat
  names(res_list)[i] <- f_ids[i]
  
  H5close()
  H5garbage_collect()
  catProgress(i, n_ids)  
}

# save(res_list, file="~/R_code/misc_objects/mmi_hha_rrcomp_dims.Rdata")
load("~/R_code/misc_objects/mmi_hha_rrcomp_dims.Rdata")

## Remove all NA results from list
res_list <- res_list[-which(unl(lapply(res_list, function(x) any(is.na(x)))))]

## Graphical presentation of comparative feature sizes
mains <- c(paste(c("HR", "HRV", "PDB", "PWF"), "features"), "HHA results")
w(20,8) ; par(mfrow=c(1, 5))
for(i in 1:5)
  boxplot(apply(matrix(unl(lapply(res_list, function(x) x[i,])), nc=2, byrow=T), 1, diff), main=mains[i])
par(mfrow=c(1,1)) ; mtext("Distributions of differences between RR v1 and RR v2 (X_RR2 - X_RR1) for HHA features and output", 1, 3)

savePlot("HHAPP_output_feature_count.png", "png")


############################
##  HHA output days used  ##
############################

res_list <- vector("list", n_ids)
for(i in 1:n_ids) {
  
  out_list <- list(NA, NA)
  if(len(grep(f_ids[i], fl_old)) > 0 & len(grep(f_ids[i], fl_new)) > 0) {
    f_old <- paste0(fp_old, fl_old[grep(f_ids[i], fl_old)])
    f_new <- paste0(fp_new, fl_new[grep(f_ids[i], fl_new)])
    
    if("/metric/health_assessment/heart/score" %in% h5contents(f_old)[,1]) {
      out_score_old <-h5read2(f_old, "/metric/health_assessment/heart/score")
      out_list[[1]] <- out_score_old[,grep("number_of_days", names(out_score_old))]
    }
    if("/metric/health_assessment/heart/score" %in% h5contents(f_new)[,1]) {
      out_score_new <-h5read2(f_new, "/metric/health_assessment/heart/score")
      out_list[[2]] <- out_score_new[,grep("number_of_days", names(out_score_new))]
    }
  }
  
  res_list[[i]] <- out_list
  names(res_list)[i] <- f_ids[i]
  
  H5close()
  H5garbage_collect()
  catProgress(i, n_ids)  
}


n_NAold_NAnew <- sum(unl(lapply(res_list, function(x) any(is.na(x[[1]])) & any(is.na(x[[2]])))))
n_NAold <- sum(unl(lapply(res_list, function(x) any(is.na(x[[1]])) & !any(is.na(x[[2]])))))
n_NAnew <- sum(unl(lapply(res_list, function(x) !any(is.na(x[[1]])) & any(is.na(x[[2]])))))

res_list_both <- res_list[unl(lapply(res_list, function(x) !any(is.na(x[[1]])) & !any(is.na(x[[2]]))))]
hrv_days_list <- lapply(res_list_both, function(x){
  x1 <- x[[1]]$hrv_number_of_days
  x2 <- x[[2]]$hrv_number_of_days
  
  return(c(x1[1], min(x1), mean(x1), median(x1),
           x2[1], min(x2), mean(x2), median(x2)))
})

hr_days_list <- lapply(res_list_both, function(x){
  x1 <- x[[1]]$hr_number_of_days
  x2 <- x[[2]]$hr_number_of_days
  
  return(c(x1[1], min(x1), mean(x1), median(x1),
           x2[1], min(x2), mean(x2), median(x2)))
})


pwf_days_list <- lapply(res_list_both, function(x){
  x1 <- x[[1]]$pwf_number_of_days
  x2 <- x[[2]]$pwf_number_of_days
  
  return(c(x1[1], min(x1), mean(x1), median(x1),
           x2[1], min(x2), mean(x2), median(x2)))
})

hha_days_list <- lapply(res_list_both, function(x){
  x1 <- x[[1]]$hha_number_of_days
  x2 <- x[[2]]$hha_number_of_days
  
  return(c(x1[1], min(x1), mean(x1), median(x1),
           x2[1], min(x2), mean(x2), median(x2)))
})


days_list <- hha_days_list
c_i1s <- matrix(unl(lapply(days_list, function(x) c(x[1], x[5]))), ncol=2, byrow=T, dimnames=list(NULL, paste("RR", c("old", "new"))))
c_mins <- matrix(unl(lapply(days_list, function(x) c(x[2], x[6]))), ncol=2, byrow=T, dimnames=list(NULL, paste("RR", c("old", "new"))))
c_means <- matrix(unl(lapply(days_list, function(x) c(x[3], x[7]))), ncol=2, byrow=T, dimnames=list(NULL, paste("RR", c("old", "new"))))
c_meds <- matrix(unl(lapply(days_list, function(x) c(x[4], x[8]))), ncol=2, byrow=T, dimnames=list(NULL, paste("RR", c("old", "new"))))

apply(c_i1s, 2, mean)
apply(c_means, 2, mean)

gg <- wgg_density_mv(c_mins, win=F, xlb="min 'days used' per individual")
gg <- gg + ggtitle("HHA Scores")
w() ; print(gg) ; ggsave(filename="./min_days_hhacomp.png")



##############################
##  Calculate # RR windows  ##
##############################

WIN_TIMES <- c(30, 60, 120, 15)
WIN_TIMES <- 30

res_list_times <- vector("list", len(WIN_TIMES))
for(j in 1:len(WIN_TIMES)) {

res_list <- vector("list", n_ids)
for(i in 1:n_ids) {
  
  res <- NA
  if(len(grep(f_ids[i], fl_old)) > 0 & len(grep(f_ids[i], fl_new)) > 0) {
    f_old <- paste0(fp_old, fl_old[grep(f_ids[i], fl_old)])
    f_new <- paste0(fp_new, fl_new[grep(f_ids[i], fl_new)])
    
    rr_old <- cb(h5read2(f_old, "/metric/on_device/rr"),
                 rrv=h5read2(f_old, "/metric/on_device/rr_isvalid")[,-1])
    rr_new <- h5read2(f_new, "/metric/on_device/rr")
    rr_new$rrv = 1
    
    rr_old$index <- as.POSIXct2(rr_old$index)
    rr_new$index <- as.POSIXct2(rr_new$index)
    
    rr_old_days = split_on_datetime(rr_old)
    rr_new_days = split_on_datetime(rr_new)
    
    rr_old_days = rr_old_days[lapply(rr_old_days, nrow) > 0]
    rr_new_days = rr_new_days[lapply(rr_new_days, nrow) > 0]
    
  
    adh_calc_fun <- function(x) 
    {
      i_sleep1 <- (x$index <= as.POSIXct2(paste0(substring(as.char(x$index[1]), 1, 10), " 07:00:00 SAST")))
      i_sleep2 <- (x$index >= as.POSIXct2(paste0(substring(as.char(x$index[1]), 1, 10), " 22:00:00 SAST")))
      i_wake <- !(i_sleep1 | i_sleep2)
      
      adh_morning <- adh_night <- adh_wake <- 0
      t_win <- WIN_TIMES[j]
      
      if(sum(i_sleep1) > 0) {
        list_morning <- split_on_datetime(x[i_sleep1, ], freq=t_win/3600, offset=0)
        list_morning <- list_morning[lapply(list_morning, nrow) > 0]
        adh_morning <- sum(unl(lapply(list_morning, function(y) (sum(y$value/1000) > 0.8*t_win) & (mean(y$rrv) > 0.8))))
      }
      
      if(sum(i_sleep2) > 0) {
        list_night <- split_on_datetime(x[i_sleep2, ], freq=t_win/3600, offset=0)
        list_night <- list_night[lapply(list_night, nrow) > 0]
        adh_night <- sum(unl(lapply(list_night, function(y) (sum(y$value/1000) > 0.8*t_win) & (mean(y$rrv) > 0.8))))
      }
      
      if(sum(i_wake) > 0) {
        list_wake <- split_on_datetime(x[i_wake, ], freq=t_win/3600, offset=0)
        list_wake <- list_wake[lapply(list_wake, nrow) > 0]
        adh_wake <- sum(unl(lapply(list_wake, function(y) (sum(y$value/1000) > 0.8*t_win) & (mean(y$rrv) > 0.8))))
      }
      
      return(c(adh_wake, adh_morning + adh_night))
    }
    
    ahd_old_out <- lapply(rr_old_days, adh_calc_fun)
    ahd_new_out <- lapply(rr_new_days, adh_calc_fun)
    
    ahd_old_out <- matrix(unl(ahd_old_out), ncol=2, byrow=T)
    ahd_new_out <- matrix(unl(ahd_new_out), ncol=2, byrow=T)
    
    res <- c("old_days"=nrow(ahd_old_out), "old_hrv_count_wake"=sum(ahd_old_out[,1]), "old_hrv_count_sleep"=sum(ahd_old_out[,2]),
             "new_days"=nrow(ahd_new_out), "new_hrv_count_wake"=sum(ahd_new_out[,1]), "new_hrv_count_sleep"=sum(ahd_new_out[,2]))
    
  }
  
  res_list[[i]] <- res
  names(res_list)[i] <- f_ids[i]
  
  H5close()
  H5garbage_collect()
  catProgress2(i, n_ids)  
}

res_list_times[[j]] <- res_list
}



