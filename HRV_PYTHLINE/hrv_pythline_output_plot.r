source("R_code/useful_funs.r")
source("R_code/useful_funs_ggplot.r")


###############################################################################
####  Load and format new output data
###############################################################################

out_list <- getDataFromDir("./", head=T)
out_list <- lapply(out_list, function(x) { 
  names(x)[1] <- "index"
  x$index <- as.POSIXct2(x$index/1e9)
  return(x) })




###############################################################################
####  Utility functions
###############################################################################


format_hrv <- function(out_hrv_row)
{
  f1 <- paste0("HRV: mnn=", round(out_hrv_row$mnn, 0))
  f2 <- paste0("pnn20=", round(out_hrv_row$pnn20, 1), "; pnn50=", round(out_hrv_row$pnn50, 1))
  f3 <- paste0("rmssd=", round(out_hrv_row$rmssd, 0), "; sdnn=", round(out_hrv_row$sdnn, 0))
  paste(f1, f2, f3, sep="\n")
}



###############################################################################
####  Scroll plotting of HRV values
###############################################################################

hrv_temp_list <- out_list[2:1]
hrv_temp_list <- lapply(hrv_temp_list, function(x){ x$cvnn <- 100*x$cvnn ; x})


# Set plotting parameters
winsize <- 5*60*60
overlap <- winsize/2
fig_path <- "./output/"
start <- as.num( min(unl(lapply(hrv_temp_list, function(x) x$index[1]))) )
n <- as.num( max(unl(lapply(hrv_temp_list, function(x) tail(x$index,1)))) )
# n <- start + 30*24*3600

p_lty <- 1


# Function to calculate hours in plot window
get_hour_inds <- function(xi)
{
  t0 <- as.POSIXct2(xi[1])
  t0_split <- strsplit(as.char(t0), " ")[[1]]
  t0n <- as.num(as.POSIXct2(paste(t0_split[1], paste0(format(t0, "%H"), ":00:00"))))
  return(seq(t0n, tail(xi, 1), 3600))
}

# Function to fill plot bg and draw ticklines
plot_fill_ticklines <- function(xi, bg_col="grey20")
{
  rect(par("usr")[1], par("usr")[3], par("usr")[2], par("usr")[4], col=bg_col, border=NA)
  par_y <- par("yaxp")
  abline(h=seq(par_y[1], par_y[2], len=(par_y[3]+1)), lty=3, col="white")
  abline(v=get_hour_inds(xi), lty=3, col="white")
}


# Determine y-limits
ylm_temp_nn <- range(unl(lsel(hrv_temp_list, "mnn")))
ylm_temp_sd <- range(c(unl(lsel(hrv_temp_list, "sdnn")), unl(lsel(hrv_temp_list, "rmssd"))))
ylm_temp_ratio <- range(c(unl(lsel(hrv_temp_list, "pnn20")), 
                          unl(lsel(hrv_temp_list, "pnn50")),
                          unl(lsel(hrv_temp_list, "cvnn"))))


# Determine plot colours
cf_n <- 100
cf1 <- colorRampPalette(c("cyan3", "mediumseagreen", "green3"))
cf1 <- cf1(cf_n)[as.integer(seq(1, cf_n, len=3))]

cf2 <- colorRampPalette(c("red", "gold"))
cf2 <- cf2(cf_n)[as.integer(seq(1, cf_n, len=3))]

col_list <- list(cf1, cf2)


# Start plotting routine
j <- 0
last <- FALSE

if(winsize > n) winsize <- n
if(overlap > winsize) overlap <- winsize

winsize <- as.integer(winsize)
overlap <- as.integer(overlap)

while(TRUE) {
  j = j+1
  xi <- (overlap*(j-1)+start):(start-1+winsize + overlap*(j-1))
  
  if(max(xi) >= floor(n)) {
    last <- TRUE
    xi <- xi[1]:n
  }
  
  which_i <- lapply(hrv_temp_list, function(x) which((x$index >= xi[1]) & (x$index <= tail(xi, 1))))
  if(any(unl(lapply(which_i, len)) > 0)) {
    
    png(paste0(fig_path, format_ind(j, 4), ".png"), width=1400, height=900)
    # png(paste0(fig_path, format_ind(j, 4), ".png"), width=1920, height=1)
    
    ## Ready data for plot
    #######################  
    
    pdat_list <- lapply(hrv_temp_list, function(hrv_temp) {
      which_i_temp <- which((hrv_temp$index >= xi[1]) & (hrv_temp$index <= tail(xi, 1)))
      if(len(which_i_temp) > 0)
        return(data.frame("x"=as.POSIXct2(hrv_temp$index[which_i_temp]), 
                          "y_mnn"=hrv_temp$mnn[which_i_temp],
                          "y_pnn20"=hrv_temp$pnn20[which_i_temp],
                          "y_pnn50"=hrv_temp$pnn50[which_i_temp],
                          "y_rmssd"=hrv_temp$rmssd[which_i_temp],
                          "y_sdnn"=hrv_temp$sdnn[which_i_temp],
                          "y_cvnn"=hrv_temp$cvnn[which_i_temp]))
      else
        return(NULL)
    })
    
    i_pdat <- which(!unl(lapply(pdat_list, is.null)))
    
    ## Plotting code
    ####################    
    
    # w()
    par(mfrow=c(2, 1), mar=c(1,5,0,4), oma=c(4,0,1,0))
    
    pdat_temp <- pdat_list[[i_pdat[1]]]
    
    plot(pdat_temp$x, pdat_temp$y_mnn, type="n", xlim=as.POSIXct2(range(xi)),
         xlab="", xaxt="n", ylab="RR (ms)", ylim=ylm_temp_nn)
    plot_fill_ticklines(xi)
    
    for(p in i_pdat) {
      pdat_temp <- pdat_list[[p]]
      points(pdat_temp$x, pdat_temp$y_mnn, col=col_list[[p]][1], pch=19)
      lines(pdat_temp$x, pdat_temp$y_mnn, col=col_list[[p]][1], lty=p_lty)
    }
    
    par(new=T)
    plot(pdat_temp$x, pdat_temp$y_rmssd, type="n", xlim=as.POSIXct2(range(xi)),
         ann=F, yaxt="n", xaxt="n", ylim=ylm_temp_sd)
    axis(4)
    mtext("SD RR (ms)", 4, line=3, cex=par()$cex)
    
    for(p in i_pdat) {
      pdat_temp <- pdat_list[[p]]
      points(pdat_temp$x, pdat_temp$y_rmssd, pch=19, col=col_list[[p]][2])
      lines(pdat_temp$x, pdat_temp$y_rmssd, pch=19, col=col_list[[p]][2], lty=p_lty)
      points(pdat_temp$x, pdat_temp$y_sdnn, pch=19, col=col_list[[p]][3])
      lines(pdat_temp$x, pdat_temp$y_sdnn, pch=19, col=col_list[[p]][3], lty=p_lty)
    }
    
    legend("topright", col=unl(col_list), lwd=2, text.col="white", bg="grey20",
           cex=0.7, leg=paste(rep(names(pdat_list), each=3), c("MNN", "RMSSD", "SDNN")))
    
    plot(pdat_temp$x, pdat_temp$y_pnn20, type="n", xlim=as.POSIXct2(range(xi)),
         xlab="", ylab="Ratio (%)", ylim=ylm_temp_ratio)
    plot_fill_ticklines(xi)
    mtext(gsub(" ", " | ", pdat_temp$x[1]), 1, line=3, cex=par()$cex)
    
    for(p in i_pdat) {
      pdat_temp <- pdat_list[[p]]
      points(pdat_temp$x, pdat_temp$y_pnn20, col=col_list[[p]][1], pch=19)
      lines(pdat_temp$x, pdat_temp$y_pnn20, col=col_list[[p]][1], lty=p_lty)
      points(pdat_temp$x, pdat_temp$y_pnn50, col=col_list[[p]][2], pch=19)
      lines(pdat_temp$x, pdat_temp$y_pnn50, col=col_list[[p]][2], lty=p_lty)
      points(pdat_temp$x, pdat_temp$y_cvnn, col=col_list[[p]][3], pch=19)
      lines(pdat_temp$x, pdat_temp$y_cvnn, col=col_list[[p]][3], lty=p_lty)
    }
    
    legend("topright", col=unl(col_list), lwd=2, text.col="white", bg="grey20",
           cex=0.7, leg=paste(rep(names(pdat_list), each=3), c("PNN20", "PNN50", "CVNN")))
    
    ###################
    
    invisible(dev.off())
    
  }
  
  if(j == 1) cat("Processing", floor((n - start)/overlap), "total windows...\n")
  catProgress(j, floor((n - start)/overlap))
  
  if(last) break
  
}




    