###################################################################################################
## Package data.table "intro" vignette:
## https://cran.r-project.org/web/packages/data.table/vignettes/datatable-intro.html

source("../useful_funs.r")
library(data.table)

##########################################################################

## Read in data
flights <- fread("~/Downloads/flights14.csv")

## Example of creating data
DT <- data.table(ID = c("b","b","b","a","a","c"), a = 1:6, b = 7:12, c = 13:18)

## General form of subsetting: DT[i, j, by]
## Take 'DT', subset rows using 'i', then calculate 'j', grouped by 'by'.

## Subsetting rows in 'i'
##########################
# Can refer to variables by names within call (no need for DT name with $)
# Can omit comma if desire is only to retrieve subset of rows
# internal optimisation of 'order()'

# – Get all the flights with “JFK” as the origin airport in the month of June.
flights[origin == "JFK" & month == 6]

# - Get first two rows
flights[1:2]

# – Sort flights first by column origin in ascending order, and then by dest in descending order:
flights[order(origin, -dest)]


## Select column(s) in 'j'
###########################
# '.()' is an alias to 'list()', helpful for wrapping var names
# Can also compute/do things in 'j'
# Refer to column names the data.frame way using 'with = FALSE' (this restores data.frame mode)
# When there's only one col var to refer to, we can drop the '.()'

# – Select arr_delay column, but return it as a vector.
flights[, arr_delay]
flights[, arr_delay] %=% flights$arr_delay

# – Select arr_delay column, but return as a data.table instead.
flights[, list(arr_delay)]
flights[, .(arr_delay)]

# – Select both arr_delay and dep_delay columns.
flights[, .(arr_delay, dep_delay)]

# – Select both arr_delay and dep_delay columns and rename them to delay_arr and delay_dep.
flights[, .(delay_arr = arr_delay, delay_dep = dep_delay)]

# – How many trips have had total delay < 0?
flights[, sum((arr_delay + dep_delay) < 0)]


## Subset in 'i' and do in 'j'
##############################
# Can use '.N' as an alias for 'length(...)'

# – Calculate the average arrival and departure delay for all flights with “JFK” as the origin airport in the month of June.
flights[origin == "JFK" & month == 6, 
        .(m_arr = mean(arr_delay), m_dep = mean(dep_delay))]

# – How many trips have been made in 2014 from “JFK” airport in the month of June?
flights[origin == "JFK" & month == 6, length(dest)]
flights[origin == "JFK" & month == 6, .N]

# – Select both arr_delay and dep_delay columns the data.frame way.
flights[, c("arr_delay", "dep_delay"), with=F]

# Select using sequence of col names
flights[, year:day]
flights[, -(year:day)]



## Aggregations: grouping using 'by'
#####################################

# – How can we get the number of trips corresponding to each origin airport?
flights[, .(.N), by=.(origin)]
flights[, .(.N), by=origin]

# – How can we calculate the number of trips for each origin airport for carrier code “AA”?
flights[carrier == "AA", .N, by = origin]

# – How can we get the total number of trips for each origin, dest pair for carrier code “AA”?
flights[carrier == "AA", .N, by = .(origin, dest)]

# – How can we get the average arrival and departure delay for each orig,dest pair for each month for carrier code “AA”?
flights[carrier == "AA", 
        .(mean(arr_delay), mean(dep_delay)), 
        by = .(origin, dest, month)]




## Aggregations: keyby
#######################

# – So how can we directly order by all the grouping variables?
flights[carrier == "AA", 
        .(mean(arr_delay), mean(dep_delay)), 
        keyby = .(origin, dest, month)]



## Aggregations: chaining
##########################
# Recondsider getting the total number of trips for each origin, dest pair for carrier “AA”
ans <- flights[carrier == "AA", .N, by=.(origin, dest)]

# How can we order ans using the columns origin in ascending order, and dest in descending order?
ans[order(origin, -dest)]

# More efficient to form a chain of expressions:
flights[carrier == "AA", .N, by=.(origin, dest)][order(origin, -dest)]



## Aggregations: expressions in 'by'
#####################################

# – Can by accept expressions as well or just take columns?
# how many flights started late but arrived early (or on time), started and arrived late etc?
flights[, .N, .(dep_delay>0, arr_delay>0)]




## Aggregations: multiple cols in 'j' with .SD
###############################################
# Special symbol .SD (subset of data)
#   holds data for current group defined using 'by'
#   allows computation over range of cols using 'lapply()' 

# contains all columns except 'by' grouping cols
DT[, print(.SD), by=ID]
DT[, lapply(.SD, mean), by=ID]

# – Do we have to compute mean() for each column individually?
# – How can we specify just the columns we would like to compute the mean() on?
flights[carrier == "AA",
        lapply(.SD, mean),
        by = .(origin, dest, month),
        .SDcols = c("arr_delay", "dep_delay")]



## Aggregations: subset '.SD' for each group
##############################################

# – How can we return the first two rows for each month?
flights[, head(.SD, 2), by=month]

# 'j' is kept flexible to allow for efficiently using preknown internal functions. Examples:
# – How can we concatenate columns a and b for each group in ID?
DT[, .(val = c(a, b)), by=ID]

# – What if we would like to have all the values of column a and b concatenated, but returned as a list column?
DT[, .(val = list(c(a,b))), by=ID]

# Play around with 'print()' in 'j' to visualise internal workings
DT[, print(c(a,b)), by=ID]
DT[, print(list(c(a,b))), by=ID]


