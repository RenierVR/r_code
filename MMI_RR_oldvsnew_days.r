source("R_code/useful_funs.r")
source("R_code/useful_funs_ggplot.r")
library(rhdf5)

## Obtain data lists
fp_old <- "~/Data/MMI_RRold_RRnew_20181119/outputs_hha/old_RR/"
fp_new <- "~/Data/MMI_RRold_RRnew_20181119/outputs_hha/new_RR/"
fl_old <- getDataFromDir(fp_old, ext="h5", dataE=F, ret="base")
fl_new <- getDataFromDir(fp_new, ext="h5", dataE=F, ret="base")

# fl_new <- fl_old
f_ids <- unl(strsplit(list(fl_old, fl_new)[[which.min(c(len(fl_old), len(fl_new)))]], ".h5"))
n_ids <- len(f_ids)



res_list_old <- vector("list", n_ids)
res_list_new <- vector("list", n_ids)
for(i in 1:n_ids) {
  f_old <- paste0(fp_old, fl_old[grep(f_ids[i], fl_old)]) 
  f_new <- paste0(fp_new, fl_new[grep(f_ids[i], fl_new)]) 
  
  res <- NA
  if((len(grep(f_ids[i], fl_old)) == 1) && len(grep("score", h5contents(f_old)[,1])) > 0) {
    dat <- h5read2(f_old, "metric/health_assessment/heart/score")
    res <- dat[, grep("number_of_days", names(dat))]
  }
  
  res_list_old[[i]] <- res
  
  res <- NA
  if((len(grep(f_ids[i], fl_old)) == 1) && len(grep("score", h5contents(f_new)[,1])) > 0) {
    dat <- h5read2(f_new, "metric/health_assessment/heart/score")
    res <- dat[, grep("number_of_days", names(dat))]
  }
  
  res_list_new[[i]] <- res
  
  names(res_list_old)[i] <- f_ids[i]
  names(res_list_new)[i] <- f_ids[i]
  
  H5close()
  H5garbage_collect()
  catProgress(i, n_ids)
}





res_list_old <- res_list_old[!sapply(res_list_old, function(x) all(is.na(x)))]

hr_days_list_old <- lapply(res_list_old, function(x) na.omit(as.num(x$hr_number_of_days)))
hrv_days_list_old <- lapply(res_list_old, function(x) na.omit(as.num(x$hrv_number_of_days)))
pwf_days_list_old <- lapply(res_list_old, function(x) na.omit(as.num(x$pwf_number_of_days)))

hr_days_list_old <- hr_days_list_old[sapply(hr_days_list_old, len) > 0]
hrv_days_list_old <- hrv_days_list_old[sapply(hrv_days_list_old, len) > 0]
pwf_days_list_old <- pwf_days_list_old[sapply(pwf_days_list_old, len) > 0]


res_list_new <- res_list_new[!sapply(res_list_new, function(x) all(is.na(x)))]

hr_days_list_new <- lapply(res_list_new, function(x) na.omit(as.num(x$hr_number_of_days)))
hrv_days_list_new <- lapply(res_list_new, function(x) na.omit(as.num(x$hrv_number_of_days)))
pwf_days_list_new <- lapply(res_list_new, function(x) na.omit(as.num(x$pwf_number_of_days)))

hr_days_list_new <- hr_days_list_new[sapply(hr_days_list_new, len) > 0]
hrv_days_list_new <- hrv_days_list_new[sapply(hrv_days_list_new, len) > 0]
pwf_days_list_new <- pwf_days_list_new[sapply(pwf_days_list_new, len) > 0]




# Density plot of all days
xdf <- list("HRc1"=as.num(unl(hr_days_list_old)), 
            "HRVc1"=as.num(unl(hrv_days_list_old)),
            "PWFc1"=as.num(unl(pwf_days_list_old)),
            "HRc2"=as.num(unl(hr_days_list_new)), 
            "HRVc2"=as.num(unl(hrv_days_list_new)),
            "PWFc2"=as.num(unl(pwf_days_list_new)))

ggdat <- data.frame("score" = as.num(unl(xdf)), "name"=rep(rep(paste(c("HR", "HRV", "PWF"), "composite"), 2), sapply(xdf, len)), 
                    "cat" = rep(c("RR v1", "RR v2"), c(sum(sapply(xdf[1:3], len)), sum(sapply(xdf[4:6], len)))), stringsAsFactors=F)

gg <- ggplot(data=ggdat, aes(x=score, fill=name)) + geom_density(alpha=0.5) + facet_wrap(~cat)
gg <- gg + xlab("number of days used") + theme(legend.title=element_blank(), legend.justification = c(0, 1), legend.position = c(0, 1))
gg1 <- gg + ggtitle("Distribution of all number_days_used") + theme(plot.title = element_text(hjust = 0.5))



# Density plot of minimum days per participant
xdf <- list("HRc1"=as.num(sapply(hr_days_list_old, function(x) x[1])),
            "HRVc1"=sapply(hrv_days_list_old, function(x) x[1]),
            "PWFc1"=sapply(pwf_days_list_old, function(x) x[1]),
            "HRc2"=as.num(sapply(hr_days_list_new, function(x) x[1])),
            "HRVc2"=sapply(hrv_days_list_new, function(x) x[1]),
            "PWFc2"=sapply(pwf_days_list_new, function(x) x[1]))

ggdat <- data.frame("score" = as.num(unl(xdf)), "name"=rep(rep(paste(c("HR", "HRV", "PWF"), "composite"), 2), sapply(xdf, len)), 
                    "cat" = rep(c("RR v1", "RR v2"), c(sum(sapply(xdf[1:3], len)), sum(sapply(xdf[4:6], len)))), stringsAsFactors=F)

gg <- ggplot(data=ggdat, aes(x=score, fill=name)) + geom_histogram(breaks=seq(1,30,1), position="identity", alpha=0.7, color="black") + facet_wrap(~cat)
gg <- gg + xlab("number of days used") + theme(legend.title=element_blank(), legend.justification = c(1, 1), legend.position = c(1, 1))
gg2 <- gg + ggtitle("Distribution of minimum number_days_used per participant") + theme(plot.title = element_text(hjust = 0.5))



w(12, 6) ; print(gg1) ; ggsave("MMI_rrnew_HHA_dist_all-days-used.png", dpi=200)
w(12, 6) ; print(gg2) ; ggsave("MMI_rrnew_HHA_dist_min-days-used.png", dpi=200)
 