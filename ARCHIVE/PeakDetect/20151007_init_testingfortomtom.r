source("../useful_funs.r")
source("./ERMADT_funs.r")


###################################################################################################
###########################       TOMTOM INITIALISATION TESTING        ############################
###################################################################################################



datlist <- getDataFromDir("D:/HealthQ/Data/Initialisation Testing/20151007_AlgEval_SanityCheck_UB2.5.125_MOD2.4.77_ALG4.0.13_1/all/", dataE=F, ret="full")


###################
## REFORMAT DATA ##
###################


# for(i in 1:len(datlist)) {
#   
#   dat <- read.table(datlist[i], sep=",", head=T)
#   dat <- dat[, c(3:9, 11, 13, 15:21)]
#   write.table2(dat, file=datlist[i], col.names=T)
# }


# fpath <- "D:/HealthQ/Data/Initialisation Testing/20151007_AlgEval_SanityCheck_UB2.5.125_MOD2.4.77_ALG4.0.13_1/all/"
# datlist <- getDataFromDir(fpath, dataE=F, ret="base")
# 
# for(i in 1:len(datlist)) {
#   if(i < 10) nr <- paste0("00", i)
#   else if(i < 100) nr <- paste0("0", i)
#   
#   datstr <- strsplit(datlist[i], "_")[[1]]
#   
#   file.rename(from = paste0(fpath, datlist[i]), 
#               to = paste(c(datstr[1], "Round1", datstr[2:len(datstr)]), collapse="_"))
# }




#################
## C DEBUGGING ##
#################

i <- 1

SKIP_SAMPLES <- 4
KS_BW_FACTOR <- 0
MAX_SAMPLES <- 495


# input <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/input_testing.txt", sep=",", head=T)


for(i in 1:len(datlist)) {
  
  name <- basename(datlist[i])
  cat("Processing file...\n", name, "\n")
  
  cout <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/output/out_initdebug_", name), sep=",")
  cpks <- as.matrix(read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/output/out_initpeaks_", name), sep=","))
  
  if(nrow(cout) < MAX_SAMPLES)
    cfin <- as.matrix(read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/output/out_initfin_", name), sep=","))
  
  cpks_o <- cpks[cpks[,3] == 1, 1:2]
  ctrgh_o <- rbind(cpks[cpks[,3] == 2, 1:2])
  
  cpks <- cpks_o
  ctrgh <- ctrgh_o
  
  cpks[,1] <- cpks[,1] - SKIP_SAMPLES
  ctrgh[,1] <- ctrgh[,1] - SKIP_SAMPLES
  if(nrow(cout) < MAX_SAMPLES)
    cfin[,1] <- cfin[,1] - SKIP_SAMPLES
  
  
  # c_unfil <- cout[1:(nrow(cout)-KS_BW_FACTOR), 2]
  # c_fil <- cout[-c(1:KS_BW_FACTOR),3]
  c_unfil <- cout[, 2]
  c_fil <- cout[,3]
  
  
  # zPlot(cbind(c_unfil, c_fil))
  

  w(28, 18) ; par(mfrow=c(3,1), mar=c(2,4,2,2))
  
  plot(c_unfil, type="n", ann=F, yaxt="n", xaxt="n")
  # abline(v=which(cout[,4] == 1), lwd=3, col="yellow")
  abline(v=which(cout[,5] == 1), lwd=2, col="orange")
  lines(c_unfil, lwd=2, col="lightblue")
  par(new=T)
  lplot(c_fil, lwd=2, col="mediumseagreen", ylab = "filtered")
  abline(h=0, lty=2, col="mediumseagreen")
  points(cpks, pch=19, cex=0.85, col="blue")
  if(nrow(cout) < MAX_SAMPLES) points(cfin, pch=19, col="red")
  text(cpks, labels=cpks_o[,1], offset=0.5, pos=3, xpd=T, cex=0.8)
  points(ctrgh, pch="x", cex=1.5, col="orange")
  text(ctrgh, labels=ctrgh_o[,1], offset=0.5, pos=3, xpd=T, cex=0.8)
  legend("bottomright", leg = paste0(strsplit(name, ".csv")[[1]]), cex = 0.85, bty = "n")
  
  
  lplot(cout[,6], lwd=2, col="brown", ylab="ACC resultant")
  abline(h = 0.5, col = "brown", lty=3)
  par(new=T)
  lplot(cout[,7], lwd=2, col="black", ann=F, yaxt="n", xaxt="n")
  abline(h = 0.001, lty=2)
  axis(4)
  

  lplot(cout[,8], lwd=2, col="orange")
  abline(h = 100, col="orange")
  par(new=T)
  lplot(cout[,9], lwd=2, col="purple", ann=F, yaxt="n", xaxt="n", 
        ylim=range(cout[-c(1:9), 9]))
  axis(4)
  
  
  
  
  
  savePlot(paste0("~/HealthQ/R files/PeakDetect/results/fig_", 
                  strsplit(name, ".csv")[[1]], ".png"), "png")
  graphics.off()
  
  
  ########################################################################
  
#   dat <- read.table(datlist[i], sep=",", head=T)
#   grn <- removeCurrentDiscont2(convertToCurrent3(dat[,3], dat[,11], dat[,15]),
#                                dat[,11], dat[,15], dat[,19], 1e8)
#   
#   zPlot(cb(grn, ksmooth2(grn, 14)), par_new=T)
#   
#   zPlot(cb(FIRfilt(grn), acc_res(dat[,7:9])), par_new=T)
  
  ########################################################################
  
  
  catProgress(i, len(datlist))
  
}






###################################################################################################
###################################################################################################

# # hr <- scan("clipboard")
# # results <- cb(results, HR_V12_25_100=hr)
# # write.table2(results, "../PeakDetect/RESULTS_INIT_TESTING_1007.txt")
# 
# 
# hr <- results$HR_V12_25_100
# delta <- abs(results$REFERENCE - hr)
# delta0 <- which(hr == 0)
# delta[delta0] <- 0
# ( rmse <- round(sqrt(sum(delta^2)/(len(delta)-len(delta0))), 2) )
# 
# for(i in 1:84) cat(delta[i], "\n")




###################################################################################################
###################################################################################################
## CHECK THAT C OUTPUT REMAINS THE SAME (REFACTORING / OPTIMISATION) ##
#######################################################################

# backup_list <- vector("list", len(datlist))

for(i in 1:len(datlist)) {
  
  out_list <- vector("list", 3)
  happiness <- TRUE
  
  name <- basename(datlist[i])
  
  out_list[[1]] <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/output/out_initdebug_", name), sep=",")
  out_list[[2]] <- as.matrix(read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/output/out_initpeaks_", name), sep=","))
  
  if(ncol(out_list[[2]]) == 3)
    out_list[[2]] <- out_list[[2]][out_list[[2]][,3] == 1, 1:2]
  
  cfin <- 0
  if(nrow(out_list[[1]]) < MAX_SAMPLES)
    cfin <- as.matrix(read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/output/out_initfin_", name), sep=","))
  out_list[[3]] <- cfin
  
  # backup_list[[i]] <- out_list
  for(j in 1:3) {
    if(!all(backup_list[[i]][[j]] == out_list[[j]])) {
      cat("Cleanup on isle: i =", i, ", j =", j, "\n")
      happiness <- FALSE
    }
  }

  if((i == len(datlist)) && (happiness))
    cat("Happiness\n")
}  




  