source("../useful_funs.r")
source("./ERMADT_funs.r")


###################################################################################################
###########################       TOMTOM INITIALISATION TESTING        ############################
###################################################################################################



datlist <- getDataFromDir("D:/HealthQ/Data/TomTom/initialisation/", dataE=F, ret="full")




# for(i in 1:len(datlist2)) {
#   
#   dat <- read.table(datlist2[i], sep=",", head=T)
#   dat <- dat[, c(3:9, 11, 13, 15:21)]
#   write.table2(dat, file=datlist2[i], col.names=T)
# }


###################
## REFORMAT DATA ##
###################

# for(i in 4:len(datlist)) {
#   dat <- read.table(datlist[i], sep=",", head=T)
#   # dat <- dat[,c(10, 20, 12, 3, 16, 15, 18, 2, 19, 1, 14, 8, 9, 6, 4, 5, 17, 13)]
#   
#   nd <- names(dat)
#   
#   dat <- dat[,c(which(nd == "g"), which(nd == "gb"), which(nd == "r"), which(nd == "ir"),
#                 which(nd == "x"), which(nd == "y"), which(nd == "z"),
#                 which(nd == "grnRf"), which(nd == "rdIrRf"), 
#                 which(nd == "grnIsub"), which(nd == "blankIsub"), which(nd == "rdIsub"), which(nd == "irIsub"),
#                 which(nd == "ledcur1"), which(nd == "ledcur2"), which(nd == "ledcur3"))]
#   
#   dat[,10:13] <- 10*dat[,10:13]
#   
#   # print(all(names(dat) == nd_b))
#   
#   write.table2(dat, file=datlist[i], col.names=T)
#   catProgress(i, len(datlist))
# }




###################################
## PLOT REFERENCE (IF AVAILABLE) ##
###################################

reflist <- lapply(getDataFromDir("D:/HealthQ/Data/TomTom/initialisation/signal quality tests/", head=T),
                  function(x) x[,2])



#################
## C DEBUGGING ##
#################

i <- 17

SKIP_SAMPLES <- 4
KS_BW_FACTOR <- 0
MAX_SAMPLES <- 496


# input <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/input_testing.txt", sep=",", head=T)


for(i in 1:len(datlist)) {
  
  name <- basename(datlist[i])
  cat("Processing file...\n", name, "\n")

  cout <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/output/out_initdebug_", name), sep=",")
  cpks <- as.matrix(read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/output/out_initpeaks_", name), sep=","))
  
  if(nrow(cout) < MAX_SAMPLES)
    cfin <- as.matrix(read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/output/out_initfin_", name), sep=","))

  cpks_o <- cpks
  
  cpks[,1] <- cpks[,1] - SKIP_SAMPLES
  if(nrow(cout) < MAX_SAMPLES)
    cfin[,1] <- cfin[,1] - SKIP_SAMPLES
  
  # c_unfil <- cout[1:(nrow(cout)-KS_BW_FACTOR), 2]
  # c_fil <- cout[-c(1:KS_BW_FACTOR),3]
  c_unfil <- cout[, 2]
  c_fil <- cout[,3]
  
  
  # zPlot(cbind(c_unfil, c_fil))
  
  dat <- read.table(datlist[i], sep=",", head=T)
  grn <- removeCurrentDiscont2(convertToCurrent3(dat[,1], dat[,8], dat[,10]),
                               dat[,8], dat[,10], dat[,14], 1e8)
  
  
  w(28, 18) ; par(mfrow=c(3,1), mar=c(2,4,2,2))
  
  plot(c_unfil, type="n", ann=F, yaxt="n", xaxt="n")
  # abline(v=which(cout[,4] == 1), lwd=3, col="yellow")
  abline(v=which(cout[,5] == 1), lwd=2, col="orange")
  lines(c_unfil, lwd=2, col="lightblue")
  par(new=T)
  lplot(c_fil, lwd=2, col="mediumseagreen", ylab = "filtered")
  abline(h=0, lty=2, col="mediumseagreen")
  points(cpks, pch=19, cex=0.75, col="blue")
  if(nrow(cout) < MAX_SAMPLES) points(cfin, pch=19, col="red")
  text(cpks, labels=cpks_o[,1], offset=0.5, pos=3, xpd=T, cex=0.8)
  legend("bottomright", leg = paste0(i, "_", strsplit(name, ".csv")[[1]]), cex = 0.75, bty = "n")
  
  
  lplot(cout[,6], lwd=2, col="brown", ylab="ACC resultant")
  abline(h = 0.5, col = "brown", lty=3)
  par(new=T)
  lplot(cout[,7], lwd=2, col="black", ann=F, yaxt="n", xaxt="n")
  abline(h = 0.001, lty=2)
  axis(4)
  
  
  lplot(acc_res(dat[1:len(c_fil), 5:7], 16), lwd=2, col="purple", ylab="acc")
  par(new=T)
  lplot(cout[,8], lwd=2, col="maroon", yaxt="n", xaxt="n", ann=F)
  axis(4)
#   legend("bottomright", leg = paste0(i, "_", strsplit(name, ".csv")[[1]]), cex = 0.75, bty = "n")
  
  
  
  
  savePlot(paste0("~/HealthQ/R files/PeakDetect/results/fig_", 
                  strsplit(name, ".csv")[[1]], "_withmovement.png"), "png")
  graphics.off()

  

  # zPlot(cb(grn, ksmooth2(grn, 14)), par_new=T)
  
  
  catProgress(i, len(datlist))
  
}
  


