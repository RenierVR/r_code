
source("../useful_funs.r")
source("./sp02_funs.r")


SKIP_SAMPLES <- 5
KS_BW_FACTOR <- 14

input <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/input_testing.txt", sep=",")


cout <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/out_initdebug.txt", sep=",")
cpks <- as.matrix(read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/out_initpeaks.txt", sep=","))
cfin <- as.matrix(read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/out_initfin.txt", sep=","))
# c_acc <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/out_initacc.txt")[,1]

cpks_o <- cpks

cpks[,1] <- cpks[,1]-SKIP_SAMPLES-KS_BW_FACTOR
cfin[,1] <- cfin[,1]-SKIP_SAMPLES-KS_BW_FACTOR

c_unfil <- cout[1:(nrow(cout)-KS_BW_FACTOR), 2]
c_fil <- cout[-c(1:KS_BW_FACTOR),3]


# zPlot(cbind(c_unfil, c_fil))


w()
lplot(c_fil, lwd=2)
points(cpks, pch=19, cex=0.75, col="red")
points(cfin, pch=19, col="blue")
text(cpks, labels=cpks_o[,1], offset=0.5, pos=3, xpd=T, cex=0.8)


par(new=T)
lplot(acc_res(input[-c(1:5),3:5],16), col="purple", ann=F, yaxt="n", xaxt="n") ; axis(4)




# par(new=T)
# lplot(c_acc, col="purple", ann=F, yaxt="n", xaxt="n") ; axis(4)
# points(c_acc, col="purple", pch=19)
# 
# 
# 
# par(new=T)
# lplot(cout[1:len(c_fil),1], col="orange", ann=F, yaxt="n", xaxt="n")






################################################################################################


# fnames <- getDataFromDir("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/data/Init_probs/", dataE=F)
# fname <- fnames[4]
# dat <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/data/Init_probs/", fname), sep=",", head=T)



fnames <- getDataFromDir("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/data/InitTestingJune/", dataE=F)[c(8:21, 1:7)]
fname <- fnames[4]
dat <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/data/InitTestingJune/", fname), sep=",", head=T)


dat <- dat[-c(1:5),]
curr1 <- curr2 <- curr3 <- matrix(0, nr=nrow(dat), nc=4)
for(j in 1:4){
  if(j < 3){
    Rf <- dat[,8]
    led <- dat[,14]
  }
  else{
    Rf <- dat[,9]
    led <- dat[,j+12]
  }
  
  safe_term <- 0
  if(j == 2){
    safe_term <- 1
    led <- numeric(nrow(dat))
  }
  
  dat_tmp <- dat[,j] 
  isub_tmp <- dat[,9+j]
  
  curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
  curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e8)  
  curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e8)[[1]]
}

zPlot(curr2[,1])

zPlot(cbind(FIRfilt(ksmooth2(curr2[,1],14),25), acc_res(dat[,5:7],16)), par_new=T)

################################################################################################

# init_wins <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/out_initwins.txt", sep=",")
# init_wins <- as.matrix(init_wins)
# 
# 
# w() ; par(ask=T)
# for(i in 1:nrow(init_wins)){
#   
#   x <- init_wins[i,1:5]
#   y <- init_wins[i,6:7]
#   lplot(x, lwd=2)
#   abline(v=which(x==max(x)), lwd=2)
#   title(main=paste("window", i, " | max", which(x==max(x)), " | pd_out", y[1], " | ind", y[2]))
#   
# }

################################################################################################

init_batch <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/out_initbatch.txt", sep=",")

all(init_batch == init_batch_b2)


for(j in 1:35)
{
  cat("\n\n---------------------", j, "---------------------\n")
  cat("25Hz1 : init to\t", init_batch_b2[j,1], "\tin\t", init_batch_b2[j,3], "s\n")
  cat("25Hz2 : init to\t", init_batch[j,1], "\tin\t", init_batch[j,3], "s\n")
}








