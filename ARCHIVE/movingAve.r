movingAve <- function(x, m, endpoint_behaviour=1)
{
  ## Moving Average filter with order 'm'
  ## 'endpoint_behaviour' sets the behaviour for endpoints used for
  ## first and last results, can have values:
  ## 1 - return original values
  ## 2 - return NAs
  ## 3 - remove and return sequence with reduced length
  
  if(length(x) < m)
    stop("Time series length shorter than filter order.")
  
  x_fil <- as.numeric(stats::filter(x, rep(1/m, m)))
  
  if(endpoint_behaviour == 1){
    x_fil[is.na(x_fil)] <- x[is.na(x_fil)]
    return(x_fil)
  }
  
  else if(endpoint_behaviour == 2)
    return(x_fil)
  
  else if(endpoint_behaviour == 3)
    return(as.numeric(na.omit(x_fil)))
  
}