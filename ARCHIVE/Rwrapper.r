###################################################################################################
###################################################################################################
###################                                                             ###################
###################         R WRAPPER FOR THE LIFEQINSIDE C LIBRARY             ###################
###################       ---------------------------------------------         ###################
###################                                                             ###################
###################       Function wrappers and interfaces to simplify          ###################
###################       analyis of Mojo raw data with the LifeQInside         ###################
###################       embedded libraries.                                   ###################
###################                                                             ###################
###################       Requirements:                                         ###################
###################       * the file path to the DLL                            ###################
###################       *  Mojo data object with                              ###################
###################          order of data columns:                             ###################
###################                      1: green PPG                           ###################
###################                      2: ambient PPG                         ###################
###################                      3: red PPG                             ###################
###################                      4: infrared PPG                        ###################
###################                      5: accelerometer X axis                ###################
###################                      6: accelerometer Y axis                ###################
###################                      7: accelerometer Z axis                ###################
###################                      8: AGC green RF                        ###################
###################                      9: AGC ambient RF                      ###################
###################                      10: AGC red RF                         ###################
###################                      11: AGC infrared RF                    ###################
###################                      12: AGC green Isub                     ###################
###################                      13: AGC ambient Isub                   ###################
###################                      14: AGC red Isub                       ###################
###################                      15: AGC infrared Isub                  ###################
###################                      16: AGC green LED                      ###################
###################                      17: AGC red LED                        ###################
###################                      18: AGC infared LED                    ###################
###################                                                             ###################
###################################################################################################

# source("C:/Users/reniervr/Documents/HealthQ/R files/useful_funs.r")

#############################
LQI_path <- "C:/Users/reniervr/Documents/HealthQ/C files/LIFEQINSIDE/project/code_blocks/bin/Debug/lifeqinside.dll"
###################################################################################################


###################################################################################################

LQI_formatData <- function(dat, dat_order = 1:18)
{
  ## Format data for input to wrappers.
  ## <dat_order> as noted in file header.
  
  fdat <- as.matrix(dat[, dat_order])
  fdat <- apply(fdat, 2, as.integer)
  return( fdat )
}

###################################################################################################


###################################################################################################

LQI_init <- function(Fs)
{
  ## Loads the LifeQInside DLL and initialise algorithms with
  ##   sampling frequency Fs.
  
  if(missing(Fs)) stop("Sampling frequency needs to be specified.")
  
  if( !any(names(getLoadedDLLs()) == strsplit(basename(LQI_path), ".dll")[[1]]) )
    dyn.load(LQI_path)
  else {
    dyn.unload(LQI_path)
    dyn.load(LQI_path)
  }
  
  out <- .C("RWRAPPER_manageAlgorithms", 
            state = as.integer(0),
            fs = as.integer(Fs),
            versions = integer(4))
  
  invisible(out)
}

###################################################################################################


###################################################################################################

LQI_processData <- function(fdat, type = c("RR", "SPO2", "SLEEP")[1],
                            get_HRV_arrays = FALSE,
                            reset = TRUE, showProgress = TRUE)
{
  ## Process the data with the LifeQInside C-libs
  ## <type> specifies the type of output
  
  if( !any(names(getLoadedDLLs()) == strsplit(basename(LQI_path), ".dll")[[1]]) )
    stop("LQI needs to be initialised first.")
  
  
  # Iout <- matrix(NA, nrow(fdat), 4)
  Iout <- matrix(NA, 1, 1)
  # colnames(Iout) <- c("Igrn", "Iamb", "Ired", "Inir")
  
  PPout <- matrix(NA, nrow(fdat), 5)
  colnames(PPout) <- c("HR", "Cadence", "HRconf", "IsContact", "GetActivity")
  
  ACCout <- matrix(NA, nrow(fdat), 3)
  colnames(ACCout) <- c("Resultant", "SmoothDiff", "ACEnergy")
  
  if(type == "RR") {
    state <- 4
    RRout <- matrix(NA, nrow(fdat), 10)
    colnames(RRout) <- c("IsReady", "RR", "RRvalid", "RRconf", 
                         "RRindex", "RRindex_offset", "RRpeakAC",
                         "RRdebug1", "RRdebug2", "RRdebug3")
  }
  
  if(type == "SPO2") {
    state <- 2
    SpO2out <- matrix(NA, nrow(fdat), 2)
    colnames(SpO2out) <- c("SpO2", "SpO2conf")
  }
  
  if(type == "SLEEP") {
    state = 5
  }
  
  if(get_HRV_arrays) {
    HRV_RR_array_list <- vector("list", nrow(fdat))
    HRV_RRV_array_list <- vector("list", nrow(fdat))
  }
  
  
  for(j in 1:nrow(fdat)) {
    
    out <- .C("RWRAPPER_addRawSample",
              grSample = as.integer(fdat[j,1]), amSample = as.integer(fdat[j,2]), 
              rdSample = as.integer(fdat[j,3]), irSample = as.integer(fdat[j,4]),
              accX = as.integer(fdat[j,5]), accY = as.integer(fdat[j,6]), accZ = as.integer(fdat[j,7]),
              grRf = as.integer(fdat[j,8]), amRf = as.integer(fdat[j,9]),
              rdRf = as.integer(fdat[j,10]), irRf = as.integer(fdat[j,11]),
              grIsub = as.integer(fdat[j,12]), amIsub = as.integer(fdat[j,13]),
              rdIsub = as.integer(fdat[j,14]), irIsub = as.integer(fdat[j,15]),
              grLED = as.integer(fdat[j,16]), rdLED = as.integer(fdat[j,17]), irLED = as.integer(fdat[j,18]),
              inputState = as.integer(state))
    
    out <- .C("RWRAPPER_getOutput", 
              currentSamples = integer(ncol(Iout)),
              PPoutput = integer(ncol(PPout)),
              ACCoutput = double(ncol(ACCout)),
              RRoutput = integer(ncol(RRout)),
              SpO2output = integer(2))
    
    # Iout[j,] <- out$currentSamples
    PPout[j,] <- out$PPoutput
    ACCout[j,] <- out$ACCoutput
    
    if(state == 4) RRout[j,] <- out$RRoutput
    if(state == 2) SpO2out[j,] <- out$SpO2output
    
    if(get_HRV_arrays) {
      out <- .C("RWRAPPER_HRV_getArrays",
                RR_array = integer(150),
                RRV_array = integer(150))
      
      HRV_RR_array_list[[j]] <- out$RR_array
      HRV_RRV_array_list[[j]] <- out$RRV_array
    }
    
    if(showProgress) catProgress(j, nrow(fdat))
    # print(j)
  }
  
  if(reset) LQI_reset()
  
  outlist <- list(current = Iout, PPoutput = PPout, ACCoutput = ACCout)
  
  if(state == 4) outlist <- c(outlist, list(RRoutput = RRout))
  if(state == 2) outlist <- c(outlist, list(SpO2output = SpO2out))
  
  if(get_HRV_arrays) {
    outlist <- c(outlist, list(HRV_arrays = list(HRV_RR_array_list,
                                                 HRV_RRV_array_list)))
  }
  
  return( outlist )
  
}

###################################################################################################


###################################################################################################

LQI_reset <- function()
{
  ## Resets LifeQInside algorithm.
  
  if( !any(names(getLoadedDLLs()) == strsplit(basename(LQI_path), ".dll")[[1]]) )
    stop("LifeQInside DLL not loaded.")
  else {

    out <- .C("RWRAPPER_manageAlgorithms", 
              state = as.integer(1),
              fs = as.integer(0),
              versions = integer(4))
    
  }
}

###################################################################################################


###################################################################################################

LQI_unload <- function()
{
  ## Disposes algorithm components and unloads LifeQInside DLL.
  
  if( !any(names(getLoadedDLLs()) == strsplit(basename(LQI_path), ".dll")[[1]]) )
    warning("LifeQInside DLL not loaded.")
  else {
    out <- .C("RWRAPPER_manageAlgorithms", 
              state = as.integer(2),
              fs = as.integer(0),
              versions = integer(4))
    
    dyn.unload(LQI_path)
  }
}

###################################################################################################

