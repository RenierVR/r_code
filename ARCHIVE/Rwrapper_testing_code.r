source("useful_funs.r")


dyn.load("../C files/RInterfaceTest/bin/Debug/RInterfaceTest.dll")

# out <- .C("testAdd", x = as.double(2), y = as.double(4), z = as.double(0))
# (out$z)

# out <- .C("testAddVectors", 
#           n = as.integer(5),
#           x = as.double(1:5),
#           y = as.double(seq(10, 50, 10)),
#           z = double(5))

.C("initGlobals")
out <- .C("getGlobArray", globA = double(5))
out <- .C("setGlobal", as.double(1))
out <- .C("getGlobArray", globA = double(5))
out <- .C("setGlobal", as.double(5))
out <- .C("getGlobArray", globA = double(5))


dyn.unload("../C files/RInterfaceTest/bin/Debug/RInterfaceTest.dll")


###################################################################################################
###################################################################################################
###################################################################################################

datlist <- getDataFromDir("D:/HealthQ/Data/TomTom/201603 SD/", head = T)
c_out <- vector("list", len(datlist))
for(i in 1:len(datlist)) 
  c_out[[i]] <- read.table(paste0("~/HealthQ/C files/LIFEQINSIDE/project/code_blocks/physcalc_out_", names(datlist)[i], ".csv"), sep = ",", head = T)

dyn.load("../C files/LIFEQINSIDE/project/code_blocks/bin/Debug/lifeqinside.dll")

out <- .C("RWRAPPER_manageAlgorithms", 
          state = as.integer(0),
          fs = as.integer(25),
          versions = integer(4))


i <- 1

for(i in 1:len(datlist)) {


dat <- datlist[[i]]
cout <- c_out[[i]]

PPout <- matrix(NA, nrow(dat), 4)
RRout <- matrix(NA, nrow(dat), 4)
for(j in 1:nrow(dat)) {
  
  out <- .C("RWRAPPER_addRawSample",
            grSample = as.integer(dat[j,2]), amSample = as.integer(dat[j,5]), 
            rdSample = as.integer(dat[j,3]), irSample = as.integer(dat[j,4]),
            accX = as.integer(dat[j,6]), accY = as.integer(dat[j,7]), accZ = as.integer(dat[j,8]),
            grRf = as.integer(dat[j,9]), amRf = as.integer(dat[j,9]),
            rdRf = as.integer(dat[j,10]), irRf = as.integer(dat[j,10]),
            grIsub = as.integer(dat[j,11]), amIsub = as.integer(dat[j,12]),
            rdIsub = as.integer(dat[j,13]), irIsub = as.integer(dat[j,14]),
            grLED = as.integer(dat[j,15]), rdLED = as.integer(dat[j,16]), irLED = as.integer(dat[j,17]),
            inputState = as.integer(4))
  
  out <- .C("RWRAPPER_getOutput", 
            currentSamples = integer(4),
            PPoutput = integer(4),
            RRoutput = integer(4),
            SpO2output = integer(2))
  
  PPout[j,] <- out$PPoutput
  RRout[j,] <- out$RRoutput
}


out <- .C("RWRAPPER_manageAlgorithms", 
          state = as.integer(1),
          fs = as.integer(25),
          versions = integer(4))

# out <- .C("RWRAPPER_manageAlgorithms", 
#           state = as.integer(2),
#           fs = as.integer(25),
#           versions = integer(4))


# cat(PPout[,1] %=% cout[,1], " | ", PPout[,3] %=% cout[,3], "\n")
cat(RRout[,2] %=% cout[,12], " | ", RRout[,3] %=% cout[,13], " | ", RRout[,4] %=% cout[,14], "\n")

}


# zPlot(cb(PPout[,1], cout[,1]))
# zPlot(cb(PPout[,3], cout[,3]))

dyn.unload("../C files/LIFEQINSIDE/project/code_blocks/bin/Debug/lifeqinside.dll")




###################################################################################################
###################################################################################################

datlist <- getDataFromDir("D:/HealthQ/Data/TomTom/201603 SD/", head = T)
c_out <- vector("list", len(datlist))
for(i in 1:len(datlist)) 
  c_out[[i]] <- read.table(paste0("~/HealthQ/C files/LIFEQINSIDE/project/code_blocks/physcalc_out_", names(datlist)[i], ".csv"), sep = ",", head = T)

LQI_unload()
LQI_init(25)

for(i in 1:len(datlist)) {
  dat <- LQI_formatData(datlist[[i]], c(2, 5, 3, 4, 6:9, 9, 10, 10:17))
  out <- LQI_processData(dat, showP = F)
  
  cout <- c_out[[i]]
  
  print(out[[3]][,2:4] %=% cout[,12:14])
}



