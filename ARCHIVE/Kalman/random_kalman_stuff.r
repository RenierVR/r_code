aa = 1:10*10

closestTo <- function(x, aa, retrn = c("all", "first")[1])
{
  xaa = abs(aa - x)
  ind = which(xaa == min(xaa))
  if(retrn == "all")
    return(aa[ind])
  else if(retrn == "first")
    return(aa[ind[1]])
}

Hvals <- c(0.001, 0.002, 0.003, 0.004, 0.005,
           0.01, 0.015, 0.02, 0.03, 0.035, 0.04, 0.05, 0.075)

Gvals <- c(0.8, 0.85, 0.87, 0.89, 0.91, 0.93, 0.95, 0.96, 0.97, 0.98, 0.99,
           1, 1.01, 1.03, 1.05)


hht <- sapply(hh, closestTo, aa = Hvals, retrn = "first")
ggt <- sapply(gg, closestTo, aa = Gvals, retrn = "first")

hg <- apply(cbind(hht, ggt), 1, paste, collapse = " ")


#########################################################################
#########################################################################
#########################################################################
#########################################################################

ds_seq <- seq(1, nrow(green), 2)
green_ds <- green[ds_seq,1]
acco_ds <- acco[ds_seq]
nir_ds <- uppersesh.dat[ds_seq, 12]
fir_ds <- uppersesh.dat[ds_seq, 14]

plot_c_output <- function(jj = 0, start = 1, accthreshold = 0.4, wl = 4)
{
  
  if(jj == 0) jj <- start:1500
  windows(17, 9.5) ; par(ask = TRUE, mar = c(3.5, 4, 1.5, 1) + 0.1) ; layout(matrix(c(1,2,3), 3, 1, byrow = TRUE))
  for(j in jj){
    plotind <- (50*(j-1)+1):(50*wl + 50*(j-1))
    ascore <- diff(range(acco_ds[plotind]))
    gs <- green_ds[plotind]
    gs2 <- green_ds[plotind]
    
    acol <- ifelse(ascore >= accthreshold, "maroon", "grey")
    
    plot(plotind, acco_ds[plotind], type = "l", lwd = 2, col = acol, xlab = "",
         ylab = "total acceleration", ylim = range(acco_ds))
    legend("bottomleft", leg = paste("score =", round(ascore,3)))
    
    plot(plotind, green_ds[plotind], type = "l", lwd = 2, col = "black", xlab = "",
         ylab = "original green signal")
    legend("topright", leg = paste("j =", j), cex = 1.3)
    
    if(ascore >= accthreshold){
      gs <- kalman_filter(gs, acco_ds[plotind], use.opt = T)[[1]]
      gs_r <- kalman_filter(gs2, scale(acc[plotind,1]), use.opt = T)[[1]]
      gs_r2 <- kalman_filter(gs2, scale(acc[plotind,1])+1, use.opt = T)[[1]]
      
      plot(plotind, gs, type = "l", lwd = 1, col = "red", xlab = "",
           ylab = "filtered green signal", ylim = range(c(range(gs), range(gs_r), range(gs_r2))))
      lines(plotind, gs_r, lwd = 2, col = "darkblue")
      lines(plotind, gs_r2, lwd = 2, col = "darkgreen")
    }
    else{
      ma_ord <- 5
      gs_ma <- as.numeric(filter(gs, rep(1/ma_ord, ma_ord)))
      
      plot(na.omit(cbind(plotind, gs_ma)), type = "l", lwd = 2, col = "darkblue", xlab = "",
           ylab = "filtered green signal")
    }
  }
}
plot_c_output(s = 100)




###############################################################################
###############################################################################
###############################################################################


windows(12,5) ; par(ask = T, mfrow = c(2,1))
for(j in 1:60){
  plot(1:1000 + 1000*(j-1), acco_ds[1:1000 + 1000*(j-1)], type = "l", lwd = 2, col = "red")
  plot(1:1000 + 1000*(j-1), kk[[1]][1:1000 + 1000*(j-1)], type = "l", lwd = 2, col = "darkgreen")
}

