

exp_smooth <- function(data, alpha=1) 
{
  # This function performs an exponential smoother
  # Data: Data must be a vector
  # Alpha: Smoothing parameter, must be between 0 and 1. Larger values give more weight to the present
  
  n <- length(data)
  smootherVec <- data[1]
  for(i in 2:n)
    smootherVec[i] <- smootherVec[i-1]*(1-alpha) + alpha*data[i]
  
  return(smootherVec)
}



###########################################################
###########################################################
######
###### EXTRACT SQ METRICS AND HR IN SLIDING WINDOWS
######


wmpfun <- function(winsize=512, overlap=50, start=1, x, grn_chan=c(1,2,3)[1], 
                   blank_sub=TRUE, gauss_order=10, dif=1, show_plot=FALSE,
                   acc_chan=c(1,2,3)[1])
{
  require(TSA)
  
  if(show_plot){
    windows(14,7)
    par(ask = TRUE, bg="grey", fg="white", mar = c(3.5, 4, 1.5, 1) + 0.1)
  }
  
  winsize <- winsize + dif
  
  colmap = c("blue", "mediumseagreen", "green", "yellow", "orange", "red")
  
  hr_vec <- NULL
  sq_mat <- NULL
  
  y <- x[,2*grn_chan]
  if(blank_sub)
    y <- y - x[,(2*grn_chan)+6]
  
  y <- ksmooth(1:length(y), y, "normal", bandw=gauss_order)$y
  
  acc <- as.numeric(diff(x[,acc_chan+12]))
  acc_res <- 8*sqrt(rowSums(((x[,13:15] - 2^15) / (2^16))^2))
  
  j = 0
  while(TRUE){
    j = j+1
    plotind <- (overlap*(j-1)+start):(start-1+winsize + overlap*(j-1))
    if(max(plotind) > length(y))
      break
    else{
      # obtain spectral coeffiecients in specific subband
      tempdat <- as.numeric(na.omit(diff(y[plotind], diff=dif)))
      bla <- (abs(fft(tempdat))[1:(length(tempdat)/2)+1])^2
      bla <- bla[12:83]
      
      # determine SQ with spectral flatness metric
      flatness <- exp((sum(log(sqrt(bla)+0.00000001))/(length(bla))))/(sum(sqrt(bla))/(length(bla)))
      
      # determine SQ with modified entropy metric
      norm_bla<-sqrt(bla)/sum(sqrt(bla))
      entropy<-(2^(-sum(norm_bla*log(norm_bla+0.0000001,base=2))/log(length(norm_bla),base=2))-1)
      
      # determine SQ with accelerometer signal
      #tempacc <- abs(acc[plotind])
      tempacc <- acc_res[plotind]
      
      # determine SQ with skewness
      skewsq <- log(abs(skewness(bla[-1]))+1)/3
      
      hr_vec <- c(hr_vec, x[max(plotind),16])
      sq_mat <- rbind(sq_mat, c(flatness^2, mean(tempacc), max(tempacc), entropy, skewsq))
      
      if(show_plot){
        sq <- flatness^2
        if(sq < 0.1) lcol = colmap[1]
        else if(sq <= 0.2) lcol = colmap[2]
        else if(sq <= 0.3) lcol = colmap[3]
        else if(sq <= 0.4) lcol = colmap[4]
        else if(sq <= 0.5) lcol = colmap[5]
        else if(sq > 0.5)  lcol = colmap[6]
      
        plot(plotind, y[plotind], col=lcol, lwd=2, type="l",
             main = paste0("HR = ", hr_vec[length(hr_vec)], " | SQ = ", 
                           round(sq_mat[nrow(sq_mat),1], 4)))
      }
    }
  }
  
  return(list(HR=hr_vec, SQ=sq_mat))
}


###########################################################
###########################################################
######
###### HR KALMAN FILTER FUNCTION
######


hr_kf <- function(hrvec, use_opt=TRUE, Ht=1, Gt=1, Ttt=1)
{
  require(FKF)
  
  y <- as.double(hrvec)
  dt <- matrix(0, nc = length(y), nr = 1)
  ct <- matrix(0, nc = length(y), nr = 1) 
  a0 <- y[1]            
  P0 <- matrix(100)
  
  Zt <- array(1, dim = c(1,1,length(y)))
  
  Tt <- array(Ttt, dim = c(1,1,length(y)))
  
  
  if(use_opt){
    fit.fkf <- optim(c(HHt = var(y, na.rm = TRUE) * .5,
                       GGt = var(y, na.rm = TRUE) * .5),
                     fn = function(par, ...)
                       -fkf(HHt = array(par[1], dim = c(1,1,length(y))), GGt = array(par[1], dim = c(1,1,length(y))), ...)$logLik,
                     yt = rbind(y), a0 = a0, P0 = P0, dt = dt, ct = ct,
                     Zt = Zt, Tt = Tt, check.input = FALSE)
    HGm <- c(round(fit.fkf$par[1],5), round(fit.fkf$par[2],5))
    Ht <- array(abs(fit.fkf$par[1]), dim = c(1,1,length(y)))
    Gt <- array(abs(fit.fkf$par[2]), dim = c(1,1,length(y)))
  }
  else{
    HGm <- c(mean(Ht), mean(Gt))
    Ht <- array(Ht, dim = c(1,1,length(y)))
    Gt <- array(Gt, dim = c(1,1,length(y)))
  }
  
  fkf.obj <- fkf(a0, P0, dt, ct, Tt, Zt, HHt = Ht, GGt = Gt, yt = rbind(y))
  return(list(fkf.obj$att[1,], HGm, fkf.obj$log))
  
}


###########################################################
###########################################################
######
###### HR MOVING AVERAGE FUNCTION (under construction)
######

adapt_ma <- function(hrsqmat, ord)
{
  aa <- cbind(hrsqmat[,1], hrsqmat[,4]>500)
  indmat <- NULL
  i <- 1
  while(i <= nrow(aa)){
    if(aa[i,2] == 1){
      j = 1
      while(1){
        if(aa[i+j,2] == 1)
          j <- j+1
        else
          break
      }
      indmat <- rbind(indmat, c(i,j))
      i <- i+j
    }
    else
      i <- i+1
  }
 
  hrma <- rep(NA, nrow(hrsqmat))
  for(k in 1:nrow(indmat))
    if(indmat[k,2] >= 2*ord){
      tmpind <- indmat[k,1]:(indmat[k,1]+indmat[k,2])
      xtemp <- hrsqmat[tmpind,1]
      xtemp <- as.numeric(filter(xtemp, rep(1/ord, ord)))
      hrma[tmpind] <- xtemp
    }
  
  aa <- cbind(hrsqmat[,1], hrma)
  aa[is.na(aa[,2]),2] <- aa[is.na(aa[,2]),1]
  
  return(aa[,2])
}





###########################################################
###########################################################
######
###### RETROSPECTIVE MA AND INCREASING HR MEASURE FUNCTION
######


retro_ma <- function(x, ma_ord=1)
{
  n <- length(x)
  xma <- numeric(n)
  xinc <- rep(0,n)
  xinc2 <- rep(0,n)
  
  for(i in 1:n){
    if(i < ma_ord)
      xma[i] <- x[i]
    else{
      xma[i] <- mean(x[(i-ma_ord+1):i])
      
      if(sum(diff(xma[(i-ma_ord+1):i])) > 0)
        xinc[i] <- 1
      
      if(mean(xma[(i-ma_ord+1):i]) < xma[i])
        xinc2[i] <- 1
    }
  }
  
  return(cbind(xma=xma, xinc=xinc, xinc2=xinc2))
}




detect_hr_drop <- function(x, acc, acc_thresh=1.75, check_prev=3, mx_jump=2)
{
  n <- length(x)
  is_decr <- rep(0, n)
  jumps_above_max <- 0;
  
  for(i in 2:n){
    if(acc[i] >= acc_thresh){
      if( diff(x[(i-1):i]) <= -mx_jump )
        jumps_above_max <- jumps_above_max + 1
      else
        jumps_above_max <- 0
      
      if(jumps_above_max == check_prev){
        is_decr[i] <- 1
        jumps_above_max <- check_prev-1
      }
    }
    else
      jumps_above_max <- 0;
  }
  
  return(is_decr)
}


extend_hr_drop <- function(x, ext=2)
{
  i <- length(x)-ext
  while(i >= 1){
    if(x[i] == 1)
      x[(i+1):(i+ext)] <- 1
    
    i <- i-1
  }
  
  return(x)
}





###########################################################
###########################################################
######
###### PLOT HR AND SELECTED SQ METRIC
######

# hrsq <- wmpfun(x=frowdat, grn=3,gauss=10, dif=2, show=F, acc_chan=1)
# 
# plot_hrsq <- FALSE
# if(plot_hrsq){
#   pind <- 80:length(hrsq$HR)
#   windows(14,7)
#   plot(pind, hrsq$HR[pind], type="l", col="blue", xlab="", ylab="", xaxt="n", yaxt="n")
#   lines(pind, frowbh[pind-75], col="black")
#   par(new=T)
#   plot(pind, hrsq$SQ[pind,3], pch=19, col="red", cex=0.75, ylab="", xlab="", yaxt="n", xaxt="n")
# }




###########################################################
###########################################################
######
###### CONDITIONING OF KALMAN MEASUREMENT COVARIANCE
######

# hrsqmat <- cbind(hrsq$HR, hrsq$SQ)
# colnames(hrsqmat) <- c("HBC_HR", "SQ_specflat", "SQ_acc_mn", "SQ_acc_md", "SQ_entropy")
# Gt1 <- 5*100*hrsqmat[,2]
# Gt2 <- 1*100*hrsqmat[,5]




###########################################################
###########################################################
######
###### CONSTRUCT STATE TRANSITION VECTOR
######

# HRrma <- retro_ma(hrsqmat[,1], 5)
# n <- nrow(hrsqmat)
# Tvec <- rep(1, n)
# #Tvec[HRrma[,3] == 1] <- 1.005
# 
# for(i in 1:n)
#   if(hrsqmat[i,4] >= 500)
#     if(HRrma[i,3] == 0)
#       Tvec[i] <- 1.005




###########################################################
###########################################################
######
###### PLOTTING RESULTS
######


# #aa <- hr_kf(hrsqmat[,1], use_opt=F, Ht=1, Gt=5*100*hrsqmat[,2])
# aa <- hr_kf(hrsqmat[,1], use_opt=F, Ht=1, Gt= Gt2, Tt=Tvec)
# 
# #pind = 76:3600
# pind = 500:1700
# #pind = 1750:3000
# windows(14,7)
# plot(pind, hrsqmat[pind,1], type="n", xlab="time (s)", ylab="HR", main="Franco row data 20140220")
# lines(pind, frowbh[pind-75], col="black")
# lines(pind, hrsqmat[pind,1], col="green")
# lines(pind, aa[[1]][pind], col="red")
# # legend("topleft", lwd=1, col=c("black", "mediumseagreen", "red", "blue"),
# #       leg=c("BH reference", "HR prediction", "Kalman prediction", "MA(17) prediction"))
# 
# 
# 
# 
# par(new=T) ; plot(pind, HRrma[pind,3], col="purple", type="l", yaxt="n")
# 
# par(new=T)
# plot(pind, hrsq$SQ[pind,3], pch=19, col="red", cex=0.75, ylab="", xlab="", yaxt="n", xaxt="n")
# abline(h=1000, col="orange", lwd=2)





