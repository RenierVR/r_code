bpm <- function(x) ((60 * 50) / x )

if(!exists("len"))
  source("~/Studies/RESEARCH/BAYESIAN SURVIVAL ANALYSIS/Misc/simplot.r")

if(!exists("kalman_filter")){
  source("~/HealthQ/R scripts/window_analyser.R")
  source("~/HealthQ/R scripts/functions.r")
}

if(!exists("mdat"))
  mdat <- read.table("~/HealthQ/C FILES/OregonGITProject/data/oregonCSVs/oregonDSP 3 11_6_13 tiaan.csv", sep = ",", head = F)

if(!exists("macc")){
  macc <- 4 * sqrt(rowSums(((mdat[,7:9] - 2^15) / (2^16))^2))
  macc_x <- 4*(mdat[,7]-2^15)/(2^16)
  macc_y <- 4*(mdat[,8]-2^15)/(2^16)
  macc_z <- 4*(mdat[,9]-2^15)/(2^16)
  macc_a <- macc_x + macc_y + macc_z
}


####################################################################################
#############                                                          #############
#############   NOISE CORRUPTION
#############                                                          #############
####################################################################################

noise_corrupt <- function(ss = seq(0, 40, 0.1), phase_n1 = 1, scale_n1 = 1,
                          noise_fun = cos)
{
  windows(17, 9.5) ; par(mar = c(3.5, 4, 1.5, 1) + 0.1)
  layout(matrix(c(1,2), 2, 1, byrow = FALSE))
  
  sig_true <- 10 + sin(ss)
  sig_n1 <- 10 + scale_n1 * noise_fun(ss * phase_n1)
  
  lplot(sig_true, xlab = "", lwd = 2, ylim = range(c(sig_true, sig_n1)))
  lines(sig_n1, xlab = "", col = "purple", lwd = 2)
  
  sig_corr <- sig_true * sig_n1
  lplot(sig_corr, xlab = "", lwd = 2, col = "blue")
  
  
}

# noise_corrupt(phase_n1 = 1.7, scale_n1 = 3)



####################################################################################
#############                                                          #############
#############   KALMAN FILTER SIMULATIONS
#############                                                          #############
####################################################################################

kalman_sim <- function(ss = seq(0, 40, 0.1), noise = c("add", "mult")[1],
                       scale_true = 0.5, scale_noise = 0.5, phase_noise = 1, ylm = 2.5*c(-1,1))
{
  n <- len(ss)  
  
  #################################################################
  ## signal construction
  sig_true <- 10 + scale_true*sin(ss)
  sig_noise <- 10 + scale_noise * sin(phase_noise * ss)
  if(noise == "add")
    sig_corrupt <- sig_true + sig_noise
  else if(noise == "mult")
    sig_corrupt <- sig_true * sig_noise
  
  #################################################################
  ## filtering
  
  sig_kf <- kalman_filter(sig_corrupt, 1/sig_noise, use.opt = T)[[1]]
  
  
  #################################################################
  ## plotting
  
  windows(17, 9.5) ; par(mar = c(3.5, 4, 1.5, 1) + 0.1)
  layout(matrix(c(1,2), 2, 1, byrow = FALSE))
  
  lplot(ss, sig_true, lwd = 2, lty = 1, col = "red", xlab = "", ylim = range(c(sig_true, sig_noise)))
  lines(ss, sig_noise, lwd = 2, col = "darkslategrey")
  
  pst <- peak_detect(cbind(ss, sig_true), mm = 1, band = 5)
  abline(v = pst[,1], col = "grey", xpd = NA)
  
  par(new = T)
  lplot(ss, sig_corrupt, lwd = 3, col = "green", ann = F, xaxt = "n", yaxt = "n", lty = 5)
  legend("bottomright", lty = c(1,2,1), col = c("red", "darkslategrey", "green"), cex = 0.6,
         leg = c("true signal", "noise signal", "noise corrupted signal"))
  
  lplot(ss, sig_corrupt, lwd = 2, )
  
}

kalman_sim(phase_n = 0.6, scale_n = 1, noise = "mult")


####################################################################################
#############                                                          #############
#############   TESTING ADDITIVE NATURE OF ACCELEROMETER NOISE
#############                                                          #############
####################################################################################

add_noise_test <- function(pin = 5600:6400, acc = macc, ascl = 1,
                           xma = 1, yma = 1)
{
  windows(17, 9.5) ; par(mar = c(3.5, 4, 1.5, 1) + 0.1)
  layout(matrix(c(1,2), 2, 1, byrow = FALSE))
  
  x <- diff(mdat[pin,2])
  y <- acc[pin]
  
  x <- filter(x, rep(1/xma, xma))
  y <- filter(y, rep(1/yma, yma))
  
  lplot(scale(x), col = "darkgreen", xlab = "")
  lines(scale(y[-1]), col = "purple")

  z <- scale(x) - ascl * scale(y[-1])
  
  lplot(z, col = "red", lwd = 2)
  par(new = T)
  lplot(x, col = "grey", ann = F, yaxt = "n", xaxt = "n")
  
}

# add_noise_test(acc = macc_a, ascl = 0.5)



