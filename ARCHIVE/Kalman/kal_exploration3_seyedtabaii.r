#yy <- scale(mdat[5601:6400, 2])
#yy <- diff(scale(mdat[5600:6400, 2]))
#xx <- scale(macc[5601:6400])

yy <- scale(mdat[5850:6100,2])
xx <- c(rep(0,6), scale(macc[5850:(6100-6)]))
xmat <- cbind(macc_x, macc_y, macc_z)[5850:6100,]
xmat <- apply(xmat, 2, scale)

( opt_var <- optim(par = c(0.5, 0.5), fn = function(par) -kf_seyedtabaii_mod(par, xmat[-(1:2),], yy)$ll) )
opt_var <- abs(opt_var$par)

kfs <- kf_seyedtabaii_mod(opt_var, xmat[-(1:2),], yy)
ylm <- range(c(yy, kfs[[1]]))
wlplot(yy, ylim = ylm, lwd = 2, col = "darkgreen")
#lines(xx, col = "purple")
lines(kfs[[1]], col = "red", lwd = 2)
lines(ksmooth(1:len(kfs[[1]]), kfs[[1]], "normal", bandwidth = 5), lwd = 2, col = "blue")


###############################################################################
###############################################################################



kf_seyedtabaii_mod <- function(HG = c(0.5, 0.5), x, y){
  Xt_init <- c(0.1, 0.1, 0.1, 0.1, 0.1)
  P <- diag(5)
  Gt <- HG[1]
  Ht <- HG[2]*diag(5)
  s <- numeric(len(y))
  s[1:2] <- c(y[1], y[2])
  y <- y[-c(1:2)]
  if(len(y) != nrow(x)) stop("Inconsistent lengths for vectors x and y.")
  
  loglik = - len(y)*log(sqrt(2*pi))
  
  Xt_new <- Xt_init
  
  Xlist <- list(Xt_init)
  Plist <- list(P)
  Klist <- list(NULL)
  
  for(i in 1:len(y)){
    Ct <- c(s[i+1], s[i], x[i,1], x[i,2], x[i,3])
    vt <- y[i] - as.numeric(Ct %*% Xt_new)
    Ft <- as.numeric(Ct %*% P %*% Ct) + Gt
    
    if(Ft <= 0){
      loglik = -Inf
      break;
    }
    else{
      Kt <- (P %*% Ct) / Ft
      att <- Xt_new + Kt * vt
      Ptt <- P - P %*% Ct %*% t(Kt)
      
      loglik <- loglik - 0.5*(log(Ft) + vt*vt/Ft)
      
      Xt_new <- att
      P <- Ptt + Ht
      
      Xlist <- c(Xlist, list(Xt_new))
      Plist <- c(Plist, list(P))
      Klist <- c(Klist, list(Kt))
      
      s[i+2] <- y[i] - (Xt_new[3]*x[i,1] + Xt_new[4]*x[i,2] + Xt_new[5]*x[i,3])
    }
  }
  
  return(list(s = s, ll = loglik, Xlist = Xlist, Plist = Plist, Klist = Klist[-1]))
}



###############################################################################
###############################################################################

kf_seyedtabaii <- function(HG = c(0.5, 0.5), x, y){
  Xt_init <- c(0.1, 0.1, 0.1, 0.1)
  P <- 10*diag(4)
  Gt <- HG[1]
  Ht <- HG[2]*diag(4)
  s <- numeric(len(y))
  s[1:2] <- c(y[1], y[2])
  y <- y[-c(1:2)]
  if(len(y) != len(x)) stop("Inconsistent lengths for vectors x and y.")
  
  loglik = - len(y)*log(sqrt(2*pi))

  Xt_new <- Xt_init

  Xlist <- list(Xt_init)
  Plist <- list(P)
  Klist <- list(NULL)
  
  for(i in 1:len(y)){
    Ct <- c(s[i+1], s[i], x[i]^2, x[i])
    vt <- y[i] - as.numeric(Ct %*% Xt_new)
    Ft <- as.numeric(Ct %*% P %*% Ct) + Gt
    
    if(Ft <= 0){
      loglik = -Inf
      break;
    }
    else{
      Kt <- (P %*% Ct) / Ft
      att <- Xt_new + Kt * vt
      Ptt <- P - P %*% Ct %*% t(Kt)
  
      loglik <- loglik - 0.5*(log(Ft) + vt*vt/Ft)
  
      Xt_new <- att
      P <- Ptt + Ht

      Xlist <- c(Xlist, list(Xt_new))
      Plist <- c(Plist, list(P))
      Klist <- c(Klist, list(Kt))
      
      s[i+2] <- y[i] - (Xt_new[3]*x[i]^2 + Xt_new[4]*x[i])
    }
  }

  return(list(s = s, ll = loglik, Xlist = Xlist, Plist = Plist, Klist = Klist[-1]))
}


###############################################################################

kfs <- kf_seyedtabaii(c(0.0015, 0.00002))
wplot(kfs$s, cex = 0.6, col = "darkgreen", main = "")
lines(kfs$s, col = "green")

optim(par = c(0.5, 0.5), fn = function(par) -kf_seyedtabaii(par, x, y)$ll)




####################################################################################
#############                                                          #############
#############   SEYEDTABAII CORRUPTED PLOT
#############                                                          #############
####################################################################################

## READ IN DATA
cc <- read.table("~/HealthQ/Seyedabataii_data.txt", head = F)
cc <- apply(cc, 2, as.numeric)
aa <- cc[1:325,]
bb <- cc[326:nrow(cc),]

## APPLY GAUSSIAN FILTERS TO OBTAIN SMOOTH SIGNALS
aaks <- ksmooth(aa[,1], aa[,2], "normal", bandwidth = 0.25)
aaks_sc <- list(x = aaks[[1]], y = scale(aaks[[2]]))

bbks <- ksmooth(bb[,1], bb[,2], "normal", bandwidth = 0.25)
bbks_sc <- list(x = bbks[[1]], y = scale(bbks[[2]]))

## CONSTRUCT POLYNOMIAL NOISE SIGNAL
fs <- bbks_sc$x[c(197:228)]
ff <- function(x) -0.18 * (x - 31.155)^2 + 1.8

fsa <- c(rep(0,196), fs, rep(0, len(bbks_sc$x)-228))
ffa <- c(rep(0,196), ff(fs), rep(0, len(bbks_sc$x)-228))

## ADD RANDOM WHITE NOISE AND RECONSTRUCT CONTAMINATED SIGNAL 
ffa <- ffa + rnorm(len(ffa), 0, 0.15)
re_cont <- bbks_sc$y + ffa
#lines(bbks_sc$x, re_cont, col = "purple", lwd = 2)


opt_var <- optim(par = c(0.5, 0.5), fn = function(par) -kf_seyedtabaii(par, ffa[-(1:2)], re_cont)$ll)$par
opt_var <- abs(opt_var)

plot_cont_results(re_cont, ffa, HG = c(3, 0.1), fin = T)
plot_cont_results(re_cont, ffa, HG = opt_var, fin = F)

####################################################################################

ffa2 <- 3 * sin(0.3 * bbks_sc$x)^2 + rnorm(len(ffa), 0, 0.15)
rec2 <- bbks_sc$y + ffa2

wlplot(bbks_sc$x, ffa,  ylim = c(-2,3.5), col = "blue")
lines(bbks_sc$x, ffa2, col = "red")

opt_var <- optim(par = c(0.5, 0.5), fn = function(par) -kf_seyedtabaii(par, ffa2[-(1:2)], rec2)$ll)$par
opt_var <- abs(opt_var)

plot_cont_results(rec2, ffa2, HG = c(5, 0.01), fin = T)
plot_cont_results(rec2, ffa2, HG = opt_var, fin = F)

####################################################################################

ffa3 <- 4 * sin(0.8* bbks_sc$x)
rec3 <- bbks_sc$y + ffa3 + rnorm(len(ffa), 0, 0.15)

wlplot(bbks_sc$x, ffa,  ylim = c(-4.5,4.5), col = "blue")
lines(bbks_sc$x, ffa3, col = "red")

opt_var <- optim(par = c(0.5, 0.5), fn = function(par) -kf_seyedtabaii(par, ffa3[-(1:2)], rec3)$ll)$par
opt_var <- abs(opt_var)

plot_cont_results(rec3, ffa3, HG = c(10, 0.0001), fin = T)
plot_cont_results(rec3, ffa3, HG = opt_var, fin = F, bw = 1.5)


####################################################################################

bbks_sc <- list(x = seq(1,3,len = 410) * bbks_sc$x, y = bbks_sc$y)

####################################################################################

plot_cont_results <- function(rec, fx, HG, finplot = FALSE, ylm, bw,
                              kplot = FALSE, xplot = FALSE)
{
  ## APPLY KALMAN FILTER AS IN SEYEDBATAII ET AL
  kfs <- kf_seyedtabaii(HG, fx[-(1:2)], rec)
  
  if(missing(ylm)) ylm <- range(c(rec, fx, kfs[[1]]))
  
  ## SEQUENTIAL PLOT
  if(!finplot){
    wlplot(bbks_sc, lwd = 2, ylim = ylm, col = "darkgreen", ann = F)
    legend("topright", lwd = 2, col = "darkgreen", leg = "original", cex = 0.7)
  
    par(ask = T)
    lplot(bbks_sc, lwd = 2, ylim = ylm, col = "darkgreen", ann = F)
    lines(bbks_sc$x, fx, col = "blue", lty = 2, lwd = 2)
    legend("topright", lwd = 2, lty = 1:2, cex = 0.7,
           col = c("darkgreen", "blue"), leg = c("original", "additive noise"))
  
    par(ask = T)
    lplot(bbks_sc, lwd = 1, ylim = ylm, col = "darkgreen", ann = F)
    lines(bbks_sc$x, fx, col = "blue", lty = 2, lwd = 2)
    lines(bbks_sc$x, rec, col = "red", lwd = 2)
    legend("topright", lwd = 2, lty = c(1,2,1), cex = 0.7,
           col = c("darkgreen", "blue", "red"), leg = c("original", "additive noise", "contaminated"))
  
    par(ask = T)
    ltyf <- 2
  }
  else{
    windows(w=12, h=5)
    ltyf <- 1
  }
  
  lplot(bbks_sc, lwd = 1, ylim = ylm, col = "darkgreen", lty=ltyf, ann = F)
  lines(bbks_sc$x, rec, col = "red", lwd = 1, lty=ltyf)
  lines(bbks_sc$x, kfs[[1]], col = "purple", lwd = 2)
  legend("topright", lwd = 2, lty = c(rep(ltyf,2),1), cex = 0.7,
         col = c("darkgreen", "red", "purple"), leg = c("original", "contaminated", "kalman filtered"))
  
  if(!missing(bw)){
    par(ask = T)
    lplot(bbks_sc, lwd = 2, ylim = ylm, col = "darkgreen", lty=1, ann = F)
    lines(bbks_sc$x, rec, col = "red", lwd = 1, lty=ltyf)
    lines(bbks_sc$x, kfs[[1]], col = "purple", lwd = 1)
    lines(ksmooth(bbks_sc$x, kfs[[1]], "normal", bandwidth = bw), lwd = 2, col = "blue")
    legend("topright", lwd = 2, lty = c(1,ltyf,1, 1), cex = 0.7,
         col = c("darkgreen", "red", "purple", "blue"), leg = c("original", "contaminated", "kalman filtered", "kalman + gaussian"))
  }
  
  ####### OTHER PLOTS ####### 
  cols <- c("darkgreen", "blue", "maroon", "orange", "purple")
  
  if(xplot){
    xp <- matrix(unlist(kfs[[3]]), nr = len(as.numeric(kfs[[3]][[1]])))
    wlplot(xp[1,], lwd = 2, ylim = range(xp), col = cols[1], ann = F)
    abline(h = 0)
    for(i in 2:nrow(xp))
      lines(xp[i,], lwd = 2, col = cols[i])
    legend("topright", lwd = 2, col = cols[1:nrow(xp)], leg = paste("component", 1:nrow(xp)), cex = 0.7)
    title(main = "Components of X vector")
  }
  
  if(kplot){
    kp <- matrix(unlist(kfs[[5]]), nr = len(as.numeric(kfs[[5]][[1]])))
    wlplot(kp[1,], lwd = 2, ylim = range(kp), col = cols[1], ann = F)
    abline(h = 0)
    for(i in 2:nrow(kp))
      lines(kp[i,], lwd = 2, col = cols[i])
    legend("topright", lwd = 2, col = cols[1:nrow(xp)], leg = paste("component", 1:nrow(kp)), cex = 0.7)
    title(main = "Components of K vector")
  }
  
}




