####################################################################################
#############                                                          #############
#############   LOADING RAW MOJO DATA  
#############                                                          #############
####################################################################################

source("~/HealthQ/R scripts/functions.r")

bpm <- function(x) ((60 * 50) / x )

if(!exists("mdat"))
  mdat <- read.table("~/HealthQ/C FILES/OregonGITProject/data/oregonCSVs/oregonDSP 3 11_6_13 tiaan.csv", sep = ",", head = F)

if(!exists("macc")){
  macc <- 4 * sqrt(rowSums(((mdat[,7:9] - 2^15) / (2^16))^2))
  macc_x <- 4*(mdat[,7]-2^15)/(2^16)
  macc_y <- 4*(mdat[,8]-2^15)/(2^16)
  macc_z <- 4*(mdat[,9]-2^15)/(2^16)
}


####################################################################################
#############                                                          #############
#############   FUNCTION TO PLOT SIGNALS
#############                                                          #############
####################################################################################

# winsize = 200 ; overlap = 200; start=1; end = nrow(mdat); sleep = 0; acc_thres = 0.75; irs = 4; irma1 = 9; irma2 = 15; grma = 3;

datplot <- function(winsize=600, overlap=600, start=1, end = nrow(mdat), sleep = 0, pmm = 1,
                    acc_thres = 0.75, irs = 4, irma1 = 9, irma2 = 15, grma = 3, 
                    acma1 = 9, acmaz = 9, cadsdlim_ac = 20, cadsdlim_ir = 20)
{
  windows(17, 9.5) ; par(ask = TRUE, mar = c(3.5, 4, 1.5, 1) + 0.1)
  if(sleep > 0) par(ask = FALSE)
  layout(matrix(c(1,2,3,4,5,6), 3, 2, byrow = FALSE))
  
  green <- mdat[,2]
  ir <- mdat[,irs]
  
  accmat <- NULL
  aczmat <- NULL
  nirmat <- NULL
  j = 0
  while(TRUE){
    j = j+1
    plotind <- (overlap*(j-1)+start):(start-1 + winsize + overlap*(j-1))
    if((max(plotind) > end) || dev.cur() == 1)
      break
    else{
      
      ## tot acceleration score
      ta_score <- diff(range(macc[plotind]))
      if(ta_score > acc_thres)
        macc_col <- c("purple", "blue")
      else
        macc_col <- rep("grey",2)
      
      ## filters
      irf1 <- as.numeric(filter(ir[plotind], rep(1/irma1, irma1)))
      irf2 <- as.numeric(filter(ir[plotind], rep(1/irma2, irma2)))
      grf <- as.numeric(filter(green[plotind], rep(1/grma, grma)))
      acf1 <- as.numeric(filter(macc[plotind], rep(1/acma1, acma1)))
      acfz <- as.numeric(filter(macc_y[plotind], rep(1/acmaz, acmaz)))
      
      ## differencing and filtering of IR
      ird <- diff(ir[plotind], diff=2)
      irdf <- as.numeric(filter(ird, rep(1/irma1, irma1)))
      irdf2 <- as.numeric(filter(irdf, rep(1/7, 7)))
      irdf3 <- as.numeric(filter(irdf2, rep(1/7, 7)))
      irdf4 <- as.numeric(filter(irdf3, rep(1/5, 5)))
      
      ## plots
      plot(plotind, green[plotind], type = "l", xlab = "", ylab = "original green",
           col = "darkgreen")
      legend("topleft", leg = paste("j =",j))
      
      

      plot(plotind, macc[plotind], type = "l", xlab = "", ylab = "tot acceleration",
           col = macc_col[1])
      
      plot(plotind, ir[plotind], type = "l", xlab = "", ylab = "original/smoothed ir",
           col = "grey")
      lines(cbind(plotind, irf1), col = "maroon", lwd = 2)
      lines(cbind(plotind, irf2), col = "red", lwd = 1.5)
      
      ###################### filtered plots
  
      ##### acc signal #####
      plot(cbind(plotind, acf1), type = "l", xlab = "", ylab = "smoothed acc",
           col = macc_col[1], lwd = 2)
      points(cbind(plotind, acf1))
      abline(h = c(1.1, 1, 0.9), lwd = c(1,2,1), lty = c(2,1,2),
             col = "red")
      
      if(ta_score > acc_thres){
        acp <- peak_detect(na.omit(cbind(plotind, acf1)), band = 3, mm = pmm, 
                           show.po = T, spcol = "blue", spcex = 1.3)
        
        under_thres <- sum(acp[,2] <= 1.1 & acp[,2] >= 0.9)
        if(under_thres <= 1){
          abline(v = acp[,1], col = "lightblue")
          cadseq <- round(bpm(diff(acp[,1])),1)
          #cadseq <- cadseq[cadseq < 250]
          legend("topright", col = lcol, cex = 1.5, bg = "white",
                  paste(round(c(mean(cadseq), sd(cadseq)),2), collapse = ", "))
          
          accmat <- rbind(accmat, c(j, mean(cadseq), sd(cadseq), as.numeric(sd(cadseq) <= cadsdlim_ac)))
          

        }
        else
          accmat <- rbind(accmat, c(j, rep(0,3)))
      }
      else
        accmat <- rbind(accmat, c(j, rep(0,3)))
      
      
      ##### acc Z smoothed #####
      plot(cbind(plotind, acfz), type = "l", xlab = "", ylab = "smoothed acc Z axis",
           col = macc_col[1], lwd = 2)
      points(cbind(plotind, acfz))
      aczp <- peak_detect(na.omit(cbind(plotind, acfz)), band = 3, mm = pmm, 
                          show.po = T, spcol = "blue", spcex = 1.3)
      
      if(ta_score > acc_thres){
        abline(v =aczp[,1], col = "blue")
        
        if(under_thres <= 1){
          cadseq <- round(bpm(diff(aczp[,1])),1)
          aczmat <- rbind(aczmat, c(j, mean(cadseq), sd(cadseq), as.numeric(sd(cadseq) <= cadsdlim_ac)))
          legend("topright", col = lcol, cex = 1.5, bg = "white",
                 paste(round(c(mean(cadseq), sd(cadseq)),2), collapse = ", "))
        }
        else
          aczmat <- rbind(aczmat, c(j, rep(0,3)))
      }
      else
        aczmat <- rbind(aczmat, c(j, rep(0,3)))
      
      
      ##### ir signal #####
      plot(cbind(plotind[1:length(ird)], irdf4), type = "l", xlab = "", ylab = "smoothed IR",
           col = "maroon", lwd = 1)
      points(cbind(plotind[1:length(ird)], irdf4))
      
      irp <- peak_detect(na.omit(cbind(plotind[1:length(ird)], irdf4)), band = 3, mm = pmm,
                         show.po = T, spcol = "blue", spcex = 1.3)
      abline(v = irp[,1], col = "blue")
      
      if(ta_score > acc_thres){
        if(under_thres <= 1){
          cadseq <- round(bpm(diff(irp[,1])),1)
          nirmat <- rbind(nirmat, c(j, mean(cadseq), sd(cadseq), as.numeric(sd(cadseq) <= cadsdlim_ir)))
          legend("topright", col = lcol, cex = 1.5, bg = "white",
                 paste(round(c(mean(cadseq), sd(cadseq)),2), collapse = ", "))
        }
        else
          nirmat <- rbind(nirmat, c(j, rep(0,3)))
      }
      else
        nirmat <- rbind(nirmat, c(j, rep(0,3)))
      
    }
    
    if(sleep > 0) Sys.sleep(sleep)
  }  
  
  return(list(accmat, nirmat, aczmat))
}


dp <- datplot(winsize = 200, overlap = 50, sleep = 0.01, start = 1, pmm = 1)

### some code to test equivalence between ACC and NIR cadence estimates
# dp[[1]][,2:3] <- cbind(round(dp[[1]][,2],0), round(dp[[1]][,3],1))
# dp[[2]][,2:3] <- cbind(round(dp[[2]][,2],0), round(dp[[2]][,3],1))
# dpmat <- cbind(dp[[1]], dp[[2]])
# both_one <- dpmat[(dpmat[,4] == 1) & (dpmat[,8] == 1), c(1:3,5:7)]
# both_mean <- cbind(both_one[,c(2,5)], apply(both_one, 1, function(x) mean(x[c(2,5)])))

####################################################################################
## complete loss: about 3000 to 4000
## moderate loss: about 5750 to 12500
## incorrec incr: about 14250 to 19500
## cadence lock:  about 32250 to 40000
####################################################################################



####################################################################################
#############                                                          #############
#############   FILTERING WITH KALMAN
#############                                                          #############
####################################################################################

source("~/HealthQ/R scripts/window_analyser.R")

kalman_stuff <- function(start = 1, winsize = 600, overlap = 600)
{
  kalf <- kalman_filter(signal = mdat[,2], acc = macc, use.opt = T, acc.scale = 0.5, use.acc = F)
  kalf2 <- kalman_filter(signal = mdat[,2], acc = macc, use.opt = T, acc.scale = 2, use.acc = F)
  kalf3 <- kalman_filter(signal = mdat[,2], acc = macc, use.opt = T, acc.scale = 4, use.acc = F)
  kalf4 <- kalman_filter(signal = mdat[,2], acc = macc, use.opt = T, acc.scale = 8, use.acc = F)
  zoomzoom(kalf[[1]], y = mdat[,2], z = macc)


  y = scale(kalf4[[1]])
  z = scale(kalf2[[1]])
  x = scale(kalf3[[1]])
  windows(17, 9.5) ; par(ask = TRUE, mar = c(3.5, 4, 1.5, 1) + 0.1)
  layout(matrix(c(1), 1, 1, byrow = FALSE))
  j = 0
  while(TRUE){
    j = j+1
    plotind <- (overlap*(j-1)+start):(start-1+winsize + overlap*(j-1))
    if(max(plotind) > length(y))
      break
    else{
      plot(plotind, y[plotind], type = "l", lwd = 1.5, col = "blue", xlab = "")
      lines(plotind, z[plotind], type = "l", lwd = 1.5, col = "darkgreen", xlab = "")
      lines(plotind, x[plotind], type = "l", lwd = 1.5, col = "maroon", xlab = "")
      legend("bottomright", col = c("blue", "darkgreen", "maroon"), lwd = 2, leg = c(0.5, 2, 4))
    }
  }
}


