
source("~/HealthQ/R scripts/HealthQ_Kalman/sq_kalman01.r")


###########################################################
###########################################################
######
###### READ IN DATA
######

fpath <- "~/HealthQ/Data/2de run/ModC_datasets/ModC_With_Reference/"

# dat_input <- c(1,1,0,15)      # 20140217 | Dataset001_Dirk_Fiets
# dat_input <- c(1,2,100,0)     # 20140217 | Dataset002_Franco_TreadmillWeights
# dat_input <- c(1,3,90,0)      # 20140217 | Dataset003_Dirk_Running

# dat_input <- c(2,1,70,0)      # 20140220 | Dataset001_Franco roei"

# dat_input <- c(3,1,0,300)     # 20140221 | Dataset001_Marc_AbsWorkout

# dat_input <- c(4,1,0,20)      # 20140222 | Dataset001_Benita_MountainRun
# dat_input <- c(4,2,100,0)     # 20140222 | Dataset002_ReinetteVanDerMerwe_MountainRun
# dat_input <- c(4,3,30,0)      # 20140222 | Dataset003_Nicol_BicycleRun
# dat_input <- c(4,4,0,60)      # 20140222 | Dataset004_Franco_Gym
# dat_input <- c(4,5,0,100)     # 20140222 | Dataset005_Liani_Gym

# dat_input <- c(5,1,50,0)      # 20140223 | Dataset001_Riaan_MountainRun
# dat_input <- c(5,2,70,0)      # 20140223 | Dataset002_Franco_Tennis
# dat_input <- c(5,3,400,400)   # 20140223 | Dataset004_Renier_MountainBootcamp
# dat_input <- c(5,4,100,1150)  # 20140223 | Dataset005_Kari_MountainCycling

######################################################################################

if(!exists("dat_input_old"))
  dat_input_old <- rep(0,4)

if(!all(dat_input == dat_input_old)){
  
  dat_date <- c("20140217", "20140220", "20140221", "20140222", "20140223")[dat_input[1]]
  if(dat_date == "20140217")
    dat_foldname <- c("Dataset001_Dirk_Fiets", 
                      "Dataset002_Franco_TreadmillWeights",
                      "Dataset003_Dirk_Running")[dat_input[2]]
  if(dat_date == "20140220")
    dat_foldname <- c("Dataset001_Franco roei")[dat_input[2]]
  if(dat_date == "20140221")
    dat_foldname <- c("Dataset001_Marc_AbsWorkout")[dat_input[2]]
  if(dat_date == "20140222")
    dat_foldname <- c("Dataset001_Benita_MountainRun",
                      "Dataset002_ReinetteVanDerMerwe_MountainRun",
                      "Dataset003_Nicol_BicycleRun",
                      "Dataset004_Franco_Gym",
                      "Dataset005_Liani_Gym")[dat_input[2]]
  if(dat_date == "20140223")
    dat_foldname <- c("Dataset001_Riaan_MountainRun",
                      "Dataset002_Franco_Tennis",
                      "Dataset004_Renier_MountainBootcamp",
                      "Dataset005_Kari_MountainCycling")[dat_input[2]]


  dat_mojo <- read.table(paste0(fpath, dat_date, "/", dat_foldname, "/mojo.txt"), head=T, sep=",")
  if(ncol(dat_mojo) == 19)
    dat_mojo <- dat_mojo[,-(16:18)]
  if(ncol(dat_mojo) == 20)
    dat_mojo <- dat_mojo[,-c(16:18,20)]
  dat_bioh <- read.table(paste0(fpath, dat_date, "/", dat_foldname, "/ref.csv"), head=T, sep=",")
  dat_bioh <- dat_bioh[,2]

}



###########################################################
###########################################################
######
###### KALMAN FILTER INPUT DATA SET
######

hrsq <- wmpfun(x=dat_mojo, grn=3, gauss=7, dif=2, show=F, acc_chan=3)
hrsqmat <- cbind(hrsq$HR, hrsq$SQ)
colnames(hrsqmat) <- c("HBC_HR", "SQ_specflat", "SQ_acc_mn", "SQ_acc_md", "SQ_entropy", "SQ_skew")
Gt1 <- 5*100*(hrsqmat[,2])
Gt2 <- 1*100*(hrsqmat[,5])
#Gt3 <- 1*50*(1/hrsqmat[,6])
#Gt3 <- 0.0003 * exp(1/((1-hrsqmat[,6])^2) -1)
Gt3 <- 30*(1-hrsqmat[,6]^3)
Gt3_sm <- exp_smooth(Gt3, 0.2)



# HRrma <- retro_ma(hrsqmat[,1], 3)
# Tvec <- rep(1, nrow(hrsqmat))
# for(i in 1:nrow(hrsqmat))
#   if(hrsqmat[i,4] >= 1.5)
#     if(HRrma[i,3] == 0)
#       Tvec[i] <- 1.01

is_decr <- detect_hr_drop(hrsqmat[,1], hrsqmat[,4], check_prev=1, mx_jump=3)
is_decr <- extend_hr_drop(is_decr, 4)
Tvec <- rep(1, nrow(hrsqmat))
Tvec[is_decr == 1] <- 1.0025
Gt4 <- rep(1, nrow(hrsqmat))
Gt4[is_decr == 1] <- 100

aa <- hr_kf(hrsqmat[,1], use_opt=F, Ht=1, Gt= Gt3_sm, Tt=Tvec)

pind = 1:(length(aa[[1]]) - dat_input[3])
# pind = 500:1700
windows(14,7) ; par(bg="grey80")
plot(pind, Gt3_sm[pind+dat_input[3]], pch=19, cex=0.6, col="yellow", ann=F, yaxt="n", xaxt="n")
lines(pind, Gt3_sm[pind+dat_input[3]], col="yellow")
axis(side=4)
par(new=TRUE)
# plot(pind, hrsqmat[pind+dat_input[3],4], pch=19, cex=0.7, col="yellow", ann=F, xaxt="n", yaxt="n")
# axis(side=4)
# abline(h=1.75, col="yellow")
# par(new=TRUE)
plot(pind, hrsqmat[pind+dat_input[3],1], type="n", xlab="time (s)", ylab="HR", main=paste(dat_foldname, "|", dat_date))
lines(pind, dat_bioh[pind+dat_input[4]], col="black", lwd=2)
lines(pind, hrsqmat[pind+dat_input[3],1], col="purple", lwd=2)
lines(pind, aa[[1]][pind+dat_input[3]], col="red", lwd=2)


# bb <- adapt_ma(hrsqmat, 15)
# lines(pind, bb[pind+dat_input[3]], col="limegreen", lwd=2)




# pind = 1:nrow(hrsqmat)
# #pind = 500:1500
# windows(14,7) ; par(bg="grey80")
# plot(pind, hrsqmat[pind,1], type="n")
# abline(v=which(hrsqmat[,4] >= 1.5), col="khaki1")
# abline(v=which(Tvec != 1), col="blue")
# abline(v=which(is_decr == 1), col="green")
# lines(pind, hrsqmat[pind,1], type="l", lwd=2, col="red")





dat_input_old <- dat_input