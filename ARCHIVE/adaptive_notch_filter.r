


ALNF <- function(signal_in, gamma_final=0.9)
{
  # Adaptive lattice notch v2 (Author: Van Zyl)
  # (ported to R: Renier)
 
  n <- len(signal_in)
  out_mat <- matrix(0, nr=n, nc=3)
  
  
  #####  STATIC VARIABLES #####
  first_notch <- TRUE
  first_smooth <- TRUE
  
  green_smooth <- 0
  
  X_t0 <- 0
  X_t1 <- 0
  X_t2 <- 0
  
  error_t0 <- 0
  error_t1 <- 0
  error_t2 <- 0
  
  C_t0 <- 0.3
  C_t1 <- 0.3
  
  D_t0 <- 0.4
  D_t1 <- 0.4
  
  k_t0 <- -0.96858
  k_t1 <- -0.96858
  
  buff_down_count <- 50
  buff_down_count2 <- 250+50
  
  ##########################################

  
  for(i in 1:n){
  
    radius <- 0.95
    lamda <- 0.75 # Memory factor
    gamma <- 0.9 # Smoothing factor
  
  
  
#   if(resetALNF){
#     first_notch <- TRUE
#     
#     lamda <- 0.5
#     gamma <- 0.9
#         
#     green_smooth <- 0
#     
#     X_t0 <- 0
#     X_t1 <- 0
#     X_t2 <- 0
#     
#     error_t0 <- 0
#     error_t1 <- 0
#     error_t2 <- 0
#     
#     C_t0 <- 0.3
#     C_t1 <- 0.3
#     
#     D_t0 <- 0.4
#     D_t1 <- 0.4
#     
#     k_t0 <- -0.96858
#     k_t1 <- -0.96858
#     
#     buff_down_count <- filter_delay
#     buff_down_count2 <- 250 + buff_down_count
#     
#     first_smooth <- TRUE
#     
#     resetALNF <- FALSE
#   }
  
  
  
    if(first_smooth){
      green_smooth <- signal_in[i]
      first_smooth <- FALSE
    }
  
    green_smooth <- 0.85*green_smooth + 0.15*signal_in[i]
    X_t0 <- signal_in[i] - green_smooth - k_t0*(1+radius)*X_t1 - radius*X_t2 #High pass
  
    # filter output
    error_t0 <- X_t0 + 2*k_t0*X_t1 + X_t2
  
  
  
    if(buff_down_count > 0)
      buff_down_count <- buff_down_count-1
    else{
      if(buff_down_count2 > 0)
        buff_down_count2 <- buff_down_count2-1
      else{
        lamda <- 0.99
        gamma <- gamma_final
      }
    
      if(first_notch){
        D_t0 <- (1.0-lamda)*2*X_t1*X_t1
        C_t0 <- (1.0-lamda)*X_t1*(X_t0+X_t2)
      }
      else{
        D_t0 <- lamda*D_t1 + (1.0-lamda)*2*X_t1*X_t1
        C_t0 <- lamda*C_t1 + (1.0-lamda)*X_t1*(X_t0+X_t2)
      }
    
      k_t0 <- -C_t0/(D_t0+1.0)
      if (k_t0 > 1.0) k_t0 <- 1.0
      if (k_t0 < -1.0) k_t0 <- -1.0
    
      if(first_notch){
        k_t1 <- k_t0
        first_notch <- FALSE
      }
    
      k_t0 <- gamma*k_t1 + (1-gamma)*k_t0
    }
  
    k_t1 <- k_t0
    X_t2 <- X_t1
    X_t1 <- X_t0
    D_t1 <- D_t0
    C_t1 <- C_t0
  
    out_freq <-  acos(-k_t0)/(2*3.14159265359)*50
    
    out_mat[i,] <- c(green_smooth, error_t0, out_freq)
  }

  return(out_mat)

}





