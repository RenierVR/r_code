source("../tiaan_specgram.r")
source("../temporalCorr.r")
source("./RLS_wiki.r")

acc_res <- function(mat, scaling=12)
  return(scaling*sqrt(rowSums(((mat - 2^15) / (2^16))^2)) )



njr_names <- c("20140512_2131_MojoNJR1_Bluetooth_RenierVanRooyen_001",
               "20140513_1520_MojoNJR1_Bluetooth_PhillipLotz_001",
               "20140513_2234_MojoNJR1_Bluetooth_DirkBadenhorst_002",
               "20140514_0958_MojoNJR1_GregorRohrig_001",
               "20140514_1434_MojoNJR1_Bluetooth_CobusHavenga_001",
               "20140514_1624_MojoNJR1_Bluetooth_AdamVanHeerden_001",
               "20140514_1826_MojoNJR1_Bluetooth_JohanVanDerMerwe_004",
               "20140515_1327_MojoNJR1_Bluetooth_ChristiaanHugo_002",
               "20140515_1525_MojoNJR1_Bluetooth_JohanJansenVanRensburg_003",
               "20140515_1722_MojoNJR1_Bluetooth_GalenWright_005",
               "20140516_0724_ModC14_UpperArm_RoberHenning_001",
               "20140516_0725_MojoNJR3_NAND_Wrist_RobertHenning_001",
               "20140516_0918_ModC14_UpperArm_FrancoisDuToit_002",
               "20140516_0918_MojoNJR3_NAND_Wrist_FrancoisDuToit_002",
               "20140516_1322_ModC14_UpperArm_WimConradie_003",
               "20140516_1814_ModC14_UpperArm_CarelVanWyk_004",
               "20140516_1814_MojoNJR3_NAND_Wrist_CarelVanWyk_004",
               "20140517_1642_ModC14_RenierVanRooyen_001",
               "20140517_1642_MojoNJR3_NAND_RenierVanRooyen_001")
njr_names <- paste0(njr_names, ".txt")


kvec <- c(-3, -2, -3, -2, -3,
           0,  0, -4,  0, -3,
          -6, -5, -1,  0,  7,
           5, -2,  8, -4)
          
          

for(i in c(1, 3, 5, 8, 9, 10, 12, 13, 14, 16, 17, 18))
# for(i in c(1, 3, 5, 10))
{
  cat("reading in dataset ", i, "...\n")
  cat(njr_names[i], "\n\n")
  dat <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/data/NJRTestWeek/", njr_names[i]), sep=",")
  
  grn_ch <- dat[,1] - dat[,2]
  acc_ch <- acc_res(dat[,3:5])
  
  grn_ch <- removeAGCJumps(grn_ch,500)
  
  grn_ch <- as.numeric(diff(grn_ch, diff=2))
  acc_ch <- as.numeric(diff(acc_ch, diff=2))
  
  grn_ch <- ksmooth(1:length(grn_ch), grn_ch, "normal", band=8)$y
  acc_ch <- ksmooth(1:length(acc_ch), acc_ch, "normal", band=8)$y
  
  grn_ch <- grn_ch[-c(1:100)]
  acc_ch <- acc_ch[-c(1:100)]
  
  #### SPECTROGRAM ####
  windows(14,6) ; par(mar=c(2,3,2,2))
  tiaan_specgram(grn_ch, windowSize=512, overlap=256, normal=1, 
                 output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T)
#   title(main=paste(njr_names[i], "| GREEN LED"))
  
  windows(14,6) ; par(mar=c(2,3,2,2))
  tiaan_specgram(acc_ch, windowSize=512, overlap=256, normal=1, 
                 output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T)
#   title(main=paste(njr_names[i], "| ACCELEROMETER"))
  
  #### TEMPORAL CORRELATION ####
  tc_out <- temporalCorr(grn_ch, acc_ch, yn="ACC", winsize=128, overlap=32, bw=500 ,k=0)#, method="ccfmax")#, k=kvec[i])
  
}




#######################################################################################################

## RIAAN OORSEE GARMIN DATA
dat <- read.table("~/HealthQ/Data/riaan-oorsee-data_2014-08-19 15-32-5116-parsed.csv", sep=",", head=T)

## RENIER OEFEN TOUSPRING DATA
dat <- read.table("~/HealthQ/Data/20140815_1530_RenierVanRooyen_LifeQMojo11.1.10_Exercise_Skipping (1).csv", sep=",", head=T)


grn_ch <- dat[,3] - dat[,4]
acc_ch <- acc_res(dat[,5:7])

grn_ch <- removeAGCJumps(grn_ch,500)

grn_ch <- as.numeric(diff(grn_ch, diff=1))
acc_ch <- as.numeric(diff(acc_ch, diff=1))

grn_ch <- ksmooth(1:length(grn_ch), grn_ch, "normal", band=8)$y
acc_ch <- ksmooth(1:length(acc_ch), acc_ch, "normal", band=8)$y

grn_ch <- grn_ch[-c(1:100)]
acc_ch <- acc_ch[-c(1:100)]

w(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(grn_ch, windowSize=512, overlap=256, blankFactor=3, maxfreq=3.5, normal=1)

w(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(acc_ch, windowSize=512, overlap=256, blankFactor=3, maxfreq=3.5, normal=1)


pind <- 20000:22000
w() ; par(mfrow=c(2,1), mar=c(2,3,2,2))
lplot(pind, grn_ch[pind], lwd=2)
lplot(pind, acc_ch[pind], lwd=2, col="purple")



#########################################################################################

cout <- read.table("~/HealthQ/C FILES/RLS_NS_project/out_fitted.txt")[-c(1:100),1]

w(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(cout, windowSize=512, overlap=256, blankFactor=3, maxfreq=3.5, normal=2)
title(main="RLS order 15")

#########################################################################################

bioh <- read.table("~/HealthQ/Data/20140815_1530_RenierVanRooyen_Bioharness_Summary_Exercise_Skipping.txt", sep=",", head=T)[,2]


cout <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/phys_calc_output.txt", sep=",")[,1]
cout <- cout[seq(1,len(cout),50)]


w() ; lplot(bioh, lwd=3, col="grey", ylab="HR") 
lines(cout_b[-c(1:15)], lwd=3, col="red") ; lines(cout[-c(1:15)], lwd=3, col="forestgreen")




