


w <- function(width=14, height=7, ...) x11(width, height, ...)
lplot <- function(..., col="forestgreen") plot(..., type="l", col=col)
len <- function(...) length(...)





removeAGCJumps <- function(xx, threshold=500)
{
  xx_adjust <- numeric(length(xx))
  running_DC <- 0
  
  for(i in 2:length(xx)){
    previousSample <- xx[i-1]
    grn1 <- xx[i]
    if(abs(previousSample - grn1) > threshold)
      running_DC <- running_DC + (grn1 - previousSample);
    xx_adjust[i] <- grn1 - running_DC;
  }
  
  return(xx_adjust)
}





##################################################################################################################
##################################################################################################################


ti_sets <- c("data_TI_subject1.txt", "data_TI_subject2.txt", "data_TI_subject3.txt", "data_TI_subject4.txt", 
             "data_TI_Subject5_Karthik_run1.txt", "data_TI_Subject5_Karthik_run2.txt",
             "data_TI_Karthik_JRC_300pm.txt", "data_TI_Karthik_Osram_347pm.txt")

ti_names <- c("Subject 1", "Subject 2", "Subject 3", "Subject 4", 
              "Subject 5: Karthik run 1", "Subject 5: Karthik run 2",
              "Karthik JRC 300pm", "Karthik Osram 347pm")

for(i in 1:len(ti_sets)){

  ti_set <- ti_sets[i]
  ti_nm  <- substr(ti_set, 1, nchar(ti_set)-4)

  dat_in  <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/data/TI_data/", ti_set), sep=",")
  hr_out  <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out_", ti_set), sep=",")[,1]

  polar <- dat_in[,7]
  polar[is.na(polar)] <- 0
  
  w() ; par(mar=c(4,4,2,2))
  hrs <- cbind(polar, dat_in[,6], hr_out)
  xx <- 1:nrow(hrs)/50
  lplot(xx, hrs[,1], ylim=range(hrs), lwd=4, col="grey", xlab="time (s)", ylab="bpm", main=ti_names[i])
  lines(xx, hrs[,2], lwd=3, col="red")
  lines(xx, hrs[,3], lwd=3, col="blue")
  legend("topleft", bty="n", lwd=c(3,3,3), col=c("grey", "red", "blue"), leg=c("Polar", "Other solution", "Our solution"))
  
#  savePlot(paste0("./TI_results/", ti_nm, ".png"), "png")
#  graphics.off()
  
  
  grn_ch <- dat_in[,1] - dat_in[,2]
  grn_ch <- removeAGCJumps(grn_ch, 500)
  grn_ch <- as.numeric(diff(grn_ch, diff=1))
  grn_ch <- ksmooth(1:length(grn_ch), grn_ch, "normal", band=6)$y

  w(14,6) ; par(mar=c(2,3,2,2))
  tiaan_specgram(grn_ch, windowSize=512, overlap=256, blankFactor=3, maxfreq=3.5, normal=1,
	    		 line_overlay=hr_out[(len(hr_out) - len(grn_ch) + 1):len(hr_out)]/60)
				 
				 
}