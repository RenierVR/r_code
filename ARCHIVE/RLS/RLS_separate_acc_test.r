
aa <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/data/NJRTestWeek/20140512_2131_MojoNJR1_Bluetooth_RenierVanRooyen_001.txt", sep=",")


grn_ch <- aa[,1] - aa[,2]

grn_ch <- removeAGCJumps(grn_ch, 500)
grn_ch <- as.numeric(diff(grn_ch, diff=1))
grn_ch <- ksmooth(1:length(grn_ch), grn_ch, "normal", band=6)$y


acc_ch <- aa[,5]
acc_ch <- as.numeric(diff(acc_ch, diff=1))
acc_ch <- ksmooth(1:length(acc_ch), acc_ch, "normal", band=6)$y


write.table(cbind(grn_ch, acc_ch), file="~/HealthQ/C FILES/RLS_NS_project/data/preprocessed/galen_accx.txt",
            sep=",", quote=F, row.names=F, col.names=F)


bb <- read.table("~/HealthQ/C FILES/RLS_NS_project/out_fitted_galen_accx.txt", sep=",")[,1]


windows(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(bb[,1], windowSize=512, overlap=256, normal=1, Fs=25,
               output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T)




#############################################################################################
#############################################################################################
#############################################################################################

njr_names <- paste0(c("20140512_2131_MojoNJR1_Bluetooth_RenierVanRooyen_001",
               "20140513_1520_MojoNJR1_Bluetooth_PhillipLotz_001",
               "20140513_2234_MojoNJR1_Bluetooth_DirkBadenhorst_002",
               "20140514_0958_MojoNJR1_GregorRohrig_001",
               "20140514_1434_MojoNJR1_Bluetooth_CobusHavenga_001",
               "20140514_1624_MojoNJR1_Bluetooth_AdamVanHeerden_001",
               "20140514_1826_MojoNJR1_Bluetooth_JohanVanDerMerwe_004",
               "20140515_1327_MojoNJR1_Bluetooth_ChristiaanHugo_002",
               "20140515_1525_MojoNJR1_Bluetooth_JohanJansenVanRensburg_003",
               "20140515_1722_MojoNJR1_Bluetooth_GalenWright_005",
               "20140516_0724_ModC14_UpperArm_RoberHenning_001",
               "20140516_0725_MojoNJR3_NAND_Wrist_RobertHenning_001",
               "20140516_0918_ModC14_UpperArm_FrancoisDuToit_002",
               "20140516_0918_MojoNJR3_NAND_Wrist_FrancoisDuToit_002",
               "20140516_1322_ModC14_UpperArm_WimConradie_003",
               "20140516_1814_ModC14_UpperArm_CarelVanWyk_004",
               "20140516_1814_MojoNJR3_NAND_Wrist_CarelVanWyk_004",
               "20140517_1642_ModC14_RenierVanRooyen_001",
               "20140517_1642_MojoNJR3_NAND_RenierVanRooyen_001"), ".txt")




out_list <- vector("list", 4)
for(i in 1:4){
  out <- read.table(paste0("~/HealthQ/C FILES/RLS_NS_project/out_fitted_", njr_names[i]), sep=",")
  out_list[[i]] <- out
}




for(i in 1:4){
  out   <- out_list[[i]]
  out_b <- out_b_list[[i]]
  
  for(j in 1:3)
    print(all(round(out[,j] - out_b[,j], 5) == 0))
  
  cat("\n\n")
}















