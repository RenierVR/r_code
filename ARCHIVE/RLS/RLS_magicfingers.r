source("./RLS_wiki.r")
source("../tiaan_specgram.r")
source("../temporalCorr.r")

# colfun <- blue2green2red
# colfun <- colorRampPalette(c("black", "black", "blue", "green", "yellow", "red"))

acc_res <- function(mat, scaling=12)
  return(scaling*sqrt(rowSums(((mat - 2^15) / (2^16))^2)) )


mfsets <- c("renier_magic_fingers_01.csv",
            "20140716_1540_RenierVanRooyen_LifeQMojo10.2.16_Testing_Typing.csv",
            "20140715_1630_RenierVanRooyen_LifeQMojo10.2.16_WristMotion_HRV_Rest.txt",
            "20140804_1630_RenierVanRooyen_LifeQMojo11.1.12_Testing_WristMotion.txt",
            "20140811_1132_Renier_MagicFingers_2G_Mojo11.1.8.txt",
            "20140812_Renier_pos_test_forearm_in.txt",
            "20140812_Renier_pos_test_forearm_out.txt",
            "20140812_Renier_pos_test_wrist_in.txt",
            "20140812_Renier_pos_test_wrist_in.txt")


acc_test_sets <- c("acc_test_magic_fistpump_type_4g.txt", 
                   "acc_test_magic_fistpump_type_2g.txt",
                   "20140813_renier_acc_test_6gto2g.txt",
                   "20140813_renier_acctest_magic_fistpump_type_6g.txt",
                   "20140813_renier_acctest_magic_fistpump_type_2g.txt")




i <- 1
dat <- read.table(paste0("~/HealthQ/C FILES/RLS_NS_project/data/magic_fingers/", mfsets[i]), sep=",", head=T)





###################################################################################################################
###################################################################################################################



grn_ch <- dat[,3] - dat[,4]
nir_ch <- dat[,1] - dat[,2]
acc_ch <- acc_res(dat[,5:7])

# nir_ch <- removeAGCJumps(nir_ch, 500)

grn_ch <- as.numeric(diff(grn_ch, diff=1))
nir_ch <- as.numeric(diff(nir_ch, diff=1))
acc_ch <- as.numeric(diff(acc_ch, diff=1))

grn_ch <- ksmooth(1:length(grn_ch), grn_ch, "normal", band=6)$y
nir_ch <- ksmooth(1:length(nir_ch), nir_ch, "normal", band=6)$y
acc_ch <- ksmooth(1:length(acc_ch), acc_ch, "normal", band=6)$y

# grn_ch <- signal::filter(butter(6, 4/25, type="low"), grn_ch)
# grn_ch <- signal::filter(butter(6, 0.5/25, type="high"), grn_ch)
# nir_ch <- signal::filter(butter(6, 4/25, type="low"), nir_ch)
# nir_ch <- signal::filter(butter(6, 0.5/25, type="high"), nir_ch)


if(i == 1){
  grn_ch <- grn_ch[2000:13000]
  nir_ch <- nir_ch[2000:13000]
  acc_ch <- acc_ch[2000:13000]
}
if(i == 4){
  grn_ch <- grn_ch[3000:25000]
  nir_ch <- nir_ch[3000:25000]
  acc_ch <- acc_ch[3000:25000]
}



#################################################
# dat[,1] <- nir_ch
# write.table(dat, file=paste0("~/HealthQ/C FILES/RLS_NS_project/data/magic_fingers/", mfsets[i], "2"), sep=",",
#             quote=F, row.names=F, col.names=F)
#################################################
################################################## 
# write.table(cbind(grn_ch, nir_ch), 
#             file="~/HealthQ/C FILES/RLS_NS_project/data/preprocessed/renier_magicfingers_0804_pp.txt",
#             quote=F, row.names=F, col.names=F, sep=",")
#################################################


windows(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(grn_ch, windowSize=512, overlap=256, normal=1, 
               output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T)
title(main="GREEN signal")

windows(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(nir_ch, windowSize=512, overlap=256, normal=1, 
               output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T)
title(main="NIR signal")

windows(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(acc_ch, windowSize=512, overlap=256, normal=1, 
               output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T)
title(main="ACCELEROMETER signal")


cout <- read.table(paste0("~/HealthQ/C FILES/RLS_NS_project/out_fitted_", mfsets[i]), sep=",")[,1]
cout <- cout[3000:25000]

windows(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(cout, windowSize=512, overlap=256, normal=1, 
               output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T,
               col=colfun)
title(main="NS-FRLS, order 15, NIR")




####################################################################################################
####################################################################################################
####################################################################################################




winsize=512 ; overlap=1 ; start=1
w(14,8)
par(ask = TRUE, mfrow=c(2,1), mar=c(2,3,2,2))
j = 0
while(TRUE){
  j = j+1
  plotind <- (overlap*(j-1)+start):(start-1+winsize + overlap*(j-1))
  if(max(plotind) > length(grn_ch))
    break
  else{
    lplot(plotind, grn_ch[plotind], col="limegreen")
    lplot(plotind, nir_ch[plotind], col="purple")
  }
}




####################################################################################################
####################################################################################################
####################################################################################################


kvec <- matrix(c(-6,  2,
                  3, -1,
                 -7,  3,
                 10,  1,
                -12,  2,
                -13, -1,
                 -9, -2,
                  8, -5,
                  8, -5), nc=2, byrow=TRUE)

for(i in 1:5){
  
  dat <- read.table(paste0("~/HealthQ/C FILES/RLS_NS_project/data/magic_fingers/", mfsets[i]), sep=",", head=T)
  
  grn_ch <- dat[,3] - dat[,4]
  nir_ch <- dat[,1] - dat[,2]
  acc_ch <- acc_res(dat[,5:7])
  
  grn_ch <- removeAGCJumps(grn_ch, 500)
  nir_ch <- removeAGCJumps(nir_ch, 500)
  
  grn_ch <- as.numeric(diff(grn_ch, diff=1))
  nir_ch <- as.numeric(diff(nir_ch, diff=1))
  acc_ch <- as.numeric(diff(acc_ch, diff=1))
  
  grn_ch <- ksmooth(1:length(grn_ch), grn_ch, "normal", band=6)$y
  nir_ch <- ksmooth(1:length(nir_ch), nir_ch, "normal", band=6)$y
  acc_ch <- ksmooth(1:length(acc_ch), acc_ch, "normal", band=6)$y
  
  offset <- 100
  grn_ch <- grn_ch[offset:(length(grn_ch)-offset)]
  nir_ch <- nir_ch[offset:(length(nir_ch)-offset)]
  acc_ch <- acc_ch[offset:(length(acc_ch)-offset)]
  
  #### SPECTROGRAM ####
#   windows(14,6) ; par(mar=c(2,3,2,2))
#   tiaan_specgram(grn_ch, windowSize=512, overlap=256, normal=1, 
#                  output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T)
#   title(main=paste(mfsets[i], "| GREEN LED"))
#   
#   windows(14,6) ; par(mar=c(2,3,2,2))
#   tiaan_specgram(nir_ch, windowSize=512, overlap=256, normal=1, 
#                  output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T)
#   title(main=paste(mfsets[i], "| NIR LED"))
#   
#   windows(14,6) ; par(mar=c(2,3,2,2))
#   tiaan_specgram(acc_ch, windowSize=512, overlap=256, normal=1, 
#                  output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T)
#   title(main=paste(mfsets[i], "| ACCELEROMETER"))
  
  #### TEMPORAL CORRELATION ####
#   temporalCorr(grn_ch, acc_ch, nir_ch, yn=c("ACC", "NIR"), winsize=1024, overlap=64, bw=500, k=c(-12,38))

  temporalCorr(grn_ch, acc_ch, nir_ch, yn=c("ACC", "NIR"), winsize=512, overlap=32, bw=500, method="fftmultmax")#, k=kvec[i,])
  
}




#######################################################################################################################
#######################################################################################################################


grn_ch <- c(grn_ch1, rep(0,100), grn_ch2)
nir_ch <- c(nir_ch1, rep(0,100), nir_ch2)
acc_ch <- c(acc_ch1, rep(0,100), acc_ch2)




kvec <- matrix(c(-8,  4,
                 -3,  8), nc=2, byrow=TRUE)



# dat1 <- dat[1:3813,]
# dat2 <- dat[3824:7542,]

for(i in 1:length(acc_test)){

  
  dat <- read.table(paste0("~/HealthQ/C FILES/RLS_NS_project/data/acc_testing/", acc_test_sets[i]), sep=",")
  
  grn_ch <- dat[,3] - dat[,4]
  nir_ch <- dat[,1] - dat[,2]
  acc_ch <- acc_res(dat[,5:7])
  
  grn_ch <- removeAGCJumps(grn_ch, 500)
  nir_ch <- removeAGCJumps(nir_ch, 500)
  
  grn_ch <- as.numeric(diff(grn_ch, diff=1))
  nir_ch <- as.numeric(diff(nir_ch, diff=1))
  acc_ch <- as.numeric(diff(acc_ch, diff=1))
  
  grn_ch <- ksmooth(1:length(grn_ch), grn_ch, "normal", band=5)$y
  nir_ch <- ksmooth(1:length(nir_ch), nir_ch, "normal", band=5)$y
  acc_ch <- ksmooth(1:length(acc_ch), acc_ch, "normal", band=5)$y
  
  offset <- 100
  grn_ch <- grn_ch[offset:(length(grn_ch)-offset)]
  nir_ch <- nir_ch[offset:(length(nir_ch)-offset)]
  acc_ch <- acc_ch[offset:(length(acc_ch)-offset)]
  
  
#   grn_ch1 <- grn_ch ; nir_ch1 <- nir_ch ; acc_ch1 <- acc_ch
#   grn_ch2 <- grn_ch ; nir_ch2 <- nir_ch ; acc_ch2 <- acc_ch
  
  
  #### SPECTROGRAM ####
  windows(14,6) ; par(mar=c(2,3,2,2))
  tiaan_specgram(grn_ch, windowSize=512, overlap=256, normal=1, 
                 output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T)
  title(main=paste(acc_test_sets[i], "| GREEN LED"))
  
  windows(14,6) ; par(mar=c(2,3,2,2))
  tiaan_specgram(nir_ch, windowSize=512, overlap=256, normal=1, 
                 output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T)
  title(main=paste(acc_test_sets[i], "| NIR LED"))
  
  windows(14,6) ; par(mar=c(2,3,2,2))
  tiaan_specgram(acc_ch, windowSize=512, overlap=256, normal=1, 
                 output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T)
  title(main=paste(acc_test_sets[i], "| ACCELEROMETER"))
  
  #### TEMPORAL CORRELATION ####
#     temporalCorr(grn_ch, acc_ch, nir_ch, yn=c("ACC", "NIR"), winsize=512, overlap=32, bw=500, k=kvec[i,])
  
  temporalCorr(grn_ch, acc_ch, nir_ch, yn=c("ACC", "NIR"), winsize=1024, overlap=64, bw=500, method="ccfmax")
  temporalCorr(grn_ch2, acc_ch2, nir_ch2, yn=c("ACC", "NIR"), winsize=1024, overlap=64, bw=500, method="ccfmax")
}






acc1 <- dat[,5] ; acc2 <- dat[,6] ; acc3 <- dat[,7]
acc1 <- as.numeric(diff(acc1, diff=1))
acc2 <- as.numeric(diff(acc2, diff=1))
acc3 <- as.numeric(diff(acc3, diff=1))

acc1 <- ksmooth(1:length(acc1), acc1, "normal", band=5)$y
acc2 <- ksmooth(1:length(acc2), acc2, "normal", band=5)$y
acc3 <- ksmooth(1:length(acc3), acc3, "normal", band=5)$y

offset <- 100
acc1 <- acc1[offset:(length(acc1)-offset)]
acc2 <- acc2[offset:(length(acc2)-offset)]
acc3 <- acc3[offset:(length(acc3)-offset)]



temporalCorr(grn_ch, acc1, nir_ch, yn=c("ACC", "NIR"), winsize=1024, overlap=64, bw=500, method="ccfmax")
temporalCorr(grn_ch, acc2, nir_ch, yn=c("ACC", "NIR"), winsize=1024, overlap=64, bw=500, method="ccfmax")
temporalCorr(grn_ch, acc3, nir_ch, yn=c("ACC", "NIR"), winsize=1024, overlap=64, bw=500, method="ccfmax")
