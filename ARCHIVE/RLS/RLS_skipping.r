source("../tiaan_specgram.r")
source("../temporalCorr.r")
source("./RLS_wiki.r")

acc_res <- function(mat, scaling=12)
  return(scaling*sqrt(rowSums(((mat - 2^15) / (2^16))^2)) )


#########################################################################################################
#########################################################################################################

mojo_fpaths <- list(c("~/HealthQ/Data/Skipping/20140815_0700_DirkBadenhorst_LifeQMojo11.1.9_Exercise_Skipping_Jogging_Skipping.csv",
                      "~/HealthQ/Data/Skipping/20140815_0700_DirkBadenhorst_LifeQMojo11.2.1_Exercise_Skipping_Jogging_Skipping.csv",
                      "~/HealthQ/Data/Skipping/20140815_0700_DirkBadenhorst_LifeQMojo11.2.2_Exercise_Skipping_Jogging_Skipping.csv"),
                    "~/HealthQ/Data/Skipping/20140815_1200_VanZylVanVuuren_LifeQMojo11.2.2_Exercise_Skipping.csv",
                    "~/HealthQ/Data/Skipping/20140815_1200_VanZylVanVuuren_LifeQMojo11.21.9_Exercise_Skipping.csv",
                    "~/HealthQ/Data/Skipping/20140815_1530_RenierVanRooyen_LifeQMojo11.1.10_Exercise_Skipping (1).csv")

bioh_fpaths <- c("~/HealthQ/Data/Skipping/20140815_0700_DirkBadenhorst_Bioharness_Summary_Exercise_Skipping_Jogging_Skipping_2.csv",
                 "~/HealthQ/Data/Skipping/20140815_1200_VanZylVanVuuren_Bioharness_Summary_Exercise_Skipping.csv",
                 "~/HealthQ/Data/Skipping/20140815_1200_VanZylVanVuuren_Bioharness_Summary_Exercise_Skipping.txt",
                 "~/HealthQ/Data/Skipping/20140815_1530_RenierVanRooyen_Bioharness_Summary_Exercise_Skipping.txt")


# i <- 1 ; j <- 3 # dirk 
# i <- 2 ; j <- 1 # van zyl 1
# i <- 3 ; j <- 1 # van zyl 2
i <- 4 ; j <- 1 # renier

mojo_dat <- read.table(mojo_fpaths[[i]][j], sep=",", head=T)



#########################################################################################################


grn_ch <- mojo_dat[,3] - mojo_dat[,4]
nir_ch <- mojo_dat[,1] - mojo_dat[,2]
# acc_ch <- acc_res(mojo_dat[,5:7])
acc_ch <- 0.01*sqrt(rowSums(mojo_dat[,5:7]^2))

grn_ch <- removeAGCJumps(grn_ch, 500)
nir_ch <- removeAGCJumps(nir_ch, 500)

grn_ch <- as.numeric(diff(grn_ch, diff=1))
nir_ch <- as.numeric(diff(nir_ch, diff=1))
acc_ch <- as.numeric(diff(acc_ch, diff=1))

grn_ch <- ksmooth(1:length(grn_ch), grn_ch, "normal", band=6)$y
nir_ch <- ksmooth(1:length(nir_ch), nir_ch, "normal", band=6)$y
acc_ch <- ksmooth(1:length(acc_ch), acc_ch, "normal", band=6)$y

offset <- c(200, 200, 200, 200)
grn_ch <- grn_ch[offset[i]:length(grn_ch)]
nir_ch <- nir_ch[offset[i]:length(nir_ch)]
acc_ch <- acc_ch[offset[i]:length(acc_ch)]



w(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(grn_ch, windowSize=512, overlap=256, blankFactor=3, maxfreq=3.5, normal=1)




#################################################################################################################
#################################################################################################################
#################################################################################################################




if(i == 1){
  write_names_acc <- c("20140815_dirk_skipjogskip_1_acc.txt", "20140815_dirk_skipjogskip_2_acc.txt", "20140815_dirk_skipjogskip_3_acc.txt")
  write.table(cbind(grn_ch, acc_ch), file=paste0("~/HealthQ/C FILES/RLS_NS_project/data/preprocessed/", write_names_acc[j]),
              quote=F, sep=",", row.names=F, col.names=F)
  write_names_nir <- c("20140815_dirk_skipjogskip_1_nir.txt", "20140815_dirk_skipjogskip_2_nir.txt", "20140815_dirk_skipjogskip_3_nir.txt")
  write.table(cbind(grn_ch, nir_ch), file=paste0("~/HealthQ/C FILES/RLS_NS_project/data/preprocessed/", write_names_nir[j]),
              quote=F, sep=",", row.names=F, col.names=F)
}
if(i != 1){
  write_names_acc <- c("20140815_vz_skipping1_acc.txt", "20140815_vz_skipping2_acc.txt", "20140815_renier_skipping_acc.txt")
  write.table(cbind(grn_ch, acc_ch), file=paste0("~/HealthQ/C FILES/RLS_NS_project/data/preprocessed/", write_names_acc[i-1]),
              quote=F, sep=",", row.names=F, col.names=F)
  write_names_nir <- c("20140815_vz_skipping1_nir.txt", "20140815_vz_skipping2_nir.txt", "20140815_renier_skipping_nir.txt")
  write.table(cbind(grn_ch, nir_ch), file=paste0("~/HealthQ/C FILES/RLS_NS_project/data/preprocessed/", write_names_nir[i-1]),
              quote=F, sep=",", row.names=F, col.names=F)  
}


#################################################################################################################


p_out_sets <- c("20140815_dirk_skipjogskip_1", "20140815_dirk_skipjogskip_2", "20140815_dirk_skipjogskip_3",
                "20140815_vz_skipping1", "20140815_vz_skipping2", "20140815_renier_skipping")

for(j in p_out_sets){
  
  ppdat_acc <- read.table(paste0("~/HealthQ/C FILES/RLS_NS_project/data/preprocessed/", j, "_acc.txt"), sep=",")
  ppdat_nir <- read.table(paste0("~/HealthQ/C FILES/RLS_NS_project/data/preprocessed/", j, "_nir.txt"), sep=",")
  
  if(!all(ppdat_acc[,1] == ppdat_nir[,1]))
    stop("Something is wrong.")
  
  rls_acc <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out_", j, "_acc.txt"), sep=",")[,2]
  rls_nir <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out_", j, "_nir.txt"), sep=",")[,2]
  
  write.table(cbind(rls_acc, ppdat_nir[,2]), 
              file=paste0("~/HealthQ/C FILES/RLS_NS_project/data/preprocessed/", j, "_accnir.txt"),
              quote=F, sep=",", row.names=F, col.names=F)
  write.table(cbind(rls_nir, ppdat_acc[,2]), 
              file=paste0("~/HealthQ/C FILES/RLS_NS_project/data/preprocessed/", j, "_niracc.txt"),
              quote=F, sep=",", row.names=F, col.names=F)
  
  
}


#################################################################################################################
#################################################################################################################
#################################################################################################################

ppnames  <- paste0(rep(p_out_sets, each=4), c("_acc", "_nir", "_accnir", "_niracc"))
ppnames2 <- paste0("physcalc_out_", ppnames, ".txt")

hr_out_list <- NULL
for(i in 1:24){
  
  dat <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/",ppnames2[i]), sep=",")
  
  hr_out  <- dat[,1]
  rls_out <- dat[,2]
  
  
  w(14,6) ; par(mar=c(2,3,2,2))
  tiaan_specgram(rls_out, windowSize=512, overlap=256, blankFactor=3, maxfreq=3.5, normal=1,
                 line_overlay=hr_out/60, col_overlay_l="white")
  title(main=ppnames[i])
  savePlot(paste0("figures/", ppnames[i]), type="png")
  
  hr_out_list <- append(hr_out_list, list(hr_out))
}

hr_out_list <- lapply(hr_out_list, function(x) x[seq(1,length(x),50)])



offsets <- matrix(c(15, 20,
                    5,  40,
                    30, 20,
                    15, 20,
                    15, 20,
                    15,  20), nc=2, byrow=T)
ylims   <- matrix(c(rep(c(30, 210),3),
                    50, 180,
                    50, 180,
                    50, 200), nc=2, byrow=T)


hr_names <- c(paste0("dirk_skippingjogging", 1:3), "vanzyl_skipping1", "vanzyl_skipping2", "renier_skipping")

for(i in 1:6){

  if(i == 1 || i == 2 || i == 3)
    ib <- 1
  else
    ib <- i-2
    bioh_dat <- read.table(bioh_fpaths[ib], sep=",", head=T)[,2]

  w() ; par(mar=c(3,4,3,1))
  lplot(bioh_dat[-c(1:offsets[i,1])], lwd=4, col="grey", ylim=ylims[i,], ylab="HR", main=hr_names[i])
  lines(hr_out_list[[(i-1)*4+1]][-c(1:offsets[i,2])], lwd=3, col="blue")
  lines(hr_out_list[[(i-1)*4+2]][-c(1:offsets[i,2])], lwd=3, col="red")
  lines(hr_out_list[[(i-1)*4+3]][-c(1:offsets[i,2])], lwd=3, col="forestgreen")
  lines(hr_out_list[[(i-1)*4+4]][-c(1:offsets[i,2])], lwd=3, col="orange")
  legend("bottomright", lwd=c(3,2,2,2,2), col=c("grey", "blue", "red", "forestgreen", "orange"), 
         leg=c("Bioharness", "ACC", "NIR", "ACC + NIR", "NIR + ACC"), bg="white")
  savePlot(paste0("figures/HR_", hr_names[i]), "png")

}






library(signal)
aa2 <- filter(butter(2, c(0.5, 8)/25, type="pass"), aa[,2])
aa2 <- filter(butter(2, c(0.5, 8)/25, type="pass"), rev(aa2))


