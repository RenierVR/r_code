## http://www.cs.tut.fi/~tabus/course/ASP/LectureNew10.pdf

rls1 <- function(u, d, lambda, M, P0=diag(M), w0=rep(0,M))
{
  n <- length(u)
  del <- 100*var(u)
  P <- del*P0
  w <- w0
  fit <- numeric(n)
  klist <- vector("list", n)
  wlist <- vector("list", n)
  Plist <- vector("list", n)
  
  for(i in 1:n){
    if(i >= M){  
      uvec <- u[(i+1-M):i]
      k <- ((P %*% uvec)/lambda)/(1 + (as.numeric(t(uvec) %*% P %*% uvec))/lambda)
      klist[[i]] <- k
      fit[i] <- as.numeric(t(uvec) %*% w)
      alpha <- d[i] - fit[i]
      
      # update step
      w <- w + alpha*k
      P <- (P - k %*% t(uvec) %*% P)/lambda
      wlist[[i]] <- w
      Plist[[i]] <- P
    }
    else{
      fit[i] <- 0
      klist[[i]] <- rep(0,M)
      wlist[[i]] <- rep(0,M)
      Plist[[i]] <- matrix(0,M,M)
    }
  }
  
  return(list(fit=fit, klist=klist, wlist=wlist, Plist=Plist))
}



rls2 <- function(u, d, lambda, M, P0=diag(M), w0=rep(0,M))
{
  n <- length(u)
  del <- 100*var(u)
  P <- del*P0
  w <- w0
  fit <- numeric(n)
  klist <- vector("list", n)
  wlist <- vector("list", n)
  Plist <- vector("list", n)
  
  for(i in 1:n){
    if(i >= M){
      uvec <- u[(i+1-M):i]
      pivec <- t(uvec) %*% P
      gamma <- lambda + as.numeric(pivec %*% uvec)
      k <- t(pivec)/gamma
      klist[[i]] <- k
      fit[i] <- as.numeric(t(w) %*% uvec)
      alpha <- d[i] - fit[i]
      
      # update
      w <- as.numeric(w + alpha*k)
      Pd <- k %*% pivec
      P <- (P - Pd)/lambda
      wlist[[i]] <- w
      Plist[[i]] <- P
      
    }
    else{
      fit[i] <- 0
      klist[[i]] <- rep(0,M)
      wlist[[i]] <- rep(0,M)
      Plist[[i]] <- matrix(0,M,M)
    }
  }
  
  return(list(fit=fit, klist=klist, wlist=wlist, Plist=Plist))
}



acc_res <- function(mat, scaling=12)
  return(scaling*sqrt(rowSums(((mat - 2^15) / (2^16))^2)) )


####################################################################################
####################################################################################

wrist_dat <- read.table("~/HealthQ/Data/wrist/20140515_WristMotionAtDifferentPlacement/20140514_MojoBiomon1_Renier_WristAction_001.txt", head=T, sep=",")
grn_bs <- wrist_dat[,1] - wrist_dat[,3]
acc <- acc_res(wrist_dat[,5:7])


grn_bs_cent <- as.numeric(scale(grn_bs, scale=F))[8800:20000]
acc_cent <- (acc - 1)[8800:20000]

grn_rls1 <- rls1(acc_cent, grn_bs_cent, 1, 10)
grn_rls2 <- rls2(acc_cent, grn_bs_cent, 1, 10)


ff1 <- grn_rls1[[1]] ; ff2 <- grn_rls2[[1]]
win(14,7)
plot(ff1[8000:11200], type="l", lwd=2, col="limegreen")
lines(ff2[8000:11200], col="purple")

for(i in 1:200){
  print(c(i, as.numeric(round(ff1[[i]],4) == round(ff2[[i]],4))))
}


####################################################################################
####################################################################################



rlsmplot <- function(winsize, overlap, start=1, y1, y2, y3)
{
  windows(12,5) ;  par(ask=T, mfrow=c(2,1), mar=c(4,2,1,1))
  j = 0
  while(TRUE){
    j = j+1
    plotind <- (overlap*(j-1)+start):(start-1+winsize + overlap*(j-1))
    if(max(plotind) > length(y1))
      break
    else{
      plot(plotind, y1[plotind], type="l", lwd=2, col="limegreen", ann=F)
      par(new=T)
      plot(plotind, y2[plotind], type="l", lwd=2, col="blue", ann=F, axes=F)
      plot(plotind, y3[plotind], type="l", lwd=2, col="purple", ann=F, ylim=c(-1,4))
    }
  }
}



rlsmplot(1000,500,y1=grn_bs_cent,y2=grn_diff, y3=acc_cent)
