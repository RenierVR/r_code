source("../tiaan_specgram.r")
library(colorRamps)

w   <- function(...) windows(...)
len <- function(...) length(...)


grn_ch <- ksmooth(1:length(grn_ch), grn_ch, "normal", band=8)$y
acc_ch <- ksmooth(1:length(acc_ch), acc_ch, "normal", band=8)$y

grn_ch <- as.numeric(diff(grn_ch, diff=1))
acc_ch <- as.numeric(diff(acc_ch, diff=1))


ccf(grn_ch[120000:200000], acc_ch[120000:200000])
ccf(grn_ch[1:10000], acc_ch[1:10000])

##################################################################################
##################################################################################


# write.table(cbind(grn_ch, acc_ch), file="njrweek_renier1.txt", quote=F, row.names=F, col.names=F, sep=", ")

w(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(grn_ch, windowSize=512, overlap=256, normal=1, 
               output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T,
               col=blue2green2red, Fs=50)

w(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(acc_ch, windowSize=512, overlap=256, normal=1, 
               output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T,
               col=blue2green2red, Fs=50)



# acc_filt <- signal::filter(butter(6, c(0.75,1.5)/25, type="pass"), acc_ch)
acc_filt <- signal::filter(butter(10, 1.5/25, type="low"), acc_ch)
grn_filt <- signal::filter(butter(6, c(0.5,4)/25, type="pass"), grn_ch)

acc_filt <- acc_filt-signal::filter(butter(8, 0.5/25, type="low"), acc_filt)

w(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(grn_filt, windowSize=512, overlap=256, normal=1, 
               output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T,
               col=blue2green2red, Fs=50)
w(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(diff(acc_filt), windowSize=512, overlap=256, normal=1, 
               output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T,
               col=blue2green2red, Fs=50)



write.table(cbind(grn_ch, acc_filt), file="~/HealthQ/C FILES/RLS_NS_project/data/VanZylTest/njrweek_renier1_filt.txt", 
            quote=F, row.names=F, col.names=F, sep=", ")





##################################################################################
##################################################################################
##################################################################################



cout <- read.table(paste0("~/HealthQ/C FILES/RLS_NS_project/out_fitted_", njr_name), sep=",")
# cout <- read.table("~/HealthQ/C FILES/RLS_NS_project/out_fitted.txt", sep=",")[,1]
cout <- read.table(paste0("~/HealthQ/C FILES/RLS_NS_project/out_fitted_njrweek_renier1_filt.txt"), sep=",")


cout_fitted <- cout[,1]
w(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(cout_fitted, windowSize=512, overlap=256, normal=1, 
               output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T,
               col=blue2green2red, Fs=50)

cout_noise <- cout[,2]
cout_noise <- cout_noise*cos(1.23 * 2*pi*(1:len(cout_noise))/50)

cout_noise <- signal::filter(butter(10, 2.5/25, type="low"), cout_noise)

w(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(cout_noise, windowSize=512, overlap=256, normal=1, 
               output=F, blankFactor=3, maxfreq=4, logTrans=T, graph=T,
               col=blue2green2red, Fs=50)

w(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(grn_ch[-c(1:17)]-cout_noise, windowSize=512, overlap=256, normal=1, 
               output=F, blankFactor=3, maxfreq=4, logTrans=T, graph=T,
               col=blue2green2red, Fs=50)


write.table(cbind(grn_ch[-c(1:17)], cout_noise), file="~/HealthQ/C FILES/RLS_NS_project/data/VanZylTest/njrweek_renier1_filt.txt", 
            quote=F, row.names=F, col.names=F, sep=", ")




##################################################################################
##################################################################################
##################################################################################



require(signal)

# w(14,7) ; plot(acc_ch[1001:1100], type="l", col="limegreen")
# points(acc_ch[1001:1100], pch=19, cex=0.75, col="forestgreen")
# 
# xx <- seq(1,100,0.5)
# z <- interp1(x=1:100, y=acc_ch[1001:1100], xi=xx, method="linear")
# w(14,7) ; plot(xx, z, type="l", col="limegreen")
# points(xx, z, pch=19, cex=0.75, col=c("forestgreen", "maroon"))
# 
# z <- interp1(x=1:100, y=acc_ch[1001:1100], xi=xx, method="cubic")
# w(14,7) ; plot(xx, z, type="l", col="limegreen")
# points(xx, z, pch=19, cex=0.75, col=c("forestgreen", "maroon"))




