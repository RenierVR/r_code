

# fpath <- "~/HealthQ/C FILES/RLS_NS_project/data/preprocessed/vz_houopkontwees_mf.txt"
# fname <- substr(fpath, rev(gregexpr("/", fpath)[[1]])[1]+1, nchar(fpath))
# 
# hr_out  <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out", fname), sep=",")[,1]
# 
# dat <- read.table(fpath, sep=",")
# grn_ch <- dat[,1]

###################################################################################################

dat_in  <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/input_testing.txt", sep=",")
hr_out  <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out.txt",sep=",")[,1]
rls_out <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/rls_output.txt")[,1]
rls_in  <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/rls_input.txt", sep=",")
rls_rst <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/rls_reset.txt")[,1]


grn_ch <- dat_in[,1] - dat_in[,2]
grn_ch <- removeAGCJumps(grn_ch, 500)
grn_ch <- as.numeric(diff(grn_ch, diff=1))
grn_ch <- ksmooth(1:length(grn_ch), grn_ch, "normal", band=6)$y
w(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(grn_ch, windowSize=512, overlap=256, blankFactor=3, maxfreq=3.5, normal=1)


w(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(grn_ch, windowSize=512, overlap=256, blankFactor=3, maxfreq=3.5, normal=1,
               line_overlay=hr_out[(len(hr_out) - len(grn_ch) + 1):len(hr_out)]/60)



# w() ; par(mar=c(2,3,2,2))
# lplot(hr_out[seq(1,length(hr_out),50)], lwd=2)
# lines((1:length(hr_out))/length(hr_out), hr_out/60, lwd=3, col="white")


acc_ch <- acc_res(dat_in[,3:5])
w() ; lplot(acc_ch, xlim=c(75000, 90000))
abline(v=rls_rst, lwd=3, col="red")



###################################################################################################
###################################################################################################


# rls_in <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/rls_input.txt", sep=",")

x <- acc_ch #x <- rls_in[,2]
sd_win_len <- 50
sd_run <- rep(0,sd_win_len-1)
for(i_sd in (sd_win_len):length(x))
  sd_run <- c(sd_run, sd(x[(i_sd-sd_win_len+1):i_sd]))

w() ; lplot(sd_run) ; abline(h=1, col="red")

aa <- nir_ch
aa[sd_run < 1] <- 0
nir_ch <- aa






###################################################################################################
###################################################################################################

nokia_sets <- c("output14-parsed", "output15-parsed", "output9-parsed", "output16-parsed", "output11-parsed",
                "output6-parsed", "output7-parsed", "output13-parsed", "output8-parsed", "output12-parsed",
                "output17-parsed", "output10-parsed", "output18-parsed", "output21-parsed", "output0-parsed", "output3-parsed")
                

polar_sets <- c("output0-parsed", "output1-parsed", "output2-parsed", "output3-parsed")


for(i in 1:length(polar_sets)){

  dat_set <- paste0("~/HealthQ/Data/polar_nicol/", polar_sets[i], ".csv")  
  dat_in <- read.table(dat_set, sep=",", head=T)
  
  grn_ch <- dat_in[,3] - dat_in[,4]
  grn_ch <- removeAGCJumps(grn_ch, 500)
  grn_ch <- as.numeric(diff(grn_ch, diff=1))
  grn_ch <- ksmooth(1:length(grn_ch), grn_ch, "normal", band=6)$y
  
  acc_ch <- acc_res(dat_in[,5:7])
  acc_ch <- as.numeric(diff(acc_ch, diff=1))
  acc_ch <- ksmooth(1:length(acc_ch), acc_ch, "normal", band=6)$y
  w(14,6) ; par(mar=c(2,3,2,2))
  tiaan_specgram(acc_ch, windowSize=512, overlap=256, blankFactor=3, maxfreq=3.5, normal=1)
  title(main = paste(polar_sets[i], "resultant acceleration"))
#   savePlot(paste0("../../Data/nokia/", nokia_sets[i], "_acc.png"), type="png") 
  
  hr_out <- dat_in[,10]

  w(14,6) ; par(mar=c(2,3,2,2))
  tiaan_specgram(grn_ch, windowSize=512, overlap=256, blankFactor=3, maxfreq=3.5, normal=1,
                 line_overlay=hr_out[-1]/60)
  title(main = paste(polar_sets[i], "green (HR overlayed)"))
#   savePlot(paste0("../../Data/nokia/", nokia_sets[i], "_green+HR.png"), type="png")
  
}
  



###################################################################################################
###################################################################################################
###################################################################################################
###################################################################################################


ti_sets <- c("data_TI_subject1.txt", "data_TI_subject2.txt", "data_TI_subject3.txt", "data_TI_subject4.txt", 
             "data_TI_Subject5_Karthik_run1.txt", "data_TI_Subject5_Karthik_run2.txt",
             "data_TI_Karthik_JRC_300pm.txt", "data_TI_Karthik_Osram_347pm.txt")

ti_names <- c("Subject 1", "Subject 2", "Subject 3", "Subject 4", 
              "Subject 5: Karthik run 1", "Subject 5: Karthik run 2",
              "Karthik JRC 300pm", "Karthik Osram 347pm")

for(i in 1:len(ti_sets)){

  ti_set <- ti_sets[i]
  ti_nm  <- substr(ti_set, 1, nchar(ti_set)-4)

  dat_in  <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/data/TI_data/", ti_set), sep=",")
  hr_out  <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out_", ti_set), sep=",")[,1]

  polar <- dat_in[,7]
  polar[is.na(polar)] <- 0
  
  w() ; par(mar=c(4,4,2,2))
  hrs <- cbind(polar, dat_in[,6], hr_out)
  xx <- 1:nrow(hrs)/50
  lplot(xx, hrs[,1], ylim=range(hrs), lwd=4, col="grey", xlab="time (s)", ylab="bpm", main=ti_names[i])
  lines(xx, hrs[,2], lwd=3, col="red")
  lines(xx, hrs[,3], lwd=3, col="blue")
  legend("topleft", bty="n", lwd=rep(3,3, 3), col=c("grey", "red", "blue"), leg=c("Polar", "Other solution", "Our solution"))
  
#  savePlot(paste0("./TI_results/", ti_nm, ".png"), "png")
#  graphics.off()
  
  
#   grn_ch <- dat_in[,1] - dat_in[,2]
#   grn_ch <- removeAGCJumps(grn_ch, 500)
#   grn_ch <- as.numeric(diff(grn_ch, diff=1))
#   grn_ch <- ksmooth(1:length(grn_ch), grn_ch, "normal", band=6)$y
#   
#   w(14,6) ; par(mar=c(2,3,2,2))
#   tiaan_specgram(grn_ch, windowSize=512, overlap=256, blankFactor=3, maxfreq=3.5, normal=1)
}




