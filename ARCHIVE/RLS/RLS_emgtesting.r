source("../tiaan_specgram.r")
source("../temporalCorr.r")
source("./RLS_wiki.r")

acc_res <- function(mat, scaling=12)
  return(scaling*sqrt(rowSums(((mat - 2^15) / (2^16))^2)) )

logzero <- function(x, b=2)
  return( sign(x)*log(abs(x)+1, base=b) )


#########################################################################################################
#########################################################################################################

mojo_fpaths <- c("~/HealthQ/Data/EMG/20140813/Dataset 13 August 2014 Shannagh/data_2014-08-13 13-44-120-parsed.csv",
                 "~/HealthQ/Data/EMG/20140813/Dataset 13 August 2014 Theo/data_2014-08-13 13-44-12_t0-parsed.csv",
                 "~/HealthQ/Data/EMG/20140813/Dataset 13 August 2014 Theo 6 electrodes/Mojo data_2014-08-13 17-43-190-parsed.csv")

bioh_fpaths <- c("~/HealthQ/Data/EMG/20140813/Dataset 13 August 2014 Shannagh/2014_08_13-13_52_18_Summary.csv",
                 "~/HealthQ/Data/EMG/20140813/Dataset 13 August 2014 Theo/2014_08_13-14_44_53_Summary.csv",
                 "~/HealthQ/Data/EMG/20140813/Dataset 13 August 2014 Theo 6 electrodes/Bioharness 2014_08_13-17_41_46_Summary.csv")

emg_fpaths <- c("~/HealthQ/Data/EMG/20140813/Dataset 13 August 2014 Shannagh/Device_3_Volts.txt",
                "~/HealthQ/Data/EMG/20140813/Dataset 13 August 2014 Theo/ecg.csv",
                "~/HealthQ/Data/EMG/20140813/Dataset 13 August 2014 Theo 6 electrodes/EMG.csv")

# i <- 1 # shannagh
# i <- 2 # theo
i <- 3 ; j <- 8 # theo 6 electrodes

mojo_dat <- read.table(mojo_fpaths[i], sep=",", head=T)
bioh_dat <- read.table(bioh_fpaths[i], sep=",", head=T)[,2]
emg_dat  <- read.table(emg_fpaths[i], sep=",")


if((i == 1) || (i == 2))
  emg_ch <- emg_dat[,2]
if(i == 3)
  emg_ch <- emg_dat[,j]
emg_ch <- emg_ch[seq(1, length(emg_ch), 10)]

# emg_ch_mod <- sqrt(rowSums(emg_dat[seq(1, nrow(emg_dat), 10), c(3,4,8)]^2))
# emg_ch <- emg_ch_mod

grn_ch <- mojo_dat[,3] - mojo_dat[,4]
nir_ch <- mojo_dat[,1] - mojo_dat[,2]
# acc_ch <- acc_res(mojo_dat[,5:7])
acc_ch <- 0.01*sqrt(rowSums(mojo_dat[,5:7]^2))

grn_ch <- removeAGCJumps(grn_ch, 500)
nir_ch <- removeAGCJumps(nir_ch, 500)

grn_ch <- as.numeric(diff(grn_ch, diff=1))
nir_ch <- as.numeric(diff(nir_ch, diff=1))
acc_ch <- as.numeric(diff(acc_ch, diff=1))
emg_ch <- as.numeric(diff(emg_ch, diff=1))

grn_ch <- ksmooth(1:length(grn_ch), grn_ch, "normal", band=6)$y
nir_ch <- ksmooth(1:length(nir_ch), nir_ch, "normal", band=6)$y
acc_ch <- ksmooth(1:length(acc_ch), acc_ch, "normal", band=6)$y
emg_ch <- ksmooth(1:length(emg_ch), emg_ch, "normal", band=6)$y

offset <- c(1030, 170, 460)
grn_ch <- grn_ch[offset[i]:length(emg_ch)]
nir_ch <- nir_ch[offset[i]:length(emg_ch)]
acc_ch <- acc_ch[offset[i]:length(emg_ch)]
emg_ch <- emg_ch[100:(length(emg_ch) - offset[i]+100)]


## extra EMG preprocessing
emg_ch2 <- 1e6*emg_ch


tc_out <- temporalCorr(grn_ch, acc_ch, emg_ch2, nir_ch, yn=c("ACC", "EMG", "NIR"), winsize=2048, overlap=128, bw=500, method="ccfmax")


# pind <- 29750:30250
# w() ; par(mfrow=c(2,1), mar=c(2,3,2,2)) ; lplot(pind, acc_ch[pind], lwd=2) ; lplot(pind, emg_ch2[pind], lwd=2, col="red")



w(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(grn_ch, windowSize=512, overlap=256, blankFactor=3, maxfreq=3.5, normal=1)
title(main = "GREEN LED")

w(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(emg_ch2, windowSize=512, overlap=256, blankFactor=3, maxfreq=3.5, normal=1)
title(main = "EMG signal")

w(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(acc_ch, windowSize=512, overlap=256, blankFactor=3, maxfreq=3.5, normal=1)


#########################################################################################################
#########################################################################################################


write_names_acc <- c("20140813_acc_shannagh.txt", "20140813_acc_theo.txt", "20140813_acc_theo6.txt")
write.table(cbind(grn_ch, acc_ch), file=paste0("~/HealthQ/C FILES/RLS_NS_project/data/preprocessed/", write_names_acc[i]),
            quote=F, sep=",", row.names=F, col.names=F)


write_names_nir <- c("20140813_nir_shannagh.txt", "20140813_nir_theo.txt", "20140813_nir_theo6.txt")
write.table(cbind(grn_ch, nir_ch), file=paste0("~/HealthQ/C FILES/RLS_NS_project/data/preprocessed/", write_names_nir[i]),
            quote=F, sep=",", row.names=F, col.names=F)


write_names_emg <- c("20140813_emg_shannagh.txt", "20140813_emg_theo.txt", paste0("20140813_emg", j, "_theo6.txt"))
write.table(cbind(grn_ch, emg_ch2), file=paste0("~/HealthQ/C FILES/RLS_NS_project/data/preprocessed/", write_names_emg[i]),
            quote=F, sep=",", row.names=F, col.names=F)







#########################################################################################################
#########################################################################################################


rls_out <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/rls_output.txt")[,1]
hr_out  <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out.txt")[,1]

hr_out <- hr_out[seq(1,length(hr_out),50)]


hr_out_acc <- hr_out
hr_out_nir <- hr_out
hr_out_emg <- hr_out

# w(14,6) ; par(mar=c(2,3,2,2))
# tiaan_specgram(rls_out, windowSize=512, overlap=256, blankFactor=3, maxfreq=3.5, normal=1)
# title(main = "RLS(15, 0.99) - EMG reference")


offsets <- matrix(c(25, 15,
                    10, 15,
                    110, 15), nc=2, byrow=T)
ylims   <- matrix(c(60, 130,
                    50, 150,
                    50, 150), nc=2, byrow=T)
w() ; par(mar=c(2,4,2,2))
lplot(bioh_dat[-c(1:offsets[i,1])], lwd=3, col="grey", ylim=ylims[i,], ylab="bpm",
      main="HR comparisons: Theo EMG data - 6 electrodes (channel 8)")
lines(hr_out_acc[-c(1:offsets[i,2])], lwd=3, col="blue")
lines(hr_out_nir[-c(1:offsets[i,2])], lwd=3, col="orange")
lines(hr_out_emg[-c(1:offsets[i,2])], lwd=3, col="red")
legend("topright", lwd=c(3,2,2), col=c("grey", "blue", "orange", "red"), leg=c("Bioharness", "ACC ref", "NIR ref", "EMG ref"), bg="white")




#########################################################################################################
#########################################################################################################


bioh_shan  <- read.table("~/HealthQ/Data/EMG/20140813/Dataset 13 August 2014 Shannagh/2014_08_13-13_52_18_Summary.csv", sep=",", head=T)[,2]
bioh_theo  <- read.table("~/HealthQ/Data/EMG/20140813/Dataset 13 August 2014 Theo/2014_08_13-14_44_53_Summary.csv", sep=",", head=T)[,2]
bioh_theo6 <- read.table("~/HealthQ/Data/EMG/20140813/Dataset 13 August 2014 Theo 6 electrodes/Bioharness 2014_08_13-17_41_46_Summary.csv", sep=",", head=T)[,2]


hr_acc_shan   <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out_20140813_acc_shannagh.txt", sep=",")[,1]
hr_emg_shan   <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out_20140813_emg_shannagh.txt", sep=",")[,1]
hr_acc_theo   <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out_20140813_acc_theo.txt", sep=",")[,1]
hr_emg_theo   <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out_20140813_emg_theo.txt", sep=",")[,1]
hr_acc_theo6  <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out_20140813_acc_theo6.txt", sep=",")[,1]
hr_emg2_theo6  <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out_20140813_emg2_theo6.txt", sep=",")[,1]
hr_emg3_theo6  <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out_20140813_emg3_theo6.txt", sep=",")[,1]
hr_emg4_theo6  <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out_20140813_emg4_theo6.txt", sep=",")[,1]
hr_emg8_theo6  <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out_20140813_emg8_theo6.txt", sep=",")[,1]

hr_acc_shan   <- hr_acc_shan[seq(1,length(hr_acc_shan),50)]
hr_emg_shan   <- hr_emg_shan[seq(1,length(hr_emg_shan),50)]
hr_acc_theo   <- hr_acc_theo[seq(1,length(hr_acc_theo),50)]
hr_emg_theo   <- hr_emg_theo[seq(1,length(hr_emg_theo),50)]
hr_acc_theo6  <- hr_acc_theo6[seq(1,length(hr_acc_theo6),50)]
hr_emg2_theo6 <- hr_emg2_theo6[seq(1,length(hr_emg2_theo6),50)]
hr_emg3_theo6 <- hr_emg3_theo6[seq(1,length(hr_emg3_theo6),50)]
hr_emg4_theo6 <- hr_emg4_theo6[seq(1,length(hr_emg4_theo6),50)]
hr_emg8_theo6 <- hr_emg8_theo6[seq(1,length(hr_emg8_theo6),50)]



maint <- "RLS (30, 0.96)"
w(20,14) ; par(mfrow=c(3,1), mar=c(3,4,1,2))
lplot(bioh_shan[-c(1:offsets[1,1])], lwd=3, col="grey", ylim=ylims[1,], ylab="HR")
lines(hr_acc_shan[-c(1:offsets[1,2])], lwd=2, col="blue")
lines(hr_emg_shan[-c(1:offsets[1,2])], lwd=2, col="red")
legend("topright", lwd=c(3,2,2), col=c("grey", "blue", "red"), leg=c("Bioharness", "ACC ref", "EMG ref"), bg="white")
legend("top", leg=maint, cex=1.25)

lplot(bioh_theo[-c(1:offsets[2,1])], lwd=3, col="grey", ylim=ylims[2,], ylab="HR")
lines(hr_acc_theo[-c(1:offsets[2,2])], lwd=2, col="blue")
lines(hr_emg_theo[-c(1:offsets[2,2])], lwd=2, col="red")
legend("topright", lwd=c(3,2,2), col=c("grey", "blue", "red"), leg=c("Bioharness", "ACC ref", "EMG ref"), bg="white")

lplot(bioh_theo6[-c(1:offsets[3,1])], lwd=3, col="grey", ylim=ylims[3,], ylab="HR")
lines(hr_acc_theo6[-c(1:offsets[3,2])], lwd=2, col="blue")
lines(hr_emg2_theo6[-c(1:offsets[3,2])], lwd=2, col="red")
lines(hr_emg3_theo6[-c(1:offsets[3,2])], lwd=2, col="orange")
lines(hr_emg4_theo6[-c(1:offsets[3,2])], lwd=2, col="gold")
lines(hr_emg8_theo6[-c(1:offsets[3,2])], lwd=2, col="maroon")
legend("topright", lwd=c(3,2,2,2,2,2), col=c("grey", "blue", "red", "orange", "gold", "maroon"), 
       leg=c("Bioharness", "ACC ref", "EMG(ch2) ref", "EMG(ch3) ref", "EMG(ch4) ref", "EMG(ch8) ref"), bg="white")








