source("../tiaan_specgram.r")

bioh_shan  <- read.table("~/HealthQ/Data/EMG/20140813/Dataset 13 August 2014 Shannagh/2014_08_13-13_52_18_Summary.csv", sep=",", head=T)[,2]
bioh_theo  <- read.table("~/HealthQ/Data/EMG/20140813/Dataset 13 August 2014 Theo/2014_08_13-14_44_53_Summary.csv", sep=",", head=T)[,2]
bioh_theo6 <- read.table("~/HealthQ/Data/EMG/20140813/Dataset 13 August 2014 Theo 6 electrodes/Bioharness 2014_08_13-17_41_46_Summary.csv", sep=",", head=T)[,2]



ppnames <- c("physcalc_out_20140813_acc_shannagh.txt", "physcalc_out_20140813_nir_shannagh.txt",
             "physcalc_out_20140813_acc_theo.txt", "physcalc_out_20140813_nir_theo.txt",
             "physcalc_out_20140813_acc_theo6.txt", "physcalc_out_20140813_nir_theo6.txt")

ppnames <- c(ppnames, "physcalc_out_20140813_accnir_shannagh.txt", "physcalc_out_20140813_niracc_shannagh.txt",
             "physcalc_out_20140813_accnir_theo.txt", "physcalc_out_20140813_niracc_theo.txt",
             "physcalc_out_20140813_accnir_theo6.txt", "physcalc_out_20140813_niracc_theo6.txt")

savenames <- c("shan_1_acc", "shan_2_nir", "theo_1_acc", "theo_2_nir", "theo6_1_acc", "theo6_2_nir",
               "shan_3_accnir", "shan_4_niracc", "theo_3_accnir", "theo_4_niracc", "theo6_3_accnir", "theo6_4_niracc")

i <- 6




####################################l#############################################################################
#################################################################################################################
#################################################################################################################




hr_out_list <- NULL
for(i in 1:12){

  dat <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/",ppnames[i]), sep=",")

  hr_out  <- dat[,1]
  rls_out <- dat[,2]


  w(14,6) ; par(mar=c(2,3,2,2))
  tiaan_specgram(rls_out, windowSize=512, overlap=256, blankFactor=3, maxfreq=3.5, normal=1,
                 line_overlay=hr_out/60, col_overlay_l="white")
  title(main=ppnames[i])
  savePlot(paste0("figures/", savenames[i]), type="png")
  
  hr_out_list <- append(hr_out_list, list(hr_out))
}

hr_out_list <- lapply(hr_out_list, function(x) x[seq(1,length(x),50)])




offsets <- matrix(c(25, 15,
                    10, 15,
                    110, 15), nc=2, byrow=T)
ylims   <- matrix(c(60, 130,
                    50, 150,
                    50, 150), nc=2, byrow=T)


w() ; par(mar=c(3,4,3,1))
lplot(bioh_shan[-c(1:offsets[1,1])], lwd=4, col="grey", ylim=ylims[1,], ylab="HR", main="Shannagh")
lines(hr_out_list[[1]][-c(1:offsets[1,2])], lwd=3, col="blue")
lines(hr_out_list[[2]][-c(1:offsets[1,2])], lwd=3, col="red")
lines(hr_out_list[[7]][-c(1:offsets[1,2])], lwd=3, col="forestgreen")
lines(hr_out_list[[8]][-c(1:offsets[1,2])], lwd=3, col="limegreen")
legend("topright", lwd=c(3,2,2,2,2), col=c("grey", "blue", "red", "forestgreen", "limegreen"), 
       leg=c("Bioharness", "ACC", "NIR", "ACC + NIR", "NIR + ACC"), bg="white")
savePlot("figures/shan_0_HR", "png")


w() ; par(mar=c(3,4,3,1))
lplot(bioh_theo[-c(1:offsets[2,1])], lwd=4, col="grey", ylim=ylims[2,], ylab="HR", main="Theo")
lines(hr_out_list[[3]][-c(1:offsets[2,2])], lwd=3, col="blue")
lines(hr_out_list[[4]][-c(1:offsets[2,2])], lwd=3, col="red")
lines(hr_out_list[[9]][-c(1:offsets[2,2])], lwd=3, col="forestgreen")
lines(hr_out_list[[10]][-c(1:offsets[2,2])], lwd=3, col="limegreen")
legend("topright", lwd=c(3,2,2,2,2), col=c("grey", "blue", "red", "forestgreen", "limegreen"), 
       leg=c("Bioharness", "ACC", "NIR", "ACC + NIR", "NIR + ACC"), bg="white")
savePlot("figures/theo_0_HR", "png")


w() ; par(mar=c(3,4,3,1))
lplot(bioh_theo6[-c(1:offsets[3,1])], lwd=4, col="grey", ylim=ylims[3,], ylab="HR", main="Theo6")
lines(hr_out_list[[5]][-c(1:offsets[3,2])], lwd=3, col="blue")
lines(hr_out_list[[6]][-c(1:offsets[3,2])], lwd=3, col="red")
lines(hr_out_list[[11]][-c(1:offsets[3,2])], lwd=3, col="forestgreen")
lines(hr_out_list[[12]][-c(1:offsets[3,2])], lwd=3, col="limegreen")
legend("topright", lwd=c(3,2,2,2,2), col=c("grey", "blue", "red", "forestgreen", "limegreen"), 
       leg=c("Bioharness", "ACC", "NIR", "ACC + NIR", "NIR + ACC"), bg="white")
savePlot("figures/theo6_0_HR", "png")





#################################################################################################################
#################################################################################################################
#################################################################################################################



for(j in c("shannagh", "theo", "theo6")){

  ppdat_acc <- read.table(paste0("~/HealthQ/C FILES/RLS_NS_project/data/preprocessed/20140813_acc_", j,".txt"), sep=",")
  ppdat_nir <- read.table(paste0("~/HealthQ/C FILES/RLS_NS_project/data/preprocessed/20140813_nir_", j,".txt"), sep=",")
  
  if(!all(ppdat_acc[,1] == ppdat_nir[,1]))
    stop("Something is wrong.")

  rls_acc <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out_20140813_acc_", j,".txt"), sep=",")[,2]
  rls_nir <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out_20140813_nir_", j,".txt"), sep=",")[,2]
  
  write.table(cbind(rls_acc, ppdat_nir[,2]), 
              file=paste0("~/HealthQ/C FILES/RLS_NS_project/data/preprocessed/20140813_accnir_", j,".txt"),
              quote=F, sep=",", row.names=F, col.names=F)
  write.table(cbind(rls_nir, ppdat_acc[,2]), 
              file=paste0("~/HealthQ/C FILES/RLS_NS_project/data/preprocessed/20140813_niracc_", j,".txt"),
              quote=F, sep=",", row.names=F, col.names=F)
  
  
}

