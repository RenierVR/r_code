source("./RLS_wiki.r")
source("../tiaan_specgram.r")
source("../zPlot.r")


acc_res <- function(mat, scaling=12)
  return(scaling*sqrt(rowSums(((mat - 2^15) / (2^16))^2)) )



njr_names <- c("20140512_2131_MojoNJR1_Bluetooth_RenierVanRooyen_001",
               "20140513_1520_MojoNJR1_Bluetooth_PhillipLotz_001",
               "20140513_2234_MojoNJR1_Bluetooth_DirkBadenhorst_002",
               "20140514_0958_MojoNJR1_GregorRohrig_001",
               "20140514_1434_MojoNJR1_Bluetooth_CobusHavenga_001",
               "20140514_1624_MojoNJR1_Bluetooth_AdamVanHeerden_001",
               "20140514_1826_MojoNJR1_Bluetooth_JohanVanDerMerwe_004",
               "20140515_1327_MojoNJR1_Bluetooth_ChristiaanHugo_002",
               "20140515_1525_MojoNJR1_Bluetooth_JohanJansenVanRensburg_003",
               "20140515_1722_MojoNJR1_Bluetooth_GalenWright_005",
               "20140516_0724_ModC14_UpperArm_RoberHenning_001",
               "20140516_0725_MojoNJR3_NAND_Wrist_RobertHenning_001",
               "20140516_0918_ModC14_UpperArm_FrancoisDuToit_002",
               "20140516_0918_MojoNJR3_NAND_Wrist_FrancoisDuToit_002",
               "20140516_1322_ModC14_UpperArm_WimConradie_003",
               "20140516_1814_ModC14_UpperArm_CarelVanWyk_004",
               "20140516_1814_MojoNJR3_NAND_Wrist_CarelVanWyk_004",
               "20140517_1642_ModC14_RenierVanRooyen_001",
               "20140517_1642_MojoNJR3_NAND_RenierVanRooyen_001")
njr_names <- paste0(njr_names, ".txt")


# multiplicative <- FALSE
for(i in 1:length(njr_names)){

  cat("dataset", i, "out of", length(njr_names), "...\n")
  njr_name <- njr_names[i]
  dat <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/data/NJRTestWeek/", njr_name), sep=",")

  grn_ch <- dat[,1] - dat[,2]
  acc_ch <- acc_res(dat[,3:5])
  
  acx_ch <- dat[,3]
  acy_ch <- dat[,4]
  acz_ch <- dat[,5]
  
  grn_ch <- removeAGCJumps(grn_ch,500)

#   if(multiplicative){
#     grn_ch[grn_ch <= 1] <- 1
#     grn_ch <- log(grn_ch, 2)[-(1:100)]
#     acc_ch[acc_ch <= 1] <- 1
#     acc_ch <- log(acc_ch, 2)[-(1:100)]
#   }


  grn_ch <- as.numeric(diff(grn_ch, diff=1))
  acc_ch <- as.numeric(diff(acc_ch, diff=1))
  acx_ch <- as.numeric(diff(acx_ch, diff=1))
  acy_ch <- as.numeric(diff(acy_ch, diff=1))
  acz_ch <- as.numeric(diff(acz_ch, diff=1))

  grn_ch <- ksmooth(1:length(grn_ch), grn_ch, "normal", band=6)$y
  acc_ch <- ksmooth(1:length(acc_ch), acc_ch, "normal", band=6)$y
  acx_ch <- ksmooth(1:length(acx_ch), acx_ch, "normal", band=6)$y
  acy_ch <- ksmooth(1:length(acy_ch), acy_ch, "normal", band=6)$y
  acz_ch <- ksmooth(1:length(acz_ch), acz_ch, "normal", band=6)$y


  ###############################################################################
  
  if(njr_dat_choice < 10)
    prefix <- paste0("0", njr_dat_choice)
  if(njr_dat_choice >= 10)
    prefix <- as.character(njr_dat_choice)

  windows(14,6) ; par(mar=c(2,3,2,2))
  tiaan_specgram(grn_ch, windowSize=512, overlap=256, normal=1, 
                 output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T)
                 #col=blue2green2red)
  title(main=paste(njr_name, "| ORIGINAL"))
  savePlot(file=paste0("NJRC_results/", prefix, "_green"), type="png")
  graphics.off()
  cat("spectrogram of original signal complete\n")

#   rls_out1 <- grn_ch - rls_wiki(acc_ch, grn_ch, 15, 0.99, 1000)
#   rls_out2 <- grn_ch - rls_wiki(acc_ch, grn_ch, 20, 0.95, 1000)

  rls_cout1 <- read.table(paste0("~/HealthQ/C FILES/RLS_NS_project/out_fitted_", njr_name), sep=",")[,1]
#   rls_cout2 <- read.table(paste0("~/HealthQ/C FILES/RLS_NS_project/out_fitted_", njr_name), sep=",")[,1]

  windows(14,6) ; par(mar=c(2,3,2,2))
  tiaan_specgram(rls_cout1, windowSize=512, overlap=256, normal=1, 
                 output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T)
  title(main=paste(njr_name, "| RLS - 15"))
  savePlot(file=paste0("NJRC_results/", prefix, "_rls_1_order15"), type="png")
  graphics.off()
  cat("spectrogram of RLS order 15 complete\n")

  windows(14,6) ; par(mar=c(2,3,2,2))
  tiaan_specgram(rls_cout2, windowSize=512, overlap=256, normal=1, 
                 output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T,
                 col=blue2green2red)
  title(main=paste(njr_name, "| RLS - 40"))
  savePlot(file=paste0("NJRC_results/", prefix, "_rls_2_order5"), type="png")
  graphics.off()
  cat("spectrogram of RLS order 5 complete\n\n")
}




for(i in c(1,5,8,9,10)){
  
  njr_name <- njr_names[i]
  
  rls_cout1 <- read.table(paste0("~/HealthQ/C FILES/RLS_NS_project/out_fitted_", njr_name), sep=",")[,1]
  
  windows(14,6) ; par(mar=c(2,3,2,2))
  tiaan_specgram(rls_cout1, windowSize=512, overlap=256, normal=1, 
                 output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T)
  title(main=paste(njr_name, "| RLS - 15"))
}


i <- 4
cout4 <- read.table(paste0("~/HealthQ/C FILES/RLS_NS_project/out_fitted_", njr_names[i]), sep=",")

cat("", dim(cout1), "\n", dim(cout1_b))
cat("\n", dim(cout2), "\n", dim(cout2_b))
cat("\n", dim(cout3), "\n", dim(cout3_b))
cat("\n", dim(cout4), "\n", dim(cout4_b))


all(round(cout1[,j] - cout1_b[,j],3) == 0)


zPlot(cbind(cout1[,j], cout1_b[,j]))


  windows(14,6) ; par(mar=c(2,3,2,2))
  tiaan_specgram(cout, windowSize=512, overlap=256, normal=1, Fs=25,
                 output=F, blankFactor=3, maxfreq=4, logTrans=T, graph=T)

