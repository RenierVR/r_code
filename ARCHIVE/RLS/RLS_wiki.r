# http://en.wikipedia.org/wiki/Recursive_least_squares_filter


rls_wiki <- function(x_in, d_in, m, lambda, delta)
{
  x_in <- c(rep(0,m-1), x_in)
  d_in <- c(rep(0,m-1), d_in)
  fit <- numeric(length(d_in))
  avec <- numeric(length(d_in))
  lsvec <- numeric(length(d_in))
  
  w <- cbind(numeric(m))
  P <- delta*diag(m)
  
  for(i in m:length(x_in)) {
#   for(i in m:(m+1)){
    x <- x_in[i:(i-m+1)]
    alpha <- d_in[i] - as.numeric(t(x)%*%w)
    
    lamsum <- lambda + as.numeric(t(x)%*%P%*%x)
    g <- (P %*% x)/(lamsum)

#     avec[i] <- alpha
#     lsvec[i] <- lamsum
    
    # update step
    P <- (1/lambda)*(P - g%*%t(x)%*%P)
    w <- w + alpha*g
    
    fit[i] <- as.numeric(t(x)%*%w)
  }

  return(fit[-(1:(m-1))])  
#   return(list(fit[-(1:(m-1))],
#               avec[-(1:(m-1))],
#               lsvec[-(1:(m-1))]))
}



# xx <- acc_ch[1:10000]
# yy <- grn_ch[1:10000]

fast_rls <- function(xx, yy, m, lambda=0.99, E0=10)
{
  res <- numeric(length(yy))
  
  ## initialisation
  xx <- c(rep(0,m-1), xx)
  A <- B <- K <- W <- cbind(numeric(m))
  gam <- 1
  alpha <- E0*lambda^m
  beta <- E0
  
  ## recursive algorithm
  for(i in 1:length(yy)){
    
    X <- cbind(xx[(i+m-1):i])
    
    ## prediction part
    e <- xx[i+m-1] - as.numeric(t(A)%*%X)
    a_new <- lambda*alpha + gam*(e^2)
    gam_N1 <- (lambda*alpha*gam)/a_new
    K_N1 <- rbind(0, K) + (e/(lambda*alpha))*rbind(-1, A)
    A <- A - e*gam*K
    r <- -lambda*beta*K_N1[m+1]
    gam <- gam_N1/(1 + gam_N1*r*K_N1[m+1])
    K <- cbind((K_N1 + K_N1[m+1]*rbind(B,-1))[1:m])
    B <- B - r*gam*K
    beta <- lambda*beta + gam*(r^2)
    
    ## filtering part
    eps <- yy[i] - as.numeric(t(W)%*%X)
    W <- W - eps*gam*K
    alpha <- a_new
    
    res[i] <- eps
  }
  
  return(res)
}




ns_frls <- function(xx, yy, m, lambda=0.99, E0=10, mu=0.25)
{
  res <- numeric(length(yy))
  
  ## initialisation
  X <- cbind(numeric(m))
  A <- B <- K <- W <- cbind(numeric(m))
  gam <- 1
  alpha <- E0*lambda^m
  beta <- E0
  
  ## recursive algorithm
  for(i in 1:length(yy)){
    
    
    ## prediction part
    e <- xx[i] - as.numeric(t(A)%*%X)
    a_new <- lambda*alpha + gam*(e^2)
    gam_N1 <- (lambda*alpha*gam)/a_new
    K_N1 <- rbind(0, K) + (e/(lambda*alpha))*rbind(-1, A)
    A <- A - e*gam*K
    
    ## rotate X vector with new value
    xx_N <- X[m]
    X <- cbind(c(xx[i], X[1:(m-1)]))
    
    
    ## numerical stabilisation
    r_c <- xx_N - as.numeric(t(B)%*%X)
    r_f0 <- -lambda*beta*K_N1[m+1]
    r_f1 <- -(lambda^(-m))*gam_N1*a_new*K_N1[m+1]
    eta <- r_c - ( (1-mu)*r_f0 + mu*r_f1  )
    r <- r_c + eta
    
    
    gam <- gam_N1/(1 + gam_N1*r*K_N1[m+1])
    K <- cbind((K_N1 + K_N1[m+1]*rbind(B,-1))[1:m])
    B <- B - r*gam*K
    beta <- lambda*beta + gam*(r^2)
    
    ## filtering part
    eps <- yy[i] - as.numeric(t(W)%*%X)
    W <- W - eps*gam*K
    alpha <- a_new
    
    res[i] <- eps
  }
  
  return(res)
}






removeAGCJumps <- function(xx, threshold=500)
{
  xx_adjust <- numeric(length(xx))
  running_DC <- 0
  
  for(i in 2:length(xx)){
    previousSample <- xx[i-1]
    grn1 <- xx[i]
    if(abs(previousSample - grn1) > threshold)
      running_DC <- running_DC + (grn1 - previousSample);
    xx_adjust[i] <- grn1 - running_DC;
  }
  
  return(xx_adjust)
}




# y <- grn_ch
# xx2 <- removeAGCJumps(y)
# 
# windows(14,7)
# par(ask = TRUE, mar=c(4,3,2,2), mfrow=c(2,1))
# j = 0 ; start=27000 ; winsize=50 ; overlap=40
# while(TRUE){
#   j = j+1
#   plotind <- (overlap*(j-1)+start):(start-1+winsize + overlap*(j-1))
#   if(max(plotind) > length(y))
#     break
#   else{
#     plot(plotind, y[plotind], type = "l", lwd=2, col="limegreen")
#     points(plotind, y[plotind], pch=19, cex=0.7, col="seagreen")
#     plot(plotind, xx2[plotind], type="l", lwd=2, col="blue")
#   }
# }


########################################################################################
########################################################################################

# tt <- seq(0, 75, 0.1)
# xx <- 0.5*cos(7*tt)
# dd <- round(100*sin(tt),0)
# dd_corrupted <- dd + xx + rnorm(length(tt), 0, 0.05)
# 
# win(14,7)
# plot(tt, dd_corrupted, type="l", lwd=2, col="blue")
# lines(tt, dd, col="black")
# lines(tt, xx, col="green")




# plot_fun_rls <- function(a, b, d)
# {
#   xx <- d*sin(a+b*tt)
#   dd_corrupted <- dd + xx + rnorm(length(tt), 0, 0.05)
#   dd_rls <- rls_wiki(xx, dd_corrupted, 15, 0.99, 100)
#   plot(tt, dd, type="l", col="black", lwd=2, ylim=range(c(dd,dd_corrupted,xx,dd_rls)), ann=F)
#   lines(tt,  dd_corrupted, col="blue")
#   lines(tt, xx, col="gray")
#   lines(tt, dd_rls, col="limegreen")
#   lines(tt, dd_corrupted-dd_rls, col="red")
#   legend("topright", lwd=1, col=c("black", "blue", "gray", "limegreen", "red"),
#          leg=c("dd", "dd_corr", "xx [ d*sin(a+bt) ]", "dd_rls", "recovered"))
# }

# win(14,7)
# plot_fun_rls(0,1)

# manipulate(plot_fun_rls(a, b, d),
#            a=slider(0,2*pi),
#            b=slider(0.6,1.4,step=0.025),
#            d=slider(0.1,10,step=0.1,init=1))



# manipulate(plot_fun_rls(a, b),
#            ord=slider(5,20,init=10),
#            lam=picker(0.96, 0.97, 0.98, 0.99, 1, init=0.99),
#            del=slider(1,200,init=100))

# bseq <- seq(0.75, 1.25,0.01)
# windows()
# par(ask=T)
# for(i in 1:length(bseq)){
#   xx <- round(10000*sin(0+bseq[i]*tt),0)
#   dd_corrupted <- dd + xx + round(rnorm(length(tt), 0, 10),0)
#   dd_rls <- rls_wiki(xx/100, dd_corrupted, 15, 0.99, 100)
#   dd_rec <- dd_corrupted - dd_rls
#   plot(tt, dd, type="l", col="black", lwd=2, ylim=range(c(dd,dd_corrupted,xx,dd_rls)), ann=F)
#   lines(tt,  dd_corrupted, col="blue")
#   lines(tt, xx, col="gray")
#   lines(tt, dd_rls, col="limegreen")
#   lines(tt, dd_rec, col="red")
#   legend("topright", lwd=1, col=c("black", "blue", "gray", "limegreen", "red"),
#          leg=c("dd", "dd_corr", "xx [ d*sin(a+bt) ]", "dd_rls", "recovered"))
#   title(paste0("noise: sin(0 + ", bseq[i],"*t)"))
#   
# #   windows(14,7)
#   tiaan_specgram(dd_rec, windowSize=128, overlap=64, normal=3, 
#                  output=F, blankFactor=1, maxfreq=1, logTrans=T, graph=T,
#                  col=blue2green2red, Fs=10)
#   title(paste0("noise: sin(0 + ", bseq[i],"*t)"))
#   
# }
