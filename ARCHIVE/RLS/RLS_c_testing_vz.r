source("../tiaan_specgram.r")
library(colorRamps)


vznames <- c("ModG16_vzpp",
             "20140512_2131_MojoNJR1_Bluetooth_RenierVanRooyen_001",
             "20140514_0958_MojoNJR1_GregorRohrig_001",
             "20140514_1434_MojoNJR1_Bluetooth_CobusHavenga_001",
             "20140513_2234_MojoNJR1_Bluetooth_DirkBadenhorst_002",
             "20140515_1722_MojoNJR1_Bluetooth_GalenWright_005",
             "20140220_ModC4_Exercise_GymRowing_001",
             "SCF2P5_signal_to_nosignal",
             "Senzo_Large_Exercisesignals")


# spc <- "new"
i <- 9

dat <- read.table(paste0("~/HealthQ/C FILES/RLS_NS_project/data/VanZylTest/", vznames[i]), sep=",", head=T)
grn_ch <- dat[,1]
acc_ch <- dat[,2]


cout <- read.table(paste0("~/HealthQ/C FILES/RLS_NS_project/out_fitted_", vznames[i]), sep=",")[,1]


# cout <- read.table("~/HealthQ/C FILES/RLS_NS_project/out_fitted.txt", sep=",")[,1]
windows(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(cout, windowSize=512, overlap=256, normal=1, 
               output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T,
               col=blue2green2red, Fs=50)
# savePlot(paste0(vznames[i], "_spec_", spc), "png")


#######################################################################################
#######################################################################################
#######################################################################################

windows(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(grn_ch, windowSize=512, overlap=256, normal=1, 
               output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T)
#                col=blue2green2red, Fs=50)



grn_rls <- rls_wiki(acc_ch, grn_ch, 15, 0.99, 100)



windows(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(grn_rls, windowSize=512, overlap=256, normal=1, 
               output=F, blankFactor=3, maxfreq=3.5, logTrans=T, graph=T,
               col=blue2green2red, Fs=50)






#######################################################################################
#######################################################################################
#######################################################################################


c_gam  <- read.table("~/HealthQ/C FILES/RLS_NS_project/out_gamma.txt")[,1]
c_gam1 <- read.table("~/HealthQ/C FILES/RLS_NS_project/out_gamma1.txt")[,1]



windows(14,7)
plot(c_gam, pch=19, cex=0.5, col="purple")#, ylim=c(0.99,1.01))
# savePlot(paste0(vznames[i], "_gamma_", spc), "png")



# neg_diff <- which(c_gam-c_gam1 < 0)
# windows(14,7) ; par(mfrow=c(2,1), mar=c(2,3,2,2))
# plot(c_gam, pch=19, cex=0.5, col="purple")
# plot(c_gam-c_gam1, type="n")
# abline(v=neg_diff, col="lightblue")
# points(c_gam-c_gam1, pch=19, cex=0.5, col="limegreen")


n_d_count <- 0
n_d_ind <- NULL
for(i in 1:length(c_gam)){
  if(c_gam[i] < c_gam1[i]){
    n_d_count <- n_d_count + 1
  }
  else
    n_d_count <- 0
  
  if(n_d_count == 3)
    n_d_ind <- c(n_d_ind, i)
}




windows(14,7) ; par(mfrow=c(2,1), mar=c(2,4,2,2))
plot(c_gam, pch=19, cex=0.5, col="purple")
plot(c_gam-c_gam1, type="n")
abline(v=n_d_ind, col="lightblue")
points(c_gam-c_gam1, pch=19, cex=0.5, col="limegreen")








