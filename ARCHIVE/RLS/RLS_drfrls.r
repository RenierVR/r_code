

DR_FRLS <- function(d, x, N, lambda, delta, eps, phi)
{
  
  if(length(d) != length(x))
    stop("Input signals not of equal length.")
  
  
  ## Initialisation
  a_x <- b_x <- xvec <- yvec <- pvec <- h <- numeric(N)
  khat_0p <- khat_0x <- numeric(N-1)
  E_bx <- E_ax <- uhat_x <- uhat_p <- 0
  
  K_vals <- c(1.5, 2.5, NA, 0, 1)
  
  
  ## reset code
  a_x     <- c(1, rep(0, N-1))
  b_x     <- c(rep(0, N-1), 1)
  xvec <- pvec <- rep(0, N)
  khat_0x <- khat_0p <- rep(0, N-1)
  E_bx    <- delta
  E_ax    <- (lambda^(N-1))*E_bx
  uhat_x  <- uhat_p <- 1   
  h       <- rep(0, N)
  
  nr_resets <- c(0, 0)
  
  out_vec   <- numeric(length(x))
  
  for(i in 1:length(x)){
    
    if(i %% 5000 == 0)
      cat(i, "\n")
    
    ## update reference vector with new value
    if(i < N)
      xvec <- c(x[i:1], rep(0, N-i))
    else
      xvec <- x[i:(i-N+1)]
    
    yvec <- xvec
    
    ########################################################
    ## regularized related updates
    ########################################################
    restart <- TRUE
    restart_cnt <- 0
    
    while(restart){
      if(restart_cnt > 1)
        stop("infinite restarts")
      
      ## update p vector according to (11)
      pvec <- rep(0, N)
      pvec[1 + (i %% N)] <- 1
        
      e_0pna <- eps * as.numeric(t(a_x) %*% pvec)
      k_0pn1 <- e_0pna/(E_ax * lambda)
      k_0p   <- c(0, khat_0p) + k_0pn1*a_x
      u_pn   <- 1/((1/uhat_p) + phi*e_0pna*k_0pn1)
    
      ef_0pnb <- eps * as.numeric(t(b_x) %*% pvec)
      kf_0pnN <- ef_0pnb/(E_bx * lambda)
      ks_0pnN <- k_0p[N]
      es_0pnb <- lambda * E_bx * ks_0pnN
    
      e1_0pnb <- K_vals[1]*ef_0pnb + (1-K_vals[1])*es_0pnb
      e2_0pnb <- K_vals[2]*ef_0pnb + (1-K_vals[2])*es_0pnb
      e5_0pnb <- K_vals[5]*ef_0pnb + (1-K_vals[5])*es_0pnb
      k_0pnN  <- K_vals[4]*kf_0pnN + (1-K_vals[4])*ks_0pnN
    
      khat_0p_new <- (k_0p - k_0pnN*b_x)[-N]
      uhat_p_new  <- 1/((1/u_pn) - phi*e5_0pnb*ks_0pnN)
    
      e_1pna  <- uhat_p * e_0pna
      a_pn    <- a_x - (phi*e_1pna)*c(0, khat_0p)
      E_apn_i <- 1/(lambda*E_ax) - phi*(k_0pn1^2)*u_pn
    
      uhat_p  <- uhat_p_new
      khat_0p <- khat_0p_new
    
      e1_1pnb <- uhat_p*e1_0pnb
      e2_1pnb <- uhat_p*e2_0pnb
    
      b_pn  <- b_x - (phi*e1_1pnb)*c(khat_0p, 0) ## which e_1pnb??
      E_bpn <- lambda*E_bx + phi*e2_0pnb*e2_1pnb ## which e_1pnb and e_0pnb??
    
#       cat(u_pn, "\n")
      ## restart regularization updates with reset
      if(u_pn > 1 || u_pn < 0){
        a_x     <- c(1, rep(0, N-1))
        b_x     <- c(rep(0, N-1), 1)
        xvec    <- rep(0, N)
        khat_0x <- khat_0p <- rep(0, N-1)
        E_bx    <- delta
        E_ax    <- (lambda^(N-1))*E_bx
        uhat_x  <- uhat_p <- 1   
        h       <- rep(0, N)
        
        restart_cnt <- restart_cnt + 1
        nr_resets[1] <- nr_resets[1] + 1
      }
      else
        restart <- FALSE
    
    
    ########################################################
    ## data related updates
    ########################################################
    
      if(!restart){
        e_0xna <- as.numeric(t(a_pn) %*% xvec)
        k_0xn1 <- e_0xna * E_apn_i
        k_0x   <- c(0, khat_0x) + k_0xn1*a_pn
        u_xn   <- 1/((1/uhat_x) + e_0xna*k_0xn1)
    
        ef_0xnb <- as.numeric(t(b_pn) %*% xvec)
        kf_0xnN <- ef_0xnb/E_bpn
        ks_0xnN <- k_0x[N]
        es_0xnb <- E_bpn * ks_0xnN
    
        e1_0xnb <- K_vals[1]*ef_0xnb + (1-K_vals[1])*es_0xnb
        e2_0xnb <- K_vals[2]*ef_0xnb + (1-K_vals[2])*es_0xnb
        e5_0xnb <- K_vals[5]*ef_0xnb + (1-K_vals[5])*es_0xnb
        k_0xnN  <- K_vals[4]*kf_0xnN + (1-K_vals[4])*ks_0xnN
    
        khat_0x_new <- (k_0x - k_0xnN*b_pn)[-N]
        uhat_x_new  <- 1/((1/u_xn) - e5_0xnb*ks_0xnN)
    
        e_1xna <- uhat_x * e_0xna
        a_x    <- a_pn - e_1xna*c(0, khat_0x)
    
        khat_0x <- khat_0x_new
        uhat_x  <- uhat_x_new
        E_ax    <- 1/(E_apn_i - (k_0xn1^2)*u_xn)
    
        e1_1xnb <- uhat_x * e1_0xnb
        e2_1xnb <- uhat_x * e2_0xnb
    
        b_x  <- b_pn - e1_1xnb * c(khat_0x, 0) ## which e_1xnb ??
        E_bx <- E_bpn + e2_0xnb * e2_1xnb      ## which e_1xnb and e_0xnb ??
      
        
        ## restart reg & data updates with reset
        if(u_xn > 1 || u_xn < 0){
          a_x     <- c(1, rep(0, N-1))
          b_x     <- c(rep(0, N-1), 1)
          xvec    <- rep(0, N)
          khat_0x <- khat_0p <- rep(0, N-1)
          E_bx    <- delta
          E_ax    <- (lambda^(N-1))*E_bx
          uhat_x  <- uhat_p <- 1   
          h       <- rep(0, N)
          
          restart_cnt <- restart_cnt + 1
          restart <- TRUE
          nr_resets[2] <- nr_resets[2] + 1
        }
        else
          restart <- FALSE
      }
    }
    
    ## likelihood variable stabilization
    if((i %% 2) == 1)
      uhat_p <- ((lambda^(N-1))*E_bx)/(uhat_x*E_ax)
    else
      uhat_x <- ((lambda^(N-1))*E_bx)/(uhat_p*E_ax)
    
    ## joint process extension
    e_0 <- d[i] - as.numeric(t(h) %*% yvec)
    e_1 <- e_0 * u_xn
    
    h <- h + e_1 * k_0x
    
    out_vec[i] <- e_0  
  }
  
  cat(paste("TOTAL RESETS:", paste(nr_resets, collapse=", ")), "\n")
  return(out_vec)
}


# xx <- 0.5*sin(3*seq(0,10,0.1))
# dd <- sin(seq(0,10,0.1)) + xx

# w() ; lplot(out)
# DR_FRLS(d=grn_ch[1:200], x=acc_ch[1:200], N=15, lambda=0.99, delta=10, eps=5, phi=1)

# grn_ch <- grn_ch[35000:125000]
# acc_ch <- acc_ch[35000:125000]

out <- DR_FRLS(d=grn_ch, x=acc_ch, N=30, lambda=0.9, delta=50, eps=1, phi=1)

w(14,6) ; par(mar=c(2,3,2,2))
tiaan_specgram(out, windowSize=512, overlap=256, blankFactor=3, maxfreq=3.5, normal=1)

