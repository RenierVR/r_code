source("../useful_funs.r")
source("./sp02_funs.r")



fpath1 <- "~/HealthQ/Data/OxySat/SpO2 forehead testing 20150331/"
indivs <- c("Kora", "Lindie", "Michael", "Nina", "Renier", "Robert", "Tiaan", "Van Zyl")

fpath2 <- paste0(fpath1, paste0(rep(indivs, each=3), c("/1/", "/2/")))


##################################################################################################
##################################################################################################


i <- 12


for(i in 1:len(fpath2)){

  
  datlist <- getDataFromDir(fpath2[i], head=T)
  dat1 <- datlist[[1]]
  dat2 <- datlist[[2]]
  wins <- datlist[[3]][,3]
  datnames <- names(datlist)[1:2]
  rm(datlist)
  
  
  ###################
  #### dat1
  
  dat <- dat1
  
  dat <- dat[dat[,1] != 0,]
  dat <- dat[-c(1:500),]
  curr1 <- curr2 <- curr3 <- matrix(0, nr=nrow(dat)-1, nc=4)
  rfmat <- isubmat <- matrix(0, nr=nrow(dat)-1, nc=4)
  for(j in 1:4){
    if(j < 3){
      Rf <- dat[,9]
      led <- dat[,11]
    }
    else{
      Rf <- dat[,10]
      led <- dat[,j+13]
    }
    
    safe_term <- 0
    if(j == 2){
      safe_term <- 1
      led <- numeric(nrow(dat))
    }
    
    dat_tmp <- dat[-1,j] 
    isub_tmp <- 10*dat[,10+j]
    isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
    Rf <- Rf[1:(len(Rf)-1)]
    led <- led[1:(len(led)-1)]
    curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
    curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)  
    curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)[[1]]
    rfmat[,j] <- Rf
    isubmat[,j] <- isub_tmp
  }
  
  zPlot(cbind(curr2[,c(1,3:4)]-curr2[,2]), par_new=T, col_f=c("lightblue", "red", "forestgreen"))
  
  redI_clipped1 <- curr3[,3]-curr3[,2]
  nirI_clipped1 <- curr3[,4]-curr3[,2]
  
  
  ###################
  #### dat2
  
  dat <- dat2
  
  dat <- dat[dat[,1] != 0,]
  dat <- dat[-c(1:500),]
  curr1 <- curr2 <- curr3 <- matrix(0, nr=nrow(dat)-1, nc=4)
  rfmat <- isubmat <- matrix(0, nr=nrow(dat)-1, nc=4)
  for(j in 1:4){
    if(j < 3){
      Rf <- dat[,9]
      led <- dat[,11]
    }
    else{
      Rf <- dat[,10]
      led <- dat[,j+13]
    }
    
    safe_term <- 0
    if(j == 2){
      safe_term <- 1
      led <- numeric(nrow(dat))
    }
    
    dat_tmp <- dat[-1,j] 
    isub_tmp <- 10*dat[,10+j]
    isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
    Rf <- Rf[1:(len(Rf)-1)]
    led <- led[1:(len(led)-1)]
    curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
    curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)  
    curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)[[1]]
    rfmat[,j] <- Rf
    isubmat[,j] <- isub_tmp
  }
  
  zPlot(cbind(curr2[,c(1,3:4)]-curr2[,2]), par_new=T, col_f=c("lightblue", "red", "forestgreen"))
  
    
  redI_clipped2 <- curr3[,3]-curr3[,2]
  nirI_clipped2 <- curr3[,4]-curr3[,2] 
  
  
  
  #######################################
  
  out <- fftSpO2_Cdev(redI_clipped1, nirI_clipped1, filtType="fir50", adapF=F, smoothACDC=0.8,
                      windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F)
  spo2_1  <- 100*calabFreeSpO2(out[[1]], clip=F)
  
  
  out <- fftSpO2_Cdev(redI_clipped2, nirI_clipped2, filtType="fir50", adapF=F, smoothACDC=0.8,
                      windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F)
  spo2_2  <- 100*calabFreeSpO2(out[[1]], clip=F)

  
  w() ; lplot(wins, lwd=3, col="black", ylim=c(80,105), ylab="spo2",
              main=paste("SpO2 test: ", substring(fpath2[i], 56, nchar(fpath2[i]))))
  lines(spo2_1, lwd=2, col="blue")
  lines(spo2_2, lwd=2, col="forestgreen")
  legend("bottomright", bg="white", lwd=2, col=c("black", "blue", "forestgreen"),
         leg=c("winspiro", datnames), cex=0.75)


  savePlot(paste0("spo2_results/result_", i), "png")

}




