source("../useful_funs.r")
source("./sp02_funs.r")


data_list_good <- NULL

## discrete ces housing
data_list <- getDataFromDir("~/HealthQ/DUMPED/20150219/Mojo 5037/", head=T)
data_list_good <- c(data_list_good, data_list[c(1,2,4,5)])

data_list <- getDataFromDir("~/HealthQ/DUMPED/20150303/Mojo 5037 (discrete ces)/", head=T)
data_list_good <- c(data_list_good, data_list[c(1,2,4)])

## discrete choc box
data_list <- getDataFromDir("~/HealthQ/DUMPED/20150303/Mojo 5045 (discrete box)/", head=T)
data_list_good <- c(data_list_good, data_list[c(1,3,4,6)])

## mojo breakout
data_list <- getDataFromDir("~/HealthQ/DUMPED/20150225/Mojo Breakout/", head=T)
data_list_good <- c(data_list_good, data_list[c(1,4,6,10,12)])


names(data_list_good) <- paste0(names(data_list_good), c(rep("_discreteces", 7),
                                                         rep("_discretebox", 4),
                                                         rep("_breakout", 5)))

################################################################################################


for(i in 1:15){

dat <- data_list[[i]]

dat <- dat[dat[,1] != 0,]
acc <- acc_res(dat[,5:7])


curr1 <- curr2 <- matrix(0, nr=nrow(dat)-1, nc=4)
rfmat <- isubmat <- matrix(0, nr=nrow(dat)-1, nc=4)
for(j in 1:4){
  if(j < 3){
    Rf <- dat[,9]
    led <- dat[,11]
  }
  else{
    Rf <- dat[,10]
    led <- dat[,j+13]
  }
  
  safe_term <- 0
  if(j == 2){
    safe_term <- 1
    led <- numeric(nrow(dat))
  }
  
  dat_tmp <- dat[-1,j] 
  isub_tmp <- 10*dat[,10+j]
  isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
  Rf <- Rf[1:(len(Rf)-1)]
  led <- led[1:(len(led)-1)]
  curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
  curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led)  
  rfmat[,j] <- Rf
  isubmat[,j] <- isub_tmp
}


# zPlot(cbind(curr2[,c(1,3:4)]-curr2[,2]), par_new=T, col_f=c("lightblue", "red", "forestgreen"))



# pind <- 3500:4500 ;w() ; par(mar=c(4,4,3,4))
# lplot(pind, curr2[pind,3]-curr2[pind,2], lwd=2, col="red",
#       ylab="current (red)", xlab="index", main="LQ-5140 (Kari)")
# par(new=T) ; lplot(pind, curr2[pind,4]-curr2[pind,2], lwd=2, col="blue", 
#                    ylab="", xlab="", xaxt="n", yaxt="n") 
# axis(4) ; mtext("current (IR)", side=4, line=2, cex=par("cex.lab"))
# legend("bottomright", bg="white", col=c("red", "blue"), lwd=2, leg=c("red", "IR"), cex=0.8)





# redI_clipped <- curr1[,3]-curr1[,2]
# nirI_clipped <- curr1[,4]-curr1[,2] 
redI_clipped <- curr2[,3]-curr2[,2]
nirI_clipped <- curr2[,4]-curr2[,2]




# zPlot(cbind(FIRfilt(redI_clipped), FIRfilt(nirI_clipped)), par_new=T)

out <- fftSpO2_Cdev(redI_clipped, nirI_clipped, subtractMeans=F, filtType="fir50", 
                    windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=T,
                    spo2_caleb_pf=0.7, spo2_caleb_e1x=0.08, spo2_caleb_e2x=0.29, 
                    spo2_caleb_e1y=0.81, spo2_caleb_e2y=0.18, spo2_caleb_bias=0)

spo2_1  <- 100*calabFreeSpO2(out[[1]], clip=F)



w() ; par(mar=c(4,4,2,3)) ; lplot(movingAve(spo2_1,1), lwd=2, col="blue", ylim=c(60,110), ylab="spO2")
par(new=T) ; lplot(movingAve(out[[4]][,7],5), ylim=range(out[[4]][,7:8]), ann=F, col="red", yaxt="n", xaxt="n")
lines(movingAve(out[[4]][,8],5), col="forestgreen") ; axis(4)
savePlot(paste0("spo2_results/", names(data_list)[i], "spo2_qm4"), "png")

graphics.off()
}