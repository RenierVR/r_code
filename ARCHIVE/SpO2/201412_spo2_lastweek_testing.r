source("sp02_funs.r")
source("../zPlot.r")



##################################################################################################
##################################################################################################


fpath <- c("~/HealthQ/Data/OxySat/")

fpath_mojo <- paste0(fpath, c("20141215/Dirk/20141215_1050_Dirk_finger.csv",
                              "20141215/Renier/20141215_1520_Renier_finger.csv",
                              "20141215/Andre/20141215_1546_Andre_finger.csv",
                              "20141215/Tiaan/20141215_1519_Tiaan_finger.csv",
                              "20141215/Van Zyl/20141215_1609_VanZyl_finger.csv",
                              "20141215/Dirk/20141215_1533_Dirk_mojo_wrist.csv",
                              "20141215/Renier/20141215_1527_Renier_mojo_wrist.csv",
                              "20141215/Andre/20141215_1523_Andre_mojo_wrist.csv",
                              "20141215/Tiaan/20141215_1543_Tiaan_wrist.csv",
                              "20141215/Van Zyl/20141215_1552_VanZyl_wrist.csv",
                              "20141217/Dirk/20141217_1142_Dirk_wrist.csv",
                              "20141217/Renier/20141217_1157_Renier_wrist.csv",
                              "20141218/Riaan/20141218_Riaan_mojo_wrist.txt"))


fpath_ws <- paste0(fpath, c("20141215/Dirk/20141215_1052_Dirk_Winspiro_Spo2_finger.csv",
                            "20141215/Renier/20141215_1118_Renier_Winspiro_Spo2_finger.csv",
                            "20141215/Andre/20141215_1147_Andre_winspiro_Spo2_finger.csv",
                            "20141215/Tiaan/20141215_1700_Tiaan_winspiro_Spo2_finger.csv",
                            "20141215/Van Zyl/20141215_1600_VanZyl_winspiro_Spo2_finger.csv",
                            "20141215/Dirk/20141215_1230_Dirk_Winspiro_Spo2_wrist.csv",
                            "20141215/Renier/20141215_1430_Renier_Winspiro_Spo2_wrist.csv",
                            "20141215/Andre/20141215_1245_Andre_Winspiro_Spo2_wrist.csv",
                            "20141215/Tiaan/20141215_1645_Tiaan_winspiro_Spo2_wrist.csv",
                            "20141215/Van Zyl/20141215_1500_VanZyl_winspiro_Spo2_wrist.csv",
                            "20141217/Dirk/20141217_1140_Dirk_winspiro_Spo2_wrist.csv",
                            "20141217/Renier/20141217_1202_Renier_winspiro_Spo2_wrist.csv",
                            "20141218/Riaan/20141218_riaan_wrist.csv"))



is_current <- c(rep(F,12), T)



i <- 5

indexes <- 1:13

write_output <- TRUE
test_against_C <- FALSE
for(i in indexes){
  cat("in progress: ", round(100*(which(i == indexes)-1)/length(indexes),2), "% ...\n")
  
  dat <- read.table(fpath_mojo[i], sep=",", head=T)
  n <- nrow(dat)
  
#   red_clipped <- removeAGCJumps(dat[,3], 500)[-1]
#   fir_clipped <- removeAGCJumps(dat[,4], 500)[-1]
  
  if(!is_current[i]){
    redI <- round(convertToCurrentV2(dat[,3], dat[,13]/1000000, dat[,10]*1000)*1e14, 0)
    firI <- round(convertToCurrentV2(dat[,4], dat[,14]/1000000, dat[,10]*1000)*1e14, 0)
    blkI <- round(convertToCurrentV2(dat[,2], dat[,12]/1000000, dat[,9]*1000)*1e14, 0)
    
    
    redI_clipped <- multSpikeFilter(redI-blkI, 1e7)
    firI_clipped <- multSpikeFilter(firI-blkI, 1e7)
    redI_clipped <- round(redI_clipped[[1]], 0)
    firI_clipped <- round(firI_clipped[[1]], 0)
  }
  else{
    redI <- dat[,4]
    firI <- dat[,3]
    blkI <- dat[,2]
    
    redI_clipped <- multSpikeFilter(redI, 1e5)
    firI_clipped <- multSpikeFilter(firI, 1e5)
    redI_clipped <- round(redI_clipped[[1]], 0)
    firI_clipped <- round(firI_clipped[[1]], 0)
  }
  
#   redI_clipped <- round(removeAGCJumps((redI - blkI), 1e3)[-1],0)
#   firI_clipped <- round(removeAGCJumps((firI - blkI), 1e3)[-1],0)
  

#   redI_clipped <- round(removeAGCJumps(redI_clipped, 1e4)[-1],0)
#   firI_clipped <- round(removeAGCJumps(firI_clipped, 1e4)[-1],0)




  
  grn <- removeAGCJumps(dat[,1]-dat[,2])[-1]
  
  while(FALSE){
    
    zPlot(grn)  
    zPlot(cbind(FIRfilt(redI_clipped), FIRfilt(firI_clipped)), col_f=c("red", "orange"), par_new=T)
    zPlot(cbind(red_clipped, fir_clipped), col_f=c("red", "blue"), par_new=T)
    zPlot(cbind(redI_clipped, firI_clipped), col_f=c("red", "blue"), par_new=T)

  }
  
  
  
  ##########################################################################################
  
  
  
  
  out <- fftSpO2_R(firI_clipped, redI_clipped, subtractMeans=F, filtType="fir50", 
                   windowSize=2^8, fftL=0, logicSD=15, fftI=F, interpBW=1, useMaxDenom=F)
#   out <- fftSpO2_R(oneSampSpikeFilter(firI_clipped, 25000), oneSampSpikeFilter(redI_clipped, 25000), 
#                    subtractMeans=F, filtType="fir50", 
#                    windowSize=2^8, fftL=0, logicSD=15, fftI=F, interpBW=1, useMaxDenom=T)

  spo2 <- 100*calabFreeSpO2(out[[1]], pF=0.65, logBias=0)#, smoothAlpha=0.8)
  spo2_ws <- read.table(fpath_ws[i], sep=",", head=T)[,3]


  w()
  lplot(movingAve(spo2,9)[20:530], ylim=c(75,100), lwd=2, col="blue",
        ylab="SpO2 %", xlab="time (s)")
  lines(spo2_ws[-c(1:1)], lwd=2, col="red")
  legend("bottomright", lwd=2, col=c("blue", "red"),
       leg=c("LifeQ", "reference"))

  title(main="Oxygen Saturation Test (2014-12-15)")

  
  while(FALSE){
    
    mean(out[[1]][,1]/out[[1]][,2])
    mean(out[[1]][,3]/out[[1]][,4])
    
    zPlot(out[[1]][,c(1,3)])
    zPlot(out[[1]][,c(2,4)])
    
    zPlot(out[[5]], c("red filtered", "ir filtered"))
    
  }
  
 
#   spo2 <- movingAve(spo2, 30)

#   isub_mat <- (dat[-c(1:501),13:14])
#   isub_mat <- isub_mat[seq(1,nrow(isub_mat),50),]
#   rf <- dat[-c(1:501),10]
#   rf <- rf[seq(1,len(rf),50)]
  
  
  w() 
#   lplot(isub_mat[,1], lty=2, col="red", axes=F, ylab="", xlab="")
#   par(new=T) ; lplot(isub_mat[,2], lty=2, col="maroon", axes=F, ylab="", xlab="")
#   par(new=T) ; lplot(rf, lty=2, col="blue", axes=F, ylab="", xlab="")
#   par(new=T) 
  lplot(movingAve(spo2,9), ylim=c(80,100), lwd=2, col="blue")
  lines(spo2_ws[-c(1:20)],1, lwd=2, col="red")

  fname <- basename(fpath_mojo[i])
  fname <- substr(fname, 1, nchar(fname)-4)
  title(main = fname)

  #   legend("bottomright", lwd=1, col=c("forestgreen", "red"),
  #          leg=c("spo2", "winspiro"))
  
  if(write_output){
    savePlot(paste0("spo2_results/", fname, "_logic0"), type="png")
    graphics.off()
    
#     write.table(format(cbind(firI, redI, blkI), digits=12),
#                 file=paste0("~/HealthQ/C FILES/SP02_IMPLEMENTATION/data/", fname, ".txt"),
#                 sep=",", quote=F, row.names=F, col.names=F)
  }


#############################################
#           TESTING R AGAINST C OUTPUT
#############################################


  if(test_against_C){
    
    cout <- read.table(paste0("~/HealthQ/C FILES/SP02_IMPLEMENTATION/physcalc_out_", fname, ".txt"), sep=",")  
    cspo2 <- cout[seq(1,nrow(cout),50),1][-c(1:20)]
    
    w() 
    lplot(spo2, lwd=2, col="blue")
    lines(cspo2, lwd=2, col="forestgreen")
    legend("bottomright", lwd=1, col=c("blue", "forestgreen"), leg=c("R", "C"))
    title(main=paste0(fname, "\nR vs C output comparison"))
    
  }
  
  if(i == indexes[length(indexes)])
    cat("completed\n")
  
}




##########################################################################################
##########################################################################################
##########################################################################################



# red1 <- oneSampSpikeFilter(dat[,1] - dat[,3], 25000)
# fir1 <- oneSampSpikeFilter(dat[,2] - dat[,3], 25000)


# i = 6, 7, 8 with static agc


fnames1 <- c("20141221_tim_rus_vinger", "20141221_kora_rus_vinger", "20141221_renier_rus_vinger",
             "20141221_renier_rus_wrist", "20141221_tim_static_agc",
             "20141222_tim_gewrig1", "20141222_renier_gewrig1", "20141222_riaan_gewrig1",
             "20141222_tim_gewrig2", "20141222_renier_gewrig2", "20141222_tiaan_gewrig1",
             "20141222_kari_gewrig1", "20141222_tim_gewrig3", "20141222_renier_gewrig3",
             "20141222_renier_gewrig_norf", "20141222_tim_gewrig_norf")
fnames2 <- paste0("~/HealthQ/Data/OxySat/dev format/", fnames1, ".txt")

i <- 16


indexes <- 6:16


for(i in indexes){
  cat("in progress: ", round(100*(which(i == indexes)-1)/length(indexes),2), "% ...\n")  
  
  
dat <- read.table(fnames2[i], sep=",")
redI <- dat[,1]
firI <- dat[,2]
blkI <- dat[,3]

# write.table(format(cbind(redI, firI, blkI), digits=12), 
#             file=paste0("~/HealthQ/C FILES/SP02_IMPLEMENTATION/data/", fnames1[i], ".txt"), 
#             sep=",", quote=F, row.names=F, col.names=F)



##########################################################


cout <- read.table(paste0("~/HealthQ/C FILES/SP02_IMPLEMENTATION/physcalc_out_", fnames1[i], ".txt"), sep=",")
cspo2 <- (cout[seq(1,nrow(cout),50),1])[-c(1:16)]
# w() ; lplot(cspo2, lwd=2)


redI_clipped <- multSpikeFilter(redI-blkI, 1e5)
firI_clipped <- multSpikeFilter(firI-blkI, 1e5)
redI_clipped <- round(redI_clipped[[1]], 0)
firI_clipped <- round(firI_clipped[[1]], 0)


# zPlot(cbind(redI_clipped, firI_clipped), par_new=T)



out <- fftSpO2_R(redI_clipped, firI_clipped, subtractMeans=F, filtType="fir50", 
                 windowSize=2^8, fftL=0, logicSD=15, fftI=T, interpBW=2, useMaxDenom=T)
###################################################################################################
# out <- fftSpO2_R(oneSampSpikeFilter(redI_clipped, 25000), oneSampSpikeFilter(firI_clipped, 25000), 
#                  subtractMeans=F, filtType="fir50", 
#                  windowSize=2^8, fftL=0, logicSD=15, fftI=T, interpBW=1, useMaxDenom=T)
##################################################################################################
spo2  <- 100*calabFreeSpO2(out[[1]], pF=0.7, logBias=0)#, smoothAlpha=0.8)
out_qm <- out[[2]]



isub_mat <- (dat[-c(1:501),7:8])
isub_mat <- isub_mat[seq(1,nrow(isub_mat),50),]
rf <- dat[-c(1:501),6]
rf <- rf[seq(1,len(rf),50)]


w() 
# lplot(isub_mat[,1], lty=2, col="red", axes=F, ylab="", xlab="")
# par(new=T) ; lplot(isub_mat[,2], lty=2, col="maroon", axes=F, ylab="", xlab="")
# par(new=T) ; lplot(rf, lty=2, col="blue", axes=F, ylab="", xlab="")
# par(new=T) 
lplot(movingAve(spo2,1), ylim=c(85,100), lwd=2)
lines(movingAve(cspo2,1), lwd=2, col="blue")
# lines( (dat[seq(1,nrow(dat),50),4]/10)[-c(1:16)], lwd=1, col="purple")
# legend("topright", lwd=2, col=c("forestgreen", "blue", "purple"), leg=c("R", "C", "device"))
title(main=fnames1[i])
# par(new=T)
# lplot(out_qm[,1], axes=F, ylab="", xlab="", ylim=c(0,1), col="red")
# lines(out_qm[,2], col="orange")
# lines(1.5*out_qm[,3], col="maroon")#, axes=F, ylab="", xlab="")
# axis(side=4)


savePlot(paste0("spo2_results/", fnames1[i], "_smooth2"), type="png")
graphics.off()

}

