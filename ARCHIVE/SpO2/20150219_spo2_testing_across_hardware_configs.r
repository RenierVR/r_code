source("../useful_funs.r")
source("./sp02_funs.r")



##################################################################################################
##########    NJRC mojos


data_list <- getDataFromDir("~/HealthQ/DUMPED/20150219/Mojo 5137/", head=T)
data_list <- getDataFromDir("~/HealthQ/DUMPED/20150219/Mojo 5140/", head=T)


##################################################################################################
##########    DISCRETE COMP CES HOUSING


data_list <- getDataFromDir("~/HealthQ/DUMPED/20150219/Mojo 5037/", head=T)
data_list <- getDataFromDir("~/HealthQ/DUMPED/20150303/Mojo 5037 (discrete ces)/", head=T)

##################################################################################################
##########    DISCRETE COMP BOX HOUSING

data_list <- getDataFromDir("~/HealthQ/DUMPED/20150303/Mojo 5045 (discrete box)/", head=T)


##################################################################################################
##########    OLD RESULTS FROM DIRK

data_list <- getDataFromDir("~/HealthQ/Data/RedIRSQ/batch 1/", head=T)
# 1 - meh 2 - meh 3 - kak 4 - hoofsaaklik kak 5 - oraait 6 - heel oraait 7 - oraait

data_list <- getDataFromDir("~/HealthQ/Data/RedIRSQ/batch 2/", head=T)
# 1 - great success 2 - ? 3 - goed 4 - ? 5 - not bad 6 - ? 7 - ?

data_list <- getDataFromDir("~/HealthQ/Data/RedIRSQ/batch 3/", head=T)
# 1 - meh 2 - meh 3 - hond poes 4 - kaka 5 - horrible 6 - kak

data_list <- getDataFromDir("~/HealthQ/Data/RedIRSQ/batch 4/", head=T)
# 1 - neewat 2 - meh 3 - okay 4 - seker okay 5 - crap 6 - nope 7 - seker okay 8 - meh 9 - nope - noisy as fuck


##################################################################################################
##################################################################################################

data_list <- getDataFromDir("~/HealthQ/DUMPED/20150225/Mojo 1231/", head=T)
data_list <- getDataFromDir("~/HealthQ/DUMPED/20150225/Mojo Breakout/", head=T)
data_list <- getDataFromDir("~/HealthQ/DUMPED/20150226/Mojo 5098 Old FW/", head=T)
data_list <- getDataFromDir("~/HealthQ/DUMPED/20150226/Mojo 5098 New FW/", head=T)
data_list <- getDataFromDir("~/HealthQ/Data/OxySat/20150226/dark box tests/", head=T)



##################################################################################################
##########    DEV DEVICE - FIXED AGC AFTER CALIBRATION

data_list <- getDataFromDir("~/HealthQ/DUMPED/20150302/Mojo 5089 fixed agc (rf=500)/", head=T)
data_list <- getDataFromDir("~/HealthQ/DUMPED/20150302/Mojo 5089 fixed agc (rf=1000)/", head=T)


##################################################################################################
##################################################################################################
##################################################################################################
##################################################################################################


## discrete ces
data_list <- getDataFromDir("~/HealthQ/DUMPED/20150310/Mojo 5037/", head=T)

## dual biomon box
data_list <- getDataFromDir("~/HealthQ/DUMPED/20150310/Mojo 5098/", head=T)

## dual biomon drop tests
data_list <- getDataFromDir("~/HealthQ/Data/OxySat/20150313/", head=T)


data_list <- getDataFromDir("~/HealthQ/DUMPED/20150323/Tyrael/", head=T)


data_list <- getDataFromDir("~/HealthQ/DUMPED/20150325/Mojo 5172 (biomon 3)/", head=T)

data_list <- getDataFromDir("~/HealthQ/DUMPED/20150326/Mojo 5098/", head=T)

data_list <- getDataFromDir("~/HealthQ/DUMPED/20150402/Goldfinger/", head=T)

data_list <- getDataFromDir("~/HealthQ/DUMPED/20150407/Goldfinger/", head=T)

data_list <- getDataFromDir("~/HealthQ/DUMPED/20150408/Goldfinger/", head=T)


##################################################################################################
##################################################################################################




i <- 5


for(i in 1:len(data_list)){

dat <- data_list[[i]]

dat <- dat[dat[,1] != 0,]
dat <- dat[-c(1:500),]



curr1 <- curr2 <- curr3 <- matrix(0, nr=nrow(dat)-1, nc=4)
rfmat <- isubmat <- matrix(0, nr=nrow(dat)-1, nc=4)
for(j in 1:4){
  if(j < 3){
    Rf <- dat[,9]
    led <- dat[,11]
  }
  else{
    Rf <- dat[,10]
    led <- dat[,j+13]
  }
  
  safe_term <- 0
  if(j == 2){
    safe_term <- 1
    led <- numeric(nrow(dat))
  }
  
  dat_tmp <- dat[-1,j] 
  isub_tmp <- 10*dat[,10+j]
  isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
  Rf <- Rf[1:(len(Rf)-1)]
  led <- led[1:(len(led)-1)]
  curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
  curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)  
  curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)[[1]]
  rfmat[,j] <- Rf
  isubmat[,j] <- isub_tmp
}


zPlot(cbind(curr2[,c(1,3:4)]), c("green LED", "red LED", "infrared LED"), par_new=T, col_f=c("lightblue", "red", "forestgreen"))

# zPlot(cbind(curr2[,c(1,3:4)]-curr2[,2], dat[-1,5:7]), par_new=T, col_f=c("lightblue", "red", "forestgreen", "black", "brown", "orange"))
# zPlot(cbind(curr2[,c(1,3:4)]-curr2[,2], dat[-1,13:14]), par_new=T, col_f=c("lightblue", "red", "forestgreen", "black", "brown", "orange"))
# zPlot(apply(cbind(curr2[,c(1,3:4)]-curr2[,2]), 2, diff), par_new=T, col_f=c("lightblue", "red", "forestgreen"))
# zPlot(cbind(FIRfilt(redI_clipped), FIRfilt(nirI_clipped)), par_new=T)



# pind <- 3500:4500 ;w() ; par(mar=c(4,4,3,4))
# lplot(pind, curr2[pind,3]-curr2[pind,2], lwd=2, col="red",
#       ylab="current (red)", xlab="index", main="LQ-5140 (Kari)")
# par(new=T) ; lplot(pind, curr2[pind,4]-curr2[pind,2], lwd=2, col="blue", 
#                    ylab="", xlab="", xaxt="n", yaxt="n") 
# axis(4) ; mtext("current (IR)", side=4, line=2, cex=par("cex.lab"))
# legend("bottomright", bg="white", col=c("red", "blue"), lwd=2, leg=c("red", "IR"), cex=0.8)

# write.table2(format(cbind(curr2[,c(1,3,4)]-curr2[,2]), digits=12),
#              file=paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/data/RedIRFilt/mojo_breakout_0",i,".txt"))



# redI_clipped <- curr2[,3]-curr2[,2]
# nirI_clipped <- curr2[,4]-curr2[,2] 
# 
# out <- fftSpO2_Cdev(redI_clipped, nirI_clipped, subtractMeans=F, filtType="fir50", adapF=F,
#                     windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=T)
# spo2_1  <- 100*calabFreeSpO2(out[[1]], clip=F)


################################################

# out <- fftSpO2_Cdev(redI_clipped, nirI_clipped, curr2[,1]-curr2[,2], subtractMeans=F, filtType="fir50", adapF=T,
#                     windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F)
# spo2_2  <- 100*calabFreeSpO2(out[[1]], clip=F)

################################################


redI_clipped <- curr3[,3]#-curr3[,2]
nirI_clipped <- curr3[,4]#-curr3[,2]

out <- fftSpO2_Cdev(redI_clipped, nirI_clipped, filtType="butter", adapF=F, smoothACDC=0.5,
                    windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F)
spo2_3  <- 100*calabFreeSpO2(out[[1]], clip=F)


ac_red <- out[[3]][,1] ; dc_red <- out[[3]][,2]
ac_nir <- out[[3]][,3] ; dc_nir <- out[[3]][,4]

################################################

out <- fftSpO2_Cdev(redI_clipped, nirI_clipped, curr3[,1], filtType="butter", adapF=F, smoothACDC=0.8,
                    windowSize=2^9, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F,
                    spo2_caleb_pf=0.6)
spo2_4  <- 100*calabFreeSpO2(out[[1]], clip=T)

################################################


w() ; par(mar=c(4,4,2,3))
lplot(round(movingAve(spo2_4,9),0), lwd=2, col="blue", ylim=c(70,105), ylab="spO2", main=names(data_list)[i])
# abline(h=median(spo2_1), col="blue")
# lines(movingAve(spo2_2,1), lwd=2, col="red") ; abline(h=median(spo2_2), col="red")
# lines(movingAve(spo2_3,1), lwd=2, col="forestgreen") ; abline(h=median(spo2_3), col="forestgreen")
# lines(movingAve(spo2_4,1), lwd=2, col="forestgreen") #; abline(h=median(spo2_4), col="orange")
par(new=T) ; lplot(ac_red, col="red", lty=1, ylim=range(c(ac_red, ac_nir)), ann=F, yaxt="n", xaxt="n")
lines(ac_nir, col="orange", lty=1) ; axis(4)
# par(new=T) ; lplot(dc_red, col="red", lty=1, ylim=range(c(dc_red, dc_nir)), ann=F, yaxt="n", xaxt="n")
# lines(dc_nir, col="orange", lty=1) ; axis(4)

savePlot(paste0("spo2_results/", names(data_list)[[i]], "_spo2result"), "png")
# write.table2(cbind(spo2_3), file=paste0("spo2_results/", names(data_list)[[i]], "_spo2result.txt"))


graphics.off()
}



# fft_red_out <- out[[5]][[1]]
# fft_nir_out <- out[[5]][[2]]
# 
# w() ; par(ask=T, mfrow=c(2,1), mar=c(2,4,2,2))
# for(i in 1:nrow(fft_red_out)){
#   
#   lplot(fft_red_out[i,-1], lwd=2, col="red")
#   legend("top", leg=paste("window", i))
#   abline(v=fft_red_out[i,1], col="blue")
#   
#   lplot(fft_nir_out[i,-1], lwd=2, col="purple")
#   abline(v=fft_nir_out[i,1], col="blue")
#   
# }