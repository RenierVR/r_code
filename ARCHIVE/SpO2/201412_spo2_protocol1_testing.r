source("sp02_funs.r")




##################################################################################################
##################################################################################################


extract_from_data <- function(dat, ind_red, ind_fir, ind_blk, 
                              ind_rf1, ind_rf2, 
                              ind_isr, ind_isf, ind_isb)
{
  red_clipped <- removeAGCJumps(dat[,ind_red], 500)[-1]
  fir_clipped <- removeAGCJumps(dat[,ind_fir], 500)[-1]

  redI <- convertToCurrentV2(dat[,ind_red], dat[,ind_isr]/1000000, dat[,ind_rf1]*1000)*1e11
  firI <- convertToCurrentV2(dat[,ind_fir], dat[,ind_isf]/1000000, dat[,ind_rf1]*1000)*1e11
  blkI <- convertToCurrentV2(dat[,ind_blk], dat[,ind_isb]/1000000, dat[,ind_rf2]*1000)*1e11

  redI_clipped <- round(removeAGCJumps((redI - blkI), 1e3)[-1],0)
  firI_clipped <- round(removeAGCJumps((firI - blkI), 1e3)[-1],0)
  
  return(list(red_clipped=red_clipped, fir_clipped=fir_clipped, 
              redI_clipped=redI_clipped, firI_clipped=firI_clipped))
}



##################################################################################################
##################################################################################################


fpath <- c("~/HealthQ/Data/New Protocol Dec 2014/")

fpath_mojo <- paste0(fpath, c("20141208/Louise/20141208_0802_Louise_4244_Protocol1.csv",
                              "20141208/Louise/20141208_0802_Louise_4245_Protocol1.csv",
                              "20141208/Lara/20141208_0914_Lara_4244_Protocol2.csv",
                              "20141208/Lara/20141208_0914_Lara_4245_Protocol2.csv",
                              "20141208/Scisco/20141208_1331_Scisco_4244_Protocol1.csv",
                              "20141208/Scisco/20141208_1331_Scisco_4246_Protocol1.csv",
                              "20141208/Herman/20141208_1854_Herman_4244_Protocol1.csv",
                              "20141208/Herman/20141208_1854_Herman_4249_Protocol1.csv",
                              "20141209/Marli/20141209_0807_Marli_4249_Protocol1.csv",
                              "20141209/Marli/20141209_0808_Marli_4244_Protocol1.csv",
                              "20141209/Scisco/20141209_1033_Scisco_4244_Protocol1.csv",
                              "20141209/Jacques/20141209_1312_Jacques_4244_Protocol1.csv",
                              "20141210/Phil/20141210_1325_Phil_2132_Protocol1.csv",
                              "20141210/Phil/20141210_1326_Phil_2131_Protocol1.csv",
                              "20141211/Neill/20141211_1152_Neill_2131_Protocol1.csv",
                              "20141211/Neill/20141211_1152_Neill_4244_Protocol1.csv",
                              "20141211/Christo/20141211_1545_Christo_4092_Protocol1.csv",
                              "20141211/Christo/20141211_1545_Christo_4244_Protocol1.csv"))


fpath_ws <- paste0(fpath, c(rep("20141208/Louise/20141208_0802_Louise_Winspiro_Rest",2),
                            rep("20141208/Lara/20141208_0914_Lara_Winspiro_Rest",2),
                            rep("20141208/Scisco/20141208_1331_Scisco_Winspiro",2),
                            rep("20141208/Herman/20141208_1854_Herman_Winspiro",2),
                            rep("20141209/Marli/20141209_0814_Marli_Winspiro", 2),
                            "20141209/Scisco/20141209_1027_Scisco_Winspiro",
                            rep("20141209/Jacques/20141209_1310_Jacques_Winspiro", 1),
                            rep("20141210/Phil/20141210_1319_Phil_Winspiro",2),
                            rep("20141211/Neill/20141211_Neill_Protocol1_Winspiro",2),
                            rep("20141211/Christo/20141211_1545_Christo_Winspiro",2)))
fpath_ws_beg <- paste0(fpath_ws, "_Beg.csv")
fpath_ws_end <- paste0(fpath_ws, "_End.csv")                                    
                                    


##########################################################################################

red_fir_order_mat <- c(0, 1, 1, 0, 2, 2, 
                       2, 1, 0, 0, 0, 0, 
                       1, 1, 2, 0, 0, 0)


index_mat <- matrix(byrow=TRUE, nc=4, nr=len(fpath_mojo),
                    c(3000, 18000, 4000, 19000,            # 5
                      5000, 19000, 5000, 18000,            # 6
                      8000, 20000, 4000, 19000,            # 3
                      7000, 20000, 4000, 19000,            # 4
                      2000, 17500, 5000, 18000,            # 7
                      3500, 21000, 1, 20000,               # 8
                      1, 20000, 5500, 18000,               # 1
                      5500, 19000, 5000, 19000,            # 2
                      3000, 17500, 5000, 20000,            # 11
                      6000, 14000, 5000, 16500,            # 12
                      2000, 17500, 9000, 18000,            # 13
                      3000, 17500, 4500, 19500,            # 10
                      4000, 19000, 3000, 13000,            # 14  
                      2000, 17500, 500, 13500,             # 15
                      4000, 19000, 4000, 19500,            # 18
                      8000, 23000, 3500, 19000,            # 19
                      2500, 18000, 5000, 19000,            # 16
                      4000, 19000, 3000, 18000))           # 17

##  ignore index 9 (very low SQ)

i <- 7

# all
indexes <- (1:18)

#good
indexes <- c(3, 4, 5, 6)

#bad
indexes <- c(7, 8, 1, 2)


#clear red/ir ordering
indexes <- (1:18)[red_fir_order_mat != 2]


write_output <- TRUE
for(i in indexes){
  cat("in progress: ", round(100*(which(i == indexes)-1)/length(indexes),2), "% ...\n")
  
  
  i <- 8
  
  dat <- read.table(fpath_mojo[i], sep=",", head=T)
  n <- nrow(dat)

#   dat1 <- dat[1:25000, c(1:4,9:14)]
#   dat1 <- dat1[index_mat[i,1]:index_mat[i,2],]
#   dat2 <- dat[(n-20000):n, c(1:4,9:14)]
#   dat2 <- dat2[index_mat[i,3]:index_mat[i,4],]
#   rm(dat)


  dat <- dat[dat[,1] != 0,]
  curr1 <- curr2 <- matrix(0, nr=nrow(dat)-1, nc=4)
  rfmat <- isubmat <- matrix(0, nr=nrow(dat)-1, nc=4)
  for(j in 1:4){
    if(j < 3)
      Rf <- dat[,9]
    else
      Rf <- dat[,10]
    
    safe_term <- 0
    if(j == 2) safe_term <- 1
    
    dat_tmp <- dat[-1,j] 
    isub_tmp <- 10*dat[,10+j]
    isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
    Rf <- Rf[1:(len(Rf)-1)]
    curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
    curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp)[[1]]
    
    rfmat[,j] <- Rf
    isubmat[,j] <- isub_tmp
  }
  

zPlot(cbind(dat[,15:17]))
zPlot(cbind(dat[,c(1,3:4)]), par_new=T, col_f=c("lightblue", "red", "forestgreen"))

zPlot(cbind(curr1[,1], curr1[,2]), par_new=T)




##########################################################################################


  red1 <- ext1$firI_clipped
  fir1 <- ext1$redI_clipped
  red2 <- ext2$firI_clipped
  fir2 <- ext2$redI_clipped
  if(red_fir_order_mat[i] == 1){
    red1 <- ext1$redI_clipped
    fir1 <- ext1$firI_clipped
    red2 <- ext2$redI_clipped
    fir2 <- ext2$firI_clipped
  }

  out1 <- fftSpO2(red1, fir1, grn1, subtractMeans=F, filtType="fir50", windowSize=2^8, 
                  fftL=1, fftI=T, interpBW=1, useMaxDenom=T)
  spo21 <- 100*calabFreeSpO2(out1[[1]], pF=0.75, logBias=0, smoothAlpha=0.8)
  spo2_ws_beg <- read.table(fpath_ws_beg[i], sep=",", head=T)[,3]

  out2 <- fftSpO2(red2, fir2, grn2, subtractMeans=F, filtType="fir50", windowSize=2^8, 
                  fftL=1, fftI=T, interpBW=1, useMaxDenom=T)
  spo22 <- 100*calabFreeSpO2(out2[[1]], pF=0.75, logBias=0, smoothAlpha=0.8)
  spo2_ws_end <- read.table(fpath_ws_end[i], sep=",", head=T)[,3]

  spo21 <- movingAve(spo21, 30)
  spo22 <- movingAve(spo22, 30)


  w() ; par(mar=c(2,4,1,1), mfrow=c(2,1))
  lplot(spo21, ylim=c(75,100), lwd=2, ylab=paste0("spo2 (begin : ", i, ") "))
  lines(spo2_ws_beg[-c(1:25)], lwd=2, col="red")

  lplot(spo22, ylim=c(75,100), lwd=2, ylab=paste0("spo2 (end : ", i, ") "))
  lines(spo2_ws_end[-c(1:25)], lwd=2, col="red")

#   legend("bottomright", lwd=1, col=c("forestgreen", "red"),
#          leg=c("spo2", "winspiro"))

  if(write_output){
    fname <- basename(fpath_mojo[i])
    fname <- substr(fname, 1, nchar(fname)-4)
    savePlot(paste0("spo2_results/", fname, "_logic11"), type="png")
    graphics.off()
  }

  if(i == indexes[length(indexes)])
    cat("completed\n")

}









