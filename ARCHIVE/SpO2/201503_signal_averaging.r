source("../useful_funs.r")
source("./sp02_funs.r")
library(signal)


dat <- getDataFromDir("~/HealthQ/DUMPED/20150324/Mojo 5143/", head=T)[[8]]

dat <- dat[dat[,1] != 0,]
dat <- dat[-c(1:500),]

curr1 <- curr2 <- curr3 <- matrix(0, nr=nrow(dat)-1, nc=4)
rfmat <- isubmat <- matrix(0, nr=nrow(dat)-1, nc=4)
for(j in 1:4){
  if(j < 3){
    Rf <- dat[,9]
    led <- dat[,11]
  }
  else{
    Rf <- dat[,10]
    led <- dat[,j+13]
  }
  
  safe_term <- 0
  if(j == 2){
    safe_term <- 1
    led <- numeric(nrow(dat))
  }
  
  dat_tmp <- dat[-1,j] 
  isub_tmp <- 10*dat[,10+j]
  isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
  Rf <- Rf[1:(len(Rf)-1)]
  led <- led[1:(len(led)-1)]
  curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
  curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)  
  curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)[[1]]
  rfmat[,j] <- Rf
  isubmat[,j] <- isub_tmp
}


zPlot(cbind(curr2[,c(1,3:4)]-curr2[,2]), par_new=T, col_f=c("lightblue", "red", "forestgreen"))



##################################################################################################
##################################################################################################

grn <- curr3[,1]
grn_out <- ER_MA_DT_peaks_C(grn, "FIR50", output_all=T)
grn_peaks <- grn_out$peaks-25
grn_filt <- grn_out$filt_signal


# pind <- 5000:6000
# w() ; lplot(pind, curr3[,1][pind], lwd=2, col="forestgreen")
# abline(v=grn_peaks, col="red")



grn_peaks <- grn_peaks[grn_peaks > 500]
grn_peaks <- grn_peaks[round(grn_peaks) != 2605]

n <- 70
grn_seg <- red_seg <- nir_seg <- numeric(60)
w() ; par(mfrow=c(3,1), mar=c(2,4,1,1), ask=T)
for(i in 2:len(grn_peaks)){
  
  grn_seg_new <- curr3[(round(grn_peaks[i-1])-10):(round(grn_peaks[i])+5),1]
  red_seg_new <- curr3[(round(grn_peaks[i-1])-10):(round(grn_peaks[i])+5),3]
  nir_seg_new <- curr3[(round(grn_peaks[i-1])-10):(round(grn_peaks[i])+5),4]
  
  grn_seg_new <- grn_seg_new - mean(grn_seg_new)
  red_seg_new <- red_seg_new - mean(red_seg_new)
  nir_seg_new <- nir_seg_new - mean(nir_seg_new)

  grn_seg_new <- resample(grn_seg_new, n/len(grn_seg_new))[-c(1:10)]
  red_seg_new <- resample(red_seg_new, n/len(red_seg_new))[-c(1:10)]
  nir_seg_new <- resample(nir_seg_new, n/len(nir_seg_new))[-c(1:10)]
  
  grn_seg <- grn_seg + grn_seg_new
  red_seg <- red_seg + red_seg_new
  nir_seg <- nir_seg + nir_seg_new
  
  lplot(grn_seg/(i-1), lwd=3, col="mediumseagreen") ; lines(grn_seg_new, col="blue")
  legend("top", leg=paste("interpeak segment", i-1))
  lplot(red_seg/(i-1), lwd=3, col="red") ; lines(red_seg_new, col="blue")
  lplot(nir_seg/(i-1), lwd=3, col="orange") ; lines(nir_seg_new, col="blue")
  
#   Sys.sleep(0.5)
}




