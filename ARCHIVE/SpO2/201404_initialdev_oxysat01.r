source("~/Studies/RESEARCH/BAYESIAN SURVIVAL ANALYSIS/Misc/simplot.r")

#########################################################################
#########    DIRK SP02 TESTING DATA 26/03/2014   ########################
#########################################################################

mojo <- read.table("~/HealthQ/Data/OxySat/20140326_ModC10_Testing_OxSatSingleCh_50mA_001.txt", sep=",")
wspiro <- read.table("~/HealthQ/Data/OxySat/20140326_winspiro_testdata.txt", sep=",")

m_nir <- mojo[,3]
m_red <- mojo[,4]

wmultiplot(500,250,1,m_red)



a1 <- as.numeric(diff(m_nir, diff=2))
a1 <- ksmooth(1:length(a1), a1, "normal", bandw=5)$y

a2 <- ksmooth(1:length(m_nir), m_nir, "normal", bandw=5)$y
a2 <- as.numeric(diff(a2, diff=2))

aa = cbind(a1,a2)
aa = aa[30:(nrow(aa)-30),]
r = 8
all(round((aa[,1] - aa[,2]),r) == 0)


######################

red_pks <- read.table("~/HealthQ/C FILES/OxySat_project/output/nir_red/red_pksid_out_modc_20140326.txt.txt", sep=",")
red_trs <- read.table("~/HealthQ/C FILES/OxySat_project/output/nir_red/red_troughs_out_modc_20140326.txt.txt", sep=",")
nir_pks <- read.table("~/HealthQ/C FILES/OxySat_project/output/nir_red/nir_pksid_out_modc_20140326.txt.txt", sep=",")
nir_trs <- read.table("~/HealthQ/C FILES/OxySat_project/output/nir_red/nir_troughs_out_modc_20140326.txt.txt", sep=",")


red_ind <- (1:nrow(red_pks))[red_pks[,1] > red_trs[1,1]][1]-1
nir_ind <- (1:nrow(nir_pks))[nir_pks[,1] > nir_trs[1,1]][1]-1


## interpolation for red peaks ##
red_pks_interp <- NULL
for(i in red_ind:(nrow(red_pks)-1)){
  tmp <- red_pks[i:(i+1),]
  red_pks_interp <- c(red_pks_interp, approx(tmp, xout=tmp[1,1]:(tmp[2,1]-1))$y)
}
red_pks_interp <- cbind(red_pks[red_ind,1]:(red_pks[nrow(red_pks),1]-1), red_pks_interp)
  

## interpolation for red troughs ##
red_trs_interp <- NULL
for(i in 1:(nrow(red_trs)-1)){
  tmp <- red_trs[i:(i+1),]
  red_trs_interp <- c(red_trs_interp, approx(tmp, xout=tmp[1,1]:(tmp[2,1]-1))$y)
}
red_trs_interp <- cbind(red_trs[1,1]:(red_trs[nrow(red_trs),1]-1), red_trs_interp)


## interpolation for nir peaks ##
nir_pks_interp <- NULL
for(i in nir_ind:(nrow(nir_pks)-1)){
  tmp <- nir_pks[i:(i+1),]
  nir_pks_interp <- c(nir_pks_interp, approx(tmp, xout=tmp[1,1]:(tmp[2,1]-1))$y)
}
nir_pks_interp <- cbind(nir_pks[nir_ind,1]:(nir_pks[nrow(nir_pks),1]-1), nir_pks_interp)


## interpolation for nir troughs ##
nir_trs_interp <- NULL
for(i in 1:(nrow(nir_trs)-1)){
  tmp <- nir_trs[i:(i+1),]
  nir_trs_interp <- c(nir_trs_interp, approx(tmp, xout=tmp[1,1]:(tmp[2,1]-1))$y)
}
nir_trs_interp <- cbind(nir_trs[1,1]:(nir_trs[nrow(nir_trs),1]-1), nir_trs_interp)


#######################################

rpi <- red_pks_interp
rti <- red_trs_interp
npi <- nir_pks_interp
nti <- nir_trs_interp
rm(red_pks_interp, red_trs_interp, nir_pks_interp, nir_trs_interp)


red_min <- max(rpi[1,1], rti[1,1])
red_max <- min(rpi[nrow(rpi),1],rti[nrow(rti),1])
nir_min <- max(npi[1,1], nti[1,1])
nir_max <- min(npi[nrow(npi),1],nti[nrow(nti),1])

rn_min <- max(red_min, nir_min)
rn_max <- min(red_max, nir_max)

rpi2 <- rpi[which(rpi[,1] == rn_min):which(rpi[,1] == rn_max),]
rti2 <- rti[which(rti[,1] == rn_min):which(rti[,1] == rn_max),]
npi2 <- npi[which(npi[,1] == rn_min):which(npi[,1] == rn_max),]
nti2 <- nti[which(nti[,1] == rn_min):which(nti[,1] == rn_max),]



xx = rpi2 ; yy = rti2
windows(14,7)
winsize=1000 ; overlap=250 ; start=1
par(ask = TRUE, bg="grey80", fg="white")
j = 0
while(TRUE){
  j = j+1
  plotind <- (overlap*(j-1)+start):(start-1+winsize + overlap*(j-1))
  if(max(plotind) > rn_max)
    break
  else{
    ylm <- range(c(xx[plotind,2], yy[plotind,2]))
    plot(xx[plotind,], type="l", lwd=2, col="blue", ylim=ylm)
    lines(yy[plotind,], lwd=2, col="seagreen")
  }
}

#######################################


red_ac <- rpi2[,2] - rti2[,2]
red_dc <- apply(cbind(rpi2[,2], rti2[,2]), 1, mean)
nir_ac <- npi2[,2] - nti2[,2]
nir_dc <- apply(cbind(npi2[,2], nti2[,2]), 1, mean)


#R <- (red_ac/red_dc) / (nir_ac/nir_dc)
#R[R>5 | R< -5] <- 0

R <- red_ac/nir_ac
R[R>1 | R< -1] <- 0

#R_sm <- ksmooth(1:length(R), R, "normal", bandw=101)$y

# R_sm <- as.numeric(filter(R, rep(1/101,101)))
# R_sm <- as.numeric(filter(R_sm, rep(1/101,101)))
# R_sm <- as.numeric(filter(R_sm, rep(1/101,101)))
# R_sm <- as.numeric(filter(R_sm, rep(1/101,101)))

R_sm <- as.numeric(filter(R, rep(1/1501,1001)))
R_sm <- as.numeric(filter(R_sm, rep(1/501,101)))
R_sm <- as.numeric(filter(R_sm, rep(1/501,501)))



R_tr <- a-b*R_sm

w(14,7) ; lplot(R_tr)
par(new=T) ; lplot(wspiro[,1], col="red", yaxt="n")



#########################################################################
#########    VANZYL SP02 TESTING DATA 02/04/2014   ######################
#########################################################################

wspiro <- read.table("~/HealthQ/Data/OxySat/20140402/20140402_winspiro_testing.txt", sep=",")
mojo_conv <- read.table("~/HealthQ/Data/OxySat/20140402/20140402_ModC10_Testing_OxSatSingleCh_50mA_Autogain_001.csv", head=T, sep=",")


red_pks <- read.table("~/HealthQ/C FILES/OxySat_project/output/nir_red/red_pksid_out_modc_20140402_nir+red.csv.txt", sep=",")
red_trs <- read.table("~/HealthQ/C FILES/OxySat_project/output/nir_red/red_troughs_out_modc_20140402_nir+red.csv.txt", sep=",")
nir_pks <- read.table("~/HealthQ/C FILES/OxySat_project/output/nir_red/nir_pksid_out_modc_20140402_nir+red.csv.txt", sep=",")
nir_trs <- read.table("~/HealthQ/C FILES/OxySat_project/output/nir_red/nir_troughs_out_modc_20140402_nir+red.csv.txt", sep=",")


red_ind <- (1:nrow(red_pks))[red_pks[,1] > red_trs[1,1]][1]-1
nir_ind <- (1:nrow(nir_pks))[nir_pks[,1] > nir_trs[1,1]][1]-1


## interpolation for red peaks ##
red_pks_interp <- NULL
for(i in red_ind:(nrow(red_pks)-1)){
  tmp <- red_pks[i:(i+1),]
  red_pks_interp <- c(red_pks_interp, approx(tmp, xout=tmp[1,1]:(tmp[2,1]-1))$y)
}
red_pks_interp <- cbind(red_pks[red_ind,1]:(red_pks[nrow(red_pks),1]-1), red_pks_interp)


## interpolation for red troughs ##
red_trs_interp <- NULL
for(i in 1:(nrow(red_trs)-1)){
  tmp <- red_trs[i:(i+1),]
  red_trs_interp <- c(red_trs_interp, approx(tmp, xout=tmp[1,1]:(tmp[2,1]-1))$y)
}
red_trs_interp <- cbind(red_trs[1,1]:(red_trs[nrow(red_trs),1]-1), red_trs_interp)


## interpolation for nir peaks ##
nir_pks_interp <- NULL
for(i in nir_ind:(nrow(nir_pks)-1)){
  tmp <- nir_pks[i:(i+1),]
  nir_pks_interp <- c(nir_pks_interp, approx(tmp, xout=tmp[1,1]:(tmp[2,1]-1))$y)
}
nir_pks_interp <- cbind(nir_pks[nir_ind,1]:(nir_pks[nrow(nir_pks),1]-1), nir_pks_interp)


## interpolation for nir troughs ##
nir_trs_interp <- NULL
for(i in 1:(nrow(nir_trs)-1)){
  tmp <- nir_trs[i:(i+1),]
  nir_trs_interp <- c(nir_trs_interp, approx(tmp, xout=tmp[1,1]:(tmp[2,1]-1))$y)
}
nir_trs_interp <- cbind(nir_trs[1,1]:(nir_trs[nrow(nir_trs),1]-1), nir_trs_interp)


#######################################

rpi <- red_pks_interp
rti <- red_trs_interp
npi <- nir_pks_interp
nti <- nir_trs_interp
rm(red_pks_interp, red_trs_interp, nir_pks_interp, nir_trs_interp)


red_min <- max(rpi[1,1], rti[1,1])
red_max <- min(rpi[nrow(rpi),1],rti[nrow(rti),1])
nir_min <- max(npi[1,1], nti[1,1])
nir_max <- min(npi[nrow(npi),1],nti[nrow(nti),1])

rn_min <- max(red_min, nir_min)
rn_max <- min(red_max, nir_max)

rpi2 <- rpi[which(rpi[,1] == rn_min):which(rpi[,1] == rn_max),]
rti2 <- rti[which(rti[,1] == rn_min):which(rti[,1] == rn_max),]
npi2 <- npi[which(npi[,1] == rn_min):which(npi[,1] == rn_max),]
nti2 <- nti[which(nti[,1] == rn_min):which(nti[,1] == rn_max),]


red_ac <- rpi2[,2] - rti2[,2]h
red_dc <- apply(cbind(rpi2[,2], rti2[,2]), 1, mean)
nir_ac <- npi2[,2] - nti2[,2]
nir_dc <- apply(cbind(npi2[,2], nti2[,2]), 1, mean)



R <- (red_ac/red_dc)/(nir_ac/nir_dc)
R[R>1 | R< -1] <- 0



R_sm <- as.numeric(filter(R, rep(1/1501,1001)))
R_sm <- as.numeric(filter(R_sm, rep(1/501,101)))
R_sm <- as.numeric(filter(R_sm, rep(1/501,501)))



R_tr <- a-b*R_sm

w(14,7) ; lplot(R_sm)
par(new=T) ; lplot(wspiro[,1], col="red", yaxt="n")

