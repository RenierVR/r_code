


##########################################################################################################
################                    TOM-TOM                    ###########################################   
##########################################################################################################

dat <- read.table("clipboard")


dat <- dat[dat[,1] != 0,]
dat <- dat[-c(1:500),]

curr1 <- curr2 <- curr3 <- matrix(0, nr=nrow(dat)-1, nc=4)
rfmat <- isubmat <- matrix(0, nr=nrow(dat)-1, nc=4)
for(j in 1:4){
  if(j < 3){
    Rf <- dat[,9]
    led <- dat[,11]
  }
  else{
    Rf <- dat[,10]
    led <- dat[,j+9]
  }
  
  safe_term <- 0
  if(j == 2){
    safe_term <- 1
    led <- numeric(nrow(dat))
  }
  
  dat_tmp <- dat[-1,j] 
  isub_tmp <- 10*dat[,4+j]
  isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
  Rf <- Rf[1:(len(Rf)-1)]
  led <- led[1:(len(led)-1)]
  curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
  curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)  
  curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)[[1]]
  rfmat[,j] <- Rf
  isubmat[,j] <- isub_tmp
}


zPlot(cbind(curr2[,c(1,3:4)]-curr2[,2]), c("green LED", "red LED", "IR LED"),
      par_new=T, col_f=c("lightblue", "red", "forestgreen"))


redI_clipped <- curr3[,3]-curr3[,2]
nirI_clipped <- curr3[,4]-curr3[,2] 

out <- fftSpO2_Cdev(redI_clipped, nirI_clipped, filtType="fir50", adapF=F,
                    windowSize=2^8, fftL=0, logicSD=15, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=T)
spo2_3  <- 100*calabFreeSpO2(out[[1]], clip=F)





w() ; par(mar=c(4,4,2,3))
lplot(round(movingAve(spo2_3,3),2), lwd=2, col="blue", ylim=c(70,110), ylab="spo2", xlab="time (s)",
      main="Black Putty (Anastasia)")
abline(h=median(spo2_3), lty=2, col="purple")
legend("bottomleft", lty=c(1,2), col=c("blue", "purple"), bg="white",
       leg=c("LifeQ SpO2", paste0("LifeQ SpO2 median (", round(median(spo2_3),1),")")))



##########################################################################################################
################                    PARTRON                    ###########################################   
##########################################################################################################



dat <- read.table("clipboard")
dat <- dat[,c(1,4,2,3,13:17,19,10:12)]


dat <- dat[dat[,1] != 0,]
dat <- dat[-c(1:500),]

curr1 <- curr2 <- curr3 <- matrix(0, nr=nrow(dat)-1, nc=4)
rfmat <- isubmat <- matrix(0, nr=nrow(dat)-1, nc=4)
for(j in 1:4){
  if(j < 3){
    Rf <- dat[,9]
    led <- dat[,11]
  }
  else{
    Rf <- dat[,10]
    led <- dat[,j+9]
  }
  
  safe_term <- 0
  if(j == 2){
    safe_term <- 1
    led <- numeric(nrow(dat))
  }
  
  dat_tmp <- dat[-1,j] 
  isub_tmp <- 10*dat[,4+j]
  isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
  Rf <- Rf[1:(len(Rf)-1)]
  led <- led[1:(len(led)-1)]
  curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
  curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)  
  curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)[[1]]
  rfmat[,j] <- Rf
  isubmat[,j] <- isub_tmp
}


zPlot(cbind(curr2[,c(1,3:4)]-curr2[,2]), c("green LED", "red LED", "IR LED"),
      par_new=T, col_f=c("lightblue", "red", "forestgreen"))


redI_clipped <- curr3[,3]-curr3[,2]
nirI_clipped <- curr3[,4]-curr3[,2] 

out <- fftSpO2_Cdev(nirI_clipped, redI_clipped, filtType="fir50", adapF=F,
                    windowSize=2^8, fftL=0, logicSD=15, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=T)
spo2_3  <- 100*calabFreeSpO2(out[[1]], clip=F)





w() ; par(mar=c(4,4,2,3))
lplot(round(movingAve(spo2_3,3),2), lwd=2, col="blue", ylim=c(70,110), ylab="spo2", xlab="time (s)")
abline(h=median(spo2_3), lty=2, col="purple")
legend("bottomleft", lty=c(1,2), col=c("blue", "purple"), bg="white",
       leg=c("LifeQ SpO2", paste0("LifeQ SpO2 median (", round(median(spo2_3),1),")")))




##########################################################################################################
################                    PARTRON                    ###########################################   
##########################################################################################################


dat <- read.table("clipboard")
dat <- dat[,c(1,2,3,4,17,18,20,19,12,13,14,16,15)]


dat <- dat[dat[,1] != 0,]
dat <- dat[-c(1:500),]

curr1 <- curr2 <- curr3 <- matrix(0, nr=nrow(dat)-1, nc=4)
rfmat <- isubmat <- matrix(0, nr=nrow(dat)-1, nc=4)
for(j in 1:4){
  if(j < 3){
    Rf <- dat[,9]
    led <- dat[,11]
  }
  else{
    Rf <- dat[,10]
    led <- dat[,j+9]
  }
  
  safe_term <- 0
  if(j == 2){
    safe_term <- 1
    led <- numeric(nrow(dat))
  }
  
  dat_tmp <- dat[-1,j] 
  isub_tmp <- 10*dat[,4+j]
  isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
  Rf <- Rf[1:(len(Rf)-1)]
  led <- led[1:(len(led)-1)]
  curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
  curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)  
  curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)[[1]]
  rfmat[,j] <- Rf
  isubmat[,j] <- isub_tmp
}


zPlot(cbind(curr2[,c(1,3:4)]-curr2[,2]), c("green LED", "red LED", "IR LED"),
      par_new=T, col_f=c("lightblue", "red", "forestgreen"))


redI_clipped <- curr3[,3]-curr3[,2]
nirI_clipped <- curr3[,4]-curr3[,2] 

out <- fftSpO2_Cdev(redI_clipped, nirI_clipped, filtType="fir50", adapF=F,
                    windowSize=2^8, fftL=0, logicSD=15, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=T)
spo2_3  <- 100*calabFreeSpO2(out[[1]], clip=F)





w() ; par(mar=c(4,4,2,3))
lplot(round(movingAve(spo2_3,3),2), lwd=2, col="blue", ylim=c(70,110), ylab="spo2", xlab="time (s)")
abline(h=median(spo2_3), lty=2, col="purple")
legend("bottomleft", lty=c(1,2), col=c("blue", "purple"), bg="white",
       leg=c("LifeQ SpO2", paste0("LifeQ SpO2 median (", round(median(spo2_3),1),")")))
