source("../useful_funs.r")
source("./sp02_funs.r")




## dual biomon drop tests
data_wins <- getDataFromDir("~/HealthQ/Data/OxySat/March Winspiro Tests/Winspiro/", head=T)
data_mojo <- getDataFromDir("~/HealthQ/Data/OxySat/March Winspiro Tests/", head=T)



## 'goldfinger' clamp drop tests
data_wins <- getDataFromDir("~/HealthQ/Data/OxySat/April Winspiro Tests/Winspiro/", head=T)
data_mojo <- getDataFromDir("~/HealthQ/Data/OxySat/April Winspiro Tests/", head=T)

##################################################################################################
##################################################################################################




i <- 1


for(i in 1:len(data_mojo)){
# for(i in 8:9){

dat <- data_mojo[[i]]
wns_plot <- data_wins[[i]][,3]

dat <- dat[dat[,1] != 0,]
dat <- dat[-c(1:500),]

curr1 <- curr2 <- curr3 <- matrix(0, nr=nrow(dat)-1, nc=4)
rfmat <- isubmat <- matrix(0, nr=nrow(dat)-1, nc=4)
for(j in 1:4){
  if(j < 3){
    Rf <- dat[,9]
    led <- dat[,11]
  }
  else{
    Rf <- dat[,10]
    led <- dat[,j+13]
  }
  
  safe_term <- 0
  if(j == 2){
    safe_term <- 1
    led <- numeric(nrow(dat))
  }
  
  dat_tmp <- dat[-1,j] 
  isub_tmp <- 10*dat[,10+j]
  isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
  Rf <- Rf[1:(len(Rf)-1)]
  led <- led[1:(len(led)-1)]
  curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
  curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)  
  curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)[[1]]
  rfmat[,j] <- Rf
  isubmat[,j] <- isub_tmp
}


# zPlot(cbind(curr2[,c(1,3:4)]-curr2[,2]), par_new=T, col_f=c("lightblue", "red", "forestgreen"))
# zPlot(apply(cbind(curr2[,c(1,3:4)]-curr2[,2]), 2, diff), par_new=T, col_f=c("lightblue", "red", "forestgreen"))
# zPlot(cbind(FIRfilt(redI_clipped), FIRfilt(nirI_clipped)), par_new=T)



# pind <- 3500:4500 ;w() ; par(mar=c(4,4,3,4))
# lplot(pind, curr2[pind,3]-curr2[pind,2], lwd=2, col="red",
#       ylab="current (red)", xlab="index", main="LQ-5140 (Kari)")
# par(new=T) ; lplot(pind, curr2[pind,4]-curr2[pind,2], lwd=2, col="blue", 
#                    ylab="", xlab="", xaxt="n", yaxt="n") 
# axis(4) ; mtext("current (IR)", side=4, line=2, cex=par("cex.lab"))
# legend("bottomright", bg="white", col=c("red", "blue"), lwd=2, leg=c("red", "IR"), cex=0.8)

# write.table2(format(cbind(curr2[,c(1,3,4)]-curr2[,2]), digits=12),
#              file=paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/data/RedIRFilt/mojo_breakout_0",i,".txt"))



# redI_clipped <- curr2[,3]-curr2[,2]
# nirI_clipped <- curr2[,4]-curr2[,2] 
# 
# out <- fftSpO2_Cdev(redI_clipped, nirI_clipped, subtractMeans=F, filtType="fir50", adapF=F,
#                     windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=T)
# spo2_1  <- 100*calabFreeSpO2(out[[1]], clip=F)
# ################################################
# 
# out <- fftSpO2_Cdev(redI_clipped, nirI_clipped, curr2[,1]-curr2[,2], subtractMeans=F, filtType="fir50", aNFilt=T,
#                     windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F)
# spo2_2  <- 100*calabFreeSpO2(out[[1]], clip=F)
# 
# ################################################

redI_clipped <- curr3[,3]#-curr3[,2]
nirI_clipped <- curr3[,4]#-curr3[,2] 

out <- fftSpO2_Cdev(redI_clipped, nirI_clipped, filtType="butter", adapF=F, smoothACDC=0.8,
                    windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F)
spo2_3  <- 100*calabFreeSpO2(out[[1]], clip=F)

# qm <- 100*out[[4]][,7:8]
# qm_smooth <- qm
# for(j in 2:nrow(qm))
#   qm_smooth[j,] <- 0.3*qm_smooth[j-1,] + (1-0.3)*qm[j,]

ac_red <- out[[3]][,1] ; dc_red <- out[[3]][,2]
ac_nir <- out[[3]][,3] ; dc_nir <- out[[3]][,4]



################################################

out <- fftSpO2_Cdev(redI_clipped, nirI_clipped, curr3[,1]-curr3[,2], filtType="butter", adapF=F, smoothACDC=0.8,
                    windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F,
                    spo2_caleb_pf=0.6)
spo2_4  <- 100*calabFreeSpO2(out[[1]], clip=F)

################################################




w() ; par(mar=c(4,4,2,3))
lplot(wns_plot, lwd=3, col="red", ylim=c(70,100), ylab="spo2", main=names(data_mojo)[i])
lines(round(movingAve(spo2_3,1),2), lwd=2, col="blue")
lines(round(movingAve(spo2_4,1),2), lwd=2, col="forestgreen")
par(new=T) ; lplot(ac_red, col="red", lty=1, ylim=range(c(ac_red, ac_nir)), ann=F, yaxt="n", xaxt="n")
lines(ac_nir, col="orange", lty=1) ; axis(4)




# savePlot(paste0("spo2_results/", names(data_mojo)[[i]], "_spo2result"), "png")
# write.table2(cbind(spo2_3), file=paste0("spo2_results/", names(data_mojo)[[i]], "_spo2result.txt"))

savePlot(paste0("spo2_results/", names(data_mojo)[i]), "png")
graphics.off() ; cat(round(100*i/len(data_mojo), 1), "% completed\n")
}