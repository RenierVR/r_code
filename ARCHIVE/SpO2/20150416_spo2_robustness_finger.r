

source("../useful_funs.r")
source("./sp02_funs.r")


# fpath1 <- "G:/TESTING/SpO2 Robustness Finger 20150416/"
fpath1 <- "~/HealthQ/Data/OxySat/SpO2 Robustness Finger 20150416/"

data_wins <- getDataFromDir(paste0(fpath1, "Winspiro/"), head=T)
data_mojo1 <- getDataFromDir(paste0(fpath1, "Goldfinger/"), head=T, dataE=F)
data_mojo2 <- getDataFromDir(paste0(fpath1, "Mojo 5172 (finger)/"), head=T, dataE=F)
data_mojo3 <- getDataFromDir(paste0(fpath1, "Mojo 5173 (wrist)/"), head=T, dataE=F)


# data_mojo2 <- getDataFromDir("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/data/SpO2FingerTests2/", head=T)



############################################################################################
############################################################################################


offset_mat <- matrix(c(22, 1,    # 1
                       1, 2,    # 2
                       1, 1,    # 3
                       10, 1,    # 4
                       25, 1,    # 5
                       3, 1,    # 6
                       28, 1,    # 7
                       1, 28,    # 8
                       1, 21,    # 9
                       9, 1,    # 10
                       1, 5,    # 11
                       1, 11,    # 12
                       54, 1,    # 13
                       57, 1,    # 14
                       1, 20,    # 15
                       22, 1,    # 16
                       10, 1,    # 17
                       8, 1,    # 18
                       26, 1,    # 19
                       21, 1,    # 20
                       15, 1,    # 21
                       5, 1), nrow=22, byrow=T)



plotting <- FALSE
include_mojo1 <- TRUE
include_mojo2 <- TRUE
include_mojo3 <- TRUE

i <- 1
# performance_mat <- matrix(0, nr=len(data_wins), nc=3)
for(i in 1:len(data_wins)){
  
  wins <- data_wins[[i]][,3]
  
if(include_mojo1){
  dat <- read.table(paste0(fpath1, "/Goldfinger/", data_mojo1[i]), sep=",", head=T)
  
  dat <- dat[dat[,1] != 0,]
  dat <- dat[1000:(nrow(dat)-1000),]
  curr1 <- curr2 <- curr3 <- matrix(0, nr=nrow(dat), nc=4)
  for(j in 1:4){
    if(j < 3){
      Rf <- dat[,8]
      led <- dat[,14]
    }
    else{
      Rf <- dat[,9]
      led <- dat[,j+12]
    }
    
    safe_term <- 0
    if(j == 2){
      safe_term <- 1
      led <- numeric(nrow(dat))
    }
    
    dat_tmp <- dat[,j] 
    isub_tmp <- dat[,9+j]
    curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
    curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)  
    curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)[[1]]
  }
  

  redI_blank_clipped1 <- removeCurrentDiscont2_m(curr1[,3]-curr1[,2], dat[,8]+dat[,9], dat[,12]+dat[,11], dat[,15], 1e8)[[1]]
  nirI_blank_clipped1 <- removeCurrentDiscont2_m(curr1[,4]-curr1[,2], dat[,8]+dat[,9], dat[,13]+dat[,11], dat[,16], 1e8)[[1]]
  

  
if(plotting){
  zPlot(curr3[,c(1,3,4)], par_new=T, col_f=c("seagreen", "red", "blue"))
  zPlot(cbind(FIRfilt(redI_blank_clipped1), FIRfilt(nirI_blank_clipped1)))
}

}
  
  
  #####################################################################
  
if(include_mojo2){
  
  dat <- read.table(paste0(fpath1, "/Mojo 5172 (finger)/", data_mojo2[i]), sep=",", head=T)

dat <- dat[dat[,1] != 0,]
dat <- dat[1000:(nrow(dat)-1000),]
curr1 <- curr2 <- curr3 <- matrix(0, nr=nrow(dat), nc=4)
for(j in 1:4){
  if(j < 3){
    Rf <- dat[,8]
    led <- dat[,14]
  }
  else{
    Rf <- dat[,9]
    led <- dat[,j+12]
  }
  
  safe_term <- 0
  if(j == 2){
    safe_term <- 1
    led <- numeric(nrow(dat))
  }
  
  dat_tmp <- dat[,j] 
  isub_tmp <- dat[,9+j]
  curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
  curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)  
  curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)[[1]]
}


redI_blank_clipped2 <- removeCurrentDiscont2_m(curr1[,3]-curr1[,2], dat[,8]+dat[,9], dat[,12]+dat[,11], dat[,15], 1e8)[[1]]
nirI_blank_clipped2 <- removeCurrentDiscont2_m(curr1[,4]-curr1[,2], dat[,8]+dat[,9], dat[,13]+dat[,11], dat[,16], 1e8)[[1]]

}
  
  #####################################################################

if(include_mojo3){

dat <- read.table(paste0(fpath1, "/Mojo 5173 (wrist)/", data_mojo3[i]), sep=",", head=T)

dat <- dat[dat[,1] != 0,]
dat <- dat[1000:(nrow(dat)-1000),]
curr1 <- curr2 <- curr3 <- matrix(0, nr=nrow(dat), nc=4)
for(j in 1:4){
  if(j < 3){
    Rf <- dat[,8]
    led <- dat[,14]
  }
  else{
    Rf <- dat[,9]
    led <- dat[,j+12]
  }
  
  safe_term <- 0
  if(j == 2){
    safe_term <- 1
    led <- numeric(nrow(dat))
  }
  
  dat_tmp <- dat[,j] 
  isub_tmp <- dat[,9+j]
  curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
  curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)  
  curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)[[1]]
}


redI_blank_clipped3 <- removeCurrentDiscont2_m(curr1[,3]-curr1[,2], dat[,8]+dat[,9], dat[,12]+dat[,11], dat[,15], 1e8)[[1]]
nirI_blank_clipped3 <- removeCurrentDiscont2_m(curr1[,4]-curr1[,2], dat[,8]+dat[,9], dat[,13]+dat[,11], dat[,16], 1e8)[[1]]



if(plotting){
  zPlot(curr3[,c(1,3,4)], par_new=T, col_f=c("seagreen", "red", "blue"))
  zPlot(cbind(FIRfilt(redI_blank_clipped3), FIRfilt(nirI_blank_clipped3)))
}


}

  #####################################################################


  fs=50 ; winsize <- 2^8
#   fs=25 ; winsize <- 2^7

  if(fs == 25){
    redI_blank_clipped1 <- redI_blank_clipped1[seq(1, len(redI_blank_clipped1), 2)]
    nirI_blank_clipped1 <- nirI_blank_clipped1[seq(1, len(nirI_blank_clipped1), 2)]
    redI_blank_clipped2 <- redI_blank_clipped2[seq(1, len(redI_blank_clipped2), 2)]
    nirI_blank_clipped2 <- nirI_blank_clipped2[seq(1, len(nirI_blank_clipped2), 2)]
  }

  
  out <- fftSpO2_Cdev(redI_blank_clipped1, nirI_blank_clipped1, Fs=fs, filtType="fir50", smoothACDC=0.8,
                      windowSize=winsize, spo2_check=F, useMaxDenom=F, spo2_caleb_pf=0.6)
  spo2_1  <- 100*calabFreeSpO2(out[[1]], clip=F, smoothAlpha=0.7)
  
  out <- fftSpO2_Cdev(redI_blank_clipped2, nirI_blank_clipped2, Fs=fs, filtType="fir50", smoothACDC=0.8,
                      windowSize=winsize, spo2_check=F, useMaxDenom=T, spo2_caleb_pf=0.6)
  spo2_2  <- 100*calabFreeSpO2(out[[1]], clip=F, smoothAlpha=0.7)

  out <- fftSpO2_Cdev(redI_blank_clipped3, nirI_blank_clipped3, filtType="fir50", smoothACDC=0.8,
                      windowSize=2^8, spo2_check=F, useMaxDenom=T, spo2_caleb_pf=0.6)
  spo2_3  <- 100*calabFreeSpO2(out[[1]], clip=F, smoothAlpha=0.7)
  

  m_name <- substring(names(data_wins)[[i]], 1, nchar(names(data_wins)[[i]])-9)

  min_len <- min(len(spo2_1), len(spo2_2), len(spo2_3), len(wins))

  write.table2(cbind(reference=wins[1:min_len], goldfinger_clamp=round(spo2_1[1:min_len],2), 
               mojo5172_finger=round(spo2_2[1:min_len],2), mojo5173_wrist=round(spo2_3[1:min_len], 2)),
               file=paste0("spo2_results/", m_name, "_results.txt"), col.names=T)

  
  w_plot <- wins[-c(1:offset_mat[i,1])]
  s1_plot <- round(spo2_1[-c(1:offset_mat[i,2])],1)

#   min_len <- min(len(w_plot), len(s1_plot))
#   w_plot <- w_plot[1:min_len]
#   s1_plot <- s1_plot[1:min_len]

#   performance_mat[i,] <- c( perf(w_plot, s1_plot)[1], perf(w_plot, spo2_2)[1], perf(w_plot, spo2_3)[1]  )


  

  w()
  lplot(wins, lwd=3, col="red", ylim=c(75,105), ylab="spo2 %", main=m_name, xlab="time (s)")
  lines(round(spo2_1,1), lwd=2, col="blue")
  lines(round(spo2_2,1), lwd=2, col="mediumseagreen")
  lines(round(spo2_3,1), lwd=2, col="orange")
  legend("bottomright", bg="white", lwd=2, col=c("red", "blue", "mediumseagreen", "orange"), cex=0.8,
         leg=c("reference (Winspiro)", paste("LifeQ",  c("(finger clamp)", "(finger)", "(wrist)"))))

# if(i != 17){
# ccf_out <- ccf(w_plot, s1_plot)
# max_lag <- ccf_out[[4]][which(ccf_out[[1]] == max(ccf_out[[1]])[1])]
# legend("topright", leg=paste("max lag at", max_lag), cex=0.75, bg="white")
# }
# else
#   plot(1,1)


  
  
  #####################################################################
  
  savePlot(paste0("spo2_results/", m_name, "_50hz"), "png")
  graphics.off()
  cat(round(100*i/len(data_wins)), "%\n")

}




w() ; par(ask=T)
c_list <- vector("list", len(data_mojo2))
for(i in 1:len(data_mojo2)){
  
  cout <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out_", data_mojo2[i]), sep=",")
  
  c_spo2 <- cout[,4]/10
  c_spo2 <- c_spo2[-c(1:which(diff(c_spo2) != 0)[1])]
  c_spo2 <- c_spo2[seq(1, len(c_spo2), 25)]
  
  c_list[[i]] <- c_spo2
  

#   lplot(c_spo2-c_list_b[[i]], main=paste("data set", i))
  lplot(round(c_list_b[[i]],1), lwd=3, col="red", main=data_mojo2[i], ylab="spo2", ylim=c(90,102))
  lines(c_spo2, lwd=2, col="blue")
#   
#   
#   savePlot(paste0("~/HealthQ/R scripts/HealthQ_OxySat/spo2_results/", names(data_mojo2)[i]), "png")
#   graphics.off()
  
  
}



