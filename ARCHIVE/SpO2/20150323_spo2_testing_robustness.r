source("../useful_funs.r")
source("./sp02_funs.r")




## dual biomon drop tests
data_list <- c(getDataFromDir("~/HealthQ/DUMPED/20150324/Mojo 5143/", head=T),
               getDataFromDir("~/HealthQ/DUMPED/20150324/Mojo 5152/", head=T))
data_list <- data_list[order(names(data_list))]


## Riaan's device
data_list <- getDataFromDir("~/HealthQ/DUMPED/20150326/Mojo 5119 (forehead tests)/", head=T)






##################################################################################################
##################################################################################################


xx <- diff(curr3[,3])
w() ; tiaan_specgram_opt(xx, windowSize=2^10, overlap=2^9, normal=3, maxfreq=2)



i <- 3


for(i in 1:len(data_list)){
  # for(i in 8:9){
  
  dat <- data_list[[i]]
  
  dat <- dat[dat[,1] != 0,]
  dat <- dat[-c(1:500),]
  
  curr1 <- curr2 <- curr3 <- matrix(0, nr=nrow(dat)-1, nc=4)
  rfmat <- isubmat <- matrix(0, nr=nrow(dat)-1, nc=4)
  for(j in 1:4){
    if(j < 3){
      Rf <- dat[,9]
      led <- dat[,11]
    }
    else{
      Rf <- dat[,10]
      led <- dat[,j+13]
    }
    
    safe_term <- 0
    if(j == 2){
      safe_term <- 1
      led <- numeric(nrow(dat))
    }
    
    dat_tmp <- dat[-1,j] 
    isub_tmp <- 10*dat[,10+j]
    isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
    Rf <- Rf[1:(len(Rf)-1)]
    led <- led[1:(len(led)-1)]
    curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
    curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)  
    curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)[[1]]
    rfmat[,j] <- Rf
    isubmat[,j] <- isub_tmp
  }
  
  
  zPlot(cbind(curr2[,c(1,3:4)]-curr2[,2]), par_new=T, col_f=c("lightblue", "red", "forestgreen"))
  # zPlot(apply(cbind(curr2[,c(1,3:4)]-curr2[,2]), 2, diff), par_new=T, col_f=c("lightblue", "red", "forestgreen"))
  # zPlot(cbind(FIRfilt(redI_clipped), FIRfilt(nirI_clipped)), par_new=T)
  
  
  
  # pind <- 3500:4500 ;w() ; par(mar=c(4,4,3,4))
  # lplot(pind, curr2[pind,3]-curr2[pind,2], lwd=2, col="red",
  #       ylab="current (red)", xlab="index", main="LQ-5140 (Kari)")
  # par(new=T) ; lplot(pind, curr2[pind,4]-curr2[pind,2], lwd=2, col="blue", 
  #                    ylab="", xlab="", xaxt="n", yaxt="n") 
  # axis(4) ; mtext("current (IR)", side=4, line=2, cex=par("cex.lab"))
  # legend("bottomright", bg="white", col=c("red", "blue"), lwd=2, leg=c("red", "IR"), cex=0.8)
  
  # write.table2(format(cbind(curr2[,c(1,3,4)]-curr2[,2]), digits=12),
  #              file=paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/data/RedIRFilt/mojo_breakout_0",i,".txt"))
  
  
  
  # redI_clipped <- curr2[,3]-curr2[,2]
  # nirI_clipped <- curr2[,4]-curr2[,2] 
  # 
  # out <- fftSpO2_Cdev(redI_clipped, nirI_clipped, subtractMeans=F, filtType="fir50", adapF=F,
  #                     windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=T)
  # spo2_1  <- 100*calabFreeSpO2(out[[1]], clip=F)
  # ################################################
  # 
  # out <- fftSpO2_Cdev(redI_clipped, nirI_clipped, curr2[,1]-curr2[,2], subtractMeans=F, filtType="fir50", aNFilt=T,
  #                     windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F)
  # spo2_2  <- 100*calabFreeSpO2(out[[1]], clip=F)
  # 
  # ################################################
  
  redI_clipped <- curr3[,3]-curr3[,2]
  nirI_clipped <- curr3[,4]-curr3[,2] 
  
  out <- fftSpO2_Cdev(redI_clipped, nirI_clipped, filtType="fir50", adapF=F,
                      windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F)
  spo2_3  <- 100*calabFreeSpO2(out[[1]], clip=F)
  
  # qm <- 100*out[[4]][,7:8]
  # qm_smooth <- qm
  # for(j in 2:nrow(qm))
  #   qm_smooth[j,] <- 0.3*qm_smooth[j-1,] + (1-0.3)*qm[j,]
  
  # acdc <- (out[[3]][,c(1,3)])/(out[[3]][,c(2,4)])
  
  
  ################################################
  
  out <- fftSpO2_Cdev(redI_clipped, nirI_clipped, curr3[,1]-curr3[,2], filtType="fir50", adapF=F, smoothACDC=0.8,
                      windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F)
  spo2_4  <- 100*calabFreeSpO2(out[[1]], clip=F)
  
  ################################################
  
    
  
  w() ; par(mar=c(4,4,2,3))
  lplot(round(movingAve(spo2_3,1),2), lwd=2, col="blue", ylim=c(70,110), ylab="spo2", main=names(data_list)[i])
  lines(round(movingAve(spo2_4,1),2), lwd=2, col="red")
  # par(new=T) ; plot(wns_plot, type="n", yaxt="n", xaxt="n", ann=F, ylim=range(acdc)) ; axis(4)
  # lines(acdc[,1],col="red") ; lines(acdc[,2], col="orange")
  
  
  
  
  # savePlot(paste0("spo2_results/", names(data_mojo)[[i]], "_spo2result"), "png")
  # write.table2(cbind(spo2_3), file=paste0("spo2_results/", names(data_mojo)[[i]], "_spo2result.txt"))
  
  savePlot(paste0("spo2_results/", names(data_list)[i]), "png")
  graphics.off() ; cat(round(100*i/len(data_list), 1), "% completed\n")
}








##################################################################################################
##################################################################################################
##################################################################################################
##################################################################################################
##################################################################################################
##################################################################################################
##################################################################################################
##################################################################################################
##################################################################################################
##################################################################################################
##
##
##            FOREHEAD ROBUSTNESS TESTING (20150331)      
##
##
##################################################################################################
##################################################################################################
##################################################################################################
##################################################################################################







fpath1 <- "~/HealthQ/Data/OxySat/SpO2 forehead testing 20150331/"
indivs <- c("Kora", "Lindie", "Michael", "Nina", "Renier", "Robert", "Tiaan", "Van Zyl")

fpath2 <- paste0(fpath1, paste0(rep(indivs, each=2), c("/1/", "/2/")))


##################################################################################################
##################################################################################################


i <- 14


# for(i in 1:len(fpath2)){
  
  
  datlist <- getDataFromDir(fpath2[i], head=T)
  dat1 <- datlist[[grep("forehead", names(datlist))]]
  wins <- datlist[[grep("winspiro", names(datlist))]][,3]
  datname <- names(datlist)[grep("forehead", names(datlist))]
  rm(datlist)
  
  
  ###################
  #### dat1
  
  dat <- dat1
  
  dat <- dat[dat[,1] != 0,]
  dat <- dat[-c(1:500),]
  curr1 <- curr2 <- curr3 <- matrix(0, nr=nrow(dat)-1, nc=4)
  rfmat <- isubmat <- matrix(0, nr=nrow(dat)-1, nc=4)
  for(j in 1:4){
    if(j < 3){
      Rf <- dat[,9]
      led <- dat[,11]
    }
    else{
      Rf <- dat[,10]
      led <- dat[,j+13]
    }
    
    safe_term <- 0
    if(j == 2){
      safe_term <- 1
      led <- numeric(nrow(dat))
    }
    
    dat_tmp <- dat[-1,j] 
    isub_tmp <- 10*dat[,10+j]
    isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
    Rf <- Rf[1:(len(Rf)-1)]
    led <- led[1:(len(led)-1)]
    curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
    curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)  
    curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)[[1]]
    rfmat[,j] <- Rf
    isubmat[,j] <- isub_tmp
  }
  
  zPlot(cbind(curr2[,c(1,3:4)]-curr2[,2]), par_new=T, col_f=c("lightblue", "red", "forestgreen"))
  
  redI_clipped1 <- curr3[,3]-curr3[,2]
  nirI_clipped1 <- curr3[,4]-curr3[,2]
  
  
  #######################################
  
  out <- fftSpO2_Cdev(redI_clipped1, nirI_clipped1, filtType="fir50", adapF=F, smoothACDC=0.8,
                      windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F)
  spo2_1  <- 100*calabFreeSpO2(out[[1]], clip=F)
  
  
#   out <- fftSpO2_Cdev(redI_clipped2, nirI_clipped2, filtType="fir50", adapF=F, smoothACDC=0.8,
#                       windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F)
#   spo2_2  <- 100*calabFreeSpO2(out[[1]], clip=F)
#   
  
  w() ; lplot(wins, lwd=3, col="black", ylim=c(80,105), ylab="spo2",
              main=paste("SpO2 test: ", substring(fpath2[i], 54, nchar(fpath2[i]))))
  lines(spo2_1, lwd=2, col="blue")
#   lines(spo2_2, lwd=2, col="forestgreen")
#   legend("bottomright", bg="white", lwd=2, col=c("black", "blue", "forestgreen"),
#          leg=c("winspiro", datnames), cex=0.75)
  
  
#   savePlot(paste0("spo2_results/result_", i), "png")
  
# }