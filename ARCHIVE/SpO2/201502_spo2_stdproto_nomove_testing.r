source("../useful_funs.r")
source("sp02_funs.r")
load("stdproto_nomove_list.Rdata")

stdproto_nomove_list <- output_list ; rm(output_list)

i <- 1

for(i in 1:len(stdproto_nomove_list)){
  
  m <- len(stdproto_nomove_list[[i]])
  
  for(j in 1:m){
    
    cat("(i =",i,", j =", j, ")\n")
    
    dat <- stdproto_nomove_list[[i]][[j]]
    
    curr1 <- curr2 <- matrix(0, nr=nrow(dat), nc=4)
    for(k in 1:4){
      if(k < 3)
        Rf <- dat[,5]
      else
        Rf <- dat[,6]
            
      curr1[,k] <- convertToCurrent3(dat[,k], Rf, 10*dat[,k+6])
#       curr2[,k] <- removeCurrentDiscont(curr1[,k], Rf, 10*dat[,k+6])
      curr2[,k] <- removeCurrentDiscont2(curr1[,k], Rf, 10*dat[,k+6])[[1]]
    }
    
#     x_ind <- 200:1000
#     
#     w() ; lplot(curr2[x_ind,3], lwd=2, col="red",
#                 xlab="index", ylab="current", main=paste0(names(stdproto_nomove_list)[i], ": no movement, segment ", j))
#     par(new=T) ; lplot(curr2[x_ind,4], lwd=2, col="blue", ann=F, xaxt="n", yaxt="n")
#     axis(4)
#     
#     savePlot(paste0("spo2_results/", paste0(names(stdproto_nomove_list)[i], "_nomove_segment", j)), "png")
#     graphics.off()
    
    
    redI_clipped <- curr1[,3]-curr1[,2]
    nirI_clipped <- curr1[,4]-curr1[,2] 
    out <- fftSpO2_Cdev(redI_clipped, nirI_clipped, subtractMeans=F, filtType="fir50", 
                        windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F,
                        spo2_caleb_pf=0.7, spo2_caleb_e1x=0.08, spo2_caleb_e2x=0.29, 
                        spo2_caleb_e1y=0.81, spo2_caleb_e2y=0.18, spo2_caleb_bias=0)
    
    spo2_1  <- 100*calabFreeSpO2(out[[1]], clip=F)#, smoothAlpha=out[[3]][,8])
    
    redI_clipped <- curr2[,3]-curr2[,2]
    nirI_clipped <- curr2[,4]-curr2[,2] 
    out <- fftSpO2_Cdev(redI_clipped, nirI_clipped, subtractMeans=F, filtType="fir50", 
                        windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F,
                        spo2_caleb_pf=0.7, spo2_caleb_e1x=0.08, spo2_caleb_e2x=0.29, 
                        spo2_caleb_e1y=0.81, spo2_caleb_e2y=0.18, spo2_caleb_bias=0)
    
    spo2_2  <- 100*calabFreeSpO2(out[[1]], clip=F)#, smoothAlpha=out[[3]][,8])
    
    w() ; lplot(movingAve(spo2_1,1), lwd=2, col="forestgreen", ylim=c(60,110))
    lines(movingAve(spo2_2,1), lwd=2, col="red")
    title(main=paste0(names(stdproto_nomove_list)[i], ": no move segment ", j))
    
    savePlot(paste0("spo2_results/201502/", paste0(names(stdproto_nomove_list)[i], "_nomove_segment", j)), "png")
    graphics.off()
    
  }  
  
  
}








i <- i+1
m <- len(stdproto_nomove_list[[i]])
j <- 0


  j <- j + 1
  dat <- stdproto_nomove_list[[i]][[j]]
  
  curr1 <- curr2 <- matrix(0, nr=nrow(dat), nc=4)
  for(k in 1:4){
    if(k < 3)
      Rf <- dat[,5]
    else
      Rf <- dat[,6]
    
    curr1[,k] <- convertToCurrent3(dat[,k], Rf, 10*dat[,k+6])
    curr2[,k] <- removeCurrentDiscont(curr1[,k], Rf, 10*dat[,k+6])
  }

  zPlot(cbind(curr1[,3:4]-curr1[,2], curr2[,3:4]-curr2[,2]), par_new=T,
        col_f=c("blue", "forestgreen", "red", "orange"))
  
  


