source("~/HealthQ/R files/HRV/ERMADT_funs.r")
source("~/HealthQ/R files/HRV/tiaan_se_amazing_QMs.r")
source("~/HealthQ/R files/adaptive_notch_filter.r")
library(signal)

fftSpO2_C <- function(Signal_num, Signal_denom, windowSize=2^8, 
                    Fs=50, overlap=50, windowing=FALSE,
                    filtType=c("butter", "fir50")[1], subtractMeans=TRUE,
                    skipSamples=500, fftLogicMode=c(0,1,2), logicSD=5, 
                    fftInterp=FALSE, interpBW=2,
                    useMaxNum=FALSE, useMaxDenom=FALSE, spo2_check_diffs=T,
                    spo2_caleb_pf=0.7, spo2_caleb_e1x=0.08, spo2_caleb_e2x=0.29, 
                    spo2_caleb_e1y=0.81, spo2_caleb_e2y=0.18, spo2_caleb_bias=0) 
{
  # data prep
  # Signal_num: signal used in the numerator of the R ratio, so for normal SpO2 this is 660nm
  # Signal_denom signal used in the denominator of the R ratio, so for normal SpO2 this is 940nm
  

  
  ################################################################################
  ######## MEAN SUBSTRACTION
   
  if(subtractMeans){
  
    mean1 <- Signal_num[1]
    mean2 <- Signal_denom[1]
    for(i in 2:len(Signal_num)){
      mean1 <- c(mean1, mean1[i-1]*((i-1)/i) + Signal_num[i]*(1/i))
      mean2 <- c(mean2, mean2[i-1]*((i-1)/i) + Signal_denom[i]*(1/i))
    }
      
    Signal_num   <- Signal_num - mean1
    Signal_denom <- Signal_denom - mean2
    
  }
  
  
  ################################################################################
  ######## FILTERING AND TRUNCATION OF INITIAL FILTER OUTPUT
  
  
  if(filtType == "butter"){
    red_new <- filter(butter(6,c(30/60/(Fs/2),210/60/(Fs/2)), type="pass"), Signal_num)
    ir_new  <- filter(butter(6,c(30/60/(Fs/2),210/60/(Fs/2)), type="pass"), Signal_denom)
    
  }
  else if(filtType == "fir50"){
    red_new <- FIRfilt(Signal_num)
    ir_new  <- FIRfilt(Signal_denom)

  }
  
  
  
  
  if(skipSamples > 0){
    red_new <- red_new[-c(1:skipSamples)]
    ir_new <- ir_new[-c(1:skipSamples)]
    
    Signal_num <- Signal_num[-c(1:skipSamples)]
    Signal_denom <- Signal_denom[-c(1:skipSamples)]
  }
  
  
  ################################################################################
  ######## INITIALISATION OF ITERATIVE WINDOWING
  
  
  #initialize
  init    <- 1
  output  <-data.frame(NULL)
  windows <-NULL
  
  if(length(Signal_num)==length(Signal_denom)) 
    n <- length(red_new)
  else 
    stop("Length of Signal_num and Signal_denom not the same")
  
  n2 <- floor((n-windowSize)/overlap)+1 # number of spectrum frames 
  
  window_w <- gausswin(windowSize)
  

#   spec_out_mat_num <- matrix(0, nr=n2, nc=(windowSize/2)+2)
#   spec_out_mat_denom <- matrix(0, nr=n2, nc=(windowSize/2)+2)

  
  fft_prev_num <- NULL
  fft_prev_denom <- NULL
  
  freqs <- seq(0, (Fs/2), length=(windowSize/2))
  
  spo2_prev <- 0
  
  #FFT
  for(i in 0:(n2-1)){
    
    # Select window
    indexvec <- 1:windowSize+overlap*i
    red      <- red_new[indexvec]/100
    ir       <- ir_new[indexvec]/100
    
    
    
    # Perform FFT
    if(windowing==TRUE) { 
      fft_Signal_num   <- (abs(fft(    red   *window_w)[1:(length(red)/2)])) 
      fft_Signal_denom <- (abs(fft(   ir   *window_w)[1:(length(ir)/2)])) 
    }
    
    if(windowing==FALSE) { 
      fft_Signal_num   <-  (abs(fft(red)[1:(length(red)/2)]))  
      fft_Signal_denom <-  (abs(fft(ir)[1:(length(ir)/2)])) 
    }


    ################################################################################
    ######## FFT NO LOGIC
    
    max_ind_num <- which(fft_Signal_num[-c(1:3)]==max(fft_Signal_num[-c(1:3)]))+3
    max_ind_denom <- which(fft_Signal_denom[-c(1:3)]==max(fft_Signal_denom[-c(1:3)]))+3


    ################################################################################
    ######## FFT LOGIC (GAUSSIAN WEIGHTS)
      
    fft_num_temp <- fft_Signal_num[-c(1:3)]
    fft_denom_temp <- fft_Signal_denom[-c(1:3)]
      
    if(i == 0){
      max_ind_num_fftl <- which(fft_num_temp == max(fft_num_temp))+3
      max_ind_denom_fftl <- which(fft_denom_temp == max(fft_denom_temp))+3
    }
    else{
        
      gauss_score_num <- dnorm(freqs, freqs[max_ind_num_fftl], logicSD/60)[-c(1:3)]
      gauss_score_denom <- dnorm(freqs, freqs[max_ind_denom_fftl], logicSD/60)[-c(1:3)]
        
      fft_num_temp <- fft_num_temp*gauss_score_num
      fft_denom_temp <- fft_denom_temp*gauss_score_denom
        
      max_ind_num_fftl <- which(fft_num_temp == max(fft_num_temp))+3
      max_ind_denom_fftl <- which(fft_denom_temp == max(fft_denom_temp))+3
    }
    
    
#     spec_out_mat_num[i+1,] <- c(fft_Signal_num, max_ind_num, max_ind_num_fftl)
#     spec_out_mat_denom[i+1,] <- c(fft_Signal_denom, max_ind_denom, max_ind_num_fftl)
  

  ################################################################################
  ######## CHOOSE BINS BASED ON LOGIC IDENTIFIER


    if(fftLogicMode == 1){
      max_ind_num <- max_ind_num_fftl
      max_ind_denom <- max_ind_denom_fftl
    }
  
    

    if(useMaxNum)
      max_ind_denom <- max_ind_num

    if(useMaxDenom)
      max_ind_num <- max_ind_denom


    if(fftInterp){
      ind1 <- max(2, max_ind_num-interpBW)
      ind2 <- max_ind_num+interpBW
      fft_num_ave <- mean(fft_Signal_num[ind1:ind2])
      
      ind1 <- max(2, max_ind_denom-interpBW)
      ind2 <- max_ind_denom+interpBW
      fft_denom_ave <- mean(fft_Signal_denom[ind1:ind2])
      
      AC_Signal_num <- fft_num_ave*2/(windowSize/2)
      AC_Signal_denom <- fft_denom_ave*2/(windowSize/2)
      
    }
    else{
      AC_Signal_num <- fft_Signal_num[max_ind_num]*2/(windowSize/2) # exclude DC term; *2/window gives the original amplitude info
      AC_Signal_denom <- fft_Signal_denom[max_ind_denom]*2/(windowSize/2)
    }

    
    DC_Signal_num <- mean(Signal_num[indexvec])
    DC_Signal_denom <- mean(Signal_denom[indexvec])
    
    #### CALCULATE SPO2 USING AC AND DC VALUES ####

    pF  <- spo2_caleb_pf
    e1x <- spo2_caleb_e1x
    e2x <- spo2_caleb_e2x
    e1y <- spo2_caleb_e1y
    e2y <- spo2_caleb_e2y
    logBias <- spo2_caleb_bias


    dAl1 <- log(  (DC_Signal_num - 0.5*AC_Signal_num + logBias)/(DC_Signal_num + 0.5*AC_Signal_num + logBias))
    dAl2 <- log(  (DC_Signal_denom - 0.5*AC_Signal_denom + logBias)/(DC_Signal_denom + 0.5*AC_Signal_denom + logBias))
    spo2_out <- (dAl2*e1y - dAl1*e2y*pF)/(dAl2*(-e1x + e1y) + dAl1*(e2x - e2y)*pF)

    #### CHECK SPO2 DIFFERENCES FOR UNREALISTIC DIPS ####
    if(spo2_check_diffs){
      
      if(spo2_prev == 0)
        spo2_prev <- spo2_out
      else{
        
        if((spo2_out-spo2_prev < -0.02) & (spo2_prev < 1))
          spo2_out <- spo2_prev
        else
          spo2_prev <- spo2_out
        
      }
      
      
    }


    
    output <- rbind(output, c(spo2_out))

  }
  
  colnames(output) <- c("spo2")
  return(list(output))#, spec_out_mat_num, spec_out_mat_denom))
}




######################################################################################
######################################################################################
######################################################################################



fftSpO2_Cdev <- function(Signal_num, Signal_denom, Signal_grn, windowSize=2^8, 
                         Fs=50, overlap=Fs, windowing=FALSE, zeroPad=0, subtractMeans=FALSE,
                         adapFilt=FALSE, afOrder=30, afAlpha=0.01, aNFilt=FALSE, aNFGamma=0.9,
                         filtType=c("butter", "fir50")[1], smoothACDC=0,
                         fftLogicMode=0, logicSD, fftInterp, interpBW,
                         skipSamples=100, useMaxNum=FALSE, useMaxDenom=FALSE, spo2_check_diffs=T,
                         spo2_caleb_pf=0.7, spo2_caleb_e1x=0.08, spo2_caleb_e2x=0.29, 
                         spo2_caleb_e1y=0.81, spo2_caleb_e2y=0.18, spo2_caleb_bias=0,
                         spo2_new_method=FALSE, calcQM=TRUE, newFFTMethod=FALSE,
                         print_progress=FALSE) 
{
  # data prep
  # Signal_num: signal used in the numerator of the R ratio, so for normal SpO2 this is 660nm
  # Signal_denom signal used in the denominator of the R ratio, so for normal SpO2 this is 940nm
  # Logic Modes: 0 -> no logic
  #              1 -> smooth FFT power spectrum with normal distribution
  #              2 -> spo2 candidates calculated for every FFT bin
  
  
  
  Signal_num_dc <- Signal_num
  Signal_denom_dc <- Signal_denom
  
  
  if(!missing(Signal_grn)) is_green <- TRUE
  else{
    is_green <- FALSE
    if(adapFilt)
      warning("Reference signal not provided for adaptive filter.")
    adapFilt <- FALSE
  }
  
  ################################################################################
  ######## SPO2 CALCULATINO FUNCTION
  
  spo2_caleb_fun <- function(AC_num, DC_num, AC_denom, DC_denom){
    
    if(!spo2_new_method){
      dAl1 <- log(  (DC_num - 0.5*AC_num + spo2_caleb_bias)/(DC_num + 0.5*AC_num + spo2_caleb_bias))
      dAl2 <- log(  (DC_denom - 0.5*AC_denom + spo2_caleb_bias)/(DC_denom + 0.5*AC_denom + spo2_caleb_bias))
    
#     if(is.nan(dAl1)) print(paste("red NAN:", AC_num, ",", DC_num))
#     if(is.nan(dAl2)) print(paste("red NAN:", AC_denom, ",", DC_denom))
  
      spo2_out <- (dAl2*spo2_caleb_e1y - dAl1*spo2_caleb_e2y*spo2_caleb_pf)/(dAl2*(-spo2_caleb_e1x + spo2_caleb_e1y) + dAl1*(spo2_caleb_e2x - spo2_caleb_e2y)*spo2_caleb_pf)
    }
    else{
      phi <- (AC_num/AC_denom)*(DC_denom/DC_denom)
      spo2_out <- (spo2_caleb_e1y - phi*spo2_caleb_e2y*spo2_caleb_pf)/((-spo2_caleb_e1x + spo2_caleb_e1y) + phi*(spo2_caleb_e2x - spo2_caleb_e2y)*spo2_caleb_pf)
    }
    
    return(spo2_out)
  }



  ################################################################################
  ######## NIELEN'S QUALITY METRIC

  calc_qm_from_fft_buffer <- function(buff, fft_start=3, fft_stop=32)
  {
  
    i_max <- which(buff == max(buff[fft_start:fft_stop]))
  
    in_start <- max(fft_start, floor(i_max*0.6))
    in_stop  <- min(fft_stop, floor(i_max*1.8))
  
    ip_start <- max(in_start, i_max-1)
    ip_stop  <- min(in_stop, i_max+4)
    p_len <- ip_stop - ip_start
  
    sum_peak <- sum_pen  <- 0
    for(i in in_start:in_stop){
      if((i < ip_start) | (i > ip_stop))
        sum_pen <- sum_pen + buff[i]
      else
        sum_peak <- sum_peak + buff[i]
    }
  
  
    sum_mean <- (sum_peak + sum_pen)/(in_stop - in_start + 1)
    max_val <- max(5000, sum_mean)
  
    max_val_peak <- max_val*p_len
    max_val_pen <- max_val*(in_stop - in_start - p_len + 1)
  
    peak_norm <- min(max_val_peak, sum_peak)/(max_val_peak+1)
    pen_norm <- min(max_val_pen, sum_pen)/(max_val_pen+1)
  
    return( max(0, peak_norm-pen_norm) )
  }
  
  
  
  ################################################################################
  ######## ADAPTIVE FILTERING
  
  if(adapFilt){

    offset_num <- Signal_num[10]
    offset_denom <- Signal_denom[10]
    
    diff_num <- diff(Signal_num)[-c(1:10)]
    diff_denom <- diff(Signal_denom)[-c(1:10)]
    diff_grn <- diff(Signal_grn)[-c(1:10)]
    
    lms_num <- LMSfilt(diff_num, diff_grn, afOrder, afAlpha)
    lms_denom <- LMSfilt(diff_denom, diff_grn, afOrder, afAlpha)
    
    Signal_num <- cumsum(lms_num) + offset_num
    Signal_denom <- cumsum(lms_denom) + offset_denom
    
  }
  
  if(aNFilt){
    
    ALNF_out_num <- ALNF(Signal_num, aNFGamma)
    ALNF_out_denom <- ALNF(Signal_denom, aNFGamma)
    
    Signal_num <- Signal_num - ALNF_out_num[,2]
    Signal_denom <- Signal_denom - ALNF_out_denom[,2]
    
  }
  
  
  ################################################################################
  ######## RUNNING MEAN SUBTRACTIONS
  
  
  if(subtractMeans){
    
    mean1 <- Signal_num[1]
    mean2 <- Signal_denom[1]
    for(i in 2:len(Signal_num)){
      mean1 <- c(mean1, mean1[i-1]*((i-1)/i) + Signal_num[i]*(1/i))
      mean2 <- c(mean2, mean2[i-1]*((i-1)/i) + Signal_denom[i]*(1/i))
    }
    
    Signal_num   <- Signal_num - mean1
    Signal_denom <- Signal_denom - mean2
    
  }
  
  
  
  ################################################################################
  ######## FILTERING AND TRUNCATION OF INITIAL FILTER OUTPUT
  
  
  if(filtType == "butter"){
    red_new <- filter(butter(6,c(30/60/(Fs/2),130/60/(Fs/2)), type="pass"), Signal_num)
    ir_new  <- filter(butter(6,c(30/60/(Fs/2),130/60/(Fs/2)), type="pass"), Signal_denom)
    
    if(is_green) grn_new <- filter(butter(6,c(30/60/(Fs/2),210/60/(Fs/2)), type="pass"), Signal_grn)
  }
  else if(filtType == "fir50"){
    red_new <- FIRfilt(Signal_num, Fs)
    ir_new  <- FIRfilt(Signal_denom, Fs) 
    
    if(is_green) grn_new <- FIRfilt(Signal_grn, Fs)
  }
  
  
  if(skipSamples > 0){
    red_new <- red_new[-c(1:skipSamples)]
    ir_new <- ir_new[-c(1:skipSamples)]
    
#     Signal_num <- Signal_num[-c(1:skipSamples)]
#     Signal_denom <- Signal_denom[-c(1:skipSamples)]
    
    Signal_num_dc <- Signal_num_dc[-c(1:skipSamples)]
    Signal_denom_dc <- Signal_denom_dc[-c(1:skipSamples)]
    
    if(is_green){
      grn_new <- grn_new[-c(1:skipSamples)]
      Signal_grn <- Signal_grn[-c(1:skipSamples)]
    }
    
  }
  
  
  ################################################################################
  ######## INITIALISATION OF ITERATIVE WINDOWING
  
  
  #initialize
  init    <- 1
  output  <-data.frame(NULL)
  windows <-NULL
  
  if(length(Signal_num)==length(Signal_denom)) 
    n <- length(red_new)
  else 
    stop("Length of Signal_num and Signal_denom not the same")
  
  if(n > 200000)
    print_progress <- TRUE
  
  n2 <- floor((n-windowSize)/overlap)+1 # number of spectrum frames 

  window_w <- gausswin(windowSize)
  
    
  spo2_candidate_list <- vector("list", 0) 
   
  freqs <- seq(0, (Fs/2), length=(windowSize/2))
  
  spo2_prev <- 0
  
  output_mat <- matrix(0, nr=n2, nc=9)
  qm_mat <- matrix(0, nr=n2, nc=5)
  fft_num_out <- matrix(0, nr=n2, nc=windowSize/2+1)
  fft_denom_out <- matrix(0, nr=n2, nc=windowSize/2+1)
  
  window_means <- matrix(0, nr=n2, nc=windowSize*2)
  
  #FFT
  for(i in 0:(n2-1)){
    
    # Select window
    indexvec <- 1:windowSize+overlap*i
    red      <- red_new[indexvec]
    ir       <- ir_new[indexvec]
    
    
    if(is_green) grn <- grn_new[indexvec]
    
    
    window_means[i+1,] <- c(red, ir)
    
    
#     red <- red-mean(red)
#     ir <- ir-mean(ir)

    if(zeroPad != 0){
      red <- c(red, rep(0, zeroPad*windowSize))
      ir <- c(ir, rep(0, zeroPad*windowSize))
    }
    
    
    # Perform FFT
    if(windowing==TRUE) { 
      fft_Signal_num   <- (abs(fft(   red  *window_w)[1:(len(red)/2)])) 
      fft_Signal_denom <- (abs(fft(   ir   *window_w)[1:(len(ir)/2)])) 
    }
    
    if(windowing==FALSE) { 
      fft_Signal_num   <-  (abs(fft(red)[1:(len(red)/2)]))  
      fft_Signal_denom <-  (abs(fft(ir)[1:(len(ir)/2)])) 
      
      if(is_green) fft_Signal_grn <- (abs(fft(grn)[1:(len(grn)/2)])) 
    }
    
    
    
#     fft_num_trunc <- fft_Signal_num[-c(1:2)]*len(red)
#     fft_denom_trunc <- fft_Signal_denom[-c(1:2)]*len(ir)
    
    
    if(calcQM){
#       qm_mat[i+1,] <- c(QM_V1(fft_num_trunc), QM_V1(fft_denom_trunc), 
#                         QM_SF(fft_num_trunc), QM_SF(fft_denom_trunc), 
#                         QM_SF_Entropy(fft_num_trunc), QM_SF_Entropy(fft_denom_trunc),
#                         QM_V5(fft_num_trunc), QM_V5(fft_denom_trunc))
      
      red_qm_new <- calc_qm_from_fft_buffer(fft_Signal_num*fft_Signal_num/(windowSize^2))
      nir_qm_new <- calc_qm_from_fft_buffer(fft_Signal_denom*fft_Signal_denom/(windowSize^2))
      
#       qm_mat[i+1,] <- c(red_qm_new, nir_qm_new, min(red_qm_new, nir_qm_new))
      
      if(i == 0)
        qm_mat[i+1,1:3] <- c(red_qm_new, nir_qm_new, min(red_qm_new, nir_qm_new))
      else{
        qm_mat[i+1,1] <- 0.3*red_qm_new + 0.7*qm_mat[i,1]
        qm_mat[i+1,2] <- 0.3*nir_qm_new + 0.7*qm_mat[i,2]
        qm_mat[i+1,3] <- min(qm_mat[i+1,1:2])
      }
      
      
      
      ## Correlation as quality metric
      qm_mat[i+1,4] <- cor(red, ir)
      qm_mat[i+1,5] <- cor(Signal_num_dc[indexvec], Signal_denom_dc[indexvec])
      
    }
      
    
      
    if(newFFTMethod){
      for(j in 1:len(fft_Signal_num)/2){
        fft_Signal_num[j] <- (fft_Signal_num[j]^0.7)*(fft_Signal_num[j*2]^0.3)
        fft_Signal_denom[j] <- (fft_Signal_denom[j]^0.7)*(fft_Signal_denom[j*2]^0.3)
      }
    }


    dropFFT <- 3
    if(Fs == 25)
      dropFFT <- 2
    
    
    ################################################################################
    ######## FFT NO LOGIC
    
    DC_Signal_num <- mean(Signal_num_dc[indexvec])
    DC_Signal_denom <- mean(Signal_denom_dc[indexvec])
    
    DC_Signal_grn <- 0
    if(is_green) DC_Signal_grn <- mean(Signal_grn[indexvec])

#     if(fftLogicMode == 0){
      
      max_ind_num <- which(fft_Signal_num[-c(1:dropFFT)]==max(fft_Signal_num[-c(1:dropFFT)])) + dropFFT
      max_ind_denom <- which(fft_Signal_denom[-c(1:dropFFT)]==max(fft_Signal_denom[-c(1:dropFFT)])) + dropFFT 
      
#     }

    if(is_green) max_ind_grn <- which(fft_Signal_grn[-c(1:dropFFT)]==max(fft_Signal_grn[-c(1:dropFFT)])) + dropFFT 



    ######## FFT LOGIC (SMOOTHING WITH GAUSSIAN)
    if(fftLogicMode == 1){
  
  
      fft_num_temp <- fft_Signal_num[-c(1:dropFFT)]
      fft_denom_temp <- fft_Signal_denom[-c(1:dropFFT)]
  
      if(i == 0){
        max_ind_num_fftl <- which(fft_num_temp == max(fft_num_temp)) + dropFFT
        max_ind_denom_fftl <- which(fft_denom_temp == max(fft_denom_temp)) + dropFFT
      }
      else{
    
        gauss_score_num <- dnorm(freqs, freqs[max_ind_num_fftl], logicSD/60)[-c(1:dropFFT)]
        gauss_score_denom <- dnorm(freqs, freqs[max_ind_denom_fftl], logicSD/60)[-c(1:dropFFT)]
    
        fft_num_temp <- fft_num_temp*gauss_score_num
        fft_denom_temp <- fft_denom_temp*gauss_score_denom
    
        max_ind_num_fftl <- which(fft_num_temp == max(fft_num_temp)) + dropFFT
        max_ind_denom_fftl <- which(fft_denom_temp == max(fft_denom_temp)) + dropFFT
      }
      
      max_ind_num <- max_ind_denom_fftl
      max_ind_denom <- max_ind_denom_fftl
  
    }


    
      if(useMaxNum)
        max_ind_denom <- max_ind_num
    
      if(useMaxDenom)
        max_ind_num <- max_ind_denom


      fft_num_out[i+1,] <- c(max_ind_num, fft_Signal_num)
      fft_denom_out[i+1,] <- c(max_ind_denom, fft_Signal_denom)

    
    
      AC_Signal_num <- fft_Signal_num[max_ind_num]*2/(len(red)/2) # exclude DC term; *2/window gives the original amplitude info
      AC_Signal_denom <- fft_Signal_denom[max_ind_denom]*2/(len(ir)/2)

      AC_Signal_grn <- 0
      if(is_green) AC_Signal_grn <- fft_Signal_grn[max_ind_grn]*2/(len(grn)/2)

#       if((i > 130) && (i < 200)){
#         AC_Signal_num <- AC_Signal_num*0.5
#         AC_Signal_denom <- AC_Signal_denom*0.5
#       }
    

      skew_num <- sample_skewness(fft_Signal_num^2)
      skew_denom <- sample_skewness(fft_Signal_denom^2)
      skew_grn <- 0
      if(is_green) skew_grn <- sample_skewness(fft_Signal_grn)


      if((smoothACDC != 0) && (i > 0)){
        AC_Signal_num <- smoothACDC*output_mat[i,1] + (1-smoothACDC)*AC_Signal_num
        DC_Signal_num <- smoothACDC*output_mat[i,2] + (1-smoothACDC)*DC_Signal_num
        AC_Signal_denom <- smoothACDC*output_mat[i,3] + (1-smoothACDC)*AC_Signal_denom
        DC_Signal_denom <- smoothACDC*output_mat[i,4] + (1-smoothACDC)*DC_Signal_denom
      }


      output_mat[i+1,] <- c(AC_Signal_num, DC_Signal_num, 
                            AC_Signal_denom, DC_Signal_denom, 
                            AC_Signal_grn, DC_Signal_grn,
                            skew_num, skew_denom, skew_grn)



      #### CALCULATE SPO2 USING AC AND DC VALUES ####    
      spo2_out <- spo2_caleb_fun(AC_Signal_num, DC_Signal_num, AC_Signal_denom, DC_Signal_denom)
    
#     }
    
    ################################################################################
    ######## FFT LOGIC (TESTING EVERY BIN)
    
    if(fftLogicMode == 2){
      
      if(Fs != 50)
        warning("This FFT mode only set up for 50Hz sampling frequency.")
      
      fft_num_rel   <- fft_Signal_num[3:128]
      fft_denom_rel <- fft_Signal_denom[3:128]
      
      spo2_candidates <- numeric(len(fft_num_rel))
      for(j in 1:len(fft_num_rel)){
        
        AC_num_tmp   <- fft_num_rel[j]*2/(windowSize/2)
        AC_denom_tmp <- fft_denom_rel[j]*2/(windowSize/2)
        
        spo2_candidates[j] <- spo2_caleb_fun(AC_num_tmp, DC_Signal_num, AC_denom_tmp, DC_Signal_denom)
        
      }
      
      spo2_candidate_list[[i+1]] <- spo2_candidates
      
#       if(i != 0){
#         
# #         spo2_out <- spo2_candidates[which.min( abs(spo2_candidates - spo2_prev) )]
#         
#         spo2_out <- max(spo2_candidates)
#         
#       }
      
      
    }





    
    
    #### CHECK SPO2 DIFFERENCES FOR UNREALISTIC DIPS ####
      
    if(spo2_check_diffs & (spo2_prev != 0)){
        
      if((spo2_out-spo2_prev < -0.02) & (spo2_prev < 1))
        spo2_out <- spo2_prev
        
    }
      
    spo2_prev <- spo2_out
      
    
    output <- rbind(output, c(spo2_out))
    
    if(print_progress)
      cat(round(100*(i+1)/n2, 1), "%\n")
    
  }
  
  colnames(output) <- c("spo2")
  return(list(spo2_output=output, spo2_candidate_list=spo2_candidate_list, 
              acdc_output=output_mat, qm_output=qm_mat, 
              fft_out_list=list(fft_num_out, fft_denom_out), window_means=window_means,
              filt_signals=cbind(red_new, ir_new), orig_signals=cbind(Signal_num, Signal_denom)))
}




######################################################################################
######################################################################################
######################################################################################



fftSpO2_R <- function(Signal_num, Signal_denom, Signal_green, windowSize=2^8, 
                      Fs=50, overlap=50, windowing=FALSE, subtractMeans=TRUE,
                      filtType=c("butter", "fir50", "gauss")[1], 
                      gauss_bw=8, gauss_ma=100,
                      skipSamples=500, fftLogicMode=c(0,1,2), logicSD=5, 
                      fftInterp=FALSE, interpBW=2,
                      useMaxNum=FALSE, useMaxDenom=FALSE, useMaxNumDenom=FALSE) 
{
  # data prep
  # Signal_num: signal used in the numerator of the R ratio, so for normal SpO2 this is 660nm
  # Signal_denom signal used in the denominator of the R ratio, so for normal SpO2 this is 940nm
  
  
  usegreen <- !missing(Signal_green)
  
  ################################################################################
  ######## MEAN SUBSTRACTION
  
#   if(subtractMeans){
#     
#     mean1 <- Signal_num[1]
#     mean2 <- Signal_denom[1]
#     for(i in 2:len(Signal_num)){
#       mean1 <- c(mean1, mean1[i-1]*((i-1)/i) + Signal_num[i]*(1/i))
#       mean2 <- c(mean2, mean2[i-1]*((i-1)/i) + Signal_denom[i]*(1/i))
#     }
#     
#     Signal_num   <- Signal_num - mean1
#     Signal_denom <- Signal_denom - mean2
#     
#   }
  
  
  ################################################################################
  ######## FILTERING AND TRUNCATION OF INITIAL FILTER OUTPUT
  
  
  if(filtType == "butter"){
    red_new <- filter(butter(6,c(30/60/(Fs/2),210/60/(Fs/2)), type="pass"), Signal_num)
    ir_new  <- filter(butter(6,c(30/60/(Fs/2),210/60/(Fs/2)), type="pass"), Signal_denom)
    
    if(usegreen)
      grn_new <- filter(butter(6,c(30/60/(Fs/2),210/60/(Fs/2)), type="pass"), Signal_green)
    
  }
  else if(filtType == "fir50"){
    red_new <- FIRfilt(Signal_num)
    ir_new  <- FIRfilt(Signal_denom)
    
    if(usegreen)
      grn_new <- FIRfilt(Signal_green)
  }
  else if(filtType == "gauss"){
    red_new <- ksmooth(1:len(Signal_num), Signal_num, "normal", bandw=gauss_bw)$y
    ir_new <- ksmooth(1:len(Signal_denom), Signal_denom, "normal", bandw=gauss_bw)$y
    
    red_new <- red_new - movingAve(red_new, gauss_ma)
    ir_new <- ir_new - movingAve(ir_new, gauss_ma)
    
    if(usegreen){
      grn_new <- ksmooth(1:len(Signal_green), Signal_green, "normal", bandw=gauss_bw)$y
      grn_new <- grn_new - movingAve(grn_new, gauss_ma)
    }
    
  }
  
  
  
  
  if(skipSamples > 0){
    red_new <- red_new[-c(1:skipSamples)]
    ir_new <- ir_new[-c(1:skipSamples)]
    
    Signal_num <- Signal_num[-c(1:skipSamples)]
    Signal_denom <- Signal_denom[-c(1:skipSamples)]
    
    if(usegreen){
      grn_new <- grn_new[-c(1:skipSamples)]
      Signal_green <- Signal_green[-c(1:skipSamples)]
    }
  }
  
  
  ################################################################################
  ######## INITIALISATION OF ITERATIVE WINDOWING
  
  
  #initialize
  init    <- 1
  output  <-data.frame(NULL)
  windows <-NULL
  
  if(length(Signal_num)==length(Signal_denom)) 
    n <- length(red_new)
  else 
    stop("Length of Signal_num and Signal_denom not the same")
  
  n2 <- floor((n-windowSize)/overlap)+1 # number of spectrum frames 
  
  window_w <- gausswin(windowSize)
  
  
  csum_mat <- matrix(0, nr=n2, nc=2)
  spec_out_mat_num <- matrix(0, nr=n2, nc=(windowSize/2)+2)
  spec_out_mat_denom <- matrix(0, nr=n2, nc=(windowSize/2)+2)
  spec_out_mat_green <- matrix(0, nr=n2, nc=(windowSize/2)+2)
  
  spec_out_mat_qms <- matrix(0, nr=n2, nc=6)
  colnames(spec_out_mat_qms) <- c("num_qm_sf", "num_qm_sf_ent", "num_qm_v1",
                                  "denom_qm_sf", "denom_qm_sf_ent", "denom_qm_v1")
  
  
  fft_prev_num <- NULL
  fft_prev_denom <- NULL
  
  freqs <- seq(0, (Fs/2), length=(windowSize/2))
  
  #FFT
  for(i in 0:(n2-1)){
    
    # Select window
    indexvec <- 1:windowSize+overlap*i
    red      <- red_new[indexvec]
    ir       <- ir_new[indexvec]
    
    if(usegreen)
      grn    <- grn_new[indexvec]
    
    
    
    # Perform FFT
    if(windowing==TRUE) { 
      fft_Signal_num   <- (abs(fft(    red   *window_w)[1:(length(red)/2)])) 
      fft_Signal_denom <- (abs(fft(   ir   *window_w)[1:(length(ir)/2)])) 
    }
    
    if(windowing==FALSE) { 
      fft_Signal_num   <-  (abs(fft(red)[1:(length(red)/2)]))  
      fft_Signal_denom <-  (abs(fft(ir)[1:(length(ir)/2)])) 
    }
    
    if(usegreen)
      fft_Signal_green <- (abs(fft(grn)[1:(length(grn)/2)]))
    
    

    
    spec_out_mat_qms[i+1,] <- c(QM_SF(fft_Signal_num), QM_SF_Entropy(fft_Signal_num), QM_V1(fft_Signal_num),
                                QM_SF(fft_Signal_denom), QM_SF_Entropy(fft_Signal_denom), QM_V1(fft_Signal_denom))
    
    
    
    ################################################################################
    ######## FFT NO LOGIC
    
    max_num <- max(fft_Signal_num[-c(1:3)])
    max_ind_num <- which(fft_Signal_num[-c(1:3)]==max_num)+3
    
    max_denom <- max(fft_Signal_denom[-c(1:3)])
    max_ind_denom <- which(fft_Signal_denom[-c(1:3)]==max_denom)+3
    
    if(usegreen)
      max_ind_grn <- which(fft_Signal_green[-c(1:3)]==max(fft_Signal_green[-c(1:3)]))+3
    
    ################################################################################
    ######## FFT LOGIC (GAUSSIAN WEIGHTS)
    
    fft_num_temp <- fft_Signal_num[-c(1:3)]
    fft_denom_temp <- fft_Signal_denom[-c(1:3)]
    if(usegreen) fft_grn_temp <- fft_Signal_green[-c(1:3)]
    
    if(i == 0){
      max_ind_num_fftl <- which(fft_num_temp == max(fft_num_temp))+3
      max_ind_denom_fftl <- which(fft_denom_temp == max(fft_denom_temp))+3
      if(usegreen) max_ind_grn_fftl <- which(fft_grn_temp == max(fft_grn_temp))+3
    }
    else{
      
      gauss_score_num <- dnorm(freqs, freqs[max_ind_num_fftl], logicSD/60)[-c(1:3)]
      gauss_score_denom <- dnorm(freqs, freqs[max_ind_denom_fftl], logicSD/60)[-c(1:3)]
      if(usegreen) gauss_score_grn <- dnorm(freqs, freqs[max_ind_grn_fftl], logicSD/60)[-c(1:3)]
      
      fft_num_temp <- fft_num_temp*gauss_score_num
      fft_denom_temp <- fft_denom_temp*gauss_score_denom
      if(usegreen) fft_grn_temp <- fft_grn_temp*gauss_score_grn
      
      max_ind_num_fftl <- which(fft_num_temp == max(fft_num_temp))+3
      max_ind_denom_fftl <- which(fft_denom_temp == max(fft_denom_temp))+3
      if(usegreen) max_ind_grn_fftl <- which(fft_grn_temp == max(fft_grn_temp))+3
    }
    
    
#     spec_out_mat_num[i+1,] <- c(fft_Signal_num, max_ind_num, max_ind_num_fftl)
#     spec_out_mat_denom[i+1,] <- c(fft_Signal_denom, max_ind_denom, max_ind_num_fftl)
#     
#     if(usegreen)
#       spec_out_mat_green[i+1,] <- c(fft_Signal_green, max_ind_grn, max_ind_grn_fftl)
    
    
    ################################################################################
    ######## CHOOSE BINS BASED ON LOGIC IDENTIFIER
    
    
    if(fftLogicMode == 1){
      max_ind_num <- max_ind_num_fftl
      max_ind_denom <- max_ind_denom_fftl
    }
    else if(fftLogicMode == 2){
      max_ind_num <- max_ind_grn
      max_ind_denom <- max_ind_grn
    }
    else if(fftLogicMode == 3){
      max_ind_num <- max_ind_grn_fftl
      max_ind_denom <- max_ind_grn_fftl
    }
    
    if(useMaxNumDenom){
      if(max_num > max_denom)
        useMaxNum <- TRUE
      else
        useMaxDenom <- TRUE
    }
    
    if(useMaxNum)
      max_ind_denom <- max_ind_num
    
    if(useMaxDenom)
      max_ind_num <- max_ind_denom
    
    
    if(fftInterp){
      ind1 <- max(2, max_ind_num-interpBW)
      ind2 <- max_ind_num+interpBW
      fft_num_ave <- mean(fft_Signal_num[ind1:ind2])
      
      ind1 <- max(2, max_ind_denom-interpBW)
      ind2 <- max_ind_denom+interpBW
      fft_denom_ave <- mean(fft_Signal_denom[ind1:ind2])
      
      AC_Signal_num <- fft_num_ave*2/(windowSize/2)
      AC_Signal_denom <- fft_denom_ave*2/(windowSize/2)
      
    }
    else{
      AC_Signal_num <- fft_Signal_num[max_ind_num]*2/(windowSize/2) # exclude DC term; *2/window gives the original amplitude info
      AC_Signal_denom <- fft_Signal_denom[max_ind_denom]*2/(windowSize/2)
    }
    
    
    DC_Signal_num <- mean(Signal_num[indexvec])
    DC_Signal_denom <- mean(Signal_denom[indexvec])
    
    
    csum_mat[i+1,] <- c(sum(as.numeric(Signal_num[indexvec])), sum(as.numeric(Signal_denom[indexvec])))
    
    output <- rbind(output, c(AC_Signal_num, DC_Signal_num, AC_Signal_denom, DC_Signal_denom))
    
  }
  
  colnames(output) <- c("AC_Signal_num","DC_Signal_num","AC_Signal_denom","DC_Signal_denom")
  return(list(output, spec_out_mat_qms))
}




######################################################################################
######################################################################################
######################################################################################






calabFreeSpO2 <- function(spo2_fft_obj, pF=0.75,e1x = 0.08, e2x = 0.29, e1y = 0.81, e2y = 0.18, e3x = 6.49928, e3y = 8.986, 
                          logBias=6.359e-07, smoothAlpha=0, clip_at_100=T)
{
  # 1=660nm    2=950nm 3=525nm
  # x = HbO y=Hb
  # pF -> 0.7 vir vinger data
  # pF -> 0.85 vir gewrig data
  # pF = Lir/Lred  
  # L = pathlength1792*2
  
  if(ncol(spo2_fft_obj) == 4){
  
    dAl1 = log(  (spo2_fft_obj$DC_Signal_num - 0.5*spo2_fft_obj$AC_Signal_num + logBias)/(spo2_fft_obj$DC_Signal_num + 0.5*spo2_fft_obj$AC_Signal_num + logBias))
    dAl2 = log(  (spo2_fft_obj$DC_Signal_denom - 0.5*spo2_fft_obj$AC_Signal_denom + logBias)/(spo2_fft_obj$DC_Signal_denom + 0.5*spo2_fft_obj$AC_Signal_denom + logBias))
    
    
    spo2_out <- (dAl2*e1y - dAl1*e2y*pF)/(dAl2*(-e1x + e1y) + dAl1*(e2x - e2y)*pF)
    
  }
  else if(ncol(spo2_fft_obj == 1))
    spo2_out <- spo2_fft_obj[,1] 
  
  
  if(!missing(smoothAlpha)){
    if(smoothAlpha[1] != 0){
      
      if(length(smoothAlpha) == 1)
        smoothAlpha <- rep(smoothAlpha, length(spo2_out))
    
      spo2_smooth <- spo2_out
      for(i in 2:length(spo2_out))
        spo2_smooth[i] <- smoothAlpha[i]*spo2_smooth[i-1] + (1-smoothAlpha[i])*spo2_out[i]
    
      spo2_out <- spo2_smooth
    
    }
  }
  
  
  if(clip_at_100)
    spo2_out[spo2_out >= 1] <- 0.999
  
  
  return(spo2_out)
  
}




######################################################################################
######################################################################################
######################################################################################





convertToCurrentV2 <- function(grn_16bit, Isub, Rf){
  # AFE4404 single stage gain and subtraction current
  Volt <- (grn_16bit - 2^15)/(2^15)*1.2 
  I <- (Volt/2/Rf) - Isub
  return(I)
}






######################################################################################
######################################################################################
######################################################################################



rawDataCFormat <- function(fpath_in, fpath_out)
{
  dat <- read.table(fpath_in, head=T, sep=",")
}




######################################################################################
######################################################################################
######################################################################################



multSpikeFilter <- function(x, thresh=1e4)
{
  fac <- 1
  n <- length(x)
  y <- numeric(n)
  
  fvec <- numeric(n)
  
  y[1] <- x[1]
  for(i in 2:n){
    
    if(abs(x[i] - x[i-1]) > thresh)
      fac <- (x[i-1]*fac)/x[i]
    
    y[i] <- x[i]*fac
    fvec[i] <- fac
  }
  
  return(list(y=y, fac=fvec))
}




# xs <- seq(0, 100, 0.1)
# xx <- sin(xs)
# xx[250:500] <- xx[250:500] + 10
# xx[501:750] <- xx[501:750] - 7
# 
# w() ; lplot(xx)
# 
# yy <- multSpikeFilter(xx, 5)
# 
# w() ; plot(yy, pch=19, cex=0.7)






######################################################################################
######################################################################################
######################################################################################



oneSampSpikeFilter <- function(x, thresh=100000)
{
  n <- length(x)
  
  i <- 2
  while(i < n){
    if(abs(x[i]-x[i-1]) > thresh)
      x[i] <- (x[i+1] + x[i-1])/2
      
    i <- i+1
  }
  
  return(x)
}






######################################################################################
######################################################################################
######################################################################################


sample_skewness <- function(xx)
{
  n <- length(xx)
  
  xx_scaled <- as.numeric(scale(xx, scale=F))
  
  mean_x2 <- ( mean(xx_scaled^2) )^(1.5)
  mean_x3 <- mean(xx_scaled^3)
  
  skewn <- mean_x3/mean_x2
  
  return( log2(abs(skewn)+1)/3  )
}



######################################################################################
######################################################################################
######################################################################################



LMSfilt <- function(y, x, m=5, alpha=0.01)
{
  n <- len(y)
  y_filt <- numeric(n)
  
  weights <- numeric(m)
  x_buffer <- numeric(m)
  err <- 0
  
  for(i in 1:n){
    
    ## update buffer
    x_buffer <- c(x[i], x_buffer[1:(m-1)])
    
    ## run LMS filter
    y_filt[i] <- sum(weights * x_buffer)
    err <- y[i] - y_filt[i]
    
    ## update filter
    x_sum <- sum(x_buffer^2)+1
    weights <- weights + err*alpha*x_buffer/x_sum
  }
  
  return(y_filt)
}





######################################################################################
######################################################################################
######################################################################################




getSpO2PointEstimate <- function(y, y_qm, min_time=10, min_qm=90, max_rsd=5)
{
  n <- len(y)
  if(len(y_qm) != n) stop("SpO2 prediction and QM vectors lengths unequal.")
  
  rsd <- c(rep(0, min_time-1), expSmooth(runningSD(y_qm, min_time), alpha=0.5))
  
  t_count <- 0
  success <- FALSE
  for(i in 1:n){
    if((y_qm[i] > min_qm) & (rsd[i] < max_rsd))
      t_count <- t_count+1
    else
      t_count <- 0
    
    if(t_count == min_time){
      success <- TRUE
      break
    }
  }
  
  
  if(success){
    t_tot <- i
    spo2 <- mean(y[(i-min_time+1):i])
  }
  else{
    t_tot <- 0
    spo2 <- 0
  }
  
  ###########################################################################
  
  cat("SpO2 point estimate: \t", round(spo2,2), "%  |  ")
  cat("obtained after: \t", t_tot, "s\n")
  
  return(invisible(c(spo2, t_tot)))
}