source("../useful_funs.r")
source("./sp02_funs.r")



fpath1 <- "~/HealthQ/Data/OxySat/SpO2 robustness drop tests 20150408/"

data_mojo1 <- getDataFromDir(paste0(fpath1, "Goldfinger/"), head=T, dataE=F)
data_mojo2 <- getDataFromDir(paste0(fpath1, "Mojo 5172/"), head=T, dataE=F)
data_wins <- getDataFromDir(paste0(fpath1, "Winspiro/"), head=T)

##################################################################################################
##################################################################################################


offset_mat <- matrix(c(1, 10,
                       10, 1,
                       8, 1,
                       7, 1,
                       1, 80,
                       1, 1,
                       1, 3,
                       8, 1,
                       10, 1), nrow=9, byrow=T)



zPlot(dat[,c(3,4,10,13,14)], par_new=T, col_f=c("red", "forestgreen", "yellow", "blue", "lightblue"))
zPlot(dat[,c(3,4,16,17)], par_new=T, col_f=c("red", "forestgreen", "blue", "lightblue"))


i <- 1

# out_list <- vector("list", len(data_wins))

for(i in 1:len(data_wins)){
  
  
  wins <- data_wins[[i]][,3]
  
  
  ###################
  #### dat1
  
  dat <- read.table(paste0(fpath1, "Goldfinger/", data_mojo1[i]), head=T, sep=",")
  
  dat <- dat[dat[,1] != 0,]
  dat <- dat[-c(1:500),]
  curr1 <- curr2 <- curr3 <- matrix(0, nr=nrow(dat)-1, nc=4)
  for(j in 1:4){
    if(j < 3){
      Rf <- dat[,9]
      led <- dat[,11]
    }
    else{
      Rf <- dat[,10]
      led <- dat[,j+13]
    }
    
    safe_term <- 0
    if(j == 2){
      safe_term <- 1
      led <- numeric(nrow(dat))
    }
    
    dat_tmp <- dat[-1,j] 
    isub_tmp <- 10*dat[,10+j]
    isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
    Rf <- Rf[1:(len(Rf)-1)]
    led <- led[1:(len(led)-1)]
    curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
    curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)  
    curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)[[1]]
  }
  
#   zPlot(cbind(curr2[,c(1,3:4)]-curr2[,2]), par_new=T, col_f=c("lightblue", "red", "forestgreen"))
  
  redI_clipped1 <- curr3[,3]-curr3[,2]
  nirI_clipped1 <- curr3[,4]-curr3[,2]

#   redI_tr <- curr3[,3]/dat[1:(nrow(dat)-1),16]
#   nirI_tr <- curr3[,4]/dat[1:(nrow(dat)-1),17]

# w() ; lplot(nirI_tr/redI_tr, ylim=c(0.8,1.6))

#   redI_tr <- removeCurrentDiscont2_m(redI_tr, led=dat[1:(nrow(dat)-1),16])[[1]]
#   nirI_tr <- removeCurrentDiscont2_m(nirI_tr, led=dat[1:(nrow(dat)-1),17])[[1]]

  
  
  ###################
  #### dat2
  
  dat <- read.table(paste0(fpath1, "Mojo 5172/", data_mojo2[i]), head=T, sep=",")
  
  dat <- dat[dat[,1] != 0,]
  dat <- dat[-c(1:500),]
  curr1 <- curr2 <- curr3 <- matrix(0, nr=nrow(dat)-1, nc=4)
  for(j in 1:4){
    if(j < 3){
      Rf <- dat[,9]
      led <- dat[,11]
    }
    else{
      Rf <- dat[,10]
      led <- dat[,j+13]
    }
    
    safe_term <- 0
    if(j == 2){
      safe_term <- 1
      led <- numeric(nrow(dat))
    }
    
    dat_tmp <- dat[-1,j] 
    isub_tmp <- 10*dat[,10+j]
    isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
    Rf <- Rf[1:(len(Rf)-1)]
    led <- led[1:(len(led)-1)]
    curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
    curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)  
    curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)[[1]]
  }
  
#   zPlot(cbind(curr2[,c(1,3:4)]-curr2[,2]), par_new=T, col_f=c("lightblue", "red", "forestgreen"))
  
  
  redI_clipped2 <- curr3[,3]#-curr3[,2]
  nirI_clipped2 <- curr3[,4]#-curr3[,2] 
  
  
  
  #######################################
  
  out <- fftSpO2_Cdev(redI_clipped1, nirI_clipped1, filtType="fir50", adapF=F, smoothACDC=0.5,
                      windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F,
                      spo2_caleb_pf=0.6)
  spo2_1  <- 100*calabFreeSpO2(out[[1]], clip=F)
#   r_ratio_1 <- ((out[[3]][,1]/out[[3]][,2]) / (out[[3]][,3]/out[[3]][,4]))

#   out_list[[i]] <- list(wins, spo2_1, r_ratio_1)
  
  
  
  out <- fftSpO2_Cdev(redI_clipped1, nirI_clipped1, curr3[,1], filtType="fir50", adapF=F, smoothACDC=0.5,
                      windowSize=2^7, fftL=0, logicSD=5, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F,
                      spo2_caleb_pf=0.6)
  spo2_2  <- 100*calabFreeSpO2(out[[1]], clip=F)
  
  
  m_name <- substring(names(data_wins)[[i]], 1, nchar(names(data_wins)[[i]])-9)

  w() ; lplot(wins[-c(1:offset_mat[i,1])], lwd=3, col="black", ylim=c(70,105), ylab="spo2",
              main=m_name)
  lines(round(spo2_1[-c(1:offset_mat[i,2])]), lwd=2, col="blue")
  lines(round(spo2_2[-c(1:offset_mat[i,2])]), lwd=2, col="forestgreen")
#   legend("bottomright", bg="white", lwd=2, col=c("black", "blue", "lightgreen"),
#          leg=c("winspiro", "goldfinger"), cex=0.75)
#   par(new=T) ; lplot(nirI_tr/redI_tr, col="red", ann=F, yaxt="n", xaxt="n", ylim=c(0.5,2))#, ylim=range(c(redI_tr, nirI_tr)))
# #   lines(nirI_tr, col="orange")
#   axis(4)
#   par(new=T) ; lplot(out[[3]][,1]/out[[3]][,3], col="red", ann=F, yaxt="n", xaxt="n")
#   axis(4)
  
  savePlot(paste0("spo2_results/", m_name), "png")
  graphics.off()
  cat(round(100*i/len(data_wins)), "%\n")
}







##############################################################################################

w() ; par(ask=T)
for(j in 1:len(out[[2]])){
  
  plot(out[[2]][[j]], pch=19, col="blue", ylim=c(0.7,1.1))
#   hist(out[[2]][[j]], breaks=100, xlim=c(0.8,1.1))
  
}




##############################################################################################
##############################################################################################
#####     SPO2 CALIBRATION     ###############################################################
##############################################################################################




y <- wins[-c(1:offset_mat[i,1])]
x_s <- spo2_1[-c(1:offset_mat[i,2])]
x_r <- ((out[[3]][,1]/out[[3]][,2]) / (out[[3]][,3]/out[[3]][,4]))[-c(1:offset_mat[i,2])]

maxlen <- min(c(len(y), len(x_s)))
y <- y[1:maxlen]
x_s <- x_s[1:maxlen]
x_r <- x_r[1:maxlen]


lm_s_out <- lm(y~x_s+I(x_s^2))
lm_r_out <- lm(y~x_r+I(x_r^2))

w() ; lplot(y, lwd=2, col="black")
lines(lm_s_out$fitted.values, lwd=2, col="blue")
lines(lm_r_out$fitted.values, lwd=2, col="forestgreen")


#########################################
## concatenate results of all drop tests
#########################################

out_mat <- NULL
for(i in 1:len(out_list)){
  
  y <- (out_list[[i]][[1]])[-c(1:offset_mat[i,1])]
  x_s <- (out_list[[i]][[2]])[-c(1:offset_mat[i,2])]
  x_r <- (out_list[[i]][[3]])[-c(1:offset_mat[i,2])]
  
  maxlen <- min(c(len(t), len(x_s), len(x_r)))
  y <- y[1:maxlen]  
  x_s <- x_s[1:maxlen]
  x_r <- x_r[1:maxlen]
  
  out_mat <- rbind(out_mat, cbind(i, y, x_s, x_r))
  
}


#########################################
## fit global regression model
#########################################


y <- out_mat[,2]
x_s <- out_mat[,3]
x_r <- out_mat[,4]

lm_s_out <- lm(y~x_s+I(x_s^2))
lm_r_out <- lm(y~x_r+I(x_r^2))



w() ; par(ask=T)
for(i in 1:len(out_list)){
  
  y <- (out_list[[i]][[1]])[-c(1:offset_mat[i,1])]
  x_s <- (out_list[[i]][[2]])[-c(1:offset_mat[i,2])]
  x_r <- (out_list[[i]][[3]])[-c(1:offset_mat[i,2])]
  
  spo2_1 <- lm_s_out$coefficients[1] + lm_s_out$coefficients[2]*x_s + lm_s_out$coefficients[3]*(x_s^2)
  spo2_2 <- lm_r_out$coefficients[1] + lm_r_out$coefficients[2]*x_r + lm_r_out$coefficients[3]*(x_r^2)
  
  lplot(y, lwd=3, col="black", ylim=c(70,105),
        main=substring(names(data_wins)[[i]], 1, nchar(names(data_wins)[[i]])-9))
  lines(spo2_1, lwd=2, col="blue")
  lines(x_s, lwd=2, col="red")
  
}









