source("../useful_funs.r")
source("./sp02_funs.r")



dat_format <- NULL
datlist1 <- datlist2 <- datlist3 <- datlist4 <- NULL

datlist1 <- getDataFromDir("~/HealthQ/Data/Partners/partron/spo2 testing/Partron FingerData_150423_Wiaan/", head=T)
dat_format <- rep(1, len(datlist1))

datlist2 <- getDataFromDir("~/HealthQ/Data/Partners/partron/spo2 testing/Partron Round 2/", head=T)
dat_format <- c(dat_format, rep(2, len(datlist2)))

# datlist3 <- getDataFromDir("~/HealthQ/Data/Partners/partron/spo2 testing/Partron Round 3 algo v3.1.150/", head=T)
# dat_format <- c(dat_format, rep(3, len(datlist3)))

datlist4 <- getDataFromDir("~/HealthQ/Data/Partners/partron/spo2 testing/Partron Round 4 (with ref) 20150513/", head=T, skip=2)
dat_format <- c(dat_format, rep(4, len(datlist4)))

datlist <- c(datlist1, datlist2, datlist3, datlist4)
rm(datlist1, datlist2, datlist3, datlist4)




i <- 2

spo2_list <- vector("list", len(datlist))
for(i in 1:len(datlist)){

dat <- datlist[[i]]

if(dat_format[i] == 1){
  dat <- cbind(dat[1:(nrow(dat)-1),1:7], dat[-1,9:17])
  dat[,10:13] <- 10*dat[,10:13]
}
if(dat_format[i] == 2){
  dat <- cbind(dat[1:(nrow(dat)-2),1:7], dat[2:(nrow(dat)-1),8:16])
  dat[,10:13] <- 10*dat[,10:13]
}
if(dat_format[i] == 3){
  dat <- cbind(dat[1:(nrow(dat)-2),1:7], dat[2:(nrow(dat)-1),8:11, 13, 12, 14, 16, 15])
  dat[,10:13] <- 10*dat[,10:13]
  dat[,3:4] <- dat[,4:3]
}
if(dat_format[i] == 4){
  dat <- dat[1:(nrow(dat)-1), c(4,5,6,7, 1:3, 14:15, 19:20, 22, 21, 16, 18, 17)]
  dat <- cbind(dat[1:(nrow(dat)-2),1:7], dat[2:(nrow(dat)-1),8:16])
  dat[,10:13] <- 10*dat[,10:13]
  
  ref <- read.table("~/HealthQ/Data/Partners/partron/spo2 testing/Partron Round 4 (with ref) 20150513/reference_data.txt", head=T)
}


# write.table2(dat, paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/data/SpO2Partron01/", names(datlist)[i], ".txt"))

dat <- dat[-c(1:5),]
dat <- dat[dat[,1] != 0,]
curr1 <- curr2 <- curr3 <- matrix(0, nr=nrow(dat), nc=4)
for(j in 1:4){
  if(j < 3){
    Rf <- dat[,8]
    led <- dat[,14]
  }
  else{
    Rf <- dat[,9]
    led <- dat[,j+12]
  }
  
  safe_term <- 0
  if(j == 2){
    safe_term <- 1
    led <- numeric(nrow(dat))
  }
  
  dat_tmp <- dat[,j] 
  isub_tmp <- dat[,9+j]
  #   isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
  #   Rf <- Rf[1:(len(Rf)-1)]
  #   led <- led[1:(len(led)-1)]
  curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
  curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e8)  
  curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e8)[[1]]
}

redI_blank_clipped <- removeCurrentDiscont2_m(curr1[,3]-curr1[,2], dat[,8]+dat[,9], dat[,12]+dat[,11], dat[,15], 1e8)[[1]]
nirI_blank_clipped <- removeCurrentDiscont2_m(curr1[,4]-curr1[,2], dat[,8]+dat[,9], dat[,13]+dat[,11], dat[,16], 1e8)[[1]]

 zPlot(curr3[,c(1,3:4)], par_new=T, col_f=c("lightblue", "red", "forestgreen"))


# out <- fftSpO2_Cdev(redI_blank_clipped, nirI_blank_clipped, filtType="fir50", smoothACDC=0.8, skipSamples=100,
#                     windowSize=2^8, spo2_check=F, useMaxDenom=F, spo2_caleb_pf=0.67, 
#                     spo2_new_method=F)
# spo2_1  <- 100*calabFreeSpO2(out[[1]], clip=F)



out <- fftSpO2_Cdev(curr3[,3], curr3[,4], filtType="fir50", smoothACDC=0.8, skipSamples=100,
                    windowSize=2^8, spo2_check=F, useMaxDenom=F, spo2_caleb_pf=0.67, 
                    spo2_new_method=F, newFFTMethod=F)
spo2_2  <- 100*calabFreeSpO2(out[[1]], clip=F, smoothAlpha=0.7)



spo2_list[[i]] <- spo2_2

w() ; plot(spo2_1, type="n", ylim=c(90, 105), main=names(datlist)[i])
# lines(round(spo2_1, 1), lwd=2, col="forestgreen")
lines(round(spo2_2, 1), lwd=2, col="blue")
abline(h=c(95,100), lty=2)


if(dat_format[i] == 4){
  lines(ref[ref[,3]==i, 1], lwd=2, col="red")
  lines(ref[ref[,3]==i, 2], lwd=2, col="purple", lty=5)
}



savePlot(paste0("spo2_results/", names(datlist)[i]), "png")
graphics.off()
cat(round(100*i/len(datlist)), "%\n")

}



# w() ; par(ask=T)
# for(i in 1:len(datlist)){
#   
#   lplot(spo2_list_b[[i]], lwd=2, col="red", ylim=c(85,105))
#   lines(spo2_list[[i]], lwd=2, col="blue")
#   
# }




################################################################################################
################################################################################################

i <- 2

# w() ; par(ask=T)
cout_list <- vector("list", len(datlist)-1)
for(i in 2:len(datlist)){

cout <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out_", names(datlist)[i], ".txt"), sep=",")
cout_list[[i]] <- cout

# c_spo2 <- cout[,4]/10
# c_spo2 <- c_spo2[-c(1:which(diff(c_spo2) != 0)[1])]
# c_spo2 <- c_spo2[seq(1, len(c_spo2), 50)]
# 
# lplot(spo2_list[[i]], lwd=2, col="red", ylim=c(80,110), main=names(datlist[i]))
# lines(c_spo2, lwd=2, col="blue")


}


################################################################################################
################################################################################################



# w() ; par(ask=T)
# rmse <- numeric(5)
# for(i in 1:5){
#   xy <- ref[ref[,3]==i, 1:2]
#   lplot(xy[,1], lwd=3, col="red", ylim=c(90,105))
#   lines(xy[,2], lwd=2, col="blue")
#   legend("bottomleft", lwd=2, col=c("red", "blue"), leg=c("reference", "lifeq"))
#   mape <- 100*mean(abs((xy[,1] - xy[,2])/xy[,1]))
#   rmse[i] <- sqrt(mean( (xy[,1]-xy[,2])^2 ))
#   legend("bottomright", leg=paste("root MSE =", round(rmse[i],2)))
# }









