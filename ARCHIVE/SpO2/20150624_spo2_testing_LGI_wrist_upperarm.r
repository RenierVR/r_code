

source("../useful_funs.r")
source("./sp02_funs.r")


fpath1 <- "D:/HealthQ/Data/LGInnotek/20150624_SPO2_WRIST_UPPERARM/"
datlist <- getDataFromDir(fpath1, head=T)
winslist <- getDataFromDir(paste0(fpath1, "winspiro/"), head=T)


############################################################################################
############################################################################################



i <- 1
for(i in 1:len(datlist)){
  
  wins <- winslist[[i]][,3]
  dat <- datlist[[i]]
  
  dat <- dat[dat[,1] != 0,]
  dat <- dat[10:nrow(dat),]
  curr1 <- curr2 <- curr3 <- matrix(0, nrow=nrow(dat), nc=4)
  for(j in 1:4){
    if(j < 3){
      Rf <- dat[,8]
      led <- dat[,14]
    }
    else{
      Rf <- dat[,9]
      led <- dat[,j+12]
    }
    
    safe_term <- 0
    if(j == 2){
      safe_term <- 1
      led <- numeric(nrow(dat))
    }
    
    dat_tmp <- dat[,j] 
    isub_tmp <- dat[,9+j]
    curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
    curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)  
    curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)[[1]]
  }
  
  
  redI_blank_clipped1 <- removeCurrentDiscont2_m(curr1[,3]-curr1[,2], dat[,8]+dat[,9], dat[,12]+dat[,11], dat[,15], 1e8)[[1]]
  nirI_blank_clipped1 <- removeCurrentDiscont2_m(curr1[,4]-curr1[,2], dat[,8]+dat[,9], dat[,13]+dat[,11], dat[,16], 1e8)[[1]]
  
  
  
  #####################################################################
  
  
  fs=50 ; winsize <- 2^8
  # fs=25 ; winsize <- 2^7
  
  out <- fftSpO2_Cdev(redI_blank_clipped1, nirI_blank_clipped1, Fs=fs, filtType="fir50", smoothACDC=0.8,
                      windowSize=winsize, spo2_check=F, useMaxDenom=F, spo2_caleb_pf=0.7)
  spo2_1  <- 100*calabFreeSpO2(out[[1]], clip=F, smoothAlpha=0.7)
  spo2_cqm_1 <- out[[4]][,4]
  
  m_name <- names(datlist)[i]
  
  
  # s1_qm_sd <- rbind(matrix(0,2,2), apply(s1_qm, 2, runningSD, 3))
  
  
  m_spo2 <- dat[,22]/10
  m_spo2 <- m_spo2[(which(m_spo2 != 0)[1]):len(m_spo2)]
  m_spo2 <- m_spo2[seq(1, len(m_spo2), fs)]
  
  
  w() ; lplot(wins, lwd=3, col="red", ylim=c(75,102), ylab="spo2 %", xlab="time (s)")
  lines(spo2_1, lwd=3, col="mediumseagreen")
  abline(h=c(95,100), col="gray")
  par(new=T) ; lplot(spo2_cqm_1, col="blue", lwd=2, lty=2, ann=F, yaxt="n", xaxt="n", ylim=c(0.5,1))
  axis(4) ; title(main=m_name)
  
  savePlot(paste0("spo2_results/", m_name), "png") ; graphics.off()
  #########################################################################
  
  
  cat(round(100*i/len(datlist)), "%\n")
  
}




# # w() ; par(ask=T)
# clist <- vector("list", len(data_mojo2))
# for(i in 1:len(data_mojo2)){
#   
#   cout <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out_", names(data_mojo2)[i], ".csv"), sep=",")
#   
#   c_spo2 <- cout[,4]/10
#   c_spo2 <- c_spo2[-c(1:which(diff(c_spo2) != 0)[1])]
#   c_spo2 <- c_spo2[seq(1, len(c_spo2), 50)]
#   
#   c_spo2qm <- cout[,6]
#   c_spo2qm <- c_spo2qm[-c(1:which(diff(c_spo2qm) != 0)[1])]
#   c_spo2qm <- c_spo2qm[seq(1, len(c_spo2qm), 50)]
#   
#   clist[[i]] <- cbind(c_spo2, c_spo2qm)
#   
#   
#   
#   #   lplot(reslist[[i]][,1], lwd=3, col="red", main=names(data_mojo2)[i], ylab="spo2", ylim=c(80,105))
#   #   lines(c_spo2, lwd=2, col="orange")
#   #   par(new=T) ; lplot(reslist[[i]][,2], lwd=2, col="blue", ylim=c(50,100), ann=F, yaxt="n", xaxt="n")
#   #   lines(c_spo2qm, lwd=2, col="forestgreen")
#   #   axis(4)
#   #   
#   #   
#   #   savePlot(paste0("~/HealthQ/R scripts/HealthQ_OxySat/spo2_results/", names(data_mojo2)[i]), "png")
#   #   graphics.off()
#   
#   
# }
# 
# 
# 
# 
# for(i in 1:len(data_mojo2))
#   print(paste(i, all(clist[[i]] == clist_b[[i]])))

