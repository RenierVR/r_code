

source("../useful_funs.r")
source("./sp02_funs.r")



fpath1 <- "D:/HealthQ/Data/LGInnotek/20150615_SPO2/"
fpath_n1 <- c("Robert", "Wiaan", "Kim", "DeWet", "Nina", "Michael")
fpath_n2 <- paste0(1:6, "_", fpath_n1)
fpath_n3 <- paste0(rep(fpath_n1, each=4), "_", 1:4)
fpath2 <- paste0(fpath1, paste(rep(fpath_n2, each=4), fpath_n3, "", sep="/"))



############################################################################################
############################################################################################


# offset_mat <- matrix(c(22, 1,    # 1
#                        1, 2,    # 2
#                        1, 1,    # 3
#                        10, 1,    # 4
#                        25, 1,    # 5
#                        3, 1,    # 6
#                        28, 1,    # 7
#                        1, 28,    # 8
#                        1, 21,    # 9
#                        9, 1,    # 10
#                        1, 5,    # 11
#                        1, 11,    # 12
#                        54, 1,    # 13
#                        57, 1,    # 14
#                        1, 20,    # 15
#                        22, 1,    # 16
#                        10, 1,    # 17
#                        8, 1,    # 18
#                        26, 1,    # 19
#                        21, 1,    # 20
#                        15, 1,    # 21
#                        5, 1), nrow=22, byrow=T)



plotmat <- matrix(c(0, 1,
                    0, 1,
                    0, 1,
                    0, 1,
                    1, 1,
                    0, 1,
                    1, 1,
                    1, 1,
                    1, 1,
                    1, 1,
                    1, 1,
                    1, 1,
                    1, 1,
                    1, 1,
                    1, 1,
                    1, 1,
                    1, 1,
                    1, 1,
                    1, 1,
                    1, 1,
                    1, 1,
                    1, 1,
                    1, 1,
                    1, 1), byrow=T, ncol=2)



# perfmat <- matrix(0, nr=len(fpath2), nc=2)
i <- 1
for(i in 1:len(fpath2)){
  
  datlist <- getDataFromDir(fpath2[i], head=T)
  datnames <- names(datlist)
  index_ws <- grep("inspiro", datnames)
  index_f <- grep("finger", datnames)
  index_w <- grep("wrist", datnames)
  
  wins <- datlist[[index_ws]][,3]
  dat1 <- datlist[[index_f]]
  dat2 <- datlist[[index_w]]
  rm(datlist)
  
  #####################################################################
  
  dat <- dat1
  
  dat <- dat[dat[,1] != 0,]
  dat <- dat[100:(nrow(dat)),]
  curr1 <- curr2 <- curr3 <- matrix(0, nrow=nrow(dat), nc=4)
  for(j in 1:4){
    if(j < 3){
      Rf <- dat[,8]
      led <- dat[,14]
    }
    else{
      Rf <- dat[,9]
      led <- dat[,j+12]
    }
    
    safe_term <- 0
    if(j == 2){
      safe_term <- 1
      led <- numeric(nrow(dat))
    }
    
    dat_tmp <- dat[,j] 
    isub_tmp <- dat[,9+j]
    curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
    curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)  
    curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)[[1]]
  }
  
  redI_blank_clipped1 <- removeCurrentDiscont2_m(curr1[,3]-curr1[,2], dat[,8]+dat[,9], dat[,12]+dat[,11], dat[,15], 1e8)[[1]]
  nirI_blank_clipped1 <- removeCurrentDiscont2_m(curr1[,4]-curr1[,2], dat[,8]+dat[,9], dat[,13]+dat[,11], dat[,16], 1e8)[[1]]
  
  
  #####################################################################
  
  dat <- dat2
  
  dat <- dat[dat[,1] != 0,]
  dat <- dat[100:(nrow(dat)),]  
  curr1 <- curr2 <- curr3 <- matrix(0, nrow=nrow(dat), nc=4)
  for(j in 1:4){
    if(j < 3){
      Rf <- dat[,8]
      led <- dat[,14]
    }
    else{
      Rf <- dat[,9]
      led <- dat[,j+12]
    }
    
    safe_term <- 0
    if(j == 2){
      safe_term <- 1
      led <- numeric(nrow(dat))
    }
    
    dat_tmp <- dat[,j] 
    isub_tmp <- dat[,9+j]
    curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
    curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)  
    curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)[[1]]
  }
  
  redI_blank_clipped2 <- removeCurrentDiscont2_m(curr1[,3]-curr1[,2], dat[,8]+dat[,9], dat[,12]+dat[,11], dat[,15], 1e8)[[1]]
  nirI_blank_clipped2 <- removeCurrentDiscont2_m(curr1[,4]-curr1[,2], dat[,8]+dat[,9], dat[,13]+dat[,11], dat[,16], 1e8)[[1]]
  
  
  #####################################################################
  
  
  fs=50 ; winsize <- 2^8
  #   fs=25 ; winsize <- 2^7
  
  if(fs == 25){
    redI_blank_clipped1 <- redI_blank_clipped1[seq(1, len(redI_blank_clipped1), 2)]
    nirI_blank_clipped1 <- nirI_blank_clipped1[seq(1, len(nirI_blank_clipped1), 2)]
    redI_blank_clipped2 <- redI_blank_clipped2[seq(1, len(redI_blank_clipped2), 2)]
    nirI_blank_clipped2 <- nirI_blank_clipped2[seq(1, len(nirI_blank_clipped2), 2)]
  }
  
  
  out <- fftSpO2_Cdev(redI_blank_clipped1, nirI_blank_clipped1, Fs=fs, filtType="fir50", smoothACDC=0.8,
                      windowSize=winsize, spo2_check=F, useMaxDenom=F, spo2_caleb_pf=0.7)
  spo2_1  <- 100*calabFreeSpO2(out[[1]], clip=F, smoothAlpha=0.7)
  spo2_cqm_1 <- out[[4]][,4]
  
  out <- fftSpO2_Cdev(redI_blank_clipped2, nirI_blank_clipped2, Fs=fs, filtType="fir50", smoothACDC=0.8,
                      windowSize=winsize, spo2_check=F, useMaxDenom=T, spo2_caleb_pf=0.7)
  spo2_2  <- 100*calabFreeSpO2(out[[1]], clip=F, smoothAlpha=0.7)
  spo2_cqm_2 <- out[[4]][,4]
  

  m_name <- fpath_n3[i]
  

  
  
#   w_plot <- wins[-c(1:offset_mat[i,1])]
#   s1_plot <- round(spo2_1[-c(1:offset_mat[i,2])],1)
#   s2_plot <- round(spo2_2[-c(1:offset_mat[i,2])],1)
#   s3_plot <- round(spo2_3[-c(1:offset_mat[i,2])],1)
#   
#   s1_qm <- spo2_cqm_1[-c(1:offset_mat[i,2]),]
#   s2_qm <- spo2_cqm_2[-c(1:offset_mat[i,2]),]
#   s3_qm <- spo2_cqm_3[-c(1:offset_mat[i,2]),]
  
  perf1 <- perf2 <- NA
  
  
  if(all(plotmat[i,] == 1)){
    w() ; par(mar=c(5,4,4,5)) 
    lplot(wins, lwd=3, col="red", ylim=c(75,105), ylab="spo2 %", xlab="time (s)")
    lines(spo2_1, lwd=3, col="blue")
    lines(spo2_2, lwd=3, col="forestgreen")
    abline(h=100, lty=2, col="grey") #abline(h=c(90,95,100), lty=c(1,2,1), col="grey")
    par(new=T) ; lplot(spo2_cqm_1, ann=F, yaxt="n", xaxt="n", ylim=c(0.4,1), col="lightblue", lwd=2, lty=2)
    lines(spo2_cqm_2, col="green", lwd=2, lty=2)
    axis(4) ; title(main=m_name) ; mtext(side=4, line=3, "signal quality")
    legend("bottomright", cex=0.85, bg="white", col=c("red", "blue", "lightblue", "forestgreen", "green"), lty=c(1,1,2,1,2),
           lwd=c(3,3,2,3,2), leg=c("reference", "LGI (finger)", "quality (finger)", "LGI (wrist)", "quality (wrist)"))
    
    
    min_len <- min(len(wins), len(spo2_1), len(spo2_2))
    perf1 <- perf(wins[1:min_len], spo2_1[1:min_len])[1]
    perf2 <- perf(wins[1:min_len], spo2_2[1:min_len])[1]
  }
  
  if((plotmat[i,1] == 0) & (plotmat[i,2] == 1)){
    w() ; par(mar=c(5,4,4,5)) 
    lplot(wins, lwd=3, col="red", ylim=c(65,105), ylab="spo2 %", xlab="time (s)")
    lines(spo2_2, lwd=3, col="forestgreen")
    abline(h=100, lty=2, col="grey") #abline(h=c(90,95,100), lty=c(1,2,1), col="grey")
    par(new=T) ; lplot(spo2_cqm_2, ann=F, yaxt="n", xaxt="n", ylim=c(0.5,1), col="green", lwd=2, lty=2)
    axis(4) ; title(main=m_name) ; mtext(side=4, line=3, "signal quality")
    legend("bottomright", cex=0.85, bg="white", col=c("red", "forestgreen", "green"), lty=c(1,1,2),
           lwd=c(3,3,2), leg=c("reference", "LGI (wrist)", "quality (wrist)"))
    
    min_len <- min(len(wins), len(spo2_2))
    perf2 <- perf(wins[1:min_len], spo2_2[1:min_len])[1]
  }

  
  savePlot(paste0("spo2_results/", m_name), "png") ; graphics.off()
  #########################################################################
  
  # perfmat[i,] <- c(perf1, perf2)
  
  cat(round(100*i/len(fpath2)), "%\n")
  
}




( perfsummary <- cbind(data.frame(fpath_n3), RMSE=round(perfmat[,1],4), LGI=c(3,3,7,7,7,7,3,3,3,3,7,7,3,3,7,7,7,7,3,3,7,7,3,3)) )

perfs1 <- na.omit(perfsummary)
perfs2 <- perfs1[1:15,]





w() ; par(mar=c(5,4,4,5)) 
lplot(wins, lwd=3, col="red", ylim=c(80,105), ylab="spo2 %", xlab="time (s)")
lines(spo2_1, lwd=3, col="blue")
# lines(spo2_2, lwd=3, col="forestgreen")
abline(h=100, lty=2, col="grey") #abline(h=c(90,95,100), lty=c(1,2,1), col="grey")
par(new=T) ; lplot(spo2_cqm_1, ann=F, yaxt="n", xaxt="n", ylim=c(0.4,1), col="lightblue", lwd=2, lty=2)
# lines(spo2_cqm_2, col="green", lwd=2, lty=2)
axis(4) ; mtext(side=4, line=3, "signal quality")
legend("bottomright", bg="white", col=c("red", "blue", "lightblue"), lty=c(1,1,2),
       lwd=c(3,3,2), leg=c("reference", "LifeQ (finger)", "SQ (finger)"))



