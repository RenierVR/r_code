

source("../useful_funs.r")
source("./sp02_funs.r")


fpath1 <- "D:/HealthQ/Data/OxySat/SpO2 Robustness Finger 20150416/"
wins_0416 <- rep(getDataFromDir(paste0(fpath1, "Winspiro/"), head=T), 3)
wins_0416 <- lapply(wins_0416, function(x) x[,3])
data_0416 <- c(getDataFromDir(paste0(fpath1, "Goldfinger/"), head=T)[-23],
               getDataFromDir(paste0(fpath1, "Mojo 5172 (finger)/"), head=T),
               getDataFromDir(paste0(fpath1, "Mojo 5173 (wrist)/"), head=T))


fpath2 <- "D:/HealthQ/Data/OxySat/20150623 wrist testing/"
wins_0623 <- as.list(rep(0,15))
data_0623 <- getDataFromDir(fpath2, head=T)


fpath3 <- "D:/HealthQ/Data/LGInnotek/20150624_SPO2_WRIST_UPPERARM/"
wins_0624 <- getDataFromDir(paste0(fpath3, "winspiro/"), head=T)
wins_0624 <- lapply(wins_0624, function(x) x[,3])
data_0624 <- getDataFromDir(fpath3, head=T)


winlist <- c(wins_0416, wins_0623, wins_0624)
datlist <- c(data_0416, data_0623, data_0624)

fslist <- c(rep(50, len(data_0416)), rep(25, len(data_0623)), rep(50, len(data_0624)))

rm(wins_0416, wins_0623, wins_0624, data_0416, data_0623, data_0624)


######################################################################################################


# getSpO2PointEstimate(spo2_1, spo2_cqm_1)





runningAbsDiff <- function(x, bandwidth=length(x))
{
  n <- len(x)
  
  if(bandwidth > n){
    warning("Bandwidth parameter larger than vector length.")
    return(0)
  }
  
  rd <- NULL
  for(i in 1:(len(x) - bandwidth + 1))  
    rd <- c(rd, mean(abs(diff(x[i:(i + bandwidth - 1)]))))
  
  return(rd)
}




######################################################################################################


i <- 74

point_est_mat <- matrix(0, nr=len(datlist), nc=2)
for(i in 1:len(datlist)){
  
  
  wins <- winlist[[i]]
  has_wins <- as.numeric( !all(wins == 0) )
  
  
  dat <- datlist[[i]]
  
  dat <- dat[dat[,1] != 0,]
  dat <- dat[10:nrow(dat),]
  curr1 <- curr2 <- curr3 <- matrix(0, nr=nrow(dat), nc=4)
  for(j in 1:4){
    if(j < 3){
      Rf <- dat[,8]
      led <- dat[,14]
    }
    else{
      Rf <- dat[,9]
      led <- dat[,j+12]
    }
    
    safe_term <- 0
    if(j == 2){
      safe_term <- 1
      led <- numeric(nrow(dat))
    }
    
    dat_tmp <- dat[,j] 
    isub_tmp <- dat[,9+j]
    curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
    curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)  
    curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)[[1]]
  }
  
  
  # redI_blank_clipped <- removeCurrentDiscont2_m(curr1[,3]-curr1[,2], dat[,8]+dat[,9], dat[,12]+dat[,11], dat[,15], 1e8)[[1]]
  # nirI_blank_clipped <- removeCurrentDiscont2_m(curr1[,4]-curr1[,2], dat[,8]+dat[,9], dat[,13]+dat[,11], dat[,16], 1e8)[[1]]
  
  redI_blank_clipped <- curr3[,3]
  nirI_blank_clipped <- curr3[,4]
  
  
  zPlot(curr3[,3:4], par_new=T)
  
  
  ################################################################
  
  
  if(fslist[i] == 50) winsize <- 2^8
  if(fslist[i] == 25) winsize <- 2^7
  
  
  out <- fftSpO2_Cdev(redI_blank_clipped, nirI_blank_clipped, Fs=fslist[i], filtType="fir50", smoothACDC=0.3,
                      windowSize=winsize, spo2_check=F, useMaxDenom=T, spo2_caleb_pf=0.6)
  spo2_1  <- 100*calabFreeSpO2(out[[1]], clip=F)#, smoothAlpha=0.7)
  spo2_cqm_1 <- 100*(out[[4]][,4]^2)
  
  p_name <- paste0("batch", i, "_", names(datlist)[i])
  
  w() ; plot(spo2_1, type="n", ylim=c(75,105), ylab="spo2 %", xlab="time (s)")
  if(has_wins == 1) lines(wins, lwd=2, col="red")
  lines(spo2_1, lwd=3, col="blue")
  abline(h=c(95,100), col="blue")
  par(new=T) ; lplot(spo2_cqm_1, col="green", ann=F, yaxt="n", xaxt="n", ylim=c(70,100))
  abline(h=90, col="mediumseagreen", lwd=2, lty=2) ; axis(4) ; title(main=p_name)
    
  savePlot(paste0("spo2_results/", p_name), "png") ; graphics.off()
  
  ################################################################
  
  spo2_qm_rsd <- c(rep(0,9), expSmooth(runningSD(spo2_cqm_1, 10), alpha=0.5))
  spo2_qm_rd <- c(rep(0,9), expSmooth(runningAbsDiff(spo2_cqm_1, 10), alpha=0.5))
  
  w() ; lplot(spo2_cqm_1, lwd=3, ylim=c(70,100))
  par(new=T) ; lplot(spo2_qm_rsd, lwd=2, col="orange", ann=F, yaxt="n")
  # lines(spo2_qm_rd, lwd=2, col="red")
  axis(4) ; title(main=p_name)
  
  savePlot(paste0("spo2_results/", p_name, "_rsd"), "png") ; graphics.off()
  
  ################################################################
  
  
  point_est <- getSpO2PointEstimate(spo2_1, spo2_cqm_1, 10, 90, 100)
  point_est[1] <- round(point_est[1],2)
  
  point_est_mat[i,] <- point_est
  
  if(i == 1) write.table2("", file="./20150629_spo2_point_est_results.txt")
  write.table2(data.frame(rbind(point_est), names(datlist)[i]), file="./20150629_spo2_point_est_results.txt", sep=",\t\t", append=TRUE)
  
  
  # cat(round(100*i/len(datlist)), "%\n")
  
}





######################################################################################################
######################################################################################################


out_df <- data.frame(point_est_mat, names(datlist))
names(out_df) <- c("spo2_point_est", "time", "file_name")
out_df



out_df <- data.frame(point_est_mat_b, point_est_mat, names(datlist))
names(out_df) <- c("spo2_point_est1", "time1", "spo2_point_est2", "time2", "file_name")
out_df





