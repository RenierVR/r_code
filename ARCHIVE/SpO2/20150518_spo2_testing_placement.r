source("../useful_funs.r")
source("./sp02_funs.r")


datlist <- getDataFromDir("~/HealthQ/Data/OxySat/20150518 SpO2 placement testing/", head=T)




i <- 1

# plotqm <- T
# for(i in 1:len(datlist)){
  
  dat <- datlist[[i]]
  dat[is.na(dat)] <- 0
  
  dat <- dat[-c(1:10),]
  dat <- dat[dat[,1] != 0,]
  curr1 <- curr2 <- curr3 <- matrix(0, nr=nrow(dat), nc=4)
  for(j in 1:4){
    if(j < 3){
      Rf <- dat[,8]
      led <- dat[,14]
    }
    else{
      Rf <- dat[,9]
      led <- dat[,j+12]
    }
    
    safe_term <- 0
    if(j == 2){
      safe_term <- 1
      led <- numeric(nrow(dat))
    }
    
    dat_tmp <- dat[,j] 
    isub_tmp <- dat[,9+j]

    curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
    curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e8)  
    curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e8)[[1]]
  }
  
  redI_blank_clipped <- removeCurrentDiscont2_m(curr1[,3]-curr1[,2], dat[,8]+dat[,9], dat[,12]+dat[,11], dat[,15], 1e8)[[1]]
  nirI_blank_clipped <- removeCurrentDiscont2_m(curr1[,4]-curr1[,2], dat[,8]+dat[,9], dat[,13]+dat[,11], dat[,16], 1e8)[[1]]
  
  zPlot(cbind(redI_blank_clipped, nirI_blank_clipped), col_f=c("red", "blue"), par_new=T)
  zPlot(curr3[,c(1,3:4)], par_new=T, col_f=c("lightblue", "red", "forestgreen"))


  redI_blank_clipped <- curr3[,3]
  nirI_blank_clipped <- curr3[,4]
  
  out <- fftSpO2_Cdev(redI_blank_clipped, nirI_blank_clipped, filtType="fir50", smoothACDC=0.8, skipSamples=100,
                      windowSize=2^8, spo2_check=F, useMaxDenom=F, spo2_caleb_pf=0.6, 
                      spo2_new_method=F)
  spo2_1  <- 100*calabFreeSpO2(out[[1]], clip=F, smoothAlpha=0.7)
  spo2_qm1 <- round(100*out[[4]][,3])
  
  
  
  m_spo2 <- dat[,22]/10
  m_spo2 <- m_spo2[-c(1:which(diff(m_spo2) != 0)[1])]
  m_spo2 <- m_spo2[seq(1, len(m_spo2), 50)]
  m_spo2_qm <- dat[,24]
  m_spo2_qm <- m_spo2_qm[-c(1:which(diff(m_spo2_qm) != 0)[1])]
  m_spo2_qm <- m_spo2_qm[seq(1, len(m_spo2_qm), 50)]


  c_out <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out_", names(datlist)[i], ".csv"), sep=",")  
  c_spo2 <- c_out[,4]/10
  c_spo2 <- c_spo2[-c(1:which(diff(c_spo2) != 0)[1])]
  c_spo2 <- c_spo2[seq(1, len(c_spo2), 50)]
  c_spo2_qm <- c_out[,6]
  c_spo2_qm <- c_spo2_qm[-c(1:which(diff(c_spo2_qm) != 0)[1])]
  c_spo2_qm <- c_spo2_qm[seq(1, len(c_spo2_qm), 50)]

  m_spo2_1 <- c_spo2
  m_spo2_qm <- c_spo2_qm


#########################################################################################
  

  w() ; plot(spo2_1, type="n", ylim=c(85, 105), main=names(datlist)[i])
  lines(spo2_1, col="blue", lwd=2)
  lines(m_spo2, col="forestgreen", lwd=2)
  abline(h=c(95,100), lty=2)

if(plotqm){
  par(new=T)
  plot(spo2_qm1, type="n", ann=F, xaxt="n", yaxt="n", ylim=range(c(spo2_qm1, m_spo2_qm)))
  lines(spo2_qm1, lwd=2, col="red")
  lines(m_spo2_qm, lwd=2, col="orange")
  axis(4)
}



  
  savePlot(paste0("spo2_results/", names(datlist)[i]), "png")
  graphics.off()
  cat(round(100*i/len(datlist)), "%\n")
  
}
