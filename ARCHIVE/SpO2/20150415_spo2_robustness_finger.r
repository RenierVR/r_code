

source("../useful_funs.r")
source("./sp02_funs.r")


fpath1 <- "~/HealthQ/Data/OxySat/Spo2 robustness finger tests 20140415/"


data_mojo1 <- getDataFromDir(paste0(fpath1, "Goldfinger/reparsed/"), head=T)#, dataE=F)
# hierdie is die data
data_mojo2 <- getDataFromDir(paste0(fpath1, "Mojo 5172/reparsed/"), head=T)#, dataE=F)
data_wins <- getDataFromDir(paste0(fpath1, "Winspiro/"), head=T)





############################################################################################
############################################################################################


offset_mat <- matrix(c(15, 1,
                       14, 4,
                       15, 1,
                       22, 3,
                       18, 1,
                       15, 1), nrow=6, byrow=T)



i <- 1
performance_mat <- matrix(0, nr=len(data_wins), nc=4)
for(i in 1:len(data_wins)){

wins <- data_wins[[i]][,3]


dat <- data_mojo1[[i]]

dat <- dat[dat[,1] != 0,]
dat <- dat[-c(1:500),]
curr1 <- curr2 <- curr3 <- matrix(0, nr=nrow(dat), nc=4)
for(j in 1:4){
  if(j < 3){
    Rf <- dat[,8]
    led <- dat[,14]
  }
  else{
    Rf <- dat[,9]
    led <- dat[,j+12]
  }
  
  safe_term <- 0
  if(j == 2){
    safe_term <- 1
    led <- numeric(nrow(dat))
  }
  
  dat_tmp <- dat[,j] 
  isub_tmp <- dat[,9+j]
#   isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
#   Rf <- Rf[1:(len(Rf)-1)]
#   led <- led[1:(len(led)-1)]
  curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
  curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)  
  curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)[[1]]
}



redI_blank_clipped <- removeCurrentDiscont2_m(curr1[,3]-curr1[,2], dat[,8]+dat[,9], dat[,12]+dat[,11], dat[,15], 1e8)[[1]]
nirI_blank_clipped <- removeCurrentDiscont2_m(curr1[,4]-curr1[,2], dat[,8]+dat[,9], dat[,13]+dat[,11], dat[,16], 1e8)[[1]]


#####################################################################


out <- fftSpO2_Cdev(redI_blank_clipped, nirI_blank_clipped, filtType="fir50", adapF=F, smoothACDC=0.8,
                    windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F,
                    spo2_caleb_pf=0.55)
spo2_1  <- 100*calabFreeSpO2(out[[1]], clip=F)


#####################################################################


# dat <- data_mojo2[[i]]
# 
# dat <- dat[dat[,1] != 0,]
# dat <- dat[-c(1:500),]
# curr1 <- curr2 <- curr3 <- matrix(0, nr=nrow(dat), nc=4)
# 
# # zPlot(curr3[,c(1,3,4)], par_new=T)
# 
# 
# redI_clipped2 <- curr3[,3]#-curr3[,2]
# nirI_clipped2 <- curr3[,4]#-curr3[,2]


#####################################################################



# out <- fftSpO2_Cdev(redI_clipped2, nirI_clipped2, filtType="fir50", adapF=F, smoothACDC=0.8,
#                     windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F,
#                     spo2_caleb_pf=0.6)
# spo2_2  <- 100*calabFreeSpO2(out[[1]], clip=F)


m_name <- substring(names(data_wins)[[i]], 1, nchar(names(data_wins)[[i]])-9)

wins_plot <- wins[-c(1:offset_mat[i,1])]
spo2_plot <- round(spo2_1[-c(1:offset_mat[i,2])],1)

min_len <- min(len(wins_plot), len(spo2_plot))
wins_plot <- wins_plot[1:min_len]
spo2_plot <- spo2_plot[1:min_len]

performance_mat[i,] <- c( mean((wins_plot - spo2_plot)^2), mean(abs(wins_plot - spo2_plot)),
                          100*mean((wins_plot - spo2_plot)/wins_plot),
                          100*mean(abs((wins_plot - spo2_plot)/wins_plot))  )


w()# ; par(mfrow=c(2,1), mar=c(2,4,1,1))
lplot(wins_plot, lwd=3, col="red", ylim=c(90,102), ylab="spo2 %", main=m_name, xlab="time (s)")
lines(spo2_plot, lwd=2, col="blue")
# lines(round(spo2_2[-c(1:offset_mat[i,2])],1)-1, lwd=2, col="forestgreen")
legend("bottomright", bg="white", lwd=2, col=c("red", "blue"),
       leg=c("reference pulse oximeter", "LifeQ"), cex=0.8)

# ccf_out <- ccf(wins_plot, spo2_plot)
# max_lag <- ccf_out[[4]][which(ccf_out[[1]] == max(ccf_out[[1]])[1])]
# legend("topright", leg=paste("max lag at", max_lag), cex=0.75, bg="white")


#####################################################################

savePlot(paste0("spo2_results/", m_name), "png")
graphics.off()
cat(round(100*i/len(data_wins)), "%\n")
}




