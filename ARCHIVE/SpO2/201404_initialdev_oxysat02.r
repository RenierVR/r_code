
wmcompplot <- function(winsize, overlap, start=1, y1, y2, cols=c("blue", "mediumseagreen"))
{
  windows(14,7)
  par(ask = TRUE, mfrow=c(2,1), mar = c(4.5, 4, 1.5, 1)+0.1, bg="grey80", fg="white")
  j = 0
  while(TRUE){
    j = j+1
    plotind <- (overlap*(j-1)+start):(start-1+winsize + overlap*(j-1))
    if(max(plotind) > length(y1))
      break
    else{
      plot(plotind, y1[plotind], type="l", lwd=2, col=cols[1])
      plot(plotind, y2[plotind], type="l", lwd=2, col=cols[2])
    }
  }
}




# mojo <- read.table("~/HealthQ/Data/OxySat/20140326_ModC10_Testing_OxSatSingleCh_50mA_001.txt", sep=",")
# wspiro <- read.table("~/HealthQ/Data/OxySat/20140326_winspiro_testdata.txt", sep=",")
# m_nir <- mojo[,3]
# m_red <- mojo[,4]


processor <- function(x, dif=1, bw1=10, bw2=0)
{
  x_sm <- ksmooth(1:length(x), x, "normal", bandw=bw1)$y
  
  if(bw2 != 0)
    x_sm <- ksmooth(1:length(x_sm), x_sm, "normal", bandw=bw2)$y
  
  if(dif != 0)
    x_df <- as.numeric(diff(x_sm, diff=dif))
    
  return(x_df)
}


# xx <- m_red
# yy1 <- processor(xx, 1, 17, 0)
# yy2 <- processor(xx, 2, 8, 0)
# wmcompplot(500,250,1,yy1,yy2)


#plot_init_oxysat(dataset="modc_20140326.txt", plot_tr=T)

#########################################################################
#########################################################################

plot_init_oxysat <- function(winsize=200, overlap=50, dataset="modc_20140402_nir+red.csv", 
                           plot_tr = FALSE, plot_df = TRUE, project="OxySat_project")
{
  #if(!missing(dataset)){
    inir_out <- read.table(paste0("~/HealthQ/C FILES/", project, "/output/init_nir_out_", dataset, ".txt"), head=T, sep=",")
    ired_out <- read.table(paste0("~/HealthQ/C FILES/", project, "/output/init_red_out_", dataset, ".txt"), head=T, sep=",")
    pnir_out <- read.table(paste0("~/HealthQ/C Files/", project, "/output/pksid_nir_out_", dataset, ".txt"), sep=",")
    pred_out <- read.table(paste0("~/HealthQ/C Files/", project, "/output/pksid_red_out_", dataset, ".txt"), sep=",")
    
    pnir_out[,1] <- pnir_out[,1] + 1
    pred_out[,1] <- pred_out[,1] + 1
    pkinds_nir <- inir_out[inir_out[,7] != -1, 7]+1
    pkinds_red <- ired_out[ired_out[,7] != -1, 7]+1
    
    if(plot_tr){
      tnir_out <- read.table(paste0("~/HealthQ/C Files/", project, "/output/trsnir_out_", dataset, ".txt"), sep=",")
      tred_out <- read.table(paste0("~/HealthQ/C Files/", project, "/output/trsred_out_", dataset, ".txt"), sep=",")
      trinds_nir <- tnir_out[,1]+1
      trinds_red <- tred_out[,1]+1
      
      tid_nir_out <- read.table(paste0("~/HealthQ/C FILES/", project, "/output/troughs_nir_out_", dataset, ".txt"), sep=",")
      tid_nir_out[,1] <- tid_nir_out[,1] + 1
      tid_red_out <- read.table(paste0("~/HealthQ/C FILES/", project, "/output/troughs_red_out_", dataset, ".txt"), sep=",")
      tid_red_out[,1] <- tid_red_out[,1] + 1
    }
  #}
  
  
  start = 1
  windows(14,8)
  par(ask = TRUE, mar = c(3, 4, 1.5, 1)+0.1, bg="grey80", fg="white", mfrow=c(2,1))

  
  j = 0
  while(TRUE){
    j = j+1
    plotind <- (overlap*(j-1)+start):(start-1+winsize + overlap*(j-1))
    #print(max(plotind))
    if(max(plotind) > max(inir_out[,1]))
      break
    else{
      
      plot(plotind, inir_out[plotind,2], type="l", lwd=2, col="mediumseagreen",
           xlab="", ylab="", yaxt="n", xaxt="n")
      par(new=T) ; plot(plotind, inir_out[plotind,3], type="l", 
           lwd=2, col="blue", xlab="samples", ylab="near-infrared LED")
      points(plotind, inir_out[plotind,3], pch=19, cex=0.7, col="darkblue")
      
      ab_pk <- pkinds_nir[min(plotind) <= pkinds_nir & max(plotind) >= pkinds_nir]
      if(length(ab_pk) > 0)
        abline(v=ab_pk, col="red", lty=2, lwd=2) 
      abline(v=pnir_out[,1], col="red", lwd=4)
      points(pnir_out, pch=19, col="red", cex=1.3)
      
      if(plot_tr){
        ab_tr <- trinds_nir[min(plotind) <= trinds_nir & max(plotind) >= trinds_nir]
        if(length(ab_tr) > 0)
          abline(v=ab_tr, col="yellow", lty=2, lwd=2) 
        
        abline(v=tid_nir_out[,1], col="yellow", lwd=4)
        points(tid_nir_out, pch=19, col="yellow", cex=1.3)
      }
      
      
      plot(plotind, ired_out[plotind,2], type="l", lwd=2, col="mediumseagreen",
           xlab="", ylab="", yaxt="n", xaxt="n")
      par(new=T) ; plot(plotind, ired_out[plotind,3], type="l", 
                        lwd=2, col="blue", xlab="samples", ylab="red LED")
      points(plotind, ired_out[plotind,3], pch=19, cex=0.7, col="darkblue")
      
      ab_pk <- pkinds_red[min(plotind) <= pkinds_red & max(plotind) >= pkinds_red]
      if(length(ab_pk) > 0)
        abline(v=ab_pk, col="red", lty=2, lwd=2) 
      abline(v=pred_out[,1], col="red", lwd=4)
      points(pred_out, pch=19, col="red", cex=1.3)
      
      
      if(plot_tr){
        ab_tr <- trinds_red[min(plotind) <= trinds_red & max(plotind) >= trinds_red]
        if(length(ab_tr) > 0)
          abline(v=ab_tr, col="yellow", lty=2, lwd=2) 
        
        abline(v=tid_red_out[,1], col="yellow", lwd=4)
        points(tid_red_out, pch=19, col="yellow", cex=1.3)
      }
      
    }
    
  }
}





#############################################################################
#############################################################################

amp_tracker <- function(pp=pnir, tt=tnir, ind=nir_ind, ilim=nrow(pp))
{
  i <- ind
  j <- 1
  k <- 0
  px <- pp[,1]
  tx <- tt[,1]
  amps <- NULL
  while(TRUE){
    print(paste("PEAKS  =", px[i], "  , ", px[i+1], "   (peak diff =", px[i+1]-px[i], ")"))
    print(paste("TROUGH =", tx[j]))
    
    if((px[i+1] - px[i]) < 100){
      if((tx[j] > px[i]) && (tx[j] < px[i+1])){
        amps <- rbind(amps, c(tx[j], pp[i,2]-tt[j,2]))
        k <- k+1
        i <- i+1
        j <- j+1
        
        print(paste("amplitude =", amps[k,2]))
      }
      else if(tx[j] > px[i+1]){
        i <- i+1 
      }
      else if(tx[j] < px[i]){
        j <- j+1
      }
    }
    
    else{
      while(TRUE){
        if(tx[j] > px[i+1]){
          i <- i+1
          break
        }
        else
          j <- j+1
        
      }
    }
    
    print("--------------------------------------------")
    
    if(i >= ilim)
      break
  }
  
  return(amps)
}



