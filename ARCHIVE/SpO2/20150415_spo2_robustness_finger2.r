source("../useful_funs.r")
source("./sp02_funs.r")


fpath1 <- "~/HealthQ/Data/OxySat/Spo2 robustness finger tests 20140415/"


data_mojo <- c(getDataFromDir(paste0(fpath1, "Goldfinger/reparsed/"), head=T),
               getDataFromDir(paste0(fpath1, "Mojo 5172/reparsed/"), head=T),
               list("20150421_1649_Renier_ondevice_droptest_mojo5175"=read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/data/SpO2FingerTests/20150421_1649_Renier_ondevice_droptest_mojo5175.csv", sep=",", head=T)))
data_mojo <- data_mojo[order(names(data_mojo))]


############################################################################################
############################################################################################

i <- 13

spo2_list <- vector("list", len(data_mojo))
for(i in 1:len(data_mojo)){

dat <- data_mojo[[i]]

dat <- dat[-c(1:5),]
dat <- dat[dat[,1] != 0,]
curr1 <- curr2 <- curr3 <- matrix(0, nr=nrow(dat), nc=4)
for(j in 1:4){
  if(j < 3){
    Rf <- dat[,8]
    led <- dat[,14]
  }
  else{
    Rf <- dat[,9]
    led <- dat[,j+12]
  }
  
  safe_term <- 0
  if(j == 2){
    safe_term <- 1
    led <- numeric(nrow(dat))
  }
  
  dat_tmp <- dat[,j] 
  isub_tmp <- dat[,9+j]
  #   isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
  #   Rf <- Rf[1:(len(Rf)-1)]
  #   led <- led[1:(len(led)-1)]
  curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
  curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e7)  
  curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e7)[[1]]
}


out <- fftSpO2_Cdev(curr3[,3], curr3[,4], filtType="fir50", adapF=F, smoothACDC=0.8, subtract=T, skipSamples=100,
                    windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F,
                    spo2_caleb_pf=0.55)
spo2_1  <- round(100*calabFreeSpO2(out[[1]], clip=F),1)

spo2_list[[i]] <- spo2_1

}


############################################################################################


ctest <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/out_testing.txt", sep=",")
ctest <- as.matrix(ctest)


w() ; lplot(ctest[,5], lwd=2, col="red") ; lines(ctest[,6], lwd=2, col="blue", lty=2)
zPlot(ctest[,1:4], col_f=c("red", "red", "blue", "blue"))



cfilt <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/out_filtered.txt", sep=",")
zPlot(cfilt)


zPlot(cbind(FIRfilt(curr3[,4]), cfilt[,2]), col_f=c("red", "blue"))


############################################################################################


# rfft <- out[[5]][[2]]
# 
# ctest <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/out_testing.txt", sep=",")
# ctest <- as.matrix(ctest)
# ctest <- ctest[,1:64]
# 
# w() ; par(ask=T)
# diffs <- NULL
# for(j in 1:nrow(ctest)){
#   lplot(ctest[j,-1]*2.56, lwd=3, col="red")
#   lines(rfft[j,3:65], lwd=3, col="blue", lty=2)
#   legend("top", leg=paste("window", j))
#   
#   diffs <- c(diffs, mean(ctest[j,2:20]*2.56 - rfft[j,3:21]))
#   
# }

##################################################################

# ctest <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/out_testing.txt", sep=",")
# ctest <- as.matrix(ctest)
# 
# 
# racdc <- out[[3]][,1:4]
# 
# 
# j <- 4
# zPlot(cbind(ctest[,j],racdc[,j]))


##################################################################


# ctest <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/out_testing.txt", sep=",")
# ctest <- as.matrix(ctest)
# 
# rwm <- out[[6]]
# 
# rwm <- t(apply(rwm, 1, function(x) c(sum(x[1:256]), sum(x[257:512]))))
# 
# j <- 1
# zPlot(cbind(rwm[,j], ctest[,j]))


############################################################################################


# w() ; par(ask=T)
c_spo2_list <- vector("list", len(data_mojo))
for(i in 1:len(data_mojo)){

cout <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out_", names(data_mojo)[i], ".csv"), sep=",")

c_spo2 <- cout[,4]/10
c_spo2 <- c_spo2[-c(1:which(diff(c_spo2) != 0)[1])]
c_spo2 <- c_spo2[seq(1, len(c_spo2), 50)]

# m_spo2 <- dat[,19]/10
# m_spo2 <- m_spo2[-c(1:which(diff(m_spo2) != 0)[1])]
# m_spo2 <- m_spo2[seq(1, len(m_spo2), 50)]
# 
# w()
# lplot(spo2_list[[i]], lwd=3, col="red", main=names(data_mojo)[i], ylab="spo2", ylim=c(75,105))
# lines(c_spo2, lwd=2, col="blue")
# lines(m_spo2, lwd=2, col="mediumseagreen")


# savePlot(paste0("~/HealthQ/R scripts/HealthQ_OxySat/spo2_results/", names(data_mojo)[i]), "png")
# graphics.off()

c_spo2_list[[i]] <- c_spo2

}




for(j in 1:len(c_spo2_list)) print(all(c_spo2_list[[j]] == c_spo2_list_b[[j]]))



############################################################################################

w() ; par(ask=T)
for(j in 1:len(spo2_list)){

  
  cout <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out_", names(data_mojo)[j], ".csv"), sep=",")
  
  c_spo2 <- cout[,4]/10
  c_spo2 <- c_spo2[-c(1:which(diff(c_spo2) != 0)[1])]
  c_spo2 <- c_spo2[seq(1, len(c_spo2), 50)]
  
  c_spo2_p <- cout[,5]/10
  c_spo2_p <- c_spo2_p[-c(1:which(diff(c_spo2_p) != 0)[1])]
  c_spo2_p <- c_spo2_p[seq(1, len(c_spo2_p), 50)]
  
  m_spo2 <- dat[,20]/10
  m_spo2 <- m_spo2[-c(1:which(diff(m_spo2) != 0)[1])]
  m_spo2 <- m_spo2[seq(1, len(m_spo2), 50)]
  
  
  spo2 <- spo2_list[[j]]
  
  mean1 <- spo2[1]
  for(k in 2:len(spo2))
    mean1 <- c(mean1, mean1[k-1]*((k-1)/k) + spo2[k]*(1/k))
  
  spo2 <- mean1
  spo2[1:10] <- 0
  
  lplot(spo2_list[[j]], lwd=3, col="red")
  lines(spo2, lwd=2, col="maroon", lty=2)
  lines(c_spo2, lwd=3, col="blue")
  lines(c_spo2_p, lwd=2, col="darkblue", lty=2)
  lines(m_spo2, lwd=2, col="mediumseagreen", lty=2)
  
}




