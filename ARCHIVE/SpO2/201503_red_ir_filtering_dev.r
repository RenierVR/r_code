source("../useful_funs.r")
source("./sp02_funs.r")


LMSfilt <- function(y, x, m=5, alpha=0.01)
{
  n <- len(y)
  y_filt <- numeric(n)
  
  weights <- numeric(m)
  x_buffer <- numeric(m)
  err <- 0
  
  for(i in 1:n){
    
    ## update buffer
    x_buffer <- c(x[i], x_buffer[1:(m-1)])
    
    ## run LMS filter
    y_filt[i] <- sum(weights * x_buffer)
    err <- y[i] - y_filt[i]
    
    ## update filter
    x_sum <- sum(x_buffer^2)+1
    weights <- weights + err*alpha*x_buffer/x_sum
  }
  
  return(y_filt)
}

######################################################################


data_list <- getDataFromDir("~/HealthQ/DUMPED/20150225/Mojo Breakout/", head=T)
data_list <- data_list_good[1:6]

i <- 1

spo2_list <- vector("list", len(data_list))
for(i in 1:len(data_list)){


dat <- data_list[[i]]
dat <- dat[dat[,1] != 0,]
dat <- dat[-c(1:1000),]

# acc <- acc_res(dat[,5:7])


curr1 <- curr2 <- matrix(0, nr=nrow(dat)-1, nc=4)
rfmat <- isubmat <- matrix(0, nr=nrow(dat)-1, nc=4)
for(j in 1:4){
  if(j < 3){
    Rf <- dat[,9]
    led <- dat[,11]
  }
  else{
    Rf <- dat[,10]
    led <- dat[,j+13]
  }
  
  safe_term <- 0
  if(j == 2){
    safe_term <- 1
    led <- numeric(nrow(dat))
  }
  
  dat_tmp <- dat[-1,j] 
  isub_tmp <- 10*dat[,10+j]
  isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
  Rf <- Rf[1:(len(Rf)-1)]
  led <- led[1:(len(led)-1)]
  curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
#   curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led)  
  curr2[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led)[[1]]  
  rfmat[,j] <- Rf
  isubmat[,j] <- isub_tmp
}


redI_clipped <- (curr2[,3]-curr2[,2])
nirI_clipped <- (curr2[,4]-curr2[,2])
# 
# 
# red_offset <- redI_clipped[10]
# nir_offset <- nirI_clipped[10]
# 
# grnI <- diff(curr2[,1]-curr2[,2])[-c(1:10)]
# redI <- diff(curr2[,3]-curr2[,2])[-c(1:10)]
# nirI <- diff(curr2[,4]-curr2[,2])[-c(1:10)]
# 
# # w() ; tiaan_specgram_opt(nir_lms, windowSize=2^10, overlap=2^7, maxfreq=2)
# 
# 
# red_lms <- LMSfilt(redI, grnI, 30, 0.01)
# nir_lms <- LMSfilt(nirI, grnI, 30, 0.01)
# 
# zPlot(cbind(cumsum(red_lms), cumsum(redI)), par_new=T)
# zPlot(cbind(cumsum(nir_lms), cumsum(nirI)), par_new=T)
# 
# redI_clipped <- cumsum(red_lms) + red_offset
# nirI_clipped <- cumsum(nir_lms) + nir_offset

out <- fftSpO2_Cdev(redI_clipped, nirI_clipped, curr2[,1]-curr2[,2], filtType="fir50", adapFilt=T,
                    windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F,
                    spo2_caleb_pf=0.7, spo2_caleb_e1x=0.08, spo2_caleb_e2x=0.29, 
                    spo2_caleb_e1y=0.81, spo2_caleb_e2y=0.18, spo2_caleb_bias=0)

spo2_1  <- 100*calabFreeSpO2(out[[1]], clip=F)
spo2_list[[i]] <- list(spo2_1, out[[3]])


######################################################################

redI_clipped <- redI_clipped[-c(1:11)]
nirI_clipped <- nirI_clipped[-c(1:11)]

out <- fftSpO2_Cdev(redI_clipped, nirI_clipped, filtType="fir50",
                    windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=T,
                    spo2_caleb_pf=0.7, spo2_caleb_e1x=0.08, spo2_caleb_e2x=0.29, 
                    spo2_caleb_e1y=0.81, spo2_caleb_e2y=0.18, spo2_caleb_bias=0)

spo2_2  <- 100*calabFreeSpO2(out[[1]], clip=F)


w() ; par(mar=c(4,4,2,3))
lplot(movingAve(spo2_2,1), lwd=2, col="blue", ylim=c(60,110), ylab="spO2", main=names(data_list)[i])
lines(movingAve(spo2_1,1), lwd=2, col="red")
abline(h=median(spo2_1), col="purple")
legend("bottomright", lwd=c(2,2,1), col=c("blue", "red", "purple"), 
       leg=c("orginal", "adaptive filtered", paste("filtered median =", round(median(spo2_1),2))))

# savePlot(paste0("spo2_results/", names(data_list)[i], "spo2_af1"), "png")
# graphics.off()
}



######################################################################
##############    CALIBRATION    #####################################
######################################################################

i <- 1

for(i in 1:len(spo2_list)){
  
  x <- spo2_list[[i]][[1]]
  outmat <- spo2_list[[i]][[2]]
  
  x_red_ac <- outmat[,1]
  x_red_dc <- outmat[,2]
  x_nir_ac <- outmat[,3]
  x_nir_dc <- outmat[,4]
  
  y <- 97 + 2*runif(len(x))

  lm_out <- lm(y ~ x + x_red_ac + x_nir_ac + I(x_red_ac/x_red_dc) + I(x_nir_ac/x_nir_dc))
  print(summary(lm_out))
  
  cat("\n\n\n")
}




######################################################################
##############    ADAPTIVE FILTER TESTING    #########################
######################################################################

# dat <- dat[20000:130000,]
# 
# grn_ch <- dat[,1]-dat[,2]
# acc_ch <- acc_res(dat[,3:5])
# grn_ch <- diff(grn_ch)
# acc_ch <- diff(acc_ch)
# 
# grn_filt <- LMSfilt(grn_ch, acc_ch, 15, .1)-grn_ch
# 
# w() ; tiaan_specgram_opt(grn_ch, windowSize=2^8, overlap=2^7, maxfreq=3.5)
# w() ; tiaan_specgram_opt(grn_filt, windowSize=2^8, overlap=2^7, maxfreq=3.5)





