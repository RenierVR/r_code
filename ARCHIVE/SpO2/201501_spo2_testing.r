source("../useful_funs.r")
source("../HealthQ_OxySat/sp02_funs.r")



######################################################################################
########    DATA FROM 2015-01-20
######################################################################################

data_fpaths <- c(getDataFromDir("~/HealthQ/Data/Oxysat/20150120/Mojo 5092/", dataExtract=F, ret="full"),
                 getDataFromDir("~/HealthQ/Data/Oxysat/20150120/Mojo 5097/", dataExtract=F, ret="full"),
                 getDataFromDir("~/HealthQ/Data/Oxysat/20150120/Mojo 5094/", dataExtract=F, ret="full"),
                 getDataFromDir("~/HealthQ/Data/Oxysat/20150120/Mojo 5100/", dataExtract=F, ret="full"),
                 getDataFromDir("~/HealthQ/Data/Oxysat/20150121/Mojo 5100/", dataExtract=F, ret="full"),
                 getDataFromDir("~/HealthQ/Data/Oxysat/20150121/Mojo 5093/", dataExtract=F, ret="full"),
                 getDataFromDir("~/HealthQ/Data/Oxysat/20150121/Mojo 5086/", dataExtract=F, ret="full"),
                 getDataFromDir("~/HealthQ/Data/Oxysat/20150123/Mojo 5087/", dataExtract=F, ret="full"),
                 getDataFromDir("~/HealthQ/Data/Oxysat/20150123/Mojo 5093/", dataExtract=F, ret="full"))
# data_list <- getDataFromDir("~/HealthQ/Data/Oxysat/20150123/Mojo 4091/", head=T)



######################################################################################
########    DATA FROM 2015-02-09
######################################################################################

data_list <- getDataFromDir("~/HealthQ/Data/OxySat/20150209/", head=T)


######################################################################################
######################################################################################

data_list <- data_fpaths
dat <- read.table(data_list[i], sep=",", head=T)


i <- 1

pars <- c(0.01552273,  0.8314437096)

dat <- dat[dat[,1] != 0,]
curr1 <- curr2 <- matrix(0, nr=nrow(dat)-1, nc=4)
rfmat <- isubmat <- matrix(0, nr=nrow(dat)-1, nc=4)
for(j in 1:4){
  if(j < 3)
    Rf <- dat[,9]
  else
    Rf <- dat[,10]
  
  dat_tmp <- dat[-1,j]
  
  isub_tmp <- 10*dat[,10+j]
  isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
  
  Rf <- Rf[1:(len(Rf)-1)]
  
  #   converted1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp, c1, c2)
  curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)#, pars[1], pars[2])
  curr2[,j] <- removeCurrentDiscont2(convertToCurrent3(dat_tmp, Rf, isub_tmp), Rf, isub_tmp)[[1]]
  
  rfmat[,j] <- Rf
  isubmat[,j] <- isub_tmp
}



#######################################################################################

# grnI_clipped <- multSpikeFilter(curr1[,1]-curr1[,2], 1e5)
# grnI_clipped <- round(grnI_clipped[[1]], 0)

redI_clipped <- curr1[,3]-curr1[,2]
nirI_clipped <- curr1[,4]-curr1[,2]

redI_clipped <- multSpikeFilter(curr1[,3]-curr1[,2], 5e4)
nirI_clipped <- multSpikeFilter(curr1[,4]-curr1[,2], 5e4)
redI_clipped <- round(redI_clipped[[1]], 0)
nirI_clipped <- round(nirI_clipped[[1]], 0)


redI_clipped <- curr2[,3]-curr2[,2]
nirI_clipped <- curr2[,4]-curr2[,2]




while(0){

zPlot(curr1[,3:4]-curr1[,2], par_new=T)  
  
  
zPlot(cbind(redI_clipped, nirI_clipped), 
      ynames=c("RED current", "IR current"), par_new=T, col_f=c("blue", "forestgreen"), lty_z=c(1,1))


zPlot(cbind(redI_clipped, nirI_clipped, grnI_clipped), par_new=T)


}


out <- fftSpO2_Cdev(redI_clipped, nirI_clipped, subtractMeans=F, filtType="fir50", 
                 windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F,
                 spo2_caleb_pf=0.7, spo2_caleb_e1x=0.08, spo2_caleb_e2x=0.29, 
                 spo2_caleb_e1y=0.81, spo2_caleb_e2y=0.18, spo2_caleb_bias=0)

spo2  <- 100*calabFreeSpO2(out[[1]], clip=F)#, smoothAlpha=out[[3]][,8])

w() ; lplot(movingAve(spo2,1), lwd=2, col="forestgreen", ylim=c(60,110))
# lines(movingAve(spo2,1), lwd=2, col="red")
# lines(round(movingAve(rep(98,n) + rnorm(n, 0, 0.5), 3),0), lwd=2, col="red")




#######################################################################################
#######################################################################################
#######################################################################################



for(i in 1:len(data_fpaths)){
  
  dat <- read.table(data_fpaths[i], sep=",", head=T)
  dat <- dat[dat[,1] != 0,]
  dat_name <- basename(data_fpaths[i]) 
  dat_name <- substring(dat_name, 1, nchar(dat_name)-4)
  
  
  curr1 <- curr2 <- matrix(0, nr=nrow(dat)-1, nc=4)
  rfmat <- isubmat <- matrix(0, nr=nrow(dat)-1, nc=4)
  for(j in 1:4){
    if(j < 3)
      Rf <- dat[,9]
    else
      Rf <- dat[,10]
    
    dat_tmp <- dat[-1,j]
    isub_tmp <- 10*dat[,10+j]
    isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
    Rf <- Rf[1:(len(Rf)-1)]
    
    curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp, pars[1], pars[2])
    curr2[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
    
    rfmat[,j] <- Rf
    isubmat[,j] <- isub_tmp
  }
  
  
  redI_clipped <- round(multSpikeFilter(curr1[,3]-curr1[,2], 5e4)[[1]], 0)
  nirI_clipped <- round(multSpikeFilter(curr1[,4]-curr1[,2], 5e4)[[1]], 0)
  
  out <- fftSpO2_Cdev(redI_clipped, nirI_clipped, subtractMeans=F, filtType="fir50", 
                      windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F,
                      spo2_caleb_pf=0.7, spo2_caleb_e1x=0.08, spo2_caleb_e2x=0.29, 
                      spo2_caleb_e1y=0.81, spo2_caleb_e2y=0.18, spo2_caleb_bias=0)

  spo2_1  <- 100*calabFreeSpO2(out[[1]], clip=F)#, smoothAlpha=out[[3]][,8])
  
  redI_clipped <- removeCurrentDiscont2(curr2[,3]-curr2[,2], rfmat[,3], isubmat[,3])[[1]]
  nirI_clipped <- removeCurrentDiscont2(curr2[,4]-curr2[,2], rfmat[,4], isubmat[,4])[[1]]
  
  out <- fftSpO2_Cdev(redI_clipped, nirI_clipped, subtractMeans=F, filtType="fir50", 
                      windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=F,
                      spo2_caleb_pf=0.7, spo2_caleb_e1x=0.08, spo2_caleb_e2x=0.29, 
                      spo2_caleb_e1y=0.81, spo2_caleb_e2y=0.18, spo2_caleb_bias=0)
  
  spo2_2  <- 100*calabFreeSpO2(out[[1]], clip=F)#, smoothAlpha=out[[3]][,8])
  

  w() ; lplot(movingAve(spo2_1,11), lwd=2, col="blue", ylim=c(80,110), main=dat_name)
  lines(movingAve(spo2_2,11), lwd=2, col="mediumseagreen")
  abline(h=c(95,100), lty=2)

  savePlot(paste0("spo2_results/", dat_name), "png")
  graphics.off()


  cat(round(100*i/len(data_fpaths),2), "% complete\n")

}






