
source("../useful_funs.r")
source("./sp02_funs.r")

dat1 <- read.table("D:/HealthQ/Data/24h/1_Shannagh/MOJO1/20150624_1121_Device5147_Shannagh_24Hour_EXERCISE.csv", head=T, sep=",")
dat1 <- dat1[,c(3:9, 11:19, 21, 27)]

dat2 <- read.table("D:/HealthQ/Data/24h/1_Shannagh/MOJO2/20150624_1122_Device5091_Shannagh_24Hour_EXERCISE.csv", head=T, sep=",")
dat2 <- dat2[,c(3:9, 11:19, 21, 27)]

# bh <- read.table("D:/HealthQ/Data/24h/1_Shannagh/BioHarness/2015_06_24-11_13_12/2015_06_24-11_13_12_Summary.csv", head=T, sep=",")[,2]
# 
# 
# dat1_hr <- dat1[,17]
# dat1_hr <- dat1_hr[(which(dat1_hr != 0)[1]):len(dat1_hr)]
# dat1_hr <- dat1_hr[seq(1, len(dat1_hr), 50)]
# 
# dat2_hr <- dat2[,17]
# dat2_hr <- dat2_hr[(which(dat2_hr != 0)[1]):len(dat2_hr)]
# dat2_hr <- dat2_hr[seq(1, len(dat2_hr), 50)]
# 
# 
# zPlot(cbind(bh, dat1_hr, dat2_hr))


############################################################################################################################


dat <- dat1

dat <- dat[dat[,1] != 0,]
dat <- dat[10:nrow(dat),]
curr1 <- curr3 <- matrix(0, nr=nrow(dat), nc=4)
for(j in 1:4){
  if(j < 3){
    Rf <- dat[,8]
    led <- dat[,14]
  }
  else{
    Rf <- dat[,9]
    led <- dat[,j+12]
  }
  
  safe_term <- 0
  if(j == 2){
    safe_term <- 1
    led <- numeric(nrow(dat))
  }
  
  dat_tmp <- dat[,j] 
  isub_tmp <- dat[,9+j]
  curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
  curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)[[1]]
}


# redI_blank_clipped <- removeCurrentDiscont2_m(curr1[,3]-curr1[,2], dat[,8]+dat[,9], dat[,12]+dat[,11], dat[,15], 1e8)[[1]]
# nirI_blank_clipped <- removeCurrentDiscont2_m(curr1[,4]-curr1[,2], dat[,8]+dat[,9], dat[,13]+dat[,11], dat[,16], 1e8)[[1]]

redI_blank_clipped <- curr3[,3]
nirI_blank_clipped <- curr3[,4]


out <- fftSpO2_Cdev(redI_blank_clipped, nirI_blank_clipped, Fs=50, filtType="fir50", smoothACDC=0.3,
                    windowSize=2^8, spo2_check=F, useMaxDenom=T, spo2_caleb_pf=0.6)
spo2_1  <- 100*calabFreeSpO2(out[[1]], clip=F)#, smoothAlpha=0.7)
spo2_cqm_1 <- 100*(out[[4]][,4]^2)


zPlot(cbind(spo2_1, spo2_cqm_1), par_new=T)




############################################################################################################################

spo2_mat <- cbind(1:len(spo2_1), spo2_1, spo2_cqm_1)
spo2_mat <- spo2_mat[seq(1, nrow(spo2_mat), 120),]


w() ; par(mar=c(4,4,2,4))
lplot(spo2_mat[,1], spo2_mat[,2], ylim=c(50,100), ann=F, yaxt="n", xaxt="n", lwd=2, col="orange")
axis(4) ; mtext("signal quality", 4, line=3)
par(new=T)
lplot(spo2_mat[,1]/3600, spo2_mat[,3], ylim=c(60,110), lwd=3, col="blue", main="24h Free Living Data (20150624)", 
      ylab="SpO2 %", xlab="time (hours)")
legend("bottomright", lwd=3, col=c("blue", "orange"), leg=c("SpO2 prediction", "quality metric"), bg="white")



######################################################################################################################


spo2_mat <- cbind(1:len(spo2_1), spo2_1, spo2_cqm_1)
spo2_mat <- spo2_mat[42900:43400,]


w() ; par(mar=c(4,4,2,4))
lplot(spo2_mat[,1], spo2_mat[,2], ylim=c(65,100), ann=F, yaxt="n", xaxt="n", lwd=3, col="orange")
axis(4) ; mtext("signal quality", 4, line=3)
par(new=T)
lplot(spo2_mat[,1]/60, spo2_mat[,3], ylim=c(65,102), lwd=3, col="blue", main="24h Free Living Data (20150624) - Segment", 
      ylab="SpO2 %", xlab="time (minutes)")
legend("bottomright", lwd=3, col=c("blue", "orange"), leg=c("SpO2 prediction", "quality metric"), bg="white")
