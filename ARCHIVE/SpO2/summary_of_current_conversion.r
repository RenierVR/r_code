############################################################################
## Finite Impulse Response filter as used in SpO2 C-algo



FIRfilt <- function(xx, Fs=50)
{
  if(Fs == 50)
    coeffs <- as.double(c(-0.192622497510184398361587909676018171012,
                          0.003925239686319114892909531988607341191,
                          -0.025783493863272863994007266796870680992,
                          -0.037947196105378770691896050948344054632,
                          -0.0210833702971454615970881008024662151  ,
                          0.006127114472135553220610848512706070323,
                          0.014753882753110807504737245210435503395,
                          -0.003948243987048265755956943934279479436,
                          -0.030889509258521696127841593693119648378,
                          -0.03806517328166025837532515652128495276 ,
                          -0.017233272035402459609887415581397362985,
                          0.010251957765906287087243065059283253504,
                          0.015731195737442440346631755687667464372,
                          -0.008679184678187073717636756953197618714,
                          -0.039781715900778592820419277131804847158,
                          -0.045251942075401584142380073672029539011,
                          -0.015423981972921938995990664977853157325,
                          0.02306834271980165787518934905619971687 ,
                          0.029816398551918529141735447751671017613,
                          -0.011134658300555497559281015185206342721,
                          -0.069037429430631500060222549564059590921,
                          -0.084258006368651661155411147774429991841,
                          -0.014770006696409019447679966674513707403,
                          0.124480298945440034930065564822143642232,
                          0.263419970991996932330181380166322924197,
                          0.321475944157013193702709941135253757238,
                          0.263419970991996932330181380166322924197,
                          0.124480298945440034930065564822143642232,
                          -0.014770006696409019447679966674513707403,
                          -0.084258006368651661155411147774429991841,
                          -0.069037429430631500060222549564059590921,
                          -0.011134658300555497559281015185206342721,
                          0.029816398551918529141735447751671017613,
                          0.02306834271980165787518934905619971687 ,
                          -0.015423981972921938995990664977853157325,
                          -0.045251942075401584142380073672029539011,
                          -0.039781715900778592820419277131804847158,
                          -0.008679184678187073717636756953197618714,
                          0.015731195737442440346631755687667464372,
                          0.010251957765906287087243065059283253504,
                          -0.017233272035402459609887415581397362985,
                          -0.03806517328166025837532515652128495276 ,
                          -0.030889509258521696127841593693119648378,
                          -0.003948243987048265755956943934279479436,
                          0.014753882753110807504737245210435503395,
                          0.006127114472135553220610848512706070323,
                          -0.0210833702971454615970881008024662151  ,
                          -0.037947196105378770691896050948344054632,
                          -0.025783493863272863994007266796870680992,
                          0.003925239686319114892909531988607341191,
                          -0.192622497510184398361587909676018171012))
  
  if(Fs == 25)
    coeffs <- c(-0.184539794921875, -0.048187255859375, -0.043426513671875,
                0.028472900390625, -0.05963134765625,  -0.0357666015625,
                0.0308837890625,   -0.07794189453125,  -0.0322265625,
                0.05975341796875,  -0.137115478515625,  -0.0306396484375,
                0.52740478515625,   0.52740478515625,  -0.0306396484375,
                -0.137115478515625,   0.05975341796875,  -0.0322265625,
                -0.07794189453125,   0.0308837890625,   -0.0357666015625,
                -0.05963134765625,   0.028472900390625, -0.043426513671875,
                -0.048187255859375, -0.184539794921875)
  
  m <- length(coeffs)
  
  xx <- c(rep(0,m-1), xx)
  yy <- numeric(length(xx))
  for(i in m:length(xx))
    yy[i] <- sum(coeffs * xx[i:(i- (m-1))])
  
  return(yy[m:length(yy)])
  
}






############################################################################
## converts raw mojo data to current (as in C-libs)

convertToCurrent3 <-  function(xx, Rf, Isub, c1=0.016061056, c2=0.809626)
{
  temp32bitVal <- xx - 32768
  
  tempVal <- c1 * temp32bitVal
  tempVal <- tempVal / Rf
  tempVal <- tempVal - c2 * (Isub/10.0)
  return (1000000.0 * tempVal)
}






############################################################################
## remove discontinuities in current signal
## (uses additive spike removal, and does not consider LED changes)

removeCurrentDiscont <- function(xx, rf, isub, threshold=50000)
{
  n <- length(xx)
  
  xx_adjust <- numeric(n)
  running_DC <- 0
  
  if(missing(rf))
    rf <- numeric(n)
  
  if(missing(isub))
    isub <- numeric(n)
  
  for(i in 2:n){
    previousSample <- xx[i-1]
    grn1 <- xx[i]
    
    if((rf[i]-rf[i-1] != 0) | (isub[i]-isub[i-1] != 0))
      agc_changed <- TRUE
    else
      agc_changed <- FALSE
    
    if((abs(previousSample - grn1) > threshold) | agc_changed)
      running_DC <- running_DC + (grn1 - previousSample)
    xx_adjust[i] <- grn1 - running_DC
  }
  
  return(xx_adjust)
}






############################################################################
## remove discontinuities in current signal
## (uses additive spike removal, and considers LED changes)

removeCurrentDiscont2 <- function(xx, rf, isub, led, threshold=1e6)
{
  n <- length(xx)
  
  xx_adjust <- numeric(n)
  running_DC <- 0
  
  if(missing(rf))
    rf <- numeric(n)
  
  if(missing(isub))
    isub <- numeric(n)
  
  if(missing(led))
    led <- numeric(n)
  
  for(i in 2:n){
    previousSample <- xx[i-1]
    grn1 <- xx[i]
    
    if((rf[i]-rf[i-1] != 0) | (isub[i]-isub[i-1] != 0) | (led[i]-led[i-1] != 0))
      agc_changed <- TRUE
    else
      agc_changed <- FALSE
    
    if((abs(previousSample - grn1) > threshold) | agc_changed)
      running_DC <- running_DC + (grn1 - previousSample)
    xx_adjust[i] <- grn1 - running_DC
  }
  
  return(xx_adjust)
}






############################################################################
## remove discontinuities in current signal
## (uses multiplicative spike removal, and does not consider LED changes)

removeCurrentDiscont_m <- function(x, rf, isub, threshold=50000)
{
  fac <- 1
  n <- length(x)
  y <- numeric(n)
  
  fvec <- numeric(n)
  
  if(missing(rf))
    rf <- numeric(n)
  
  if(missing(isub))
    isub <- numeric(n)
  
  y[1] <- x[1]
  for(i in 2:n){
    
    if((rf[i]-rf[i-1] != 0) | (isub[i]-isub[i-1] != 0))
      agc_changed <- TRUE
    else
      agc_changed <- FALSE
    
    if((abs(x[i] - x[i-1]) > threshold) | agc_changed)
      fac <- (x[i-1]*fac)/x[i]
    
    y[i] <- x[i]*fac
    fvec[i] <- fac
  }
  
  return(list(y=y, fac=fvec))
}







############################################################################
## remove discontinuities in current signal
## (uses multiplicative spike removal, and considers LED changes)

removeCurrentDiscont2_m <- function(x, rf, isub, led, threshold=1e6)
{
  fac <- 1
  n <- length(x)
  y <- numeric(n)
  
  fvec <- numeric(n)
  
  if(missing(rf))
    rf <- numeric(n)
  
  if(missing(isub))
    isub <- numeric(n)
  
  if(missing(led))
    led <- numeric(n)
  
  y[1] <- x[1]
  for(i in 2:n){
    
    if((rf[i]-rf[i-1] != 0) | (isub[i]-isub[i-1] != 0) | (led[i]-led[i-1] != 0))
      agc_changed <- TRUE
    else
      agc_changed <- FALSE
    
    if((abs(x[i] - x[i-1]) > threshold) | agc_changed)
      fac <- (x[i-1]*fac)/x[i]
    
    y[i] <- x[i]*fac
    fvec[i] <- fac
  }
  
  return(list(y=y, fac=fvec))
}



######################################################################################
######################################################################################


## read the raw mojo data
dat <- read.table(....)

## remove rows with 0s and initial settling period
dat <- dat[dat[,1] != 0,]
dat <- dat[-c(1:100),]



## matrices which stores various forms of current output
## curr1 : current converted signals
## curr2 : current converted signals with additive discontinuity removal
## curr3 : current converted signals with multiplicative discontinuity removal
curr1 <- curr2 <- curr3 <- matrix(0, nr=nrow(dat)-1, nc=4)


## convert green, blank, red and IR signals to current
for(j in 1:4){
  if(j < 3){
    Rf <- dat[,9]
    led <- dat[,11]
  }
  else{
    Rf <- dat[,10]
    led <- dat[,j+13]
  }
  
  safe_term <- 0
  if(j == 2){
    safe_term <- 1
    led <- numeric(nrow(dat))
  }
  
  ## account for lag between samples and AFE settings
  dat_tmp <- dat[-1,j] 
  isub_tmp <- 10*dat[,10+j]
  isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
  Rf <- Rf[1:(len(Rf)-1)]
  led <- led[1:(len(led)-1)]
  
  curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
  curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)  
  curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e6)[[1]]
}


## plot current converted, blank subtracted green signal
windows()
plot(curr2[,1]-curr2[,2], type="l", lwd=2, col="forestgreen")


## plot current converted, blank subtracted, FIR filtered green signal
windows()
plot(FIRfilt(curr2[,1]-curr2[,2]), lwd=2, col="forestgreen")


