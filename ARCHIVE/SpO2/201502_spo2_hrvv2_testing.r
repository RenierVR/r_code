source("../useful_funs.r")
source("../HealthQ_OxySat/sp02_funs.r")


fpath1_1 <- "~/HealthQ/HRV_validation2/20150205/"
fpath2_1 <- paste0(rep(paste0(fpath1_1, "HRV_", c("Andre", "Kari", "Marc", "Nic", "Renier", "Tiaan")), each=2),
                   c("/Mojo 5106/", "/Mojo 5113/"))

fpath1_2 <- "~/HealthQ/HRV_validation2/20150213/"
fpath2_2 <- paste0(fpath1_2, "HRV_", c("Christopher", "Danie", "Dirk", "Dominique", "Eugene", "Kim", "Koch", "Kora",
                           "Lafras", "Liani", "LuckyMbele", "Michelle", "Nike", "Nina",
                           "Shannagh", "SiyakdumisaZono", "Claudia", "Michael"), "/")

fpath3 <- c(fpath2_1, fpath2_2)

f_ids <- c(paste0(rep(c("Andre", "Kari", "Marc", "Nic", "Renier", "Tiaan"), each=2), 1:2), 
           "Christopher", "Danie", "Dirk", "Dominique", "Eugene", "Kim", "Koch", "Kora", 
           "Lafras", "Liani", "LuckyMbele", "Michelle", "Nike", "Nina", "Shannagh", 
           "SiyakdumisaZono", "Claudia", "Michael")




#######################################################################################
#######################################################################################



i <- 6

for(i in 1:len(fpath3)){


files <- list.files(fpath3[i])
files_m <- files[grep("Unknown", files)]
dat <- read.table(paste0(fpath3[i], files_m[grep("csv", files_m)]), sep=",", head=T)



dat <- dat[dat[,1] != 0,]
curr1 <- curr2 <- matrix(0, nr=nrow(dat)-1, nc=4)
rfmat <- isubmat <- matrix(0, nr=nrow(dat)-1, nc=4)
for(j in 1:4){
  if(j < 3)
    Rf <- dat[,9]
  else
    Rf <- dat[,10]
  
  safe_term <- 0
  if(j == 2) safe_term <- 1
  
  dat_tmp <- dat[-1,j] 
  isub_tmp <- 10*dat[,10+j]
  isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
  Rf <- Rf[1:(len(Rf)-1)]
  curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
  curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp)[[1]]
  
  rfmat[,j] <- Rf
  isubmat[,j] <- isub_tmp
}


redI_clipped1 <- curr1[,3]-curr1[,2]
nirI_clipped1 <- curr1[,4]-curr1[,2] 
redI_clipped2 <- curr2[,3]-curr2[,2]
nirI_clipped2 <- curr2[,4]-curr2[,2] 

# zPlot(cbind(FIRfilt(redI_clipped1), FIRfilt(nirI_clipped1)), par_new=T)

# zPlot(cbind(redI_clipped1, FIRfilt(redI_clipped1)), par_new=T)
# zPlot(cbind(nirI_clipped1, FIRfilt(nirI_clipped1)), par_new=T)

red2 <- redI_clipped2
nir2 <- nirI_clipped2

# red2 <- ksmooth2(redI_clipped2, 10)
# nir2 <- ksmooth2(nirI_clipped2, 10)

# zPlot(cbind(red2, nir2), par_new=T, col_f=c("red", "forestgreen"))
# zPlot(cbind(FIRfilt(red2), FIRfilt(nir2)), par_new=T, col_f=c("red", "forestgreen"))
            

out <- fftSpO2_Cdev(redI_clipped1, nirI_clipped1, subtractMeans=F, filtType="fir50", 
                    windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=T,
                    spo2_caleb_pf=0.7, spo2_caleb_e1x=0.08, spo2_caleb_e2x=0.29, 
                    spo2_caleb_e1y=0.81, spo2_caleb_e2y=0.18, spo2_caleb_bias=0)

spo2_1  <- 100*calabFreeSpO2(out[[1]], clip=F)#, smoothAlpha=out[[3]][,8])

out <- fftSpO2_Cdev(red2, nir2, subtractMeans=F, filtType="fir50", 
                    windowSize=2^8, fftL=0, logicSD=10, fftI=F, interpBW=0, spo2_check=F, useMaxDenom=T,
                    spo2_caleb_pf=0.7, spo2_caleb_e1x=0.08, spo2_caleb_e2x=0.29, 
                    spo2_caleb_e1y=0.81, spo2_caleb_e2y=0.18, spo2_caleb_bias=0)

spo2_2  <- 100*calabFreeSpO2(out[[1]], clip=F)#, smoothAlpha=out[[3]][,8])

w() ; lplot(movingAve(spo2_1,1), lwd=2, col="forestgreen", ylim=c(60,110))
lines(movingAve(spo2_2,1), lwd=2, col="red")
title(main=substring(fpath3[i], 27, nchar(fpath3[i])-1))


savePlot(paste0("spo2_results/spo2_rest", i, "_", f_ids[i]), "png")
graphics.off()

cat(round(100*i/len(fpath3),2), "% completed\n")

}







