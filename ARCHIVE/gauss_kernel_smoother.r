p_out <- read.table("D:/HealthQ/Output/AutoVO2/dev/acc_filt.csv")

xx1 <- ksmooth2(p_out[,1], 10)

zPlot(cb(p_out, xx1))


initKernelWeights <- function(bw)
{
  eff_bw <- 3*bw - 1
  
  w_temp <- abs((0:(eff_bw-1) - floor(eff_bw/2))/(bw*0.3706506))
  gauss_w <- exp(-0.5*w_temp^2)
  gauss_w <- gauss_w/sum(gauss_w)
  
  return(gauss_w)
}

BDRksmooth <- function(x, bw)
{
  eff_bw <- 3*bw - 1
  y <- numeric(eff_bw)
  gauss_w <- initKernelWeights(bw)
  
  x2 <- numeric(len(x))
  for(j in 1:len(x)) {
    
    num = 0
    for(i in 2:eff_bw) {
      y[i-1] = y[i]
      num = num + gauss_w[i-1]*y[i-1]
    }
    
    y[eff_bw] = x[j]
    num = num + gauss_w[eff_bw]*x[j]
    
    x2[j] = num
  }
  
#   x2 <- stats::filter(x=x, gauss_w)
#   x2[is.na(x2)] <- x[is.na(x2)]
#   x2 <- as.numeric(x2)

  return(x2)
}






set.seed(1234)
xx <- 100*(sin(3*seq(0.1, 10, 0.1)) + rnorm(100, 0, 0.2))

BANDWIDTH <- 10

xx1 <- ksmooth2(xx, BANDWIDTH)
xx2 <- BDRksmooth(xx, BANDWIDTH)

w() ; lplot(xx, lwd = 3, col = "red")
lines(xx1, col = "blue", lwd = 2)
lines(xx2[-c(1:floor((3*BANDWIDTH-1)/2))], col = "mediumseagreen", lwd = 2)
# lines(p_out[-c(1:floor((3*BANDWIDTH-1)/2))], col = "orange", lwd = 2, lty = 2)
lines(p_out, col = "orange", lwd = 2, lty = 2)

zPlot(cb(xx, xx1, xx2[-c(1:floor((3*BANDWIDTH-1)/2))]))
