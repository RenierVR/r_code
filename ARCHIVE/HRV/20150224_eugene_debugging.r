source("../useful_funs.r")
source("../SpO2/sp02_funs.r")


data_list <- getDataFromDir("D:/HealthQ/Data/HRV/20150223 Eugene Debugging/", head=T)

for(i in 1:len(data_list)){
  
  writeMojoFormat(data_list[[i]], file=paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/data/HRV_eugenedebug/", 
                                              paste0(substring(names(data_list)[i], 1, 13), "_eugene_hrvdebug.txt")))
  
}



#################################################################################################


w() ; par(ask=T)
for(i in 1:len(data_list)){

  pcout <- read.table(paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/", 
                      paste0("physcalc_out_", substring(names(data_list)[i], 1, 13), "_eugene_hrvdebug.txt")), sep=",")

  rr <- pcout[,3]
  rr <- rr[which(diff(rr) != 0) + 1]

#   lplot(rr, lwd=2, main=substring(names(data_list)[i], 1, 13), ylim=c(600,1200))

print(pcout[nrow(pcout),6:7])
  
}


dat <- data_list[[11]]


dat <- dat[dat[,1] != 0,]
curr1 <- curr2 <- matrix(0, nr=nrow(dat)-1, nc=4)
rfmat <- isubmat <- matrix(0, nr=nrow(dat)-1, nc=4)
for(j in 1:4){
  if(j < 3)
    Rf <- dat[,9]
  else
    Rf <- dat[,10]
  
  dat_tmp <- dat[-1,j]
  
  isub_tmp <- 10*dat[,10+j]
  isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
  
  Rf <- Rf[1:(len(Rf)-1)]
  
  curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)#, pars[1], pars[2])
  curr2[,j] <- removeCurrentDiscont(curr1[,j], Rf, isub_tmp)
  
  rfmat[,j] <- Rf
  isubmat[,j] <- isub_tmp
}

zPlot(curr2[,1]-curr2[,2])


# zPlot(dat[,1])


