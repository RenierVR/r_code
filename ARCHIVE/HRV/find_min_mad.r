###################################################################################################
###################################################################################################


find_min_mad <- function(rr1, rr2, 
                         lags1 = seq(-10, 10, by=0.1),
                         lags2 = seq(-1, 1, by=0.005))
{
  if(ncol(cb(rr1)) == 1) rr1 <- cb(cumsum(rr1)/1000, rr1)
  if(ncol(cb(rr2)) == 1) rr2 <- cb(cumsum(rr2)/1000, rr2)
  
  mad_lags_f <- function(lags) {
    sapply(lags, function(k) {
      mad_vec <- ( rr1[,2] - rr2[sapply(rr1[,1] + k, function(x) which.min(abs(x - rr2[,1]))), 2] )
      return( median(abs(mad_vec)) )
    })
  }
  
  mad_lags1 <- mad_lags_f(lags1)
  k_min_lag1 <- lags1[mean(which(mad_lags1 == min(mad_lags1)))]
  
  mad_lags2 <- mad_lags_f(lags2 + k_min_lag1)
  k_min_lag2 <- lags2[mean(which(mad_lags2 == min(mad_lags1)))] + k_min_lag1
  
  
  cat("Minimum lag: ", format(round(k_min_lag2, 3), digits=4), "s\n", sep="")
  
  invisible(list("min_lag" = k_min_lag2,
                        "mad_lag" = mad_lags2))
}

###################################################################################################

# set.seed(666)
# y1 <- round(rnorm(150, 1000, 10), 2)
# y2 <- y1 + rnorm(len(y1), 0, 2)
# y2 <- round(c(rnorm(5, 1000, 10), y2[1:(len(y2)-5)]), 2)
# 
# x1 <- cumsum(y1)/1000
# x2 <- cumsum(y2)/1000
# 
# w() ; lplot(x1, y1, lwd=2) ; lines(x2, y2, lwd=2, col="blue")
# 
# min_lag <- find_min_mad(y1, y2)[[1]]
# 
# w() ; lplot(x1+min_lag, y1, lwd=2) ; lines(x2, y2, lwd=2, col="blue")

###################################################################################################
###################################################################################################