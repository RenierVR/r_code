source("../useful_funs.r")
fpaths <- paste0("D:/HealthQ/Data/RRI/GarminValidation/", c("20160829_TPID0223/",
                                                            


# Read Garmin and Mojo data
dat_m <- read.table(paste0(fpath, "MOJO/20160829_093837_20160829_TPID0223_LQ16-10187_Right_FREELIVING.csv"), head=T, sep=",")
dat_g <- read.table(paste0(fpath, "Garmin/20160829_093829_20160829_TPID0223_Garmin_T3_FREELIVING.csv"), head=T, sep=",")

mojo_rr <- dat_m[(which(!is.na(dat_m[,30]))[1]):nrow(dat_m), 30:32]
mojo_rr <- mojo_rr[c(1, which(diff(mojo_rr[,1]) != 0) + 1),]
mojo_rr <- cb(cumsum(mojo_rr[,1])/1000, mojo_rr)

garm_rr <- dat_g[(which(!is.na(dat_g[,14]))[1]):nrow(dat_g), 14:16]
garm_rr <- garm_rr[c(1, which(diff(garm_rr[,1]) != 0) + 1),]
garm_rr <- cb(cumsum(garm_rr[,1])/1000, garm_rr)


# Read reference data
library(edfReader)
ref_header <- readEdfHeader(paste0(fpath, "20160829_TPID0223_Faros_F1.EDF"))
ref <- readEdfSignals(ref_header)
ref_rr <- ref$HRV$signal
ref_rr <- ref_rr[ref_rr != 0]
ref_rr <- cb(cumsum(ref_rr)/1000, ref_rr)







###################################################################################################
###################################################################################################

# Plot to inspect

###################################################################################################
###################################################################################################




# plotScroll <- function(ref_rr, mojo_rr, garm_rr, lag_ref, lag_mojo, lag_garm)
# {
ref_rr_align <- ref_rr[,]#[-c(1:25),]
mojo_rr_align <- mojo_rr[,]
garm_rr_align <- garm_rr[,]#[-c(1:12),]


n <- max(tail(ref_rr[,1], 1), tail(mojo_rr[,1], 1), tail(garm_rr[,1], 1))

w() ; par(ask = T)#, mfrow = c(2, 1), mar = c(2, 4, 1, 1))
start <- 1 ; winsize <- 200 ; overlap <- 100 ; j <- 0 ; last <- FALSE
if(winsize > n) winsize <- n
while(TRUE){
  
  j = j+1
  plotind <- (overlap*(j-1)+start):(start-1+winsize + overlap*(j-1))
  
  if(max(plotind) > n) {
    last <- TRUE
    plotind <- plotind[1]:n
  }
  
  
  # Determine array indice range corresponding to plot x-axis
  ref_rr_p <- (ref_rr_align[,1] >= plotind[1]) & (ref_rr_align[,1] <= tail(plotind, 1))
  mojo_rr_p <- (mojo_rr_align[,1] >= plotind[1]) & (mojo_rr_align[,1] <= tail(plotind, 1))
  garm_rr_p <- (garm_rr_align[,1] >= plotind[1]) & (garm_rr_align[,1] <= tail(plotind, 1))
  
  # select relevant parts of matrices
  ref_rr_align_p <- ref_rr_align[ref_rr_p,]
  mojo_rr_align_p <- mojo_rr_align[mojo_rr_p,]
  garm_rr_align_p <- garm_rr_align[garm_rr_p,]
  
  
  # Calculate alignment factors
  ccf_mojo <- ccf(ref_rr_align_p[,2], mojo_rr_align_p[,2], lag=60, plot=F)
  ccf_mojo_lag <- ccf_mojo$lag[which.max(abs(ccf_mojo$acf))]
  
  ccf_garm <- ccf(ref_rr_align_p[,2], garm_rr_align_p[,2], type="covariance", lag=60, plot=F)
  ccf_garm_lag <- ccf_garm$lag[which.max(abs(ccf_garm$acf))]
  print(ccf_garm_lag)
  
  # Align
  ref_rr_align_p <- ref_rr_align_p[,]
  mojo_rr_align_p <- mojo_rr_align_p[,]
  garm_rr_align_p <- garm_rr_align_p[,]
  
  ref_rr_align_p[,1] <- cumsum(ref_rr_align_p[,2])/1000
  mojo_rr_align_p[,1] <- cumsum(mojo_rr_align_p[,2])/1000
  garm_rr_align_p[,1] <- cumsum(garm_rr_align_p[,2])/1000
  
  
  
  # Plot
  lplot(ref_rr_align_p[, 1], ref_rr_align_p[, 2], lwd=3, col="red", ylim=c(200, 2000))
  # lines(mojo_rr_align_p[, 1], mojo_rr_align_p[, 2], lwd=2, col="blue")
  lines(garm_rr_align_p[, 1], garm_rr_align_p[, 2], lwd=2, col="mediumseagreen")
  
  # last <- TRUE
  if(last) break
}
# }


# w() ; par(ask = T)#, mfrow = c(2, 1), mar = c(2, 4, 1, 1))
# start <- 1 ; winsize <- 200 ; overlap <- 100 ; j <- 0 ; last <- FALSE
# if(winsize > n) winsize <- n
# while(TRUE){
#   
#   j = j+1
#   plotind <- (overlap*(j-1)+start):(start-1+winsize + overlap*(j-1))
#   
#   if(max(plotind) > n) {
#     last <- TRUE
#     plotind <- plotind[1]:n
#   }
#   
#   
#   ref_rr_p <- (ref_rr[,1] >= plotind[1]) & (ref_rr[,1] <= tail(plotind, 1)) 
#   ref_rr_align_p <- ref_rr[ref_rr_p,]
#   lplot(ref_rr_align_p[, 1], ref_rr_align_p[, 2], lwd=3, col="red", ylim=c(200, 2000),
#         xlab="time (s)", ylab="RR value (ms)")
#   
#   # last <- TRUE
#   if(last) break
# }



###################################################################################################
###################################################################################################

# AlignToRef

###################################################################################################
###################################################################################################

source("alignToRefRR_v2.r")
source("ERMADT_funs.r")

align_out_mojo <- alignToRefRR(ref = ref_rr[,2], rrmat = mojo_rr[,1:3], winsize = 100,
                               LAG_RANGE_NORMAL = 10, LAG_RANGE_PRECISE = 5,
                               ANY_PLOTS = T, PLOT_TYPE = 2, SAVE_PATH = "./results/GarminValidation/mojo_segment")

align_out_garm <- alignToRefRR(ref = ref_rr[,2], rrmat = garm_rr[,1:3], winsize = 100, 
                               LAG_RANGE_NORMAL = 10, LAG_RANGE_PRECISE = 5,
                               ANY_PLOTS = T, PLOT_TYPE = 2, SAVE_PATH = "./results/GarminValidation/garm_segment")









