source("../useful_funs.r")
source("./ERMADT_funs.r")

dat <- read.table("D:/HealthQ/Data/Nokia/streamlog_10_12_2015_22_36", head = T, sep = ",")


dat <- cb(dat[-1, 1:8], dat[1:(nrow(dat)-1), 9:24])

grncur <- convertToCurrent3(dat[,2], dat[,9], dat[,11])
ambcur <- convertToCurrent3(dat[,3], dat[,9], dat[,12])

acc <- acc_res(dat[,7:8], 16)


rr_embedded <- dat[dat[,22] != 0, 22]


zPlot(cb(grncur, ambcur, dat[, c(9, 11, 12)]), 
      col_f = c("forestgreen", "blue", "yellow", "red", "orange"), par_new = T)




zPlot(cb(grncur_sub_adjusted, grncur_adjusted - ambcur), par_new=T)
zPlot(cb(ambcur, dat[,c(3, 9, 11, 12)]), par_new = T, col_f = c("blue", "forestgreen", "yellow", "red", "orange"))





grncur_adjusted <- removeCurrentDiscont2(grncur, dat[,9], dat[,11], dat[,15])

grncur_sub_adjusted <- removeCurrentDisconts(grncur - ambcur, dat[,9], dat[,9], dat[,11], dat[,12], dat[,15])


Fs <- 50
hrv_out <- ER_MA_DT_peaks_C(grncur_adjusted - ambcur, filt_t="FIRHAM", clip=4*Fs, output=T, interp=T, bf_fs=Fs,
                            check_mult_peaks_above_zero=T, post_filt_running_mean=F, print_output = T,
                            run_mean_prefilt_cap = 50)
rr1 <- hrv_out$RR
xf1 <- hrv_out$filt_signal
pks1 <- cb(hrv_out$peaks, hrv_out$peakvals)


Fs <- 50
hrv_out <- ER_MA_DT_peaks_C(grncur_adjusted - ambcur, filt_t="FIRHAM", clip=4*Fs, output=T, interp=T, bf_fs=Fs,
                            check_mult_peaks_above_zero=T, post_filt_running_mean=F, print_output = T,
                            run_mean_prefilt_cap = 100)
rr2 <- hrv_out$RR
xf2 <- hrv_out$filt_signal
pks2 <- cb(hrv_out$peaks, hrv_out$peakvals)





###################################################################################################
###################################################################################################

# cout <- read.table("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/physcalc_out_20151210_NokiaLongHRV.csv", head = T, sep = ",")
cout <- read.table("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/out_testing.txt", head = T, sep = ",")


###################################################################################################
###################################################################################################


pind <- 373100:403100
x_grn <- grncur_adjusted[pind]

CAP <- 100
xmean <- x_grn
for(i in 2:len(x_grn)) {
  n <- i
  if(i > CAP) n <- CAP
  xmean[i] <- xmean[i-1]*((n-1)/n) + x_grn[i]*(1/n)
}

x_grn_ms <- x_grn - xmean

# zPlot(cb(x_grn, x_grn_ms), par_new = T)

x_grn_f <- FIRfilt(x_grn)
x_grn_f_ms <- FIRfilt(x_grn_ms)
x_grn_f2 <- FIRfilt_50hz_LP_hamming_20_6hz(x_grn_f)
x_grn_f2_ms <- FIRfilt_50hz_LP_hamming_20_6hz(x_grn_f_ms)

zPlot(cb(x_grn, x_grn_f, x_grn_f2), par_new = T)
zPlot(cb(x_grn_f2, x_grn_f2_ms), par_new = T)


###################################################################################################
###################################################################################################



rr <- rr1
xf <- xf1
pks <- pks1


pks[,1] <- pks[,1]*0.02
rr_csum <- cumsum(rr)/1000
rr_csum <- rr_csum + pks[1, 1]

rr_ot <- detectRROutliers(rr)[,1]


w() ; par(ask = T, mfrow = c(2, 1), mar = c(2, 4, 1, 1))
start <- 7000 ; winsize <- 20 ; overlap <- winsize/2 ; j <- 0
while(TRUE){
  
  j = j+1
  plotind <- (overlap*(j-1)+start):(start-1+winsize + overlap*(j-1))
  
  if(max(plotind) > max(rr_csum))
    break
  else{
    
    grn_pind <- range(plotind)*50
    grn_pind <- min(grn_pind):max(grn_pind)
    
    pks_plot <- pks[(pks[,1] >= min(plotind)) & (pks[,1] <= max(plotind)),]
    
    par(xpd = F)
    lplot(grn_pind*0.02, xf[grn_pind], lwd = 2, col = "mediumseagreen",
          xlim = range(plotind), xlab = "", ylab = "filtered")
    points(pks_plot, pch = 19, col = "forestgreen")
    
    
    ######################################################################
    
    which_rr <- (rr_csum >= min(plotind)) & (rr_csum <= max(plotind))
    xp <- rr_csum[which_rr]
    yp <- rr[which_rr]
    
    
    
    
    plot(xp, yp, type = "n", xlab = "time (s)", ylab = "RR (ms)",
         xlim = range(plotind), ylim = c(800, 1200))
    abline(v = rr_csum[rr_ot == 1], lwd = 2, col = "red", lty = 1)
    lines(xp, yp, lwd = 2, col = "blue")
    points(xp, yp, pch = 19, col = "blue", cex = 0.8)
    
    par(xpd = NA)
    abline(v = xp, lty = 2)
    
  }
}


