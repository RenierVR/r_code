source("../useful_funs.r")
source("./ERMADT_funs.r")


###################################################################################################
###################################################################################################

dat <- read.table("D:/HealthQ/Data/ADI/StressProtocol_1.csv", head = T, sep = ",")

acc <- acc_res(dat[,3:5])

zPlot(cb(dat[,2], acc), par_new=T)

Fs <- 50
hrv_out <- ER_MA_DT_peaks_C(dat[,2], filt_t="FIRHAM", clip=4*Fs, output=T, interp=T, bf_fs=Fs,
                            check_mult_peaks_above_zero=T, post_filt_running_mean=F)
rr1 <- hrv_out$RR
xf1 <- hrv_out$filt_signal



w() ; lplot(1:nrow(dat)/50, dat[,3], lwd=1, col = "red", ylim = range(dat[,3:5]),
            xlab = "time (s)", ylab = "accelerometer")
lines(1:nrow(dat)/50, dat[,4], col = "blue")
lines(1:nrow(dat)/50, dat[,5], col = "forestgreen")
legend("topright", lwd = 3, col = c("red", "blue", "forestgreen"),
       leg = paste("ACC", c("X", "Y", "Z")), bg = "white")



pind <- 53800:55200
w() ; lplot(pind/50, xf1[pind], lwd=1, col = "forestgreen",
            xlab = "time (s)", ylab = "")


w() ; lplot(cumsum(rr1)/1000, rr1,
            xlab = "time (s)", ylab = "RR (ms)", main = "RRI output")


###################################################################################################
###################################################################################################


datlist <- getDataFromDir("D:/HealthQ/Data/ADI/Misc Testing/", head=T)

dat <- datlist[[1]]


# grn <- dat[-1, 2]
# grn_led <- dat[-1, 9]
# grn <- grn[-1]
# grn_led <- grn_led[-len(grn_led)]

grn <- dat[,2]
grn_led <- dat[,9]


grn <- removeCurrentDiscont2(grn, rep(0, len(grn)), rep(1, len(grn)), grn_led)

Fs <- 25
hrv_out <- ER_MA_DT_peaks_C(grn, filt_t="FIRHAM", clip=4*Fs, output=T, interp=T, bf_fs=Fs,
                            check_mult_peaks_above_zero=T, post_filt_running_mean=T)
rr1 <- hrv_out$RR
xf1 <- hrv_out$filt_signal



cout <- read.table(paste0("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/physcalc_out_", names(datlist)[i], ".csv"), sep=",", head=T)
crrmat <- cout[cout[,10] == 1, c(11,13)]
crr <- crrmat[,1]

w() ; lplot(cumsum(rr1)/1000, rr1)
lines(cumsum(crr)/1000, crr, col = "blue")



###################################################################################################
###################################################################################################

library(signal)

dat <- read.table("D:/HealthQ/Data/ADI/StressProtocol_2.csv", head = T, sep = ",")



tstamp <- numeric(nrow(dat))
td_prev <- 0
for(i in 1:nrow(dat)) {
  
  td <- strsplit(as.character(dat[i,5]), ":")[[1]][3:4]
  if(nchar(td[2]) == 1)
    td[2] <- paste0("00",  td[2])
  if(nchar(td[2]) == 2)
    td[2] <- paste0("0", td[2])
  if(nchar(td[2]) == 4) {
    td[1] <- as.character(as.numeric(td[1]) + 1)
    td[2] <- "000"
  }
  
  td <- as.numeric(paste(td, collapse = "."))
  
  if(i == 1)
    td_prev <- td
  else {
    
    tstamp[i] <- td - td_prev
    if(tstamp[i] < 0) tstamp[i] <- tstamp[i] + 60
    
    td_prev <- td
    
  }
  
}


times <- cumsum(tstamp)

pind <- 1:1000
w() ; lplot(times[pind], dat[pind, 1])



##########################################################################

x2 <- seq(0, max(times), 0.02)
y2 <- interp1(times, dat[,1], x2, method = "spline")


Fs <- 50
hrv_out <- ER_MA_DT_peaks_C(y2, filt_t="FIRHAM", clip=4*Fs, output=T, interp=T, bf_fs=Fs,
                            check_mult_peaks_above_zero=T, post_filt_running_mean=F)
rr1 <- hrv_out$RR
xf1 <- hrv_out$filt_signal
pks1 <- cb(hrv_out$peaks, hrv_out$peakvals)

hrv_out <- ER_MA_DT_peaks_C(y2, filt_t="FIRHAM", clip=4*Fs, output=T, interp=T, bf_fs=Fs,
                            check_mult_peaks_above_zero=T, post_filt_running_mean=T)
rr2 <- hrv_out$RR
xf2 <- hrv_out$filt_signal
pks2 <- cb(hrv_out$peaks, hrv_out$peakvals)

rr1_csum <- cumsum(rr1)/1000
rr2_csum <- cumsum(rr2)/1000

rr1_o <- detectRROutliers(rr1)[,1]
rr1_ot <- rr1_csum[which(rr1_o == 1)]
rr2_o <- detectRROutliers(rr2)[,1]
rr2_ot <- rr2_csum[which(rr2_o == 1)]

rr2_oc <- as.numeric(!rr2_o)

# w() ; plot(cb(rr1_ot, 1), pch = 19, col = "blue", ylim = c(0,3), xlim = c(1000,1200)) ; points(cb(rr2_ot, 2), pch = 19, col = "red")



# w() ; pind <- 1:len(rr1)
# plot((cumsum(rr1)/1000)[pind], rr1[pind], type = "n", xlab = "time (s)", ylab = "RR (ms)")
# abline(v = rr1_ot, lwd = 2, col = "green")
# lines((cumsum(rr1)/1000)[pind], rr1[pind], lwd = 2, col = "forestgreen")
# abline(v = rr2_ot, lwd = 2, col = "lightblue")
# lines((cumsum(rr2)/1000)[pind], rr2[pind], lwd = 2, col = "blue")

w()
plot(rr2_csum, rr2, type = "n", ann = F, ylim = c(400, 900))
abline(v = rr2_ot, col = "red")
lines(rr2_csum, rr2, col = "blue")
title(xlab = "time (s)", ylab = "RRI (ms)",
      main = "ADI stress protocol RR interval result")
legend("topright", lwd = 2, col = c("blue", "red"),
       leg = c("LifeQ RRIs", "LifeQ RR validity"), bg = "white")



##########################################################################


xf <- xf1 ; pks <- pks1
rr <- rr1 ; rr_csum <- rr1_csum ; rr_ot <- rr1_ot

xf <- xf2 ; pks <- pks2
rr <- rr2 ; rr_csum <- rr2_csum ; rr_ot <- rr2_ot


pks[,1] <- pks[,1]*0.02
rr_csum <- rr_csum + pks[1, 1]


rr_o <- detectRROutliers(rr)[,1]
rr_ot <- rr_csum[which(rr_o == 1)]



w() ; par(ask = T, mfrow = c(2, 1), mar = c(2, 4, 1, 1))
start <- 1 ; winsize <- 20 ; overlap <- 10 ; j <- 0
while(TRUE){
  
  j = j+1
  plotind <- (overlap*(j-1)+start):(start-1+winsize + overlap*(j-1))
  
  if(max(plotind) > max(rr_csum))
    break
  else{
    
    grn_pind <- range(plotind)*50
    grn_pind <- min(grn_pind):max(grn_pind)
    
    pks_plot <- pks[(pks[,1] >= min(plotind)) & (pks[,1] <= max(plotind)),]
    
    par(xpd = F)
    lplot(grn_pind*0.02, xf[grn_pind], lwd = 2, col = "mediumseagreen",
          xlim = range(plotind), xlab = "", ylab = "filtered")
    points(pks_plot, pch = 19, col = "forestgreen")
    

    ######################################################################
    
    which_rr <- (rr_csum >= min(plotind)) & (rr_csum <= max(plotind))
    xp <- rr_csum[which_rr]
    yp <- rr[which_rr]
    
    
    plot(xp, yp, type = "n", xlab = "time (s)", ylab = "RR (ms)",
         xlim = range(plotind))
    abline(v = rr_ot, lwd = 2, col = "red", lty = 1)
    lines(xp, yp, lwd = 2, col = "blue")
    points(xp, yp, pch = 19, col = "blue", cex = 0.8)
    
    par(xpd = NA)
    abline(v = xp, lty = 2)

  }
}




