# source("../movingAve.r")
# source("../RLS/RLS_wiki.r")
# source("../HRV/improved_peaks_cubic_interp.r")
source("../ARCHIVE/HRV/improved_peaks_cubic_interp.r")
source("../ARCHIVE/RLS/RLS_wiki.r")


## PEAK DETECTION with DUAL MOVING AVERAGES
##
####################################################################
####################################################################
##
## source: http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0076585#pone-0076585-t002
## Method IV: Event-Related Moving Averages with Dynamic Threshold
##
####################################################################


ER_MA_DT_peaks <- function(x, w1=ceiling(111/20), w2=ceiling(667/20),
                           bf_stime=200, bf_ord=2, bf_f1=0.5, bf_f2=8, bf_fs=50, beta=0.002,
                           output_all=FALSE, interp=TRUE, print_output=TRUE)
{
  len <- function(...) length(...)
  
  pks <- NULL
  
  cat("preprocessing...\n")
  
  # bandpass filter
  require(signal)
  x_butt0 <- filter(butter(bf_ord, c(bf_f1, bf_f2)/(bf_fs/2), type="pass"), x)
  x_butt0 <- filter(butter(bf_ord, c(bf_f1, bf_f2)/(bf_fs/2), type="pass"), rev(x_butt0))
  x_butt0 <- rev(x_butt0)
  
  x_butt0 <- x_butt0[-c(1:bf_stime)]
  x_butt  <- x_butt0
  
  # clipping and squaring filtered signal
  x_butt[x_butt < 0] <- 0
  y <- x_butt^2
  
  # application of moving averages
  ma_peak <- movingAve(y, w1)
  ma_beat <- movingAve(y, w2)
  
  # thresholding to generate blocks of interest
  blocks <- numeric(length(ma_peak))
  #   thr1 <- ma_beat + (beta*mean(y[-(1:bf_stime)]))
  thr1 <- ma_beat + (beta*mean(y))
  blocks[ma_peak > thr1] <- 0.1
  
  block_list <- vector("list")
  prev_block <- numeric(0)
  for(i in 1:length(blocks)){
    
    if(blocks[i] > 0)
      prev_block <- c(prev_block, i)
    else if(len(prev_block) > 0 ){
      block_list[[len(block_list)+1]] <- prev_block
      
      #       if(len(prev_block) > w1)
      pks <- c(pks, prev_block[which(ma_peak[prev_block] == max(ma_peak[prev_block]))])
      
      prev_block <- numeric(0)
    }
    
    cat(round(100*i/length(blocks), 2), "%\n")
    
  }
  
  if(interp)
    pks <- getInterpPeaks(x_butt0, pks)
  
  if(output_all)
    return(list(filt_signal=x_butt0, clipped_signal=y,
                ma_peak=ma_peak, ma_beat=ma_beat, thr1=thr1,
                blocks=blocks, block_list=block_list,
                peaks=pks))
  else
    return(pks)
  
}


##############################################################################################
##############################################################################################
##############################################################################################


ER_MA_DT_peaks_C <- function(x, filt_type, filt_fun, clip=0, post_filt_running_mean=FALSE,
                             w1=3, w2=45, bf_stime=10, bf_ord=3, bf_f1=0.5, bf_f2=8, 
                             bf_fs=50, beta=0.02, bf_fs_actual=bf_fs,
                             check_mult_peaks_above_zero=FALSE,
                             run_mean_prefilt_cap=50, run_mean_postfilt_cap=50, 
                             run_mean_algo_cap=50, interp=TRUE,
                             amend_peaks_under_0=TRUE,
                             output_all=FALSE, print_output=FALSE)
{
  len <- function(...) length(...)
  
  if(print_output) cat("preprocessing...\n")
  
  # adjust for 25Hz sampling freq
  if(bf_fs == 25) {
    w2 <- 23
  }
  
  
  # estimate running mean
  xmean <- x
  for(i in 2:len(x)) {
    n <- i
    if(i > run_mean_prefilt_cap) n <- run_mean_prefilt_cap
    xmean[i] <- xmean[i-1]*((n-1)/n) + x[i]*(1/n)
  }
  
  x <- x - xmean
  
  # bandpass filter
  require(signal)
  if(filt_type == "ChebyGauss"){
    x_butt0 <- filter(cheby1(bf_ord, 1, W=c(bf_f1, bf_f2)/(bf_fs/2), type="pass"), x)
    x_g <- ksmooth(1:length(x_butt0), x_butt0, "normal", band=6)$y
  }
  else if(filt_type == "Cheby"){
    x_butt0 <- filter(cheby1(bf_ord, 1, W=c(bf_f1, bf_f2)/(bf_fs/2), type="pass"), x)
    x_g <- x_butt0
  }
  else if(filt_type == "Gauss"){
    x_g <- ksmooth(1:length(x), x, "normal", band=6)$y
  }
  else if(filt_type == "Butter"){
    x_butt0 <- filter(butter(bf_ord, c(bf_f1, bf_f2)/(bf_fs/2), type="pass"), x)
    x_butt0 <- x_butt0[-c(1:bf_stime)]
    x_g     <- ksmooth(1:length(x_butt0), x_butt0, "normal", band=6)$y
  }
  else if(filt_type == "ButterButter"){
    x_butt0 <- filter(butter(bf_ord, c(bf_f1, bf_f2)/(bf_fs/2), type="pass"), x)
    x_butt0 <- filter(butter(bf_ord, c(bf_f1, bf_f2)/(bf_fs/2), type="pass"), x_butt0)

    x_butt0 <- x_butt0[-c(1:bf_stime)]
    x_g     <- x_butt0
  }
  else if(filt_type == "ButterButterRev"){
    x_butt0 <- filter(butter(bf_ord, c(bf_f1, bf_f2)/(bf_fs/2), type="pass"), x)
    x_butt0 <- filter(butter(bf_ord, c(bf_f1, bf_f2)/(bf_fs/2), type="pass"), rev(x_butt0))
    x_butt0 <- rev(x_butt0)
  
    x_butt0 <- x_butt0[-c(1:bf_stime)]
    x_g     <- x_butt0
  }
  else if(filt_type == "FIR50"){
    x_butt0 <- FIRfilt(x, bf_fs)
    x_g      <- x_butt0
  }
  else if(filt_type == "FIRFIR"){
    x_butt0 <- FIRfilt(x, bf_fs)
    x_butt0 <- FIRfilt(x_butt0, bf_fs)
    x_g     <- x_butt0
  }
  else if(filt_type == "FIRHAM"){
    x_butt0 <- FIRfilt(x, bf_fs)
    if(bf_fs == 50)
      x_butt0 <- FIRfilt_50hz_LP_hamming_20_6hz(x_butt0)
    if(bf_fs == 25)
      x_butt0 <- FIRfilt_25hz_LP_hamming_10_6hz(x_butt0)
    x_g     <- x_butt0
  }
  else if(filt_type == "FUN"){
    x_butt0 <- filt_fun(x)
    x_g     <- x_butt0
  }
  
  
  if(post_filt_running_mean) {
    xgmean <- x_g
    for(i in 2:len(x_g)) {
      n <- i
      if(i > run_mean_postfilt_cap) n <- run_mean_postfilt_cap
      xgmean[i] <- xgmean[i-1]*((n-1)/n) + x_g[i]*(1/n)
    }
    
    x_g <- x_g - xgmean
  }
  
  
  # clipping and squaring filtered signal
  y <- x_g^2
  y[x_g < 0] <- 0
  
  
  if(print_output) cat("applying moving averages...\n")
  
  
  # application of moving averages
  ma_peak <- movingAve(y, w1, 2)
  ma_beat <- movingAve(y, w2, 2)
  
  # clipping for comparison with C
  if(clip != 0){
    x_g     <- x_g[-c(1:clip)]
    y       <- y[-c(1:clip)]
    ma_peak <- ma_peak[-c(1:clip)]
    ma_beat <- ma_beat[-c(1:clip)]
  }
  
  # estimate running mean of y
  ymean <- y
  for(i in 2:len(y)) {
    n <- i
    if(i > run_mean_algo_cap) n <- run_mean_algo_cap
    ymean[i] <- ymean[i-1]*((n-1)/n) + y[i]*(1/n)
  }
  
  pks <- NULL
  
  # thresholding to generate blocks of interest
  blocks <- numeric(length(ma_peak))
  thr1 <- ma_beat + beta*ymean #(beta*mean(y))
  blocks[ma_peak > thr1] <- 1
  
  blocks0 <- as.numeric(y > 0)
  
  
  if(print_output) cat("analysing blocks...\n")
  
  
  prev_block <- numeric(0)
  prev_block0 <- numeric(0)
  block0_list <- vector("list")
  
  for(i in 1:length(blocks)){
    
    if(check_mult_peaks_above_zero) {
      if(blocks0[i] == 1)
        prev_block0 <- c(prev_block0, i)
      else if(len(prev_block0) > 0) {
        block0_list[[len(block0_list)+1]] <- prev_block0
        prev_block0 <- numeric(0)
      }
    }

    
    if(blocks[i] > 0)
      prev_block <- c(prev_block, i)
    else if(len(prev_block) > 0 ){
      
      #       if(len(prev_block) > w1)
      pks <- c(pks, prev_block[which(ma_peak[prev_block] == max(ma_peak[prev_block]))])
      
      prev_block <- numeric(0)
    }
    
    if(print_output) catProgress(i, len(blocks))
  }
  
  
  if(check_mult_peaks_above_zero) {
    
    if(print_output) cat("analysing blocks above 0...\n")
    
    for(i in 1:len(block0_list)) {
      bl0 <- block0_list[[i]]
      pks_in_bl0 <- pks[(pks >= bl0[1]) & (pks <= bl0[len(bl0)])]
      
      if(len(pks_in_bl0) > 0) {
        max_peak <- pks_in_bl0[which.max(x_g[pks_in_bl0])]
        pks <- c(pks[pks < bl0[1]], max_peak, pks[pks > bl0[len(bl0)]])
      }
      
      if(print_output) catProgress(i, len(block0_list))
    }
  }
  
  if(amend_peaks_under_0) {
    blocks1 <- abs(1-blocks0)
    pks_i <- numeric(len(blocks1))
    pks_i[pks] <- 1
    
    pks_under_0 <- which((blocks1 == 1) & (pks_i == 1))
    
    if(len(pks_under_0) > 1)
      pks <- pks[-match(pks_under_0, pks)]
  }
  
  
  
  if(interp)
    pksmat <- getInterpPeaks(x_g, pks)
  else
    pksmat <- cbind(pks, x_g[pks])
  
  if(output_all)
    return(list(filt_signal=x_g, clipped_signal=y,
                ma_peak=ma_peak, ma_beat=ma_beat, thr1=thr1,
                blocks=blocks, block0_list=block0_list,
                peaks=pksmat[,1], peakvals=pksmat[,2],
                RR=diff(1000*pksmat[,1]/bf_fs_actual) ))
  else
    return(pksmat[,1])
  
}




##############################################################################################
##############################################################################################
##############################################################################################




detectRROutliers <- function(rr, mean_angle=0.33, N=2, num_angles=100,
                             rr_thresh_lower=300, rr_thresh_upper=1800,
                             ang_thresh_lower=0, ang_thresh_upper=0.5,
                             win_len=10, win_oult_thresh=4)
{
  n       <- length(rr)
  badlist <- numeric(n)
  angles  <- numeric(n)
  rr_diff <- abs(diff(rr))
  
  
  ## initial outlier check
  badlist[(rr < rr_thresh_lower) | (rr > rr_thresh_upper)] <- 1
  
  ## outlier check using angles
  for(i in 2:(n-1)){
    
    ## detect local max or min
    if(((rr[i] > rr[i-1]) & (rr[i] > rr[i+1])) | ((rr[i] < rr[i-1]) & (rr[i] < rr[i+1]))){
      
      angle <- rr_diff[i-1]/rr[i] + rr_diff[i]/rr[i]
      
      ## test angle against threshold
      if((angle > ang_thresh_lower) & (angle < ang_thresh_upper)){
        mean_angle <- (mean_angle*N - mean_angle + angle)/N
        
        if(N < num_angles)
          N <- N+1
      }
      
      ## outlier classification
      if(angle > mean_angle*3)
        badlist[i] <- 1
      
      
    }
    
    angles[i] <- rr[i+1]
  }
  
  ## fill outliers in noisy areas
#   for(i in 1:(n-win_len+1)){
#     bl_win <- badlist[i:(i+win_len-1)]
#     
#     bl_bad <- (i:(i+win_len-1))[which(bl_win != 0)]
#     if(length(bl_bad) >= win_oult_thresh){
#       badlist[bl_bad[1]:bl_bad[length(bl_bad)]] <- 1
#     }
#   }
  
  
  
  return(cbind(badlist, angles))
}



##############################################################################################
##############################################################################################
##############################################################################################


tdomain_metrics <- function(rr, outliers)
{
  
  n <- length(rr)
  succ_diff <- NULL
  newdiff <- FALSE
  
  for(i in 2:n){
    
    if(outliers[i] == 0)
      if(newdiff)
        newdiff <- FALSE
      else
        succ_diff <- c(succ_diff, abs(rr[i]-rr[i-1]))
    else{
      newdiff <- TRUE
    }
    
  }
  
  sdnn  <- sd(rr[outliers == 0])
  sdsd  <- sd(succ_diff)
  rmssd <- sqrt(sum(succ_diff^2)/length(succ_diff))
  pnn50 <- 100*sum(succ_diff > 50)/length(succ_diff)
  score <- min(100, 20*log(rmssd))
  
  return(list(succ_diff, c(sdnn, sdsd, rmssd, pnn50, score)))
  
}



# tdm <- tdomain_metrics(rr, outliers)


##############################################################################################
##############################################################################################
##############################################################################################






hrv_ERMADT_plot <- function(out)
{
  
  zPlot(cbind((out$filt_signal)^2,
              replace(out$ma_beat, is.na(out$ma_beat), 0),
              replace(out$ma_peak, is.na(out$ma_peak), 0), 
              replace(out$thr1, is.na(out$thr1), 0)),
        ynames=c("Filtered", "MA beat", "MA peak", "threshold"),
        col_f=c("grey", "blue", "mediumseagreen", "orange"),
        points_c=cbind(out$peaks, (out$peakvals)^2))
  
}