source("../useful_funs.r")
source("./ERMADT_funs.r")
source("./amendConfidencePostProcess.r")

## get file names
datlist <- getDataFromDir("D:/HealthQ/Data/24h/2_Renier/", dataE=F, ret="full")
reflist <- list(read.table("D:/HealthQ/Data/24h/2_Renier/Bioharness/2015_10_14-19_02_47/2015_10_14-19_02_47_RR.csv", sep=",", head=T)[,2])

###################################################################################################
## REFORMAT DATA FOR C

# for(i in 1:len(datlist)){
#   dat <- read.table(datlist[[i]], head=T, sep=",")
# #   dat <- dat[,c(1:7, 9:17, 8, 18:26)]
# #   dat[,10:13] <- 10*dat[,10:13]
# #   dat <- cbind(dat[-1,1:7], dat[1:(nrow(dat)-1), 8:26])
#   colnames(dat) <- c("TOKEN", "SUBTOKEN", "ppg_green", "ppg_ambient",  "ppg_red",	 "ppg_infrared",	 "acc_x",	 "acc_y",	 "acc_z",
#                      "voltage",  "amp_green",	 "amp_red",	 "off_green",	 "off_ambient",	 "off_red",	 "off_infrared", 
#                      "led_green",	 "led_red",	 "led_infrared",  "bufferspace",	 
#                      "emb_heartrate",	 "emb_cadence",	 "emb_calories",	 "emb_epoch",	 "emb_vo2",	 "emb_bloodlactate", "emb_snr")
#   write.table2(dat, datlist[[i]], col.names=T)
# }

###################################################################################################

###############################################
##                                           ##
##  RUN HRV ALGORITHM THROUGH FULL 24h DATA  ## 
##                                           ##
###################################################################################################

Fs <- 25
i <- 2


ref <- reflist[[1]]

if(!exists("rr")) {
  rr <- vector("list", 2)
  for(i in 1:2) {
    dat <- read.table(datlist[i], sep=",", head=T)
    ares <- acc_res(dat[,7:9], 16)
    
    dat <- dat[dat[,1] != 0, c(3:9, 11, 13, 15:21)]
    dat <- dat[10:nrow(dat),]
    curr1 <- curr2 <- matrix(0, nr=nrow(dat), nc=1)
    for(j in 1:1){
      if(j < 3){
        Rf <- dat[,8]
        led <- dat[,14]
      }
      else{
        Rf <- dat[,9]
        led <- dat[,j+12]
      }
      
      safe_term <- 0
      if(j == 2){
        safe_term <- 1
        led <- numeric(nrow(dat))
      }
      
      dat_tmp <- dat[,j] 
      isub_tmp <- dat[,9+j]
      curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
      curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e8)  
    }
    rm(dat)
    
    hrv_out <- ER_MA_DT_peaks_C(curr2[,1], filt_t="FIRHAM", clip=200, output=T, interp=T, print_output=T, bf_fs=25)
    rr[[i]] <- cbind(cumsum(hrv_out$RR)/1000, hrv_out$RR)
    # xf1 <- hrv_out$filt_signal
  }
}



## 350, 1600
# rr_list <- list(ref=ref, rr_wrist=rr1, rr_uarm=rr2)

lapply(rr_list, function(x) sum(x < 350))
lapply(rr_list, function(x) sum(x > 1600))


############################################
##                                        ##
##  ANALYSIS OF GREEN vs RED vs INFRARED  ## 
##                                        ##
###################################################################################################


hrv_out <- ER_MA_DT_peaks_C(curr2[,1], filt_t="FIRHAM", clip=200, output=T, interp=F, print_output=T, bf_fs=25)
rr1 <- hrv_out$RR
xf1 <- hrv_out$filt_signal

# hrv_out <- ER_MA_DT_peaks_C(curr2[,3], filt_t="FIRHAM", clip=200, output=T, interp=F, print_output=T, bf_fs=25)
# rr2 <- hrv_out$RR
# xf2 <- hrv_out$filt_signal

hrv_out <- ER_MA_DT_peaks_C(curr2[,4], filt_t="FIRHAM", clip=200, output=T, interp=F, print_output=T, bf_fs=25)
rr3 <- hrv_out$RR
xf3 <- hrv_out$filt_signal

rr1_time <- cumsum(rr1)/1000
rr3_time <- cumsum(rr3)/1000

pind <- 20000:50000
w() ; lplot(rr3_time, rr3, lwd=1, col="red", xlim=range(pind), ylim=c(0, 4000), 
            xlab="time (s)", ylab="RR length (ms)", main="RR intervals (green vs infrared)")
lines(rr1_time, rr1, lwd=1, col="forestgreen")
savePlot("./Renier_sleep_upperarm_RR_green_vs_ir.png", "png")

2
rr1t <- rr1[(rr1_time >= 20000) & (rr1_time <= 50000)]
rr3t <- rr3[(rr3_time >= 20000) & (rr3_time <= 50000)]
rr1t_sd1 <- runningSD(rr1t, 100)
rr1t_sd2 <- runningSD(rr1t, 500)
rr3t_sd1 <- runningSD(rr3t, 100)
rr3t_sd2 <- runningSD(rr3t, 500)

w() ; lplot(rr1t_sd1, col="forestgreen", ylim=range(c(rr1t_sd1, rr3t_sd1)), lwd=2, 
            ylab="SD of RR intervals", main="Running Standard Dev of RRs (window length 100)")
lines(rr3t_sd1, col="red", lwd=2)
savePlot("./Renier_sleep_upperarm_RR_runningSD_100.png", "png")

w() ; lplot(rr1t_sd2, col="forestgreen", ylim=range(c(rr1t_sd2, rr3t_sd2)), lwd=2, 
            ylab="SD of RR intervals", main="Running Standard Dev of RRs (window length 500)")
lines(rr3t_sd2, col="red", lwd=2)
# savePlot("./Renier_sleep_RR_runningSD_500.png", "png")


rr1_no <- rr1t[(rr1t >= sort(rr1)[floor(len(rr1)*0.05)]) & (rr1t <= sort(rr1)[floor(len(rr1)*0.85)])]
rr3_no <- rr3t[(rr3t >= sort(rr3)[floor(len(rr3)*0.05)]) & (rr3t <= sort(rr3)[floor(len(rr3)*0.85)])]
rr1t_no_sd1 <- runningSD(rr1_no, 100)
rr3t_no_sd1 <- runningSD(rr3_no, 100)


w() ; lplot(rr1t_no_sd1, col="forestgreen", ylim=range(c(rr1t_no_sd1, rr3t_no_sd1)), lwd=2, 
            ylab="SD of RR intervals", main="Running Standard Dev of RRs (window length 100, outliers removed)")
lines(rr3t_no_sd1, col="red", lwd=2)
savePlot("./Renier_sleep_upperarm_RR_runningSD_100_no_outliers.png", "png")












#################
##             ##
##  C OUTPUT   ## 
##             ##
###################################################################################################


## read C output
cout <- read.table(paste0("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/physcalc_out_", basename(datlist[i])), head=T, sep=",")
crrmat <- cout[cout[,10] == 1, c(11,13)]
crrtime <- cumsum(crrmat[,1])/1000

## post-process RR confidence metric
crrmat2 <- amendConfidencePostProcess(crrmat[,1], crrmat[,2])
# crrmat2 <- cb(crrmat2[,1], crrmat[,1], crrmat2[,3])


# rr[[2]] <- cbind(crrtime, crrmat[,1], crrmat2[,2])


## C debugging output
cdebug <- read.table(paste0("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/debug_out_", basename(datlist[i])), sep=",")





###############################
##                           ##
##  WRITING OUTPUT TO FILE   ## 
##                           ##
###################################################################################################

out_mat <- cb(crrmat2[,1:2], cout[cout[,10] == 1, 12], crrmat2[,3])
colnames(out_mat) <- c("time", "RRI", "is_RR_invalid", "RR_confidence")

write.table2(out_mat, file = "20151015_Renier_20h_wrist_RRoutput.txt", sep = ", ", col.names = T)
# write.table2(out_mat, file = "20151015_Renier_20h_upperarm_RRoutput.txt", sep = ", ", col.names = T)

###################################
##                               ##
##  SCROLL PLOTTING / ALIGNMENT  ## 
##                               ##
###################################################################################################

if(i == 1) sleep_bounds <- c(18800, 52800)
if(i == 2) sleep_bounds <- c(18800, 52800)

out_list <-  alignToRefRR(ref, crrmat2, XLIMIT = 70000, SLEEP = sleep_bounds, PLOT_T = 2, 
                          SAVE_PLOTS = T, SAVE_PATH = "./results/20151016 Renier 20h/segment")





cat("proportion of windows used =", out_list[[2]][1], "%",
    "\nmean proportion outliers (all) =", out_list[[2]][2], "%",
    "\nmean proportion outliers (ref) =", out_list[[2]][3], "%",
    "\nmean proportion outliers (mojo) =", out_list[[2]][4], "%",
    "\n\nmean abs diff =", round(out_list[[3]][1],2), "\nmean abs diff (weighted) =", round(out_list[[3]][2],2),
    "\nmean abs % diff =", round(out_list[[3]][3],2), "%\nmean abs % diff (weighted) =", round(out_list[[3]][4],2), "%\n")




w() ; lplot(out_mat[,7], lwd=2, col="red")
par(new=T)
lplot(out_mat[,9], ann=F, yaxt="n", xaxt="n")
axis(4)













