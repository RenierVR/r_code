#######################################
####  LOAD RELEVANT PACKAGES       ####
#######################################

library(rhdf5)
source("../useful_funs.r")


options(digits=22)

#######################################
####  READ FAROS REFERENCE FILE     ####
#######################################

library(edfReader)

faros_header <- readEdfHeader("D:/HealthQ/Data/RRI/Tim SNR testing/20170131_TPID0113_F1_Faros/19-50-10.EDF")
faros_out <- readEdfSignals(faros_header)

rr_f_time1 <- as.numeric(as.POSIXct(faros_out$HRV$startTime)) 

faros_rr <- faros_out$HRV$signal[faros_out$HRV$signal != 0]
faros_rr <- cb(as.double(rr_f_time1 + (55) + cumsum(faros_rr)/1000), faros_rr)


#######################################
####  READ HDF5 FILE CONTENTS      ####
#######################################

# path to h5 file
h5_paths <- paste0("D:/HealthQ/Data/RRI/Tim SNR testing/Tim SNR testing-parsed/",
                   c("20170131_215147_65643238363730396136313531356266_FREELIVING.h5",
                     "20170201_014737_65643238363730396136313531356266_FREELIVING.h5",
                     "20170201_054609_65643238363730396136313531356266_FREELIVING.h5"))



# print group names with corresponding dimension
h5_contents <- h5ls(h5_paths[1])
print( h5_contents[h5_contents$group != "/", c(1, 5)] )

# load green data
dat_conv_1 <- h5read(h5_paths[1], "/raw_converted", bit64conversion="double", compoundAsDataFrame=T)$table
dat_conv_2 <- h5read(h5_paths[2], "/raw_converted", bit64conversion="double", compoundAsDataFrame=T)$table
dat_conv_3 <- h5read(h5_paths[3], "/raw_converted", bit64conversion="double", compoundAsDataFrame=T)$table

# load unconverted accelerometer
dat_raw_1 <- h5read(h5_paths[1], "/raw_garmin", bit64conversion="double", compoundAsDataFrame=T)$table
dat_raw_2 <- h5read(h5_paths[2], "/raw_garmin", bit64conversion="double", compoundAsDataFrame=T)$table
dat_raw_3 <- h5read(h5_paths[3], "/raw_garmin", bit64conversion="double", compoundAsDataFrame=T)$table

f_acc_res <- function(x) { sqrt(apply(x^2, 1, sum))  }

dat_all <- cb(c(dat_conv_1$index, dat_conv_2$index, dat_conv_3$index)/10e5,
              c(f_acc_res(dat_conv_1[,2:4]), f_acc_res(dat_conv_2[,2:4]), f_acc_res(dat_conv_3[,2:4])),
              c(dat_conv_1$ppg_green, dat_conv_2$ppg_green, dat_conv_3$ppg_green),
              c(dat_conv_1$led_green, dat_conv_2$led_green, dat_conv_3$led_green))

dat_all_acc <- cb(c(dat_raw_1$acc_x, dat_raw_2$acc_x, dat_raw_3$acc_x),
                  c(dat_raw_1$acc_y, dat_raw_2$acc_y, dat_raw_3$acc_y),
                  c(dat_raw_1$acc_z, dat_raw_2$acc_z, dat_raw_3$acc_z))

rm(dat_conv_1, dat_conv_2, dat_conv_3)
rm(dat_raw_1, dat_raw_2, dat_raw_3)


#######################################
####  SELECT SEGMENTS FOR USE      ####
#######################################


ts_ranges <- list(c(1485899731134, 1485900001760),
                  c(1485900005040, 1485901594281),
                  c(1485907435935, 1485910477006),
                  c(1485910589888, 1485910683240),
                  c(1485910684040, 1485913070377), #*
                  c(1485913207937, 1485913266560),
                  c(1485913267040, 1485913655733),
                  c(1485913658612, 1485913800960),
                  c(1485913802040, 1485915533133),
                  c(1485916467993, 1485917887720),
                  c(1485917888040, 1485921721600), #*
                  c(1485921722040, 1485922070755),
                  c(1485925628040, 1485927542935))


seg_which <- lapply(ts_ranges, function(ts) { which((dat_all[,1] >= ts[1]) & (dat_all[,1] <= ts[2])) })




#################################################
####  CORRECT FOR LED ADJUSTMENTS IN GARMIN  ####
#################################################


for(i in 1:len(seg_which)) {
  
  seg <- dat_all[seg_which[[i]],]
  led_which <- which(diff(seg[,4]) != 0)
  led_new <- integer(nrow(seg))
  
  if(len(led_which) == 0) next
  
  for(j in 1:len(led_which)) {
    ppg_diff <- abs(diff(seg[,3])[led_which[j]:(led_which[j]+10)])
    ppg_diff_max1 <- which.max(ppg_diff)
    ppg_diff_max2 <- ifelse(which.max(ppg_diff[ppg_diff_max1 + c(-1, 1)]) == 1,
                            ppg_diff_max1-1, ppg_diff_max1+1)
    
    led_new[c(ppg_diff_max1 + led_which[j], ppg_diff_max2 + led_which[j])] <- 1:2
  }
  
  dat_all[seg_which[[i]], 3] <- removeCurrentDiscont2(seg[,3], led=led_new)
  
  catProgress(i, len(seg_which))
}



###########################################################
####  CREATE SIMULATED NOISE SIGNALS AND SEND TO FILE  ####
###########################################################


GAUSS_NOISE_INCREMENTS <- as.integer(seq(0, 500, 25))



# for(i in 1:len(seg_which)) {
#   
#   seg_ppg <- dat_all[seg_which[[i]], 3]
#   noise_ppg <- sapply(GAUSS_NOISE_INCREMENTS, function(x) seg_ppg + rnorm(len(seg_ppg), 0, x))
#   
#   seg <- cb(dat_all[seg_which[[i]], c(1, 4)], dat_all_acc[seg_which[[i]], ], noise_ppg)
#   colnames(seg) <- c("TIMESTAMP", "LED_GREEN", "ACC_X", "ACC_Y", "ACC_Z", paste0("PPG_GREEN_", GAUSS_NOISE_INCREMENTS))
#   
#   write.table2(seg, paste0("D:/HealthQ/Data/RRI/Tim SNR testing/PPG_NOISE_SIMULATIONS/segment", i, ".csv"), col.names = T)
#   catProgress(i, len(seg_which))
# }



##############################################################
####  READ SNR PYTHON OUTPUT FOR SIMULATED NOISE SIGNALS  ####
##############################################################

datlist <- getDataFromDir("D:/HealthQ/Data/RRI/Tim SNR testing/PPG_NOISE_SIMULATIONS/", dataE=F)
segments_raw <- datlist[-grep("_SNR.csv", datlist)]
segments_snr <- datlist[grep("_SNR.csv", datlist)]

seg_snr_all <- matrix(0, nr=13, nc=len(GAUSS_NOISE_INCREMENTS))

for(i in 1:len(segments_snr)) {
  
  seg_snr <- t(read.table(segments_snr[i], sep=","))
  # snr_sm <- apply(seg_snr[,-1], 2, movingAve, 0.25*nrow(seg_snr), 2)
  snr_sm <- seg_snr[,-1]
  
  seg_snr_all[i,] <- apply(snr_sm, 2, mean, na.rm=T)
  
}


## Plot showing example of PPG with simulated noise
# 
# pind <- 500:800
# dat_p <- seg_raw[pind, c(26, 16, 10, 6)]
# w() ; lplot_g(pind, dat_p[,1], col="yellow",
#               xlab="Sample index", ylab="Green PPG",
#               main="Example of PPG with simulated noise")
# lines(pind, dat_p[,2], col="orange", lwd=2)
# lines(pind, dat_p[,3], col="red", lwd=2)
# lines(pind, dat_p[,4], col="blue", lwd=2)
# legend("topright", lwd=2, col=c("blue", "red", "orange", "yellow"), bg="white",
#        leg=c("original", "Gaussian noise, SD=100", "Gaussian noise, SD=250", "Gaussian noise, SD=500"))


## Plot showing example of SNR for session
# w()
# cols <- heat.colors(20)
# lplot_g(seg_snr[, 1], snr_sm[, 1], ylim=c(0, 80), xlim=c(16000,27000),
#         xlab="Sample index", ylab="Raw SNR",
#         main="SNR over noise levels for a segment")
# for(j in 2:21)
#   lines(seg_snr[, 1], snr_sm[, j], col=cols[j-1])
# legend("topright", bg="white", lwd=1, col=c("blue", cols),
#        leg=c("original", paste0("SD = ", GAUSS_NOISE_INCREMENTS[-1])), cex=0.75)





##############################################################
####  CALCULATE RR ACCURACY FOR SIMULATED PPG SIGNALS     ####
##############################################################

source("ERMADT_funs.r")


##  Calculate RR values on all simulated noise PPG signals
###########################################################

rr_list_all <- vector("list", len(segments_raw))
names(rr_list_all) <- basename(segments_raw)
for(i in 1:len(segments_raw)) {

  seg_raw <- read.table(segments_raw[i], head=T, sep=",")

  rr_list_seg <- vector("list", len(GAUSS_NOISE_INCREMENTS))
  names(rr_list_seg) <- paste0("NOISE_", GAUSS_NOISE_INCREMENTS)
  for(j in 1:(len(GAUSS_NOISE_INCREMENTS))) {
    out_rr <- ER_MA_DT_peaks_C(seg_raw[,j+5], filt_t="FIRHAM", clip=4*25, output=T, interp=T, bf_fs=25,
                               check_mult_peaks_above_zero=T, post_filt_running_mean=F, amend_peaks_under_0=T)
    rr_list_seg[[j]] <- out_rr$RR
  }
  
  
  rr_list_all[[i]] <- list("TIME_SEGMENT_START" = as.double(seg_raw[1,1]), 
                           "RR_LIST" = rr_list_seg)
    
  catProgress(i, len(segments_raw))
}

# Test all RR output relatively similar cum sum
# lapply(rr_list_all, function(x) range(as.numeric(round(unlist(lapply(x[[2]], function(y) { sum(y)/1000 })), 2))))


## Align all RR streams to reference
#####################################

source("find_min_mad.r")

min_mads_list <- vector("list", len(rr_list_all))
for(i in 1:len(rr_list_all)) {
  
  rr_g_time1 <- rr_list_all[[i]]$TIME_SEGMENT_START/1000
  rr_g_list <- rr_list_all[[i]]$RR_LIST
  
  min_mads_seg <- numeric(len(rr_g_list))
  for(j in 1:len(rr_g_list)) {
    
    rr_g_all <- rr_g_list[[j]]
    rr_g <- cb(cumsum(rr_g_all)/1000, rr_g_all)
    
    rr_f_all <- faros_rr[(faros_rr[,1] > rr_g_time1) & (faros_rr[,1] < (rr_g_time1 + tail(rr_g)[1,1] + 10)), 2]
    rr_f <- cb(cumsum(rr_f_all)/1000, rr_f_all)
    
    min_mad_lag <- find_min_mad(rr_g_all, rr_f_all, lags1=seq(-15, 5, 0.1))$min_lag
    min_mads_seg[j] <- min_mad_lag
    
  }
  
  min_mads_list[[i]] <- min_mads_seg
  
  catProgress(i, len(rr_list_all))
  
}

# save(min_mads_list, file="./tim_snr_min_mads_list.Rdata")




## Calculate accuracy of aligned RRI streams
###############################################

source("calc_aligned_RR_accuracy_wrt_ref.r")

MAD_list_all <- vector("list", len(rr_list_all))
for(i in 1:len(rr_list_all)) {
  
  rr_g_time1 <- rr_list_all[[i]]$TIME_SEGMENT_START/1000
  rr_g_list <- rr_list_all[[i]]$RR_LIST
  
  MAD_list_seg <- matrix(0, nr=len(rr_g_list), nc=2)
  for(j in 1:len(rr_g_list)) {
    
    rr_g_all <- rr_g_list[[j]]
    rr_g <- cb(cumsum(rr_g_all)/1000, rr_g_all)
    
    rr_f_all <- faros_rr[(faros_rr[,1] > rr_g_time1) & (faros_rr[,1] < (rr_g_time1 + tail(rr_g)[1,1] + 10)), 2]
    rr_f <- cb(cumsum(rr_f_all)/1000, rr_f_all)
    
    # Align to reference
    rr_g[,1] <- rr_g[,1] + min_mads_list[[i]][j]
    
    # Calculate accuracy
    rr_acc <- calc_aligned_RR_accuracy_wrt_ref(rr_g, rr_f)
    MAD_list_seg[j,] <- c(len(rr_acc$absolute_differences), rr_acc$mean_abs_diff)
    
  }
  
  MAD_list_all[[i]] <- MAD_list_seg
  
  catProgress(i, len(rr_list_all))
  
}


# rr_g1 <- rr_g
# rrg_out1 <- detectRROutliers(rr_g1[,2])[,1]
# rr_g2 <- rr_g
# rrg_out2 <- detectRROutliers(rr_g2[,2])[,1]
# 
# w() ; lplot_g(rr_f, lwd=3, col="red", xlim=c(50, 180), ylim=c(600, 1000),
#               xlab="time (s)", ylab="RR interval (ms)",
#               main="RRI against reference (original and noisy)")
# lines(rr_g1, lwd=2, col="blue")
# abline(v=rr_g1[rrg_out1==1,1], col="blue", lty=2)
# lines(rr_g2, lwd=2, col="seagreen")
# abline(v=rr_g2[rrg_out2==1,1], col="seagreen", lty=2)
# legend("topleft", lwd=2, lty=c(1,1,2,1,2), bg="white", cex=0.8,
#        col=c("red", rep("blue",2), rep("seagreen", 2)),
#        leg=c("Faros reference", 
#              "Garmin (no noise)", "Garmin (no noise) detected outliers", 
#              "Garmin (SD = 350)", "Garmin (SD = 350) detected outliers"))



## Count outliers across all data
##################################


rrf_outliers_all <- numeric(len(rr_list_all))
rrg_outliers_all <- matrix(0, nr=len(rr_list_all), nc=len(rr_list_all[[1]]$RR_LIST))
for(i in 1:len(rr_list_all)) {
  
  rr_g_list <- rr_list_all[[i]]$RR_LIST
  for(j in 1:len(rr_g_list)) {
    
    rr_g_all <- rr_g_list[[j]]
    rr_f_all <- faros_rr[(faros_rr[,1] > rr_g_time1) & (faros_rr[,1] < (rr_g_time1 + tail(rr_g)[1,1] + 10)), 2]
    
    rrg_outliers_all[i,j] <- sum(detectRROutliers(rr_g_all)[,1])
    
    if(j == 1)
      rrf_outliers_all[i] <- sum(detectRROutliers(rr_f_all)[,1])
  }
  
  catProgress(i, len(rr_list_all))
  
}



## Summary statistics
######################

# Remap list for segment concatenation

MAD_reshaped <- vector("list", nrow(MAD_list_all[[1]]))
for(i in 1:len(MAD_reshaped))
  MAD_reshaped[[i]] <- matrix(unlist(lapply(MAD_list_all, function(x) x[i,])), nc=2, byrow=T)

# Calculate MAD over each session (concatenated segments) for each noise level
mean_mads <- unlist(lapply(MAD_reshaped, function(x) sum(x[,2] * x[,1]/sum(x[,1]))))

# Calculate mean SNR over each session (concatenated segments) for each noise level
mean_snr <- apply(seg_snr_all, 2, mean)

# Calculate outliers for each noise level
sum_outliers <- colSums(rrg_outliers_all)


# Plot results
w(14,10)
plot_g(mean_snr, mean_mads, col="red",
       xlab = "SNR (session average)", ylab = "RR mean absolute error in ms",
       main="Simulated RR accuracy as a function of embedded SNR")
# lines(mean_snr, mean_mads, col="red")

text(mean_snr, mean_mads,labels=GAUSS_NOISE_INCREMENTS, 
     pos=c(rep(3, 4), rep(2, 3), rep(4, 14)), col="seagreen", cex=0.75)
legend("topright", lwd=2, col="seagreen", bg="white",
       leg=c("Gaussian noise standard deviation"), cex=0.75)



w() ; par(mfrow=c(1,2), mar=c(4,4,3,1))
plot_g(GAUSS_NOISE_INCREMENTS, mean_mads, col="red", cex=1.3,
       xlab = "Gaussian noise SD", ylab = "RR mean absolute error in ms",
       main="Simulated RR accuracy as a function of noise standard deviation")

plot_g(GAUSS_NOISE_INCREMENTS, mean_snr, col="red", cex=1.3,
       xlab = "Gaussian noise SD", ylab = "SNR (session average)",
       main="Simulated SNR as a function of noise standard deviation")



w(14,10)
plot_g(mean_snr, sum_outliers, col="red",
       xlab = "SNR (session average)", ylab = "Outlier count",
       main="Amount of RR outliers as a function of embedded SNR")