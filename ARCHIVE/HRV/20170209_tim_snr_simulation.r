#######################################
####  LOAD RELEVANT PACKAGES       ####
#######################################

library(rhdf5)
source("../useful_funs.r")


options(digits=22)

#######################################
####  READ FAROS REFERENCE FILE     ####
#######################################

library(edfReader)

faros_header <- readEdfHeader("D:/HealthQ/Data/RRI/Tim SNR testing/20170131_TPID0113_F1_Faros/19-50-10.EDF")
faros_out <- readEdfSignals(faros_header)

rr_f_time1 <- as.numeric(as.POSIXct(faros_out$HRV$startTime)) 

faros_rr <- faros_out$HRV$signal[faros_out$HRV$signal != 0]
faros_rr <- cb(as.double(rr_f_time1 + (55) + cumsum(faros_rr)/1000), faros_rr)


# rr_g_time1 <- rr_list[[1]]$TIME_SEGMENT_START/1000
# 
# aa <- faros_rr[faros_rr[,1] > (rr_g_time1), 2]
# bb <- rr_list[[1]]$RR[,1]
# 
# rr_f <- cb(cumsum(aa)/1000, aa)
# rr_g <- cb(cumsum(bb)/1000, bb)
# 
# scrollPlot(winsize=300, overlap=200, start=1, n=43100, plotfun=function(xi){
# 
#   which_f <- which((rr_f[,1] >= xi[1]) & (rr_f[,1] <= tail(xi)[1]))
#   which_g <- which((rr_g[,1] >= xi[1]) & (rr_g[,1] <= tail(xi)[1]))
# 
#   lplot(rr_f[which_f,1], rr_f[which_f,2], lwd=2, ylim=c(700, 900))
#   lines(rr_g[which_g,1], rr_g[which_g,2], lwd=2, col="blue")
# })
# 


#######################################
####  READ HDF5 FILE CONTENTS      ####
#######################################

# path to h5 file
h5_paths <- paste0("D:/HealthQ/Data/RRI/Tim SNR testing/Tim SNR testing-parsed/",
                   c("20170131_215147_65643238363730396136313531356266_FREELIVING.h5",
                     "20170201_014737_65643238363730396136313531356266_FREELIVING.h5",
                     "20170201_054609_65643238363730396136313531356266_FREELIVING.h5"))



# print group names with corresponding dimension
h5_contents <- h5ls(h5_paths[1])
print( h5_contents[h5_contents$group != "/", c(1, 5)] )

# load green data
dat_conv_1 <- h5read(h5_paths[1], "/raw_converted", bit64conversion="double", compoundAsDataFrame=T)$table
dat_conv_2 <- h5read(h5_paths[2], "/raw_converted", bit64conversion="double", compoundAsDataFrame=T)$table
dat_conv_3 <- h5read(h5_paths[3], "/raw_converted", bit64conversion="double", compoundAsDataFrame=T)$table

f_acc_res <- function(x) { sqrt(apply(x^2, 1, sum))  }

dat_all <- cb(c(dat_conv_1$index, dat_conv_2$index, dat_conv_3$index)/10e5,
              c(f_acc_res(dat_conv_1[,2:4]), f_acc_res(dat_conv_2[,2:4]), f_acc_res(dat_conv_3[,2:4])),
              c(dat_conv_1$ppg_green, dat_conv_2$ppg_green, dat_conv_3$ppg_green),
              c(dat_conv_1$led_green, dat_conv_2$led_green, dat_conv_3$led_green))

rm(dat_conv_1, dat_conv_2, dat_conv_3)


#######################################
####  SELECT SEGMENTS FOR USE      ####
#######################################


# scrollPlot(winsize=100000, overlap = 100000-1, n=716628, plotfun=function(xi){
#   
#   lplot_g(dat_all[xi, 1:2], lwd=1, ylim=c(0, 1.5), ann_ax=F)
#   par(new = T) ; lplot(dat_all[xi, c(1,3)], 
#                        ylab="PPG 525nm converted", xlab="time stamp",
#                        main=paste(dat_all[c(xi[1], tail(xi,1)), 1], collapse=" | "))
#   
# })




ts_ranges <- list(c(1485899731134, 1485900001760),
                  c(1485900005040, 1485901594281),
                  c(1485907435935, 1485910477006),
                  c(1485910589888, 1485910683240),
                  c(1485910684040, 1485913070377), #*
                  c(1485913207937, 1485913266560),
                  c(1485913267040, 1485913655733),
                  c(1485913658612, 1485913800960),
                  c(1485913802040, 1485915533133),
                  c(1485916467993, 1485917887720),
                  c(1485917888040, 1485921721600), #*
                  c(1485921722040, 1485922070755),
                  c(1485925628040, 1485927542935))


seg_which <- lapply(ts_ranges, function(ts) { which((dat_all[,1] >= ts[1]) & (dat_all[,1] <= ts[2])) })


seg <- dat_all[seg_which[[13]],]

################################
# table(diff(seg[,1]))
# which(diff(seg[,1]) == 600)
# seg[1859:1860,1]
################################

# wsize <- 10000
# scrollPlot(winsize=wsize, overlap = wsize*0.75, n=nrow(seg), plotfun=function(xi){
#   
#   lplot_g(seg[xi, 1:2], lwd=1, ylim=c(0, 1.5), ann_ax=F)
#   par(new = T) ; lplot(seg[xi, c(1,3)], 
#                        ylab="PPG 525nm converted", xlab="time stamp",
#                        main=paste(seg[c(xi[1], tail(xi,1)), 1], collapse=" | "))
#   
# })
# 
# 
# 
# seg <- dat_all[seg_which[[3]],]
# which(diff(seg[,4]) != 0)




#################################################
####  CORRECT FOR LED ADJUSTMENTS IN GARMIN  ####
#################################################


for(i in 1:len(seg_which)) {
  
  seg <- dat_all[seg_which[[i]],]
  led_which <- which(diff(seg[,4]) != 0)
  led_new <- integer(nrow(seg))
  
  if(len(led_which) == 0) next
  
  for(j in 1:len(led_which)) {
    ppg_diff <- abs(diff(seg[,3])[led_which[j]:(led_which[j]+10)])
    ppg_diff_max1 <- which.max(ppg_diff)
    ppg_diff_max2 <- ifelse(which.max(ppg_diff[ppg_diff_max1 + c(-1, 1)]) == 1,
                            ppg_diff_max1-1, ppg_diff_max1+1)
    
    led_new[c(ppg_diff_max1 + led_which[j], ppg_diff_max2 + led_which[j])] <- 1:2
  }
  
  dat_all[seg_which[[i]], 3] <- removeCurrentDiscont2(seg[,3], led=led_new)
  
  catProgress(i, len(seg_which))
}




#################################################
####  CALCULATE RR VALUES FOR EACH SEGMENT   ####
#################################################

source("ERMADT_funs.r")


rr_list <- vector("list", len(seg_which))
for(i in 1:len(seg_which)) {
  
  seg <- dat_all[seg_which[[i]],]
  
  out_rr <- ER_MA_DT_peaks_C(seg[,3], filt_t="FIRHAM", clip=4*25, output=T, interp=T, bf_fs=25,
                             check_mult_peaks_above_zero=T, post_filt_running_mean=F)
  out_rrv <- detectRROutliers(out_rr$RR)
  
  rr_list[[i]] <- list("TIME_SEGMENT_START" = as.double(seg[1,1]), 
                       "RR" = cb(out_rr$RR, out_rrv[,1]))
  
  catProgress(i, len(seg_which))
}



#################################################
####  CALCULATE RR ALIGNMENT WRT REFERENCE   ####
#################################################

source("find_min_mad.r")


min_mad_vec <- rep(NA, len(rr_list))
min_mad_vec[1:13] <- c(-1.64, -2.55, -4.975, -5.12, -6.405,
                       -6.315, -6.47, -6.69, -7.02, -8.045,
                       -8.25, -8.33, -11.77)



for(i in 1:len(rr_list)) {
  
  rr_g_time1 <- rr_list[[i]]$TIME_SEGMENT_START/1000
  rr_g_all <- rr_list[[i]]$RR[,1]
  rr_g <- cb(cumsum(rr_g_all)/1000, rr_g_all)
  
  rr_f_all <- faros_rr[(faros_rr[,1] > rr_g_time1) & (faros_rr[,1] < (rr_g_time1 + tail(rr_g)[1,1] + 10)), 2]
  rr_f <- cb(cumsum(rr_f_all)/1000, rr_f_all)
  
  # if(is.na(min_mad_vec[i])) {
  #   min_mad_lag <- find_min_mad(rr_g_all, rr_f_all, lags1=seq(-15, 0, 0.1))$min_lag
  #   min_mad_vec[i] <- min_mad_lag
  # }
  # else
    min_mad_lag <- min_mad_vec[i]
  
  rr_g[,1] <- rr_g[,1] + min_mad_lag
  
  
  scrollPlot(winsize=300, overlap=200, n=min(tail(rr_f, 1)[1,1], tail(rr_g, 1)[1,1]), plotfun=function(xi){
    
    which_f <- which((rr_f[,1] >= xi[1]) & (rr_f[,1] <= tail(xi)[1]))
    which_g <- which((rr_g[,1] >= xi[1]) & (rr_g[,1] <= tail(xi)[1]))
    
    lplot(rr_f[which_f,1], rr_f[which_f,2], lwd=2)#, ylim=c(700, 900))
    lines(rr_g[which_g,1], rr_g[which_g,2], lwd=2, col="blue")
    title(main = paste("RR list element", i))
  })
  
  
  
}



#################################################
####  CALCULATE RR ALIGNMENT WRT REFERENCE   ####
#################################################


source("calc_aligned_RR_accuracy_wrt_ref.r")


min_mad_vec[1:13] <- c(-1.64, -2.55, -4.975, -5.12, -6.405,
                       -6.315, -6.47, -6.69, -7.02, -8.045,
                       -8.25, -8.33, -11.77)


MAD_list <- vector("list", len(rr_list))
for(i in 1:len(rr_list)) {
  
  rr_g_time1 <- rr_list[[i]]$TIME_SEGMENT_START/1000
  rr_g_all <- rr_list[[i]]$RR[,1]
  rr_g <- cb(cumsum(rr_g_all)/1000, rr_g_all)
  
  rr_f_all <- faros_rr[(faros_rr[,1] > rr_g_time1) & (faros_rr[,1] < (rr_g_time1 + tail(rr_g)[1,1] + 10)), 2]
  rr_f <- cb(cumsum(rr_f_all)/1000, rr_f_all)
  
  ## Align Garmin to Faros
  min_mad_lag <- min_mad_vec[i]
  rr_g[,1] <- rr_g[,1] + min_mad_lag
  
  ## Calculate accuracy
  MAD_list[[i]] <- calc_aligned_RR_accuracy_wrt_ref(rr_g, rr_f)
  
}
  
MAD_out_all <- cb(unlist(lapply(MAD_list, function(x) len(x[[1]]))), unlist(lapply(MAD_list, function(x) x[[2]])))
MAD_all <- sum(MAD_out_all[,1]/sum(MAD_out_all[,1]) * MAD_out_all[,2])





