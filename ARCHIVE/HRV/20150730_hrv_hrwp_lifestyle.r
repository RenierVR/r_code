source("../useful_funs.r")
source("./ERMADT_funs.r")
source("./amendConfidencePostProcess.r")
source("./alignToRefRR_v2.r")



###################################################################################################
#########################      201507 HR WHITE PAPER - HRV ANALYSIS      ##########################
###################################################################################################

# ## PROTOCOL:
# jump up and down 3 times for alignment
# lie down (3 min)
# sitting (3 min)
# sorting (2 min)
# rest (1 min)
# typing (2 min)
# rest (2 min)
# jump up and down 3 times for alignment




###################################
## get HRWP lifestyle data names ##
###################################


fpath <- "D:/HealthQ/Data/201507_HR_WP/ALL/LIFESTYLE/"
hrwp_lifestyle <- dir(fpath)


##########################
## perform HRV analyses ##
##########################


Fs <- 50
i_range <- 1:len(hrwp_lifestyle)


for(i in i_range) {
  
  #################
  ## manage data ##
  #################
  
  datsets <- getDataFromDir(paste0(fpath, hrwp_lifestyle[i], "/"), dataE=F, ret="full")
  
  dat_mojo <- datsets[grep("HRWP", datsets)]
  dat_rr <- datsets[grep("_RR", datsets)]
  
  bh_rr <- read.table(dat_rr, head=T, sep=",")[,2]
  
  
  ######################################
  ## read data and convert to current ##
  ######################################
  
  m <- length(dat_mojo)
  hrv_list <- vector("list", m)
  rr_list <- vector("list", m)
  for(j in 1:m) {
    
    dat <- read.table(dat_mojo[j], sep=",", head=T)
    dat <- dat[, c(3:9, 11, 13, 15:21)]
    
    dat <- dat[dat[,1] != 0,]
    dat <- dat[10:nrow(dat),]
    
    grncur <- convertToCurrent3(dat[,1], dat[,8], dat[,10])
    blncur <- convertToCurrent3(dat[,2], dat[,8], dat[,11])
    
    grncur <- removeCurrentDiscont2(grncur, dat[,8], dat[,10], dat[,14])
    
    
    ################################################
    ## run HRV algorithm and extract RR intervals ##
    ################################################
    
    hrv_list[[j]] <- ER_MA_DT_peaks_C(grncur, filt_t="FIRHAM", clip=4*Fs, output=T, interp=T, bf_fs=Fs,
                                      check_mult_peaks_above_zero=T, post_filt_running_mean=F)
    rr_list[[j]] <- hrv_list[[j]]$RR
  }
    
  
  #######################
  ## exploratory shite ##
  #######################
  
#   min_len <- min(c(len(bh_rr), unlist(lapply(rr_list, len))))
#   xx <- cbind(bh_rr[1:min_len],
#               matrix(unlist(lapply(rr_list, function(x) x[1:min_len])), nr=min_len))
#   
#   
#   zPlot(xx, ynames=c("ECG", paste0("mojo", 1:(ncol(xx)-1))),
#         col_f=c("red", "blue", "mediumseagreen", "purple"))
  
  
}


# read data into R
dat <- read.table("/.../", sep=",", head=T)

# assign arrays for ppg_signal, and AGC adjustments
ppg_green <- dat$ppg_green
led_green <- dat$led_green
rf_green <- dat$amp_green

# Remove discontinuities in signal due to AGC adjustments
green_cur <- removeCurrentDiscont2(ppg_green, rf=rf_green, isub=rep(0, nrow(dat)), led=led_green)

# Run post-processing RRI algorithm
rr_out <- ER_MA_DT_peaks_C(green_cur, filt_t="FIRHAM", clip=100, output=T, interp=T, bf_fs=25,
                           check_mult_peaks_above_zero=T, post_filt_running_mean=F)


###################################################################################################
###################################################################################################


######################
## analyse C output ##
######################

datlist <- getDataFromDir("D:/HealthQ/Data/HRV/201507 HRWP Lifestyle/", head=T)


cPt <- 0
for(i in 1:len(datlist)) {
  
  dat <- datlist[[i]]
  dat <- dat[10:nrow(dat),]
  
  grncur <- convertToCurrent3(dat[,1], dat[,8], dat[,10])
  grncur <- removeCurrentDiscont2(grncur, dat[,8], dat[,10], dat[,14])
  
  acc <- acc_res(dat[,6:7], 16)
  
  cout <- read.table(paste0("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/physcalc_out_", names(datlist)[i], ".csv"), sep=",", head=T)
  crrmat <- cout[cout[,10] == 1, c(11,13)]
  crrconf <- cout[-c(1:9),13]
  
  # zPlot(cbind(grncur, crrconf, acc), col_f=c("mediumseagreen", "red", "purple"), par_new=T)
  
  #####################################################################
  
  w(18,10) ; par(mar=c(4,4,1,4), mfrow=c(2,1))
  
  lplot(grncur, lwd=2, col="mediumseagreen", xlab="index", ylab="green LED")
  par(new=T)
  plot(acc, ann=F, yaxt="n", xaxt="n", type="n")
  lines(crrconf, col="red")
  lines(acc, lwd=2, col="purple")
  axis(4)
  
  
  crrmat2 <- amendConfidencePostProcess(crrmat[,1], crrmat[,2])
  
  
  lplot(cumsum(crrmat[,1])/1000, crrmat[,1], lwd=2, col="forestgreen",
        xlab="time (s)", ylab="RR (ms)", ylim=c(500,2000))
  par(new=T)
  lplot(cumsum(crrmat[,1])/1000, crrmat2[,2], lwd=2, col="red", ann=F, yaxt="n", xaxt="n")
  axis(4)
  
  
  savePlot(paste0("./results/", names(datlist)[i], "_RR_ACC_plot_2.png"), "png")
  graphics.off()
  
  cPt <- catProgress(i, len(datlist), 0, cPt)
}



i <- 1
cdb <- read.table(paste0("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/debug_out_", names(datlist)[i], ".csv"), sep=",")

# zPlot(cb(cdb[,7], 30))
# zPlot(cb(cdb[,8], 0.001))
# zPlot(cb(cdb[,9], 0.05))

conf <- (cdb[,7] > 30) & (cdb[,8] < 0.001) & (cdb[,9] < 0.05)

conf <- cdb[,1]
conf[cdb[,7] == 1] <- 0

###################################################################################################
###################################################################################################




##########################
## BATCH ACCURACY STATS ##
##########################


Fs <- 50
i_range <- 1:len(hrwp_lifestyle)

datlist <- getDataFromDir("D:/HealthQ/Data/HRV/201507 HRWP Lifestyle/", head=T)
reflist <- getDataFromDir("D:/HealthQ/Data/HRV/201507 HRWP Lifestyle/reference/", dataE = F, ret = "full")
reflist_rr <- reflist[grep("RR", reflist)]
reflist_acc <- reflist[grep("Acc", reflist)]

reflist_rr <- lapply(reflist_rr, function(x) read.table(x, head = T, sep = ",")[,2])
names(reflist_rr) <- unlist(strsplit(basename(reflist[grep("RR", reflist)]), ".csv"))




##########################################################################

i_use <- (1:len(datlist))[-c(1:5, 44:47)]
align_factors <- c(rep(0, 5), -1135, -1150, -1125, -1150,
                   -338.5, -370, -339.1, -95.2, -95.2, -98.2, -92.8, -860, -890, -860, -880,
                   -710, -730, -720, -730, -11.3, -11.9, -7.2, -60.5, -62, -53.4, -53,
                   -56.4, -85.4, -55.3, -78.2, -39.1, -39.1, -33.3, -33.6,
                   -24.5, -24.7, -31.8, -18.6, rep(0, 4), -580, -600, -590, -590,
                   -189.5, -180, -189.5, -181.9, -270, -300, -270, -300,
                   -170.3, -169.6, -159.3, -159.7, -11.3, -11.7, -2.9, -1.8)

##########################################################################




# opt_lag <- numeric(len(datlist))
cPt <- 0
summ_list <- conf_props <- NULL
nvec <- NULL
# w()
for(i in i_use) {
  
  
  reflim <- 0
  
  
  ## get data
  dat <- datlist[[i]]
  dat <- dat[10:nrow(dat),]
  
  ## get reference
  id <- paste(strsplit(names(datlist)[i], "_")[[1]][1:3], collapse = "_")
  ref_rr <- reflist_rr[[grep(id, names(reflist_rr))]]
  
  nvec <- rbind(nvec, c(id, names(datlist)[i]))
  

  ## get C-libs output
  cout <- read.table(paste0("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/physcalc_out_", names(datlist)[i], ".csv"), sep=",", head=T)
  crrmat <- cout[cout[,11] == 1, c(12,14)]
  crrconf <- cout[-c(1:9),14]
  
  crrmat2 <- amendConfidencePostProcess(crrmat[,1], crrmat[,2])
  
  
  ## analyse RR interval data
  ref_time <- cumsum(ref_rr)/1000
  ref_time <- ref_time + align_factors[i]
  
  ## truncate negative time from reference
  ref_start <- which(ref_time >= 0)[1]
  ref_time <- ref_time[ref_start:len(ref_time)]
  ref_rr <- ref_rr[ref_start:len(ref_rr)]
  
  ## equalise length of data streams (roughly)
  t_stop <- min(tail(ref_time, 1), crrmat2[nrow(crrmat2),1])
  which_rr <- which(crrmat2[,1] <= t_stop)
  crrmat2 <- crrmat2[which_rr,]
  which_ref <- which(ref_time <= t_stop)
  ref_time <- ref_time[which_ref]
  ref_rr <- ref_rr[which_ref]
  
  
  ## plot full output
  w()
  plot(ref_time, ref_rr, type = "n", ylim = c(0, 2000),
        main = paste0(names(datlist)[i], "\nlag = ", align_factors[i]))
  abline(v = crrmat2[crrmat2[,3] == 1, 1], col = "lightblue", lwd = 2)
  lines(ref_time, ref_rr, col = "red", lwd = 1)
  lines(crrmat2[,1:2], col = "forestgreen", lwd = 1)
  savePlot(paste0("results/", i, "_", id, "_optlag.png"), "png")
  graphics.off()
  
  
  ## segmented alignment and stats
  align_out <- alignToRefRR(ref_rr, crrmat2, winsize = 200, overlap = 190,
                            PLOT_TYPE = 2, 
                            LAG_RANGE_NORMAL = 5, LAG_RANGE_PRECISE = 5,
                            SAVE_PATH = paste0("./results/HRWP segments/", paste0(i, "_", names(datlist)[i], "_segment")))
  
  summ_list <- c(summ_list, list(align_out[[3]]$SUMMARY_STATS))
  conf_props <- c(conf_props, align_out[[2]]$CONFIDENCE_STATS[1])

  
  cPt <- catProgress(i, len(datlist), 0, cPt)
  
}  
graphics.off()
  

##########################################################################

nd <- names(datlist)[i_use]


summ_list <- lapply(summ_list, function(x) x[1:2,])

summ_list_all <- matrix(unlist(lapply(summ_list, function(x) x[1,])), nc = 2, byrow = T)
summ_list_conf <- matrix(unlist(lapply(summ_list, function(x) x[2,])), nc = 2, byrow = T)

which_wrist <- grep("Wrist", nd)
which_wrist_AGC <- which_wrist[grep("AGC", nd[which_wrist])]
which_wrist_NoAGC <- which_wrist[-grep("AGC", nd[which_wrist])]

which_upperarm <- grep("UpperArm", nd)
which_uparm_AGC <- which_upperarm[grep("AGC", nd[which_upperarm])]
which_uparm_NoAGC <- which_upperarm[-grep("AGC", nd[which_upperarm])]



colM_all <- round(matrix(c(colMeans(summ_list_all),
                           colMeans(summ_list_all[which_wrist, ]),
                           colMeans(summ_list_all[which_wrist_NoAGC, ]),
                           colMeans(summ_list_all[which_wrist_AGC, ]),
                           colMeans(summ_list_all[which_upperarm, ]),
                           colMeans(summ_list_all[which_uparm_NoAGC, ]),
                           colMeans(summ_list_all[which_uparm_AGC, ])), ncol = 2, byrow = T,
                         dimnames = list(c("all", "wrist", "wrist_noAGC", "wristAGC",
                                           "upperarm", "upperarm_noAGC", "upperarm_AGC"),
                                         c("MAE", "MAPE"))), 3)

colM_conf <- round(matrix(c(colMeans(summ_list_conf),
                            colMeans(summ_list_conf[which_wrist, ]),
                            colMeans(summ_list_conf[which_wrist_NoAGC, ]),
                            colMeans(summ_list_conf[which_wrist_AGC, ]),
                            colMeans(summ_list_conf[which_upperarm, ]),
                            colMeans(summ_list_conf[which_uparm_NoAGC, ]),
                            colMeans(summ_list_conf[which_uparm_AGC, ])), ncol = 2, byrow = T,
                          dimnames = list(c("all", "wrist", "wrist_noAGC", "wristAGC",
                                            "upperarm", "upperarm_noAGC", "upperarm_AGC"),
                                          c("MAE", "MAPE"))), 3)





w(14, 7)
plot(ref_time/60, ref_rr, type = "n", ylim = c(200, 2000), xlim = c(0, 10),
     xlab = "time (min)", ylab = "RRI (ms)")
abline(v = crrmat2[crrmat2[,3] == 1, 1]/60, col = "lightblue", lwd = 2)
lines(ref_time/60, ref_rr, col = "red", lwd = 3)
lines(crrmat2[,1]/60, crrmat2[,2], col = "forestgreen", lwd = 2)
legend("topleft", lwd = 3, col = c("red", "forestgreen", "lightblue"),
       leg = c("reference", "LifeQ", "confident"), bg = "white")


