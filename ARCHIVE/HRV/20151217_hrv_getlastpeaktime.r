source("../useful_funs.r")
source("./ERMADT_funs.r")



cout <- read.table("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/out_testing.txt", head = T, sep = ",")
capi <- read.table("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/physcalc_out.txt", head = T, sep = ",")

Fs <- 25


ERMADT_W1 <- 3
if(Fs == 50) {
  FILT_BP_ORDER <- 51
  FILT_LP_ORDER <- 21
  ERMADT_W2 <- 45
}
if(Fs == 25) {
  FILT_BP_ORDER <- 26
  FILT_LP_ORDER <- 11
  ERMADT_W2 <- 23
}


ERMADT_W <- floor((ERMADT_W1 + ERMADT_W2)/2)

pkind <- cout[,4]
pkind <- pkind[which(diff(pkind) != 0) + 1] + ((( 3 ))) - ERMADT_W - (floor(FILT_BP_ORDER/2) + floor(FILT_LP_ORDER/2))



pktime <- capi[,18] + capi[,19]
pktime <- pktime[which(diff(pktime) != 0) + 1]


w() ; par(ask = T, mar = c(4,4,2,4))
start <- 1 ; winsize <- 250 ; overlap <- 125 ; j <- 0
while(TRUE){
  
  j = j+1
  plotind <- (overlap*(j-1)+start):(start-1+winsize + overlap*(j-1))
  
  if(max(plotind) > nrow(capi))
    break
  else{
    
    lplot(plotind, cout[plotind, 1], lwd = 2, col = "forestgreen")
    par(new = T)
    lplot(plotind, cout[plotind, 3], lwd = 2, col = "blue", ann = F, yaxt = "n", xaxt = "n")
    axis(4)
    
    abline(v = pktime, col = "red", lwd =2)
  }
}



# aa <- cout[1200:2200, 1]
# aa2 <- aa
# aa2[485:510] <- aa2[485:510]*1.1
# bb <- FIRfilt(aa2)
# 
# bb2 <- FIRfilt_50hz_LP_hamming_20_6hz(bb)


# aa2 <- c(rep(0, 35), aa2)
# zPlot(cb(aa2[1:len(bb2)], bb2), par_new = T)



