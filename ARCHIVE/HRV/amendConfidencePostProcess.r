




amendConfidencePostProcess <- function(rr_intervals, rr_confidence,
                                       MIN_TIME_CONF_1 = 15, MIN_TIME_CONF_0 = 10)
{
  
  # create matrix with two columns: the cumulative sum of the RR intervals
  # (i.e. time in sec on which each RR occurs), and the corresponding confidence
  rr_mat <- cbind(cumsum(rr_intervals)/1000, rr_confidence) 
  
  
  ################################################################
  # FIRST LOOP TO ELIMINATE CONFIDENCE-1 BLOCKS DEEMED TOO SHORT #
  ################################################################
  
  # variables to indicate if previous confidence block was 1
  prev1 <- FALSE
  t_index <- 0
  
  for(i in 1:nrow(rr_mat)) {
    
    # indicate beginning of confidence-1 block
    if(!prev1 && (rr_mat[i, 2] == 1)) {
      t_index <- i
      prev1 <- TRUE
    }
    
    # if confidence-1 block ended, evaluate its length of time
    if(prev1 && (rr_mat[i, 2] == 0)) {
      time_block1 <- rr_mat[i, 1] - rr_mat[t_index, 1]
      
      # if the duration too short, change block to 0 confidence
      if(time_block1 < MIN_TIME_CONF_1)
        rr_mat[t_index:i, 2] <- 0
      
      prev1 <- FALSE
    }
    
  }
  
  #################################################################
  # SECOND LOOP TO ELIMINATE CONFIDENCE-0 BLOCKS DEEMED TOO SHORT #
  #################################################################
  
  # variables to indicate if previous confidence block was 0
  prev0 <- FALSE
  t_index <- 0
  
  for(i in 1:nrow(rr_mat)) {
    
    # indicate beginning of confidence-0 block
    if(!prev0 && (rr_mat[i, 2] == 0)) {
      t_index <- i
      prev0 <- TRUE
    }
    
    # if confidence-0 block ended, evaluate its length of time
    if(prev0 && (rr_mat[i, 2] == 1)) {
      time_block0 <- rr_mat[i, 1] - rr_mat[t_index, 1]
      
      # if the duration too short, change block to 1 confidence
      if(time_block0 < MIN_TIME_CONF_0)
        rr_mat[t_index:i, 2] <- 1
      
      prev0 <- FALSE
    }
    
  }
  
  # return matrix with amended confidence metric
  return(cb(rr_mat[,1], rr_intervals, rr_mat[,2]))
}
