source("../useful_funs.r")
source("./ERMADT_funs.r")
source("./hrv_viz.r")



datlist <- getDataFromDir("D:/HealthQ/Data/HRV/High HR/", head=T)
reflist <- getDataFromDir("D:/HealthQ/Data/HRV/High HR/Reference/", head=T)
reflist <- lapply(reflist, function(x) x[,1])
reflist2 <- getDataFromDir("D:/HealthQ/Data/HRV/High HR/Reference/bioharness RR/", head=T)
reflist2 <- lapply(reflist2, function(x) x[,2])



i <- 7
batch_plot <- FALSE

RRlist_R <- vector("list", len(datlist))

for(i in 1:len(datlist)) {
  
  dat <- datlist[[i]]
  dat <- dat[dat[,1] != 0,]
  dat <- dat[10:nrow(dat), c(3:9, 11, 13, 15:21)]
  
  curr1 <- curr2 <- matrix(0, nr=nrow(dat), nc=2)
  for(j in 1:1){
    if(j < 3){
      Rf <- dat[,8]
      led <- dat[,14]
    }
    else{
      Rf <- dat[,9]
      led <- dat[,j+12]
    }
    
    safe_term <- 0
    if(j == 2){
      safe_term <- 1
      led <- numeric(nrow(dat))
    }
    
    dat_tmp <- dat[,j] 
    isub_tmp <- dat[,9+j]
    curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
    curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e8)  
    # curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e8)[[1]]
  }
  
  grn <- curr2[,1]

  
  Fs <- 50
  hrv_out <- ER_MA_DT_peaks_C(grn, filt_t="FIRHAM", clip=4*Fs, output=T, interp=T, bf_fs=Fs, check_mult_peaks_above_zero=T)
  rr1 <- hrv_out$RR
  xf1 <- hrv_out$filt_signal
  
  
  Fs <- 50
  hrv_out <- ER_MA_DT_peaks_C(-grn, filt_t="FIRHAM", clip=4*Fs, output=T, interp=T, bf_fs=Fs, check_mult_peaks_above_zero=T,
                              run_mean_prefilt_cap=50, run_mean_algo_cap=50,
                              post_filt_running_mean=T, run_mean_postfilt_cap=5)
  rr2 <- hrv_out$RR
  xf2 <- hrv_out$filt_signal
  
  
  if(batch_plot) {
    zPlot(cb(rr1, rr2))
    zPlot(cb(xf1, xf2))
  }

  
  
#   Fs <- 25
#   grn <- grn[seq(1, len(grn), 2)]
#   
#   hrv_out <- ER_MA_DT_peaks_C(grn, filt_t="FIRHAM", clip=4*Fs, output=T, interp=T, bf_fs=Fs, check_mult_peaks_above_zero=T)
#   rr2 <- hrv_out$RR
#   xf2 <- hrv_out$filt_signal

  
  RRlist_R[[i]] <- list(rr1, rr2)
  
  
  catProgress(i, len(datlist))
}



###################################################################################################
###################################################################################################




index_not_broken <- c(1:3, 5:7, 10:11)

# w() ; par(ask=T)
for(i in index_not_broken) {
  
  ref <- reflist[[i]]
  # ref2 <- reflist2[[i]]
  rr1 <- RRlist_R[[i]][[1]]
  rr2 <- RRlist_R[[i]][[2]]
  
  w()
  lplot(cumsum(ref)/1000, ref, lwd=3, col="red", main=paste(i, "|", names(reflist)[i]))
  lines(cumsum(rr1)/1000, rr1, lwd=2, col="blue")
  lines(cumsum(rr2)/1000, rr2, lwd=2, col="mediumseagreen")
  
  savePlot(paste0("./results/", i, "_", names(datlist)[i], "_RRout.png"), "png")
  graphics.off()
  
}






###################################################################################################
###################################################################################################



sampdiff_to_hr <- function(x, fs=50)
{
  return(cbind(bpm=60/(x/fs), RR=1000*(x/fs)))
}


