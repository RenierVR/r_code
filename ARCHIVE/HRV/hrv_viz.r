source("../useful_funs.r")
source("./ERMADT_funs.r")


###################################################################################################
###################################################################################################

hrv_ERMADT_plot <- function(out)
{
  
  zPlot(cbind((out$filt_signal)^2,
              replace(out$ma_beat, is.na(out$ma_beat), 0),
              replace(out$ma_peak, is.na(out$ma_peak), 0), 
              replace(out$thr1, is.na(out$thr1), 0)),
        ynames=c("Filtered", "MA beat", "MA peak", "threshold"),
        col_f=c("grey", "blue", "mediumseagreen", "orange"),
        points_c=cbind(out$peaks, (out$peakvals)^2))
  
}