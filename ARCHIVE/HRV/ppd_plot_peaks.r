w <- function(width=14, height=7, ...) windows(width, height, ...)

# out_init  <- read.table("~/HealthQ/C FILES/PeakPredictionDetection_project/out_init.txt", sep=",")
# out_pks   <- read.table("~/HealthQ/C FILES/PeakPredictionDetection_project/out_pks.txt", sep=",")
# out_pksid <- read.table("~/HealthQ/C FILES/PeakPredictionDetection_project/out_pksid.txt", sep=",")


plot_peaks <- function(read_dat=TRUE, winsize=512, overlap=256, start=1,
                       acc_ylim=c(50,90), project="PeakPredictionDetection_project")
{
  if(read_dat){
    out_init  <- read.table(paste0("~/HealthQ/C FILES/", project,"/out_init.txt"), sep=",")
    out_pksid <- read.table(paste0("~/HealthQ/C FILES/", project,"/out_pksid.txt"), sep=",")
  }

  n <- nrow(out_init)
  
  n_na   <- ceiling(n/winsize)*winsize-n
  y      <- c(out_init[,3], rep(NA, n_na))
  acc    <- c(out_init[,4], rep(NA, n_na))
  w_brks <- which(out_init[,5] == 1)
  pks    <- out_init[,7][out_init[,7] != -1]+1
  out_pksid[,1] <- out_pksid[,1]+1
  
  ## determine point of initialisation
  poi <- out_pksid[which(out_pksid[,2] == 1), 1]
  
  
  w(14,7)
  par(mar=c(2,4,2,2), ask=T)
  layout(matrix(c(1,2), nr=2, nc=1), heights=c(1,2))
  j <- 0
  while(TRUE){
    j <- j+1
    plotind <- (overlap*(j-1)+start):(start-1+winsize + overlap*(j-1))
    if(max(plotind) > length(y))
      break
    else{
      plot(plotind, acc[plotind], type="l", lwd=2, col="orange", ylim=acc_ylim, ylab="acc")
      
      yp  <- y[plotind]
      ypr <- na.omit(yp)
      plot(plotind, yp, type="n", ylim=range(ypr[!(ypr==0 | ypr==-1)]), ylab="green (Gauss 4)")
      
      abline(v=poi, lwd=6, col="yellow")
      lines(plotind, y[plotind], lwd=2, col="limegreen")
      points(plotind, y[plotind], pch=19, cex=0.5, col="limegreen")
      abline(v=w_brks, lty=2, col="gray")
      
      ## draw initial peak detection lines
      abline(v=pks, col="blue", lwd=2)
      
      abline(v=out_pksid[,1], col="red", lwd=2, lty=2)
      points(out_pksid, pch=19, col="red")
      
#       abline(v=imp_pks, col="purple", lty=3)
    }
  }
  
}



plot_peaks(read=T, winsize=64, overlap=32)
# plot_peaks(read=F, start=847000)


# > init_res[ids,]
# 521  203538 203017  19
# 634  264958 264324 113
# 793  324964 324171  75
# 1182 458734 457552  80
# 1476 563683 562207  32 x
# 1701 646476 644775  78
# 1977 746615 744638  76
# 2260 853632 851372  74 (check dnotches)
# 2383 904116 901733  72


# 847150 - 847200 (2251)
# 854100 - 854200 (2266)

# 
# write.table(cbind(dat[849435:856408,3]-dat[849435:856408,4], 0),
#             "~/HealthQ/C FILES/Init_project_test/data/preprocessed/eugene_slaap_dnotch_slice.txt",
#             sep=",", quote=F, row.names=F, col.names=F)





