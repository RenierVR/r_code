source("../useful_funs.r")
source("./ERMADT_funs.r")

## get file names
datlist <- getDataFromDir("D:/HealthQ/Data/24h/1_Shannagh/", dataE=F, ret="full")
reflist <- list(read.table("D:/HealthQ/Data/24h/1_Shannagh/BioHarness/2015_06_24-11_13_12/2015_06_24-11_13_12_RR.csv", sep=",", head=T)[,2])

###################################################################################################
## REFORMAT DATA FOR C

# for(i in 1:len(datlist)){
#   dat <- read.table(datlist[[i]], head=T, sep=",")
# #   dat <- dat[,c(1:7, 9:17, 8, 18:26)]
# #   dat[,10:13] <- 10*dat[,10:13]
# #   dat <- cbind(dat[-1,1:7], dat[1:(nrow(dat)-1), 8:26])
#   colnames(dat) <- c("TOKEN", "SUBTOKEN", "ppg_green", "ppg_ambient",  "ppg_red",	 "ppg_infrared",	 "acc_x",	 "acc_y",	 "acc_z",
#                      "voltage",  "amp_green",	 "amp_red",	 "off_green",	 "off_ambient",	 "off_red",	 "off_infrared", 
#                      "led_green",	 "led_red",	 "led_infrared",  "bufferspace",	 
#                      "emb_heartrate",	 "emb_cadence",	 "emb_calories",	 "emb_epoch",	 "emb_vo2",	 "emb_bloodlactate", "emb_snr")
#   write.table2(dat, datlist[[i]], col.names=T)
# }

###################################################################################################

###############################################
##                                           ##
##  RUN HRV ALGORITHM THROUGH FULL 24h DATA  ## 
##                                           ##
###################################################################################################

Fs <- 50
i <- 1


ref <- reflist[[i]]

dat <- read.table(datlist[i], sep=",", head=T)
ares <- acc_res(dat[,7:9], 16)

dat <- dat[dat[,1] != 0, c(3:9, 11:19)]
dat <- dat[10:nrow(dat),]
curr1 <- curr2 <- matrix(0, nr=nrow(dat), nc=1)
for(j in 1:1){
  if(j < 3){
    Rf <- dat[,8]
    led <- dat[,14]
  }
  else{
    Rf <- dat[,9]
    led <- dat[,j+12]
  }
  
  safe_term <- 0
  if(j == 2){
    safe_term <- 1
    led <- numeric(nrow(dat))
  }
  
  dat_tmp <- dat[,j] 
  isub_tmp <- dat[,9+j]
  curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
  curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e8)  
}
rm(dat)


hrv_out <- ER_MA_DT_peaks_C(curr2[,1], filt_t="FIR50", clip=200, output=T, interp=F, print_output=T)
rr1 <- hrv_out$RR
xf1 <- hrv_out$filt_signal




################################################
##                                            ##
##  SEGMENTATION OF 24H DATA (AND REFERENCE)  ## 
##                                            ##
###################################################################################################

seg_inds <- c(1, 290000, 577500, 888000, 1183000, 1486000, 1784000, 1999000, 2270800, 2589600, 2940700, 3257000, 3519000)

for(j in 2:len(seg_inds))
  write.table2(dat[seg_inds[j-1]:seg_inds[j],], col.names=T,
               file = paste0("D:/HealthQ/Data/24h/1_Shannagh/segmented/20150624_1121_Device5147_Shannagh_24Hour_EXERCISE_segment", j-1, ".csv"))

bhmat <- cb(Time=cumsum(ref)/1000, RtoR=ref)

for(j in 2:len(seg_inds)) {
  
  lower <- seg_inds[j-1]*0.02
  upper <- seg_inds[j]*0.02
  
  # if(!(lower == 0.02)) lower <- lower - 600
  upper <- upper + 600
  
  bhmat_seg <- bhmat[(bhmat[,1] > lower) & (bhmat[,1] < upper),]
  write.table2(bhmat_seg, col.names=T,
               file = paste0("D:/HealthQ/Data/24h/1_Shannagh/segmented/reference/20150624_Shannagh_24Hour_Bioharness_RR_segment", j-1, ".csv"))
  
  if(upper >= 62419.69)
    break
}




#####################################
##                                 ##
##  ANALYSE SEGMENTS INDIVIDUALLY  ## 
##                                 ##
###################################################################################################

## find alignment info and data/reference file paths
align_info <- read.table("D:/HealthQ/Data/HRV/SVN_HRV_DATA/trunk/healthq/mojo14/HRV data/24h/segmented/reference/alignment_info.txt", head=T, sep=",")
reflist <- paste0("D:/HealthQ/Data/HRV/SVN_HRV_DATA/trunk/", align_info[,1])
seglist <- paste0("D:/HealthQ/Data/HRV/SVN_HRV_DATA/trunk/", align_info[,2])
align_info <- align_info[,3]


## analyse each segment sequentially
for(i in 1:len(seglist)) {
  
  ## read reference segment and align appropriately
  ref <- read.table(reflist[i], sep=",", head=T)
  ref[,1] <- cumsum(ref[,2])/1000 - align_info[i]
  ref <- ref[ref[,1] >= 0,]
  
  ## read data segment
  # seg <- read.table(seglist[i], head=T, sep=",")[, c(3:9, 11:19)]
  
  ## read C output
  cout <- read.table(paste0("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/physcalc_out_", basename(seglist[i])), head=T, sep=",")
  crrmat <- cout[cout[,10] == 1, c(11,13)]
  crrtime <- cumsum(crrmat[,1])/1000
  
  ## post-process RR confidence metric
  crrmat2 <- amendConfidencePostProcess(crrmat[,1], crrmat[,2])
  
  ## truncate to equal lengths
  if(tail(ref[,1], 1) > tail(crrtime, 1))
    ref <- ref[ref[,1] <= tail(crrtime, 1),]
  
  #######################
  ## visualise results ##
  #######################
  
#   w()
#   plot(ref, type="n", xlim=c(1,600))
#   abline(v=crrtime[which(crrmat2[,2] == 1)], col="yellow")
#   lines(ref, lwd=2, col="red")
#   lines(crrtime, crrmat[,1], lwd=2, col="blue")
  
  ## in 10 minute partitions
  pind <- c(seq(1, tail(crrtime, 1), 600), tail(crrtime, 1))
  for(j in 2:len(pind)) {
    
    w()
    plot(ref, type="n", xlim = pind[(j-1):j], ylim = c(300, 1500), 
         main = paste0("20150624 | Shannagh 24h | segment ", i, ".", j-1))
    abline(v=crrtime[which(crrmat2[,2] == 1)], col="yellow", lwd=2)
    lines(ref, lwd=3, col="red")
    lines(crrtime, crrmat[,1], lwd=2, col="blue")
    
    savePlot(paste0("./results/Shannagh24h/segment_", i, "_", j-1, ".png"), "png")
    graphics.off()
    
  }
  
  catProgress(i, len(seglist))
  
}









