source("../useful_funs.r")
source("./ERMADT_funs.r")
source("./amendConfidencePostProcess.r")
source("./alignToRefRR.r")



#################
#################
##             ##
##  LOAD DATA  ## 
##             ##
###################################################################################################
###################################################################################################

## 20151016 - Renier 20h data (wrist)
datname <- getDataFromDir("D:/HealthQ/Data/24h/2_Renier/", dataE = F, ret = "full")[1]
ref <- read.table("D:/HealthQ/Data/24h/2_Renier/Bioharness/2015_10_14-19_02_47/2015_10_14-19_02_47_RR.csv", sep = ",", head = T)[,2]

## 20151016 - Renier 20h data (upper arm)
datname <- getDataFromDir("D:/HealthQ/Data/24h/2_Renier/", dataE = F, ret = "full")[2]
ref <- read.table("D:/HealthQ/Data/24h/2_Renier/Bioharness/2015_10_14-19_02_47/2015_10_14-19_02_47_RR.csv", sep = ",", head = T)[,2]



######################
######################
##                  ##
##  READ C OUTPUT   ## 
##                  ##
###################################################################################################
###################################################################################################

## read C output
cout <- read.table(paste0("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/physcalc_out_", basename(datname)), head = T, sep = ",")
crrmat <- cout[cout[,10] == 1, c(11, 13)]
crrtime <- cumsum(crrmat[,1])/1000

## post-process RR confidence metric
crrmat2 <- amendConfidencePostProcess(crrmat[,1], crrmat[,2], 60, 30)
crrmat2 <- cb(crrmat2[,1], crrmat[,1], crrmat2[,2])



#################################
#################################
##                             ##
##  ALIGNMENT WITH REFERENCE   ## 
##                             ##
###################################################################################################
###################################################################################################

# assign sleep section limits (in seconds)
if(basename(datname) == "20151015_Renier20hour_5232_Wrist.csv") {
  sleep_bounds <- c(18800, 52800)
  
  out_list <-  alignToRefRR(ref, crrmat2, XLIMIT = 70000, SLEEP = sleep_bounds, PLOT_T = 2, 
                            SAVE_PLOTS = T, SAVE_PATH = "./results/20151016 Renier 20h/wrist_segment")
  

#   out_list_20151016_renier_20h_wrist <- out_list
#   save(out_list_20151016_renier_20h_wrist, 
#        file="20151016_renier_20h_RRI_wrist_out.Rdata")

}


##########################################################################
##########################################################################


if(basename(datname) == "20151015_Renier20hour_5233_UpperArm.csv")  {
  sleep_bounds <- c(18800, 52800)

  out_list <-  alignToRefRR(ref, crrmat2, XLIMIT = 70000, SLEEP = sleep_bounds, PLOT_T = 2, 
                            SAVE_PLOTS = T, SAVE_PATH = "./results/20151016 Renier 20h/upperarm_segment")
  

#   out_list_20151016_renier_20h_upperarm <- out_list
#   save(out_list_20151016_renier_20h_upperarm, 
#        file="20151016_renier_20h_RRI_upperarm_out.Rdata")


}



# cat("proportion of windows used =", out_list[[2]][1], "%",
#     "\nmean proportion outliers (all) =", out_list[[2]][2], "%",
#     "\nmean proportion outliers (ref) =", out_list[[2]][3], "%",
#     "\nmean proportion outliers (mojo) =", out_list[[2]][4], "%",
#     "\n\nmean abs diff =", round(out_list[[3]][1],2), "\nmean abs diff (weighted) =", round(out_list[[3]][2],2),
#     "\nmean abs % diff =", round(out_list[[3]][3],2), "%\nmean abs % diff (weighted) =", round(out_list[[3]][4],2), "%\n")
# 
# 
# 
# 
# w() ; lplot(out_mat[,7], lwd=2, col="red")
# par(new=T)
# lplot(out_mat[,9], ann=F, yaxt="n", xaxt="n")
# axis(4)


