source("../useful_funs.r")
source("./ERMADT_funs.r")

## get file names
datlist <- getDataFromDir("D:/HealthQ/Data/Sleep/MOJO DATA/", dataE=F, ret="full", ext="csv")


###################################################################################################
## REFORMAT DATA FOR C

# for(i in 1:len(datlist)){
#   dat <- read.table(datlist[[i]], head=T, sep=",")
#   
#   # dat <- dat[,c(3:9, 10, 12, 14:20, 21:30)]
#   dat <- dat[,c(1:7, 9:17, 8, 18:26)] 
#   
#   dat[,10:13] <- 10*dat[,10:13]
#   dat <- cbind(dat[-1,1:7], dat[1:(nrow(dat)-1), 8:25])
#   write.table2(dat, datlist[[i]], col.names=T)
# }

# dat <- cbind(dat[1:(nrow(dat)-1),1:7], dat[-1, 8:26])

###################################################################################################

Fs <- 50
i <- 5

# ANALYSE_SLEEP_DATA <- function(i=1, Fs=50)
# {
  dat <- read.table(datlist[i], sep=",", head=T)
  
  dat <- dat[dat[,1] != 0,]
  dat <- dat[10:nrow(dat),]
  curr1 <- curr2 <- matrix(0, nr=nrow(dat), nc=4)
  for(j in 1:1){
    if(j < 3){
      Rf <- dat[,8]
      led <- dat[,14]
    }
    else{
      Rf <- dat[,9]
      led <- dat[,j+12]
    }
    
    safe_term <- 0
    if(j == 2){
      safe_term <- 1
      led <- numeric(nrow(dat))
    }
    
    dat_tmp <- dat[,j] 
    isub_tmp <- dat[,9+j]
    curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
    curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e8)  
    # curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e8)[[1]]
  }
  
  
  
  hrv_out <- ER_MA_DT_peaks_C(curr2[,1], filt_t="FIR50", clip=200, output=T, interp=T, print_output=T)
  rr1 <- hrv_out$RR
  xf1 <- hrv_out$filt_signal
  
  
  
#   ff <- function(x)
#   {
#     y <- FIRfilt(x, Fs=Fs)
#     y <- FIRfilt_50hz_LP_hamming_20_6hz(y)
#     return(y)
#   }
  hrv_out <- ER_MA_DT_peaks_C(curr2[,1], filt_t="FIRHAM", clip=200, output=T, interp=T, print_output=T)
  rr2 <- hrv_out$RR
  xf2 <- hrv_out$filt_signal
  
  hrv_out <- ER_MA_DT_peaks_C(curr2[,1], filt_t="FIRHAM", clip=200, output=T, interp=T, print_output=T, 
                              post_filt_running_mean=F, check_mult_peaks_above_zero=F)
  rr3 <- hrv_out$RR
  xf3 <- hrv_out$filt_signal
  

    
# }


x <- curr2[,1]
xf1 <- FIRfilt(x)
xf2 <- IIRfilt_LP_Butter_7(x)





###################################################################################################
###################################################################################################

rpeaks <- hrv_out$peaks

# cout <- read.table(paste0("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/physcalc_out_20141208_2147_Kari_Sleep.csv"), head=T, sep=",")
# cout <- read.table(paste0("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/physcalc_out_kari_sleep_segment.txt"), head=T, sep=",")
# cout <- read.table(paste0("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/physcalc_out_20150702_2155_EliznaCole_Sleep_profiler.csv"), head=T, sep=",")
cout <- read.table(paste0("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/physcalc_out_20150424_2221_Bertha_Sleep_profiler.csv"), head=T, sep=",")
crr <- cout[cout[,10]==1, 11]

ctest <- read.table("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/out_testing.txt", sep=",", head=T)


c4 <- ctest[,4]
cpeaks <- c4[which(diff(c4) != 0)+1]
cpeaks <- cpeaks - 231
cpeaks <- cpeaks[cpeaks > 1]
cpeaks <- cpeaks[1:len(rpeaks)]

pind <- 50:70
w() ; plot(rpeaks[pind], rep(1, len(rpeaks))[pind], pch=19)
points(cpeaks[pind], rep(1.01, len(rpeaks))[pind], pch=19, col="red")



aa <- cbind(rpeaks, cpeaks, rpeaks-cpeaks)



w() ; lplot(cumsum(rr2)/1000, rr2, lwd=2, col="blue", xlim=c(4000,4990))
lines(cumsum(crr)/1000, crr, lwd=2, col="red")
