dat <- read.table("clipboard")



grn <- dat[,5]
grnbl <- dat[,6]
grn_rf <- dat[,16]
grn_led <- dat[,18]
grn_isub <- dat[,21]
grnbl_isub <- dat[,22]

rr <- dat[,26]
rr <- rr[1:6000]

acc <- acc_res(dat[,1:3])

rm(dat)


grn_curr <- convertToCurrent3(grn[1:(len(grn)-1)], grn_rf[-1], grn_isub[-1])
grnbl_curr <- convertToCurrent3(grnbl[1:(len(grn)-1)], grn_rf[-1], grnbl_isub[-1])

grn_curr1 <- removeCurrentDiscont2(grn_curr, grn_rf[-1], grn_isub[-1], grn_led[-1], 1e6)
grn_curr2 <- removeCurrentDiscont2(grn_curr-grnbl_curr, grn_rf[-1], grn_isub[-1], grn_led[-1], 1e6)



zPlot(cbind(acc[-1], grn_curr2)[1:6000,], par_new=T, col_f=c("pink", "blue"))


w() ; lplot(rr, lwd=2)



bh_rr <- scan("clipboard")

lq_rr <- rr[seq(1,len(rr),50)]

w() ; lplot(lq_rr, lwd=2) ; lines(bh_rr[-c(1:300)], col="red", lwd=2)
