alignToRefRR <- function(ref, rrmat, winsize = 750, overlap = 0.9*winsize, start = 1,
                         ALIGN_FACTOR = 0, XLIMIT = 70000,
                         PROP_OUTL_CAP = 0.5, PROP_OUTL_CAP_POST = 0.3,
                         SLEEP_BOUNDS = c(0, 0),
                         SAVE_PLOTS = TRUE, SAVE_PATH = "./results/segment",
                         ANY_PLOTS = FALSE, PLOT_TYPE = (1:2)[1],
                         REF_FAIL = 5000, OUTPUT = TRUE)
{
  
#   ALIGN_FACTOR <- 6.4 ; PROP_OUTL_CAP <- 0.5 ; XLIMIT <- 70000
#   SAVE_PLOTS <- F
#   winsize = 750 ; overlap = winsize*0.9 ; start = 1
#   REF_FAIL <- 5000 ; OUTPUT <- TRUE
  
  
  # manage plotting logistics
  ANY_PLOTS <- ANY_PLOTS | SAVE_PLOTS
  
  if(ANY_PLOTS) { #&& (!SAVE_PLOTS)) {
    w(14,6)
    if(PLOT_TYPE == 1) par(ask = !SAVE_PLOTS, mfrow = c(2,1), mar = c(4,4,2,2))
    if(PLOT_TYPE == 2) {
      par(ask = !SAVE_PLOTS, mar = c(0,5,0,4), oma = c(5,0,4,0))
      layout(matrix(c(1,2), nr=2, nc=1), heights=c(1,3))
    }
  }
  
  # check if confidence should be analysed
  include_conf <- FALSE
  if(ncol(rrmat) == 3)
    include_conf <- TRUE
  
  # check if sleep should be analysed
  include_sleep <- FALSE
  if(!missing(SLEEP_BOUNDS))
    include_sleep <- TRUE
  
  # cumulative time of reference RRIs
  ref_csum <- cumsum(ref)/1000 + ALIGN_FACTOR
  
  # initiate variables
  k_min_lag <- 0
  k_lag_all <- 0
  prev_long_shift <- FALSE
  prev_ref_fail <- FALSE
  is_diff_valid <- TRUE
  is_sleep_section <- FALSE
  
  out_mat <- NULL
  


  
  
  j = 0
  cPt = 0
  
  while(TRUE){
    
    j = j+1
    plotind <- (overlap*(j-1)+start):(start-1+winsize + overlap*(j-1))
    
    if(max(plotind) > XLIMIT)
      break
    else{
      
      ########################################
      ##  designate vectors for new window  ##
      ########################################
      
      xp1 <- ref_csum[plotind]
      yp1 <- ref[plotind]
      
      x_lim <- range(ref_csum[plotind])
      x_which <- which((rrmat[,1] >= x_lim[1]) & (rrmat[,1] <= x_lim[2]))
      
      xp2 <- rrmat[x_which, 1]
      yp2 <- rrmat[x_which, 2]
      
      if(include_conf) xconf <- rrmat[x_which, 3]
      
      
      ############################################
      ##  plot unaligned (when PLOT_TYPE == 1)  ##
      ############################################
      
      if(ANY_PLOTS && (PLOT_TYPE == 1)) {
#         if(SAVE_PLOTS) {
#           w(14,8) ; par(mfrow=c(2,1), mar=c(4,4,2,2))
#         }
        
        lplot(xp1, yp1, lwd=3, col="red", 
              ylab = "RR intervals (ms)", xlab = "time (s)")
        lines(xp2, yp2, lwd=2, col="forestgreen")
      }      
      
      ##########################
      ##  determine outliers  ##
      ##########################
      
      # equalise size
      min_len <- min(winsize, len(x_which))
      
      yp1_amend <- yp1[1:min_len]
      yp2_amend <- yp2[1:min_len]
      
      # calculate moving aves to use as proxies for outliers
      yp1_ma <- movingAve(yp1_amend, 25)
      yp2_ma <- movingAve(yp2_amend, 25)
      
      yp1_bounds <- cbind(yp1_ma - 300, yp1_ma + 300)
      yp1_bounds[yp1_bounds[,1] <= 400, 1] <- 400
      yp1_bounds[yp1_bounds[,2] >= 1600, 2] <- 1600
      
      yp2_bounds <- cbind(yp2_ma - 300, yp2_ma + 300)
      yp2_bounds[yp2_bounds[,1] <= 400, 1] <- 400
      yp2_bounds[yp2_bounds[,2] >= 1600, 2] <- 1600
      
      # replace outliers
      yp1_outls <- (yp1_amend >= yp1_bounds[,2]) | (yp1_amend <= yp1_bounds[,1])
      yp2_outls <- (yp2_amend >= yp2_bounds[,2]) | (yp2_amend <= yp2_bounds[,1])
      yp1_amend[yp1_outls] <- yp1_ma[yp1_outls]
      yp2_amend[yp2_outls] <- yp2_ma[yp2_outls]
      
      # update plotting vectors length
      yp1 <- yp1[1:min_len]
      yp2 <- yp2[1:min_len]
      xp1 <- xp1[1:min_len]
      xp2 <- xp2[1:min_len]
      
      if(include_conf) xconf <- xconf[1:min_len]
      
      if(include_sleep)
        is_sleep_section <- (min(xp1) > SLEEP_BOUNDS[1]) & (max(xp2) < SLEEP_BOUNDS[2])
      
      
      ########################################
      ##  look for argmin time lag wrt MAD  ##
      ########################################
      
      any_outl <- yp1_outls | yp2_outls
      sum_good <- sum(!any_outl)
      conf_no_outl <- (!any_outl) & (xconf == 1)
      conf_outl_yp1 <- (yp1_outls) & (xconf == 1)
      conf_outl_yp2 <- (yp2_outls) & (xconf == 1)
      
      k_min_lag <- 0
      mad_text <- "---"
      mad_text_conf <- "---"
      is_diff_valid <- TRUE
      
      out_all <- c(OUT_MAD = 0, OUT_RMSD = 0, OUT_MAPD = 0)
      out_conf <- c(OUT_MAD_CONF = 0, OUT_RMSD_CONF = 0, OUT_MAPD_CONF = 0)
      # out_sleep <- c(OUT_MAD_SLEEP = 0, OUT_RMSD_SLEEP = 0, OUT_MAPD_SLEEP = 0)
      # out_sleep_conf <- c(OUT_MAD_SLEEP_CONF = 0, OUT_RMSD_SLEEP_CONF = 0, OUT_MAPD_SLEEP_CONF = 0)
      
      
      if((mean(any_outl) <= PROP_OUTL_CAP)) {
        
        if(j == 1) {
          # initial alignment
          lags <- seq(-120, 120, by=0.5)
        }
        else if(any(yp1[1:min_len] > REF_FAIL) || prev_ref_fail) {
          # if reference failed, calculate time lost
          lags <- seq(-1, 300, by=0.5)
          if(any(yp1[1:min_len] > REF_FAIL)) 
            prev_ref_fail <- TRUE
          else {
            prev_ref_fail <- FALSE
            prev_long_shift <- TRUE
          }
          
          is_diff_valid <- FALSE
        }
        else {
          # normally, limit alignment to 1 min both directions
          lags <- seq(-60, 60, by=1)
          prev_ref_fail <- FALSE
        }
        
        msd_lags <- sapply(lags, function(k) {
          
          msd_vec <- ( yp1 - 
                         yp2[sapply(xp1 + k, function(x) which.min(abs(x - xp2)))] )[!any_outl]
          
          return( median(abs(msd_vec)) )
          
        })
        
        k_min <- which.min(msd_lags)
        k_min_lag <- lags[k_min]
        
        if((k_min_lag > 5) || (k_min_lag < -5)) {
          if(prev_long_shift || prev_ref_fail)
            prev_long_shift <- FALSE
          else if(j > 1){
            k_min_lag <- 0
            prev_long_shift <- TRUE
            is_diff_valid <- FALSE
          }
        }
        else
          prev_long_shift <- FALSE
        
        ##################################################################
        
        # determine lag with increased accuracy
        lags <- seq(-10, 10, by=0.1) + k_min_lag
        
        msd_lags <- sapply(lags, function(k) {
          
          msd_vec <- ( yp1 - 
                         yp2[sapply(xp1 + k, function(x) which.min(abs(x - xp2)))] )[!any_outl]
          
          return( median(abs(msd_vec)) )
        })
        
        
        k_min <- which.min(msd_lags)
        k_min_lag <- lags[k_min]
        
        
        # calculate stats at corrected lag
        mad_all <- yp1 - yp2[sapply(xp1 + k_min_lag, function(x) which.min(abs(x - xp2)))] 
        
        mad_vec <- mad_all[!any_outl]
        
        # stats excluding outliers
        out_all[1] <- mean(abs(mad_vec))
        out_all[2] <- sqrt(mean((mad_vec)^2))
        out_all[3] <- mean(abs(mad_vec)/yp1[!any_outl])
        
        # stats for only good confidence, excluding outliers
        if(sum(conf_no_outl) > 0)
          mad_vec_conf <- mad_all[conf_no_outl]
        else
          mad_vec_conf <- NA
        
        out_conf[1] <- mean(abs(mad_vec_conf))
        out_conf[2] <- sqrt(mean((mad_vec_conf)^2))
        out_conf[3] <- mean(abs(mad_vec_conf)/yp1[conf_no_outl])
    
        
        mad_text <- as.character(round(out_all[1] ,2))
        mad_text_conf <- as.character(round(out_conf[1] ,2))
        
      }
      
      ####################
      ##  plot aligned  ##
      ####################
      
      if(ANY_PLOTS) {
#         if(SAVE_PLOTS && (PLOT_TYPE == 2)) {
#           # w(14,8)
#           # par(ask = F, mar = c(0,5,0,4), oma = c(5,0,4,0))
#           # layout(matrix(c(1,2), nr = 2, nc = 1), heights = c(1,4))
#         }
        
        if(PLOT_TYPE == 1) {
          plot(xp1 + k_min_lag, yp1, type="n", ylim=c(500,1600), 
               ylab = "trimmed RR intervals (ms)",
               xlab = paste0("aligned time | shifted ", round(k_min_lag,2), 
                             " s (window) | shifted ", round(k_lag_all,2), " s (total)"),
               main = paste0("Mean Abs Diff (trimmed) = ", mad_text, 
                             " ms | (", round(100*mean(any_outl)), "% outliers omitted)"))
          abline(v = (xp1 + k_min_lag)[which(yp1_outls)], col="orange")
          abline(v = xp2[which(yp2_outls)], col="green")
          lines(xp1 + k_min_lag, yp1, lwd=3, col="red")
          lines(xp2, yp2, lwd=2, col="forestgreen") 
        }
        
        if(PLOT_TYPE == 2) {
          plot(xp1 + k_min_lag, yp1, type = "n", yaxt = "n", xaxt = "n", ann = F)
          abline(v = xp2[which(xconf == 1)], lwd = 2, col = c("blue", "lightblue")[as.numeric(is_sleep_section)+1])
          mtext(paste0("Mean abs diff (trimmed) = ", mad_text, 
                       " ms | (", round(100*mean(any_outl)), "% outliers omitted)"), 3, 2, cex = 1.1)
          mtext(paste0("Mean abs diff (confident, trimmed) = ", mad_text_conf, 
                       " ms | (", round(100*(mean(conf_outl_yp1) + mean(conf_outl_yp2))), "% outliers omitted)"), 3, 1, cex = 1.1)
          legend("topright", leg = c(paste0(round(100*sum(xconf)/min_len, 1), "% good confidence"),
                                     paste0(round(100*sum(conf_no_outl)/min_len, 1), "% good confidence, outliers excl"),
                                     paste0("is_sleep_section = ", is_sleep_section)),
                 bg = "white", cex = 0.8)
          
          plot(xp1 + k_min_lag, yp1, type="n", ylim=c(500,1600), 
               ylab = "trimmed RR intervals (ms)")
          abline(v = (xp1 + k_min_lag)[which(yp1_outls)], col="orange")
          abline(v = xp2[which(yp2_outls)], col="green")
          lines(xp1 + k_min_lag, yp1, lwd=3, col="red")
          lines(xp2, yp2, lwd=2, col="forestgreen")
          mtext(paste0("aligned time | shifted ", round(k_min_lag,2), 
                       " s (window) | shifted ", round(k_lag_all,2), " s (total)"), 1, 3)
        }
        
        
        
        if(SAVE_PLOTS) {
          savePlot(paste0(SAVE_PATH, j, ".png"), "png")
          # graphics.off()
        }
      }
      
      
      ##############################
      ##  end-of-iteration admin  ##
      ##############################
      
      # ignore window if outlier proportion too high
      if(mean(any_outl) > PROP_OUTL_CAP) {
        k_min_lag <- 0
        is_diff_valid <- FALSE
      }
      
      # adjust reference globally with alignment factor
      ref_csum[min(plotind):len(ref_csum)] <- ref_csum[min(plotind):len(ref_csum)] + k_min_lag
      
      k_lag_all <- k_lag_all + k_min_lag
      
      
      if(OUTPUT)
        out_mat <- rbind(out_mat, c(min_len, sum_good, 
                                    mean(any_outl), mean(yp1_outls), mean(yp2_outls),
                                    mean(xconf), mean(conf_no_outl), 
                                    mean(conf_outl_yp1), mean(conf_outl_yp2), 
                                    as.numeric(is_diff_valid), as.numeric(is_sleep_section),
                                    out_all, out_conf,
                                    k_min_lag, k_lag_all))
      
      cPt <- catProgress(j, floor((XLIMIT-start-winsize+overlap)/overlap), 1, cPt)
      
    }
  }
  
  graphics.off()
  
  
  colnames(out_mat) <- c("MIN_LEN", "SUM_GOOD", 
                         "PROP_OUTL", "PROP_OUTL_REF", "PROP_OUTL_Y", 
                         "PROP_CONF", "PROP_CONF_OUTL_EXCL", "PROP_CONF_OUTL_REF", "PROP_CONF_OUTL_Y",
                         "IS_MAD_VALID", "IS_SLEEP_SECTION",
                         "MEAN_ABS_DIFF", "ROOT_MEAN_SQ_DIFF", "MEAN_ABS_PERC_DIFF",
                         "MEAN_ABS_DIFF_CONF", "ROOT_MEAN_SQ_DIFF_CONF", "MEAN_ABS_PERC_DIFF_CONF",
                         "LAG_WINDOW", "LAG_TOTAL")
  
  
  
  ####################################
  ##  post alignment summary stats  ##
  ####################################
  
  # summary stats -- all
  which_valid <- which(out_mat[,3] <= PROP_OUTL_CAP_POST)
  valid_used <- 1 - out_mat[which_valid, 3]
  
  out_mad <- out_mat[which_valid, 12]
  out_mad <- c(mean(out_mad), sum(out_mad*valid_used)/sum(valid_used))
  
  out_mapd <- out_mat[which_valid, 14]
  out_mapd <- c(100*mean(out_mapd), 100*(sum(out_mapd*valid_used)/sum(valid_used)))
  
  # summary stats -- confidence
  which_valid_conf <- !apply(out_mat, 1, function(x) any(is.na(x)))
  valid_used_conf <- 1 - out_mat[which_valid_conf, 3]
  
  out_mad_conf <- out_mat[which_valid_conf, 15]
  out_mad_conf <- c(mean(out_mad_conf), sum(out_mad_conf*valid_used_conf)/sum(valid_used_conf))
  
  out_mapd_conf <- out_mat[which_valid_conf, 17]
  out_mapd_conf <- c(100*mean(out_mapd_conf), 100*(sum(out_mapd_conf*valid_used_conf)/sum(valid_used_conf)))
  
  # stats regarding time durations
  time_tot <- tail(xp2)[1]/3600
  time_tot_valid <- sum(out_mat[which_valid, 1])/3600
  
  time_sleep_tot <- 0
  time_sleep_valid <- 0
  if(include_sleep) {
    time_sleep_tot <- diff(SLEEP_BOUNDS)/3600
    
    which_sleep_valid <- (out_mat[,11] == 1) & (out_mat[,3] <= PROP_OUTL_CAP_POST)
    time_sleep_valid <- sum(out_mat[which_sleep_valid, 1])/3600
  }
  
  # summary stats -- sleep
  sleep_p_outl <- c(0, 0, 0)
  sleep_p_conf <- c(0, 0)
  sleep_p_conf_outl <- c(0, 0)
  out_mad_sleep <- c(0, 0)
  out_mapd_sleep <- c(0, 0)
  out_mad_sleep_conf <- c(0, 0)
  out_mapd_sleep_conf <- c(0, 0)
  
  if(include_sleep) {
    out_mat_sleep <- out_mat[which_sleep_valid, c(3:9, 12:17)]
    valid_used_sleep <- 1 - out_mat_sleep[,1]
    
    sleep_p_outl <- as.numeric(apply(out_mat_sleep[,1:3], 2, mean))
    sleep_p_conf <- as.numeric(apply(out_mat_sleep[,4:5], 2, mean))
    sleep_p_conf_outl <- as.numeric(apply(out_mat_sleep[,6:7], 2, mean))
    
    out_mad_sleep <- c(mean(out_mat_sleep[,8]), 
                       sum(out_mat_sleep[,8]*valid_used_sleep)/sum(valid_used_sleep))
    
    out_mapd_sleep <- c(100*mean(out_mat_sleep[,10]), 
                        100*sum(out_mat_sleep[,10]*valid_used_sleep)/sum(valid_used_sleep))
    
    which_valid_sleep_conf <- out_mat_sleep[,5] != 0
    valid_used_sleep_conf <- out_mat_sleep[which_valid_sleep_conf, 5]
    
    out_mad_sleep_conf <- c(mean(out_mat_sleep[which_valid_sleep_conf, 11]), 
                            sum(out_mat_sleep[which_valid_sleep_conf, 11]*valid_used_sleep_conf)/sum(valid_used_sleep_conf))
    
    out_mapd_sleep_conf <- c(100*mean(out_mat_sleep[which_valid_sleep_conf, 13]), 
                             100*sum(out_mat_sleep[which_valid_sleep_conf, 13]*valid_used_sleep_conf)/sum(valid_used_sleep_conf))
    
  }
  
  
  #################################################################################################
  
  # final output list
  
  all_stats <- list(TIME_STATS = c(TOT_TIME_DURATION = round(time_tot, 2),
                                   TOT_TIME_VALID = round(time_tot_valid, 2),
                                   SLEEP_TIME_DURATION = round(time_sleep_tot, 2),
                                   SLEEP_TIME_VALID = round(time_sleep_valid, 2)),
                    OUTLIER_STATS = c(PROP_WINDOWS_USED = round(100*len(which_valid)/nrow(out_mat), 2),
                                      MEAN_PROP_OUTL_ALL = round(100*mean(out_mat[,3]), 2),
                                      MEAN_PROP_OUTL_REF = round(100*mean(out_mat[,4]), 2),
                                      MEAN_PROP_OUTL_Y = round(100*mean(out_mat[,5]), 2)),
                    CONFIDENCE_STATS = c(PROP_CONF = round(100*mean(out_mat[,6]), 2),
                                         PROP_CONF_OUTL_EXCL = round(100*mean(out_mat[,7]), 2),
                                         PROP_CONF_OUTL_REF = round(100*mean(out_mat[,8]), 2),
                                         PROP_CONF_OUTL_Y = round(100*mean(out_mat[,9]), 2)),
                    SLEEP_STATS = c(MEAN_PROP_OUTL_ALL = round(100*sleep_p_outl[1], 2),
                                    MEAN_PROP_OUTL_REF = round(100*sleep_p_outl[2], 2),
                                    MEAN_PROP_OUTL_Y = round(100*sleep_p_outl[3], 2),
                                    PROP_CONF = round(100*sleep_p_conf[1], 2),
                                    PROP_CONF_OUTL_EXCL = round(100*sleep_p_conf[2], 2),
                                    PROP_CONF_OUTL_REF = round(100*sleep_p_conf_outl[1], 2),
                                    PROP_CONF_OUTL_Y = round(100*sleep_p_conf_outl[2], 2)),
                    SUMMARY_STATS_ALL = c(MAD = out_mad[1], weightedMAD = out_mad[2],
                                          MAPD = out_mapd[1], weightedMAPD = out_mapd[2]),
                    SUMMARY_STATS_CONF = c(MAD = out_mad_conf[1], weightedMAD = out_mad_conf[2],
                                           MAPD = out_mapd_conf[1], weightedMAPD = out_mapd_conf[2]),
                    SUMMARY_STATS_SLEEP = c(MAD = out_mad_sleep[1], weightedMAD = out_mad_sleep[2],
                                            MAPD = out_mapd_sleep[1], weightedMAPD = out_mapd_sleep[2],
                                            MAD_CONF = out_mad_sleep_conf[1], weightedMAD_CONF = out_mad_sleep_conf[2],
                                            MAPD_CONF = out_mapd_sleep_conf[1], weightedMAPD_CONF = out_mapd_sleep_conf[2]))
  
  out_list <- list(WINDOW_OUTPUT = out_mat,
                   ALL_STATS = all_stats,
                   FORMATTED_STATS = list(SUMMARY_STATS = matrix(round(c(all_stats[[5]][c(2, 4)], 
                                                                         all_stats[[6]][c(2, 4)], 
                                                                         all_stats[[7]][c(2, 4, 6, 8)]), 2), 4, 2, byrow = T,
                                                                 dimnames = list(c("all", "confident", "sleep", "sleep_confident"),
                                                                                 c("wMAD", "wMAPD"))),
                                          TIME_STATS = matrix(all_stats[[1]], 2, 2, byrow = T,
                                                              dimnames = list(c("all", "sleep"), c("total", "valid"))),
                                          OUTLIER_STATS = all_stats[[2]][c(3, 4, 2)],
                                          SLEEP_OUTLIER_STATS = all_stats[[4]][c(2,3,1)],
                                          SLEEP_CONF_STATS = all_stats[[4]][c(4:7)]))
  
  
  # final output 
  
  print(out_list[[3]])
  invisible(out_list)
}