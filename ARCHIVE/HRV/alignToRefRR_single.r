alignToRefRR_single <- function(ref, rr1, rr2, ref_outl = NA,
                         ALIGN_FACTOR = 0, CUT_LAST = 2,
                         OUTPUT = TRUE, 
                         SHOW_OUTLIERS = TRUE,
                         DETECT_OUTLIERS = FALSE)
{
  
  
  # check if second set of measured RRIs to be aligned
  is_RR2 <- FALSE
  if(!missing(rr2))
    is_RR2 <- TRUE
  
  
  # cumulative time of RRIs
  ref_csum <- cumsum(ref)/1000 + ALIGN_FACTOR
  rr1_csum <- cumsum(rr1)/1000
  if(is_RR2) rr2_csum <- cumsum(rr2)/1000
  
  
  #################
  ##  alignment  ##
  #################
  
  k_rr1 <- calcOptLag(ref, ref_csum, rr1, rr1_csum)
  rr1_csum <- rr1_csum - k_rr1
  
  if(is_RR2) {
    k_rr2 <- calcOptLag(ref, ref_csum, rr2, rr2_csum)
    rr2_csum <- rr2_csum - k_rr2
  }
  else
    rr2_csum <- rr1_csum

  x_start <- max(ref_csum[1], rr1_csum[1], rr2_csum[1])
  x_stop <- min(tail(ref_csum, 1), tail(rr1_csum, 1), tail(rr2_csum, 1))
  
  which_rr <- function(rrs)
  {
    wrr <- which((rrs >= x_start) & (rrs <= x_stop))
    if(min(wrr) > 1) wrr <- c(min(wrr)-1, wrr)
    if(max(wrr) < len(rrs)) wrr <- c(wrr, max(wrr)+1)
    return(wrr)
  }  
  
  which_ref <- which_rr(ref_csum)
  which_rr1 <- which_rr(rr1_csum)
  if(is_RR2) which_rr2 <- which_rr(rr2_csum)
  
  xp0 <- ref_csum[which_ref]
  yp0 <- ref[which_ref]
  xp1 <- rr1_csum[which_rr1]
  yp1 <- rr1[which_rr1]
  if(is_RR2) {
    xp2 <- rr2_csum[which_rr2]
    yp2 <- rr2[which_rr2]
  }
  else
    yp2 <- yp1
  
  
  #########################
  ##  outlier detection  ##
  #########################
  
#   yp0_ma <- movingAve(yp0, 10)
#   yp0_bounds <- cbind(yp0_ma - 300, yp0_ma + 300)
#   yp0_bounds[yp0_bounds[,1] <= 350, 1] <- 350
#   yp0_bounds[yp0_bounds[,2] >= 1600, 2] <- 1600
#   yp0_outls <- (yp0 >= yp0_bounds[,2]) | (yp0 <= yp0_bounds[,1])
  
  # zPlot(cb(yp0, yp0_ma, yp0_bounds))
  
  if(DETECT_OUTLIERS || SHOW_OUTLIERS) {
    yp0_outl <- detectRROutliers(yp0, rr_thresh_upper = 1775)
    yp1_outl <- detectRROutliers(yp1, rr_thresh_upper = 1775)
    if(is_RR2) yp2_outl <- detectRROutliers(yp2, rr_thresh_upper = 1775)
    
    if(all(!is.na(ref_outl))) yp0_outl[ref_outl, 1] <- 1
  }  
  
  ref_good <- rep(TRUE, len(yp0))
  ref_o_used <- NULL
  if(all(!is.na(ref_outl))) {
    for(ro in ref_outl) {
      if(!is.na(ref_good[ro])) {
        ref_good[ro] <- FALSE
        ref_o_used <- c(ref_o_used, ro)
      }
    }
    
    # ref_good[ref_outl] <- FALSE
  }
  
  if(is.null(ref_o_used)) ref_outl <- NA
  
  #############
  ##  stats  ##
  ############# 
  
  mad_mat1 <- cb(yp0, yp0 - yp1[sapply(xp0, function(x) which.min(abs(x - xp1)))] )
  
  if(is_RR2) {
    mad_mat2 <- cb(yp0, yp0 - yp2[sapply(xp0, function(x) which.min(abs(x - xp2)))] )
    mad_mat3 <- cb(yp1, yp1 - yp2[sapply(xp1, function(x) which.min(abs(x - xp2)))] )
  }
  
  mad_mat1 <- mad_mat1[ref_good, ]
  mad_mat1 <- mad_mat1[1:(nrow(mad_mat1) - CUT_LAST),]
  
  if(is_RR2)  {
    mad_mat2 <- mad_mat2[ref_good, ]
    mad_mat2 <- mad_mat2[1:(nrow(mad_mat2) - CUT_LAST),]
  }  
  
  if(is_RR2) {
    MADs <- c(mean(abs(mad_mat1[,2])), mean(abs(mad_mat2[,2])), mean(abs(mad_mat3[,2])))
    MAPDs <- 100*c(mean(abs(mad_mat1[,2])/mad_mat1[,1]), 
                   mean(abs(mad_mat2[,2])/mad_mat2[,1]),
                   mean(abs(mad_mat3[,2])/mad_mat3[,1]))
  }
  else {
    MADs <- mean(abs(mad_mat1[,2]))
    MAPDs <- 100*mean(abs(mad_mat1[,2])/mad_mat1[,1])
  }
  
  ############
  ##  plot  ##
  ############ 

  # w(14, 8) #; par(mfrow = c(2,1), mar=c(4,4,1,1))
  
  xp0 <- xp0[1:(len(xp0) - CUT_LAST)]
  yp0 <- yp0[1:(len(yp0) - CUT_LAST)]
  xp1 <- xp1[1:(len(xp1) - CUT_LAST)]
  yp1 <- yp1[1:(len(yp1) - CUT_LAST)]
  if(is_RR2) {
    xp2 <- xp2[1:(len(xp2) - CUT_LAST)]
    yp2 <- yp2[1:(len(yp2) - CUT_LAST)]
  }
  
  
  ylm <- range(c(yp0, yp1, yp2))
  plot(xp0, yp0, type = "n", ann = F, ylim = c(max(400, ylm[1]), min(1500, ylm[2])))
  if(all(!is.na(ref_outl)))
    abline(v = seq(min(xp0[ref_o_used]), max(xp0[ref_o_used]), 0.1), lwd = 3, col = "grey")
  lines(xp0, yp0, lwd = 5, col = "black")
  lines(xp1, yp1, lwd = 3, col = "blue")
  if(is_RR2) lines(xp2, yp2, lwd = 2, col = "red")
  
  if(SHOW_OUTLIERS) {
    abline(v = xp0[yp0_outl[,1] == 1], lwd=3, lty=1, col="red")
    abline(v = xp1[yp1_outl[,1] == 1], lwd=3, lty=2, col="mediumseagreen")
    if(is_RR2) abline(v = xp2[yp2_outl[,1] == 1], lwd=2, col="red")
  }  
  
  
  title(xlab = "time (s)", ylab = "RRI (ms)")
  
  if(is_RR2) {
    if(all(!is.na(ref_outl)))
      legend("topright", lwd = 3, col = c("black", "grey", "blue", "red"), cex = 0.9, bg = "white",
             leg = c("Bioharness reference", "Bioharness outliers (section ignored)",
                     paste0("LifeQ (Fs=50Hz) | MAD = ", round(MADs[1],1), "ms | MAPD = ", round(MAPDs[1],2), "%"),
                     paste0("LifeQ (Fs=25Hz) | MAD = ", round(MADs[2],1), "ms | MAPD = ", round(MAPDs[2],2), "%")))
    if(!all(!is.na(ref_outl)))
      legend("topright", lwd = 3, col = c("black", "blue", "red"), cex = 0.9, bg = "white",
             leg = c("Bioharness reference",
                     paste0("LifeQ (Fs=50Hz) | MAD = ", round(MADs[1],1), "ms | MAPD = ", round(MAPDs[1],2), "%"),
                     paste0("LifeQ (Fs=25Hz) | MAD = ", round(MADs[2],1), "ms | MAPD = ", round(MAPDs[2],2), "%")))
  }
  
  # which.min(abs(xp0 - locator(1)$x))
  
  ########################################################################
  
#   lplot(mad_mat1[,2], lwd=2, col = "blue")
#   lines(mad_mat1[,2], lwd=2, col = "red")
  
  ########################################################################
  
  return(list(MADs = MADs, MAPDs = MAPDs, 
              ref_good = ref_good, 
              ref = yp0, rr1 = yp1))
}     
    





calcOptLag <- function(ref, ref_csum, rr, rrs)
{
  k_min_lag <- 0
  
  x_lim <- range(ref_csum)
  x_which <- which((rrs >= x_lim[1]) & (rrs <= x_lim[2]))
  
  
  # ref_len <- which(ref_csum >= tail(xp2, 1))[1]
  min_len <- min(len(ref), len(x_which))
  
  xp1 <- ref_csum[1:min_len]
  yp1 <- ref[1:min_len]
  xp2 <- (rrs[x_which])[1:min_len]
  yp2 <- (rr[x_which])[1:min_len]
  
  
  #   yp1_amend <- yp1[1:min_len]
  #   yp2_amend <- yp2[1:min_len]
  #   
  #   # calculate moving aves to use as proxies for outliers
  #   yp1_ma <- movingAve(yp1_amend, 25)
  #   yp2_ma <- movingAve(yp2_amend, 25)
  #   
  #   yp1_bounds <- cbind(yp1_ma - 300, yp1_ma + 300)
  #   yp1_bounds[yp1_bounds[,1] <= 400, 1] <- 400
  #   yp1_bounds[yp1_bounds[,2] >= 1600, 2] <- 1600
  #   
  #   yp2_bounds <- cbind(yp2_ma - 300, yp2_ma + 300)
  #   yp2_bounds[yp2_bounds[,1] <= 400, 1] <- 400
  #   yp2_bounds[yp2_bounds[,2] >= 1600, 2] <- 1600
  #   
  #   # replace outliers
  #   yp1_outls <- (yp1_amend >= yp1_bounds[,2]) | (yp1_amend <= yp1_bounds[,1])
  #   yp2_outls <- (yp2_amend >= yp2_bounds[,2]) | (yp2_amend <= yp2_bounds[,1])
  # 
  #   any_outl <- yp1_outls | yp2_outls
  #   sum_good <- sum(!any_outl)
  
  any_outl <- rep(F, min_len)
  
  # do a min median abs diff search over seq of lags
  lags <- seq(-(min_len/2), (min_len/2), by=0.1)
  
  msd_lags <- sapply(lags, function(k) {
    
    msd_vec <- ( yp1 - 
                   yp2[sapply(xp1 + k, function(x) which.min(abs(x - xp2)))] )[!any_outl]
    
    return( median(abs(msd_vec)) )
    
  })
  
  k_min <- which.min(msd_lags)
  k_min_lag <- lags[k_min]
  
  return(k_min_lag)
}