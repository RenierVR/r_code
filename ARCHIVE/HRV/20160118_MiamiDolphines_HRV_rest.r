source("../useful_funs.r")
source("./ERMADT_funs.r")

datlist <- getDataFromDir("D:/HealthQ/Data/HRV/MiamiDolphins/", head = T)

i <- 2

dat <- datlist[[i]]
dat <- dat[dat[,1] == 15, ]


grncur <- convertToCurrent3(dat[,3], dat[,11], dat[,15])
ambcur <- convertToCurrent3(dat[,4], dat[,11], dat[,16])


acc <- acc_res(dat[,7:8], 16)


grncur_adjusted <- removeCurrentDiscont2(grncur, dat[,11], dat[,15], dat[,19])
grncur_sub_adjusted <- removeCurrentDisconts(grncur - ambcur, dat[,11], dat[,11], dat[,15], dat[,16], dat[,19])


Fs <- 25
hrv_out <- ER_MA_DT_peaks_C(grncur_adjusted - ambcur, filt_t="FIRHAM", clip=4*Fs, output=T, interp=T, bf_fs=Fs,
                            check_mult_peaks_above_zero=T, post_filt_running_mean=F, print_output = T,
                            run_mean_prefilt_cap = 50)
rr1 <- hrv_out$RR
xf1 <- hrv_out$filt_signal
pks1 <- cb(hrv_out$peaks, hrv_out$peakvals)



rr_emb <- na.omit(dat[,30])
rr_emb <- rr_emb[c(1, which(diff(rr_emb) != 0) + 1)]


if(i == 1) {
  rr1_p <- rr1[-c(1:2)]
  rr_emb_p <- rr_emb
  ylm <- c(900, 1400)
}
if(i == 2) {
  rr1_p <- rr1[5:120]
  rr_emb_p <- rr_emb[1:115]
  ylm <- c(800, 1200)
}


w() ; lplot(cumsum(rr1_p)/1000, rr1_p, lwd = 2, col = "red", ylim = ylm, ylab = "RR (ms)", xlab = "time (s)")
lines(cumsum(rr_emb_p)/1000, rr_emb_p, lwd = 2, col = "blue")
legend("topright", lwd = 2, col = c("red", "blue"), leg = c("post-processed", "embedded"))


###################################################################################################
###################################################################################################


w()
lplot(cumsum(rr_emb_p)/1000, rr_emb_p, lwd = 3, col = "blue", ylim = c(800, 1200),
      ylab = "RR (ms)", xlab = "time (s)", main = "RR interval results (standing)")


###################################################################################################
###################################################################################################


tdmat <- rbind(tdomain_metrics(rr1_p, detectRROutliers(rr1_p)[,1])[[2]],
               tdomain_metrics(rr_emb_p, detectRROutliers(rr_emb_p)[,1])[[2]])
colnames(tdmat) <- c("sdnn", "sdsd", "rmssd", "pnn50", "score")


###################################################################################################
###################################################################################################


fs <- Fs
rr <- rr1
xf <- xf1
pks <- pks1


pks[,1] <- pks[,1]/fs
rr_csum <- cumsum(rr)/1000
rr_csum <- rr_csum + pks[1, 1]

rr_ot <- detectRROutliers(rr)[,1]


w() ; par(ask = T, mfrow = c(2, 1), mar = c(2, 4, 1, 1))
start <- 1 ; winsize <- 25 ; overlap <- winsize/2 ; j <- 0
while(TRUE){
  
  j = j+1
  plotind <- (overlap*(j-1)+start):(start-1+winsize + overlap*(j-1))
  
  if(max(plotind) > max(rr_csum))
    break
  else{
    
    grn_pind <- range(plotind)*fs
    grn_pind <- min(grn_pind):max(grn_pind)
    
    pks_plot <- pks[(pks[,1] >= min(plotind)) & (pks[,1] <= max(plotind)),]
    
    par(xpd = F)
    lplot(grn_pind/fs, xf[grn_pind], lwd = 2, col = "mediumseagreen",
          xlim = range(plotind), xlab = "", ylab = "green PPG")
    points(pks_plot, pch = 19, col = "forestgreen")
    
    
    ######################################################################
    
    which_rr <- (rr_csum >= min(plotind)) & (rr_csum <= max(plotind))
    xp <- rr_csum[which_rr]
    yp <- rr[which_rr]
    
    
    
    
    plot(xp, yp, type = "n", xlab = "time (s)", ylab = "RR (ms)",
         xlim = range(plotind), ylim = c(800, 1500))
    abline(v = rr_csum[rr_ot == 1], lwd = 2, col = "red", lty = 1)
    lines(xp, yp, lwd = 2, col = "blue")
    points(xp, yp, pch = 19, col = "blue", cex = 0.8)
    
    par(xpd = NA)
    abline(v = xp, lty = 2)
    
  }
}


