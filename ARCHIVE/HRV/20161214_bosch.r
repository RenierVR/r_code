source("../useful_funs.r")
source("../Rwrapper.r")
source("ERMADT_funs.r")

library(signal)



hr1hz_from_rr <- function(rr, rrv, max_jump=25, ma_order=2, alpha=0.5)
{
  rr_x <- cumsum(rr)/1000
  hr <- round(60/(rr/1000), 2)
  
  if(missing(rrv)) hr_outl <- numeric(0, len(hr))
  else hr_outl <- rrv
  
  # Compute 1Hz heart rate from RR intervals
  rr_x_c <- ceiling(rr_x)
  i_start <- which(rr_x_c >= 1)[1]
  hr_1hz <- rbind(c(rr_x_c[i_start], hr[i_start], hr_outl[i_start]))
  
  for(i in (i_start+1):len(rr_x)) {
    d <- rr_x_c[i] - rr_x_c[i-1]
    
    if(d == 0) {
      hr_1hz[nrow(hr_1hz), 2:3] <- c(hr[i], hr_outl[i])
    }
    
    else if(d == 1) 
      hr_1hz <- rbind(hr_1hz, c(rr_x_c[i], hr[i], hr_outl[i]))
    
    else if(d > 1){
      print(i)
      hr_1hz <- rbind(hr_1hz, cb((rr_x_c[i-1]+1):rr_x_c[i],
                                 c(rep(hr_1hz[nrow(hr_1hz), 2], d-1), hr[i]),
                                 c(rep(hr_1hz[nrow(hr_1hz), 3], d-1), hr_outl[i])))
    }
  }
  
  # Remove outliers from 1Hz HR
  hr2 <- hr_1hz[,2]
  outliers <- numeric(len(hr2))
  ma_tmp_lst <- numeric(len(hr2))
  
  for(i in (ma_order + 1):len(hr2)) {
    ma_tmp <- mean(hr2[(i - ma_order):(i-1)])
    ma_tmp_lst[i] <- ma_tmp
    
    if((abs(hr2[i]-ma_tmp) >= max_jump) | (hr_1hz[i,3] == 1)) {
      hr2[i] <- ma_tmp
      outliers[i] <- 1
    }
  }
  
  # Smooth 1Hz HR
  hr2_sm <- expSmooth(hr2, alpha=alpha)
  
  return(cb(hr_1hz[,1], hr2_sm, outliers))
  
}






#################################################
####  LOAD DATA
#################################################

datlist <- getDataFromDir("D:/HealthQ/Data/Bosch/Bosch_Data_201611/", dataE=T)
datlist_polar <- datlist[grep("polar", names(datlist))]
datlist_raw <- datlist[-grep("polar", names(datlist))]


# w() ; par(ask=T)
# for(i in 1:12) {
#   
#   # hz <- 1/diff(datlist_raw[[i]][,1])
#   # hz[is.infinite(hz)] <- 0
#   # 
#   # plot_g(hz, lwd=1, cex=0.7, ylab="Hz (from timestamp)", main=paste("Bosch data:", names(datlist_raw)[i]))
#   
#   hr <- datlist_raw[[i]][,2]
#   
#   zPlot(hr)
# }


#################################################
####  Get output from C-libs
#################################################


outlist <- vector("list", 12)
for(i in 1:12) {
  
  dat <- datlist_raw[[i]]
  
  y_rs <- cb(resample(dat[,2], 25/128),
             resample(dat[,3], 25/128),
             resample(dat[,4], 25/128),
             resample(dat[,5], 25/128),
             resample(dat[,6], 25/128))
  
  # y_rs_offset <- y_rs[5] - dat[5,2]
  # y_rs <- y_rs[-c(1:4)] - y_rs_offset
  
  y_rs <- y_rs[-c(1:4),]
  
  
  LQI_init(25)
  fdat <- LQI_formatData(cb(y_rs[,1:2], 0, 0, 10*y_rs[,3:5], 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
  
  out <- LQI_processData(fdat)
  out_acc <- out[[3]]
  out_rr <- out[[4]][out[[4]][,1] == 1, c(2:6)]
  out_rr[,4] <- out_rr[,4] + out_rr[,5]/1000
  
  outlist[[i]] <- list("RR"=out_rr[,-5],
                      "RESAMPLED"=y_rs,
                      "ACC_RES"=out_acc[,1])
  
  LQI_unload()
  
  catProgress(i, 12)  
}



#################################################
####  Plot RR and ACC
#################################################

w(18, 14) ; par(ask=T, mfrow=c(2,1), mar=c(4,4,2,1))
for(i in 1:12) {
  
  out <- outlist[[i]]
  out_rr <- cb(out$RR[,4]/25, out$RR[,1:2])
  out_rr_t <- out$RR[,4]
  
  out_acc <- cb(1:len(out$ACC_RES)/25, out$ACC_RES/12)[-c(1:100),]
  
  plot_g(out_rr[,1:2], main=paste("RR output: ", names(datlist_raw)[i]),
         xlim=c(0, max(out_rr[,1])), xlab="time (s)", ylab="RR interval (ms)",
         type="n", ylim=c(min(out_rr[,2]), min(max(out_rr[,2]), 5000)))
  abline(v=out_rr[out_rr[,3]==1, 1], col="red")
  lines(out_rr[,1:2], lwd=2, col="blue")
  
  lplot_g(out_acc, col="seagreen", xlim=c(0, max(out_rr[,1])), 
          xlab="time (s)", ylab="Acceleration (scaled G)")

}




#################################################
####  Plot RR and ACC and HR
#################################################

i <- 11
ref_offset <- c(4, 0, 10, 3, 0, -3,
                -5, -5, -3, -3, 7, 0)


w(14, 9) ; par(ask=F, mfrow=c(3,1), mar=c(0,5,0.5,4), oma=c(5,0,3,0))
for(i in 1:12) {
  
  out <- outlist[[i]]
  out_rr <- cb(out$RR[,4]/25, out$RR[,1:2])
  out_rr_t <- out$RR[,4]
  colnames(out_rr)[c(1,3)] <- c("Timestamp", "RR_Outlier")
  
  out_acc <- cb(1:len(out$ACC_RES)/25, out$ACC_RES/12)[-c(1:100),]
  
  
  ####  Calculate 1Hz HR
  hr = hr1hz_from_rr(rr=out_rr[,2], rrv=out_rr[,3], alpha=0.25)
  hr[,1] <- hr[,1] + out_rr[1,1]
  # hr[,1] <- floor(hr[,1])
  colnames(hr) <- c("Timestamp", "HR_from_RR", "HR_outlier")
  
  ####  Get reference HR
  ref_hr <- datlist_polar[[i]][,1]
  if(ref_offset[i] < 0) 
    ref_hr <- c(rep(0, abs(ref_offset[i])), ref_hr)
  if(ref_offset[i] >= 0) 
    ref_hr <- ref_hr[-c(1:ref_offset[i])]
  
  
  plot_g(out_rr[,1:2],
         xlim=c(0, max(out_rr[,1])), ylab="RR interval (ms)",
         type="n", ylim=c(min(out_rr[,2]), min(max(out_rr[,2]), 5000)), xaxt="n")
  abline(v=out_rr[out_rr[,3]==1, 1], col="yellow", lwd=2)
  lines(out_rr[,1:2], lwd=2, col="blue")
  legend("bottomleft", lwd=2, col=c("blue", "yellow"), bg="white",
         leg=c("RR intervals", "RR outliers"))
  title(main=paste("RR output:", names(datlist_raw)[i]), outer=T)
  
  lplot_g(out_acc, col="seagreen", xlim=c(0, max(out_rr[,1])), 
          ylab="Acceleration (scaled G)", xaxt="n")
  
  plot_g(hr[,1:2], type="n", ylab="HR (bpm)",
         xlim=c(0, max(out_rr[,1])), ylim=c(min(hr[,2]), max(hr[,2], ref_hr)))
  abline(v=hr[hr[,3]==1, 1], col="yellow", lwd=2)
  lines(ref_hr, lwd=3, col="red")
  lines(hr[,1:2], col="blue", lwd=2)
  legend("topleft", lwd=2, col=c("blue", "yellow", "red"), bg="white",
         leg=c("HR from LifeQ RR", "HR outliers", "HR from Polar"))
  title(xlab="time (s)", outer=T)
  
  #### Save output
  # savePlot(paste0("./RR_fig_", names(datlist_raw)[i], ".png"), "png")
  
  # write.table2(out_rr, file=paste0("./RR_output_", names(datlist_raw)[i], ".csv"), col.names=T)
  # write.table2(hr, file=paste0("./RR_HR_output_", names(datlist_raw)[i], ".csv"), col.names=T)
  
}



