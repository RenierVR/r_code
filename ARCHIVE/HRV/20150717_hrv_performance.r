source("../useful_funs.r")

datlist <- c(getDataFromDir("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/data/HRV_validation2/", head=T, ext="txt"),
             getDataFromDir("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/data/HRV_motion/", head=T, ext="txt"))



rroutl_acc_all <- NULL
# w() ; par(ask=T, mfrow=c(2,1), mar=c(2,2,2,2))
# for(i in (1:len(datlist))[-14]     ){
for(i in c(13,15,16)) {
  
  cout <- read.table(paste0("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/physcalc_out_", names(datlist)[i], ".txt"), sep=",")
  
  c_acc <- cout[,5:6]
  c_hrv <- cout[,10:14]
  
  c_rr <- c_hrv[,1]
  c_rroutl <- c_hrv[,2]
  
  start_ind <- (which(c_rr != 0)[1])
  c_rr <- c_rr[start_ind:len(c_rr)]
  c_rroutl <- c_rroutl[start_ind:len(c_rroutl)]
  
  rr_inds <- c(1, which(diff(c_rr) != 0)+1)
  c_rr <- c_rr[rr_inds]
  c_rroutl <- c_rroutl[rr_inds]
  
  print(paste(round(nrow(c_acc)/50 - sum(c_rr)/1000, 2), " || ", start_ind/50))
  
#   w()
#   lplot(ksmooth2(c_acc[,1], 100), lwd=2)
#   par(new=T) ; lplot(c_acc[,2], lwd=2, col="blue", ann=F, yaxt="n", xaxt="n")
#   axis(4)
  
#     # w()
#     lplot(cout[,3], lwd=2)
#     par(new=T) ; lplot(cout[,4], lwd=2, col="blue", ann=F, yaxt="n", xaxt="n")
#     axis(4)  
    
    
    
  # w()
#   lplot(cumsum(c_rr)/1000, c_rr, lwd=2, col="mediumseagreen")
#   points(cumsum(c_rr)/1000, c_rr, pch=19, col="mediumseagreen")
#   abline(v=cbind(cumsum(c_rr)/1000, c_rroutl)[c_rroutl==1,1], lwd=2, col="red")
  
  
  rroutl_t <- cbind(cumsum(c_rr)/1000, c_rroutl)[c_rroutl==1,1] + start_ind/50
  rroutl_t <- round(50*rroutl_t)
  rroutl_t <- sapply(rroutl_t, function(x) x+c(-25,25))
  
  if(len(rroutl_t) > 0) {
    rroutl_acc <- apply(rroutl_t, 2, function(x) mean(c_acc[x[1]:x[2], 1]))
    rroutl_acc_all <- c(rroutl_acc_all, round(rroutl_acc))
  }

}


rroutl_breaks <- seq(0, round(max(rroutl_acc_all), -2), by=1000)
rroutl_cut <- table(cut(rroutl_acc_all, breaks=rroutl_breaks))

rroutl_plot <- cbind(rroutl_breaks[-1], rroutl_cut)

w() ; lplot(rroutl_plot, lwd=2)





