source("improved_peaks_cubic_interp.r")

project="PeakPredictionDetection_project"
hrv_names <- c("20140715_1630_RenierVanRooyen_LifeQMojo10.2.16_Testing_WristMotion_HRV_Resting.txt",   # 300 - 1040
              "20140807_1600_KariSchoonbee_LifeQMojo11.1.9_Rest_Breathing.txt",                        # 40  - 580
              "20140818_1600_EugenePretorius_LifeQMojo11.1.15_Rest_HRV.txt",                           # 20  - 600
              "20140818_1630_BernardGreef_LifeQMojo11.1.15_Rest_HRV.txt",                              # 20  - 625
              "20140821_1345_VanZylVanVuuren_LifeQMojo11.1.12_LeftIndexFinger_Rest_HRV.txt",           # no ecg
              "20140821_1345_VanZylVanVuuren_LifeQMojo11.1.21_LeftWrist_Rest_HRV.txt",                 # no ecg
              "20140821_1345_VanZylVanVuuren_LifeQMojo11.2.1_RightIndexFinger_Rest_HRV.txt",           # no ecg
              "20140821_1420_MarcDeKlerk_LifeQMojo11.1.12_LeftIndexFinger_Rest_HRV.txt",               # no ecg
              "20140821_1420_MarcDeKlerk_LifeQMojo11.2.1_RightIndexFinger_Rest_HRV.txt",               # no ecg
              "20140821_1525_DirkBadenhorst_LifeQMojo11.1.12_LeftIndexFinger_Rest_HRV.txt",            # no ecg
              "20140821_1525_DirkBadenhorst_LifeQMojo11.1.21_LeftWrist_Rest_HRV.txt",                  # no ecg
              "20140821_1525_DirkBadenhorst_LifeQMojo11.2.1_RightIndexFinger_Rest_HRV.txt",            # no ecg
              "20140821_1620_TiaanVanDerMerwe_LifeQMojo11.1.12_LeftIndexFinger_Rest_HRV.txt",          # no ecg
              "20140821_1620_TiaanVanDerMerwe_LifeQMojo11.1.21_LeftWrist_Rest_HRV.txt",                # no ecg
              "20140821_1620_TiaanVanDerMerwe_LifeQMojo11.2.1_RightIndexFinger_Rest_HRV.txt",          # no ecg
              "20140822_1535_TheoVanStaden_LifeQMojo11.1.12_LeftWrist_Resting_HRV.txt",                # no ecg
              "20140822_1535_TheoVanStaden_LifeQMojo11.1.18_LeftFinger_Resting_HRV.txt",               # no ecg
              "20140822_1535_TheoVanStaden_LifeQMojo11.2.1_RightWrist_Resting_HRV.txt",                # no ecg
              "20140822_1535_TheoVanStaden_LifeQMojo11.2.2_RightFinger_Resting_HRV.txt",               # no ecg
              "20140826_1600_KoraHolm_LifeQMojo11.1.17_LeftWrist_Resting_HRV.txt",                     # 20  - 1000
              "20140826_1600_KoraHolm_LifeQMojo11.1.20_LeftFinger_Resting_HRV.txt",                    # 20  - 1000
              "20140826_1900_LianiDuPreez_LifeQMojo11.1.12_LeftWrist_Resting_HRV.txt",                 # no ecg
              "20140826_1900_LianiDuPreez_LifeQMojo11.2.1_RightWrist_Resting_HRV.txt",                 # 20  - 800
              "20140826_1900_LianiDuPreez_LifeQMojo11.2.2_RightFinger_Resting_HRV.txt",                # 20  - 800
              "20140827_TimVerschaeve_HRV_1121_RWrist0-parsed.txt",                                    # no ecg
              "20140827_TimVerschaeve_HRV_1122_RFinger0-parsed.txt",                                   # no ecg
              "20140827_TimVerschaeve_HRV_11112_LWrist0-parsed.txt",                                   # no ecg
              "20140827_TimVerschaeve_HRV_11118_LFinger0-parsed.txt")                                  # no ecg



i_ref   <- c(1:4, 20:21, 23:24)
i_range <- 50*matrix(c(300,1040, 40,580, 20,600, 20,625, 20,1000, 20,1000, 20,800, 20,800), nc=2, byrow=T)




i_index <- 1:length(hrv_names)
pd_out_list <- vector("list", length(i_index))
for(i in i_index){
  
  hrv_fname_pd <- paste0("~/HealthQ/C FILES/", project, "/output/out_pd_", hrv_names[i])
  hrv_fname_in <- paste0("~/HealthQ/C FILES/", project, "/output/out_init_", hrv_names[i])
  
  pd_out  <- read.table(hrv_fname_pd)[,1]
  pd_out  <- pd_out[pd_out != 0] + 1
  
  grn_out <- read.table(hrv_fname_in, sep=",")[,3]
  
  
  interp_pks <- getInterpPeaks(grn_out, pd_out)
  
  write.table(cbind(interp_pks), file=paste0("~/HealthQ/C FILES/PeakPredictionDetection_project/interpolated_peaks/ipk_", hrv_names[i]),
              quote=F, row.names=F, col.names=F)
  
  
}





################################################################################################
####################   TESTING C CODE AGAINST SIMILAR R (PSEUDO REAL-TIME)  ####################
################################################################################################

rout_list <- vector("list", len(hrv_names))
for(i in 1:len(hrv_names)){
  
  cat("File:", hrv_names[i], "\n")
  dat   <- read.table(paste0("~/HealthQ/C FILES/PeakPredictionDetection_project/data/hrv/", hrv_names[i]), sep=",")
  cat("Running R algorithm to obtain RR intervals.....\n")
  rout  <- ER_MA_DT_peaks_C(removeAGCJumps(dat[,3] - dat[,4])[-1], clip=0, output=F)
  
  cat("Reading C output containing RR intervals.......\n")
  cout <- read.table(paste0("~/HealthQ/C FILES/PeakPredictionDetection_project/output/out_pd_", hrv_names[i]))[,1]
  cout <- cout[cout != 0]
  
  rout_list[[i]] <- list(diff(rout), diff(cout))
  cat(i/len(hrv_names), "% completed\n")
  
  cat("\n-------------------------------------\n\n")
}


# save("rout_list", file="r_ermadt_out_list")

i <- 28
zPlot(cbind(rout_list[[i]][[1]][-1], rout_list[[i]][[2]]), c("R out", "C out"))




#####################################################################################################
####################   TESTING R (PSEUDO REAL-TIME) CODE AGAINST R/PYTHON (post processed)  ###########
#####################################################################################################

source("ERMADT_funs.r")


rout_list <- vector("list", len(i_ref))
for(i in i_ref){
  
  cat("File:", hrv_names[i], "\n")
  dat   <- read.table(paste0("~/HealthQ/C FILES/PeakPredictionDetection_project/data/hrv/", hrv_names[i]), sep=",")
  cat("Running R algorithms to obtain RR intervals.....\n")
  
  
  grn <- removeAGCJumps(dat[,3] - dat[,4])
  grn <- grn[i_range[which(i==i_ref),1]:i_range[which(i==i_ref),2]]
  
#   r1 <- ER_MA_DT_peaks(grn, w1=3, w2=45, bf_ord=2, bf_f1=0.5, bf_f2=8, bf_fs=50, beta=0.2)
  r1  <- read.table(paste0("~/HealthQ/LIFEQ_TOOLS/hrv/RR_output/kari_peaks_", hrv_names[i]))[,1]
  r2  <- ER_MA_DT_peaks_C(grn, filt_type="FIR50")
  
#   rout_list[[which(i==i_ref)]] <- list(diff(r1), diff(r2))
  rout_list[[which(i==i_ref)]] <- list(r1, diff(r2*0.02)*1000)
  cat(100*which(i==i_ref)/len(i_ref), "% completed\n")
  
  cat("\n-------------------------------------\n\n")
}


# save("rout_list", file="r_ermadt_out_list")

i <- 1
r1 <- rout_list[[i]][[1]] ;  r2 <- rout_list[[i]][[2]][-c(1:4)]
zPlot(cbind(r1[1:min(len(r1), len(r2))], r2[1:min(len(r1), len(r2))]), 
      c("Python PP", "R RT"), col_f=c("blue", "red"))





################################################################################################
####################   TESTING C CODE AGAINST PYTHON (POST-PROCESSED)  #########################
################################################################################################


rout_list <- vector("list", len(i_ref))
for(i in i_ref){
  
  cat("File:", hrv_names[i], "\n")
  cat("Reading Python output.....\n")
  r1  <- read.table(paste0("~/HealthQ/LIFEQ_TOOLS/hrv/RR_output/kari_peaks_", hrv_names[i]))[,1]
  
  cat("Reading C output.....\n")
  cout <- read.table(paste0("~/HealthQ/C FILES/PeakPredictionDetection_project/output/out_pd_", hrv_names[i]))[,1]
  r2   <- cout
#   r2 <- unique(cout[cout != 0])
  
  rout_list[[which(i==i_ref)]] <- list(r1, r2)
  cat(100*which(i==i_ref)/len(i_ref), "% completed\n")
  
  cat("\n-------------------------------------\n\n")
}


# save("rout_list", file="r_ermadt_out_list")

i <- 2
r1 <- rout_list[[i]][[1]] ;  r2 <- rout_list[[i]][[2]][-c(1:13)]
zPlot(cbind(r1[1:min(len(r1), len(r2))], r2[1:min(len(r1), len(r2))]), 
      c("Python PP", "C RT"), col_f=c("blue", "red"))





################################################################################################
####################   TESTING C CODE AGAINST C CODE  #########################
################################################################################################


c_old_list <- vector("list", len(i_ref))
for(i in i_ref){
  
  cat("File:", hrv_names[i], "\nReading C output.....\n")
  cout <- read.table(paste0("~/HealthQ/C FILES/PeakPredictionDetection_project/output/out_pd_", hrv_names[i]), sep=",")
  
  c_old_list[[which(i==i_ref)]] <- cout
  cat(100*which(i==i_ref)/len(i_ref), "% completed\n\n-------------------------------------\n\n")
}

################################################

c_new_list <- vector("list", len(i_ref))
for(i in i_ref){
  
  cat("File:", hrv_names[i], "\nReading C output.....\n")
  cout <- read.table(paste0("~/HealthQ/C FILES/PeakPredictionDetection_project/output/out_pd_", hrv_names[i]), sep=",")
  
  c_new_list[[which(i==i_ref)]] <- cout
  cat(100*which(i==i_ref)/len(i_ref), "% completed\n\n-------------------------------------\n\n")
}


j <- 2
for(i in 1:8)
  print(c(all(c_old_list[[i]][,j] == c_new_list[[i]][,j])))


w()
par(ask=T)
for(i in 1:8){
  c_old <- c_old_list[[i]]
  c_new <- c_new_list[[i]]

  lplot(c_old - c_new)
}



i <- 2
r1 <- c_old_list[[i]][-c(1:1)] ;  r2 <- c_new_list[[i]][-c(1:1)]
zPlot(cbind(r1[1:min(len(r1), len(r2))], r2[1:min(len(r1), len(r2))]), 
      c("C OLD", "C NEW"), col_f=c("blue", "red"))



######################################################################################
########################   TESTING RR OUTLIER DETECTION   ############################
######################################################################################


rr1 <- c_old_list[[6]]
rrout1 <- detectRROutliers(rr1)

# rr2 <- c_new_list[[6]]
# rr2 <- rr2[rr2 != 0]
# rrout2 <- detectRROutliers(rr2)



rr <- cbind(rr1, rrout1[,1], rrout1[,2])
rr <- rr[-c(1,nrow(rr)),]

###############################################################################

c_out <- read.table("~/HealthQ/C FILES/PeakPredictionDetection_project/out_rr.txt", sep=",")

all(rr[,1] == c_out[,1])
all(rr[,2] == c_out[,2])
all(rr[,3] == c_out[,3])


zPlot(cbind(rr[,3], c_out[,3]), c("R out", "C out"))




w() ; par(ask=T, mfrow=c(2,1), mar=c(3,3,1,1))
for(i in i_ref){
  dat <- read.table(paste0("~/HealthQ/C FILES/PeakPredictionDetection_project/data/hrv/", hrv_names[i]), sep=",")
  grn <- dat[,3] - dat[,4]
  acc <- acc_res(dat[,5:7])
  
  lplot(acc, main=paste(i))
  lplot(grn)
}




################################################################################################
############################        TEST TIME DOMAIN METRICS         ###########################
################################################################################################



# j <- 1
# cbind(hrv_out[,j], hrv_out_b[,j])
# 
# for(j in 4:8)
#   print(all(hrv_out[,j] == hrv_out_b[,j]))

hrv_out <- read.table("~/HealthQ/C FILES/PeakPredictionDetection_project/hrv_metric_results.txt", sep=",")
hrv_out <- hrv_out[,-ncol(hrv_out)]

all(hrv_out_b == hrv_out)



for(j in 1:3){
  
  hrv1 <-  hrv_out_b[,j]/1000
  hrv2 <- hrv_out[,j]/500
  
  cat("\n", paste(round(100*hrv2/hrv1,1), collapse=" | "), "\n\n\n")
  
}


out_ls <- read.table("~/HealthQ/C FILES/PeakPredictionDetection_project/out_ls.txt", sep=",")

all(out_ls_b[-500,] == out_ls)

################################################################################################
################################################################################################
################################################################################################


cout <- read.table(paste0("~/HealthQ/C FILES/PeakPredictionDetection_project/output/out_pd_", hrv_names[i]), sep=",")
r2 <- cout[cout != 0]

rr <- cout[cout[,1] != 0,]
all(out[out[,2] == 0,1] == cout)


out <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/out_rr.txt", sep=",")


mojo <- read.table("~/HealthQ/rendier RR debug", sep=",")
mojo <- mojo[,-ncol(mojo)]

write.table(cbind(mojo[,1:2], matrix(0,nrow(mojo),3)), file="mojo_rr_02.txt", sep=",", 
            quote=F, row.names=F, col.names=F)


cout <- read.table(paste0("~/HealthQ/C FILES/PeakPredictionDetection_project/output/out_pd_", "mojo_rr_02.txt"), sep=",")
zPlot(cbind(mojo[,3], cout[,2]), c("mojo", "pc"))


ctest <- read.table("~/HealthQ/C FILES/PeakPredictionDetection_project/out_testing.txt", sep=",")

j <- 5
all(round(ctest[,j]-mojo[,j+3], 1) == 0)
    
    
zPlot(ctest[,j]-mojo[,j+3])




################################################################################################
################################################################################################
################################################################################################





