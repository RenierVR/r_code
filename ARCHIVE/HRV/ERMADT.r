source("../movingAve.r")
source("../RLS/RLS_wiki.r")
source("improved_peaks_cubic_interp.r")
source("ERMADT_funs.r")





xx <- green[-(1:20000)]
pks_all <- ER_MA_DT_peaks(xx, output_all=T, beta=0.05)
pks <- pks_all$peaks


windows(14,7)
par(ask = TRUE, mar=c(2,4,2,2))
j = 0
while(TRUE){
  j = j+1
  plotind <- (overlap*(j-1)+start):(start-1+winsize + overlap*(j-1))
  if(max(plotind) > length(xx))
    break
  else{

#     plot(plotind, pks_all$blocks[plotind], col="purple", ann=F, yaxt="n", xaxt="n", type="l")
#     par(new=T)
#     plot(plotind, pks_all$ma_peak[plotind], col="blue", ann=F, yaxt="n", xaxt="n", type="l")
#     #par(new=T)
#     lines(plotind, pks_all$thr1[plotind], col="orange")
#     par(new=T)
    plot(plotind, xx[plotind], type="l", col="limegreen", lwd=2)
    points(pks, xx[pks], col="red", pch=19)
  }
}

######################################################################################
######################################################################################
######################################################################################



dat <- read.table("~/HealthQ/Data/HRV_dat/EugeneHRV_new/20140818_1600_EugenePretorius_LifeQMojo11.1.15_Rest_HRV.txt", sep=",", head=T)
grn <- dat[,3] - dat[,4]

grn <- removeAGCJumps(grn, 200)




pks_all <- ER_MA_DT_peaks(grn, output_all=F, beta=0.02, w1=3, w2=45, bf_stime=150, interp=F)


w()
lplot(10000:11000, pks_all$filt_signal[10000:11000])
points(pks_all$peaks, pks_all$filt_signal[pks_all$peaks], pch=19, cex=0.6)


pks_diff <- (diff(pks_all[-1]/50)*1000)[2:557]



kari_pks <- read.table("C:/Users/reniervr/PycharmProjects/Py_GettingStarted/kari_peaks.csv")[,1]
kari_diff <- diff(kari_pks)*1000


w() ; lplot(pks_diff[-c(1:2)])
lines(kari_diff, col="red")


zPlot(cbind(pks_diff,kari_diff), yn=c("renier", "kari"))



######################################################################################
######################################################################################
######################################################################################


dat <- read.table(paste0("~/HealthQ/C FILES/PeakPredictionDetection_project/data/hrv/", hrv_names[i]), sep=",")
dat <- dat[seq(2,nrow(dat),2),]


ctest <- read.table("~/HealthQ/C FILES/PeakPredictionDetection_project/out_testing.txt", sep=",")
colnames(ctest) <- c("BLANK_SUB", "RUNMEAN_SUB", "FILTERED", "MA1", "MA2")

x <- dat[,3] - dat[,4]

##########################
## test blank subtraction
##########################
all(x == ctest[,1])


################################
## test running mean subtration
################################
x    <- removeAGCJumps(x)
x[1] <- (dat[,3]-dat[,4])[1]
xmean <- x[1]
for(i in 2:len(x))
  xmean <- c(xmean, xmean[i-1]*((i-1)/i) + x[i]*(1/i))
x <- x-xmean

all(round(x-ctest[,2],1) == 0)

# w() ; lplot(x-ctest[,2])
# zPlot(cbind(x, ctest[,2]))


###################
## test filtering
###################
x <- ctest[,2]
x <- FIRfilt(x, 25)


all(round(x-ctest[,3],2) == 0)

|# w() ; lplot(x-ctest[,3])


#############
## test MA 1
#############
LEN_ADJUST <- 23
# LEN_ADJUST <- 45

y        <- ctest[,3]
y[y < 0] <- 0
y        <- y^2

yy <- movingAve(y, 3, 3)
yy <- yy[-c(1:floor(LEN_ADJUST/2))]
cc <- (ctest[,4])[-c(1:LEN_ADJUST)]
if(len(yy) > len(cc))
  yy <- yy[1:len(cc)]
if(len(yy) < len(cc))
  cc <- cc[1:len(yy)]


all(round(yy-cc,0) == 0)

# w() ; lplot(yy-cc)
# zPlot(cbind(yy,cc))


#############
## test MA 2
#############
LEN_ADJUST <- 23
# LEN_ADJUST <- 45

y        <- ctest[,3]
y[y < 0] <- 0
y        <- y^2

yy <- movingAve(y, LEN_ADJUST, 3)
yy <- yy[-1]
cc <- (ctest[,5])[-c(1:LEN_ADJUST)]
if(len(yy) > len(cc))
  yy <- yy[1:len(cc)]
if(len(yy) < len(cc))
  cc <- cc[1:len(yy)]


all(round(yy-cc,1) == 0)

w() ; lplot(yy-cc)
zPlot(cbind(yy,cc))





######################################################################################
######################################################################################
######################################################################################




