source("../useful_funs.r")
source("../SpO2/sp02_funs.r")


fpath1 <- "D:/HealthQ/Data/HRV/201502 HRV Validation/20150205/"

fpath2 <- paste0("HRV_", c("Andre", "Kari", "Marc", "Nic", "Renier", "Tiaan"))
fpath3 <- paste0(fpath1, fpath2, "/")


for(i in 1:len(fpath3)){
  m5113_list <- getDataFromDir(paste0(fpath3[i], "Mojo 5113/"), dataExtract=F, ret="full")
  dat <- read.table(m5113_list[grep("Unknown", m5113_list)], head=T, sep=",")
  dat <- dat[dat[,1] != 0,]
  writeMojoFormat(dat, paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/data/HRV_validation2/20150205_mojo5113_", fpath2[i], ".txt"))
  
  m5106_list <- getDataFromDir(paste0(fpath3[i], "Mojo 5106/"), dataExtract=F, ret="full")
  dat <- read.table(m5106_list[grep("Unknown", m5106_list)], head=T, sep=",")
  dat <- dat[dat[,1] != 0,]
  writeMojoFormat(dat, paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/data/HRV_validation2/20150205_mojo5106_", fpath2[i], ".txt"))
}



offsets <- matrix(c(6, 1, 2, 8, 2, 3,
                    5, 1, 2, 14, 1, 2,
                    4, 1, 2, 3, 1, 2,
                    5, 1, 2, 8, 1, 2,
                    17, 1, 1, 18, 1, 2,
                    15, 1, 2, 16, 1, 2), ncol=6, byrow=T)


# for(i in 1:len(fpath3)){

i <- 6


  m5113_list <- getDataFromDir(paste0(fpath3[i], "Mojo 5113/"), dataExtract=F, ret="full")
  m5113_dat <- read.table(m5113_list[grep("Unknown", m5113_list)], head=T, sep=",")
  m5113_dat <- m5113_dat[m5113_dat[,1] != 0,]
  m5113_bhrr <- diff(read.table(paste0(fpath3[i], "Mojo 5113/ecg_peaks.txt"))[,1])
  m5113_rr <- read.table(paste0(fpath3[i], "Mojo 5113/mojo_rr.txt"))[,1]
  m5113_crr <- read.table(paste0(fpath3[i], "Mojo 5113/out_rr_volt.txt"), sep=",")
  
  m5106_list <- getDataFromDir(paste0(fpath3[i], "Mojo 5106/"), dataExtract=F, ret="full")
  m5106_dat <- read.table(m5106_list[grep("Unknown", m5106_list)], head=T, sep=",")
  m5106_dat <- m5106_dat[m5106_dat[,1] != 0,]
  m5106_bhrr <- diff(read.table(paste0(fpath3[i], "Mojo 5106/ecg_peaks.txt"))[,1])
  m5106_rr <- read.table(paste0(fpath3[i], "Mojo 5106/mojo_rr.txt"))[,1]
  m5106_crr <- read.table(paste0(fpath3[i], "Mojo 5106/out_rr_current.txt"), sep=",")
  
  
  w() ; par(mfrow=c(2,1), mar=c(2,3,0,1))
  bhplot <- m5113_bhrr[-c(1:offsets[i,1])]
  rrplot <- m5113_rr[-c(1:offsets[i,2])]
  crrplot <- m5113_crr[-c(1:offsets[i,3]),1]
  lplot(cumsum(bhplot), bhplot, lwd=3, ylim=c(450,1450), xlim=range(cumsum(rrplot)))
  lines(cumsum(rrplot), rrplot, lwd=2, col="blue")
  lines(cumsum(crrplot), crrplot, lwd=2, col="red")
  
  bhplot <- m5106_bhrr[-c(1:offsets[i,4])]
  rrplot <- m5106_rr[-c(1:offsets[i,5])]
  crrplot <- m5106_crr[-c(1:offsets[i,6]),1]
  lplot(cumsum(bhplot), bhplot, lwd=3, ylim=c(450,1450), xlim=range(cumsum(rrplot)))
  lines(cumsum(rrplot), rrplot, lwd=2, col="blue")
  lines(cumsum(crrplot), crrplot, lwd=2, col="red")
  
  
# }






################################################################################################
################################################################################################
################################################################################################
################################################################################################





offsets <- matrix(c(17, 13, 8, 3, 600, 1300,
                    5, 2, 16, 4, 700, 1400,
                    5, 4, 8, 9, 600, 1400,
                    5, 3, 7, 3, 600, 1200,
                    17, 1, 19, 3, 700, 1300,
                    15, 2, 16, 3, 500, 1100), ncol=6, byrow=T)




cor_vec1 <- NULL ; cor_vec2 <- NULL
mae_vec1 <- mae_vec2 <- NULL
mpe_vec1 <- mpe_vec2 <- NULL
for(i in 1:6){

m5113_bhrr <- diff(read.table(paste0(fpath3[i], "Mojo 5113/ecg_peaks.txt"))[,1])
# m5113_crr_v <- read.table(paste0(fpath3[i], "Mojo 5113/out_rr_volt.txt"), sep=",")
m5113_crr_c <- read.table(paste0(fpath3[i], "Mojo 5113/out_rr_current.txt"), sep=",")[,1]

m5106_bhrr <- diff(read.table(paste0(fpath3[i], "Mojo 5106/ecg_peaks.txt"))[,1])
# m5106_crr_v <- read.table(paste0(fpath3[i], "Mojo 5106/out_rr_volt.txt"), sep=",")
m5106_crr_c <- read.table(paste0(fpath3[i], "Mojo 5106/out_rr_current.txt"), sep=",")[,1]


w() 
bhplot <- m5113_bhrr[-c(1:offsets[i,1])]
crrplot <- m5113_crr_c[-c(1:offsets[i,2])]
minlen <- min(len(bhplot), len(crrplot)) ; bhplot <- bhplot[1:minlen] ; crrplot <- crrplot[1:minlen]

lplot(cumsum(bhplot), bhplot, lwd=4, ylim=offsets[i,5:6], 
      xlim=c(1,sum(crrplot)), xaxt="n", col="red", ann=F)
lines(cumsum(crrplot), crrplot, lwd=3, col="forestgreen", lty=1)
axis(1, at=pretty(cumsum(bhplot)), labels=pretty(cumsum(bhplot))/1000)
legend("topleft", cex=0.8, lwd=2, col=c("red", "forestgreen"), bg="white", leg=c("ECG reference", "LifeQ"))
title(xlab="time (s)", ylab="interval length (ms)", 
      main=paste(substring(fpath2[i], 5, nchar(fpath2[i])), "HRV test 1"))

savePlot(file=paste0(fpath1, "RR_PLOTS/hrv_plot_", substring(fpath2[i], 5, nchar(fpath2[i])), "1_tachogram"), "png")


w() ; lplot(bhplot, lwd=4, ylim=offsets[i,5:6], col="red", ann=F)
lines(crrplot, lwd=3, col="forestgreen", lty=1)
legend("topleft", cex=0.8, lwd=2, col=c("red", "forestgreen"), bg="white", leg=c("ECG reference", "LifeQ"))
title(xlab="index", ylab="interval length (ms)", 
      main=paste(substring(fpath2[i], 5, nchar(fpath2[i])), "HRV test 1"))

savePlot(file=paste0(fpath1, "RR_PLOTS/hrv_plot_", substring(fpath2[i], 5, nchar(fpath2[i])), "1_rrindex"), "png")

write.table2(cbind(bhplot, crrplot), 
             file=paste0(fpath1, "RR_OUTPUT/hrv_rr_", substring(fpath2[i], 5, nchar(fpath2[i])), "1.csv"))



cor_vec1 <- c(cor_vec1, cor(bhplot, crrplot))
mae_vec1 <- c(mae_vec1, mean(abs(bhplot-crrplot)))
mpe_vec1 <- c(mpe_vec1, 100*mean((crrplot-bhplot)/bhplot))


##################################################

w() 
bhplot <- m5106_bhrr[-c(1:offsets[i,3])]
crrplot <- m5106_crr_c[-c(1:offsets[i,4])]
minlen <- min(len(bhplot), len(crrplot)) ; bhplot <- bhplot[1:minlen] ; crrplot <- crrplot[1:minlen]

lplot(cumsum(bhplot), bhplot, lwd=4, ylim=offsets[i,5:6], 
      xlim=c(1,sum(crrplot)), xaxt="n", col="red", ann=F)
lines(cumsum(crrplot), crrplot, lwd=3, col="forestgreen", lty=1)
axis(1, at=pretty(cumsum(bhplot)), labels=pretty(cumsum(bhplot))/1000)
legend("topleft", cex=0.8, lwd=2, col=c("red", "forestgreen"), bg="white", leg=c("ECG reference", "LifeQ"))
title(xlab="time (s)", ylab="interval length (ms)", 
      main=paste(substring(fpath2[i], 5, nchar(fpath2[i])), "HRV test 2"))

savePlot(file=paste0(fpath1, "RR_PLOTS/hrv_plot_", substring(fpath2[i], 5, nchar(fpath2[i])), "2_tachogram"), "png")


w() ; lplot(bhplot, lwd=4, ylim=offsets[i,5:6], col="red", ann=F)
lines(crrplot, lwd=3, col="forestgreen", lty=1)
legend("topleft", cex=0.8, lwd=2, col=c("red", "forestgreen"), bg="white", leg=c("ECG reference", "LifeQ"))
title(xlab="index", ylab="interval length (ms)", 
      main=paste(substring(fpath2[i], 5, nchar(fpath2[i])), "HRV test 2"))

savePlot(file=paste0(fpath1, "RR_PLOTS/hrv_plot_", substring(fpath2[i], 5, nchar(fpath2[i])), "2_rrindex"), "png")

write.table2(cbind(bhplot, crrplot), 
             file=paste0(fpath1, "RR_OUTPUT/hrv_rr_", substring(fpath2[i], 5, nchar(fpath2[i])), "2.csv"))



cor_vec2 <- c(cor_vec2, cor(bhplot, crrplot))
mae_vec2 <- c(mae_vec2, mean(abs(bhplot-crrplot)))
mpe_vec2 <- c(mpe_vec2, 100*mean((crrplot-bhplot)/bhplot))

graphics.off()
}



