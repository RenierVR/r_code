source("../useful_funs.r")
source("./ERMADT_funs.r")
source("./alignToRefRR.r")
source("./alignToRefRR_single.r")



rr_resample <- function(x, Fs_new, Fs_old=50)
{
  x_new <- Fs_old*x/1000
  x_new <- (1000*x_new)/Fs_new
  return(x_new)
}


###################################################################################################
##########################       HRV BATCH PROCESSING - NOMOTION       ############################
###################################################################################################

#########################################
## consolidation of no motion HRV data ##
#########################################

fpath <- "D:/HealthQ/Data/HRV/HRV BATCH NOMOTION/"



# datlist1 <- getDataFromDir(paste0(fpath, "2014 validation/"), head=T)

# reflist1 <- getDataFromDir(paste0(fpath, "2014 validation/reference/"), ext="txt", head=T)
# reflist1 <- lapply(reflist1, function(x) diff(x[,1]))

###################################################################################################

# datlist2 <- getDataFromDir(paste0(fpath, "2015 informal testing/"), head=T)
# reflist2 <- as.list(rep(0, len(datlist2)))

###################################################################################################

datlist3 <- getDataFromDir(paste0(fpath, "201502 validation/"), head=T)
reflist3 <- getDataFromDir(paste0(fpath, "201502 validation/reference/"), ext="txt")
reflist3 <- lapply(reflist3, function(x) cbind(NA, diff(x[,1])))
reflist3 <- c(reflist3, getDataFromDir(paste0(fpath, "201502 validation/reference/"), head=T))
reflist3 <- reflist3[order(names(reflist3))]
reflist3 <- lapply(reflist3, function(x) x[,2])

###################################################################################################

# datlist <- c(datlist1, datlist2, datlist3)
# reflist <- c(reflist1, reflist2, reflist3)

datlist <- c(datlist3)
reflist <- c(reflist3)



# datlist <- getDataFromDir(paste0(fpath, "2014 validation/"), dataE=F, ret="full")
# for(i in 1:len(datlist)) {
#   dat <- read.table(datlist[i], head=T, sep=",")
#   dat <- dat[dat[,1] != 0,]
#   dat <- dat[,c(1:7, 9:17, 8, 18:25)]
#   dat <- cbind(dat[-1,1:7], dat[1:(nrow(dat)-1),8:ncol(dat)])
#   dat[,10:13] <- 10*dat[,10:13]
#   write.table2(dat, file=datlist[i], sep=", ", col.names=T)
# }



#####################
## R: HRV algo run ##
#####################

i <- 1
batch_plot <- FALSE

RRlist_R <- vector("list", len(datlist))

for(i in 1:len(datlist)) {
  
  Fs <- 50
  dat <- datlist[[i]]
  
  dat <- dat[dat[,1] != 0,]
  dat <- dat[10:nrow(dat),]
  curr1 <- curr2 <- matrix(0, nr=nrow(dat), nc=2)
  for(j in 1:1){
    if(j < 3){
      Rf <- dat[,8]
      led <- dat[,14]
    }
    else{
      Rf <- dat[,9]
      led <- dat[,j+12]
    }
    
    safe_term <- 0
    if(j == 2){
      safe_term <- 1
      led <- numeric(nrow(dat))
    }
    
    dat_tmp <- dat[,j] 
    isub_tmp <- dat[,9+j]
    curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
    curr2[,j] <- removeCurrentDiscont2(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e8)  
    # curr3[,j] <- removeCurrentDiscont2_m(curr1[,j]+safe_term, Rf, isub_tmp, led, 1e8)[[1]]
  }
  
  grn <- curr2[,1]
  acc <- acc_res(dat[,5:7])
  

  hrv_out <- ER_MA_DT_peaks_C(grn, filt_t="FIRHAM", clip=4*Fs, output=T, interp=T, bf_fs=Fs,
                              check_mult_peaks_above_zero=T, post_filt_running_mean=F,
                              run_mean_prefilt_cap = 50)
  rr1 <- hrv_out$RR
  xf1 <- hrv_out$filt_signal
  
  
  # Fs <- 25
  # grn <- grn[seq(1, len(grn), 2)]
  hrv_out <- ER_MA_DT_peaks_C(grn, filt_t="FIRHAM", clip=4*Fs, output=T, interp=T, bf_fs=Fs,
                              check_mult_peaks_above_zero=T, post_filt_running_mean=F,
                              run_mean_prefilt_cap = 100)
  rr2 <- hrv_out$RR
  xf2 <- hrv_out$filt_signal
  
  # hrv_ERMADT_plot(hrv_out)
  
#   ff <- function(x)
#   {
#     y <- FIRfilt(x, Fs=Fs)
#     y <- FIRfilt_50hz_LP_hamming_20_6hz(y)
#     return(y)
#   }
#   hrv_out <- ER_MA_DT_peaks_C(grn, filt_t="FUN", filt_f=ff, clip=4*Fs, output=T, interp=T, bf_fs=Fs)
#   rr2 <- hrv_out$RR
#   xf2 <- hrv_out$filt_signal
  
  
  if(batch_plot)
    # zPlot(cbind(xf1, xf2))
    zPlot(cbind(xf1[seq(1, len(xf1), 2)], xf2))
  
  
  
  RRlist_R[[i]] <- list(rr1, rr2)
  # RRlist_R[[i]] <- rr1
  
  catProgress(i, len(datlist))
}




######################################
## 50hz vs 25hz with auto alignment ##
######################################

i_used <- c(1, 4, 5, 9, 11, 17, 18, 20, 21, 22, 24, 29, 34)
i_used <- c(i_used, 2, 8, 13, 25, 26, 27, 28, 32, 33)


i_factor <- matrix(c(1, 2, 4, 5, 8, 9, 11, 13, 17, 18, 20, 21, 22, 
                     24, 25, 26, 27, 28, 29, 32, 33, 34,
                     49.7, 50.1, 50, 49.2, 49, 49.9, 49.3, 50, 
                     50.1, 49.5, 50.4, 49.6, 49.7, 49.8, 50.1, 
                     49.7, 49, 49.2, 49.6, 50, 49.4, 49.8),
                   nrow = 2, byrow = T)
fs_factor <- rep(50, max(i_used))
fs_factor[i_factor[1,]] <- i_factor[2,]


# w(14, 7) 
j <- 0
out_mat <- matrix(0, len(i_used), 6)
rr_good_all <- NULL
for(i in sort(i_used)) {
  
  # i <- sort(i_used)[1]
  j <- j+1
  
  
  ref <- reflist[[i]]
  rr1 <- RRlist_R[[i]][[1]]
  rr2 <- RRlist_R[[i]][[2]]
  
  ref_outl <- NA
  
  if(i == 2)
    ref_outl <- c(52, 53, 54)
  if(i == 8) {
    ref <- ref[-c(1:12)]
  }
  if(i == 9) {
    ref <- ref[-c(1:15)]
  }
  if(i == 10) {
    ref <- ref[-c(1:25)]
  }
  if(i == 13) {
    ref <- ref[-c(1:85)]
    rr1 <- rr1[-c(1:80)]
    rr2 <- rr2[-c(1:80)]
  }
  if(i == 20)
    ref <- ref[10:130]
  if(i == 21) {
    ref <- ref[-c(1:19)]
  }
  if(i == 25) {
    ref <- ref[-c(1:7)]
    rr1 <- rr1[-c(1:6)]
    rr2 <- rr2[-c(1:6)]
  }
  if(i == 26)
    ref_outl <- c(108, 109)
  if(i == 27) {
    ref_outl <- c(28, 29)
    ref <- ref[-c(1:60)]
    rr1 <- rr1[-c(1:40)]
    rr2 <- rr2[-c(1:40)]
  }
  if(i == 28) {
    ref_outl <- c(28, 29)
    ref <- ref[-c(1:55)]
    rr1 <- rr1[-c(1:50)]
    rr2 <- rr2[-c(1:50)]
  }
  
  
  # ss <- round(seq(49.0, 51.0, 0.1), 1)
  k <- fs_factor[i]
    

  rr1 <- rr_resample(rr1, k)
  rr2 <- rr_resample(rr2, k)
  
  align_out <- alignToRefRR_single(ref, rr1, rr2, ref_outl)
  title(main = paste0("RRI test ", j))
  
  savePlot(paste0("./results/25 vs 50/window_", i, ".png"), "png")
  graphics.off()
  
  out_mat[j,] <- c(align_out[[1]], align_out[[2]])
  rr_good_all <- c(rr_good_all, align_out[[3]])
  
  catProgress(j, len(i_used))
}





w()
boxplot(x = list("LifeQ (50Hz) vs ref"=out_mat[,4], 
                 "LifeQ (25Hz) vs ref"=out_mat[,5], 
                 "LifeQ (50Hz) vs LifeQ (25Hz)"=out_mat[,6]),
        ylab = "MAPD (%)", col = c("blue", "red", "green"),
        main = "Distribution of MAPD statistics over all data")


hr_all <- 60/(rr_good_all/1000)
w()
hist(hr_all, prob = T, breaks = 60, xlim = c(40, 140), col = "grey",
     xlab = "HR (bpm)", main = "Histogram of heart rates over all data")





####################
## Accuracy stuff ##
####################





i_used <- c(1, 4, 5, 9, 11, 17, 18, 20, 21, 22, 24, 29, 34)
i_used <- c(i_used, 2, 8, 13, 25, 26, 27, 28, 32, 33)


i_factor <- matrix(c(1, 2, 4, 5, 8, 9, 11, 13, 17, 18, 20, 21, 22, 
                     24, 25, 26, 27, 28, 29, 32, 33, 34,
                     49.7, 50.1, 50, 49.2, 49, 49.9, 49.3, 50, 
                     50.1, 49.5, 50.4, 49.6, 49.7, 49.8, 50.1, 
                     49.7, 49, 49.2, 49.6, 50, 49.4, 49.8),
                   nrow = 2, byrow = T)
fs_factor <- rep(50, max(i_used))
fs_factor[i_factor[1,]] <- i_factor[2,]


w(14, 7) 
j <- 0
out_mat <- matrix(0, len(i_used), 2)
out_td <- vector("list", len(i_used))
n_rr <- 0
for(i in sort(i_used)) {
  
  # i <- sort(i_used)[1]
  j <- j+1
  
  
  ref <- reflist[[i]]
  rr1 <- RRlist_R[[i]][[2]]

  ref_outl <- NA
  
  if(i == 2)
    ref_outl <- c(52, 53, 54)
  if(i == 8) {
    ref <- ref[-c(1:12)]
  }
  if(i == 9) {
    ref <- ref[-c(1:15)]
  }
  if(i == 10) {
    ref <- ref[-c(1:25)]
  }
  if(i == 13) {
    ref <- ref[-c(1:85)]
    rr1 <- rr1[-c(1:80)]
  }
  if(i == 20)
    ref <- ref[10:130]
  if(i == 21) {
    ref <- ref[-c(1:19)]
  }
  if(i == 25) {
    ref <- ref[-c(1:7)]
    rr1 <- rr1[-c(1:6)]
  }
  if(i == 26)
    ref_outl <- c(108, 109)
  if(i == 27) {
    ref_outl <- c(28, 29)
    ref <- ref[-c(1:60)]
    rr1 <- rr1[-c(1:40)]
  }
  if(i == 28) {
    ref_outl <- c(28, 29)
    ref <- ref[-c(1:55)]
    rr1 <- rr1[-c(1:50)]
  }
  
  
  rr <- rr_resample(rr1, fs_factor[i])
  
  align_out <- alignToRefRR_single(ref, rr, ref_o = ref_outl)
  

  out_mat[j,] <- c(align_out[[1]], align_out[[2]])
  
  ref <- align_out[[4]]
  rr <- align_out[[5]]
  rr_outl <- detectRROutliers(rr, rr_thresh_upper = 1775)
  ref_outl_det <- detectRROutliers(ref, rr_thresh_upper = 1775)
  if(len(which(!align_out[[3]])) > 0) ref_outl_det[which(!align_out[[3]]), 1] <- 1
  
  n_rr <- n_rr + len(align_out[[5]])
  
  title(main = paste0("RRI test ", j, " | 50Hz\n# ref outliers = ", sum(rr_outl[,1]),
                      " | # obs outliers = ", sum(ref_outl_det[,1])))
  savePlot(paste0("./results/window_", i, "_25Hz.png"), "png")
  
  
  
  td_ref <- tdomain_metrics(ref, ref_outl_det[,1])[[2]]
  td_rr <- tdomain_metrics(rr, rr_outl[,1])[[2]]
  out_td[[j]] <- rbind(td_ref, td_rr)
  colnames(out_td[[j]]) <- c("sdnn", "sdsd", "rmssd", "pnn50", "score")
  
  
  

  catProgress(j, len(i_used))
}
graphics.off()



td_ref_all <- matrix(unlist(lapply(out_td, function(x) x[1, 1:3])), ncol = 3, byrow = T)
td_rr_all <- matrix(unlist(lapply(out_td, function(x) x[2, 1:3])), ncol = 3, byrow = T)
td_diff <- matrix(unlist(lapply(out_td, function(x) apply(x[,1:3], 2, diff))), ncol = 3, byrow = T)



w()
boxplot(x = list("LifeQ (50Hz) vs ref"=td_diff_50[,3], 
                 "LifeQ (25Hz) vs ref"=td_diff[,3]),
        ylab = "mean difference RMSSD (ms)", col = c("blue", "mediumseagreen"),
        main = "Distribution of RMSSD statistics over all data")
 









#######################
## Visualise results ##
#######################

ecg_adjustment1 <- matrix(c( 15,   6,  11,  28,  17,  85,   6,   8,   4,   4,  96,   5,
                            130, 124, 127, 145, 131, 210, 133, 126, 124, 122, 213, 122), ncol=2) 

R_adjustment1   <- c(12, 16, 17, 15, 20,  5,  1,  8,  1,  1,  1,  2)

###################################################################################################

ecg_adjustment2 <- matrix(0, 30, 2)

R_adjustment2   <- c(rep(5, 30))

###################################################################################################

ecg_adjustment3 <- c(c( 22,  21,   9,  10,   6,   5,   7,   9,   7,  16,  18,  21,   3,   5,   9,   6,   5,  
                         4,   9,  69,   8,   4,   5,  38,   3,   8,  11,  11,  11,  12,  30,  47,  30,  10),
                     c(137, 138, 130, 129, 126, 130, 124, 134, 126, 137, 133, 141, 189, 131, 121, 175, 121, 
                       121,  79, 186, 136, 121, 181, 155, 120, 126, 143, 151, 128, 134, 162, 185, 147, 127))
ecg_adjustment3 <- matrix(ecg_adjustment3, ncol=2)

R_adjustment3   <- c( 3,  1,  1,  1,  1,  1,  1,  1,  1,  1,
                      1,  4,  5,  1,  1,  1,  2,  1, 50,  1,
                      1,  1,  1,  1,  1,  1,  2,  1,  1,  1,
                      1,  1,  1,  1)

###################################################################################################

ecg_adjustments <- rbind(ecg_adjustment1, ecg_adjustment2, ecg_adjustment3)
R_adjustments   <- c(R_adjustment1, R_adjustment2, R_adjustment3)



pfnames <- c(paste0("./results/00", 1:9, "_", names(datlist)[1:9]),
             paste0("./results/0", 10:len(datlist), "_", names(datlist)[10:len(datlist)]))


plot_C <- TRUE

if(plot_C) {
  RRlist_C <- vector("list", len(datlist))
  for(i in 1:len(datlist)) {
    
    cout <- read.table(paste0("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/physcalc_out_", names(datlist)[i], ".csv"), sep=",", head=T)
    RRlist_C[[i]] <- cout[cout[,10] == 1, c(11,13)]
    
  }
}


w() ;# par(ask=T)
for(i in 1:len(datlist)) {
  
  print(i)
  
  rr_ecg <- reflist[[i]]
  rr_R1 <- RRlist_R[[i]][[1]]
  rr_R2 <- RRlist_R[[i]][[2]] 
  
  if(len(rr_R2) > len(rr_R1))
    rr_R2 <- rr_R2[(len(rr_R2) - len(rr_R1) + 1):len(rr_R2)]
  
  
  if(all(rr_ecg == 0)) rr_ecg <- rep(mean(rr_R1), len(rr_R1))
  
  if(ecg_adjustments[i,2] != 0) rr_ecg <- rr_ecg[ecg_adjustments[i,1]:ecg_adjustments[i,2]]
  rr_R1 <- rr_R1[-c(1:R_adjustments[i])]
  rr_R2 <- rr_R2[-c(1:R_adjustments[i])]
  
  # w()
  lplot(cumsum(rr_ecg)/1000, rr_ecg, lwd=4, col="red", 
        main=paste0(i, "_", names(datlist)[i]), xlab="time (s)", ylab="RR (ms)")
  lines(cumsum(rr_R1)/1000, rr_R1, lwd=2, col="blue")
  # lines(cumsum(rr_R2)/1000, rr_R2, lwd=2, col="mediumseagreen")
  
  if(plot_C) {
    rr_C <- RRlist_C[[i]][-c(1:2),1]
    rr_C_conf <- RRlist_C[[i]][-c(1:2),2]
    lines(cumsum(rr_C)/1000, rr_C, lwd=2, col="mediumseagreen")
    par(new=T)
    lplot(cumsum(rr_C)/1000, rr_C_conf, lwd=2, col="black", ann=F, yaxt="n", xaxt="n")
    axis(4)
  }
  
  savePlot(paste0(pfnames[i], "_out.png"), "png")
  
}





#######################
## new C development ##
#######################

w()
for(i in 1:len(datlist)) {
  cfilt <- read.table(paste0("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/debug_out_", names(datlist)[i], ".csv"), sep=",")
  names(cfilt) <- c("green_current", "after_hamming", "after_diff", "after_RLS", "after_rescaling", "after_BP")
  
  
  ff <- function(x) return(-x)
  hrv_out <- ER_MA_DT_peaks_C(cfilt[,4], filt_t="FUN", filt_f=ff, clip=200, output=T, interp=T, bf_fs=25)
  xf2 <- hrv_out$filt_signal
  
  # w() 
  lplot(RRlist_R[[i]][[1]][-c(1:4)], lwd=3, col="blue")
  lines(hrv_out$RR, lwd=3, col="mediumseagreen")
  
  savePlot(paste0(pfnames[i], "_out.png"), "png")
  
  catProgress(i, len(datlist))
  
}


xf1 <- RRlist_R[[i]][[2]]
xf1 <- xf1[seq(1, len(xf1), 2)]

zPlot(cbind(xf1[-c(1:95)], xf2))


###################################################################################################
###################################################################################################

aa <- read.table("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/out_testing.txt", sep=",")


i <- 55
cfilt <- read.table(paste0("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/debug_out_", names(datlist)[i], ".csv"), sep=",")
names(cfilt) <- c("green_current", "after_hamming", "after_diff", "after_RLS", "after_rescaling", "after_BP")

inds <- c(1, 4, 6)
zPlot(cfilt[,inds], par_new=T)




  