dirk <- read.table("~/HealthQ/Data/DirkVinger30-parsed.csv", head=T, sep=",")

acc_res <- function(mat, scaling=12)
  return(scaling*sqrt(rowSums(((mat - 2^15) / (2^16))^2)) )

write.table(cbind(dirk[350:900,5],0,dirk[350:900,13:15]),
            file="dnotch_dirk01.txt", quote=F, row.names=F, col.names=F, sep=",")
