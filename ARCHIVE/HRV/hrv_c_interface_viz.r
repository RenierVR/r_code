source("../useful_funs.r")
source("./ERMADT_funs.r")


###################################################################################################
###################################################################################################

##################
## read in data ##
##################

## input to PP_addAfeSamples2()
c_in <- read.table("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/input_testing.txt", sep=",", head=T)

## PhysCalc output 
c_out <- read.table("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/physcalc_out.txt", sep=",", head=T)

## debugging output
c_test <- read.table("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/out_testing.txt", sep=",", head=T)


##############################
## extract relevant metrics ##
##############################

c_hrv <- c_out[,10:15]
c_hrv <- c_hrv[c_hrv[,1]==1, 2:3]
crr <- c_hrv[,1]



###################################################################################################
###################################################################################################

###################################
## plot RR intervals (tachogram) ##
###################################

hrv_cplot_tgram <- function(y=crr, y_test=NULL, Rcompare=FALSE)
{
  zPlot(y)
  
  if(Rcompare & !missing(y_test)) {
    hrv_out <- ER_MA_DT_peaks_C(y_test[,1], filt="FIR50", clip=200, output=T, interp=F)
    rr <- diff(1000*hrv_out$peaks/50)
    
    zPlot(cbind(crr_b, crr), ynames=c("R", "C"), col_f=c("red", "mediumseagreen"))
  }
}





########################
## plot input signals ##
########################

hrv_cplot_input <- function(y=c_in, acc=TRUE)
{
  if(acc)
    zPlot(cbind(y[,1], acc_res(y[,5:7])), c("green", "acc"), col_f=c("forestgreen", "purple"), 
          par_new=T)
  else
    zPlot(cbind(y[,1]), "green", col_f=c("forestgreen"), par_new=T)
}


###############################
## plot HRV filtering stages ##
###############################

hrv_cplot_filt <- function(y=c_test, ord=1:3)
{
  yn <- c("greensub", "meansub", "bpfilt", "sq_acc")
  ycol <- c("forestgreen", "blue", "maroon")
  zPlot(y[,ord], ynames=yn[ord], col_f=ycol[ord], par_new=T)
}


