source("../useful_funs.r")
source("./ERMADT_funs.r")
source("./amendConfidencePostProcess.r")
source("./alignToRefRR_v2.r")
source("../Rwrapper.r")




###################################################################################################
#########################      201507 HR WHITE PAPER - HRV ANALYSIS      ##########################
###################################################################################################

# ## PROTOCOL:
# jump up and down 3 times for alignment
# lie down (3 min)
# sitting (3 min)
# sorting (2 min)
# rest (1 min)
# typing (2 min)
# rest (2 min)
# jump up and down 3 times for alignment





###################################################################################################
###################################################################################################


##################
## get C output ##
##################

datlist <- getDataFromDir("D:/HealthQ/Data/HRV/201507 HRWP Lifestyle/", head=T)

dat <- datlist[[1]]
dat <- dat[seq(1, nrow(dat), 2),]



LQI_init(25)
fdat <- LQI_formatData(dat[, c(1:8, 8:9, 9:16)])

out <- LQI_processData(fdat)[[4]]
out_rr <- out[out[,1] == 1, 2:4]
out_rr <- out_rr <- cb(cumsum(out_rr[,1])/1000, out_rr)

out_rr_i <- out[out[,1] == 1, 5:6]
out_rr_i <- out_rr_i[,1] + out_rr_i[,2]/1000


LQI_unload()



######################
## Plot PPG signal  ##
######################

pind <- 800:1050
pind_rr <- out_rr_i[(out_rr_i >= pind[1]) & (out_rr_i <= tail(pind, 1))]

rr_lines <- cb(pind_rr*0.02, dat$ppg_green[pind_rr])

w() ; lplot_g(pind*0.02, dat$ppg_green[pind], col="seagreen", lwd=3,
              ylab="PPG green", xlab="time (s)", main="Illustration of RR peaks in PPG signal")

for(j in 2:nrow(rr_lines)) {
  lines(cb(rr_lines[(j-1):j, 1], rr_lines[j-1,2]), lwd=2, col="red")
  lines(cb(rr_lines[j,1], rr_lines[(j-1):j,2]), lwd=1, lty=2, col="red")
}

legend("topright", lwd=2, lty=1, col="red", leg="RR intervals", bg="white")



#############################
## Plot clean RR intervals ##
#############################

pind <- c(120, 200)
rr_which <- (out_rr[,1] <= tail(pind,1)) & (out_rr[,1] >= pind[1])

w() ; lplot_g(out_rr[rr_which, 1:2], lwd=3,
              ylab="RRI (ms)", xlab = "time (s)",
              main="Tachogram example during rest")

points(out_rr[rr_which, 1:2], col="blue", cex=1.0, pch=19)



######################
## Plot RRV example ##
######################

pind <- c(1, 80)
rr_which <- (out_rr[,1] <= tail(pind,1)) & (out_rr[,1] >= pind[1])

w() ; lplot_g(out_rr[rr_which, 1:2], lwd=3,
              ylab="RRI (ms)", xlab = "time (s)",
              main="Tachogram with RRV indicator")

abline(v = out_rr[rr_which, 1][out_rr[rr_which, 3] == 1],
       lwd=2, col="red")
points(out_rr[rr_which, 1:2], col="blue", cex=1.0, pch=19)
points(out_rr[rr_which, 1:2][out_rr[rr_which, 3] == 1, 1:2], col="red", pch=19)
legend("topright", lwd=2, pch=19,, col="red", leg="Invalid RRIs", bg="white")


##########################
## Plot RRV with signal ##
##########################

# pind <- c(1, 80)
# rr_which <- (out_rr[,1] <= tail(pind,1)) & (out_rr[,1] >= pind[1])
# 
# w() ; lplot_g(out_rr[rr_which, 1:2], lwd=3,
#               ylab="RRI (ms)", xlab = "time (s)",
#               main="Tachogram with RRV indicator")
# 
# abline(v = out_rr[rr_which, 1][out_rr[rr_which, 3] == 1],
#        lwd=2, col="red")
# points(out_rr[rr_which, 1:2], col="blue", cex=1.0, pch=19)
# points(out_rr[rr_which, 1:2][out_rr[rr_which, 3] == 1, 1:2], col="red", pch=19)
# legend("topright", lwd=2, pch=19,, col="red", leg="Invalid RRIs", bg="white")
# 
# 
# dat <- datlist[[1]]
# green_c <- removeCurrentDiscont2(dat$ppg_green, dat$amp_green, dat$off_green, dat$led_green)
# acc_r <- acc_r <- acc_res(dat[,5:7], 16)
# 
# Fs <- 50
# rr_pp <- ER_MA_DT_peaks_C(green_c, filt_t="FIRHAM", clip=4*Fs, output=T, interp=T, bf_fs=Fs,
#                           check_mult_peaks_above_zero=T, post_filt_running_mean=F)
# rrv_pp <- detectRROutliers(rr_pp$RR)


###############
## Plot RRCM ##
###############

# 25
## 28
### 32

dat <- datlist[[32]]
dat <- dat[seq(1, nrow(dat), 2),]

LQI_init(25)
out <- LQI_processData(LQI_formatData(dat[, c(1:8, 8:9, 9:16)]))[[4]]
out_rr <- out[out[,1] == 1, 2:4]
out_rr <- out_rr <- cb(cumsum(out_rr[,1])/1000, out_rr)
LQI_unload()



rrcm_post <- amendConfidencePostProcess(out_rr[,2], out_rr[,4])[,3]


w() ; plot_g(out_rr[,1:2], type="n",
             ylab="RRI (ms)", xlab = "time (s)",
             main="Tachogram of resting + lifestyle data")
abline(v=out_rr[out_rr[,3] == 1, 1], col="red")
lines(out_rr[,1:2], lwd=2, col="blue")
legend("topright", lwd=2, col="red", leg="Invalid RRIs", bg="white")




w() ; plot_g(out_rr[,1:2], type="n",
             ylab="RRI (ms)", xlab = "time (s)",
             main="Tachogram of resting + lifestyle data (with RRCM)")
abline(v = out_rr[rrcm_post == 1,1], col="lightgreen", lwd=2)
abline(v=out_rr[out_rr[,3] == 1, 1], col="red")
lines(out_rr[,1:2], lwd=2, col="blue")
legend("topright", lwd=2, col=c("red", "lightgreen"), 
       leg=c("Invalid RRIs", "High RR confidence"), bg="white")




###########################
## DISRUPTED ECG SIGNAL  ##
###########################

dat <- datlist[[2]]

green_c <- removeCurrentDiscont2(dat$ppg_green, dat$amp_green, dat$off_green, dat$led_green)
acc_r <- acc_r <- acc_res(dat[,5:7], 16)

pind <- 18400:20000
w(14,10) ; par(mar=c(0,5,0,4), oma=c(5,0,3,0))
layout(matrix(c(1,2), nr=2, nc=1), heights=c(2,1))
lplot_g(pind*0.02, green_c[pind], lwd=3, col="seagreen", ylab="PPG green")
title(main="PPG signal disruption from motion", outer=T)
lplot_g(pind*0.02, acc_r[pind], lwd=2, col="red", ylab="Acceleration (G)")
title(xlab="time (s)", outer=T)



###############
## PLOT ECG  ##
###############


library(rhdf5)
h5_fpath <- "D:/HealthQ/Data/RRI/24h_Faros/20160117_TPID0404_RRI_24hr/20170117_TPID0404_Faros/TPID0404_Faros.h5"
ecg_f <- t(h5read(h5_fpath, "ECG", bit64conversion="double", compoundAsDataFrame=TRUE)[[4]])


pind <- 1181000:1186000
w() ; lplot_g((pind - pind[1])/250 + 5, ecg_f[pind], lwd=2,
              ylab="ECG", xlab="time (s)", main="Illustration of RR peaks in ECG signal")
  

pind_rr <- locator(11)
rr_lines <- as.matrix(as.data.frame(pind_rr))

for(j in 2:nrow(rr_lines)) {
  lines(cb(rr_lines[(j-1):j, 1], rr_lines[j-1,2]), lwd=2, col="red")
  lines(cb(rr_lines[j,1], rr_lines[(j-1):j,2]), lwd=1, lty=2, col="red")
}

legend("bottomright", lwd=2, lty=1, col="red", leg="RR intervals", bg="white")




################################
## PLOT RSA AND LOMB-SCARGLE  ##
################################


fpath <- "D:/HealthQ/Data/RRI/GarminValidation/ALL/"


library(edfReader)
library(lomb)

datlist_ref <- as.list(getDataFromDir(paste0(fpath, "reference/"), 
                                      dataE=F, ret="full", ext="EDF"))
datlist_names <- unlist(datlist_ref)

for(i in 1:len(datlist_ref)) {
  ref_header <- readEdfHeader(datlist_ref[[i]])
  ref <- readEdfSignals(ref_header)
  ref_rr <- ref$HRV$signal
  ref_rr <- ref_rr[ref_rr != 0]
  
  
  datlist_ref[[i]] <- ref_rr
}
names(datlist_ref) <- basename(datlist_names)



i <- 6
out_rr <- datlist_ref[[i]]
out_rr <- cb(cumsum(out_rr)/1000, out_rr)
which_rr <- which((out_rr[,1] >= 40) & (out_rr[,1] <= 150))

rr_mat <- out_rr[which_rr, 1:2]

w() ; lplot_g(rr_mat, lwd=3, ylim=c(650,1050),
              ylab="RRI (ms)", xlab = "time (s)",
              main="Tachogram during rest showing RSA")
points(rr_mat, col="blue", cex=1.0, pch=19)

rr_mat[,2] <- rr_mat[,2] - mean(rr_mat[,2])
f <- seq(0, 0.45, len=1000)
lsp_out <- lsp(rr_mat, from=2*pi*f[1], to=2*pi*f[len(f)], plot=F)



ff_hz <- f
hrv_vlf <- which((ff_hz > 0.0033) & (ff_hz < 0.04))
hrv_lf  <- which((ff_hz > 0.04) & (ff_hz < 0.15))
hrv_hf  <- which((ff_hz > 0.15) & (ff_hz < 0.4))

w(12,10) ; lplot_g(lsp_out$scanned/(2*pi), lsp_out$power, lwd=1, col="black",
                   xlab="frequency (Hz)", ylab="power", main="Power spectral density of HRV during rest")
abline(v=c(0.0033, 0.04, 0.15, 0.4), lwd=2)
legend("topright", col=c("green", "blue", "red"), bg="white", lwd=3,
       leg=c("VLF component", "LF component", "HF component"))

