source("../useful_funs.r")
source("./ERMADT_funs.r")

calc_aligned_RR_accuracy_wrt_ref <- function(rr, rr_ref, skip_first=0)
{ # Assume aligned RR sequence (with respect to reference)
  
  if(ncol(cb(rr)) == 1) rr <- cb(cumsum(rr)/1000, rr)
  if(ncol(cb(rr_ref)) == 1) rr_ref <- cb(cumsum(rr_ref)/1000, rr_ref)
  
  ## Detect outliers in both streams of values
  rr_invalid <- detectRROutliers(rr[,2])[,1]
  rr_ref_invalid <- detectRROutliers(rr_ref[,2])[,1]
  
  rr <- cb(rr, rr_invalid)
  rr_ref <- cb(rr_ref, rr_ref_invalid)
  
  ## Determine closest-in-time RRI values
  which_rr_closest <- sapply(rr_ref[,1], function(x) which.min(abs(x - rr[,1])) )
  
  ## Determine non-corresponding ref RR values to omit
  diff_rr_closest <- diff(which_rr_closest)
  which_rr_gaps <- which(diff_rr_closest == 0)
  # if(tail(diff_rr_closest, 1) == 0) which_rr_gaps <- c(which_rr_gaps, len(which_rr_closest))
  which_rr_gaps_list <- split_diff_into_list(which_rr_gaps)
  
  which_ref_idxs_remove <- NULL
  for(i in 1:len(which_rr_gaps_list)) {
    tmp_idxs <- which_rr_gaps_list[[i]]
    tmp_idxs <- c(tmp_idxs, tail(tmp_idxs, 1)+1)
    
    tmp_ref_values <- rr_ref[tmp_idxs, 1]
    tmp_rr_val <- rr[which_rr_closest[tmp_idxs[1]], 1]
    
    which_ref_idxs_remove <- c(which_ref_idxs_remove, 
                               tmp_idxs[-which.min(abs(tmp_rr_val - tmp_ref_values))])
  }
  
  
  ## Amend idxs of corresponding RR values to compare
  which_idxs <- cb(1:nrow(rr_ref), which_rr_closest)
  which_idxs <- which_idxs[-which_ref_idxs_remove, ]
  
  
  ## Discount reference outliers
  which_idxs <- which_idxs[!(rr_ref[which_idxs[,1], 3] != 0), ]
  
  ## Discount outliers
  which_idxs <- which_idxs[!(rr[which_idxs[,2], 3] != 0), ]
  

  ## Calcalte accuracy of valid corresponding RR values  
  abs_diff <- abs(rr_ref[which_idxs[,1], 2] - rr[which_idxs[,2], 2])
  mad <- mean(abs_diff)
  
  
  ## Print the mean abs diff and return output
  cat("MAD =", format(mad, digits=3), "\n")
  invisible(list("absolute_differences" = abs_diff,
                 "mean_abs_diff"=mad))
}








# scrollPlot(winsize=300, overlap=200, n=min(tail(rr_f, 1)[1,1], tail(rr_g, 1)[1,1]), plotfun=function(xi){
# 
#   which_f <- which((rr_f[,1] >= xi[1]) & (rr_f[,1] <= tail(xi)[1]))
#   which_g <- which((rr_g[,1] >= xi[1]) & (rr_g[,1] <= tail(xi)[1]))
# 
#   lplot(rr_f[which_f,1], rr_f[which_f,2], lwd=2)
#   lines(rr_g[which_g,1], rr_g[which_g,2], lwd=2, col="blue")
#   title(main = paste("RR list element", i))
# 
#   abline(v=rr_f[rr_f[,3] == 1, 1], col="orange")
#   abline(v=rr_g[rr_g[,3] == 1, 1], col="red")
# })









