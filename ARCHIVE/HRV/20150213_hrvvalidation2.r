source("../useful_funs.r")
source("../HealthQ_OxySat/sp02_funs.r")


fpath1 <- "D:/HealthQ/Data/HRV/201502 HRV Validation/20150213/"

fpath2 <- paste0("HRV_", c("Christopher", "Danie", "Dirk", "Dominique", "Eugene", "Kim", "Koch", "Kora",
                           "Lafras", "Liani", "LuckyMbele", "Michelle", "Nike", "Nina",
                           "Shannagh", "SiyakdumisaZono", "Claudia", "Michael"))
fpath3 <- paste0(fpath1, fpath2, "/")





i <- 18


i_index <- (1:len(fpath3))[-c(3,5)]
cor_vec <- NULL
mae_vec <- NULL
mpe_vec <- NULL
for(i in i_index){


###################
###### FILE INPUT
###########################

files <- list.files(fpath3[i])
f_bhecg <- grep("ecg_peaks", files)
f_bhrr <- grep("RR", files)

if(len(f_bhecg) > 0)
  bhrr <- diff(read.table(paste0(fpath3[i], files[f_bhecg]))[,1])
if(len(f_bhecg) == 0)
  bhrr <- read.table(paste0(fpath3[i], files[f_bhrr]), head=T, sep=",")[,2]

###################

# crr_c <- read.table(paste0(fpath3[i], "out_rr_current.txt"), sep=",")
# crr_v <- read.table(paste0(fpath3[i], "Mojo 5113/out_rr_volt.txt"), sep=",")

###################


mojorr <- scan(paste0(fpath3[i], files[grep("HRV_-results", files)]), skip=12, what="character", sep=",")

mojorr_tmp <- which(substring(mojorr[1], 1:nchar(mojorr[1]), 1:nchar(mojorr[1])) == "[")
mojorr[1] <- substring(mojorr[1], mojorr_tmp+1, mojorr_tmp+4)
mojorr_tmp <- which(sapply(mojorr, function(x) substring(x, nchar(x), nchar(x))) == "]")
mojorr[mojorr_tmp] <- substring(mojorr[mojorr_tmp], 1, nchar(mojorr[mojorr_tmp])-1)
mojorr <- as.numeric(mojorr[1:mojorr_tmp])




offsets <- matrix(c(68, 3, 600, 1200, # i = 1
                    3, 3, 600, 1200, # i = 2
                    20, 1, 400, 1600, # i = 3
                    8, 9, 500, 1100, # i = 4
                    15, 1, 800, 1600, # i = 5
                    5, 1, 700, 1300, # i = 6
                    37, 3, 700, 1400, # i = 7
                    17, 1, 600, 1400, # i = 8
                    9, 1, 600, 1400, # i = 9
                    8, 52, 600, 1200, # i = 10
                    72, 3, 700, 1100, # i = 11
                    3, 3, 400, 1100, # i = 12
                    2, 2, 700, 1200, # i = 13
                    50, 7, 400, 1600, # i = 14
                    8, 1, 600, 1000, # i = 15
                    58, 5, 600, 800, # i = 16
                    28, 2, 400, 700, # i = 17
                    8, 2, 700, 1500), nc=4, byrow=T)  # i = 18


bhplot <- bhrr[-c(1:offsets[i,1])]
crrplot <- mojorr[-c(1:offsets[i,2])]
minlen <- min(len(bhplot), len(crrplot)) ; if(i == 6) minlen <- 107 ;#if(i == 14) minlen = 115
bhplot <- bhplot[1:minlen] ; crrplot <- crrplot[1:minlen]

w() 
lplot(cumsum(bhplot), bhplot, lwd=4, ylim=offsets[i,3:4], 
      xlim=c(1,sum(crrplot)), xaxt="n", col="red", ann=F)
lines(cumsum(crrplot), crrplot, lwd=3, col="forestgreen", lty=1)
axis(1, at=pretty(cumsum(bhplot)), labels=pretty(cumsum(bhplot))/1000)
legend("topleft", lwd=2, col=c("red", "forestgreen"), bg="white",
       leg=c("ECG reference", "LifeQ"), cex=0.8)
title(xlab="time (s)", ylab="interval length (ms)", 
      main=paste(substring(fpath2[i], 5, nchar(fpath2[i])), "HRV test"))

savePlot(file=paste0(fpath1, "RR_PLOTS/hrv_plot_", substring(fpath2[i], 5, nchar(fpath2[i])), "_tachogram"), "png")



if(i == 14) bhplot <- bhplot[-c(12,54:56,120)]
# if(i == 16){ bhplot <- bhplot[-c(11, 78, 86)] ; crrplot <- crrplot[-c(87, 90)] }
if(i == 9) bhplot <- bhplot[-c(35)]
minlen <- min(len(bhplot), len(crrplot)) ; if(i == 6) minlen <- 107
bhplot <- bhplot[1:minlen] ; crrplot <- crrplot[1:minlen]

w()
lplot(bhplot, lwd=4, ylim=offsets[i,3:4], col="red", ann=F)
lines(crrplot, lwd=3, col="forestgreen", lty=1)
# axis(1, at=pretty(1:len(bhplot)), labels=pretty(1:len(bhplot))/1000)
legend("topleft", lwd=2, col=c("red", "forestgreen"), bg="white",
       leg=c("ECG reference", "LifeQ"), cex=0.8)
title(xlab="index", ylab="interval length (ms)", 
      main=paste(substring(fpath2[i], 5, nchar(fpath2[i])), "HRV test"))

savePlot(file=paste0(fpath1, "RR_PLOTS/hrv_plot_", substring(fpath2[i], 5, nchar(fpath2[i])), "_rrindex"), "png")

write.table2(cbind(bhplot, crrplot), 
             file=paste0(fpath1, "RR_OUTPUT/hrv_rr_", substring(fpath2[i], 5, nchar(fpath2[i])), ".csv"))


cor_vec <- c(cor_vec, cor(bhplot, crrplot))
mae_vec <- c(mae_vec, mean(abs(bhplot-crrplot)))
mpe_vec <- c(mpe_vec, 100*mean((crrplot-bhplot)/bhplot))


graphics.off()
}





#######################################################################################
#######################################################################################



i <- 14
files <- list.files(fpath3[i])
files_m <- files[grep("Unknown", files)]
dat <- read.table(paste0(fpath3[i], files_m[grep("csv", files_m)]), sep=",", head=T)


# writeMojoFormat(dat, )

dat <- dat[dat[,1] != 0,]
curr1 <- curr2 <- matrix(0, nr=nrow(dat)-1, nc=4)
rfmat <- isubmat <- matrix(0, nr=nrow(dat)-1, nc=4)
for(j in 1:4){
  if(j < 3)
    Rf <- dat[,9]
  else
    Rf <- dat[,10]
  
  dat_tmp <- dat[-1,j]
  
  isub_tmp <- 10*dat[,10+j]
  isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
  
  Rf <- Rf[1:(len(Rf)-1)]
  
  #   converted1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp, c1, c2)
  curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)#, pars[1], pars[2])
  curr2[,j] <- removeCurrentDiscont(curr1[,j], Rf, isub_tmp)
  
  rfmat[,j] <- Rf
  isubmat[,j] <- isub_tmp
}



zPlot(cbind(curr2[,1]-curr2[,2], dat[1:(nrow(dat)-1),15:17]), par_new=T, 
      col_f=c("blue", "red", "orange", "green"))


zPlot(cbind(curr2[,1]-curr2[,2], rfmat[,1], isubmat[,1]), par_new=T, 
      col_f=c("blue", "red", "orange"))


zPlot(curr2[,1])

zPlot(cbind(curr2[,1], FIRfilt(curr2[,1]), acc_res(dat[-1,5:7])), par_new=T)


