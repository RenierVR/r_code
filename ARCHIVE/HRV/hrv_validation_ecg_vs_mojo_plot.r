source("../useful_funs.r")
source("../SpO2/sp02_funs.r")



m_rr <- scan("clipboard", sep=",")



b_rr <- scan("clipboard")

# b_rr <- diff(scan("clipboard"))



w() ; lplot(m_rr[-c(1:1)], lwd=2) ; lines(b_rr[-c(1:100)], lwd=2, col="red")



##################################################################################


dat <- read.table("clipboard")
dat <- dat[dat[,1] != 0,]
acc <- acc_res(dat[,5:7])[-1]
curr1 <- curr2 <- matrix(0, nr=nrow(dat)-1, nc=4)
rfmat <- isubmat <- matrix(0, nr=nrow(dat)-1, nc=4)
for(j in 1:4){
  if(j < 3)
    Rf <- dat[,9]
  else
    Rf <- dat[,10]
  
  safe_term <- 0
  if(j == 2) safe_term <- 1
  
  dat_tmp <- dat[-1,j] 
  isub_tmp <- 10*dat[,10+j]
  isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
  Rf <- Rf[1:(len(Rf)-1)]
  curr1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)
  #   curr2[,j] <- removeCurrentDiscont(curr1[,j]+safe_term, Rf, isub_tmp)
  curr2[,j] <- removeCurrentDiscont(curr1[,j]+safe_term, Rf, isub_tmp)  
  rfmat[,j] <- Rf
  isubmat[,j] <- isub_tmp
}

zPlot(cbind(acc, FIRfilt(curr2[,1]-curr2[,2]), curr2[,1]-curr2[,2]), par_new=T,
      col_f=c("yellow", "mediumseagreen", "blue"))




##################################################################################
##################################################################################

m_hr <- scan("clipboard")
m_hr <- m_hr[seq(1,len(m_hr),50)]
m_hr <- m_hr[m_hr != 0]
(mean(m_hr))

b_hr <- scan("clipboard")
(mean(b_hr))

w() ; lplot(b_hr[-c(1:1)], lwd=2, col="red", ylim=range(c(m_hr,b_hr))) ; lines(m_hr[-c(1:1)], lwd=2, col="forestgreen")
