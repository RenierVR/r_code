source("../useful_funs.r")
source("../SpO2/sp02_funs.r")


######################################################################################
########    DATA FROM 2015-01-27

data_list <- getDataFromDir("D:/HealthQ/Data/HRV/20150127 Mojo 5393 Jaco/", head=T)


######################################################################################
########    DATA FROM 2015-02-02

data_list <- getDataFromDir("D:/HealthQ/Data/HRV/201502 HRV Validation/20150202/", head=T)

######################################################################################
########    DATA FROM 2015-02-02

data_list <- getDataFromDir("D:/HealthQ/Data/HRV/201502 HRV Validation/20150203/", head=T)

######################################################################################
########    DATA FROM 2015-02-04

data_list <- getDataFromDir("D:/HealthQ/Data/HRV/201502 HRV Validation/20150204/Mojo 5113/", head=T)
data_list <- getDataFromDir("D:/HealthQ/Data/HRV/201502 HRV Validation/20150204/Mojo 5106/", head=T)


######################################################################################


convertToCurrent3 <-  function(xx, Rf, Isub, c1=0.016061056, c2=0.809626)
{
  temp32bitVal <- xx - 32768
  
  tempVal <- c1 * temp32bitVal
  tempVal <- tempVal / Rf
  tempVal <- tempVal - c2 * (Isub/10.0)
  return (1000000.0 * tempVal)
}


######################################################################################



i <- 8
dat <- data_list[[i]]
dat <- dat[dat[,1] != 0,]
pars <- c(0.01552273,  0.8314437096)
converted1 <- matrix(0, nr=nrow(dat)-1, nc=4)
rfmat <- isubmat <- matrix(0, nr=nrow(dat)-1, nc=4)
for(j in 1:4){
  if(j < 3)
    Rf <- dat[,9]
  else
    Rf <- dat[,10]
  
  Rf <- Rf[1:(len(Rf)-1)]

  dat_tmp <- dat[-1,j]
  isub_tmp <- 10*dat[,10+j]
  isub_tmp <- isub_tmp[1:(len(isub_tmp)-1)]
  
  #   converted1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp, c1, c2)
  converted1[,j] <- convertToCurrent3(dat_tmp, Rf, isub_tmp)#, pars[1], pars[2])
  
  rfmat[,j] <- Rf
  isubmat[,j] <- isub_tmp
}


zPlot(cbind(dat[,1], rfmat[,1], isubmat[,1]), col_f=c("blue", "red", "orange"), par_new=T)
zPlot(cbind(converted1[,1], rfmat[,1], isubmat[,1]), col_f=c("blue", "red", "orange"), par_new=T)



grnI <- converted1[,1]-converted1[,2]
grnI_filt <- FIRfilt(grnI)

zPlot(cbind(grnI, grnI_filt), col_f=c("gray", "forestgreen"), par_new=T)



i <- 3

for(i in 1:3){
dat <- data_list[[i]]
dat <- dat[dat[,1] != 0,]
writeMojoFormat(dat, file=paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/data/HRV_new_tests/20150204_renier_hrvtest_Mojo5106_0",i,".txt"))
}




######################################################################################
######################################################################################


cdat <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/input_testing.txt", sep=",")
ctest <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/out_testing.txt", sep=",")

zPlot(cbind(cdat[,1], removeAGCJumps(cdat[,1],2000)), par_new=T)

zPlot(ctest[,1:2], par_new=T)

cout <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out.txt", sep=",")
lotCout(cout)


plotCout <- function(cout)
{
  w() ; par(mar=c(3,3,2,3))
  lplot(cout[,1], ylim=c(600,1400))
  abline(v=which(cout[,2] != 0), col="gray", lwd=3)
  lines(cout[,1], col="forestgreen")
  par(new=T) ; lplot(cout[,3], lwd=2, col="yellow", yaxt="n", xaxt="n", ann=F)
  par(new=T) ; lplot(cout[,4], col="orange", xaxt="n", yaxt="n", ann=F, ylim=range(cout[,4:5]))
  lines(cout[,5], col="red") ; axis(4)
}





######################################################################################
######################################################################################

prr <- scan("clipboard")

crr_out <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/out_rr.txt", sep=",")
crr_plot <- crr_out[-c(1:2),1]
prr_plot <- prr[-c(1:1)]

w() ; par(mar=c(3,3,2,2))
lplot(cumsum(crr_plot), crr_plot, lwd=2, ylim=range(c(crr_plot, prr_plot)), col="blue")
lines(cumsum(prr_plot), prr_plot, lwd=2, col="red")






pcout <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out_20150202_renier_hrvtest_06.txt", sep=",")


bb <- cbind(0, 0, 0, aa[,1:2], 0, 0, aa[,5], 0, aa[,6:7], 0, 0)
# bb <- cbind(bb[-1,1:7], bb[1:(nrow(bb)-1),8:13])
write.table2(format(bb,digits=12), file="~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/data/HRV_new_tests/20150204_plankie02.txt")
