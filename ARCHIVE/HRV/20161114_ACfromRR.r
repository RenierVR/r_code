source("../useful_funs.r")
source("../Rwrapper.r")


################################
##  READ IN DATA
################################

fpath <- "D:/HealthQ/Data/RRI/GarminValidation/"
fpaths <- list.dirs(fpath, recursive=F)
fpaths <- fpaths[grep("TPID", fpaths)]

datlist <- vector("list", len(fpaths))
for(i in 1:len(fpaths)) {
  
  ls <- list.files(fpaths[i])
  ls <- ls[grep(".csv", ls)]
  
  datlist[[i]] <- read.table(paste0(fpaths[i], "/", ls[grep("LQ", ls)]), sep=",", head=T)
}


################################
##  PASS DATA THROUGH C-LIBS
################################

outlist <- vector("list", len(datlist))
for(i in 1:len(datlist)) {
  
  LQI_init(25)
  
  dat <- datlist[[i]]
  fdat <- LQI_formatData(dat[, c(3:9, 11:21)])
  
  out <- LQI_processData(fdat)[[4]]
  out_rr <- out[out[,1] == 1, c(2:6, 11)]
  out_rr[,4] <- out_rr[,4] + out_rr[,5]/1000
  out_rr[,6] <- out_rr[,6]/100
  
  outlist[[i]] <- list("raw"=out[,7:9],
                       "RR"=out_rr[,-5])
  
  LQI_unload()
  
  catProgress(i, len(datlist))
}


# outlist_b <- vector("list", len(datlist))
for(i in 1:len(datlist)) {

  LQI_init(25)

  dat <- datlist[[i]]
  fdat <- LQI_formatData(dat[, c(3:9, 11:21)])

  out <- LQI_processData(fdat, showProgress=F)[[4]]
  
  print(outlist_b[[i]] %=% out[,7])


  LQI_unload()

  catProgress(i, len(datlist))
}


################################
##  INVESTIGATE PEAK INDICES
################################


i <- 1
  
out <- outlist[[i]]
dat <- datlist[[i]]



# PEAK_INDEX_ADJUSTMENT <- 0
PEAK_INDEX_ADJUSTMENT <- 17
out$RR[,4] <- out$RR[,4] + PEAK_INDEX_ADJUSTMENT






scrollPlot(winsize=350, n=nrow(dat), plotfun=function(xi){
  p1 <- out$raw[xi, 1]
  p2 <- out$raw[xi, 3]
  
  which_rr <- which((out$RR[,4] >= xi[1]) & (out$RR[,4] <= tail(xi, 1)))
  
  plot_g(xi, p1, type="n", ylab="Green PPG (current)")
  # abline(v=(out$RR[which_rr,4]), lty=2, col="yellow")
  
  lines(xi, p1, lwd=3, col="forestgreen")
  
  par(new=T)
  lplot(xi, p2, lwd=2, col="red", ann=F, yaxt="n", xaxt="n")
  axis(4)

  abline(h=0, lty=2, lwd=2, col="yellow")
  for(k in 1:len(which_rr)) {
    i_tmp <- out$RR[which_rr[k], 4:5]
    lines(matrix(c(i_tmp[1], 0, i_tmp), 2, byrow=T), lwd=2, col="yellow")
    points(i_tmp[1], i_tmp[2], pch=19, col="yellow", cex=0.8)
  }
  
  
  
  legend("topright", lwd=2, lty=1, col=c("forestgreen", "red", "yellow"),
         leg=c("green PPG current", "green filtered", "green filtered AC"), 
         cex=0.75, bg="white")
})



