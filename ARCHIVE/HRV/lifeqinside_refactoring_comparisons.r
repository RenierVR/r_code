source("../useful_funs.r")


cout_list <- getDataFromDir("D:/HealthQ/Output/RRtesting/b2/", dataE = F, ret = "full")

cout_debug <- cout_list[grep("debug_out", cout_list)]
cout_pcout <- cout_list[grep("physcalc_out", cout_list)]


cout_d_b <- vector("list", len(cout_debug))
cout_p_b <- vector("list", len(cout_pcout))

for(i in 1:len(cout_debug)) {
  cout_d_b[[i]] <- read.table(cout_debug[i], sep = ",", head = T)
  cout_p_b[[i]] <- read.table(cout_pcout[i], sep = ",", head = T)
  catProgress(i, len(cout_debug))
}



##################################################################################################
##################################################################################################


cout_list <- getDataFromDir("D:/HealthQ/Output/RRtesting/", dataE = F, ret = "full")

cout_debug <- cout_list[grep("debug_out", cout_list)]
cout_pcout <- cout_list[grep("physcalc_out", cout_list)]


comp_mat <- matrix(0, len(cout_debug), 2)
print_all <- FALSE
for(i in 1:len(cout_debug)) {
  cout_d <- read.table(cout_debug[i], sep = ",", head = T)
  cout_p <- read.table(cout_pcout[i], sep = ",", head = T)
  
  if(is.null(cout_d)) warning(paste("No debug for", i))
  else {
    comp_d <- cout_d %=% cout_d_b[[i]]
    comp_mat[i, 1] <- as.numeric(comp_d)
    
    if(print_all) {
      cat("Data set", i, "\n")
      cat("\tdebug ->", comp_d, "\n")
    }
  }  
  
  
  if(is.null(cout_p)) warning(paste("No output for", i))
  else {
    comp_p <- cout_p %=% cout_p_b[[i]]
    comp_mat[i, 2] <- as.numeric(comp_p)
    
    if(print_all)
      cat("\tp_out ->", comp_p, "\n\n")
  }
  
  if(!print_all) catProgress(i, len(cout_debug))
}

comp_fail_d <- which(comp_mat[,1] != 1)
comp_fail_p <- which(comp_mat[,2] != 1)

if(len(comp_fail_d) == 0) cat("All DEBUG compared TRUE\n")
if(len(comp_fail_p) == 0) cat("All P_OUT compared TRUE\n\n")




##################################################################################################
##################################################################################################


# w() ; par(ask = T)
# for(i in 1:166){ 
#   rr <- cout_p_b[[i]][cout_p_b[[i]][,11] == 1,12]
#   lplot(cumsum(rr)/1000, rr, lwd = 3, col = "mediumseagreen")
# }

