source("../useful_funs.r")


cout_list <- getDataFromDir("D:/HealthQ/Output/InitTesting/b/", dataE = F, ret = "full")

cout_debug <- cout_list[grep("out_initdebug", cout_list)]
cout_fin <- cout_list[grep("out_initfin", cout_list)]
cout_peaks <- cout_list[grep("out_initpeaks", cout_list)]


cout_d_b <- vector("list", len(cout_debug))
cout_f_b <- vector("list", len(cout_fin))
cout_p_b <- vector("list", len(cout_peaks))


for(i in (1:len(cout_debug))[-c(1, 10, 122, 162)]) {
  cout_d_b[[i]] <- read.table(cout_debug[i], sep = ",", head = T)
  cout_f_b[[i]] <- read.table(cout_fin[i], sep = ",", head = T)
  cout_p_b[[i]] <- read.table(cout_peaks[i], sep = ",", head = T)
  catProgress(i, len(cout_debug))
}



##################################################################################################
##################################################################################################


cout_list <- getDataFromDir("D:/HealthQ/Output/InitTesting/", dataE = F, ret = "full")

cout_debug <- cout_list[grep("out_initdebug", cout_list)]
cout_fin <- cout_list[grep("out_initfin", cout_list)]
cout_peaks <- cout_list[grep("out_initpeaks", cout_list)]


comp_mat <- matrix(0, len(cout_debug), 3)
for(i in (1:len(cout_debug))[-c(1, 10, 122, 162)]) {
  cout_d <- read.table(cout_debug[i], sep = ",", head = T)
  cout_f <- read.table(cout_fin[i], sep = ",", head = T)
  cout_p <- read.table(cout_peaks[i], sep = ",", head = T)
  
  
  if(is.null(cout_d)) warning(paste("No debug for", i))
  else {
    comp_d <- cout_d %=% cout_d_b[[i]]
    comp_mat[i, 1] <- as.numeric(comp_d)
  }  
  
  
  if(is.null(cout_f)) warning(paste("No output for", i))
  else {
    comp_f <- cout_f %=% cout_f_b[[i]]
    comp_mat[i, 2] <- as.numeric(comp_f)
  }
  
  
  if(is.null(cout_p)) warning(paste("No output for", i))
  else {
    comp_p <- cout_p %=% cout_p_b[[i]]
    comp_mat[i, 3] <- as.numeric(comp_p)
  }
  
  catProgress(i, len(cout_debug))
}

comp_mat <- comp_mat[-c(1, 10, 122, 162),]

comp_fail_d <- which(comp_mat[,1] != 1)
comp_fail_f <- which(comp_mat[,2] != 1)
comp_fail_p <- which(comp_mat[,3] != 1)

if(len(comp_fail_d) == 0) cat("All DEBUG compared TRUE\n")
if(len(comp_fail_f) == 0) cat("All FINSH compared TRUE\n")
if(len(comp_fail_p) == 0) cat("All PEAKS compared TRUE\n\n")




##################################################################################################
##################################################################################################

w() ; par(ask = T)
for(i in (1:len(cout_debug))[-c(1, 10, 122, 162)]) {
  cout_d <- read.table(cout_debug[i], sep = ",", head = T)
  j <- 5
  aa <- cb(cout_d[,j], cout_d_b[[i]][,j])
  lplot(aa[,1], lwd = 3, col = "red",
        main = paste(i, "|", basename(cout_debug[i])))
  lines(aa[,2], lwd = 2, col = "blue")
}
  
  
  
  
  

