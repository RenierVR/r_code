library(TSA)

QM_V1 <- function(spec,norm=TRUE){
  # Gives values more or less between 0 and 1.
  # Should actually cap skewness at 20
  if(norm==FALSE) return((log(abs(skewness(spec))+1)/3)^3)
  if(norm==TRUE) {spec <- spec/sum(spec) * 100 # normalize
                  return((log(abs(skewness(spec))+1)/3)^3)}
}


QM_SF<-function (spec,bias=0.000001) {
  # Function that gives the tradional Spectral flatness measure, as the ratio of the geometric mean to the artihmetic mean
  # Spec can be a subband of the spectrum
  
  if (any(spec < 0)) 
    stop("Data do not have to be in dB")
  
  #Subsample if spec is too large
  if (length(spec) > 512) {
    step <- seq(1, length(spec), by = round(length(spec)/256))
    spec <- spec[step]
  }
  
  spec <- ifelse(spec == 0, yes = bias, no = spec) # add bias if there are any zeros
  spec <- spec/sum(spec) * 100 # normalize
  n<-length(spec)
  geo <- prod(spec)^(1/n)
  ari <- mean(spec)
  SF <- geo/ari
  return(1-SF)   # NB: This is to follow convention that 0 is bad and 1 is good
  #SF<- exp((sum(log(norm_spec))/n))/(sum(norm_spec)/n)
  
}


QM_SF_Entropy<-function (spec,bias=0.000001){
  # Function that gives the Spectral flatness measure, more robust than the traditional version
  # Spec can be a subband of the spectrum
  
  if (any(spec < 0)) 
    stop("Data do not have to be in dB")
  
  #Subsample if spec is too large
  if (length(spec) > 400) {
    step <- seq(1, length(spec), by = round(length(spec)/256))
    spec <- spec[step]
  }
  
  spec <- ifelse(spec == 0, yes = bias, no = spec) # add bias if there are any zeros
  spec <- spec/sum(spec) # normalize
  n<-length(spec)
  
  norm_entropy<-spec*log(spec,base=2)/log(n,base=2)
  
  SF<- (2^(-sum(norm_entropy))-1)
  
  return(1-SF) # NB: This is to follow convention that 0 is bad and 1 is good
  
}



QM_V4 <- function(spec)
{
  i_max <- which(spec == max(spec))
  i_sec <- i_max+1
  if(i_max > 1)
    if(spec[i_max-1] > spec[i_max+1])
      i_sec <- i_max-1
  
  max_val <- (2^32)/12
  peak_energy_norm <- min(max_val, spec[i_max] + spec[i_sec])/max_val
  
  rem_energy <- sum(spec[-c(i_max, i_sec)])
  penalty_norm <- min(max_val, rem_energy/(len(spec)-2))/max_val
  
  return(sqrt(peak_energy_norm-penalty_norm))
}


QM_V5 <- function(spec)
{
  i_max <- which(spec == max(spec))
  ip_start <- max(1, i_max-1)
  ip_stop <- min(len(spec), i_max+2)
  pw_len <- ip_stop-ip_start
  
  max_val <- (2^32)/100
  max_val_peak <- max_val*pw_len
  max_val_pen <- max_val*(len(spec)-pw_len)
  
  sum_peak <- sum(spec[ip_start:ip_stop])
  sum_pen <- sum(spec[-c(ip_start:ip_stop)])
  
  peak_energy_norm <- min(max_val_peak, sum_peak)/max_val_peak
  penalty_norm <- min(max_val_pen, sum_pen)/max_val_pen
  
  return(max(0, peak_energy_norm-penalty_norm))
}





