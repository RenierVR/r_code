source("../useful_funs.r")
fpath <- "D:/HealthQ/Data/RRI/GarminValidation/ALL/"


datlist_mojo <- getDataFromDir(paste0(fpath, "mojo_output/"), head=T)
datlist_garm <- getDataFromDir(paste0(fpath, "garmin_output/"), head=T)

# Read reference data
library(edfReader)
datlist_ref <- as.list(getDataFromDir(paste0(fpath, "reference/"), 
                                      dataE=F, ret="full", ext="EDF"))
datlist_names <- unlist(datlist_ref)

for(i in 1:len(datlist_ref)) {
  ref_header <- readEdfHeader(datlist_ref[[i]])
  ref <- readEdfSignals(ref_header)
  ref_rr <- ref$HRV$signal
  ref_rr <- ref_rr[ref_rr != 0]
  
  
  datlist_ref[[i]] <- ref_rr
}
names(datlist_ref) <- basename(datlist_names)




###################################################################################################
###################################################################################################

# AlignToRef

###################################################################################################
###################################################################################################

source("alignToRefRR_v2.r")
source("ERMADT_funs.r")

tpids_ref <- unlist(lapply(strsplit(names(datlist_ref), "_"), function(x) x[grep("TPID", x)]))
tpids_mojo <- unlist(lapply(strsplit(names(datlist_mojo), "_"), function(x) x[grep("TPID", x)]))
tpids_garm <- unlist(lapply(strsplit(names(datlist_garm), "_"), function(x) x[grep("TPID", x)]))


# winsize = 100 ; lag_normal = 10 ; lag_precise = 5
winsize = 100 ; lag_normal = 3 ; lag_precise = 3

outlist <- vector("list", len(tpids_ref))
names(outlist) <- tpids_ref
for(i in 1:len(tpids_ref)) {
  
  rr_ref <- datlist_ref[[i]]
  rr_mojo <- datlist_mojo[[match(tpids_ref[i], tpids_mojo)]]
  rr_garm <- datlist_garm[[match(tpids_ref[i], tpids_garm)]]
  
  rr_mojo2 <- cb(cumsum(rr_mojo[,1])/1000, rr_mojo[,1], rr_mojo[,3])
  rr_garm2 <- cb(cumsum(rr_garm[,1])/1000, rr_garm[,1], rr_garm[,3])
  
  if(i < 10) id <- paste0("0", i)
  
  align_out_mojo <- alignToRefRR(ref = rr_ref, rrmat = rr_mojo2, winsize = winsize,
                                 LAG_RANGE_NORMAL = lag_normal, LAG_RANGE_PRECISE = lag_precise,
                                 ANY_PLOTS = T, PLOT_TYPE = 2, PRINT_OUTPUT = F,
                                 SAVE_PATH = paste0("./results/GarminValidation/mojo plots/", id, "_", tpids_ref[i], "_mojo_segment"))
  
  align_out_garm <- alignToRefRR(ref = rr_ref, rrmat = rr_garm2, winsize = winsize, 
                                 LAG_RANGE_NORMAL = lag_normal, LAG_RANGE_PRECISE = lag_precise,
                                 ANY_PLOTS = T, PLOT_TYPE = 2, PRINT_OUTPUT = F,
                                 SAVE_PATH = paste0("./results/GarminValidation/garmin plots/", id, "_", tpids_ref[i], "_garm_segment"))
  
  outlist[[i]] <- list("MOJO_SUMMARY" = align_out_mojo$FORMATTED_STATS$SUMMARY_STATS[1:2,], 
                       "MOJO_OUTLIERS" = align_out_mojo$ALL_STATS$CONFIDENCE_STATS[4],
                       "GARMIN_SUMMARY" = align_out_garm$FORMATTED_STATS$SUMMARY_STATS[1:2,], 
                       "GARMIN_OUTLIERS" = align_out_garm$ALL_STATS$CONFIDENCE_STATS[4])
  
  
  catProgress(i, len(tpids_ref))
  

}


i <- 1
for(i in 1:10) {
  ol <- outlist[[i]]
  cat(tpids_ref[i], "   |   ",
      ol$MOJO_SUMMARY[1,1], "ms (", ol$MOJO_SUMMARY[1,2], "%)   |   ", 
      ol$GARMIN_SUMMARY[1,1], "ms (", ol$GARMIN_SUMMARY[1,2], "%)\n", sep="")
}



i <- 1
for(i in 1:10) {
  ol <- outlist[[i]]
  cat(tpids_ref[i], "   |   ",
      ol$MOJO_SUMMARY[2,1], "ms (", ol$MOJO_SUMMARY[2,2], "%)   |   ", 
      ol$GARMIN_SUMMARY[2,1], "ms (", ol$GARMIN_SUMMARY[2,2], "%)\n", sep="")
}



for(i in 1:10) {
  ol <- outlist[[i]]
  cat(tpids_ref[i], "   |   ",
      round(ol$MOJO_OUTLIERS[2],1), "%  |  ", round(ol$GARMIN_OUTLIERS[2],1), "%\n", sep="")
}







###################################################################################################
###################################################################################################

# Plot to inspect

###################################################################################################
###################################################################################################




# plotScroll <- function(ref_rr, mojo_rr, garm_rr, lag_ref, lag_mojo, lag_garm)
# {
ref_rr_align <- ref_rr[,]#[-c(1:25),]
mojo_rr_align <- mojo_rr[,]
garm_rr_align <- garm_rr[,]#[-c(1:12),]


n <- max(tail(ref_rr[,1], 1), tail(mojo_rr[,1], 1), tail(garm_rr[,1], 1))

w() ; par(ask = T)#, mfrow = c(2, 1), mar = c(2, 4, 1, 1))
start <- 1 ; winsize <- 200 ; overlap <- 100 ; j <- 0 ; last <- FALSE
if(winsize > n) winsize <- n
while(TRUE){
  
  j = j+1
  plotind <- (overlap*(j-1)+start):(start-1+winsize + overlap*(j-1))
  
  if(max(plotind) > n) {
    last <- TRUE
    plotind <- plotind[1]:n
  }
  
  
  # Determine array indice range corresponding to plot x-axis
  ref_rr_p <- (ref_rr_align[,1] >= plotind[1]) & (ref_rr_align[,1] <= tail(plotind, 1))
  mojo_rr_p <- (mojo_rr_align[,1] >= plotind[1]) & (mojo_rr_align[,1] <= tail(plotind, 1))
  garm_rr_p <- (garm_rr_align[,1] >= plotind[1]) & (garm_rr_align[,1] <= tail(plotind, 1))
  
  # select relevant parts of matrices
  ref_rr_align_p <- ref_rr_align[ref_rr_p,]
  mojo_rr_align_p <- mojo_rr_align[mojo_rr_p,]
  garm_rr_align_p <- garm_rr_align[garm_rr_p,]
  
  
  # Calculate alignment factors
  ccf_mojo <- ccf(ref_rr_align_p[,2], mojo_rr_align_p[,2], lag=60, plot=F)
  ccf_mojo_lag <- ccf_mojo$lag[which.max(abs(ccf_mojo$acf))]
  
  ccf_garm <- ccf(ref_rr_align_p[,2], garm_rr_align_p[,2], type="covariance", lag=60, plot=F)
  ccf_garm_lag <- ccf_garm$lag[which.max(abs(ccf_garm$acf))]
  print(ccf_garm_lag)
  
  # Align
  ref_rr_align_p <- ref_rr_align_p[,]
  mojo_rr_align_p <- mojo_rr_align_p[,]
  garm_rr_align_p <- garm_rr_align_p[,]
  
  ref_rr_align_p[,1] <- cumsum(ref_rr_align_p[,2])/1000
  mojo_rr_align_p[,1] <- cumsum(mojo_rr_align_p[,2])/1000
  garm_rr_align_p[,1] <- cumsum(garm_rr_align_p[,2])/1000
  
  
  
  # Plot
  lplot(ref_rr_align_p[, 1], ref_rr_align_p[, 2], lwd=3, col="red", ylim=c(200, 2000))
  # lines(mojo_rr_align_p[, 1], mojo_rr_align_p[, 2], lwd=2, col="blue")
  lines(garm_rr_align_p[, 1], garm_rr_align_p[, 2], lwd=2, col="mediumseagreen")
  
  # last <- TRUE
  if(last) break
}
# }


# w() ; par(ask = T)#, mfrow = c(2, 1), mar = c(2, 4, 1, 1))
# start <- 1 ; winsize <- 200 ; overlap <- 100 ; j <- 0 ; last <- FALSE
# if(winsize > n) winsize <- n
# while(TRUE){
#   
#   j = j+1
#   plotind <- (overlap*(j-1)+start):(start-1+winsize + overlap*(j-1))
#   
#   if(max(plotind) > n) {
#     last <- TRUE
#     plotind <- plotind[1]:n
#   }
#   
#   
#   ref_rr_p <- (ref_rr[,1] >= plotind[1]) & (ref_rr[,1] <= tail(plotind, 1)) 
#   ref_rr_align_p <- ref_rr[ref_rr_p,]
#   lplot(ref_rr_align_p[, 1], ref_rr_align_p[, 2], lwd=3, col="red", ylim=c(200, 2000),
#         xlab="time (s)", ylab="RR value (ms)")
#   
#   # last <- TRUE
#   if(last) break
# }









