source("../useful_funs.r")


fnames <- paste0("20150205_", c(rep("mojo5113", 6), rep("mojo5106", 6)), "_HRV_",
                 c("Andre", "Kari", "Marc", "Nic", "Renier", "Tiaan"))

fnames2 <- paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/physcalc_out_", fnames, ".txt")

fnames3 <- paste0("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/data/HRV_validation2/", fnames, ".txt")




###################################################################################################
###################################################################################################
###################################################################################################


c_out_hr <- scan("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/out_hrv_rr.txt", sep=",")
splits <- which(c_out_hr == 1001)
c_hr_list <- vector("list", len(splits))
for(i in 2:(len(splits)+1)){
  if(i <= len(splits))
    c_hr_list[[i-1]] <- c_out_hr[(splits[i-1]+1):(splits[i]-1)]
  else
    c_hr_list[[i-1]] <- c_out_hr[(splits[i-1]+1):(len(c_out_hr)-1)]
}




w() ; par(ask=T, mar=c(3,4,2,4))
for(i in 1:len(fnames)){
  
  out <- read.table(fnames2[i], sep=",")  
  x_1hz <- seq(1, nrow(out), 50)
  out_hr <- out[x_1hz,1]
  out_rr <- out[which(diff(out[,3]) != 0)+1,3]
  out_rrbad <- out[which(diff(out[,3]) != 0)+1,4]
  
  out_rr_hr <- c(rep(0, sum(out_rr==0)), rr_ma(out_rr[out_rr != 0], out_rrbad[out_rr != 0]))

  lplot(out_rr, ylim=c(500,1500), ann=F, yaxt="n", xaxt="n") ; axis(4)
  abline(v=which(out_rrbad==1), lwd=2, col="green")
  
  par(new=T) ; lplot(out_hr, lwd=2, col="red", ylim=c(40,120), ylab="bpm")
  lines(c_hr_list[[i]], lwd=2, col="blue")
  lines(out_rr_hr, lwd=2, col="orange", lty=2)
  
}




rr_ma <- function(x, z, ord=7, alpha=0.3)
{
  y <- NULL
  for(i in ord:len(x)){
    xt <- x[(i-ord+1):i][z[(i-ord+1):i] == 0]
    y <- c(y, mean(xt))
  }
  
  y <- 60/(y/1000)
  
  
  for(i in 2:len(y))
    y[i] <- alpha*y[i] + (1-alpha)*y[i-1]
  
  return(y)
}




###################################################################################################
###################################################################################################
###################################################################################################


hrv_first <- scan("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/out_hrv_1st_rr.txt", sep=",")
hrv_peaks <- scan("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/out_hrv_peaks.txt", sep=",")
hrv_test <- read.table("~/HealthQ/C FILES/LIFEQINSIDE_Data_Interface/out_testing.txt", sep=",")

w() ; par(ask=T)
for(i in 1:len(fnames)){
  
  out <- read.table(fnames2[i], sep=",") 
  dat <- read.table(fnames3[i], sep=",")
  
  hrv_1 <- 50*(hrv_first[i]/1000)

  pindex <- hrv_1 + c(-200,200)
  pindex[1] <- max(1, pindex[1])
  pindex <- pindex[1]:pindex[2]
  
  pindex <- 1500:1800
  w() ; lplot(pindex, hrv_test[pindex,2])
  abline(v=hrv_peaks-47, lwd=2, col="red")
  
}



