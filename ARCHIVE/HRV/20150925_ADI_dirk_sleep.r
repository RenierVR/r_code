source("../useful_funs.r")
source("./ERMADT_funs.r")
source("../SpO2/sp02_funs.r")


###############################################################################################
#########################      ADI INITIAL TESTING - DIRK SLEEP      ##########################
###############################################################################################

dat <- read.table("D:/HealthQ/Data/ADI/20150925 DIRK SLEEP/20150925_0136_033959c960a8235045680d0ad8f8d18d_EXERCISE.csv", head=T, sep=",")


seg_seq <- seq(1, nrow(dat), 7500)
seg_ind_valid <- c(2:7, 17:22) + 1


for(i in 2:len(seg_seq)) {
  
  if(any(i == seg_ind_valid)) {
    
    ## extract segment
    seg <- dat[seg_seq[i-1]:seg_seq[i], 1:2]
    seg <- seg[250:(nrow(seg)-250),]
    
    
    
    
    # ############ #
    # RR ALGORITHM #
    # ############ # ##############################################################################
    
    Fs <- 25
    
    hrv_out1 <- ER_MA_DT_peaks_C(seg[,1], filt_t="FIRHAM", clip=4*Fs, bf_fs=Fs, output=T, interp=T,
                                check_mult_peaks_above_zero=T, post_filt_running_mean=F)
    rr1 <- hrv_out1$RR
    
    hrv_out2 <- ER_MA_DT_peaks_C(seg[,2], filt_t="FIRHAM", clip=4*Fs, bf_fs=Fs, output=T, interp=T,
                                 check_mult_peaks_above_zero=T, post_filt_running_mean=F)
    rr2 <- hrv_out2$RR
    
    
    
    # ############## #
    # SPO2 ALGORITHM #
    # ############## # ############################################################################
    
    out <- fftSpO2_Cdev(seg[,2], seg[,1], Fs=25, filtType="fir50", 
                        windowSize=2^7, subtractMeans=T, useMaxDenom=T,
                        smoothACDC=0.5, spo2_check=F, spo2_caleb_pf = 0.3,
                        spo2_new_method = F, varSmoothACDC = T,
                        print_progress = FALSE)
    
    smoothA <- rep(0.1, len(out$cor_qm_smoothed))
    smoothA[out$cor_qm_smoothed > 0.9] <- 0.5 
    
    spo2  <- 100*calabFreeSpO2(out[[1]], clip=F, smoothAlpha=smoothA)
    spo2_cqm <- out$cor_qm_smoothed
    
    
    
    # ######## #
    # PLOTTING #
    # ######## # ##################################################################################
    
    
    w() ; lplot(seg[1000:2000, 1], lwd=2, col="blue", ylab="",
                main = paste0("ADI dirk sleep seg ", i-1, " | raw red and infared"))
    par(new=T)
    lplot(seg[1000:2000, 2], lwd=2, col="red", ann=F, yaxt="n", yaxt="n")
    axis(4)
    savePlot(paste0("./results/ADI/ADI_dirk_sleep_seg_", i-1, "raw_red_ir.png"), "png")
    graphics.off()
    
    
    w(28, 20) ; par(mar=c(4,4,1,4), mfrow=c(3,1))
    
  
    lplot(seg[,1], lwd=2, col="blue", 
          xlab="index", ylab="red / infrared", ylim=range(seg[,1:2]))
    lines(seg[,2], lwd=2, col="red")
    
    
    lplot(cumsum(rr1)/1000, rr1, lwd=2, col="blue", ylim=range(c(rr1, rr2)),
          xlab="time (s)", ylab="RR interval (ms)")
    lines(cumsum(rr2)/1000, rr2, lwd=2, col="red")
    legend("topright", lwd=2, col=c("red", "blue"), leg=c("red", "infrared"))
    

    plot(spo2_cqm, type="n", ann=F, yaxt="n", xaxt="n", ylim=c(50,100))
    abline(v=which(spo2_cqm > 92.5), col=rgb(1, 1, 128/255), lwd=4)
    par(new=T) ; plot(spo2, type="n", xlab="time (s)", ylab="SpO2 (%)", ylim=c(75,105))
    abline(h=seq(94, 100, 0.1), col="lightgreen")
    par(new=T)
    
    lplot(spo2_cqm, lwd=2, col="orange", ann=F, yaxt="n", xaxt="n", ylim=c(50,100))
    axis(4) ; par(new=T)
    lplot(spo2, lwd=3, col="forestgreen", ann=F, yaxt="n", xaxt="n", ylim=c(75,105))
    legend("bottomright", lwd=2, col=c("forestgreen", "orange"), leg=c("SpO2", "QM"), bg="white")
    

    
    
    savePlot(paste0("./results/ADI/ADI_dirk_sleep_seg_", i-1, ".png"), "png")
    graphics.off()
    
  }
  
  catProgress(i-1, len(seg_seq)-1)
}



