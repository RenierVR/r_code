source("ERMADT_funs.r")


write.table(cbind(aa[,1], aa[,2], matrix(0, nc=3, nr=nrow(aa))), 
            file="~/HealthQ/C FILES/PeakPredictionDetection_project/data/format2/mojo_hrvtest.txt",
            sep=",", quote=F, row.names=F, col.names=F)

aa <- read.table("~/HealthQ/C FILES/PeakPredictionDetection_project/data/format2/mojo_hrvtest.txt", sep=",")


c_out <- read.table("~/HealthQ/C FILES/PeakPredictionDetection_project/out_testing.txt", sep=",")
c_rr  <- read.table("~/HealthQ/C FILES/PeakPredictionDetection_project/output/out_pd_mojo_hrvtest.txt", sep=",")
c_out <- read.table("~/HealthQ/C FILES/PeakPredictionDetection_project/out_ermatd_filt.txt", sep=",")

grn <- aa[,1] - aa[,2]



pind  <- 5000:6000
grn   <- removeAGCJumps(grn)
c_grn <- c_out[,1]
c_pks <- c_out[c_out[,2] != 0,2] - 45
w()
#lplot(pind, grn[pind], lwd=2)
#par(new=T)
lplot(pind, c_grn[pind], col="blue", lwd=2)
abline(v=c_pks, col="red")


###############################################################


r_out <- ER_MA_DT_peaks_C(grn, filt_type="FIR50", output_all=T)
r_grn <- sqrt(r_out$clipped_signal)
r_pks <- r_out$peaks 

w() ; lplot(pind, cg2[pind])
abline(v=c_pks, col="red")




###############################################################
###############################################################
###############################################################

fdirs_24 <- paste0("D:/HealthQ/Data/HRV/2014 HRV Validation/", 
                c("20141124_test1/", "20141124_test2/", "20141124_test3/"))
fdirs_25 <- paste0("D:/HealthQ/Data/HRV/2014 HRV Validation/", 
                   c("20141125_test1_R/", "20141125_test2_R/", "20141125_test3_R/",
                     "20141125_test4_K/", "20141125_test5_K/"))
fdirs_27 <- paste0("D:/HealthQ/Data/HRV/2014 HRV Validation/", 
                   c("20141127_test6_D/", "20141127_test7_D/"))
fdirs_28 <- paste0("D:/HealthQ/Data/HRV/2014 HRV Validation/", 
                   c("20141128_test8_R/"))
fdirs_01 <- paste0("D:/HealthQ/Data/HRV/2014 HRV Validation/",
                   c("20141201_test1_M/", "20141201_test2_M/"))
fdirs_08 <- paste0("D:/HealthQ/Data/HRV/2014 HRV Validation/",
                   c("20141208_test1_R/", "20141208_test2_R/"))

fdirs_18 <- paste0("D:/HealthQ/Data/HRV/2014 HRV Validation/", c("20141218_test1_R/"))


fdirs <- c(fdirs_24, fdirs_25, fdirs_27, fdirs_28, fdirs_01, fdirs_08, fdirs_18)


i <- 6

dat <- read.table(paste0(fdirs[i], "mojo.csv"), sep=",", head=T)


# aa <- cbind(dat[,1], dat[,2])
# write.table(cbind(aa[,1], aa[,2], matrix(0, nc=3, nr=nrow(aa))), 
#             file="~/HealthQ/C FILES/PeakPredictionDetection_project/data/format2/mojo_hrvtest.txt",
#             sep=",", quote=F, row.names=F, col.names=F)



grn <- removeAGCJumps( dat[,1]-dat[,2] )




### compare RR intervals: C against R
######################################

c_rr  <- read.table(paste0(fdirs[i], "out_pd_mojo_hrvtest.txt"), sep=",")[,1]

r_out <- ER_MA_DT_peaks_C(grn, filt_type="FIR50", output_all=T)
r_pks <- r_out$peaks
r_rr  <- round(20*diff(r_pks),0)

# if(i == 1) zPlot(cbind(c_rr, r_rr[-c(1:40)]), c("C", "R"))
# if(i == 2) zPlot(cbind(c_rr, r_rr[-c(1:15)]), c("C", "R"))
# if(i == 3) zPlot(cbind(c_rr, r_rr[-c(1:23)]), c("C", "R"))

if(i == 1){  c_plot <- c_rr ; r_plot <- r_rr[-c(1:40)] }
if(i == 2){  c_plot <- c_rr ; r_plot <- r_rr[-c(1:15)] }
if(i == 3){  c_plot <- c_rr ; r_plot <- r_rr[-c(1:23)] }
if(i == 4){  c_plot <- c_rr ; r_plot <- r_rr[-c(1:33)] }
if(i == 5){  c_plot <- c_rr ; r_plot <- r_rr[-c(1:2)] }
if(i == 6){  c_plot <- c_rr ; r_plot <- r_rr[-c(1:1)] }
if(i == 7){  c_plot <- c_rr ; r_plot <- r_rr[-c(1:33)] }
if(i == 8){  c_plot <- c_rr[-c(1:3)] ; r_plot <- r_rr[-c(1:1)] }
if(i == 9){  c_plot <- c_rr ; r_plot <- r_rr[-c(1:18)] }
if(i == 10){  c_plot <- c_rr ; r_plot <- r_rr[-c(1:17)] }
if(i == 11){  c_plot <- c_rr ; r_plot <- r_rr[-c(1:17)] }
if(i == 12){  c_plot <- c_rr ; r_plot <- r_rr[-c(1:6)] }
if(i == 13){  c_plot <- c_rr ; r_plot <- r_rr[-c(1:3)] }
if(i == 14){  c_plot <- c_rr ; r_plot <- r_rr[-c(1:2)] }
if(i == 15){  c_plot <- c_rr ; r_plot <- r_rr[-c(1:2)] }

w() ; lplot(cumsum(c_plot)/1000, c_plot, ylim=c(100,2000), lwd=2)
lines(cumsum(r_plot)/1000, r_plot, lwd=2, col="blue")
legend("topright", col=c("mediumseagreen", "blue"), leg=c("C", "R"), lwd=2)




### compare RR intervals: C against ECG
########################################

e_out <- read.table(paste0(fdirs[i], "ecg_peaks.txt"))[,1]
e_rr  <- diff(e_out)

# if(i == 1) zPlot(cbind(c_rr, e_rr[-c(1:5)]), c("C", "ECG"), col_f=c("mediumseagreen", "red"))
# if(i == 2) zPlot(cbind(c_rr, e_rr[-c(1:5)]), c("C", "ECG"), col_f=c("mediumseagreen", "red"))
# if(i == 3) zPlot(cbind(c_rr, e_rr[-c(1:7)]), c("C", "ECG"), col_f=c("mediumseagreen", "red"))

if(i == 1){  c_plot <- c_rr ; e_plot <- e_rr[-c(1:7)] }
if(i == 2){  c_plot <- c_rr ; e_plot <- e_rr[-c(1:7)] }
if(i == 3){  c_plot <- c_rr ; e_plot <- e_rr[-c(1:7)] }
if(i == 4){  c_plot <- c_rr ; e_plot <- e_rr[-c(1:12)] }
if(i == 5){  c_plot <- c_rr ; e_plot <- e_rr[-c(1:6)] }
if(i == 6){  c_plot <- c_rr ; e_plot <- e_rr[-c(1:8)] }
if(i == 7){  c_plot <- c_rr ; e_plot <- e_rr[-c(1:28)] }
if(i == 8){  c_plot <- c_rr ; e_plot <- e_rr[-c(1:15)] }
if(i == 9){  c_plot <- c_rr ; e_plot <- e_rr[-c(1:91)] }
if(i == 10){  c_plot <- c_rr ; e_plot <- e_rr[-c(1:16)] }
if(i == 11){  c_plot <- r_rr[-c(1:5)] ; e_plot <- e_rr[-c(1:1)] }
if(i == 12){  c_plot <- c_rr[-c(1:3)] ; e_plot <- e_rr[-c(1:5)] }
if(i == 13){  c_plot <- c_rr[-c(1:3)] ; e_plot <- e_rr[-c(1:3)] }
if(i == 14){  c_plot <- c_rr[-c(1:3)] ; e_plot <- e_rr[-c(1:96)] }
if(i == 15){  c_plot <- c_rr[-c(1:3)] ; e_plot <- e_rr[-c(1:4)] }
if(i == 16){  c_plot <- c_rr[-c(1:3)] ; e_plot <- e_rr[-c(1:6)] }

e_plot <- e_plot[1:len(c_plot)]

w() ; lplot(cumsum(c_plot)/1000, c_plot, ylim=c(500,1700), col="blue",
            lwd=2, xlab="time (s)", ylab="RR interval length (ms)")
lines(cumsum(e_plot)/1000, e_plot, lwd=2, col="red")
legend("bottomright", col=c("blue", "red"), leg=c("PPG", "ECG"), lwd=2)
# title(main=c("RR Tachogram\nHRV validation test 2014-11-27 (2): Dirk"))

n_init <- substr(fdirs[i], nchar(fdirs[i])-1, nchar(fdirs[i])-1)
if(n_init == "R") n_init <- "Renier"
if(n_init == "D") n_init <- "Dirk"
if(n_init == "M") n_init <- "Marc"
n_dat <- substr(fdirs[i], 26, 33)
n_num <- substr(fdirs[i], 39, 39)
title(main=paste0("RR Tachogram\nHRV validation test ", n_dat, " (", n_num, "): ", n_init))



### compare RR intervals: ECG against BH
##########################################

bh_rr <- read.table(paste0(fdirs[i], "RR.csv"), head=T)[-c(1:1),1]
e_rr_plot <- e_rr[-c(1:1)]

w() ; lplot(cumsum(bh_rr)/1000, bh_rr, ylim=c(100,2000), lwd=2, col="blue")
lines(cumsum(e_rr_plot)/1000, e_rr_plot, lwd=2, col="red")
legend("topright", col=c("red", "blue"), leg=c("ECG", "BH"), lwd=2)




# w() ; lplot(cumsum(bh_rr)/1000, bh_rr, ylim=c(100,2000), lwd=2, col="blue")
# lines(cumsum(r_rr)/1000, r_rr, lwd=2, col="red")
# legend("topright", col=c("red", "blue"), leg=c("R", "BH"), lwd=2)






