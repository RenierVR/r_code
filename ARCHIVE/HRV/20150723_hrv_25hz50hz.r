source("../useful_funs.r")
source("./ERMADT_funs.r")





t <- seq(0, 450, len=450*50)
x <- 1000 + 100*sin(2*pi*t[1:7500])
x <- c(x, rep(1000, 7500))
# x <- c(x, 1000 + 100*sin(2*pi*t[7501:15000]))
x <- c(x, 1000 + 100*sin(2*pi*t[15001:22500]))


rr1_out <- ER_MA_DT_peaks_C(x, filt="FIR50", clip=200, output=T, interp=F)
rr1 <- diff(1000*rr1_out$peaks/50)

# rr2_out <- ER_MA_DT_peaks_C(x[seq(1,len(x1),2)], filt="FIR50", bf_fs=25, clip=100, output=T)
# rr2 <- diff(1000*rr1_out$peaks/25)


# t <- seq(0, 1200, len=1200*50)
# x <- 1000 + 100*sin(2*pi*t)


y <- convertToCurrent3(x, 1, 0)

xdat <- cbind(2^15, 2^15, 2^15, x, 2^15, 0, 0, 1, 1, 0, 0, 0, 0)
xdat2 <- xdat[seq(1, nrow(xdat), 2), ]

xdat[,4] <- 1*xdat[,4]
xdat2[,4] <- 1*xdat2[,4]


# write.table2(xdat, file="~/HealthQ/C files/LIFEQINSIDE_Data_Interface/data/HRV2/trivial_input_test_50hz.txt")
# write.table2(xdat2, file="~/HealthQ/C files/LIFEQINSIDE_Data_Interface/data/HRV2/trivial_input_test_25hz.txt")


###################################################################################################
###################################################################################################


cout1 <- read.table("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/physcalc_out_trivial_input_test_50hz.txt", sep=",")
cout2 <- read.table("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/physcalc_out_trivial_input_test_25hz.txt", sep=",")


chrv1 <- cout1[,10:15]
chrv2 <- cout2[,10:15]
chrv <- list(chrv1, chrv2)

crr <- lapply(chrv, function(x) x[x[,1]==1, 2:3])

# w() ; par(ask=T) ; lapply(crr, function(x) lplot(x[-1,1], lwd=2))


crr1 <- crr[[1]][,1]

zPlot(cbind(rr1[-len(rr1)], crr1[-1]))

###################################################################################################







cout <- read.table("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/physcalc_out_trivial_input_test_50hz.txt", sep=",")
ctest <- read.table("~/HealthQ/C files/LIFEQINSIDE_Data_Interface/out_testing.txt", sep=",", head=T)


chrv <- cout[,10:15]
chrv <- chrv[chrv[,1]==1, 2:3]
crr <- chrv[,1]






