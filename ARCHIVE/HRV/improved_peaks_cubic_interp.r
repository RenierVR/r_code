# cubic_interp <- function(x, x0, x1, x2, x3)
# {
#   t0 <- x0[2]/((x0[1] - x1[1])*(x0[1] - x2[1])*(x0[1] - x3[1]))
#   t1 <- x1[2]/((x1[1] - x0[1])*(x1[1] - x2[1])*(x1[1] - x3[1]))
#   t2 <- x2[2]/((x2[1] - x0[1])*(x2[1] - x1[1])*(x2[1] - x3[1]))
#   t3 <- x3[2]/((x3[1] - x0[1])*(x3[1] - x1[1])*(x3[1] - x2[1]))
#   
#   t0x <- (x - x1[1])*(x - x2[1])*(x - x3[1])
#   t1x <- (x - x0[1])*(x - x2[1])*(x - x3[1])
#   t2x <- (x - x0[1])*(x - x1[1])*(x - x3[1])
#   t3x <- (x - x0[1])*(x - x1[1])*(x - x2[1])
#   
#   return(t0*t0x + t1*t1x + t2*t2x + t3*t3x)
# }
# 
# 
# 
# 
# w() ; plot(0,0, type="n", xlim=c(1,10), ylim=c(1,10))
# 
# 
# xx <- seq(1, 10, 0.1)
# lines(xx, cubic_interp(xx, mxy[1,], mxy[2,], mxy[3,], mxy[4,]))


#########################


improvePeak <- function(m_xy)
{
  xx <- m_xy[,1]
  
  x0 <- m_xy[1,]
  x1 <- m_xy[2,]
  x2 <- m_xy[3,]
  x3 <- m_xy[4,]
  
  t0 <- x0[2]/((x0[1] - x1[1])*(x0[1] - x2[1])*(x0[1] - x3[1]))
  t1 <- x1[2]/((x1[1] - x0[1])*(x1[1] - x2[1])*(x1[1] - x3[1]))
  t2 <- x2[2]/((x2[1] - x0[1])*(x2[1] - x1[1])*(x2[1] - x3[1]))
  t3 <- x3[2]/((x3[1] - x0[1])*(x3[1] - x1[1])*(x3[1] - x2[1]))
  
  A <- 3*(t0 + t1 + t2 + t3)
  B <- -2*(t0*(xx[2]+xx[3]+xx[4]) + t1*(xx[1]+xx[3]+xx[4]) + t2*(xx[1]+xx[2]+xx[4]) + t3*(xx[1]+xx[2]+xx[3]))
  C <- t0*(xx[2]*xx[4] + xx[3]*xx[4] + xx[2]*xx[3]) + t1*(xx[1]*xx[3] + xx[1]*xx[4] + xx[3]*xx[4]) + 
    t2*(xx[1]*xx[2] + xx[1]*xx[4] + xx[2]*xx[4]) + t3*(xx[1]*xx[2] + xx[1]*xx[3] + xx[2]*xx[3])
  
#   cat("A =", A)
#   cat("\nB = ", B)
#   cat("\nC = ", C, "\n\n")
  

  D <- B^2 - 4*A*C

  if(D > 0 & (A > 1e-10 | A < -1e-10)){
    sol <- (-B + c(-1,1)*sqrt(D))/(2*A)
    x <- sol[sol > xx[1] & sol < xx[4]]
  }
  else{
    y_max <- max(m_xy[,2])[1]
    x <- m_xy[which(m_xy[,2] == y_max),1]
  }
  
#   cat("sol: ")
#   print(sol)
  
  t0x <- (x - x1[1])*(x - x2[1])*(x - x3[1])
  t1x <- (x - x0[1])*(x - x2[1])*(x - x3[1])
  t2x <- (x - x0[1])*(x - x1[1])*(x - x3[1])
  t3x <- (x - x0[1])*(x - x1[1])*(x - x2[1])
  y <- t0*t0x + t1*t1x + t2*t2x + t3*t3x
  
  return(c(x,y))
}



# grn <- out_init[,3]
# pks <- out_pksid[,1]+1


getInterpPeaks <- function(grn, pks)
{
  imp_pks <- NULL
  for(i in pks){
    
    if(i == 1) {
      imp_pk <- i
    }
    else {
      if(grn[i-1] < grn[i+1]){
        isq <- (i-1):(i+2)
      }
      else
        isq <- (i-2):(i+1)
  
#   isq2 <- (i-2):(i+2)
#   print(paste(isq2, collapse=", "))
#   print(paste(grn[isq2], collapse=", "))
  
      imp_pk <- improvePeak(cbind(isq, grn[isq]))[1]
      if(is.na(imp_pk))
        imp_pk <- i
    }
  
    imp_pks <- c(imp_pks, imp_pk)
  
  }

  imp_pks <- as.numeric(imp_pks)
  
  return(cbind(imp_pks, grn[round(imp_pks)]))
}





##############################################################################3

# pks_hr <- cbind(pks[-1], 60/(diff(pks)*0.02))
# imp_pks_hr <- cbind(imp_pks[-1], 60/(diff(imp_pks)*0.02))
# 
# 
# write.table(cbind(imp_pks), file="~/HealthQ/C FILES/PeakPredictionDetection_project/eugene_slaap_peaks.txt",
#             quote=F, row.names=F, col.names=F)


##############################################################################

# Fs <- 1
# write.table(cbind(imp_pks/Fs), file="~/HealthQ/C FILES/PeakPredictionDetection_project/interp_peak_times.txt",
#             quote=F, row.names=F, col.names=F)
