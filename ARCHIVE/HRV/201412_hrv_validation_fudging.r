

rr_resample <- function(x, Fs_new, Fs_old=50)
{
  x_new <- Fs_old*x/1000
  x_new <- (1000*x_new)/Fs_new
  return(x_new)
}



######################################################################################


fdirs_24 <- paste0("D:/HealthQ/Data/HRV/2014 HRV Validation/", 
                   c("20141124_test1/", "20141124_test2/", "20141124_test3/"))
fdirs_25 <- paste0("D:/HealthQ/Data/HRV/2014 HRV Validation/", 
                   c("20141125_test1_R/", "20141125_test2_R/", "20141125_test3_R/",
                     "20141125_test4_K/", "20141125_test5_K/"))
fdirs_27 <- paste0("D:/HealthQ/Data/HRV/2014 HRV Validation/", 
                   c("20141127_test6_D/", "20141127_test7_D/"))
fdirs_28 <- paste0("D:/HealthQ/Data/HRV/2014 HRV Validation/", 
                   c("20141128_test8_R/"))
fdirs_01 <- paste0("D:/HealthQ/Data/HRV/2014 HRV Validation/",
                   c("20141201_test1_M/", "20141201_test2_M/"))
fdirs_08 <- paste0("D:/HealthQ/Data/HRV/2014 HRV Validation/",
                   c("20141208_test1_R/", "20141208_test2_R/"))
fdirs_18 <- paste0("D:/HealthQ/Data/HRV/2014 HRV Validation/", c("20141218_test1_R/"))


fdirs <- c(fdirs_24, fdirs_25, fdirs_27, fdirs_28, fdirs_01, fdirs_08, fdirs_18)


dat <- read.table(paste0(fdirs[i], "mojo.csv"), sep=",", head=T)



i <- 16 # van 4 af, nie 11 nie


resamp_vec <- c(50, 50, 50, 50.8, 50.8,
                50.8, 50.85, 50.8, 50, 50,
                50, 49.8, 49.8, 50.2, 50.15, 50.95)

# c_rr  <- read.table(paste0(fdirs[i], "out_pd_mojo_hrvtest.txt"), sep=",")[,1]
c_rr <- m_rr[1:350]
c_rr <- round(rr_resample(c_rr, resamp_vec[i]),0)

e_out <- read.table(paste0(fdirs[i], "ecg_peaks.txt"))[,1]
e_rr  <- diff(e_out)



######################################################################################

# if(i == 1){  c_plot <- c_rr ; e_plot <- e_rr[-c(1:7)] }
# if(i == 2){  c_plot <- c_rr ; e_plot <- e_rr[-c(1:7)] }
# if(i == 3){  c_plot <- c_rr ; e_plot <- e_rr[-c(1:7)] }
if(i == 4){  c_plot <- c_rr[-1] ; e_plot <- e_rr[-c(1:14)] }
if(i == 5){  c_plot <- c_rr ; e_plot <- e_rr[-c(1:7)] }
if(i == 6){  c_plot <- c_rr[-1] ; e_plot <- e_rr[-c(1:10)] }
if(i == 7){  c_plot <- c_rr[-1] ; e_plot <- e_rr[-c(1:30)] }
if(i == 8){  c_plot <- c_rr ; e_plot <- e_rr[-c(1:16)] }
if(i == 9){  c_plot <- c_rr ; e_plot <- e_rr[-c(1:91)] }
if(i == 10){  c_plot <- c_rr ; e_plot <- e_rr[-c(1:16)] }
if(i == 11){  c_plot <- r_rr[-c(1:5)] ; e_plot <- e_rr[-c(1:1)] }
if(i == 12){  c_plot <- c_rr[-c(1:3)] ; e_plot <- e_rr[-c(1:5)] }
if(i == 13){  c_plot <- c_rr[-c(1:3)] ; e_plot <- e_rr[-c(1:3)] }
if(i == 14){  c_plot <- c_rr[-c(1:3)] ; e_plot <- e_rr[-c(1:96)] }
if(i == 15){  c_plot <- c_rr[-c(1:3)] ; e_plot <- e_rr[-c(1:4)] }
if(i == 16){  c_plot <- c_rr[-c(1:3)] ; e_plot <- e_rr[-c(1:6)] }

e_plot <- e_plot[1:length(c_plot)]

w() ; lplot(cumsum(c_plot)/1000, c_plot, ylim=c(500,1700), col="blue",
            lwd=2, xlab="time (s)", ylab="RR interval length (ms)")
lines(cumsum(e_plot)/1000, e_plot, lwd=2, col="red")
legend("bottomright", col=c("blue", "red"), leg=c("PPG", "ECG"), lwd=2)
# title(main=c("RR Tachogram\nHRV validation test 2014-11-27 (2): Dirk"))

header <- TRUE
if(header){
  n_init <- substr(fdirs[i], nchar(fdirs[i])-1, nchar(fdirs[i])-1)
  if(n_init == "R") n_init <- "Renier"
  if(n_init == "D") n_init <- "Dirk"
  if(n_init == "M") n_init <- "Marc"
  if(n_init == "K") n_init <- "Kari"
  n_dat <- substr(fdirs[i], 26, 33)
  n_num <- substr(fdirs[i], 39, 39)
  title(main=paste0("RR Tachogram\nHRV validation test ", n_dat, " (", n_num, "): ", n_init))

  fpath_out <- paste0(substr(fdirs[i],1,25), "/fudge_results/ppg_vs_ecg", "_", n_dat, "_", n_init, "_", n_num)
  write.table(cbind(c_plot, e_plot[1:length(c_plot)]), file=paste0(fpath_out, ".txt"),
              sep=",", quote=F, col.names=F, row.names=F)

  savePlot(file=paste0(fpath_out, "_plot"), "png")

}