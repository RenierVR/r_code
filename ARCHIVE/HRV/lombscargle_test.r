library(lomb)



###############################################################################
####################     eugene python example recreate     ###################
###############################################################################

#phi  <- 0.5*pi
Nin  <- 100
Nout <- 1000
frac <- 0.3

# r <- runif(Nin)
# x <- seq(0.01, 5*pi, len=Nin)
# x <- x[r >= frac]
x <- round(x, 5)

y <- 2*sin(x+phi)
y <- round(y,5)

f <- seq(0.01, 10, len=Nout)

lsp_out <- lsp(x=cbind(x,y), from=f[1], to=f[len(f)], freq_len=1000, plot=F)


w() ; par(mfrow=c(2,1), mar=c(3,3,1,1))
layout(matrix(c(1,2), 2, 1, byrow=T), heights=c(1,2))
lplot(x, y)
points(x, y, pch=19, cex=0.75)
lplot(f, lsp_out$power)
lines(f, cc_pow, col="red")


cc <- read.table("~/HealthQ/C FILES/PeakPredictionDetection_project/out_testing.txt", sep=",")
cc_pow <- cc[,2]/2 #sqrt(4*cc[,2]/66)

zPlot(cbind(lsp_out$power, sqrt(8*cls1[,2]/66)^2))



###############################################################################
##########################     C vs R comparison     ##########################
###############################################################################

# w() ; par(ask=T)
# r_lsp_list <- vector("list", 4)
# for(i in 1:4){
# cout  <- read.table(paste0("~/HealthQ/C FILES/PeakPredictionDetection_project/output/out_pd_", hrv_names[i]), sep=",")[,1]
# 
# f <- seq(0.0001591549, 0.5, len=1000)
# 
# rr <- cout[]
# rr_t <- cumsum(rr)/1000
# # rrmean <- rr[1]
# # for(j in 2:len(rr))
# #   rrmean <- c(rrmean, rrmean[j-1]*((j-1)/j) + rr[j]*(1/j))
# # rr <- rr - rrmean
# rr <- rr-mean(rr)
# 
# #w()
# lsp_out <- lsp(cbind(rr_t, rr), from=2*pi*f[1], to=2*pi*f[len(f)], freq_len=1000, plot=F)
# r_lsp_list[[i]] <- lsp_out[[2]]
# }



ls_py <- read.table("~/../PycharmProjects/Py_GettingStarted/ls_py_results.txt", sep=",")


######################################

cls <- read.table("~/HealthQ/C FILES/PeakPredictionDetection_project/out_ls.txt", sep=",")
ff  <- seq(0.001, pi, len=1000)

all(round(cls[,1] - ff, 4) == 0)


zPlot(cbind(cls[,2], ls_py[,2]), par_new=T)


ff_hz   <- ff/(2*pi)
hrv_vlf <- which((ff_hz > 0.0033) & (ff_hz < 0.04))
hrv_lf  <- which((ff_hz > 0.04) & (ff_hz < 0.15))
hrv_hf  <- which((ff_hz > 0.15) & (ff_hz < 0.4))

( paste(hrv_vlf_sum <- sum(cls[hrv_vlf,2]/1000)) )
( paste(hrv_lf_sum  <- sum(cls[hrv_lf,2]/1000))  )
( paste(hrv_hf_sum  <- sum(cls[hrv_hf,2]/1000))  )

# > 0.0033*2*pi
# [1] 0.02073451
# > 0.04*2*pi
# [1] 0.2513274
# > 0.15*2*pi
# [1] 0.9424778
# > 0.4*2*pi
# [1] 2.513274
