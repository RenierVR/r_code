library(ROCR)

calc_auc_ROCR <- function(prob, true) 
  return(performance(prediction(prob, as.factor(true)), measure="auc")@y.values[[1]])


runSupervisedCV <- function(dat,
                            model_type = c("GLMNET", "GLMNET_ORDINAL", "RF")[1],
                            CV_K = 5,
                            CV_SEED = 0,
                            CV_STRATIFY_FOLDS = TRUE,
                            CV_TRAIN_RESAMPLE = TRUE,
                            PRINT_PROGRESS = TRUE,
                            GLMNET_OUT_DIR = "./cv_glmnet output/",
                            GLMNET_RUN_CV = FALSE,
                            ...)
{
  
  
  m <- len(unique(dat[,1]))
  
  
  # Remove rows with NA
  ######################
  which_rows_NA <- apply(dat, 1, function(x) any(is.na(x)))
  dat <- dat[!which_rows_NA, ]
  
  
  # Vector importance
  #####################
  if(model_type %in% c("GLMNET", "GLMNET_ORDINAL")) {
    var_select_counts <- matrix(0, nrow = (ncol(dat)-1), ncol=CV_K)
    rownames(var_select_counts) <- names(dat)[-1]
    
    var_coeffs_counts <- matrix(0, nrow = (ncol(dat)-1), ncol=CV_K)
    rownames(var_coeffs_counts) <- names(dat)[-1]
  }

  var_imp_list <- vector("list", CV_K)
  
  
  # Partition data into folds
  ############################
  set.seed(CV_SEED)
  
  if(!CV_STRATIFY_FOLDS) {
    folds <- sample(rep(1:CV_K, each=ceiling(nrow(dat)/CV_K)))[1:nrow(dat)]
    dat$FOLDS <- folds
  }
  if(CV_STRATIFY_FOLDS) {
    nclass <- table(dat[,1])
    
    for(i in 1:m) 
      dat$FOLDS[dat[,1] == as.numeric(names(nclass)[i])] <- sample(rep(1:CV_K, each=ceiling(nclass[i]/CV_K)))[1:nclass[i]]
    
  }
  
  
  # for(i in 1:3) print(table(dat[dat$FOLDS == i, 1])/nrow(dat[dat$FOLDS == i,]))
  
  cvout <- NA
  
  
  # Iterate over CV folds
  ########################
  model_list <- vector("list", CV_K)
  res_list <- vector("list", CV_K)
  for(k in 1:CV_K) {
    
    # Split into test and train based on fold 'k'
    i_f <- grep("FOLDS", names(dat))
    dat_train <- dat[dat$FOLDS != k, -i_f]
    dat_test <- dat[dat$FOLDS == k, -i_f]
    
    
    if(CV_TRAIN_RESAMPLE) {
      
      dat_train <- balance_data_with_SMOTE(dat_train)
      dat_train[,1] <- as.numeric(dat_train[,1])
      
      if(any(colnames(dat_train) == "BIO_SEX"))
        dat_train[, grep("BIO_SEX", colnames(dat_train))] <- round(dat_train[, grep("BIO_SEX", colnames(dat_train))], 0)
      
    }
    
    # Ensure randomised observations
    dat_train <- dat_train[sample(1:nrow(dat_train)),]
    dat_train <- apply(dat_train, 2, as.num)
    
    
    # Fit models
    ########################
    if((model_type == "GLMNET") && (m == 3)) {
      
      out <- glmnet(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), 
                    family="multinomial", type.multinomial="grouped", ...)
      
      
      cvout <- NA
      if(GLMNET_RUN_CV) {
        cvout <- cv.glmnet(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), 
                           family="multinomial", type.multinomial="grouped", ...)
        
        cvout <- c("lambda_1se"=cvout$lambda.1se, "index"=which(out$lambda == cvout$lambda.1se))
      }
      
      
      # Store selected variables output
      ##################################
      bnames <- lapply(out$beta, function(x) {
        xmat <- as.matrix(x)
        b <- apply(xmat, 2, function(y) y[y != 0])
        names(b) <- round(out$lambda, 4)
        return(b) })[[1]]
      
      
      # Update selected var counts
      for(i in 1:len(bnames)) {
        if(len(bnames[[i]]) > 0) {
          i_match <- match(names(bnames[[i]]), rownames(var_select_counts))
          var_select_counts[i_match, k] <- var_select_counts[i_match, k] + 1
          var_coeffs_counts[i_match, k] <- var_coeffs_counts[i_match, k] + bnames[[i]]
        }
      }
      
      
      # Predictions and confusion matrices
      #####################################
      out_pred_train <- predict(out, newx=as.matrix(dat_train[,-1]), type="class")
      out_pred_test <- predict(out, newx=as.matrix(dat_test[,-1]), type="class")
      out_confmat_train <- apply(out_pred_train, 2, function(x) list(table(factor(dat_train[,1], levels=0:2), factor(as.num(x), levels=0:2))))
      out_confmat_test <- apply(out_pred_test, 2, function(x) list(table(factor(dat_test[,1], levels=0:2), factor(as.num(x), levels=0:2))))
      
      out_prob_test <- predict(out, newx=as.matrix(dat_test[,-1]), type="response")
      out_prob_test <- apply(out_prob_test, 3, function(x) data.frame("TRUE"=as.num(as.char(dat_test[,1])), "PROB"=x))
      
    }
    
    if((model_type == "GLMNET") && (m == 2)) {
      
      out <- glmnet(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), 
                    family="binomial", ...)
      
      if(GLMNET_RUN_CV) {
        cvout <- cv.glmnet(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), 
                           family="binomial", ...)
        
        cvout <- c("lambda_1se"=cvout$lambda.1se, "index"=which(out$lambda == cvout$lambda.1se))
      }
      
      
      # Store selected variables output
      ##################################
      bnames <- apply(as.matrix(out$beta), 2, function(y) y[y != 0])
      names(bnames) <- round(out$lambda, 4)
        
    
      # Update selected var counts
      for(i in 1:len(bnames)) {
        if(len(bnames[[i]]) > 0) {
          i_match <- match(names(bnames[[i]]), rownames(var_select_counts))
          var_select_counts[i_match, k] <- var_select_counts[i_match, k] + 1
          var_coeffs_counts[i_match, k] <- var_coeffs_counts[i_match, k] + bnames[[i]]
        }
      }
      
      # Predictions and confusion matrices
      #####################################
      out_pred_train <- predict(out, newx=as.matrix(dat_train[,-1]), type="class")
      out_pred_test <- predict(out, newx=as.matrix(dat_test[,-1]), type="class")
      out_confmat_train <- apply(out_pred_train, 2, function(x) list(table(factor(dat_train[,1], levels=0:1), factor(as.num(x), levels=0:1))))
      out_confmat_test <- apply(out_pred_test, 2, function(x) list(table(factor(dat_test[,1], levels=0:1), factor(as.num(x), levels=0:1))))
      
      
      out_prob_test <- predict(out, newx=as.matrix(dat_test[,-1]), type="response")
      out_prob_test <- apply(out_prob_test, 2, function(x) data.frame("TRUE"=as.num(as.char(dat_test[,1])), "PROB"=x))
      
    }
    
    if((model_type == "GLMNET_ORDINAL") && (m == 3)) {
      
      out <- glmnet.cr(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), maxit=250, ...)
      
      if(GLMNET_RUN_CV)
        cvout <- select.glmnet.cr(out)
      
      
      # Store selected variables output
      ##################################
      bnames <- apply(as.matrix(out$beta), 2, function(y) y[y != 0])
      names(bnames) <- round(out$lambda, 4)
      
      
      # Update selected var counts
      for(i in 1:len(bnames)) {
        if(len(bnames[[i]]) > 0) {
          i_match <- na.omit(match(names(bnames[[i]]), rownames(var_select_counts)))
          
          if(len(i_match) > 0) {
            var_select_counts[i_match, k] <- var_select_counts[i_match, k] + 1
            var_coeffs_counts[i_match, k] <- var_coeffs_counts[i_match, k] + bnames[[i]][-attr(i_match, "na.action")]
          }
        }
      }
      
      
      # Predictions and confusion matrices
      #####################################
      out_pred_train <- predict(out, newx=as.matrix(dat_train[,-1]))[[3]]
      out_pred_test <- predict(out, newx=as.matrix(dat_test[,-1]))[[3]]
      out_confmat_train <- apply(out_pred_train, 2, function(x) list(table(factor(dat_train[,1], levels=0:2), factor(as.num(x), levels=0:2))))
      out_confmat_test <- apply(out_pred_test, 2, function(x) list(table(factor(dat_test[,1], levels=0:2), factor(as.num(x), levels=0:2))))
      
 
      out_prob_test <- predict(out, newx=as.matrix(dat_test[,-1]))[[4]]
      out_prob_test <- apply(out_prob_test, 3, function(x) data.frame("TRUE"=as.num(as.char(dat_test[,1])), "PROB"=x))
      
    }
    
    if((model_type == "RF") && (m == 3)) {
      
      out <- randomForest(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), importance=T, ...)
      
      
      cvout <- NULL
      
      # Update variable importance
      var_imp_list[[k]] <- list("RF_var_importance" = out$importance, "RF_var_importanceSD" = out$importanceSD)
      
      
      # Predictions and confusion matrices
      out_pred_train <- predict(out, as.matrix(dat_train[,-1]), type="class")
      out_pred_test <- predict(out, as.matrix(dat_test[,-1]), type="class")
      
      out_confmat_train <- table(factor(dat_train[,1], levels=0:2), factor(out_pred_train, levels=0:2))
      out_confmat_test <- table(factor(dat_test[,1], levels=0:2), factor(out_pred_test, levels=0:2))
      
      out_prob_test <- cb("TRUE"=as.num(as.char(dat_test[,1])), predict(out, as.matrix(dat_test[,-1]), type="prob"))
      
    }
    
    if((model_type == "RF") && (m == 2)) {
      
      out <- randomForest(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), importance=T, ...)
      
      
      cvout <- NULL
      
      # Update variable importance
      var_imp_list[[k]] <- list("RF_var_importance" = out$importance, "RF_var_importanceSD" = out$importanceSD)
      
      
      # Predictions and confusion matrices
      out_pred_train <- predict(out, as.matrix(dat_train[,-1]), type="class")
      out_pred_test <- predict(out, as.matrix(dat_test[,-1]), type="class")
      
      out_confmat_train <- table(factor(dat_train[,1], levels=0:1), factor(out_pred_train, levels=0:1))
      out_confmat_test <- table(factor(dat_test[,1], levels=0:1), factor(out_pred_test, levels=0:1))
      
      out_prob_test <- cb("TRUE"=as.num(as.char(dat_test[,1])), predict(out, as.matrix(dat_test[,-1]), type="prob"))
      
    }
    
    
    # Update output structures
    ###########################
    model_list[[k]] <- list("OUT" = out, "CV_OUT" = cvout)
    res_list[[k]] <- list("CONFMAT_TRAIN" = out_confmat_train, 
                          "CONFMAT_TEST" = out_confmat_test, 
                          "PRED_PROBS_TEST" = out_prob_test)


    if(PRINT_PROGRESS)
      catProgress(k, CV_K)
    
  }
  
  
  
  if(len(grep("GLM", model_type)) > 0)
    var_imp_list = list("VAR_COUNTS"=var_select_counts, "VAR_COEFF_COUNTS"=var_coeffs_counts)
  
  
  return(list(model_list, res_list, var_imp_list))
  
  
}






runSupervisedOnSplit <- function(dat_test, dat_train,
                                 model_type = c("GLMNET", "GLMNET_ORDINAL", "RF")[1],
                                 TRAIN_RESAMPLE = TRUE,
                                 GLMNET_OUT_DIR = "./cv_glmnet output/",
                                 GLMNET_RUN_CV = TRUE,
                                 ...)
{
  
  
  m <- len(unique(dat_train[,1]))
  
  
  # Remove rows with NA
  ######################
  which_rows_NA <- apply(dat_train, 1, function(x) any(is.na(x)))
  dat_train <- dat_train[!which_rows_NA, ]
  
  which_rows_NA <- apply(dat_test, 1, function(x) any(is.na(x)))
  dat_test <- dat_test[!which_rows_NA, ]
  
  
  # Form vector of variable counts
  ##################################
  var_select_counts <- num(ncol(dat_train) - 1)
  names(var_select_counts) <- names(dat_train)[-1]
  
  

  model_list <- vector("list", 1)
  res_list <- vector("list", 1)
  predprobs_list <- vector("list", 1)
  glmnet_cv_vars <- char(1)
  
  if(TRAIN_RESAMPLE) {
    
    dat_train <- balance_data_with_SMOTE(dat_train)
    dat_train[,1] <- as.numeric(dat_train[,1])
    
  }
  
  # Ensure randomised observations
  dat_train <- dat_train[sample(1:nrow(dat_train)),]
  dat_train <- apply(dat_train, 2, as.num)
  
  
  # Fit models
  ########################
  if((model_type == "GLMNET") && (m == 3)) {
    
    out <- glmnet(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), 
                  family="multinomial", type.multinomial="grouped", ...)
    
    
    cvout <- NA
    if(GLMNET_RUN_CV)
      cvout <- cv.glmnet(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), 
                         family="multinomial", type.multinomial="grouped", ...)
    
    
    # Store cv.glmnet output
    if(!is.null(GLMNET_OUT_DIR)) {
      fldnm <- ifelse(k < 10, paste0("fold0", k), paste0("fold", k))
      
      if(GLMNET_RUN_CV) {
        w() ; plot(cvout) ; abline(v=log(cvout$lambda.1se), lty=1, lwd=2, col="blue")
        legend("bottomleft", leg=paste("lambda_min_1se =", round(cvout$lambda.1se, 4)), cex=0.8)
        savePlot(paste0(GLMNET_OUT_DIR, "fig_", fldnm, "_cvglmnet.png"), "png")
        graphics.off()
      }
      
      
      bnames <- lapply(out$beta, function(x) {
        b <- apply(as.matrix(x), 2, function(y) names(y)[y != 0])
        names(b) <- round(out$lambda, 4)
        return(b) })[[1]]
      
      
      # Update selected var counts
      for(i in 1:len(bnames)) {
        if(len(bnames[[i]]) > 0) {
          i_match <- match(bnames[[i]], names(var_select_counts))
          var_select_counts[i_match] <- var_select_counts[i_match] + 1
        }
      }
      
      
      if(GLMNET_RUN_CV) {
        ilam <- which(cvout$glmnet.fit$lambda == cvout$lambda.1se)
        bnames_cv <- bnames[ilam]
      }
      
      
      bnames <- bnames[unl(lapply(bnames, len)) !=0]
      bnames <- bnames[1:which.max(unl(lapply(bnames, len)))]
      if(GLMNET_RUN_CV) bnames <- c(bnames_cv, bnames)
      bnames <- lapply(bnames, function(x) format(x, width=max(nchar(x))))
      bnames <- unl(lapply(bnames, paste, collapse="\t\t\t"))
      bnames <- paste(paste0("lambda=", names(bnames)), bnames, sep="\n")
      
      txtf <- file(paste0(GLMNET_OUT_DIR, "varnames_all_lambda_", fldnm, ".txt"), open="w")
      writeLines(bnames[-1], con=txtf, sep="\n\n")
      close(txtf)
      
      if(GLMNET_RUN_CV) glmnet_cv_vars[k] <- bnames[1]
    }
    
    # Predictions and confusion matrices
    out_pred_train <- predict(out, newx=as.matrix(dat_train[,-1]), type="class")
    out_pred_test <- predict(out, newx=as.matrix(dat_test[,-1]), type="class")
    out_confmat_train <- apply(out_pred_train, 2, function(x) list(table(factor(dat_train[,1], levels=0:2), factor(as.num(x), levels=0:2))))
    out_confmat_test <- apply(out_pred_test, 2, function(x) list(table(factor(dat_test[,1], levels=0:2), factor(as.num(x), levels=0:2))))
    
    cvout_confmat_train <- cvout_confmat_test <- out_predprobs_test <- NA
    if(GLMNET_RUN_CV) {
      cvout_pred_train <- predict(cvout, newx=as.matrix(dat_train[,-1]), type="class")
      cvout_pred_test <- predict(cvout, newx=as.matrix(dat_test[,-1]), type="class")
      cvout_confmat_train <- table(factor(dat_train[,1], levels=0:2), factor(as.num(cvout_pred_train[,1]), levels=0:2))
      cvout_confmat_test <- table(factor(dat_test[,1], levels=0:2), factor(as.num(cvout_pred_test[,1]), levels=0:2))
      
      out_predprobs_test <- predict(cvout, newx=as.matrix(dat_test[,-1]), type="response")
    }
    
    
    # ROC curve AUC analysis
    # out_predprob_train <- predict(out, newx=as.matrix(dat_train[,-1]), type="response")
    # aa <- apply(out_predprob_train, 3, function(x) multiclass.roc(as.factor(dat_train[,1]), x))
    # x <- out_predprob_train[,,2]
    # multiclass.roc(as.factor(dat_train[,1]), x)
    
  }
  
  if((model_type == "GLMNET") && (m == 2)) {
    
    out <- glmnet(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), 
                  family="binomial",  ...)
    
    cvout <- cv.glmnet(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), 
                       family="binomial",  ...)
    
    
    # dat_train <- dat_train_b[, 1:18]
    # glmnet(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), 
    #        family="multinomial", type.multinomial="grouped")
    
    
    
    # Store cv.glmnet output
    if(!is.null(GLMNET_OUT_DIR)) {
      fldnm <- ifelse(k < 10, paste0("fold0", k), paste0("fold", k))
      
      w() ; plot(cvout) ; abline(v=log(cvout$lambda.1se), lty=1, lwd=2, col="blue")
      legend("bottomleft", leg=paste("lambda_min_1se =", round(cvout$lambda.1se, 4)), cex=0.8)
      savePlot(paste0(GLMNET_OUT_DIR, "fig_", fldnm, "_cvglmnet.png"), "png")
      graphics.off()
      
      ilam <- which(cvout$glmnet.fit$lambda == cvout$lambda.1se)
      
      bnames <- apply(as.matrix(cvout$glmnet.fit$beta), 2, function(y) names(y)[y != 0])
      names(bnames) <- round(cvout$glmnet.fit$lambda, 4)
      
      bnames_cv <- bnames[ilam]
      
      bnames <- bnames[unl(lapply(bnames, len)) !=0]
      bnames <- bnames[1:which.max(unl(lapply(bnames, len)))]
      bnames <- c(bnames_cv, bnames)
      bnames <- lapply(bnames, function(x) format(x, width=max(nchar(x))))
      bnames <- unl(lapply(bnames, paste, collapse="\t\t\t"))
      bnames <- paste(paste0("lambda=", names(bnames)), bnames, sep="\n")
      
      txtf <- file(paste0(GLMNET_OUT_DIR, "varnames_all_lambda_", fldnm, ".txt"), open="w")
      writeLines(bnames[-1], con=txtf, sep="\n\n")
      close(txtf)
      
      glmnet_cv_vars[k] <- bnames[1]
    }
    
    # Predictions and confusion matrices
    out_pred_train <- predict(out, newx=as.matrix(dat_train[,-1]), type="class")
    out_pred_test <- predict(out, newx=as.matrix(dat_test[,-1]), type="class")
    out_confmat_train <- apply(out_pred_train, 2, function(x) list(table(factor(dat_train[,1], levels=0:2), factor(as.num(x), levels=0:2))))
    out_confmat_test <- apply(out_pred_test, 2, function(x) list(table(factor(dat_test[,1], levels=0:2), factor(as.num(x), levels=0:2))))
    
    cvout_pred_train <- predict(cvout, newx=as.matrix(dat_train[,-1]), type="class")
    cvout_pred_test <- predict(cvout, newx=as.matrix(dat_test[,-1]), type="class")
    cvout_confmat_train <- table(factor(dat_train[,1], levels=0:2), factor(as.num(cvout_pred_train[,1]), levels=0:2))
    cvout_confmat_test <- table(factor(dat_test[,1], levels=0:2), factor(as.num(cvout_pred_test[,1]), levels=0:2))
    
    out_predprobs_test <- predict(out, newx=as.matrix(dat_test[,-1]), type="response")
    
  }
  
  if((model_type == "GLMNET_ORDINAL") && (m == 3)) {
    
    out <- glmnet.cr(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), ...)
    
    # select.glmnet.cr(out)
    
    
    
    # Predictions and confusion matrices
    out_pred_train <- predict(out, newx=as.matrix(dat_train[,-1]))[[3]]
    out_pred_test <- predict(out, newx=as.matrix(dat_test[,-1]))[[3]]
    out_confmat_train <- apply(out_pred_train, 2, function(x) list(table(factor(dat_train[,1], levels=0:2), factor(as.num(x), levels=0:2))))
    out_confmat_test <- apply(out_pred_test, 2, function(x) list(table(factor(dat_test[,1], levels=0:2), factor(as.num(x), levels=0:2))))
    
    
    out_predprobs_test <- predict(out, newx=as.matrix(dat_test[,-1]))[[4]]
  }
  
  if((model_type == "RF") && (m == 3)) {
    
    out <- randomForest(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), ...)
    
    
    cvout <- NULL
    
    
    # Predictions and confusion matrices
    out_pred_train <- predict(out, as.matrix(dat_train[,-1]), type="class")
    out_pred_test <- predict(out, as.matrix(dat_test[,-1]), type="class")
    
    out_confmat_train <- table(factor(dat_train[,1], levels=0:2), factor(out_pred_train, levels=0:2))
    out_confmat_test <- table(factor(dat_test[,1], levels=0:2), factor(out_pred_test, levels=0:2))
    
    out_predprobs_test <- predict(out, as.matrix(dat_test[,-1]), type="prob")
    
  }
  
  if((model_type == "RF") && (m == 2)) {
    
    out <- randomForest(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), ...)
    cvout <- NULL
    
    
    # Predictions and confusion matrices
    out_pred_train <- predict(out, as.matrix(dat_train[,-1]), type="class")
    out_pred_test <- predict(out, as.matrix(dat_test[,-1]), type="class")
    
    out_confmat_train <- table(factor(dat_train[,1], levels=0:1), factor(out_pred_train, levels=0:1))
    out_confmat_test <- table(factor(dat_test[,1], levels=0:1), factor(out_pred_test, levels=0:1))
    
    out_predprobs_test <- cb("TRUE"=dat_test[,1], "PROBS"=predict(out, as.matrix(dat_test[,-1]), type="prob"))
     
  }
  
  
  
  
  if(model_type == "GLMNET") {
    model_list[[1]] <- list("OUT"=out, "CV_OUT"=cvout)
    res_list[[1]] <- list("OUT_CONFMAT_TRAIN"=out_confmat_train, "OUT_CONFMAT_TEST"=out_confmat_test,
                          "CV_OUT_CONFMAT_TRAIN"=cvout_confmat_train, "CV_OUT_CONFMAT_TEST"=cvout_confmat_test)
  }
  else if((model_type == "GLMNET_ORDINAL") | (model_type == "RF")) {
    model_list[[1]] <- out
    res_list[[1]] <- list("OUT_CONFMAT_TRAIN"=out_confmat_train, "OUT_CONFMAT_TEST"=out_confmat_test)
  }
  predprobs_list[[1]] <- out_predprobs_test
  
  
  
  # Write all selected variables in cv.glmnet fit for each fold to file
  if(!is.null(GLMNET_OUT_DIR)) {
    txtf <- file(paste0(GLMNET_OUT_DIR, "varnames_cv_lambda.txt"), open="w")
    writeLines(glmnet_cv_vars, con=txtf, sep="\n\n")
    close(txtf)
  }
  
  
  return(list(model_list, res_list, predprobs_list, var_select_counts))
  
  
}




runSupervisedCV_repeats <- function(n = 10, dat,
                                    model_type = c("GLMNET")[1],
                                    CV_K = 5,
                                    CV_SEEDS = floor(runif(n, 0, 1e6)),
                                    CV_STRATIFY_FOLDS = TRUE,
                                    CV_TRAIN_RESAMPLE = TRUE,
                                    PRINT_PROGRESS = TRUE,
                                    GLMNET_OUT_DIRS = NULL,
                                    ...)
{
  out_list_models <- NULL
  out_list_results <- NULL
  out_list_varsel <- NULL
  for(i in 1:n) {
    out <- runSupervisedCV(dat=dat, model_type=model_type, CV_K=CV_K,
                           CV_SEED=CV_SEEDS[i], CV_STRATIFY_FOLDS = TRUE, CV_TRAIN_RESAMPLE = TRUE,
                           GLMNET_OUT_DIR=GLMNET_OUT_DIRS[i], PRINT_PROGRESS=PRINT_PROGRESS, ...)
    
    out_list_models <- c(out_list_models, out[[1]])
    out_list_results <- c(out_list_results, out[[2]])
    out_list_varsel <- c(out_list_varsel, out[[3]])
    
    if(model_type %in% c("GLMNET", "GLMNET_ORDINAL"))
      names(out_list_varsel)[len(out_list_varsel) + c(-1, 0)] <- paste(paste0("REP", i), names(out_list_varsel)[len(out_list_varsel) + c(-1, 0)], sep=".")
    
    if(PRINT_PROGRESS)
      cat("OVERALL PROGRESS: ")
    catProgress(i, n)
  }
  
  return(list(out_list_models, out_list_results, out_list_varsel))
}






resultAggregator <- function(res_list, model_type=c("GLMNET", "GLMNET_ORD", "RF")[1])
{
  if(model_type == "GLMNET")
    return( lapply(res_list, function(x) {
      
      kappa_out_tr <- unl(lapply(x[[1]], function(y) calc_cohen_kappa(y[[1]])))
      kappa_out_tst <- unl(lapply(x[[2]], function(y) calc_cohen_kappa(y[[1]])))
      kappa_cvout_tr <- calc_cohen_kappa(x[[3]])
      kappa_cvout_tst <- calc_cohen_kappa(x[[4]])
      
      acc_out_tr <- unl(lapply(x[[1]], function(y) sum(diag(y[[1]]))/sum(y[[1]]) ))
      acc_out_tst <- unl(lapply(x[[2]], function(y) sum(diag(y[[1]]))/sum(y[[1]]) ))
      acc_cvout_tr <- sum(diag(x[[3]]))/sum(x[[3]])
      acc_cvout_tst <- sum(diag(x[[4]]))/sum(x[[4]])
      
      recall_out_tr <- matrix(unl(lapply(x[[1]], function(y) diag(y[[1]])/rowSums(y[[1]]) )), ncol=3, byrow=T)
      recall_out_tst <- matrix(unl(lapply(x[[2]], function(y) diag(y[[1]])/rowSums(y[[1]]) )), ncol=3, byrow=T)
      
      recall_cvout_tr <- diag(x[[3]])/rowSums(x[[3]])
      recall_cvout_tst <- diag(x[[4]])/rowSums(x[[4]])
      
      return(list("OUT"=cb("OUT_TRAIN_KAPPA"=kappa_out_tr, "OUT_TRAIN_ACC"=acc_out_tr,
                           "OUT_TEST_KAPPA"=kappa_out_tst, "OUT_TEST_ACC"=acc_out_tst,
                           "OUT_TRAIN_RECALL"=recall_out_tr, "OUT_TEST_RECALL"=recall_out_tst),
                  "CV_OUT"=c("CVOUT_TRAIN_KAPPA"=kappa_cvout_tr, "CVOUT_TRAIN_ACC"=acc_cvout_tr,
                             "CVOUT_TEST_KAPPA"=kappa_cvout_tst, "CVOUT_TEST_ACC"=acc_cvout_tst,
                             "CVOUT_TRAIN_RECALL"=recall_cvout_tr, "CVOUT_TEST_RECALL"=recall_cvout_tst)))
      
    }) )
  
  if(model_type == "GLMNET_ORD")
    return( lapply(res_list, function(x) {
      
      kappa_out_tr <- unl(lapply(x[[1]], function(y) calc_cohen_kappa(y[[1]])))
      kappa_out_tst <- unl(lapply(x[[2]], function(y) calc_cohen_kappa(y[[1]])))
  
      acc_out_tr <- unl(lapply(x[[1]], function(y) sum(diag(y[[1]]))/sum(y[[1]]) ))
      acc_out_tst <- unl(lapply(x[[2]], function(y) sum(diag(y[[1]]))/sum(y[[1]]) ))
    
      recall_out_tr <- matrix(unl(lapply(x[[1]], function(y) diag(y[[1]])/rowSums(y[[1]]) )), ncol=3, byrow=T)
      recall_out_tst <- matrix(unl(lapply(x[[2]], function(y) diag(y[[1]])/rowSums(y[[1]]) )), ncol=3, byrow=T)
      
      return(cb("OUT_TRAIN_KAPPA"=kappa_out_tr, "OUT_TRAIN_ACC"=acc_out_tr,
                "OUT_TEST_KAPPA"=kappa_out_tst, "OUT_TEST_ACC"=acc_out_tst,
                "OUT_TRAIN_RECALL"=recall_out_tr, "OUT_TEST_RECALL"=recall_out_tst))
      
    }) )
  
  if(model_type == "RF")
    return( lapply(res_list, function(x) {

      kappa_out_tr <- calc_cohen_kappa(x[[1]])
      kappa_out_tst <- calc_cohen_kappa(x[[2]])
    
      acc_out_tr <- sum(diag(x[[1]]))/sum(x[[1]])
      acc_out_tst <- sum(diag(x[[2]]))/sum(x[[2]])
      
      recall_out_tr <- diag(x[[1]])/rowSums(x[[1]])
      recall_out_tst <- diag(x[[2]])/rowSums(x[[2]])
      
      return(c("OUT_TRAIN_KAPPA"=kappa_out_tr, "OUT_TRAIN_ACC"=acc_out_tr,
                "OUT_TEST_KAPPA"=kappa_out_tst, "OUT_TEST_ACC"=acc_out_tst,
                "OUT_TRAIN_RECALL"=recall_out_tr, "OUT_TEST_RECALL"=recall_out_tst))
      
    }) )
  
}



plotAggResultStat <- function(res_agg, model_type = c("GLMNET", "RF")[1], class_names = c("super-pref", "pref", "non-pref"))
{

  if(model_type == "GLMNET") {  
    res_cvout <- matrix(unl(lapply(res_agg, function(x) x$CV_OUT[c(1:4, 8:10)])), ncol=7, byrow=T,
                        dimnames=list(NULL, c("train_Kappa", "train_accuracy", "test_Kappa", "test_accuracy",
                                              "test_recall_0", "test_recall_1", "test_recall_2")))
  }
  
  if(model_type == "RF") {  
    res_cvout <- matrix(unl(lapply(res_agg, function(x) x[c(1:4, 8:10)])), ncol=7, byrow=T,
                        dimnames=list(NULL, c("train_Kappa", "train_accuracy", "test_Kappa", "test_accuracy",
                                              "test_recall_0", "test_recall_1", "test_recall_2")))
  }
    

  m <- len(class_names)
  if(m == 3) i_recall <- 5:7
  if(m == 2) i_recall <- 5:6
  
  
  res_cvout <- res_cvout[, c(1, 3, 2, 4, i_recall)]
  res_stats <- round(apply(res_cvout, 2, function(x) c("mean"=mean(x), "sd"=sd(x))), 3)
  
  
  # Determine if only 2 classes
    
    
  ## ggplot2   
  g_refine <- function(g){ return(g + geom_density(alpha=0.4) + theme(legend.title = element_blank(), legend.justification = c(1, 1), legend.position = c(1, 1))) }
  
  gdat1 <- data.frame("Kappa"=c(res_cvout[,1:2]), "accuracy"=c(res_cvout[,3:4]), 
                      "LABEL"=rep(c("train", "test"), each=nrow(res_cvout)))
  gdat2 <- data.frame("recall"=c(res_cvout[,i_recall]), "LABEL"=rep(class_names, each=nrow(res_cvout)))
  
  g1a <- g_refine(ggplot(gdat1, aes(x=Kappa, fill=LABEL))) + coord_cartesian(xlim = c(-0.2, 1)) + geom_vline(xintercept=res_stats[1, 1:2], color=rev(gg_color_hue(2)))
  g1b <- g_refine(ggplot(gdat1, aes(x=accuracy, fill=LABEL))) + coord_cartesian(xlim = c(0, 1)) + geom_vline(xintercept=res_stats[1, 3:4], color=rev(gg_color_hue(2)))
  g2 <- g_refine(ggplot(gdat2, aes(x=recall, fill=LABEL))) + coord_cartesian(xlim = c(0, 1))
  g4 <- tableGrob(format(t(res_stats), width=4))
  
  # jpeg("./cv_glmnet_alpha/test2.jpg") 
  w(14,10)
  grid.arrange(g4, g1a, g1b, g2, ncol=2)
  # dev.off()
}



evaluateLambdaResults <- function(cv_list, model_type = c("GLMNET", "GLMNET_ORD")[1],
                                  ylim_kappa = c(-0.1, 0.65), 
                                  ylim_acc = c(0, 1),
                                  class_names = c("s-pref", "pref", "n-pref"),
                                  output_dir = "./cv_glmnet_lambda_vs_stats/",
                                  output_path_delim = "",
                                  plots_close = FALSE)
{
  
  m <- len(class_names)
  
  if(model_type == "GLMNET") {
    lam_vals <- lapply(cv_list[[1]], function(x) log(x$OUT$lambda))
    lam_vals_autoselect <- unl(lapply(cv_list[[1]], function(x) log(x$CV_OUT$lambda.1se)))
    dfs <- lapply(cv_list[[1]], function(x) x$OUT$df)
    
    out_res <- lapply(resultAggregator(cv_list[[2]]), function(x) x$OUT)
    
    lam_breaks <- seq(1.005*min(unl(lam_vals)), 1.005*max(unl(lam_vals)), len=76)
    lam_cat <- lapply(lam_vals, cut, lam_breaks)
    lam_lvls <- levels(lam_cat[[1]])
    
    # for(i in 1:len(out_res)) rownames(out_res[[i]]) <- lam_cat[[i]]
    
    res_list <- vector("list", len(lam_lvls))
    names_res_list  <- lam_lvls
    df_list <- vector("list", len(lam_lvls))
    for(i in 1:len(lam_lvls)) {
      
      ccmat <- NULL
      which_lvl <- lapply(lam_cat, function(x) which(x == lam_lvls[i]))
      for(j in 1:len(out_res))
        ccmat <- rbind(ccmat, out_res[[j]][which_lvl[[j]], , drop=F])
      
      
      dfvec <- NULL
      for(j in 1:len(dfs))
        dfvec <- c(dfvec, dfs[[j]][which_lvl[[j]]])
      df_list[[i]] <- round(median(dfvec), 0)
      
      colnames(ccmat) <- c("OUT_TRAIN_KAPPA", "OUT_TRAIN_ACC",   "OUT_TEST_KAPPA", "OUT_TEST_ACC",
                           paste0("OUT_TRAIN_RECALL.", 0:2), paste0("OUT_TEST_RECALL.", 0:2))
      
      
      if(m == 2) ccmat <- ccmat[, -c(7, 10), drop=F]
      res_list[[i]] <- ccmat
      
    }
  }
  
  if(model_type == "GLMNET_ORD") {
    lam_vals <- lapply(cv_list[[1]], function(x) log(x$lambda))
    dfs <- lapply(cv_list[[1]], function(x) x$df)
    
    lam_vals_autoselect <- unl(lapply(cv_list[[1]], function(x) log(x$lambda)[select.glmnet.cr(x)]))
    
    out_res <- resultAggregator(cv_list[[2]], "GLMNET_ORD")
    
    lam_breaks <- seq(1.005*min(unl(lam_vals)), 1.005*max(unl(lam_vals)), len=76)
    lam_cat <- lapply(lam_vals, cut, lam_breaks)
    lam_lvls <- levels(lam_cat[[1]])
    

    res_list <- vector("list", len(lam_lvls))
    names_res_list  <- lam_lvls
    df_list <- vector("list", len(lam_lvls))
    for(i in 1:len(lam_lvls)) {
      
      ccmat <- NULL
      which_lvl <- lapply(lam_cat, function(x) which(x == lam_lvls[i]))
      for(j in 1:len(out_res))
        ccmat <- rbind(ccmat, out_res[[j]][which_lvl[[j]], , drop=F])
      
      
      dfvec <- NULL
      for(j in 1:len(dfs))
        dfvec <- c(dfvec, dfs[[j]][which_lvl[[j]]])
      df_list[[i]] <- round(median(dfvec), 0)
      
      colnames(ccmat) <- c("OUT_TRAIN_KAPPA", "OUT_TRAIN_ACC",   "OUT_TEST_KAPPA", "OUT_TEST_ACC",
                           paste0("OUT_TRAIN_RECALL.", 0:2), paste0("OUT_TEST_RECALL.", 0:2))
      
      res_list[[i]] <- ccmat
      
    }
  }
  

  
  makePlot <- function(res, plot_title="Result", ylim=c(0, 1)) {
    lam_midpoints <- lam_breaks[-len(lam_breaks)] + diff(lam_breaks)/2
    
    plot_g(lam_midpoints, res[,1], type="n", ylim=ylim,
           ylab=plot_title, xlab="log lambda")
    
    
    ups_seq <- seq(lam_midpoints[1], tail(lam_midpoints, 1), len=5*len(lam_midpoints))
    # ups_seq <- ups_seq + diff(ups_seq[c(1, 5)])
    res_ups <- cb(predict(loess(res[,2] ~ lam_midpoints, span=0.1), ups_seq),
                  predict(loess(res[,4] ~ lam_midpoints, span=0.1), ups_seq))
    
    for(i in 1:len(ups_seq))
      lines(cb(ups_seq[i], res_ups[i,]), col="pink", lwd=1)
    
    lines(ups_seq, res_ups[,1], col="deeppink")
    lines(ups_seq, res_ups[,2], col="deeppink")
    
    lines(lam_midpoints, res[,1], lwd=3, col="blue")
    abline(v=c(range(lam_vals_autoselect), mean(lam_vals_autoselect)), col="seagreen", lwd=c(1,1,2))
    title(main = paste0(plot_title, " against shrinkage parameter (averaged over ", len(out_res), "-fold CV)"))
    
    # axis(1, at=lam_midpoints[seq(1, len(lam_midpoints), 2)],
         # labels=unl(df_list)[seq(1, len(df_list), 2)], outer=T)
    
    mtext(side=1, text=unl(df_list)[seq(1, len(df_list), 2)], 
          at=lam_midpoints[seq(1, len(lam_midpoints), 2)], cex=0.75)
    
  }
  
  
  # res_kappa_test <- matrix(unl(lapply(res_list, function(x) {
  #   y <- x[,grep("OUT_TEST_KAPPA", colnames(x))]
  #   yq <- quantile(y, probs=c(0.16, 0.50, 0.84))
  #   return(c(mean(y), yq))
  # })), ncol=4, byrow=T)
  # makePlot(res_kappa_test, "Test Kappa")
  
  res_for_plot <- lapply(as.list(colnames(res_list[[2]])), function(y) matrix(unl(lapply(res_list, function(x) {
    y <- x[,grep(y, colnames(x))]
    yq <- quantile(y, probs=c(0.16, 0.50, 0.84), na.rm=T)
    return(c(mean(y), yq))
  })), ncol=4, byrow=T))
  
  
  if(!is.null(output_dir)) {
    png(paste0(output_dir, "fig_lam_vs_kappa_acc", output_path_delim, ".png"), width=1600, height=900)
    par(mfrow=c(2,2), mar=c(4,4,1.5,1))
    makePlot(res_for_plot[[1]], "Train Kappa", ylim=ylim_kappa)
    makePlot(res_for_plot[[3]], "Test Kappa", ylim=ylim_kappa)
    makePlot(res_for_plot[[2]], "Train accuracy", ylim=ylim_acc)
    makePlot(res_for_plot[[4]], "Test accuracy", ylim=ylim_acc)
    dev.off()
    
    png(paste0(output_dir, "fig_lam_vs_recall", output_path_delim, ".png"), width=1600, height=900)
    par(mfrow=c(m,2), mar=c(4,4,1.5,1))
    if(m == 3) {
      makePlot(res_for_plot[[5]], "Train recall (s-pref)")
      makePlot(res_for_plot[[8]], "Test recall (s-pref)")
      makePlot(res_for_plot[[6]], "Train recall (pref)")
      makePlot(res_for_plot[[9]], "Test recall (pref)")
      makePlot(res_for_plot[[7]], "Train recall (n-pref)")
      makePlot(res_for_plot[[10]], "Test recall (n-pref)")
    }
    if(m == 2){
      makePlot(res_for_plot[[5]], paste0("Train recall (", class_names[1], ")"))
      makePlot(res_for_plot[[7]], paste0("Test recall (", class_names[1], ")"))
      makePlot(res_for_plot[[6]], paste0("Train recall (", class_names[2], ")"))
      makePlot(res_for_plot[[8]], paste0("Test recall (", class_names[2], ")"))
    }
    dev.off()
  }
  
  if(!plots_close) {
    w() ; par(mfrow=c(2,2), mar=c(4,4,1.5,1))
    makePlot(res_for_plot[[1]], "Train Kappa", ylim=ylim_kappa)
    makePlot(res_for_plot[[3]], "Test Kappa", ylim=ylim_kappa)
    makePlot(res_for_plot[[2]], "Train accuracy", ylim=ylim_acc)
    makePlot(res_for_plot[[4]], "Test accuracy", ylim=ylim_acc)
    
    w() ; par(mfrow=c(m,2), mar=c(4,4,1.5,1))
    if(m == 3) {
      makePlot(res_for_plot[[5]], "Train recall (s-pref)")
      makePlot(res_for_plot[[8]], "Test recall (s-pref)")
      makePlot(res_for_plot[[6]], "Train recall (pref)")
      makePlot(res_for_plot[[9]], "Test recall (pref)")
      makePlot(res_for_plot[[7]], "Train recall (n-pref)")
      makePlot(res_for_plot[[10]], "Test recall (n-pref)")
    }
    if(m == 2){
      makePlot(res_for_plot[[5]], paste0("Train recall (", class_names[1], ")"))
      makePlot(res_for_plot[[7]], paste0("Test recall (", class_names[1], ")"))
      makePlot(res_for_plot[[6]], paste0("Train recall (", class_names[2], ")"))
      makePlot(res_for_plot[[8]], paste0("Test recall (", class_names[2], ")"))
    }
  }

  
}



calcAUCfromGLMNET <- function(cv_list)
{
  
  mod <- cv_list[[1]][[1]]$OUT
  
  
}



































# if(!is.null(GLMNET_OUT_DIR)) {
#   fldnm <- ifelse(k < 10, paste0("fold0", k), paste0("fold", k))
#   
#   if(GLMNET_RUN_CV) {
#     w() ; plot(cvout) ; abline(v=log(cvout$lambda.1se), lty=1, lwd=2, col="blue")
#     legend("bottomleft", leg=paste("lambda_min_1se =", round(cvout$lambda.1se, 4)), cex=0.8)
#     savePlot(paste0(GLMNET_OUT_DIR, "fig_", fldnm, "_cvglmnet.png"), "png")
#     graphics.off()
#   }
#   
#   
#   bnames <- lapply(out$beta, function(x) {
#     b <- apply(as.matrix(x), 2, function(y) names(y)[y != 0])
#     names(b) <- round(out$lambda, 4)
#     return(b) })[[1]]
#   
#   
#   # Update selected var counts
#   for(i in 1:len(bnames)) {
#     if(len(bnames[[i]]) > 0) {
#       i_match <- match(bnames[[i]], names(var_select_counts))
#       var_select_counts[i_match] <- var_select_counts[i_match] + 1
#     }
#   }
#   
#   
#   if(GLMNET_RUN_CV) {
#     ilam <- which(cvout$glmnet.fit$lambda == cvout$lambda.1se)
#     bnames_cv <- bnames[ilam]
#   }
#   
#   
#   bnames <- bnames[unl(lapply(bnames, len)) !=0]
#   bnames <- bnames[1:which.max(unl(lapply(bnames, len)))]
#   if(GLMNET_RUN_CV) bnames <- c(bnames_cv, bnames)
#   bnames <- lapply(bnames, function(x) format(x, width=max(nchar(x))))
#   bnames <- unl(lapply(bnames, paste, collapse="\t\t\t"))
#   bnames <- paste(paste0("lambda=", names(bnames)), bnames, sep="\n")
#   
#   txtf <- file(paste0(GLMNET_OUT_DIR, "varnames_all_lambda_", fldnm, ".txt"), open="w")
#   writeLines(bnames[-1], con=txtf, sep="\n\n")
#   close(txtf)
#   
#   if(GLMNET_RUN_CV) glmnet_cv_vars[k] <- bnames[1]
# }