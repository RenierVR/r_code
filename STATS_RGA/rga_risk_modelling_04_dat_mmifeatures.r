source("rga_risk_modelling_01_dat.r")

library(glmnet)
library(glmnetcr)
library(randomForest)
library(ggplot2)
library(gridExtra)
library(ROCR)


#################################################
####  Formulate some model subsets  #############
#################################################

nms_all <- names(dat_all)
nms_allnp <- names(dat_all_noprof)
nms_rga <- names(dat_rga)
nms_rganp <- names(dat_rga_noprof)


dat_mmifeat_ss <- dat_all[, c(grep("UW_SS", nms_all), unl(sapply(c("REM_latency_std",
                                                                   "wake_no_motion_hr_skewness_mean",
                                                                   "deep_sleep_hrv_sdnn_median_mean",
                                                                   "BMI",
                                                                   "REM_latency_mean",
                                                                   "sleep_no_motion_hr_amplitude_std",
                                                                   "wake_no_motion_hr_median_std",
                                                                   "hr_clf_unhealthy",
                                                                   "sleep_no_motion_hr_median_std",
                                                                   "moderate_active_hr_mean",
                                                                   "moderate_active_hr_std",
                                                                   "no_motion_hr_amplitude_std",
                                                                   "apnea",
                                                                   "ae_hrv_4",
                                                                   "ae_act_1",
                                                                   "ae_pw_5"), grep, nms_all)))]








#################################################
####  Random Forest standard/substandard  #######
#################################################


out <- runSupervisedOnSplit(dat_train = dat_mmifeat_ss[dat_all$COHORT == "MMI",],
                            dat_test = dat_mmifeat_ss[dat_all$COHORT == "RGA",],
                            model_type = "RF", ntree=5000)


par_ntree <- c(50, 1000, 5000)
out_confmat_list <- vector("list", len(par_ntree))
for(i in 1:len(par_ntree)) {
  
  out_confmat <- vector("list", 50)
  for(j in 1:len(out_confmat)) {
    out <- runSupervisedOnSplit(dat_train = dat_mmifeat_ss[dat_all$COHORT == "MMI",],
                                dat_test = dat_mmifeat_ss[dat_all$COHORT == "RGA",],
                                model_type = "RF", ntree=par_ntree[i])
    
    out_confmat[[j]] <- list("CONF_MAT" = out[[2]][[1]]$OUT_CONFMAT_TEST,
                             "KAPPA" = calc_cohen_kappa(out[[2]][[1]]$OUT_CONFMAT_TEST),
                             "AUC" = calc_auc_ROCR(out[[3]][[1]][,3], out[[3]][[1]][,1]))
    
    catProgress(j, len(out_confmat))
  }
  
  out_confmat_list[[i]] <- out_confmat
  
  
  catProgress(i, len(par_ntree))
}


lapply(out_confmat_list, function(x) {
  xkap <- unl(lapply(x, function(y) y[[2]]))
  return(c(mean(xkap), sd(xkap)))
})













