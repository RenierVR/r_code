source("rga_risk_modelling_01_dat.r")

library(glmnet)
library(glmnetcr)
library(randomForest)
library(ggplot2)
library(gridExtra)


#################################################
####  Formulate some model subsets  #############
#################################################

nms_all <- names(dat_all)
nms_allnp <- names(dat_all_noprof)
nms_rga <- names(dat_rga)
nms_rganp <- names(dat_rga_noprof)



# dat_mmi <- dat_use[, c(grep("UW_K3", nms_use), unl(sapply(c("REM_latency_std",
#                                                             "wake_no_motion_hr_skewness_mean",
#                                                             "deep_sleep_hrv_sdnn_median_mean",
#                                                             "BMI",
#                                                             "REM_latency_mean",
#                                                             "sleep_no_motion_hr_amplitude_std",
#                                                             "wake_no_motion_hr_median_std",
#                                                             "hr_clf_unhealthy",
#                                                             "sleep_no_motion_hr_median_std",
#                                                             "moderate_active_hr_mean",
#                                                             "moderate_active_hr_std",
#                                                             "no_motion_hr_amplitude_std",
#                                                             "apnea",
#                                                             "ae_hrv_4",
#                                                             "ae_act_1",
#                                                             "ae_pw_5"), grep, nms_use)))]
# dat_mmi <- dat_mmi[!apply(dat_mmi, 1, function(x) any(is.na(x))), ]
# 



## DATA SUBSET: ALL COHORTS, PROFILE-DEPENDENT
###############################################


dat_all_use <- dat_all[, c(match(c("UW_K3", "BIO_SEX", "BIO_AGE", "BIO_BMI", "BIO_DEFAULT_VO2MAX"), nms_all), 15:ncol(dat_all))] 
dat_all_use <- dat_all_use[, which(count_NA(dat_all_use) <= 50)]
dat_all_use <- dat_all_use[!apply(dat_all_use, 1, function(x) any(is.na(x))), ]
# sum(apply(dat_all_use, 1, function(x) any(is.na(x))))



## DATA SUBSET: ALL COHORTS, NO PROFILE
########################################

dat_np_use <- dat_all_noprof[, c(match(c("UW_K3", "BIO_SEX", "BIO_AGE"), nms_allnp), 15:ncol(dat_all_noprof))] 
dat_np_use <- dat_np_use[, which(count_NA(dat_np_use) <= 50)]
dat_np_use <- dat_np_use[!apply(dat_np_use, 1, function(x) any(is.na(x))), ]
# sum(apply(dat_np_use, 1, function(x) any(is.na(x))))

dat_np_use2 <- dat_all_noprof[, c(match(c("UW_K2_sp", "BIO_SEX", "BIO_AGE"), nms_allnp), 15:ncol(dat_all_noprof))] 
dat_np_use2 <- dat_np_use2[, which(count_NA(dat_np_use2) <= 50)]
dat_np_use2 <- dat_np_use2[!apply(dat_np_use2, 1, function(x) any(is.na(x))), ]

dat_np_use3 <- dat_all_noprof[, c(match(c("UW_K2_p", "BIO_SEX", "BIO_AGE"), nms_allnp), 15:ncol(dat_all_noprof))] 
dat_np_use3 <- dat_np_use3[, which(count_NA(dat_np_use3) <= 50)]
dat_np_use3 <- dat_np_use3[!apply(dat_np_use3, 1, function(x) any(is.na(x))), ]


i_hrv <- grep("HRV", names(dat_np_use2))
i_hrv <- i_hrv[1:which(diff(i_hrv) > 1)[1]]


dat_np_use2_ae_act <- dat_np_use2[, -i_hrv]
dat_np_use2_ae_act <- dat_np_use2_ae_act[, c(match(c("UW_K2_sp", "BIO_SEX", "BIO_AGE"), names(dat_np_use2_ae_act)),
                                     grep("AEVALS", names(dat_np_use2_ae_act)),
                                     grep("ACTIVITY", names(dat_np_use2_ae_act)))]



## DATA SUBSET: ALL COHORTS, AUTO ENCODERS, PROFILE-DEPENDENT
###############################################################

dat_ae <- dat_all[, c(match(c("UW_K3", "BIO_SEX", "BIO_AGE", "BIO_BMI", "BIO_DEFAULT_VO2MAX"), nms_all), grep("AEVALS", nms_all))]

## DATA SUBSET: ALL COHORTS, AUTO ENCODERS, NO PROFILE
#######################################################

dat_np_ae <- dat_all_noprof[, c(match(c("UW_K3", "BIO_SEX", "BIO_AGE"), nms_allnp), grep("AEVALS", nms_allnp))]
dat_np_ae <- dat_np_ae[!apply(dat_np_ae, 1, function(x) any(is.na(x))), ]




###################################################################
####  CROSS VALIDATION TEST/TRAIN SPLIT AND MODEL APPLICATION  ####
###################################################################



source("./rga_risk_modelling_02_model_funs.r")




#################################################
####  GLMNET models fitting with repeats  #######
#################################################


# ## All (MMI + RGA) data
# allout_all <- runSupervisedCV(dat=dat_np_use, model="GLMNET", 
#                               CV_K=5, CV_SEED=floor(runif(1, 1, 1e6)), CV_TRAIN_RESAMPLE=T,
#                               alpha=0)
# res_agg_all <- resultAggregator(allout_all[[2]])
# plotAggResultStat(res_agg_all, F)
# evaluateLambdaResults(allout_all)





## MMI features
outlist_mmi <- runSupervisedCV_repeats(n=2, dat=dat_mmi, model_type="GLMNET", CV_K=10, alpha=1)
res_agg_mmi <- lapply(outlist_mmi, function(x) resultAggregator(x[[2]]))
res_agg_mmi <- resultAggregator(outlist_mmi[[2]])
plotAggResultStat(res_agg_mmi[1:10])
plotAggResultStat(res_agg_mmi[11:20])
plotAggResultStat(res_agg_mmi)


alpha_vec <- seq(0, 1, by=0.1)
for(i in 1:len(alpha_vec)) {
  outlist_mmi <- runSupervisedCV_repeats(n=5, dat=dat_mmi, model_type="GLMNET", CV_K=10, alpha=1)
  res_agg_mmi <- lapply(outlist_mmi, function(x) resultAggregator(x[[2]]))
  plotAggResultStat(res_agg_mmi)
  
  ai <- ifelse(i < 10, paste0("0", i), as.char(i)) ; a <- as.char(10*alpha_vec)[i]
  savePlot(paste0("./cv_glmnet_alpha/fig_agg_res_", ai, "_alpha_", a, ".png"), "png")
  graphics.off()
  
  cat("OVERALL PROGRESS: ")
  catProgress(i, len(alpha_vec))
}  



## DATA SUBSET: ALL COHORTS, NO-PROFILE, 3 CLASSES
###################################################


outlist_all_alphas <- list("alpha_0"=runSupervisedCV_repeats(n=3, dat=dat_np_use, model_type="GLMNET", CV_K=10, alpha=0),
                           "alpha_0.2"=runSupervisedCV_repeats(n=3, dat=dat_np_use, model_type="GLMNET", CV_K=10, alpha=0.2),
                           "alpha_0.4"=runSupervisedCV_repeats(n=3, dat=dat_np_use, model_type="GLMNET", CV_K=10, alpha=0.4),
                           "alpha_0.6"=runSupervisedCV_repeats(n=3, dat=dat_np_use, model_type="GLMNET", CV_K=10, alpha=0.6),
                           "alpha_0.8"=runSupervisedCV_repeats(n=3, dat=dat_np_use, model_type="GLMNET", CV_K=10, alpha=0.8),
                           "alpha_1"=runSupervisedCV_repeats(n=3, dat=dat_np_use, model_type="GLMNET", CV_K=10, alpha=1))



path_delim <- paste0("_a", c("00", "02", "04", "06", "08", "10"))
for(i in 1:len(outlist_all_alphas2)) {
  plotAggResultStat(resultAggregator(outlist_all_alphas2[[i]][[2]]))
  savePlot(paste0("./cv_glmnet_alpha/fig_res_agg", path_delim[i], ".png"), "png")
  evaluateLambdaResults(outlist_all_alphas2[[i]], output_path_delim=path_delim[i])
  graphics.off()
}



out <- runSupervisedCV_repeats(n=3, dat=dat_np_use, model_type="GLMNET", CV_K=5, alpha=1, 
                               GLMNET_OUT_DIR = "./cv_glmnet output/", GLMNET_RUN_CV=F)



## DATA SUBSET: ALL COHORTS, NO-PROFILE, 2 CLASSES
###################################################


outlist_all_alphas2 <- list("alpha_0"=runSupervisedCV_repeats(n=3, dat=dat_np_use2, model_type="GLMNET", CV_K=10, alpha=0),
                            "alpha_0.2"=runSupervisedCV_repeats(n=3, dat=dat_np_use2, model_type="GLMNET", CV_K=10, alpha=0.2),
                            "alpha_0.4"=runSupervisedCV_repeats(n=3, dat=dat_np_use2, model_type="GLMNET", CV_K=10, alpha=0.4),
                            "alpha_0.6"=runSupervisedCV_repeats(n=3, dat=dat_np_use2, model_type="GLMNET", CV_K=10, alpha=0.6),
                            "alpha_0.8"=runSupervisedCV_repeats(n=3, dat=dat_np_use2, model_type="GLMNET", CV_K=10, alpha=0.8),
                            "alpha_1"=runSupervisedCV_repeats(n=3, dat=dat_np_use2, model_type="GLMNET", CV_K=10, alpha=1))


outlist_all_alphas3 <- list("alpha_0"=runSupervisedCV_repeats(n=3, dat=dat_np_use3, model_type="GLMNET", CV_K=10, alpha=0),
                            "alpha_0.2"=runSupervisedCV_repeats(n=3, dat=dat_np_use3, model_type="GLMNET", CV_K=10, alpha=0.2),
                            "alpha_0.4"=runSupervisedCV_repeats(n=3, dat=dat_np_use3, model_type="GLMNET", CV_K=10, alpha=0.4),
                            "alpha_0.6"=runSupervisedCV_repeats(n=3, dat=dat_np_use3, model_type="GLMNET", CV_K=10, alpha=0.6),
                            "alpha_0.8"=runSupervisedCV_repeats(n=3, dat=dat_np_use3, model_type="GLMNET", CV_K=10, alpha=0.8),
                            "alpha_1"=runSupervisedCV_repeats(n=3, dat=dat_np_use3, model_type="GLMNET", CV_K=10, alpha=1))




path_delim <- paste0("_k2sp_a", c("00", "02", "04", "06", "08", "10"))
outlist_a <- outlist_all_alphas2
for(i in 1:len(outlist_a)) {
  plotAggResultStat(resultAggregator(outlist_a[[i]][[2]]), class=c("pref", "non-pref"))
  savePlot(paste0("./cv_glmnet_alpha/fig_res_agg", path_delim[i], ".png"), "png")
  evaluateLambdaResults(outlist_a[[i]], output_path_delim=path_delim[i], class=c("spref", "non-spref"))
  graphics.off()
}



outlist_all_alphas_sp_shrv <- list("alpha_0"=runSupervisedCV_repeats(n=5, dat=dat_np_use2_ae_act, model_type="GLMNET", CV_K=5, alpha=0),
                                   "alpha_0.25"=runSupervisedCV_repeats(n=5, dat=dat_np_use2_ae_act, model_type="GLMNET", CV_K=5, alpha=0.25),
                                   "alpha_0.5"=runSupervisedCV_repeats(n=5, dat=dat_np_use2_ae_act, model_type="GLMNET", CV_K=5, alpha=0.5),
                                   "alpha_0.75"=runSupervisedCV_repeats(n=5, dat=dat_np_use2_ae_act, model_type="GLMNET", CV_K=5, alpha=0.75),
                                   "alpha_1"=runSupervisedCV_repeats(n=5, dat=dat_np_use2_ae_act, model_type="GLMNET", CV_K=5, alpha=1))




path_delim <- paste0("_shrv_a", c("00", "025", "045", "075", "10"))
outlist_a <- outlist_all_alphas_sp_shrv
for(i in 1:len(outlist_a)) {
  plotAggResultStat(resultAggregator(outlist_a[[i]][[2]]), class=c("pref", "non-pref"))
  savePlot(paste0("./cv_glmnet_alpha/fig_res_agg", path_delim[i], ".png"), "png")
  evaluateLambdaResults(outlist_a[[i]], output_path_delim=path_delim[i], class=c("spref", "non-spref"))
  graphics.off()
}





#######################################
####  GLMNET ORDINAL model fitting ####
#######################################

# out <- runSupervisedCV(dat=dat_np_use, model_type="GLMNET_ORDINAL", CV_K=10)

outlist_all_ord_alphas <- list("alpha_0"=runSupervisedCV_repeats(n=3, dat=dat_np_ae, model_type="GLMNET_ORDINAL", CV_K=10, alpha=0),
                               "alpha_0.2"=runSupervisedCV_repeats(n=3, dat=dat_np_ae, model_type="GLMNET_ORDINAL", CV_K=10, alpha=0.2),
                               # "alpha_0.4"=runSupervisedCV_repeats(n=3, dat=dat_np_ae, model_type="GLMNET_ORDINAL", CV_K=10, alpha=0.4),
                               # "alpha_0.6"=runSupervisedCV_repeats(n=3, dat=dat_np_ae, model_type="GLMNET_ORDINAL", CV_K=10, alpha=0.6),
                               "alpha_0.8"=runSupervisedCV_repeats(n=3, dat=dat_np_ae, model_type="GLMNET_ORDINAL", CV_K=10, alpha=0.8),
                               "alpha_1"=runSupervisedCV_repeats(n=3, dat=dat_np_ae, model_type="GLMNET_ORDINAL", CV_K=10, alpha=1))



path_delim <- paste0("_ord_a", c("00", "02", "04", "06", "08", "10"))
path_delim <- paste0("_ord_a", c("00", "02", "06", "10"))
outlist_a <- outlist_all_ord_alphas
for(i in 1:len(outlist_a)) {
  evaluateLambdaResults(outlist_a[[i]], "GLMNET_ORD", output_path_delim=path_delim[i], plots_close = T)
  catProgress(i, len(outlist_a))
}




#######################################
####  Random Forest model fitting  ####
#######################################


out_ntree500 <- runSupervisedCV(dat=dat_np_use, model_type="RF", CV_K=10, ntree=500)
plotAggResultStat(resultAggregator(out_ntree500[[2]], model_type="RF"), "RF")


out_ntree100 <- runSupervisedCV_repeats(5, dat=dat_np_use, model_type="RF", CV_K=10, ntree=150)
plotAggResultStat(resultAggregator(out_ntree100[[2]], model_type="RF"), "RF")


out_ntree100 <- runSupervisedCV(dat=dat_np_use, model_type="RF", CV_K=10, ntree=100)
plotAggResultStat(resultAggregator(out_ntree100[[2]], model_type="RF"), "RF")


out_ntree1 <- runSupervisedCV(dat=dat_np_use, model_type="RF", CV_K=10, ntree=1)
plotAggResultStat(resultAggregator(out_ntree1[[2]], model_type="RF"), "RF")



