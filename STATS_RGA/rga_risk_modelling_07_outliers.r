
library(rhdf5)
H5close()

source("../useful_funs.r")

datlist <- getDataFromDir("~/HealthQ/RGA/Report/outliers/", head=T)
datlist <- datlist[grep("allrepeats", names(datlist))]



i <- 1
dat <- datlist[[i]]
nms_ae <- grep("AE", names(dat))
nms_clf <- grep("CLF", names(dat))

# miscl_sp_list <- vector("list", 10)
# miscl_np_list <- vector("list", 10)
# for(j in 1:10) {
#   y <- dat[, match(paste0(c("y_", "yhat_"), "test_", j), names(dat))]
#   
#   # spref UW, predicted npref
#   miscl_sp_list[[j]] <- which((y[,1] == 0) & (y[,2] >= quantile(y[,2], 0.75)))
#   
#   # npref UW, predicted spref
#   miscl_np_list[[j]] <- which((y[,1] == 2) & (y[,2] <= quantile(y[,2], 0.25)))
# }
# 
# 
# nms_miscl_sp <- names(which(table(unl(miscl_sp_list)) > 2))
# nms_miscl_np <- names(which(table(unl(miscl_np_list)) > 1))
# 
# View(dat[as.num(nms_miscl_np), -nms_clf])


dat_pt <- read.table("~/HealthQ/RGA/Dirk modelling results/fixedID_regression_pred_v_targ.csv", head=T, sep=",")[,-1]

dat_match <- match(as.char(dat[,1]), as.char(dat_pt[,1]))
dat_pt <- cb(dat[, c(1, nms_ae)], dat_pt[dat_match, -1])
dat_pt[,1] <- as.char(dat_pt[,1])

dat_use_match <- match(dat_pt[,1], as.char(dat_use[,1]))

dat_pt <- cb(dat_use[dat_use_match, c(1, 16:17)], dat_pt[,-1])
dat_pt[,1] <- as.char(dat_pt[,1])

i_y <- grep("y_test", names(dat_pt))
i_yhat <- grep("yhat_test", names(dat_pt))


# w() ; par(ask=T)
# for(j in 1) {
  # dat_gg <- data.frame("target"=factor(dat_pt[,2]), "prediction"=dat_pt[, grep("yhat_test", names(dat_pt))[j]])
  # levels(dat_gg[,1]) <- c("s-pref", "pref", "standard", "rated")
  # 
  # g1 <- ggplot(dat_gg, aes(x=prediction, fill=target))
  # g1 <- g1 + xlab("Predicted value") + ylab("Density") + ggtitle(paste0("Distribution of predicted values by true class"))
  # g1 <- g1 + geom_density(alpha=0.4) + coord_cartesian(xlim = c(0.1, 1))
  # g1 <- g1 + theme(legend.title = element_blank(), legend.justification = c(1, 1), legend.position = c(1, 1))
  # w(9, 5) ; print(g1)
# }


##############################################################################################
##############################################################################################

# w(9, 9) ; par(ask = T)
# for(i in 1:nrow(dat_pt)) {
#   # w(9, 9)  
#   hist(as.num(dat_pt[i, i_yhat]), breaks="scott", xlim=c(0,1), col="mediumseagreen", main=paste0("Histogram | participant ", i))
# }
# 
# rngs <- apply(apply(dat_pt[, i_yhat], 1, range), 2, diff)

dat_pt <- cb(dat_pt[,1:(i_y[1]-1)], "yhat_test_median" = apply(dat_pt[, i_yhat], 1, median) )



##############################################################################################
#####  GRAPHICAL INVESTIGATION
##############################################################################################


w(12, 10) ; par(ask = T)
cols <- c("blue", "mediumseagreen", "orange", "red")
for(i in 1:5)
plot_g(100-dat_pt[,3+i], 1-dat_pt$yhat_test_median, col=cols[dat_pt$UW_K4 + 1], 
       main = names(dat_pt)[3+i], ylim=c(0, 1), xlim=c(0,100))


dat_gg <- dat_pt[, c(2, 4:8)]
dat_gg[,-1] <- apply(dat_gg[, -1], 2, function(x) 100-x)
dat_gg[,1] <- factor(dat_gg[,1])
levels(dat_gg[,1]) <- c("s-pref", "pref", "standard", "rated")

dat_gg <- data.frame("UW_K4"=rep(dat_gg[,1], 5), "AE"=as.num(as.matrix(dat_gg[,-1])),
                     "AEname"= factor(rep(as.char(1:5), each=nrow(dat_pt))))
levels(dat_gg[,3]) <- c("Activity", "Heart Rate", "Pulse Waveform", "Sleep", "Heart Rate Variability")


g1 <- ggplot(dat_gg, aes(x=UW_K4, y=AE, fill=UW_K4))
g1 <- g1 + facet_grid(~AEnames)
g1 <- g1 + geom_boxplot(alpha = 0.4) + guides(fill=FALSE)
w(10, 10) ; print(g1)

# g1 <- g1 + xlab("Activity AE score") + ylab("Density")# + ggtitle(paste0("Distribution of predicted values by true class (outliers removed)"))
# g1 <- g1 + geom_density(alpha=0.4) + coord_cartesian(xlim = c(0, 100))
# g1 <- g1 + theme(legend.title = element_blank(), legend.justification = c(1, 1), legend.position = c(1, 1))
# w(9, 5) ; print(g1)





##############################################################################################
#####  ASSIGNING 12 MOST MISCLASSIFIED CASES TO DIRK AND RENIER
##############################################################################################


targ <- dat_pt$UW_K4
pred <- dat_pt$yhat_test_median

miscl_sp <- which((targ == 0) & (pred >= quantile(pred, 0.75)))
miscl_np <- which((targ >= 2) & (pred <= quantile(pred, 0.75)))

itp <- cb(1:nrow(dat_pt), targ, pred)
itp_ord <- itp[order(pred),]

miscl_sp <- tail(itp_ord[itp_ord[,2] == 0, 1], 11)
miscl_np <- head(itp_ord[itp_ord[,2] >= 2, 1], 15)


targ <- targ[-c(miscl_sp, miscl_np)]
pred <- pred[-c(miscl_sp, miscl_np)]

dat_gg <- data.frame("target"=factor(targ), "prediction"=pred)
# evels(dat_gg[,1]) <- c("s-pref", "pref", "n-pref")
levels(dat_gg[,1]) <- c("s-pref", "pref", "standard", "rated")

g1 <- ggplot(dat_gg, aes(x=prediction, fill=target))
g1 <- g1 + xlab("Predicted value") + ylab("Density")# + ggtitle(paste0("Distribution of predicted values by true class (outliers removed)"))
g1 <- g1 + geom_density(alpha=0.4) + coord_cartesian(xlim = c(0.1, 1))
g1 <- g1 + theme(legend.title = element_blank(), legend.justification = c(1, 1), legend.position = c(1, 1))
w(9, 5) ; print(g1)



########################################3


targ <- dat_pt$UW_K4
pred <- dat_pt$yhat_test_median

miscl_sp <- which((targ == 0) & (pred >= quantile(pred, 0.75)))
miscl_np <- which((targ >= 2) & (pred <= quantile(pred, 0.75)))

itp <- cb(1:nrow(dat_pt), targ, pred)
itp_ord <- itp[order(pred),]

miscl_sp <- tail(itp_ord[itp_ord[,2] == 0, 1], 11)
miscl_np <- head(itp_ord[itp_ord[,2] >= 2, 1], 15)


targ2 <- targ[-c(miscl_sp, miscl_np)]
pred2 <- pred[-c(miscl_sp, miscl_np)]

dat_gg <- data.frame("target"=factor(c(targ, targ2)), "prediction"=c(pred, pred2), 
                     "fct"=rep(c("1", "2"), c(len(targ), len(targ2))))
# evels(dat_gg[,1]) <- c("s-pref", "pref", "n-pref")
levels(dat_gg[,1]) <- c("super-pref", "pref", "standard", "rated")
levels(dat_gg[,3]) <- c("Full sample", "Excluding outliers")

g1 <- ggplot(dat_gg, aes(x=prediction, fill=target)) + facet_wrap(~fct, ncol=1) 
g1 <- g1 + xlab("Predicted value") + ylab("Density")# + ggtitle(paste0("Distribution of predicted values by true class (outliers removed)"))
g1 <- g1 + geom_density(alpha=0.4) + coord_cartesian(xlim = c(0.1, 1))
g1 <- g1 + theme(legend.title = element_blank(), legend.justification = c(1, 1), legend.position = c(1, 1))
w(10, 10) ; print(g1)



#########################################



















dat_mc_assigned <- cb("AssignedTo"=rep(c("R", "D"), each=3),
                      "Misclassification"=rep(c("MISCLASS_SUPERPREF", "MISCLASS_NONPREF"), each=6), 
                      dat_pt[c(miscl_sp, miscl_np),])


write.table2(dat_mc_assigned, "./misclass.csv", col.names=T)



dat_df[match(dat_mc_assigned$ID, dat_df[,3]), c(1, 3)]


##############################################################################################
#####  INVESTIGATE AGGREGATED FEATURES 
##############################################################################################

dat <- dat_mc_assigned[dat_mc_assigned[,1] == "R", ]


i_act <- grep("ACTIVITY", nms_all)
i_hr <- which("HR." == substr(nms_all, 1, 3))
i_pwf <- grep("PWF", nms_all)
i_sleep <- which("SLEEP." == substr(nms_all, 1, 6))
i_hrv <- grep("SLEEP_HRV", nms_all)



####################################################
####  CASE 1: f360e32a-a538-4fbb-ae13-2ee8775bc846
####################################################

j <- 1
dat[j,]
c1 <- dat_all[which(dat_all$ANON_ID == dat[j,]$ID), ]

# dat_c1 <- h5read("~/HealthQ/RGA/Report/outliers/cases/f360e32a-a538-4fbb-ae13-2ee8775bc846.h5", 
                 # "/summary_metrics", bit64conversion="double", compoundAsDataFrame=T)$table


c1[1:29]



i_plot <- i_act

c1[i_plot]

w(10, 10) ; par(ask = T)
for(i in 1:len(i_plot)) {
  xx <- na.omit(as.num(dat_all[,i_plot[i]]))
  hist(xx, breaks="scott", col="mediumseagreen", main=nms_all[i_plot][i])
  abline(v = c1[i_plot[i]], col="red", lwd=3)
  title(sub=paste0("Percentile = ", round(ecdf(xx)(as.num(c1[i_plot[i]])), 2) ))
  
}





####################################################
####  CASE 2: 0dc3f594-3abe-4e4c-b0a9-f0887ed3346a
####################################################

j <- 2
dat[j,]
c2 <- dat_all[which(dat_all$ANON_ID == dat[j,]$ID), ]


c2[1:29]


i_plot <- i_sleep

c2[i_plot]

w(10, 10) ; par(ask = T)
for(i in 1:len(i_plot)) {
  xx <- na.omit(as.num(dat_all[,i_plot[i]]))
  hist(xx, breaks="scott", col="mediumseagreen", main=nms_all[i_plot][i])
  abline(v = c2[i_plot[i]], col="red", lwd=3)
  title(sub=paste0("Percentile = ", round(ecdf(xx)(as.num(c2[i_plot[i]])), 2) ))
  
}






####################################################
####  CASE 3: f0cbc2b8-c818-4154-afe7-a818440a8918
####################################################

j <- 3
dat[j,]
c3 <- dat_all[which(dat_all$ANON_ID == dat[j,]$ID), ]


c3[1:29]


i_plot <- i_hrv

c3[i_plot]

w(10, 10) ; par(ask = T)
for(i in 1:len(i_plot)) {
  xx <- na.omit(as.num(dat_all[,i_plot[i]]))
  hist(xx, breaks="scott", col="mediumseagreen", main=nms_all[i_plot][i])
  abline(v = c3[i_plot[i]], col="red", lwd=3)
  title(sub=paste0("Percentile = ", round(ecdf(xx)(as.num(c3[i_plot[i]])), 2) ))
  
}








####################################################
####  CASE 7: d8db9614-459a-4fc6-a237-2eb2d1b8db32
####################################################

j <- 4
dat[j,]
c7 <- dat_all[which(dat_all$ANON_ID == dat[j,]$ID), ]


c7[1:29]


i_plot <- i_hrv

c7[i_plot]

w(10, 10) ; par(ask = T)
for(i in 1:len(i_plot)) {
  xx <- na.omit(as.num(dat_all[,i_plot[i]]))
  hist(xx, breaks="scott", col="mediumseagreen", main=nms_all[i_plot][i])
  abline(v = c7[i_plot[i]], col="red", lwd=3)
  title(sub=paste0("Percentile = ", round(ecdf(xx)(as.num(c7[i_plot[i]])), 2) ))
  
}








####################################################
####  CASE 8: 476f698a-ce26-460e-9a64-5d3132bb8c43
####################################################

j <- 5
dat[j,]
c8 <- dat_all[which(dat_all$ANON_ID == dat[j,]$ID), ]


c8[1:29]


i_plot <- i_hrv

c8[i_plot]

w(10, 10) ; par(ask = T)
for(i in 1:len(i_plot)) {
  xx <- na.omit(as.num(dat_all[,i_plot[i]]))
  hist(xx, breaks="scott", col="mediumseagreen", main=nms_all[i_plot][i])
  abline(v = c8[i_plot[i]], col="red", lwd=3)
  title(sub=paste0("Percentile = ", round(ecdf(xx)(as.num(c8[i_plot[i]])), 2) ))
  
}









####################################################
####  CASE 9: fd3c9f0b-a3bc-4db1-acc2-f69f2dd547c7
####################################################

j <- 6
dat[j,]
c9 <- dat_all[which(dat_all$ANON_ID == dat[j,]$ID), ]


c9[1:29]


i_plot <- i_hrv

c9[i_plot]

w(10, 10) ; par(ask = T)
for(i in 1:len(i_plot)) {
  xx <- na.omit(as.num(dat_all[,i_plot[i]]))
  hist(xx, breaks="scott", col="mediumseagreen", main=nms_all[i_plot][i])
  abline(v = c9[i_plot[i]], col="red", lwd=3)
  title(sub=paste0("Percentile = ", round(ecdf(xx)(as.num(c9[i_plot[i]])), 2) ))
  
}


