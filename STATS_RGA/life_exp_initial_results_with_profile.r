source("../useful_funs.r")

fpath <- "~/HealthQ/RGA/LE/"


SA_cohort_profile <- read.table("~/HealthQ/RGA/LE/LifeQ_profile_RHR_BMI_VO2max_based_life_expectancies.csv", head=T, sep=",")
UK_cohort_profile <- read.table("~/HealthQ/RGA/LE/RGA_profile_RHR_BMI_VO2max_based_life_expectancies.csv", head=T, sep=",")
SA_cohort_profile_submax <- read.table("~/HealthQ/RGA/LE/LifeQ_profile_RHR_BMI_plus_submax_VO2max_based_life_expectancies.csv", head=T, sep=",")



########################################
####  Massage data into one matrix  ####
########################################

dat <- SA_cohort_profile
dat_match_submax <- match(SA_cohort_profile_submax$email, dat$email)
dat$VO2max_submax <- integer(nrow(dat))
dat$VO2max_submax[dat_match_submax] <- SA_cohort_profile_submax$VO2max
dat$lifeq_Life_Expectancy_submax <- integer(nrow(dat))
dat$lifeq_Life_Expectancy_submax[dat_match_submax] <- SA_cohort_profile_submax$lifeq_Life_Expectancy
dat$email_upper <- toupper(as.character(dat$email))



##############################################
####  Plot diff between VO2max estimates  ####
##############################################

v2 <- dat$VO2max_submax
v2[which(v2 == 0)] <- dat$VO2max[which(v2 == 0)]

cols = rep("red", nrow(dat))
cols[which(dat$VO2max_submax == 0)] <- "blue"


vo2_err <- apply(dat[-which(dat$VO2max_submax == 0), c(9, 12)], 1, diff)
vo2_mape <- round(perf(dat[-which(dat$VO2max_submax == 0), 12], dat[-which(dat$VO2max_submax == 0), 9])[4], 2)

w() ; par(mfrow=c(1,2))
plot_g(dat$VO2max, v2, col=cols, main=c(paste0("VO2max estimates: profile vs submax (MAE = ", round(mean(abs(vo2_err)), 2),
                                               ", MAPE = ", vo2_mape, "%)")),
       xlab="profile VO2max estimate", ylab="VO2max submax estimate")
legend("bottomright", col=c("blue", "red"), pch=19, leg=c("profile", "submax"), bg="white")
hist(vo2_err, breaks="scott", prob=T, main="VO2max estimate difference distribution", 
     col="blue", xlab="(profile estimate - submax estimate)")



###################################################################
####  Plot diff between LE values based on different VO2maxes  ####
###################################################################

le2 <- dat$lifeq_Life_Expectancy_submax
le2[which(le2 == 0)] <- dat$lifeq_Life_Expectancy[which(le2 == 0)]


cols = rep("red", nrow(dat))
cols[which(dat$lifeq_Life_Expectancy_submax == 0)] <- "blue"


le_err <- apply(dat[-which(dat$lifeq_Life_Expectancy_submax == 0), c(10, 13)], 1, diff)
le_mape <- round(perf(dat[-which(dat$lifeq_Life_Expectancy_submax == 0), 13], dat[-which(dat$lifeq_Life_Expectancy_submax == 0), 10])[4], 2)

w() ; par(mfrow=c(1,2))
plot_g(dat$lifeq_Life_Expectancy, le2, col=cols, main=c(paste0("LE-profile vs LE-submax (MAE = ", round(mean(abs(le_err)), 2),
                                               ", MAPE = ", le_mape, "%)")),
       xlab="LE-profile", ylab="LE-submax")
legend("bottomright", col=c("blue", "red"), pch=19, leg=c("profile", "submax"), bg="white")
hist(le_err, breaks="scott", prob=T, main="LE difference distribution", 
     col="blue", xlab="(LE_profile - LE_submax)")




#############################################
####  LE correspondence with UW results  ####
#############################################

source("./uw_results_refactor.r")

le <- le2
# le <- dat$lifeq_Life_Expectancy

# Investigate OVERALL UW
le_df <- data.frame(ID=uw[,1], UW=uw_overall2, LE=le, AGE=dat$Age, BMI=dat$BMI, V=v2)
le_df <- le_df[le_df[,2] != 99, ]

# uw_ov <- data.frame(uw[,1], uw_overall2)
# uw_ov[,1] <- as.character(uw_ov[,1])
# cb(uw_ov[,1], dat$email_upper, match(uw_ov[,1], dat$email_upper))

uw_cols <- c("blue", "red", "orange")[le_df$UW+1]

w(14, 12)
plot_g(le_df$LE - le_df$AGE, le_df$LE, col=uw_cols,
       main=paste0("Correspondence between LE and UW (n = ", nrow(le_df), ")"),
       xlab="life remaining", ylab="LifeQ life expectancy")
legend("bottomright", col=unique(uw_cols), pch=19, bg="white",
       leg=c("standard", "rated", "not accepted"))



# w(14, 12)
# plot_g(le_df$BMI, le_df$LE, col=uw_cols,
#        main=paste0("Correspondence between LE and UW (n = ", nrow(le_df), ")"),
#        xlab="life remaining", ylab="LifeQ life expectancy")
# legend("topright", col=unique(uw_cols), pch=19, bg="white",
#        leg=c("standard", "rated", "not accepted"))




# Investigate PREF/SPREF UW
le_df <- data.frame(ID=uw[,1], UW=uw_pref3, LE=le, AGE=dat$Age, BMI=dat$BMI)

uw_cols <- c("blue", "red", "orange")[le_df$UW+1]

w(14, 12)
plot_g(le_df$LE - le_df$AGE, le_df$LE, col=uw_cols,
       main=paste0("Correspondence between LE and UW (n = ", nrow(le_df), ")"),
       xlab="life remaining", ylab="LifeQ life expectancy")
legend("bottomright", col=c("blue", "red", "orange"), pch=19, bg="white",
       leg=c("super-preferred", "preferred", "non-preferred"))



# w(14, 12)
# plot_g(le_df$BMI, le_df$LE, col=uw_cols,
#        main=paste0("Correspondence between LE and UW (n = ", nrow(le_df), ")"),
#        xlab="life remaining", ylab="LifeQ life expectancy")
# legend("topright", col=c("blue", "red", "orange"), pch=19, bg="white",
#        leg=c("super-preferred", "preferred", "not preferred"))



####################################################
####  Life Gain correspondence with UW results  ####
####################################################


le <- le2

# Investigate OVERALL UW
le_df <- data.frame(ID=uw[,1], UW=uw_overall2, LE=le, AGE=dat$Age, BMI=dat$BMI, 
                    LR=le-dat$Age, LG=le-dat$populatoin_io_Life_Expectancy)
le_df <- le_df[le_df[,2] != 99, ]

# uw_ov <- data.frame(uw[,1], uw_overall2)
# uw_ov[,1] <- as.character(uw_ov[,1])
# cb(uw_ov[,1], dat$email_upper, match(uw_ov[,1], dat$email_upper))

uw_cols <- c("blue", "red", "orange")[le_df$UW+1]

w(14, 12)
plot_g(le_df$LR, le_df$LG, col=uw_cols,
       main=paste0("Correspondence between Life Gain and UW (n = ", nrow(le_df), ")"),
       xlab="life remaining", ylab="life gain")
legend("bottomright", col=unique(uw_cols), pch=19, bg="white",
       leg=c("standard", "rated", "not accepted"))

cor(le_df$LG, le_df$UW)




# Investigate PREF/SPREF UW
le_df <- data.frame(ID=uw[,1], UW=uw_pref3, LE=le, AGE=dat$Age, BMI=dat$BMI, 
                    LR=le-dat$Age, LG=le-dat$populatoin_io_Life_Expectancy)

uw_cols <- c("blue", "red", "orange")[le_df$UW+1]

w(14, 12)
plot_g(le_df$LE, le_df$LG, col=uw_cols,
       main=paste0("Correspondence between LE and UW (n = ", nrow(le_df), ")"),
       xlab="life remaining", ylab="LifeQ life expectancy")
legend("bottomright", col=c("blue", "red", "orange"), pch=19, bg="white",
       leg=c("super-preferred", "preferred", "non-preferred"))


cor(le_df$LG, le_df$UW)





write.table2(data.frame(EMAIL=uw[,1], UW_OVERALL=uw_overall2, UW_PREF=uw_pref3), "~/HealthQ/RGA/20170516_uw_reformatted.csv", col.names=T)


