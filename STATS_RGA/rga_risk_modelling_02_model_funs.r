library(ROCR)
library(caTools)

calc_auc_ROCR <- function(prob, true) 
  return(performance(prediction(prob, as.factor(true)), measure="auc")@y.values[[1]])




runSupervisedCV <- function(dat,
                            model_type = c("GLMNET", "GLMNET_ORDINAL", "RF")[1],
                            CV_K = 5,
                            CV_SEED = 0,
                            CV_STRATIFY_FOLDS = TRUE,
                            CV_TRAIN_RESAMPLE = TRUE,
                            PRINT_PROGRESS = TRUE,
                            GLMNET_OUT_DIR = "./cv_glmnet output/",
                            GLMNET_RUN_CV = FALSE,
                            ...)
{
  
  
  m <- len(unique(dat[,1]))
  
  
  # Remove rows with NA
  ######################
  which_rows_NA <- apply(dat, 1, function(x) any(is.na(x)))
  dat <- dat[!which_rows_NA, ]
  
  
  # Vector importance
  #####################
  if(model_type %in% c("GLMNET", "GLMNET_ORDINAL")) {
    var_select_counts <- matrix(0, nrow = (ncol(dat)-1), ncol=CV_K)
    rownames(var_select_counts) <- names(dat)[-1]
    
    var_coeffs_counts <- matrix(0, nrow = (ncol(dat)-1), ncol=CV_K)
    rownames(var_coeffs_counts) <- names(dat)[-1]
  }

  var_imp_list <- vector("list", CV_K)
  
  
  # Partition data into folds
  ############################
  set.seed(CV_SEED)
  
  if(!CV_STRATIFY_FOLDS) {
    folds <- sample(rep(1:CV_K, each=ceiling(nrow(dat)/CV_K)))[1:nrow(dat)]
    dat$FOLDS <- folds
  }
  if(CV_STRATIFY_FOLDS) {
    nclass <- table(dat[,1])
    
    for(i in 1:m) 
      dat$FOLDS[dat[,1] == as.numeric(names(nclass)[i])] <- sample(rep(1:CV_K, each=ceiling(nclass[i]/CV_K)))[1:nclass[i]]

  }
  
  
  # for(i in 1:3) print(table(dat[dat$FOLDS == i, 1])/nrow(dat[dat$FOLDS == i,]))
  
  cvout <- NA
  
  
  # Iterate over CV folds
  ########################
  model_list <- vector("list", CV_K)
  res_list <- vector("list", CV_K)
  for(k in 1:CV_K) {
    
    # Split into test and train based on fold 'k'
    i_f <- grep("FOLDS", names(dat))
    dat_train <- dat[dat$FOLDS != k, -i_f]
    dat_test <- dat[dat$FOLDS == k, -i_f]
    
    
    if(CV_TRAIN_RESAMPLE) {
      
      dat_train <- balance_data_with_SMOTE(dat_train)
      dat_train[,1] <- as.numeric(dat_train[,1])
      
      if(any(colnames(dat_train) == "BIO_SEX"))
        dat_train[, grep("BIO_SEX", colnames(dat_train))] <- round(dat_train[, grep("BIO_SEX", colnames(dat_train))], 0)
      
    }
    
    # Ensure randomised observations
    dat_train <- dat_train[sample(1:nrow(dat_train)),]
    dat_train <- apply(dat_train, 2, as.num)
    
    
    # Fit models
    ########################
    if((model_type == "GLMNET") && (m == 3)) {
      
      out <- glmnet(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), 
                    family="multinomial", type.multinomial="grouped", ...)
      
      
      cvout <- NA
      if(GLMNET_RUN_CV) {
        cvout <- cv.glmnet(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), 
                           family="multinomial", type.multinomial="grouped", ...)
        
        cvout <- c("lambda_1se"=cvout$lambda.1se, "index"=which(out$lambda == cvout$lambda.1se))
      }
      
      
      # Store selected variables output
      ##################################
      bnames <- lapply(out$beta, function(x) {
        xmat <- as.matrix(x)
        b <- apply(xmat, 2, function(y) y[y != 0])
        names(b) <- round(out$lambda, 4)
        return(b) })[[1]]
      
      
      # Update selected var counts
      for(i in 1:len(bnames)) {
        if(len(bnames[[i]]) > 0) {
          i_match <- match(names(bnames[[i]]), rownames(var_select_counts))
          var_select_counts[i_match, k] <- var_select_counts[i_match, k] + 1
          var_coeffs_counts[i_match, k] <- var_coeffs_counts[i_match, k] + bnames[[i]]
        }
      }
      
      
      # Predictions and confusion matrices
      #####################################
      out_pred_train <- predict(out, newx=as.matrix(dat_train[,-1]), type="class")
      out_pred_test <- predict(out, newx=as.matrix(dat_test[,-1]), type="class")
      out_confmat_train <- apply(out_pred_train, 2, function(x) list(table(factor(dat_train[,1], levels=0:2), factor(as.num(x), levels=0:2))))
      out_confmat_test <- apply(out_pred_test, 2, function(x) list(table(factor(dat_test[,1], levels=0:2), factor(as.num(x), levels=0:2))))
      
      out_prob_test <- predict(out, newx=as.matrix(dat_test[,-1]), type="response")
      out_prob_test <- apply(out_prob_test, 3, function(x) data.frame("TRUE"=as.num(as.char(dat_test[,1])), "PROB"=x))
      
    }
    
    if((model_type == "GLMNET") && (m == 2)) {
      
      out <- glmnet(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), 
                    family="binomial", ...)
      
      if(GLMNET_RUN_CV) {
        cvout <- cv.glmnet(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), 
                           family="binomial", ...)
        
        cvout <- c("lambda_1se"=cvout$lambda.1se, "index"=which(out$lambda == cvout$lambda.1se))
      }
      
      
      # Store selected variables output
      ##################################
      bnames <- apply(as.matrix(out$beta), 2, function(y) y[y != 0])
      names(bnames) <- round(out$lambda, 4)
        
    
      # Update selected var counts
      for(i in 1:len(bnames)) {
        if(len(bnames[[i]]) > 0) {
          i_match <- match(names(bnames[[i]]), rownames(var_select_counts))
          var_select_counts[i_match, k] <- var_select_counts[i_match, k] + 1
          var_coeffs_counts[i_match, k] <- var_coeffs_counts[i_match, k] + bnames[[i]]
        }
      }
      
      # Predictions and confusion matrices
      #####################################
      out_pred_train <- predict(out, newx=as.matrix(dat_train[,-1]), type="class")
      out_pred_test <- predict(out, newx=as.matrix(dat_test[,-1]), type="class")
      out_confmat_train <- apply(out_pred_train, 2, function(x) list(table(factor(dat_train[,1], levels=0:1), factor(as.num(x), levels=0:1))))
      out_confmat_test <- apply(out_pred_test, 2, function(x) list(table(factor(dat_test[,1], levels=0:1), factor(as.num(x), levels=0:1))))
      
      
      out_prob_test <- predict(out, newx=as.matrix(dat_test[,-1]), type="response")
      out_prob_test <- apply(out_prob_test, 2, function(x) data.frame("TRUE"=as.num(as.char(dat_test[,1])), "PROB"=x))
      
    }
    
    if((model_type == "GLMNET_ORDINAL") && (m == 3)) {
      
      out <- glmnet.cr(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), maxit=250, ...)
      
      if(GLMNET_RUN_CV)
        cvout <- select.glmnet.cr(out)
      
      
      # Store selected variables output
      ##################################
      bnames <- apply(as.matrix(out$beta), 2, function(y) y[y != 0])
      names(bnames) <- round(out$lambda, 4)
      
      
      # Update selected var counts
      for(i in 1:len(bnames)) {
        if(len(bnames[[i]]) > 0) {
          i_match <- na.omit(match(names(bnames[[i]]), rownames(var_select_counts)))
          
          if(len(i_match) > 0) {
            var_select_counts[i_match, k] <- var_select_counts[i_match, k] + 1
            var_coeffs_counts[i_match, k] <- var_coeffs_counts[i_match, k] + bnames[[i]][-attr(i_match, "na.action")]
          }
        }
      }
      
      
      # Predictions and confusion matrices
      #####################################
      out_pred_train <- predict(out, newx=as.matrix(dat_train[,-1]))[[3]]
      out_pred_test <- predict(out, newx=as.matrix(dat_test[,-1]))[[3]]
      out_confmat_train <- apply(out_pred_train, 2, function(x) list(table(factor(dat_train[,1], levels=0:2), factor(as.num(x), levels=0:2))))
      out_confmat_test <- apply(out_pred_test, 2, function(x) list(table(factor(dat_test[,1], levels=0:2), factor(as.num(x), levels=0:2))))
      
 
      out_prob_test <- predict(out, newx=as.matrix(dat_test[,-1]))[[4]]
      out_prob_test <- apply(out_prob_test, 3, function(x) data.frame("TRUE"=as.num(as.char(dat_test[,1])), "PROB"=x))
      
    }
    
    if((model_type == "RF") && (m == 3)) {
      
      out <- randomForest(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), importance=T, ...)
      
      
      cvout <- NULL
      
      # Update variable importance
      var_imp_list[[k]] <- list("RF_var_importance" = out$importance, "RF_var_importanceSD" = out$importanceSD)
      
      
      # Predictions and confusion matrices
      out_pred_train <- predict(out, as.matrix(dat_train[,-1]), type="class")
      out_pred_test <- predict(out, as.matrix(dat_test[,-1]), type="class")
      
      out_confmat_train <- table(factor(dat_train[,1], levels=0:2), factor(out_pred_train, levels=0:2))
      out_confmat_test <- table(factor(dat_test[,1], levels=0:2), factor(out_pred_test, levels=0:2))
      
      out_prob_test <- cb("TRUE"=as.num(as.char(dat_test[,1])), predict(out, as.matrix(dat_test[,-1]), type="prob"))
      
    }
    
    if((model_type == "RF") && (m == 2)) {
      
      out <- randomForest(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), importance=T, ...)
      
      
      cvout <- NULL
      
      # Update variable importance
      var_imp_list[[k]] <- list("RF_var_importance" = out$importance, "RF_var_importanceSD" = out$importanceSD)
      
      
      # Predictions and confusion matrices
      out_pred_train <- predict(out, as.matrix(dat_train[,-1]), type="class")
      out_pred_test <- predict(out, as.matrix(dat_test[,-1]), type="class")
      
      out_confmat_train <- table(factor(dat_train[,1], levels=0:1), factor(out_pred_train, levels=0:1))
      out_confmat_test <- table(factor(dat_test[,1], levels=0:1), factor(out_pred_test, levels=0:1))
      
      out_prob_test <- cb("TRUE"=as.num(as.char(dat_test[,1])), predict(out, as.matrix(dat_test[,-1]), type="prob"))
      
    }
    
    
    # Update output structures
    ###########################
    model_list[[k]] <- list("OUT" = out, "CV_OUT" = cvout)
    res_list[[k]] <- list("CONFMAT_TRAIN" = out_confmat_train, 
                          "CONFMAT_TEST" = out_confmat_test, 
                          "PRED_PROBS_TEST" = out_prob_test)


    if(PRINT_PROGRESS)
      catProgress(k, CV_K)
    
  }
  
  
  
  if(len(grep("GLM", model_type)) > 0)
    var_imp_list = list("VAR_COUNTS"=var_select_counts, "VAR_COEFF_COUNTS"=var_coeffs_counts)
  
  
  return(list(model_list, res_list, var_imp_list))
  
  
}





runSupervisedCV_MMIRGATest <- function(dat_id, folds,
                                       model_type = c("GLMNET", "GLMNET_ORDINAL", "RF")[1],
                                       CV_TRAIN_RESAMPLE = TRUE,
                                       PRINT_PROGRESS = TRUE,
                                       GLMNET_OUT_DIR = "./cv_glmnet output/",
                                       GLMNET_RUN_CV = FALSE,
                                       ...)
{
  
  
  m <- len(unique(dat_id[,3]))
  CV_K <- len(unique(folds[,2]))
  
  
  # Remove rows with NA
  ######################
  which_rows_NA <- apply(dat_id, 1, function(x) any(is.na(x)))
  dat_id <- dat_id[!which_rows_NA, ]
  
  
  # Vector importance
  #####################
  if(model_type %in% c("GLMNET", "GLMNET_ORDINAL")) {
    var_select_counts <- matrix(0, nrow = (ncol(dat_id)-1), ncol=CV_K)
    rownames(var_select_counts) <- names(dat_id)[-1]

    var_coeffs_counts <- matrix(0, nrow = (ncol(dat_id)-1), ncol=CV_K)
    rownames(var_coeffs_counts) <- names(dat_id)[-1]
  }

  var_imp_list <- vector("list", CV_K)
  
  cvout <- NA

  
  # Partition data into folds
  ############################
  # f <- folds[, c(2, which(names(folds) == paste0("FOLDS", i)))]
  f <- folds
  CV_K <- len(unique(f[,2]))
  
  # Iterate over CV folds
  ########################
  model_list <- vector("list", CV_K)
  res_list <- vector("list", CV_K)
  for(k in 1:CV_K) {
    
    # Split into test and train based on fold 'k'
    dat_train <- dat_id[dat_id$COHORT == "MMI", -c(1:2)]
    
    id_f <- f[f[,2] %in% (1:CV_K)[-k], 1]
      
    
    dat_train <- rbind(dat_train, 
                       dat_id[na.omit(match(id_f, dat_id$ANON_ID)), -c(1:2)])
      
    
    id_f2 <- f[f[,2] %in% k, 1]
    dat_test <- dat_id[na.omit(match(id_f2, dat_id$ANON_ID)), -c(1:2)]
    
    
    if(CV_TRAIN_RESAMPLE) {
      
      dat_train <- balance_data_with_SMOTE(dat_train)
      dat_train[,1] <- as.numeric(dat_train[,1])
      
      if(any(colnames(dat_train) == "BIO_SEX"))
        dat_train[, grep("BIO_SEX", colnames(dat_train))] <- round(dat_train[, grep("BIO_SEX", colnames(dat_train))], 0)
      
    }
    
    # Ensure randomised observations
    dat_train <- dat_train[sample(1:nrow(dat_train)),]
    dat_train <- apply(dat_train, 2, as.num)
    
    
    # Fit models
    ########################
    if((model_type == "GLMNET") && (m == 3)) {
      
      out <- glmnet(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), 
                    family="multinomial", type.multinomial="grouped", ...)
      
      
      cvout <- NA
      if(GLMNET_RUN_CV) {
        cvout <- cv.glmnet(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), 
                           family="multinomial", type.multinomial="grouped", ...)
        
        cvout <- c("lambda_1se"=cvout$lambda.1se, "index"=which(out$lambda == cvout$lambda.1se))
      }
      
      
      # Store selected variables output
      ##################################
      bnames <- lapply(out$beta, function(x) {
        xmat <- as.matrix(x)
        b <- apply(xmat, 2, function(y) y[y != 0])
        names(b) <- round(out$lambda, 4)
        return(b) })[[1]]
      
      
      # Update selected var counts
      for(i in 1:len(bnames)) {
        if(len(bnames[[i]]) > 0) {
          i_match <- match(names(bnames[[i]]), rownames(var_select_counts))
          var_select_counts[i_match, k] <- var_select_counts[i_match, k] + 1
          var_coeffs_counts[i_match, k] <- var_coeffs_counts[i_match, k] + bnames[[i]]
        }
      }
      
      
      # Predictions and confusion matrices
      #####################################
      out_pred_train <- predict(out, newx=as.matrix(dat_train[,-1]), type="class")
      out_pred_test <- predict(out, newx=as.matrix(dat_test[,-1]), type="class")
      out_confmat_train <- apply(out_pred_train, 2, function(x) list(table(factor(dat_train[,1], levels=0:2), factor(as.num(x), levels=0:2))))
      out_confmat_test <- apply(out_pred_test, 2, function(x) list(table(factor(dat_test[,1], levels=0:2), factor(as.num(x), levels=0:2))))
      
      out_prob_test <- predict(out, newx=as.matrix(dat_test[,-1]), type="response")
      out_prob_test <- apply(out_prob_test, 3, function(x) data.frame("TRUE"=as.num(as.char(dat_test[,1])), "PROB"=x))
      
    }
    
    if((model_type == "GLMNET") && (m == 2)) {
      
      out <- glmnet(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), 
                    family="binomial", ...)
      
      if(GLMNET_RUN_CV) {
        cvout <- cv.glmnet(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), 
                           family="binomial", ...)
        
        cvout <- c("lambda_1se"=cvout$lambda.1se, "index"=which(out$lambda == cvout$lambda.1se))
      }
      
      
      # Store selected variables output
      ##################################
      bnames <- apply(as.matrix(out$beta), 2, function(y) y[y != 0])
      names(bnames) <- round(out$lambda, 4)
      
      
      # Update selected var counts
      for(i in 1:len(bnames)) {
        if(len(bnames[[i]]) > 0) {
          i_match <- match(names(bnames[[i]]), rownames(var_select_counts))
          var_select_counts[i_match, k] <- var_select_counts[i_match, k] + 1
          var_coeffs_counts[i_match, k] <- var_coeffs_counts[i_match, k] + bnames[[i]]
        }
      }
      
      # Predictions and confusion matrices
      #####################################
      out_pred_train <- predict(out, newx=as.matrix(dat_train[,-1]), type="class")
      out_pred_test <- predict(out, newx=as.matrix(dat_test[,-1]), type="class")
      out_confmat_train <- apply(out_pred_train, 2, function(x) list(table(factor(dat_train[,1], levels=0:1), factor(as.num(x), levels=0:1))))
      out_confmat_test <- apply(out_pred_test, 2, function(x) list(table(factor(dat_test[,1], levels=0:1), factor(as.num(x), levels=0:1))))
      
      
      out_prob_test <- predict(out, newx=as.matrix(dat_test[,-1]), type="response")
      out_prob_test <- apply(out_prob_test, 2, function(x) data.frame("TRUE"=as.num(as.char(dat_test[,1])), "PROB"=x))
      
    }
    
    if((model_type == "GLMNET_ORDINAL") && (m == 3)) {
      
      out <- glmnet.cr(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), maxit=250, ...)
      
      if(GLMNET_RUN_CV)
        cvout <- select.glmnet.cr(out)
      
      
      # Store selected variables output
      ##################################
      bnames <- apply(as.matrix(out$beta), 2, function(y) y[y != 0])
      names(bnames) <- round(out$lambda, 4)
      
      
      # Update selected var counts
      for(i in 1:len(bnames)) {
        if(len(bnames[[i]]) > 0) {
          i_match <- na.omit(match(names(bnames[[i]]), rownames(var_select_counts)))
          
          if(len(i_match) > 0) {
            var_select_counts[i_match, k] <- var_select_counts[i_match, k] + 1
            var_coeffs_counts[i_match, k] <- var_coeffs_counts[i_match, k] + bnames[[i]][-attr(i_match, "na.action")]
          }
        }
      }
      
      
      # Predictions and confusion matrices
      #####################################
      out_pred_train <- predict(out, newx=as.matrix(dat_train[,-1]))[[3]]
      out_pred_test <- predict(out, newx=as.matrix(dat_test[,-1]))[[3]]
      out_confmat_train <- apply(out_pred_train, 2, function(x) list(table(factor(dat_train[,1], levels=0:2), factor(as.num(x), levels=0:2))))
      out_confmat_test <- apply(out_pred_test, 2, function(x) list(table(factor(dat_test[,1], levels=0:2), factor(as.num(x), levels=0:2))))
      
      
      out_prob_test <- predict(out, newx=as.matrix(dat_test[,-1]))[[4]]
      out_prob_test <- apply(out_prob_test, 3, function(x) data.frame("TRUE"=as.num(as.char(dat_test[,1])), "PROB"=x))
      
    }
    
    if((model_type == "RF") && (m == 3)) {
      
      out <- randomForest(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), importance=T, ...)
      
      
      cvout <- NULL
      
      # Update variable importance
      var_imp_list[[k]] <- list("RF_var_importance" = out$importance, "RF_var_importanceSD" = out$importanceSD)
      
      
      # Predictions and confusion matrices
      out_pred_train <- predict(out, as.matrix(dat_train[,-1]), type="class")
      out_pred_test <- predict(out, as.matrix(dat_test[,-1]), type="class")
      
      out_confmat_train <- table(factor(dat_train[,1], levels=0:2), factor(out_pred_train, levels=0:2))
      out_confmat_test <- table(factor(dat_test[,1], levels=0:2), factor(out_pred_test, levels=0:2))
      
      out_prob_test <- cb("TRUE"=as.num(as.char(dat_test[,1])), predict(out, as.matrix(dat_test[,-1]), type="prob"))
      
    }
    
    if((model_type == "RF") && (m == 2)) {
      
      out <- randomForest(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), importance=T, ...)
      
      
      cvout <- NULL
      
      # Update variable importance
      var_imp_list[[k]] <- list("RF_var_importance" = out$importance, "RF_var_importanceSD" = out$importanceSD)
      
      
      # Predictions and confusion matrices
      out_pred_train <- predict(out, as.matrix(dat_train[,-1]), type="class")
      out_pred_test <- predict(out, as.matrix(dat_test[,-1]), type="class")
      
      out_confmat_train <- table(factor(dat_train[,1], levels=0:1), factor(out_pred_train, levels=0:1))
      out_confmat_test <- table(factor(dat_test[,1], levels=0:1), factor(out_pred_test, levels=0:1))
      
      out_prob_test <- cb("TRUE"=as.num(as.char(dat_test[,1])), predict(out, as.matrix(dat_test[,-1]), type="prob"))
      
    }
    
    
    # Update output structures
    ###########################
    model_list[[k]] <- list("OUT" = out, "CV_OUT" = cvout)
    res_list[[k]] <- list("CONFMAT_TRAIN" = out_confmat_train, 
                          "CONFMAT_TEST" = out_confmat_test, 
                          "PRED_PROBS_TEST" = out_prob_test)
    
    
    if(PRINT_PROGRESS)
      catProgress(k, CV_K)
    
  }
  
  
  
  if(len(grep("GLM", model_type)) > 0)
    var_imp_list = list("VAR_COUNTS"=var_select_counts, "VAR_COEFF_COUNTS"=var_coeffs_counts)
  
  
  return(list(model_list, res_list, var_imp_list))
  
  
}








runSupervisedOnSplit <- function(dat_test, dat_train,
                                 model_type = c("GLMNET", "GLMNET_ORDINAL", "RF")[1],
                                 TRAIN_RESAMPLE = TRUE,
                                 GLMNET_OUT_DIR = "./cv_glmnet output/",
                                 GLMNET_RUN_CV = TRUE,
                                 ...)
{
  
  
  m <- len(unique(dat_train[,1]))
  
  
  # Remove rows with NA
  ######################
  which_rows_NA <- apply(dat_train, 1, function(x) any(is.na(x)))
  dat_train <- dat_train[!which_rows_NA, ]
  
  which_rows_NA <- apply(dat_test, 1, function(x) any(is.na(x)))
  dat_test <- dat_test[!which_rows_NA, ]
  
  
  # Form vector of variable counts
  ##################################
  var_select_counts <- num(ncol(dat_train) - 1)
  names(var_select_counts) <- names(dat_train)[-1]
  
  

  model_list <- vector("list", 1)
  res_list <- vector("list", 1)
  predprobs_list <- vector("list", 1)
  glmnet_cv_vars <- char(1)
  
  if(TRAIN_RESAMPLE) {
    
    dat_train <- balance_data_with_SMOTE(dat_train)
    dat_train[,1] <- as.numeric(dat_train[,1])
    
  }
  
  # Ensure randomised observations
  dat_train <- dat_train[sample(1:nrow(dat_train)),]
  dat_train <- apply(dat_train, 2, as.num)
  
  
  # Fit models
  ########################
  if((model_type == "GLMNET") && (m == 3)) {
    
    out <- glmnet(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), 
                  family="multinomial", type.multinomial="grouped", ...)
    
    
    cvout <- NA
    if(GLMNET_RUN_CV)
      cvout <- cv.glmnet(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), 
                         family="multinomial", type.multinomial="grouped", ...)
    
    
    # Store cv.glmnet output
    if(!is.null(GLMNET_OUT_DIR)) {
      fldnm <- ifelse(k < 10, paste0("fold0", k), paste0("fold", k))
      
      if(GLMNET_RUN_CV) {
        w() ; plot(cvout) ; abline(v=log(cvout$lambda.1se), lty=1, lwd=2, col="blue")
        legend("bottomleft", leg=paste("lambda_min_1se =", round(cvout$lambda.1se, 4)), cex=0.8)
        savePlot(paste0(GLMNET_OUT_DIR, "fig_", fldnm, "_cvglmnet.png"), "png")
        graphics.off()
      }
      
      
      bnames <- lapply(out$beta, function(x) {
        b <- apply(as.matrix(x), 2, function(y) names(y)[y != 0])
        names(b) <- round(out$lambda, 4)
        return(b) })[[1]]
      
      
      # Update selected var counts
      for(i in 1:len(bnames)) {
        if(len(bnames[[i]]) > 0) {
          i_match <- match(bnames[[i]], names(var_select_counts))
          var_select_counts[i_match] <- var_select_counts[i_match] + 1
        }
      }
      
      
      if(GLMNET_RUN_CV) {
        ilam <- which(cvout$glmnet.fit$lambda == cvout$lambda.1se)
        bnames_cv <- bnames[ilam]
      }
      
      
      bnames <- bnames[unl(lapply(bnames, len)) !=0]
      bnames <- bnames[1:which.max(unl(lapply(bnames, len)))]
      if(GLMNET_RUN_CV) bnames <- c(bnames_cv, bnames)
      bnames <- lapply(bnames, function(x) format(x, width=max(nchar(x))))
      bnames <- unl(lapply(bnames, paste, collapse="\t\t\t"))
      bnames <- paste(paste0("lambda=", names(bnames)), bnames, sep="\n")
      
      txtf <- file(paste0(GLMNET_OUT_DIR, "varnames_all_lambda_", fldnm, ".txt"), open="w")
      writeLines(bnames[-1], con=txtf, sep="\n\n")
      close(txtf)
      
      if(GLMNET_RUN_CV) glmnet_cv_vars[k] <- bnames[1]
    }
    
    # Predictions and confusion matrices
    out_pred_train <- predict(out, newx=as.matrix(dat_train[,-1]), type="class")
    out_pred_test <- predict(out, newx=as.matrix(dat_test[,-1]), type="class")
    out_confmat_train <- apply(out_pred_train, 2, function(x) list(table(factor(dat_train[,1], levels=0:2), factor(as.num(x), levels=0:2))))
    out_confmat_test <- apply(out_pred_test, 2, function(x) list(table(factor(dat_test[,1], levels=0:2), factor(as.num(x), levels=0:2))))
    
    cvout_confmat_train <- cvout_confmat_test <- out_predprobs_test <- NA
    if(GLMNET_RUN_CV) {
      cvout_pred_train <- predict(cvout, newx=as.matrix(dat_train[,-1]), type="class")
      cvout_pred_test <- predict(cvout, newx=as.matrix(dat_test[,-1]), type="class")
      cvout_confmat_train <- table(factor(dat_train[,1], levels=0:2), factor(as.num(cvout_pred_train[,1]), levels=0:2))
      cvout_confmat_test <- table(factor(dat_test[,1], levels=0:2), factor(as.num(cvout_pred_test[,1]), levels=0:2))
      
      out_predprobs_test <- predict(cvout, newx=as.matrix(dat_test[,-1]), type="response")
    }
    
    
    # ROC curve AUC analysis
    # out_predprob_train <- predict(out, newx=as.matrix(dat_train[,-1]), type="response")
    # aa <- apply(out_predprob_train, 3, function(x) multiclass.roc(as.factor(dat_train[,1]), x))
    # x <- out_predprob_train[,,2]
    # multiclass.roc(as.factor(dat_train[,1]), x)
    
  }
  
  if((model_type == "GLMNET") && (m == 2)) {
    
    out <- glmnet(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), 
                  family="binomial",  ...)
    
    cvout <- cv.glmnet(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), 
                       family="binomial",  ...)
    
    
    # dat_train <- dat_train_b[, 1:18]
    # glmnet(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), 
    #        family="multinomial", type.multinomial="grouped")
    
    
    
    # Store cv.glmnet output
    if(!is.null(GLMNET_OUT_DIR)) {
      fldnm <- ifelse(k < 10, paste0("fold0", k), paste0("fold", k))
      
      w() ; plot(cvout) ; abline(v=log(cvout$lambda.1se), lty=1, lwd=2, col="blue")
      legend("bottomleft", leg=paste("lambda_min_1se =", round(cvout$lambda.1se, 4)), cex=0.8)
      savePlot(paste0(GLMNET_OUT_DIR, "fig_", fldnm, "_cvglmnet.png"), "png")
      graphics.off()
      
      ilam <- which(cvout$glmnet.fit$lambda == cvout$lambda.1se)
      
      bnames <- apply(as.matrix(cvout$glmnet.fit$beta), 2, function(y) names(y)[y != 0])
      names(bnames) <- round(cvout$glmnet.fit$lambda, 4)
      
      bnames_cv <- bnames[ilam]
      
      bnames <- bnames[unl(lapply(bnames, len)) !=0]
      bnames <- bnames[1:which.max(unl(lapply(bnames, len)))]
      bnames <- c(bnames_cv, bnames)
      bnames <- lapply(bnames, function(x) format(x, width=max(nchar(x))))
      bnames <- unl(lapply(bnames, paste, collapse="\t\t\t"))
      bnames <- paste(paste0("lambda=", names(bnames)), bnames, sep="\n")
      
      txtf <- file(paste0(GLMNET_OUT_DIR, "varnames_all_lambda_", fldnm, ".txt"), open="w")
      writeLines(bnames[-1], con=txtf, sep="\n\n")
      close(txtf)
      
      glmnet_cv_vars[k] <- bnames[1]
    }
    
    # Predictions and confusion matrices
    out_pred_train <- predict(out, newx=as.matrix(dat_train[,-1]), type="class")
    out_pred_test <- predict(out, newx=as.matrix(dat_test[,-1]), type="class")
    out_confmat_train <- apply(out_pred_train, 2, function(x) list(table(factor(dat_train[,1], levels=0:2), factor(as.num(x), levels=0:2))))
    out_confmat_test <- apply(out_pred_test, 2, function(x) list(table(factor(dat_test[,1], levels=0:2), factor(as.num(x), levels=0:2))))
    
    cvout_pred_train <- predict(cvout, newx=as.matrix(dat_train[,-1]), type="class")
    cvout_pred_test <- predict(cvout, newx=as.matrix(dat_test[,-1]), type="class")
    cvout_confmat_train <- table(factor(dat_train[,1], levels=0:2), factor(as.num(cvout_pred_train[,1]), levels=0:2))
    cvout_confmat_test <- table(factor(dat_test[,1], levels=0:2), factor(as.num(cvout_pred_test[,1]), levels=0:2))
    
    out_predprobs_test <- predict(out, newx=as.matrix(dat_test[,-1]), type="response")
    
  }
  
  if((model_type == "GLMNET_ORDINAL") && (m == 3)) {
    
    out <- glmnet.cr(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), ...)
    
    # select.glmnet.cr(out)
    
    
    
    # Predictions and confusion matrices
    out_pred_train <- predict(out, newx=as.matrix(dat_train[,-1]))[[3]]
    out_pred_test <- predict(out, newx=as.matrix(dat_test[,-1]))[[3]]
    out_confmat_train <- apply(out_pred_train, 2, function(x) list(table(factor(dat_train[,1], levels=0:2), factor(as.num(x), levels=0:2))))
    out_confmat_test <- apply(out_pred_test, 2, function(x) list(table(factor(dat_test[,1], levels=0:2), factor(as.num(x), levels=0:2))))
    
    
    out_predprobs_test <- predict(out, newx=as.matrix(dat_test[,-1]))[[4]]
  }
  
  if((model_type == "RF") && (m == 3)) {
    
    out <- randomForest(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), ...)
    
    
    cvout <- NULL
    
    
    # Predictions and confusion matrices
    out_pred_train <- predict(out, as.matrix(dat_train[,-1]), type="class")
    out_pred_test <- predict(out, as.matrix(dat_test[,-1]), type="class")
    
    out_confmat_train <- table(factor(dat_train[,1], levels=0:2), factor(out_pred_train, levels=0:2))
    out_confmat_test <- table(factor(dat_test[,1], levels=0:2), factor(out_pred_test, levels=0:2))
    
    out_predprobs_test <- predict(out, as.matrix(dat_test[,-1]), type="prob")
    
  }
  
  if((model_type == "RF") && (m == 2)) {
    
    out <- randomForest(as.matrix(dat_train[,-1]), as.factor(dat_train[,1]), ...)
    cvout <- NULL
    
    
    # Predictions and confusion matrices
    out_pred_train <- predict(out, as.matrix(dat_train[,-1]), type="class")
    out_pred_test <- predict(out, as.matrix(dat_test[,-1]), type="class")
    
    out_confmat_train <- table(factor(dat_train[,1], levels=0:1), factor(out_pred_train, levels=0:1))
    out_confmat_test <- table(factor(dat_test[,1], levels=0:1), factor(out_pred_test, levels=0:1))
    
    out_predprobs_test <- cb("TRUE"=dat_test[,1], "PROBS"=predict(out, as.matrix(dat_test[,-1]), type="prob"))
     
  }
  
  
  
  
  if(model_type == "GLMNET") {
    model_list[[1]] <- list("OUT"=out, "CV_OUT"=cvout)
    res_list[[1]] <- list("OUT_CONFMAT_TRAIN"=out_confmat_train, "OUT_CONFMAT_TEST"=out_confmat_test,
                          "CV_OUT_CONFMAT_TRAIN"=cvout_confmat_train, "CV_OUT_CONFMAT_TEST"=cvout_confmat_test)
  }
  else if((model_type == "GLMNET_ORDINAL") | (model_type == "RF")) {
    model_list[[1]] <- out
    res_list[[1]] <- list("OUT_CONFMAT_TRAIN"=out_confmat_train, "OUT_CONFMAT_TEST"=out_confmat_test)
  }
  predprobs_list[[1]] <- out_predprobs_test
  
  
  
  # Write all selected variables in cv.glmnet fit for each fold to file
  if(!is.null(GLMNET_OUT_DIR)) {
    txtf <- file(paste0(GLMNET_OUT_DIR, "varnames_cv_lambda.txt"), open="w")
    writeLines(glmnet_cv_vars, con=txtf, sep="\n\n")
    close(txtf)
  }
  
  
  return(list(model_list, res_list, predprobs_list, var_select_counts))
  
  
}




runSupervisedCV_repeats <- function(n = 10, dat,
                                    model_type = c("GLMNET")[1],
                                    CV_K = 5,
                                    CV_SEEDS = floor(runif(n, 0, 1e6)),
                                    CV_STRATIFY_FOLDS = TRUE,
                                    CV_TRAIN_RESAMPLE = TRUE,
                                    PRINT_PROGRESS = TRUE,
                                    GLMNET_OUT_DIRS = NULL,
                                    ...)
{
  out_list_models <- NULL
  out_list_results <- NULL
  out_list_varsel <- NULL
  for(i in 1:n) {
    out <- runSupervisedCV(dat=dat, model_type=model_type, CV_K=CV_K,
                           CV_SEED=CV_SEEDS[i], CV_STRATIFY_FOLDS = TRUE, CV_TRAIN_RESAMPLE = TRUE,
                           GLMNET_OUT_DIR=GLMNET_OUT_DIRS[i], PRINT_PROGRESS=PRINT_PROGRESS, ...)
    
    out_list_models <- c(out_list_models, out[[1]])
    out_list_results <- c(out_list_results, out[[2]])
    out_list_varsel <- c(out_list_varsel, out[[3]])
    
    if(model_type %in% c("GLMNET", "GLMNET_ORDINAL"))
      names(out_list_varsel)[len(out_list_varsel) + c(-1, 0)] <- paste(paste0("REP", i), names(out_list_varsel)[len(out_list_varsel) + c(-1, 0)], sep=".")
    
    if(PRINT_PROGRESS)
      cat("OVERALL PROGRESS: ")
    catProgress(i, n)
  }
  
  return(list(out_list_models, out_list_results, out_list_varsel))
}






resultAggregator <- function(cv_list, n_lam_breaks=76, lam_vals_for_breaks)
{
  
  # Extract and organise lambdas
  lam_vals <- lapply(cv_list[[1]], function(x) log(x$OUT$lambda))
  
  if(missing(lam_vals_for_breaks))
    lam_vals_for_breaks <- unl(lam_vals)
  
  lam_breaks <- seq(1.005*min(lam_vals_for_breaks), 1.005*max(lam_vals_for_breaks), len=n_lam_breaks)
  lam_cat <- lapply(lam_vals, cut, lam_breaks)
  lam_lvls <- levels(lam_cat[[1]])
  lam_midpoints <- lam_breaks[-len(lam_breaks)] + diff(lam_breaks)/2
  
  m <- nrow(cv_list[[2]][[1]][[1]][[1]][[1]])
  
  # Perform summary statistic calculations on each fold
  res_list <- lapply(cv_list[[2]], function(x) {
    
    kappa_train <- unl(lapply(x[[1]], function(y) calc_cohen_kappa(y[[1]])))
    kappa_test <- unl(lapply(x[[2]], function(y) calc_cohen_kappa(y[[1]])))
    
    acc_train <- unl(lapply(x[[1]], function(y) sum(diag(y[[1]]))/sum(y[[1]]) ))
    acc_test <- unl(lapply(x[[2]], function(y) sum(diag(y[[1]]))/sum(y[[1]]) ))
    
    recall_train <- matrix(unl(lapply(x[[1]], function(y) diag(y[[1]])/rowSums(y[[1]]) )), ncol=m, byrow=T)
    recall_test <- matrix(unl(lapply(x[[2]], function(y) diag(y[[1]])/rowSums(y[[1]]) )), ncol=m, byrow=T)
    
    if(m == 3) {
      aucs <- lapply(x[[3]], function(y) rowMeans(colAUC(as.matrix(y[,-1]), as.factor(y[,1]))))
      aucs <- t(as.data.frame(aucs))
      aucs <- cb(aucs, "AUC_mean"=apply(aucs, 1, mean))
      
      res_mat <- cb(kappa_train, acc_train, recall_train, kappa_test, acc_test, recall_test, aucs)
      colnames(res_mat) <- c(paste0(rep(c("train_", "test_"), each=5), c("kappa", "acc", "recall.0", "recall.1", "recall.2")),
                             paste0("AUC_", c("0v1", "0v2", "1v2", "mean")))
    }
    if(m == 2) {
      aucs <- lapply(x[[3]], function(y) calc_auc_ROCR(y[,2], y[,1]))
      
      res_mat <- cb(kappa_train, acc_train, recall_train, kappa_test, acc_test, recall_test, aucs)
      colnames(res_mat) <- c(paste0(rep(c("train_", "test_"), each=4), c("kappa", "acc", "recall.0", "recall.1")), "AUC")
    }
    
    return(res_mat)
    
  })
  
  # Link lambdas and categories to result matrices
  for(i in 1:len(res_list)) {
    res_list[[i]] <- cb("loglambda"=lam_vals[[i]], res_list[[i]])
    rownames(res_list[[i]]) <- lam_cat[[i]]
  }
  
  # Group statistics according to lambda
  res_list_grp <- vector("list", len(lam_lvls))
  names(res_list_grp)  <- as.char(round(lam_midpoints, 4))
  for(i in 1:len(lam_lvls)) {
    
    ccmat <- NULL
    which_lvl <- lapply(lam_cat, function(x) which(x == lam_lvls[i]))
    for(j in 1:len(res_list))
      ccmat <- rbind(ccmat, res_list[[j]][which_lvl[[j]], , drop=F])
    
    res_list_grp[[i]] <- ccmat
    
  }
  
  return(res_list_grp)
  
}







resultAggregatorNewMethod <- function(cv_list, cv_k=5)
{
  

  lam_vals <- lapply(cv_list[[1]], function(x) log(x$OUT$lambda))
  lam_breaks <- seq(1.005*min(unl(lam_vals)), 1.005*max(unl(lam_vals)), len=99)
  lam_cat <- lapply(lam_vals, cut, lam_breaks)
  lam_lvls <- levels(lam_cat[[1]])
  lam_midpoints <- lam_breaks[-len(lam_breaks)] + diff(lam_breaks)/2
  
  m <- nrow(cv_list[[2]][[1]][[1]][[1]][[1]])
  
  cv_seq <- c(seq(1, len(cv_list[[2]]), cv_k), (1 + len(cv_list[[2]])))
  agg_list <- vector("list", len(cv_seq)-1)
  for(i in 2:len(cv_seq)) {
  
    cv_set <- cv_list[[2]][cv_seq[i-1]:(cv_seq[i]-1)]
    cv_cat <- lam_cat[cv_seq[i-1]:(cv_seq[i]-1)]
    res_agg <- resultAggregator(list(cv_list[[1]][cv_seq[i-1]:(cv_seq[i]-1)], cv_set), 99, unl(lam_vals))
    
    ccmat_by_lam <- vector("list", len(lam_lvls))
    ccmat_by_lam <- lapply(ccmat_by_lam, function(x) matrix(0, m, m))
    
    for(j in 1:len(lam_lvls)) {
      which_lvl <- lapply(cv_cat, function(x) which(x == lam_lvls[j]))
      for(k in 1:len(cv_set)) {
        if(len(which_lvl[[k]]) == 0)
          next
        
        ccmats <- cv_set[[k]][[2]][ which_lvl[[k]] ]
        
        for(l in 1:len(ccmats))
          ccmat_by_lam[[j]] <- ccmat_by_lam[[j]] + as.matrix(ccmats[[l]][[1]])
      
      }
    }
    
    ccmat_stats <- matrix(unl(lapply(ccmat_by_lam, function(x) 
      c(calc_cohen_kappa(x), sum(diag(x))/sum(x)))), ncol=2, byrow=T)
    
    if(m == 3)
      auc_stats <- matrix(unl(lapply(res_agg, function(x) colMeans(x[,grep("AUC", colnames(x)), drop=F]))), ncol=4, byrow=T)
    if(m == 2)
      auc_stats <- unl(lapply(res_agg, function(x) mean(unl(x[,grep("AUC", colnames(x))])) ))
    
    agg_list[[i-1]] <- cb(ccmat_stats, auc_stats)
    rownames(agg_list[[i-1]]) <- names(res_agg)
    if(m == 3) colnames(agg_list[[i-1]]) <- c("kappa", "acc", "auc_0v1", "auc_0v2", "auc_1v2", "auc_mean")
    if(m == 2) colnames(agg_list[[i-1]]) <- c("kappa", "acc", "auc")
    
  }
  
  agg_stats <- lapply(as.list(1:nrow(agg_list[[1]])), function(z) 
    apply(matrix(unl(lapply(agg_list, function(x) x[z,])), ncol=(3 + (m-2)*3), byrow=T), 2, function(y) c(mean(y, na.rm=T), sd(y, na.rm=T))) )
  
  
  return(list("aggregated_all" = agg_list, "aggregated_stats"=agg_stats))
}







evaluateLambdaResults <- function(cv_list, model_type = c("GLMNET", "GLMNET_ORD")[1],
                                  ylim_kappa = c(-0.1, 0.65), 
                                  ylim_acc = c(0, 1),
                                  class_names = c("s-pref", "pref", "n-pref"),
                                  output_dir = "./cv_glmnet_lambda_vs_stats/",
                                  output_path_delim = "",
                                  plots_close = FALSE)
{
  
  m <- len(class_names)
  
  if(model_type == "GLMNET") {
    lam_vals <- lapply(cv_list[[1]], function(x) log(x$OUT$lambda))
    lam_vals_autoselect <- unl(lapply(cv_list[[1]], function(x) log(x$CV_OUT$lambda.1se)))
    dfs <- lapply(cv_list[[1]], function(x) x$OUT$df)
    
    out_res <- lapply(resultAggregator(cv_list[[2]]), function(x) x$OUT)
    
    lam_breaks <- seq(1.005*min(unl(lam_vals)), 1.005*max(unl(lam_vals)), len=76)
    lam_cat <- lapply(lam_vals, cut, lam_breaks)
    lam_lvls <- levels(lam_cat[[1]])
    
    # for(i in 1:len(out_res)) rownames(out_res[[i]]) <- lam_cat[[i]]
    
    res_list <- vector("list", len(lam_lvls))
    names_res_list  <- lam_lvls
    df_list <- vector("list", len(lam_lvls))
    for(i in 1:len(lam_lvls)) {
      
      ccmat <- NULL
      which_lvl <- lapply(lam_cat, function(x) which(x == lam_lvls[i]))
      for(j in 1:len(out_res))
        ccmat <- rbind(ccmat, out_res[[j]][which_lvl[[j]], , drop=F])
      
      
      dfvec <- NULL
      for(j in 1:len(dfs))
        dfvec <- c(dfvec, dfs[[j]][which_lvl[[j]]])
      df_list[[i]] <- round(median(dfvec), 0)
      
      colnames(ccmat) <- c("OUT_TRAIN_KAPPA", "OUT_TRAIN_ACC",   "OUT_TEST_KAPPA", "OUT_TEST_ACC",
                           paste0("OUT_TRAIN_RECALL.", 0:2), paste0("OUT_TEST_RECALL.", 0:2))
      
      
      if(m == 2) ccmat <- ccmat[, -c(7, 10), drop=F]
      res_list[[i]] <- ccmat
      
    }
  }
  
  if(model_type == "GLMNET_ORD") {
    lam_vals <- lapply(cv_list[[1]], function(x) log(x$lambda))
    dfs <- lapply(cv_list[[1]], function(x) x$df)
    
    lam_vals_autoselect <- unl(lapply(cv_list[[1]], function(x) log(x$lambda)[select.glmnet.cr(x)]))
    
    out_res <- resultAggregator(cv_list[[2]], "GLMNET_ORD")
    
    lam_breaks <- seq(1.005*min(unl(lam_vals)), 1.005*max(unl(lam_vals)), len=76)
    lam_cat <- lapply(lam_vals, cut, lam_breaks)
    lam_lvls <- levels(lam_cat[[1]])
    

    res_list <- vector("list", len(lam_lvls))
    names_res_list  <- lam_lvls
    df_list <- vector("list", len(lam_lvls))
    for(i in 1:len(lam_lvls)) {
      
      ccmat <- NULL
      which_lvl <- lapply(lam_cat, function(x) which(x == lam_lvls[i]))
      for(j in 1:len(out_res))
        ccmat <- rbind(ccmat, out_res[[j]][which_lvl[[j]], , drop=F])
      
      
      dfvec <- NULL
      for(j in 1:len(dfs))
        dfvec <- c(dfvec, dfs[[j]][which_lvl[[j]]])
      df_list[[i]] <- round(median(dfvec), 0)
      
      colnames(ccmat) <- c("OUT_TRAIN_KAPPA", "OUT_TRAIN_ACC",   "OUT_TEST_KAPPA", "OUT_TEST_ACC",
                           paste0("OUT_TRAIN_RECALL.", 0:2), paste0("OUT_TEST_RECALL.", 0:2))
      
      res_list[[i]] <- ccmat
      
    }
  }
  

  
  makePlot <- function(res, plot_title="Result", ylim=c(0, 1)) {
    lam_midpoints <- lam_breaks[-len(lam_breaks)] + diff(lam_breaks)/2
    
    plot_g(lam_midpoints, res[,1], type="n", ylim=ylim,
           ylab=plot_title, xlab="log lambda")
    
    
    ups_seq <- seq(lam_midpoints[1], tail(lam_midpoints, 1), len=5*len(lam_midpoints))
    # ups_seq <- ups_seq + diff(ups_seq[c(1, 5)])
    res_ups <- cb(predict(loess(res[,2] ~ lam_midpoints, span=0.1), ups_seq),
                  predict(loess(res[,4] ~ lam_midpoints, span=0.1), ups_seq))
    
    for(i in 1:len(ups_seq))
      lines(cb(ups_seq[i], res_ups[i,]), col="pink", lwd=1)
    
    lines(ups_seq, res_ups[,1], col="deeppink")
    lines(ups_seq, res_ups[,2], col="deeppink")
    
    lines(lam_midpoints, res[,1], lwd=3, col="blue")
    abline(v=c(range(lam_vals_autoselect), mean(lam_vals_autoselect)), col="seagreen", lwd=c(1,1,2))
    title(main = paste0(plot_title, " against shrinkage parameter (averaged over ", len(out_res), "-fold CV)"))
    
    # axis(1, at=lam_midpoints[seq(1, len(lam_midpoints), 2)],
         # labels=unl(df_list)[seq(1, len(df_list), 2)], outer=T)
    
    mtext(side=1, text=unl(df_list)[seq(1, len(df_list), 2)], 
          at=lam_midpoints[seq(1, len(lam_midpoints), 2)], cex=0.75)
    
  }
  
  
  # res_kappa_test <- matrix(unl(lapply(res_list, function(x) {
  #   y <- x[,grep("OUT_TEST_KAPPA", colnames(x))]
  #   yq <- quantile(y, probs=c(0.16, 0.50, 0.84))
  #   return(c(mean(y), yq))
  # })), ncol=4, byrow=T)
  # makePlot(res_kappa_test, "Test Kappa")
  
  res_for_plot <- lapply(as.list(colnames(res_list[[2]])), function(y) matrix(unl(lapply(res_list, function(x) {
    y <- x[,grep(y, colnames(x))]
    yq <- quantile(y, probs=c(0.16, 0.50, 0.84), na.rm=T)
    return(c(mean(y), yq))
  })), ncol=4, byrow=T))
  
  
  if(!is.null(output_dir)) {
    png(paste0(output_dir, "fig_lam_vs_kappa_acc", output_path_delim, ".png"), width=1600, height=900)
    par(mfrow=c(2,2), mar=c(4,4,1.5,1))
    makePlot(res_for_plot[[1]], "Train Kappa", ylim=ylim_kappa)
    makePlot(res_for_plot[[3]], "Test Kappa", ylim=ylim_kappa)
    makePlot(res_for_plot[[2]], "Train accuracy", ylim=ylim_acc)
    makePlot(res_for_plot[[4]], "Test accuracy", ylim=ylim_acc)
    dev.off()
    
    png(paste0(output_dir, "fig_lam_vs_recall", output_path_delim, ".png"), width=1600, height=900)
    par(mfrow=c(m,2), mar=c(4,4,1.5,1))
    if(m == 3) {
      makePlot(res_for_plot[[5]], "Train recall (s-pref)")
      makePlot(res_for_plot[[8]], "Test recall (s-pref)")
      makePlot(res_for_plot[[6]], "Train recall (pref)")
      makePlot(res_for_plot[[9]], "Test recall (pref)")
      makePlot(res_for_plot[[7]], "Train recall (n-pref)")
      makePlot(res_for_plot[[10]], "Test recall (n-pref)")
    }
    if(m == 2){
      makePlot(res_for_plot[[5]], paste0("Train recall (", class_names[1], ")"))
      makePlot(res_for_plot[[7]], paste0("Test recall (", class_names[1], ")"))
      makePlot(res_for_plot[[6]], paste0("Train recall (", class_names[2], ")"))
      makePlot(res_for_plot[[8]], paste0("Test recall (", class_names[2], ")"))
    }
    dev.off()
  }
  
  if(!plots_close) {
    w() ; par(mfrow=c(2,2), mar=c(4,4,1.5,1))
    makePlot(res_for_plot[[1]], "Train Kappa", ylim=ylim_kappa)
    makePlot(res_for_plot[[3]], "Test Kappa", ylim=ylim_kappa)
    makePlot(res_for_plot[[2]], "Train accuracy", ylim=ylim_acc)
    makePlot(res_for_plot[[4]], "Test accuracy", ylim=ylim_acc)
    
    w() ; par(mfrow=c(m,2), mar=c(4,4,1.5,1))
    if(m == 3) {
      makePlot(res_for_plot[[5]], "Train recall (s-pref)")
      makePlot(res_for_plot[[8]], "Test recall (s-pref)")
      makePlot(res_for_plot[[6]], "Train recall (pref)")
      makePlot(res_for_plot[[9]], "Test recall (pref)")
      makePlot(res_for_plot[[7]], "Train recall (n-pref)")
      makePlot(res_for_plot[[10]], "Test recall (n-pref)")
    }
    if(m == 2){
      makePlot(res_for_plot[[5]], paste0("Train recall (", class_names[1], ")"))
      makePlot(res_for_plot[[7]], paste0("Test recall (", class_names[1], ")"))
      makePlot(res_for_plot[[6]], paste0("Train recall (", class_names[2], ")"))
      makePlot(res_for_plot[[8]], paste0("Test recall (", class_names[2], ")"))
    }
  }

  
}



plotAggResults <- function(res_agg, 
                           ylim_kappa = c(0.1, 0.5),
                           ylim_acc = c(0.3, 0.8),
                           ylim_auc = c(0.5, 0.9),
                           output_dir = "./cv_glmnet_lambda_vs_stats/", output_path_delim = "")
{
  
  m <- 3
  if(ncol(res_agg[[1]]) == 10) m <- 2
  
  makePlot <- function(res, plot_title="Result", ylim=c(0, 1), nfolds=50) {
    lam_midpoints <- as.num(rownames(res))
    
    # w()
    plot_g(lam_midpoints, res[,1], type="n", ylim=ylim,
           ylab=plot_title, xlab="log lambda")
    
    
    ups_seq <- seq(lam_midpoints[1], tail(lam_midpoints, 1), len=5*len(lam_midpoints))
    res_ups <- cb(predict(loess(res[,2] ~ lam_midpoints, span=0.1), ups_seq),
                  predict(loess(res[,3] ~ lam_midpoints, span=0.1), ups_seq))
    
    for(i in 1:len(ups_seq))
      lines(cb(ups_seq[i], res_ups[i,]), col="pink", lwd=1)
    
    lines(ups_seq, res_ups[,1], col="deeppink")
    lines(ups_seq, res_ups[,2], col="deeppink")
    
    lines(lam_midpoints, res[,1], lwd=3, col="mediumseagreen")
    title(main = paste0(plot_title, " against shrinkage parameter (averaged over ", nfolds, "-fold CV)"))
    
  }
  
  
  
  res_for_plot <- lapply(res_agg, function(x) {
    if(m == 3)
      x <- x[, match(c("train_kappa", "test_kappa", "test_acc", 
                       "test_recall.0", "test_recall.1", "test_recall.2",
                       "AUC_0v1", "AUC_0v2", "AUC_1v2", "AUC_mean"), colnames(x))]
    
    if(m == 2)
      x <- x[, match(c("train_kappa", "test_kappa", "test_acc", 
                       "test_recall.0", "test_recall.1", "AUC"), colnames(x))]
    
    apply(x, 2, function(y){ z <- unl(y) ; z})
    
    return(t(apply(x, 2, function(y){ z <- unl(y) ; return(c(mean(z), mean(z) + c(-1, 1)*sd(z) )) })))
  })
  
  
  res_for_plot_bylam <- vector("list", nrow(res_for_plot[[1]]))
  names(res_for_plot_bylam) <- rownames(res_for_plot[[1]])
  for(i in 1:len(res_for_plot_bylam))
    res_for_plot_bylam[[i]] <- matrix(unl(lapply(res_for_plot, function(x) x[i,])), ncol=3, byrow=T, dimnames=list(names(res_agg), NULL))
  
  
  if(!is.null(output_dir)) {
    png(paste0(output_dir, "fig_lam_vs_kappa_acc", output_path_delim, ".png"), width=1600, height=900)
    par(mfrow=c(2,2), mar=c(4,4,1.5,1))
    makePlot(res_for_plot_bylam[[1]], "Train Kappa", ylim=ylim_kappa)
    makePlot(res_for_plot_bylam[[2]], "Test Kappa", ylim=ylim_kappa)
    makePlot(res_for_plot_bylam[[3]], "Test accuracy", ylim=ylim_acc)
    makePlot(res_for_plot_bylam[[10 - ifelse(m == 2, 4, 0)]], "Test AUC (mean)", ylim=ylim_auc)
    dev.off()
    
    png(paste0(output_dir, "fig_lam_vs_recall", output_path_delim, ".png"), width=1600, height=900)
    par(mfrow=c(m,2), mar=c(4,4,1.5,1))
    if(m == 3) {
      makePlot(res_for_plot_bylam[[4]], "Test recall (s-pref)")
      makePlot(res_for_plot_bylam[[7]], "AUC (0v1)", ylim=ylim_auc)
      makePlot(res_for_plot_bylam[[5]], "Test recall (pref)")
      makePlot(res_for_plot_bylam[[8]], "AUC (0v2)", ylim=ylim_auc)
      makePlot(res_for_plot_bylam[[6]], "Test recall (n-pref)")
      makePlot(res_for_plot_bylam[[9]], "AUC (1v2)", ylim=ylim_auc)
    }
    # if(m == 2){
    #   makePlot(res_for_plot[[5]], paste0("Train recall (", class_names[1], ")"))
    #   makePlot(res_for_plot[[7]], paste0("Test recall (", class_names[1], ")"))
    #   makePlot(res_for_plot[[6]], paste0("Train recall (", class_names[2], ")"))
    #   makePlot(res_for_plot[[8]], paste0("Test recall (", class_names[2], ")"))
    # }
    dev.off()
  }
  
  return(res_for_plot_bylam)
  
}




























# if(!is.null(GLMNET_OUT_DIR)) {
#   fldnm <- ifelse(k < 10, paste0("fold0", k), paste0("fold", k))
#   
#   if(GLMNET_RUN_CV) {
#     w() ; plot(cvout) ; abline(v=log(cvout$lambda.1se), lty=1, lwd=2, col="blue")
#     legend("bottomleft", leg=paste("lambda_min_1se =", round(cvout$lambda.1se, 4)), cex=0.8)
#     savePlot(paste0(GLMNET_OUT_DIR, "fig_", fldnm, "_cvglmnet.png"), "png")
#     graphics.off()
#   }
#   
#   
#   bnames <- lapply(out$beta, function(x) {
#     b <- apply(as.matrix(x), 2, function(y) names(y)[y != 0])
#     names(b) <- round(out$lambda, 4)
#     return(b) })[[1]]
#   
#   
#   # Update selected var counts
#   for(i in 1:len(bnames)) {
#     if(len(bnames[[i]]) > 0) {
#       i_match <- match(bnames[[i]], names(var_select_counts))
#       var_select_counts[i_match] <- var_select_counts[i_match] + 1
#     }
#   }
#   
#   
#   if(GLMNET_RUN_CV) {
#     ilam <- which(cvout$glmnet.fit$lambda == cvout$lambda.1se)
#     bnames_cv <- bnames[ilam]
#   }
#   
#   
#   bnames <- bnames[unl(lapply(bnames, len)) !=0]
#   bnames <- bnames[1:which.max(unl(lapply(bnames, len)))]
#   if(GLMNET_RUN_CV) bnames <- c(bnames_cv, bnames)
#   bnames <- lapply(bnames, function(x) format(x, width=max(nchar(x))))
#   bnames <- unl(lapply(bnames, paste, collapse="\t\t\t"))
#   bnames <- paste(paste0("lambda=", names(bnames)), bnames, sep="\n")
#   
#   txtf <- file(paste0(GLMNET_OUT_DIR, "varnames_all_lambda_", fldnm, ".txt"), open="w")
#   writeLines(bnames[-1], con=txtf, sep="\n\n")
#   close(txtf)
#   
#   if(GLMNET_RUN_CV) glmnet_cv_vars[k] <- bnames[1]
# }