
library(ggplot2)

datlist_all <- getDataFromDir("~/HealthQ/RGA/Dirk modelling results/", dataE=F)


############################################################
####  PRINT KAPPA AND ACCURACY FROM CONFUSION MATRICES  ####
############################################################


# datlist <- datlist_conf[grep("m6", datlist_conf)]
datlist <- datlist_E[grep("conf_matrix", datlist_E)]
res_list <- vector("list", len(datlist))
for(i in 1:len(datlist)) {
  
  dat <- scan(datlist[i], what="character", sep=",")
  cm <- matrix(as.num(dat[c(6:8, 10:12, 14:16)]), 3, 3)
  cm01v2 <- matrix(c(sum(cm[-3,-3]), sum(cm[3,-3]), sum(cm[-3,3]), cm[3,3]), 2, 2)
  cm0v12 <- matrix(c(cm[1,1], sum(cm[-1,1]), sum(cm[1,-1]), sum(cm[-1,-1])), 2, 2)
  
  print(sum(cm0v12))
  
  # cat(i, ":\n")
  # print(cm)
  # print(cm0v12)
  # print(cm01v2)
  
  res <- cb("kap" = c(calc_cohen_kappa(cm), calc_cohen_kappa(cm0v12), calc_cohen_kappa(cm01v2)),
            "acc" = c(sum(diag(cm))/sum(cm), sum(diag(cm0v12))/sum(cm0v12), sum(diag(cm01v2))/sum(cm01v2)))
  
  res_list[[i]] <- res  
  
}

for(i in which.max(unl(lapply(res_list, function(x) x[1,1])))) {
  
  dat <- scan(datlist[i], what="character", sep=",")
  cm <- matrix(as.num(dat[c(6:8, 10:12, 14:16)]), 3, 3)
  cm01v2 <- matrix(c(sum(cm[-3,-3]), sum(cm[3,-3]), sum(cm[-3,3]), cm[3,3]), 2, 2)
  cm0v12 <- matrix(c(cm[1,1], sum(cm[-1,1]), sum(cm[1,-1]), sum(cm[-1,-1])), 2, 2)
  
  cat(i, ":\n")
  print(cm)

}

for(i in which.max(unl(lapply(res_list, function(x) x[2,1])))) {
  
  dat <- scan(datlist[i], what="character", sep=",")
  cm <- matrix(as.num(dat[c(6:8, 10:12, 14:16)]), 3, 3)
  cm01v2 <- matrix(c(sum(cm[-3,-3]), sum(cm[3,-3]), sum(cm[-3,3]), cm[3,3]), 2, 2)
  cm0v12 <- matrix(c(cm[1,1], sum(cm[-1,1]), sum(cm[1,-1]), sum(cm[-1,-1])), 2, 2)
  
  cat(i, ":\n")
  print(cm0v12)

}

for(i in which.max(unl(lapply(res_list, function(x) x[3,1])))) {
  
  dat <- scan(datlist[i], what="character", sep=",")
  cm <- matrix(as.num(dat[c(6:8, 10:12, 14:16)]), 3, 3)
  cm01v2 <- matrix(c(sum(cm[-3,-3]), sum(cm[3,-3]), sum(cm[-3,3]), cm[3,3]), 2, 2)
  cm0v12 <- matrix(c(cm[1,1], sum(cm[-1,1]), sum(cm[1,-1]), sum(cm[-1,-1])), 2, 2)
  
  cat(i, ":\n")
  print(cm01v2)
  
}




################################################33



stats_from_datlist <- function(datlist)
{
  res_list <- vector("list", len(datlist))
  for(i in 1:len(datlist)) {
    
    dat <- scan(datlist[i], what="character", sep=",")
    cm <- matrix(as.num(dat[c(6:8, 10:12, 14:16)]), 3, 3)
    cm01v2 <- matrix(c(sum(cm[-3,-3]), sum(cm[3,-3]), sum(cm[-3,3]), cm[3,3]), 2, 2)
    cm0v12 <- matrix(c(cm[1,1], sum(cm[-1,1]), sum(cm[1,-1]), sum(cm[-1,-1])), 2, 2)
    
    res <- cb("kap" = c(calc_cohen_kappa(cm), calc_cohen_kappa(cm0v12), calc_cohen_kappa(cm01v2)),
              "acc" = c(sum(diag(cm))/sum(cm), sum(diag(cm0v12))/sum(cm0v12), sum(diag(cm01v2))/sum(cm01v2)))
    
    res_list[[i]] <- res  
  }
  
  res_stats <- rbind(t(apply(matrix(unl(lapply(res_list, function(x) x[1,])), ncol=2, 
                                    byrow=T, dimnames=list(NULL, c("kap_full", "acc_full"))), 2, function(y) c(mean(y), sd(y)))),
                     t(apply(matrix(unl(lapply(res_list, function(x) x[2,])), ncol=2, 
                                    byrow=T, dimnames=list(NULL, c("kap_0v12", "acc_0v12"))), 2, function(y) c(mean(y), sd(y)))),
                     t(apply(matrix(unl(lapply(res_list, function(x) x[3,])), ncol=2, 
                                    byrow=T, dimnames=list(NULL, c("kap_01v2", "acc_01v2"))), 2, function(y) c(mean(y), sd(y)))))
  
  return(round(res_stats, 3))
}


datlist_conf <- datlist_all[grep("conf_matrix", datlist_all)]

( conf_stats_m1 <- stats_from_datlist(datlist_conf[grep("m1", datlist_conf)]) )
( conf_stats_m2 <- stats_from_datlist(datlist_conf[grep("m2", datlist_conf)]) )
( conf_stats_m5 <- stats_from_datlist(datlist_conf[grep("m5", datlist_conf)]) )
( conf_stats_m6 <- stats_from_datlist(datlist_conf[grep("m6", datlist_conf)]) )

cs <- conf_stats_m6
for(i in 1:nrow(cs)) cat(cs[i,1], " (", cs[i,2], ")\t", rownames(cs)[i], "\n", sep="")


############################

datlist_E <- getDataFromDir("~/HealthQ/RGA/Dirk modelling results/last model/", dataE=F)
stats_from_datlist(datlist_E[grep("conf_matrix", datlist_E)])


##################################
####  AUC SUMMARY STATISTICS  ####
##################################


auc_all <- read.table(datlist_all[grep("auc", datlist_all)], sep=",")
auc_all[,3] <- gsub("^\\s+|\\s+$", "", as.char(auc_all[,3]))

auc_grp <- lapply(as.list(c("0v1", "0v2", "1v2", "0v12", "01v2")), 
                  function(x) auc_all[which(!is.na(match(as.char(auc_all[,3]), x))), ])

auc_stats <- lapply(auc_grp, function(y) matrix(unl(tapply(y[,4], y[,1], function(x) c(mean(x), sd(x)))), ncol=2, byrow=T,
                                                dimnames = list(LETTERS[1:4], NULL)))
names(auc_stats) <- c("0v1", "0v2", "1v2", "0v12", "01v2")

auc_stats <- lapply(auc_stats, round, 3)

i <- 5

for(j in 0:4) {
  if(j == 0) cat(names(auc_stats)[i], "\n")
  else
    cat(auc_stats[[i]][j, 1], " (", auc_stats[[i]][j, 2], ")\n", sep="")
}


# mean AUC
auc_grp2 <- lapply(as.list(c("m1", "m2", "m5", "m6")), 
                   function(x) auc_all[which(!is.na(match(as.char(auc_all[,1]), x))), ])

auc_grp2 <- lapply(auc_grp2, function(x){ 
  y <- NULL
  for(i in 0:9) y <- c(y, mean(x[x[,2] == paste0(" fold", i), 4][3:5]) )
  return(y) })



##### profile only model
###########################

datlist_profonly <- getDataFromDir("~/HealthQ/RGA/Dirk modelling results/profile_only/", dataE=F)
auc_all <- read.table(datlist_profonly[grep("auc", datlist_profonly)], sep=",")
auc_all[,3] <- gsub("^\\s+|\\s+$", "", as.char(auc_all[,3]))

auc_grp <- lapply(as.list(c("0v1", "0v2", "1v2", "0v12", "01v2")), 
                  function(x) auc_all[which(!is.na(match(as.char(auc_all[,3]), x))), ])

auc_stats <- lapply(auc_grp, function(y) c(mean(y[,4]), sd(y[,4])))
names(auc_stats) <- c("0v1", "0v2", "1v2", "0v12", "01v2")
auc_stats <- lapply(auc_stats, round, 3)

auc_grp2 <- lapply(as.list(c("ordinal_regression")), 
                   function(x) auc_all[which(!is.na(match(as.char(auc_all[,1]), x))), ])

auc_grp2 <- lapply(auc_grp2, function(x){ 
  y <- NULL
  for(i in 0:9) y <- c(y, mean(x[x[,2] == paste0(" fold", i), 4][3:5]) )
  return(y) })



###########################################
####  INTERPOLATE AND PLOT ROC CURVES  ####
###########################################


read_model_rocs <- function(datlist)
{
  # 3 class model:
  roc_0v1 <- lapply(datlist[grep("_0v1_", datlist)], function(x) read.table(x, head=T, sep=",")[,-1])
  roc_0v2 <- lapply(datlist[grep("_0v2_", datlist)], function(x) read.table(x, head=T, sep=",")[,-1])
  roc_1v2 <- lapply(datlist[grep("_1v2_", datlist)], function(x) read.table(x, head=T, sep=",")[,-1])
  
  # 2 class model 0v12:
  roc_0v12 <- lapply(datlist[grep("_0v12_", datlist)], function(x) read.table(x, head=T, sep=",")[,-1])
  
  # 2 class model 01v2:
  roc_01v2 <- lapply(datlist[grep("_01v2_", datlist)], function(x) read.table(x, head=T, sep=",")[,-1])
  
  # cols <- heat.colors(10)
  # w(12, 12) ; lplot_g(roc_0v1[[1]][,1], roc_0v1[[1]][,2], col=cols[1])
  # for(i in 2:10) lines(roc_0v1[[i]][,1], roc_0v1[[i]][,2], col=cols[i])
  
  
  make_roc_spline <- function(roc)
  {
    roc <- lapply(roc, function(x) cb(x[,1], x[,2]))
    roc_u <- roc[[1]]
    for(i in 2:len(roc)) roc_u <- rbind(roc_u, roc[[i]])
    return(smooth.spline(roc_u[,1], roc_u[,2]))
  }
  
  return(list(make_roc_spline(roc_0v1),
              make_roc_spline(roc_0v2),
              make_roc_spline(roc_1v2),
              make_roc_spline(roc_0v12),
              make_roc_spline(roc_01v2)))
  
  
  # w(12, 12) ; lplot_g(roc_spline$x, roc_spline$y, col="red", lwd=3,
  #                     xlab="false positive rate", ylab = "true positive rate")
  # abline(0, 1, lty=2)
  
}


datlist_roc <- datlist_all[grep("roc", datlist_all)]

roc_splines_m1 <- read_model_rocs(datlist_roc[grep("m1", datlist_roc)])
roc_splines_m2 <- read_model_rocs(datlist_roc[grep("m2", datlist_roc)])
roc_splines_m5 <- read_model_rocs(datlist_roc[grep("m5", datlist_roc)])
roc_splines_m6 <- read_model_rocs(datlist_roc[grep("m6", datlist_roc)])


plot_roc_splines <- function(i, main="")
{
  require(RColorBrewer)
  
  cols <- c(brewer.pal(3, "Set1"), "orange")
  
  w(12, 12)
  lplot_g(roc_splines_m1[[i]]$x, roc_splines_m1[[i]]$y, col=cols[1], lwd=3,
          xlab="False Positive Rate", ylab = "True Positive Rate", main=main)
  lines(roc_splines_m2[[i]]$x, roc_splines_m2[[i]]$y, col=cols[2], lwd=3)
  lines(roc_splines_m5[[i]]$x, roc_splines_m5[[i]]$y, col=cols[3], lwd=3)
  lines(roc_splines_m6[[i]]$x, roc_splines_m6[[i]]$y, col=cols[4], lwd=3)
  abline(0, 1, lty=2)
  
  legend("bottomright", lwd=2, col=cols, bg="white",
         leg=c(paste("Data subset", c("A", "B", "C", "D"))))
  
  
}


plot_roc_splines(1, "ROC curves for 3-class model: 0 vs 1")
plot_roc_splines(2, "ROC curves for 3-class model: 0 vs 2")
plot_roc_splines(3, "ROC curves for 3-class model: 1 vs 2")
plot_roc_splines(4, "ROC curves for 2-class model: 0 vs {1, 2}")
plot_roc_splines(5, "ROC curves for 2-class model: {0, 1} vs 2")


# cols <- rainbow(10)
# w(12, 12) ; lplot_g(roc_1v2[[1]][,1], roc_1v2[[1]][,2], col=cols[1], lwd=2,
#                     xlab="False Positive Rate", ylab = "True Positive Rate",
#                     main = "ROC curves for 1v2")
# for(i in 2:10) lines(roc_1v2[[i]][,1], roc_1v2[[i]][,2], col=cols[i], lwd=2)
# abline(0, 1, lwd=2)



gdat <- data.frame("fpr" = c(roc_splines_m1[[1]]$x, roc_splines_m2[[1]]$x, roc_splines_m5[[1]]$x, roc_splines_m6[[1]]$x),
                   "tpr" = c(roc_splines_m1[[1]]$y, roc_splines_m2[[1]]$y, roc_splines_m5[[1]]$y, roc_splines_m6[[1]]$y),
                   model = as.factor(rep(paste0("data subset ", LETTERS[1:4]),  c(len(roc_splines_m1[[1]]$x), len(roc_splines_m2[[1]]$x), 
                                                                            len(roc_splines_m5[[1]]$x), len(roc_splines_m6[[1]]$x)))))

g2 <- ggplot(gdat, aes(x=fpr, y=tpr, group=model)) + geom_line(aes(color=model), size=2)
g2 <- g2 + geom_abline(slope=1, intercept=0, linetype="dashed")
g2 <- g2 + ggtitle("ROC curves for 3-class model: 0 vs 1") + xlab("false positive rate") + ylab("true positive rate")
g2 <- g2 + theme(legend.title = element_blank(), legend.justification = c(1, 1), legend.position = c(1, 0.2))
w(12, 12)  ; g2
           


###################################
####  DIRK MODEL OUTPUT PLOTS  ####
###################################


datlist_pred <- datlist_all[grep("pred", datlist_all)]

for(i in 1:len(datlist_pred)) {
  dat <- read.table(datlist_pred[i], head=T, sep=",")[,-1]
  
  for(j in 1:10) {
    
    xy <- dat[, match(paste0(c("y", "yhat"), "_test_", j), names(dat))]
    xy[,1] <- factor(xy[,1], labels = c("s-pref", "pref", "n-pref"))
    names(xy) <- c("class", "prediction")
    
    png(paste0("C:/Users/reniervr/Documents/HealthQ/R_scripts/STATS_RGA/dirk_pred_vs_targ/m", i, "_pred_vs_targ_", j-1, ".png"),
         width=1600, height=900, res=200)#, quality=100)
    g <- ggplot(as.data.frame(xy), aes(x=prediction, fill=class)) + ggtitle("Distribution of predicted values by true class")
    g <- g + geom_density(alpha=0.4) + coord_cartesian(xlim = c(0, 1))
    g <- g + theme(legend.title = element_blank(), legend.justification = c(1, 1), legend.position = c(1, 1))
    print(g) ; invisible(dev.off())
    
  }
}



i <- 1 ; j <- 9
i <- 2 ; j <- 10
i <- 3 ; j <- 2
i <- 4 ; j <- 4





#######################
####  FINAL PLOTS  ####
#######################

### MODEL A:
##############
dat <- read.table(datlist_pred[1], head=T, sep=",")[,-1]
xy <- dat[, match(paste0(c("y", "yhat"), "_test_", 9), names(dat))]
xy[,1] <- factor(xy[,1], labels = c("s-pref", "pref", "n-pref"))
xy <- cbind(xy, as.factor(rep("Data subset A", nrow(xy))))
names(xy) <- c("class", "prediction", "model")

### MODEL B:
##############
dat <- read.table(datlist_pred[2], head=T, sep=",")[,-1]
xy2 <- dat[, match(paste0(c("y", "yhat"), "_test_", 10), names(dat))]
xy2[,1] <- factor(xy2[,1], labels = c("s-pref", "pref", "n-pref"))
xy2 <- cbind(xy2, as.factor(rep("Data subset B", nrow(xy2))))
names(xy2) <- c("class", "prediction", "model")

### MODEL C:
##############
dat <- read.table(datlist_pred[3], head=T, sep=",")[,-1]
xy3 <- dat[, match(paste0(c("y", "yhat"), "_test_", 2), names(dat))]
xy3[,1] <- factor(xy3[,1], labels = c("s-pref", "pref", "n-pref"))
xy3 <- cbind(xy3, as.factor(rep("Data subset C", nrow(xy3))))
names(xy3) <- c("class", "prediction", "model")

### MODEL D:
##############
dat <- read.table(datlist_pred[4], head=T, sep=",")[,-1]
xy4 <- dat[, match(paste0(c("y", "yhat"), "_test_", 4), names(dat))]
xy4[,1] <- factor(xy4[,1], labels = c("s-pref", "pref", "n-pref"))
xy4 <- cbind(xy4, as.factor(rep("Data subset D", nrow(xy4))))
names(xy4) <- c("class", "prediction", "model")

g1dat_0v1v2 <- rbind(xy, xy2, xy3, xy4)

target_0v12 <- as.char(g1dat_0v1v2[,1])
target_0v12[target_0v12 %in% c("pref", "n-pref")] <- "non-s-pref"
g1dat_0v12 <- cbind("class"=as.factor(target_0v12), g1dat_0v1v2[,-1])

target_01v2 <- as.char(g1dat_0v1v2[,1])
target_01v2[target_01v2 %in% c("s-pref", "pref")] <- "all-pref"
g1dat_01v2 <- cbind("class"=as.factor(target_01v2), g1dat_0v1v2[,-1])





png(paste0("C:/Users/reniervr/Documents/HealthQ/RGA/Report/pred_v_target_all_0v1v2.png"),
    width=2500, height=1400, res=200)
g1 <- ggplot(g1dat_0v1v2, aes(x=prediction, fill=class)) + facet_wrap(~model) 
g1 <- g1 + xlab("Predicted value") + ylab("Density") # + ggtitle("Distribution of predicted values by true class")
g1 <- g1 + geom_density(alpha=0.4) + coord_cartesian(xlim = c(0.15, 0.95))
g1 <- g1 + theme(legend.title = element_blank(), legend.justification = c(1, 1), legend.position = c(1, 1))
print(g1) ; invisible(dev.off())



png(paste0("C:/Users/reniervr/Documents/HealthQ/RGA/Report/pred_v_target_all_0v12.png"),
    width=2500, height=1400, res=200)
g1dat_0v12[,1] <- relevel(g1dat_0v12[,1], "s-pref")
g1 <- ggplot(g1dat_0v12, aes(x=prediction, fill=class)) + facet_wrap(~model) 
g1 <- g1 + xlab("Predicted value") + ylab("Density") #  + ggtitle("Distribution of predicted values by true class")
g1 <- g1 + geom_density(alpha=0.4) + coord_cartesian(xlim = c(0.15, 0.95))
g1 <- g1 + theme(legend.title = element_blank(), legend.justification = c(1, 1), legend.position = c(1, 1))
print(g1) ; invisible(dev.off())
# w() ; g1


png(paste0("C:/Users/reniervr/Documents/HealthQ/RGA/Report/pred_v_target_all_01v2.png"),
    width=2500, height=1400, res=200)
g1 <- ggplot(g1dat_01v2, aes(x=prediction, fill=class)) + facet_wrap(~model) 
g1 <- g1 + xlab("Predicted value") + ylab("Density") #  + ggtitle("Distribution of predicted values by true class")
g1 <- g1 + geom_density(alpha=0.4) + coord_cartesian(xlim = c(0.15, 0.95))
g1 <- g1 + theme(legend.title = element_blank(), legend.justification = c(1, 1), legend.position = c(1, 1))
print(g1) ; invisible(dev.off())





#############################################################################
#############################################################################
#############################################################################





gdat <- data.frame("fpr" = c(roc_splines_m1[[1]]$x, roc_splines_m2[[1]]$x, roc_splines_m5[[1]]$x, roc_splines_m6[[1]]$x),
                   "tpr" = c(roc_splines_m1[[1]]$y, roc_splines_m2[[1]]$y, roc_splines_m5[[1]]$y, roc_splines_m6[[1]]$y),
                   model = as.factor(rep(paste0("data subset ", LETTERS[1:4]),  c(len(roc_splines_m1[[1]]$x), len(roc_splines_m2[[1]]$x), 
                                                                                  len(roc_splines_m5[[1]]$x), len(roc_splines_m6[[1]]$x)))))

g2 <- ggplot(gdat, aes(x=fpr, y=tpr, group=model)) + geom_line(aes(color=model), size=2)
g2 <- g2 + geom_abline(slope=1, intercept=0, linetype="dashed")
g2 <- g2 + xlab("false positive rate") + ylab("true positive rate") #ggtitle("ROC curves for 3-class model: 0 vs 1")
g2 <- g2 + theme(legend.title = element_blank(), legend.justification = c(1, 1), legend.position = c(1, 0.2))

png(paste0("C:/Users/reniervr/Documents/HealthQ/RGA/Report/roc_a1_0v1.png"),
    width=1400, height=1400, res=200)
print(g2) ; invisible(dev.off())


#############################################################################

gdat <- data.frame("fpr" = c(roc_splines_m1[[2]]$x, roc_splines_m2[[2]]$x, roc_splines_m5[[2]]$x, roc_splines_m6[[2]]$x),
                   "tpr" = c(roc_splines_m1[[2]]$y, roc_splines_m2[[2]]$y, roc_splines_m5[[2]]$y, roc_splines_m6[[2]]$y),
                   model = as.factor(rep(paste0("data subset ", LETTERS[1:4]),  c(len(roc_splines_m1[[2]]$x), len(roc_splines_m2[[2]]$x), 
                                                                                  len(roc_splines_m5[[2]]$x), len(roc_splines_m6[[2]]$x)))))

g2 <- ggplot(gdat, aes(x=fpr, y=tpr, group=model)) + geom_line(aes(color=model), size=2)
g2 <- g2 + geom_abline(slope=1, intercept=0, linetype="dashed")
g2 <- g2 + xlab("false positive rate") + ylab("true positive rate") #ggtitle("ROC curves for 3-class model: 0 vs 1")
g2 <- g2 + theme(legend.title = element_blank(), legend.justification = c(1, 1), legend.position = c(1, 0.2))

png(paste0("C:/Users/reniervr/Documents/HealthQ/RGA/Report/roc_a1_0v2.png"),
    width=1400, height=1400, res=200)
print(g2) ; invisible(dev.off())


#############################################################################

gdat <- data.frame("fpr" = c(roc_splines_m1[[3]]$x, roc_splines_m2[[3]]$x, roc_splines_m5[[3]]$x, roc_splines_m6[[3]]$x),
                   "tpr" = c(roc_splines_m1[[3]]$y, roc_splines_m2[[3]]$y, roc_splines_m5[[3]]$y, roc_splines_m6[[3]]$y),
                   model = as.factor(rep(paste0("data subset ", LETTERS[1:4]),  c(len(roc_splines_m1[[3]]$x), len(roc_splines_m2[[3]]$x), 
                                                                                  len(roc_splines_m5[[3]]$x), len(roc_splines_m6[[3]]$x)))))

g2 <- ggplot(gdat, aes(x=fpr, y=tpr, group=model)) + geom_line(aes(color=model), size=2)
g2 <- g2 + geom_abline(slope=1, intercept=0, linetype="dashed")
g2 <- g2 + xlab("false positive rate") + ylab("true positive rate") #ggtitle("ROC curves for 3-class model: 0 vs 1")
g2 <- g2 + theme(legend.title = element_blank(), legend.justification = c(1, 1), legend.position = c(1, 0.2))

png(paste0("C:/Users/reniervr/Documents/HealthQ/RGA/Report/roc_a1_1v2.png"),
    width=1400, height=1400, res=200)
print(g2) ; invisible(dev.off())


#############################################################################

gdat <- data.frame("fpr" = c(roc_splines_m1[[4]]$x, roc_splines_m2[[4]]$x, roc_splines_m5[[4]]$x, roc_splines_m6[[4]]$x),
                   "tpr" = c(roc_splines_m1[[4]]$y, roc_splines_m2[[4]]$y, roc_splines_m5[[4]]$y, roc_splines_m6[[4]]$y),
                   model = as.factor(rep(paste0("data subset ", LETTERS[1:4]),  c(len(roc_splines_m1[[4]]$x), len(roc_splines_m2[[4]]$x), 
                                                                                  len(roc_splines_m5[[4]]$x), len(roc_splines_m6[[4]]$x)))))

g2 <- ggplot(gdat, aes(x=fpr, y=tpr, group=model)) + geom_line(aes(color=model), size=2)
g2 <- g2 + geom_abline(slope=1, intercept=0, linetype="dashed")
g2 <- g2 + xlab("false positive rate") + ylab("true positive rate") #ggtitle("ROC curves for 3-class model: 0 vs 1")
g2 <- g2 + theme(legend.title = element_blank(), legend.justification = c(1, 1), legend.position = c(1, 0.2))

png(paste0("C:/Users/reniervr/Documents/HealthQ/RGA/Report/roc_a2_0v12.png"),
    width=1400, height=1400, res=200)
print(g2) ; invisible(dev.off())


#############################################################################

gdat <- data.frame("fpr" = c(roc_splines_m1[[5]]$x, roc_splines_m2[[5]]$x, roc_splines_m5[[5]]$x, roc_splines_m6[[5]]$x),
                   "tpr" = c(roc_splines_m1[[5]]$y, roc_splines_m2[[5]]$y, roc_splines_m5[[5]]$y, roc_splines_m6[[5]]$y),
                   model = as.factor(rep(paste0("data subset ", LETTERS[1:4]),  c(len(roc_splines_m1[[5]]$x), len(roc_splines_m2[[5]]$x), 
                                                                                  len(roc_splines_m5[[5]]$x), len(roc_splines_m6[[5]]$x)))))

g2 <- ggplot(gdat, aes(x=fpr, y=tpr, group=model)) + geom_line(aes(color=model), size=2)
g2 <- g2 + geom_abline(slope=1, intercept=0, linetype="dashed")
g2 <- g2 + xlab("false positive rate") + ylab("true positive rate") #ggtitle("ROC curves for 3-class model: 0 vs 1")
g2 <- g2 + theme(legend.title = element_blank(), legend.justification = c(1, 1), legend.position = c(1, 0.2))

png(paste0("C:/Users/reniervr/Documents/HealthQ/RGA/Report/roc_a3_01v2.png"),
    width=1400, height=1400, res=200)
print(g2) ; invisible(dev.off())

























