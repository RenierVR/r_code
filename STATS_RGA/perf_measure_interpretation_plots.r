library(viridis)

w() ; plot(0, 0, type="n", xlim=c(0,1), ylim=c(0,1), axes=F, ylab="", xlab="")
axis(1, seq(0, 1, 0.1))

n <- 100
nseq <- seq(0, 1, len=(n+1))
ncols <- viridis(n)
for(i in 1:n) rect(nseq[i], 0, nseq[i+1], 0.25, col=ncols[i], border=NA)

mtext("random", side=1, line=2, at=0)
mtext("chance", side=1, line=3, at=0)
mtext("perfect", side=1, line=2, at=1)
mtext("agreement", side=1, line=3, at=1)


text(0.1, 0.3, "poor")
text(0.3, 0.3, "fair")
text(0.55, 0.3, "good")
text(0.75, 0.3, "excellent")




########################################
########################################



x <- seq(-1, 0, 0.01)
y <- (1 - x^2)^(1/2)
y2 <- (1 - x^4)^(1/4)
# y3 <- (1 - x^6)^(1/6)

# w(12, 12) ; lplot_g(x, y) ; lines(x, y2 + (x+1))# ; lines(x, y3)

gdat <- data.frame(X=rep(x+1, 2), Y=c(y, y2), LAB=as.factor(rep(paste("AUC = ", c("0.8", "0.9")), each=len(x))))

g2 <- ggplot(gdat, aes(x=X, y=Y, group=LAB)) + geom_line(aes(color=LAB), size=2)
g2 <- g2 + geom_abline(slope=1, intercept=0, linetype="dashed")
g2 <- g2 + xlab("false positive rate") + ylab("true positive rate")
g2 <- g2 + theme(legend.title = element_blank(), legend.justification = c(1, 1), legend.position = c(1, 0.2))


w(12, 12) ; g2


png(paste0("C:/Users/reniervr/Documents/HealthQ/RGA/Report/roc_curve_example.png"),
    width=1400, height=1400, res=200)
print(g2) ; invisible(dev.off())



yf1 <- function(x) return((1 - x^2)^(1/2))
yf2 <- function(x) return((1 - x^4)^(1/4))
yf3 <- function(x) return((1 - x^6)^(1/6))

integrate(yf1, -1, 0)
integrate(yf2, -1, 0)
integrate(yf3, -1, 0)





########################################
########################################

rga_samp <- table(dat_all[dat_all$COHORT == "RGA",]$UW_K3)


ncols <- viridis(10)[seq(1, 10, 2)]

df1<-data.frame(Loc=c(rep("Analysis 1", sum(rga_samp)),rep("Analysis 2", sum(rga_samp)),rep("Analysis 3", sum(rga_samp))),
                Type=factor(c(rep("super-pref", rga_samp[1]), rep("pref", rga_samp[2]), rep("non-pref", rga_samp[3]), 
                              rep("super-pref", rga_samp[1]), rep("non-super-pref", sum(rga_samp[2:3])), 
                              rep("all-pref", sum(rga_samp[1:2])), rep("non-pref", rga_samp[3])),
                            levels = c("super-pref", "all-pref", "pref", "non-super-pref", "non-pref")))
                # y2009=rep("A",12),y2010=c("A","B","A","A","A","A","B","B","A","A","B","B"),
                # y2011=c("B","B","B","A","B",rep("B",4),"A","B","B"))
df1


g3 <- ggplot(df1,aes(x=Type, fill=Type)) + geom_bar()+facet_grid(~Loc, scales = "free_x") 
g3 <- g3 + scale_fill_manual("legend", values = c("super-pref" = ncols[1], 
                                                  "all-pref" = ncols[2], 
                                                  "pref" = ncols[3], 
                                                  "non-super-pref" = ncols[4], 
                                                  "non-pref" = ncols[5]))
g3 <- g3 + guides(fill = FALSE) + xlab("") + ylab("n")

# w(12,4) ; gg


png(paste0("C:/Users/reniervr/Documents/HealthQ/RGA/Report/rga_class_breakdown.png"),
    width=2400, height=800, res=200)
print(g3) ; invisible(dev.off())











