source("rga_risk_modelling_01_dat.r")
source("rga_risk_modelling_02_model_funs.r")

library(glmnet)
library(glmnetcr)
library(randomForest)
library(ggplot2)
library(gridExtra)


#######################################
####  Data for modelling  #############
#######################################

nms_all <- names(dat_all)
nms_allnp <- names(dat_all_noprof)
nms_rga <- names(dat_rga)
nms_rganp <- names(dat_rga_noprof)


dat_np_k3 <- dat_all_noprof[, c(match(c("COHORT", "ANON_ID", "UW_K3", "BIO_SEX", "BIO_AGE"), nms_allnp), 
                                grep("ACTIVITY.sessionSteps_mean", nms_allnp):ncol(dat_all_noprof))] 

# Remove "HRV" features
i_hrv <- grep("HRV", names(dat_np_k3))
i_hrv <- i_hrv[1:which(diff(i_hrv) > 1)[1]]
dat_np_k3 <- dat_np_k3[, -i_hrv]

# Remove columns with more than specified nr of NAs
dat_np_k3 <- dat_np_k3[, which(count_NA(dat_np_k3) <= 111)]

# Set of 2 class data subsets
dat_np_k2sp <- cb(dat_np_k3[, 1:2], dat_all_noprof[grep("UW_K2_sp", nms_allnp)], dat_np_k3[, 4:ncol(dat_np_k3)])
dat_np_k2p <- cb(dat_np_k3[, 1:2], dat_all_noprof[grep("UW_K2_p", nms_allnp)], dat_np_k3[, 4:ncol(dat_np_k3)])


# All rows with missing values should automatically be removed in modelling functions
# dat_np_k3 <- dat_np_k3[!apply(dat_np_k3, 1, function(x) any(is.na(x))), ]

# sum(apply(dat_np_k3, 1, function(x) any(is.na(x))))





dat_k3 <- dat_all[, c(match(c("COHORT", "ANON_ID", "UW_K3", "BIO_SEX", "BIO_AGE",
                              "BIO_BMI", "BIO_DEFAULT_VO2MAX"), nms_all), 
                      grep("ACTIVITY.sessionSteps_mean", nms_all):ncol(dat_all))] 

# Remove "HRV" features
i_hrv <- grep("HRV", names(dat_k3))
i_hrv <- i_hrv[1:which(diff(i_hrv) > 1)[1]]
dat_k3 <- dat_k3[, -i_hrv]

# Remove columns with more than specified nr of NAs
dat_k3 <- dat_k3[, which(count_NA(dat_k3) <= 111)]

# Set of 2 class data subsets
dat_k2sp <- cb(dat_k3[, 1:2], dat_all[grep("UW_K2_sp", nms_all)], dat_k3[, 4:ncol(dat_k3)])
dat_k2p <- cb(dat_k3[, 1:2], dat_all[grep("UW_K2_p", nms_all)], dat_k3[, 4:ncol(dat_k3)])







dat_nrs_k3 <- dat_all[, c(match(c("COHORT", "ANON_ID", "UW_K3", "BIO_SEX", "BIO_AGE",
                                  "BIO_BMI", "BIO_DEFAULT_VO2MAX", "BIO_BP_SYS", "BIO_BP_DIA", "BIO_CHOL"), nms_all),
                          grep("ACTIVITY.sessionSteps_mean", nms_all):ncol(dat_all))] 

# Remove "HRV" features
i_hrv <- grep("HRV", names(dat_nrs_k3))
i_hrv <- i_hrv[1:which(diff(i_hrv) > 1)[1]]
dat_nrs_k3 <- dat_nrs_k3[, -i_hrv]

# Remove columns with more than specified nr of NAs
dat_nrs_k3 <- dat_nrs_k3[, which(count_NA(dat_nrs_k3) <= 111)]

# Set of 2 class data subsets
dat_nrs_k2sp <- cb(dat_nrs_k3[, 1:2], dat_all[grep("UW_K2_sp", nms_all)], dat_nrs_k3[, 4:ncol(dat_nrs_k3)])
dat_nrs_k2p <- cb(dat_nrs_k3[, 1:2], dat_all[grep("UW_K2_p", nms_all)], dat_nrs_k3[, 4:ncol(dat_nrs_k3)])







dat_nrsq_k3 <- dat_all[, c(match(c("COHORT", "ANON_ID", "UW_K3", "BIO_SEX", "BIO_AGE",
                                  "BIO_BMI", "BIO_DEFAULT_VO2MAX", "BIO_BP_SYS", "BIO_BP_DIA", "BIO_CHOL"), nms_all),
                          grep("QUESTION", nms_all),
                          grep("ACTIVITY.sessionSteps_mean", nms_all):ncol(dat_all))]

# Remove "HRV" features
i_hrv <- grep("HRV", names(dat_nrsq_k3))
i_hrv <- i_hrv[1:which(diff(i_hrv) > 1)[1]]
dat_nrsq_k3 <- dat_nrsq_k3[, -i_hrv]

# Remove columns with more than specified nr of NAs
dat_nrsq_k3 <- dat_nrsq_k3[, which(count_NA(dat_nrsq_k3) <= 111)]

# Digitise Smoke Status
i_smoke <- grep("SMOKE", names(dat_nrsq_k3))
dat_nrsq_k3[, i_smoke] <- as.char(dat_nrsq_k3[, i_smoke])
dat_nrsq_k3[dat_nrsq_k3[, i_smoke] %in% c("Ex1", "Ex2"), i_smoke] <- "Yes"
dat_nrsq_k3[dat_nrsq_k3[, i_smoke] %in% c("Ex3"), i_smoke] <- "No"
dat_nrsq_k3[, i_smoke] <- as.num(as.factor(dat_nrsq_k3[, i_smoke])) - 1


# Set of 2 class data subsets
dat_nrsq_k2sp <- cb(dat_nrsq_k3[, 1:2], dat_all[grep("UW_K2_sp", nms_all)], dat_nrsq_k3[, 4:ncol(dat_nrsq_k3)])
dat_nrsq_k2p <- cb(dat_nrsq_k3[, 1:2], dat_all[grep("UW_K2_p", nms_all)], dat_nrsq_k3[, 4:ncol(dat_nrsq_k3)])








dat_lf_k3 <- dat_all[, c(match(c("COHORT", "ANON_ID", "UW_K3", "BIO_SEX", "BIO_AGE",
                                 "BIO_BMI", "BIO_DEFAULT_VO2MAX"), nms_all),
                         grep("QUESTION", nms_all),
                         grep("ACTIVITY.sessionSteps_mean", nms_all):ncol(dat_all))]

# Remove "HRV" features
i_hrv <- grep("HRV", names(dat_lf_k3))
i_hrv <- i_hrv[1:which(diff(i_hrv) > 1)[1]]
dat_lf_k3 <- dat_lf_k3[, -i_hrv]

# Remove columns with more than specified nr of NAs
dat_lf_k3 <- dat_lf_k3[, which(count_NA(dat_lf_k3) <= 111)]

# Digitise Smoke Status
i_smoke <- grep("SMOKE", names(dat_lf_k3))
dat_lf_k3[, i_smoke] <- as.char(dat_lf_k3[, i_smoke])
dat_lf_k3[dat_lf_k3[, i_smoke] %in% c("Ex1", "Ex2"), i_smoke] <- "Yes"
dat_lf_k3[dat_lf_k3[, i_smoke] %in% c("Ex3"), i_smoke] <- "No"
dat_lf_k3[, i_smoke] <- as.num(as.factor(dat_lf_k3[, i_smoke])) - 1


# Set of 2 class data subsets
dat_lf_k2sp <- cb(dat_lf_k3[, 1:2], dat_all[grep("UW_K2_sp", nms_all)], dat_lf_k3[, 4:ncol(dat_lf_k3)])
dat_lf_k2p <- cb(dat_lf_k3[, 1:2], dat_all[grep("UW_K2_p", nms_all)], dat_lf_k3[, 4:ncol(dat_lf_k3)])



#############################################################

dat_nw_k3 <- dat_all[, c(match(c("COHORT", "ANON_ID", "UW_K3", "BIO_SEX", "BIO_AGE"), nms_all))]
dat_nw_k2sp <- dat_all[, c(match(c("COHORT", "ANON_ID", "UW_K2_sp", "BIO_SEX", "BIO_AGE"), nms_all))]
dat_nw_k2p <- dat_all[, c(match(c("COHORT", "ANON_ID", "UW_K2_p", "BIO_SEX", "BIO_AGE"), nms_all))]


dat_nw_p_k3 <- dat_all[, c(match(c("COHORT", "ANON_ID", "UW_K3", "BIO_SEX", "BIO_AGE", "BIO_BMI"), nms_all))]
dat_nw_p_k2sp <- dat_all[, c(match(c("COHORT", "ANON_ID", "UW_K2_sp", "BIO_SEX", "BIO_AGE", "BIO_BMI"), nms_all))]
dat_nw_p_k2p <- dat_all[, c(match(c("COHORT", "ANON_ID", "UW_K2_p", "BIO_SEX", "BIO_AGE", "BIO_BMI"), nms_all))]


dat_nw_nrs_k3 <- dat_all[, c(match(c("COHORT", "ANON_ID", "UW_K3", "BIO_SEX", "BIO_AGE", "BIO_BMI", "BIO_BP_SYS", "BIO_BP_DIA", "BIO_CHOL"), nms_all))]
dat_nw_nrs_k2sp <- dat_all[, c(match(c("COHORT", "ANON_ID", "UW_K2_sp", "BIO_SEX", "BIO_AGE", "BIO_BMI", "BIO_BP_SYS", "BIO_BP_DIA", "BIO_CHOL"), nms_all))]
dat_nw_nrs_k2p <- dat_all[, c(match(c("COHORT", "ANON_ID", "UW_K2_p", "BIO_SEX", "BIO_AGE", "BIO_BMI", "BIO_BP_SYS", "BIO_BP_DIA", "BIO_CHOL"), nms_all))]


dat_nw_nrsq_k3 <- dat_all[, c(match(c("COHORT", "ANON_ID", "UW_K3", "BIO_SEX", "BIO_AGE", "BIO_BMI", "BIO_BP_SYS", "BIO_BP_DIA", "BIO_CHOL"), nms_all), grep("QUESTION", nms_all))]
dat_nw_nrsq_k2sp <- dat_all[, c(match(c("COHORT", "ANON_ID", "UW_K2_sp", "BIO_SEX", "BIO_AGE", "BIO_BMI", "BIO_BP_SYS", "BIO_BP_DIA", "BIO_CHOL"), nms_all), grep("QUESTION", nms_all))]
dat_nw_nrsq_k2p <- dat_all[, c(match(c("COHORT", "ANON_ID", "UW_K2_p", "BIO_SEX", "BIO_AGE", "BIO_BMI", "BIO_BP_SYS", "BIO_BP_DIA", "BIO_CHOL"), nms_all), grep("QUESTION", nms_all))]


dat_nw_pq_k3 <- dat_all[, c(match(c("COHORT", "ANON_ID", "UW_K3", "BIO_SEX", "BIO_AGE", "BIO_BMI"), nms_all), grep("QUESTION", nms_all))]
dat_nw_pq_k2sp <- dat_all[, c(match(c("COHORT", "ANON_ID", "UW_K2_sp", "BIO_SEX", "BIO_AGE", "BIO_BMI"), nms_all), grep("QUESTION", nms_all))]
dat_nw_pq_k2p <- dat_all[, c(match(c("COHORT", "ANON_ID", "UW_K2_p", "BIO_SEX", "BIO_AGE", "BIO_BMI"), nms_all), grep("QUESTION", nms_all))]



# Digitise Smoke Status
i_smoke <- grep("SMOKE", names(dat_nw_nrsq_k3))
dat_nw_nrsq_k3[, i_smoke] <- as.char(dat_nw_nrsq_k3[, i_smoke])
dat_nw_nrsq_k3[dat_nw_nrsq_k3[, i_smoke] %in% c("Ex1", "Ex2"), i_smoke] <- "Yes"
dat_nw_nrsq_k3[dat_nw_nrsq_k3[, i_smoke] %in% c("Ex3"), i_smoke] <- "No"
dat_nw_nrsq_k3[, i_smoke] <- as.num(as.factor(dat_nw_nrsq_k3[, i_smoke])) - 1

dat_nw_nrsq_k2sp$QUESTION_SMOKE <- dat_nw_nrsq_k3$QUESTION_SMOKE
dat_nw_nrsq_k2p$QUESTION_SMOKE <- dat_nw_nrsq_k3$QUESTION_SMOKE

dat_nw_pq_k3$QUESTION_SMOKE <- dat_nw_nrsq_k3$QUESTION_SMOKE
dat_nw_pq_k2sp$QUESTION_SMOKE <- dat_nw_nrsq_k3$QUESTION_SMOKE
dat_nw_pq_k2p$QUESTION_SMOKE <- dat_nw_nrsq_k3$QUESTION_SMOKE


#############################################################


folds5 <- read.table("./stratified_folds.csv", sep=",", head=T)
folds3 <- read.table("C:/Users/reniervr/Downloads/folds_3.csv", sep=",", head=T)

generate_RGA_test_results <- function(dat_id, folds, model_type, ...) 
{
  n <- ncol(folds) - 2
  m <- len(unique(dat_id[,3]))
  
  print(m)
  
  out_list_models <- NULL
  out_list_results <- NULL
  for(i in 1:n) {
    out <- runSupervisedCV_MMIRGATest(dat_id, folds=folds[,c(2, i+2)], model_type=model_type, ...)
    
    out_list_models <- c(out_list_models, out[[1]])
    out_list_results <- c(out_list_results, out[[2]])
    
    cat("OVERALL PROGRESS: ")
    catProgress(i, n)
  }
  
  return(list(out_list_models, out_list_results))
}


print_results <- function(cv_list)
{
  res_agg <- resultAggregator(cv_list)
  res <- plotAggResults(res_agg)
  
  m <- nrow(cv_list[[2]][[1]][[1]][[1]][[1]])
  
  if(m == 3) {
    res_print <- rbind(res$test_kappa[which.max(res$test_kappa[,1]), ],
                       res$AUC_0v1[which.max(res$AUC_0v1[,1]), ],
                       res$AUC_1v2[which.max(res$AUC_1v2[,1]), ],
                       res$AUC_0v2[which.max(res$AUC_0v2[,1]), ])
    res_print <- round(t(apply(res_print, 1, function(x) c(x[1], x[3]-x[1]))),3)
    
    dimnames(res_print) <- list(c("Kappa_full", "AUC_0v1", "AUC_1v2", "AUC_0v2"), c("MEAN", "SD"))
  }
  if(m == 2) {
    res_print <- rbind(res$test_kappa[which.max(res$test_kappa[,1]), ],
                       res$AUC[which.max(res$AUC[,1]), ])
    res_print <- round(t(apply(res_print, 1, function(x) c(x[1], x[3]-x[1]))),3)
    
    dimnames(res_print) <- list(c("Kappa_full", "AUC"), c("MEAN", "SD"))
  }
  
  print(res_print)
}


#####################


# out_np_k3 <- generate_RGA_test_results(dat_id=dat_np_k3, folds=folds5, model_type="GLMNET_ORDINAL", alpha=0.2)
# out_k3 <- generate_RGA_test_results(dat_id=dat_k3, folds=folds5, model_type="GLMNET_ORDINAL", alpha=0.2)
# out_nrs_k3 <- generate_RGA_test_results(dat_id=dat_nrs_k3, folds=folds5, model_type="GLMNET_ORDINAL", alpha=0.2)

# save(out_np_k3, file="test_out_np_k3.Rdata")
# save(out_k3, file="test_out_k3.Rdata")
# save(out_nrs_k3, file="test_out_nrs_k3.Rdata")


#####################


# out_np_k2sp <- generate_RGA_test_results(dat_id=dat_np_k2sp, folds=folds5, model_type="GLMNET", alpha=0.8, PRINT_PROGRESS=F)
# out_k2sp <- generate_RGA_test_results(dat_id=dat_k2sp, folds=folds5, model_type="GLMNET", alpha=0.8, PRINT_PROGRESS=F)
# out_nrs_k2sp <- generate_RGA_test_results(dat_id=dat_nrs_k2sp, folds=folds5, model_type="GLMNET", alpha=0.8, PRINT_PROGRESS=F)

# save(out_np_k2sp, file="test_out_np_k2sp.Rdata")
# save(out_k2sp, file="test_out_k2sp.Rdata")
# save(out_nrs_k2sp, file="test_out_nrs_k2sp.Rdata")


#####################


# out_np_k2p <- generate_RGA_test_results(dat_id=dat_np_k2p, folds=folds5, model_type="GLMNET", alpha=0.8, PRINT_PROGRESS=F)
# out_k2p <- generate_RGA_test_results(dat_id=dat_k2p, folds=folds5, model_type="GLMNET", alpha=0.8, PRINT_PROGRESS=F)
# out_nrs_k2p <- generate_RGA_test_results(dat_id=dat_nrs_k2p, folds=folds5, model_type="GLMNET", alpha=0.8, PRINT_PROGRESS=F)

# save(out_np_k2p, file="test_out_np_k2p.Rdata")
# save(out_k2p, file="test_out_k2p.Rdata")
# save(out_nrs_k2p, file="test_out_nrs_k2p.Rdata")

#####################


# out_nrsq_k3 <- generate_RGA_test_results(dat_id=dat_nrsq_k3, folds=folds5, model_type="GLMNET_ORDINAL", alpha=0.2, PRINT_PROGRESS=F)
# out_nrsq_k2sp <- generate_RGA_test_results(dat_id=dat_nrsq_k2sp, folds=folds5, model_type="GLMNET", alpha=0.8, PRINT_PROGRESS=F)
# out_nrsq_k2p <- generate_RGA_test_results(dat_id=dat_nrsq_k2p, folds=folds5, model_type="GLMNET", alpha=0.8, PRINT_PROGRESS=F)
# 
# save(out_nrsq_k3, file="test_out_nrsq_k3.Rdata")
# save(out_nrsq_k2sp, file="test_out_nrsq_k2sp.Rdata")
# save(out_nrsq_k2p, file="test_out_nrsq_k2p.Rdata")

#####################


# out_lf_k3 <- generate_RGA_test_results(dat_id=dat_lf_k3, folds=folds5, model_type="GLMNET_ORDINAL", alpha=0.2, PRINT_PROGRESS=F)
# out_lf_k2sp <- generate_RGA_test_results(dat_id=dat_lf_k2sp, folds=folds5, model_type="GLMNET", alpha=0.8, PRINT_PROGRESS=F)
# out_lf_k2p <- generate_RGA_test_results(dat_id=dat_lf_k2p, folds=folds5, model_type="GLMNET", alpha=0.8, PRINT_PROGRESS=F)
# 
# save(out_lf_k3, file="test_out_lf_k3.Rdata")
# save(out_lf_k2sp, file="test_out_lf_k2sp.Rdata")
# save(out_lf_k2p, file="test_out_lf_k2p.Rdata")



#####################



out_nw_k3 <- generate_RGA_test_results(dat_id=dat_nw_k3, folds=folds5, model_type="GLMNET_ORDINAL", alpha=0.2, PRINT_PROGRESS=F)
out_nw_p_k3<- generate_RGA_test_results(dat_id=dat_nw_p_k3, folds=folds5, model_type="GLMNET_ORDINAL", alpha=0.2, PRINT_PROGRESS=F)
out_nw_nrs_k3 <- generate_RGA_test_results(dat_id=dat_nw_nrs_k3, folds=folds5, model_type="GLMNET_ORDINAL", alpha=0.2, PRINT_PROGRESS=F)
out_nw_nrsq_k3 <- generate_RGA_test_results(dat_id=dat_nw_nrsq_k3, folds=folds5, model_type="GLMNET_ORDINAL", alpha=0.2, PRINT_PROGRESS=F)
out_nw_pq_k3 <- generate_RGA_test_results(dat_id=dat_nw_pq_k3, folds=folds5, model_type="GLMNET_ORDINAL", alpha=0.2, PRINT_PROGRESS=F)

res_agg_nw_k3 <- resultAggregatorNewMethod(out_nw_k3)
res_agg_nw_p_k3 <- resultAggregatorNewMethod(out_nw_p_k3)
res_agg_nw_nrs_k3 <- resultAggregatorNewMethod(out_nw_nrs_k3)
res_agg_nw_nrsq_k3 <- resultAggregatorNewMethod(out_nw_nrsq_k3)
res_agg_nw_pq_k3 <- resultAggregatorNewMethod(out_nw_pq_k3)




out_nw_k2sp <- generate_RGA_test_results(dat_id=dat_nw_k2sp, folds=folds5, model_type="GLMNET", alpha=0.8, PRINT_PROGRESS=F)
out_nw_p_k2sp<- generate_RGA_test_results(dat_id=dat_nw_p_k2sp, folds=folds5, model_type="GLMNET", alpha=0.8, PRINT_PROGRESS=F)
out_nw_nrs_k2sp <- generate_RGA_test_results(dat_id=dat_nw_nrs_k2sp, folds=folds5, model_type="GLMNET", alpha=0.8, PRINT_PROGRESS=F)
out_nw_nrsq_k2sp <- generate_RGA_test_results(dat_id=dat_nw_nrsq_k2sp, folds=folds5, model_type="GLMNET", alpha=0.8, PRINT_PROGRESS=F)
out_nw_pq_k2sp <- generate_RGA_test_results(dat_id=dat_nw_pq_k2sp, folds=folds5, model_type="GLMNET", alpha=0.8, PRINT_PROGRESS=F)

res_agg_nw_k2sp <- resultAggregatorNewMethod(out_nw_k2sp)
res_agg_nw_p_k2sp <- resultAggregatorNewMethod(out_nw_p_k2sp)
res_agg_nw_nrs_k2sp <- resultAggregatorNewMethod(out_nw_nrs_k2sp)
res_agg_nw_nrsq_k2sp <- resultAggregatorNewMethod(out_nw_nrsq_k2sp)
res_agg_nw_pq_k2sp <- resultAggregatorNewMethod(out_nw_pq_k2sp)




out_nw_k2p <- generate_RGA_test_results(dat_id=dat_nw_k2p, folds=folds5, model_type="GLMNET", alpha=0.8, PRINT_PROGRESS=F)
out_nw_p_k2p<- generate_RGA_test_results(dat_id=dat_nw_p_k2p, folds=folds5, model_type="GLMNET", alpha=0.8, PRINT_PROGRESS=F)
out_nw_nrs_k2p <- generate_RGA_test_results(dat_id=dat_nw_nrs_k2p, folds=folds5, model_type="GLMNET", alpha=0.8, PRINT_PROGRESS=F)
out_nw_nrsq_k2p <- generate_RGA_test_results(dat_id=dat_nw_nrsq_k2p, folds=folds5, model_type="GLMNET", alpha=0.8, PRINT_PROGRESS=F)
out_nw_pq_k2p <- generate_RGA_test_results(dat_id=dat_nw_pq_k2p, folds=folds5, model_type="GLMNET", alpha=0.8, PRINT_PROGRESS=F)

res_agg_nw_k2p <- resultAggregatorNewMethod(out_nw_k2p)
res_agg_nw_p_k2p <- resultAggregatorNewMethod(out_nw_p_k2p)
res_agg_nw_nrs_k2p <- resultAggregatorNewMethod(out_nw_nrs_k2p)
res_agg_nw_nrsq_k2p <- resultAggregatorNewMethod(out_nw_nrsq_k2p)
res_agg_nw_pq_k2p <- resultAggregatorNewMethod(out_nw_pq_k2p)



#####################



# load("test_out_k2sp.Rdata")
# load("test_out_np_k2sp.Rdata")
# load("test_out_nrs_k2sp.Rdata")
# load("test_out_nrsq_k2sp.Rdata")
# 
# load("test_out_k2p.Rdata")
# load("test_out_np_k2p.Rdata")
# load("test_out_nrs_k2p.Rdata")
# load("test_out_nrsq_k2p.Rdata")
# 
# load("test_out_k3.Rdata")
# load("test_out_np_k3.Rdata")
# load("test_out_nrs_k3.Rdata")
# load("test_out_nrsq_k3.Rdata")




# res_agg_np_k3 <- resultAggregatorNewMethod(out_np_k3)
# res_agg_k3 <- resultAggregatorNewMethod(out_k3)
# res_agg_nrs_k3 <- resultAggregatorNewMethod(out_nrs_k3)
# res_agg_nrsq_k3 <- resultAggregatorNewMethod(out_nrsq_k3)
# 
# 
# res_agg_np_k2sp <- resultAggregatorNewMethod(out_np_k2sp)
# res_agg_k2sp <- resultAggregatorNewMethod(out_k2sp)
# res_agg_nrs_k2sp <- resultAggregatorNewMethod(out_nrs_k2sp)
# res_agg_nrsq_k2sp <- resultAggregatorNewMethod(out_nrsq_k2sp)
# 
# 
# res_agg_np_k2p <- resultAggregatorNewMethod(out_np_k2p)
# res_agg_k2p <- resultAggregatorNewMethod(out_k2p)
# res_agg_nrs_k2p <- resultAggregatorNewMethod(out_nrs_k2p)
# res_agg_nrsq_k2p <- resultAggregatorNewMethod(out_nrsq_k2p)
# 
# 
# res_agg_lf_k3 <- resultAggregatorNewMethod(out_lf_k3)
# res_agg_lf_k2sp <- resultAggregatorNewMethod(out_lf_k2sp)
# res_agg_lf_k2p <- resultAggregatorNewMethod(out_lf_k2p)






# res_list <- list(res_agg_np_k3[[2]], res_agg_k3[[2]], res_agg_nrs_k3[[2]], res_agg_nrsq_k3[[2]])
# for(i in 1:len(res_list)) {
#   res <- res_list[[i]]
#   res_mat1 <- matrix(unl(lapply(res, function(x) x[,1])), ncol=2, byrow=T)
#   res_mat2 <- matrix(unl(lapply(res, function(x) x[,2])), ncol=2, byrow=T)
#   res_mat3 <- matrix(unl(lapply(res, function(x) x[,3])), ncol=2, byrow=T)
#   res_mat4 <- matrix(unl(lapply(res, function(x) x[,4])), ncol=2, byrow=T)
#   res_mat5 <- matrix(unl(lapply(res, function(x) x[,5])), ncol=2, byrow=T)
#   res_mat6 <- matrix(unl(lapply(res, function(x) x[,6])), ncol=2, byrow=T)
#   
#   # mxi <- 60 
#   mxi <- which.max(res_mat6[,1])
#   
#   cat("DATA SUBSET", LETTERS[i], "\n")
#   
#   cat("KAPPA: \t\t", paste(round(res_mat1[mxi, ],3), collapse=", "), "\n",
#       "ACCURACY: \t", paste(round(res_mat2[mxi, ],3), collapse=", "), "\n",
#       "AUC_0v1: \t", paste(round(res_mat3[mxi, ]/c(1, sqrt(10)), 3), collapse=", "), "\n",
#       "AUC_0v2: \t", paste(round(res_mat4[mxi, ]/c(1, sqrt(10)), 3), collapse=", "), "\n",
#       "AUC_1v2: \t", paste(round(res_mat5[mxi, ]/c(1, sqrt(10)), 3), collapse=", "), "\n",
#       "AUC_mean: \t", paste(round(res_mat6[mxi, ]/c(1, sqrt(10)), 3), collapse=", "), "\n\n", sep="")
# }









# res_list <- list(res_agg_np_k2sp[[2]], res_agg_k2sp[[2]], res_agg_nrs_k2sp[[2]], res_agg_nrsq_k2sp[[2]])
# for(i in 1:len(res_list)) {
#   res <- res_list[[i]]
#   res_mat1 <- matrix(unl(lapply(res, function(x) x[,1])), ncol=2, byrow=T)
#   res_mat2 <- matrix(unl(lapply(res, function(x) x[,2])), ncol=2, byrow=T)
#   res_mat3 <- matrix(unl(lapply(res, function(x) x[,3])), ncol=2, byrow=T)
#   
#   mxi <- 68
#   # mxi <- which.max(res_mat3[,1])
#   
#   cat("DATA SUBSET", LETTERS[i], "\n")
#   
#   cat("KAPPA: \t\t", paste(round(res_mat1[mxi, ],3), collapse=", "), "\n",
#       "ACCURACY: \t", paste(round(res_mat2[mxi, ],3), collapse=", "), "\n",
#       "AUC_0v12: \t", paste(round(res_mat3[mxi, ]/c(1, sqrt(10)), 3), collapse=", "), "\n\n", sep="")
#   
#   print(mxi)
# }






# res_list <- list(res_agg_np_k2p[[2]], res_agg_k2p[[2]], res_agg_nrs_k2p[[2]], res_agg_nrsq_k2p[[2]])
# for(i in 1:len(res_list)) {
#   res <- res_list[[i]]
#   res_mat1 <- matrix(unl(lapply(res, function(x) x[,1])), ncol=2, byrow=T)
#   res_mat2 <- matrix(unl(lapply(res, function(x) x[,2])), ncol=2, byrow=T)
#   res_mat3 <- matrix(unl(lapply(res, function(x) x[,3])), ncol=2, byrow=T)
#   
#   # mxi <- 78
#   mxi <- which.max(res_mat3[,1])
#   
#   cat("DATA SUBSET", LETTERS[i], "\n")
#   
#   cat("KAPPA: \t\t", paste(round(res_mat1[mxi, ],3), collapse=", "), "\n",
#       "ACCURACY: \t", paste(round(res_mat2[mxi, ],3), collapse=", "), "\n",
#       "AUC_01v2: \t", paste(round(res_mat3[mxi, ]/c(1, sqrt(10)), 3), collapse=", "), "\n\n", sep="")
#   
#   # print(mxi)
# }




################################################################################3


# res_list <- list(res_agg_lf_k3[[2]])
# for(i in 1:len(res_list)) {
#   res <- res_list[[i]]
#   res_mat1 <- matrix(unl(lapply(res, function(x) x[,1])), ncol=2, byrow=T)
#   res_mat2 <- matrix(unl(lapply(res, function(x) x[,2])), ncol=2, byrow=T)
#   res_mat3 <- matrix(unl(lapply(res, function(x) x[,3])), ncol=2, byrow=T)
#   res_mat4 <- matrix(unl(lapply(res, function(x) x[,4])), ncol=2, byrow=T)
#   res_mat5 <- matrix(unl(lapply(res, function(x) x[,5])), ncol=2, byrow=T)
#   res_mat6 <- matrix(unl(lapply(res, function(x) x[,6])), ncol=2, byrow=T)
#   
#   # mxi <- 60 
#   mxi <- which.max(res_mat6[,1])
#   
#   cat("DATA SUBSET", "E", "\n")
#   
#   cat("KAPPA: \t\t", paste(round(res_mat1[mxi, ],3), collapse=", "), "\n",
#       "ACCURACY: \t", paste(round(res_mat2[mxi, ],3), collapse=", "), "\n",
#       "AUC_0v1: \t", paste(round(res_mat3[mxi, ]/c(1, sqrt(10)), 3), collapse=", "), "\n",
#       "AUC_0v2: \t", paste(round(res_mat4[mxi, ]/c(1, sqrt(10)), 3), collapse=", "), "\n",
#       "AUC_1v2: \t", paste(round(res_mat5[mxi, ]/c(1, sqrt(10)), 3), collapse=", "), "\n",
#       "AUC_mean: \t", paste(round(res_mat6[mxi, ]/c(1, sqrt(10)), 3), collapse=", "), "\n\n", sep="")
# }




# res_list <- list(res_agg_lf_k2sp[[2]])
# for(i in 1:len(res_list)) {
#   res <- res_list[[i]]
#   res_mat1 <- matrix(unl(lapply(res, function(x) x[,1])), ncol=2, byrow=T)
#   res_mat2 <- matrix(unl(lapply(res, function(x) x[,2])), ncol=2, byrow=T)
#   res_mat3 <- matrix(unl(lapply(res, function(x) x[,3])), ncol=2, byrow=T)
#   
#   mxi <- 68
#   # mxi <- which.max(res_mat3[,1])
#   
#   cat("DATA SUBSET", "E", "\n")
#   
#   cat("KAPPA: \t\t", paste(round(res_mat1[mxi, ],3), collapse=", "), "\n",
#       "ACCURACY: \t", paste(round(res_mat2[mxi, ],3), collapse=", "), "\n",
#       "AUC_0v12: \t", paste(round(res_mat3[mxi, ]/c(1, sqrt(10)), 3), collapse=", "), "\n\n", sep="")
#   
#   print(mxi)
# }






# res_list <- list(res_agg_lf_k2p[[2]])
# for(i in 1:len(res_list)) {
#   res <- res_list[[i]]
#   res_mat1 <- matrix(unl(lapply(res, function(x) x[,1])), ncol=2, byrow=T)
#   res_mat2 <- matrix(unl(lapply(res, function(x) x[,2])), ncol=2, byrow=T)
#   res_mat3 <- matrix(unl(lapply(res, function(x) x[,3])), ncol=2, byrow=T)
#   
#   # mxi <- 78
#   mxi <- which.max(res_mat3[,1])
#   
#   cat("DATA SUBSET", "E", "\n")
#   
#   cat("KAPPA: \t\t", paste(round(res_mat1[mxi, ],3), collapse=", "), "\n",
#       "ACCURACY: \t", paste(round(res_mat2[mxi, ],3), collapse=", "), "\n",
#       "AUC_01v2: \t", paste(round(res_mat3[mxi, ]/c(1, sqrt(10)), 3), collapse=", "), "\n\n", sep="")
#   
#   # print(mxi)
# }






############################################################





res_list <- list(res_agg_nw_k3[[2]], res_agg_nw_p_k3[[2]], res_agg_nw_nrs_k3[[2]], res_agg_nw_nrsq_k3[[2]], res_agg_nw_pq_k3[[2]])
for(i in 1:len(res_list)) {
  res <- res_list[[i]]
  res_mat1 <- matrix(unl(lapply(res, function(x) x[,1])), ncol=2, byrow=T)
  res_mat2 <- matrix(unl(lapply(res, function(x) x[,2])), ncol=2, byrow=T)
  res_mat3 <- matrix(unl(lapply(res, function(x) x[,3])), ncol=2, byrow=T)
  res_mat4 <- matrix(unl(lapply(res, function(x) x[,4])), ncol=2, byrow=T)
  res_mat5 <- matrix(unl(lapply(res, function(x) x[,5])), ncol=2, byrow=T)
  res_mat6 <- matrix(unl(lapply(res, function(x) x[,6])), ncol=2, byrow=T)

  mxi <- 55
  # mxi <- which.max(res_mat6[,1])

  cat("DATA SUBSET", LETTERS[i], "\n")

  # cat("KAPPA: \t\t", paste(round(res_mat1[mxi, ],3), collapse=", "), "\n",
      # "ACCURACY: \t", paste(round(res_mat2[mxi, ],3), collapse=", "), "\n",
  cat("AUC_0v1: \t", paste(round(res_mat3[mxi, ]/c(1, sqrt(10)), 3), collapse=" ("), ")\n",
      "AUC_0v2: \t", paste(round(res_mat4[mxi, ]/c(1, sqrt(10)), 3), collapse=" ("), ")\n",
      "AUC_1v2: \t", paste(round(res_mat5[mxi, ]/c(1, sqrt(10)), 3), collapse=" ("), ")\n",
      "AUC_mean: \t", paste(round(res_mat6[mxi, ]/c(1, sqrt(10)), 3), collapse=" ("), ")\n\n", sep="")
}




res_list <- list(res_agg_nw_k2sp[[2]], res_agg_nw_p_k2sp[[2]], res_agg_nw_nrs_k2sp[[2]], res_agg_nw_nrsq_k2sp[[2]], res_agg_nw_pq_k2sp[[2]])
for(i in 1:len(res_list)) {
  res <- res_list[[i]]
  res_mat1 <- matrix(unl(lapply(res, function(x) x[,1])), ncol=2, byrow=T)
  res_mat2 <- matrix(unl(lapply(res, function(x) x[,2])), ncol=2, byrow=T)
  res_mat3 <- matrix(unl(lapply(res, function(x) x[,3])), ncol=2, byrow=T)

  mxi <- 67
  # mxi <- which.max(res_mat3[,1])

  cat("DATA SUBSET", LETTERS[i], "\n")

  cat("KAPPA: \t\t", paste(round(res_mat1[mxi, ],3), collapse=", "), "\n",
      "ACCURACY: \t", paste(round(res_mat2[mxi, ],3), collapse=", "), "\n",
      "AUC_0v12: \t", paste(round(res_mat3[mxi, ]/c(1, sqrt(10)), 3), collapse=" ("), ")\n\n", sep="")

  # print(mxi)
}






res_list <- list(res_agg_nw_k2p[[2]], res_agg_nw_p_k2p[[2]], res_agg_nw_nrs_k2p[[2]], res_agg_nw_nrsq_k2p[[2]], res_agg_nw_pq_k2p[[2]])
for(i in 1:len(res_list)) {
  res <- res_list[[i]]
  res_mat1 <- matrix(unl(lapply(res, function(x) x[,1])), ncol=2, byrow=T)
  res_mat2 <- matrix(unl(lapply(res, function(x) x[,2])), ncol=2, byrow=T)
  res_mat3 <- matrix(unl(lapply(res, function(x) x[,3])), ncol=2, byrow=T)

  mxi <- 73
  # mxi <- which.max(res_mat3[,1])

  cat("DATA SUBSET", LETTERS[i], "\n")

  cat("KAPPA: \t\t", paste(round(res_mat1[mxi, ],3), collapse=", "), "\n",
      "ACCURACY: \t", paste(round(res_mat2[mxi, ],3), collapse=", "), "\n",
      "AUC_01v2: \t", paste(round(res_mat3[mxi, ]/c(1, sqrt(10)), 3), collapse=" ("), ")\n\n", sep="")

  # print(mxi)
}



