source("../useful_funs.r")


############################################
####  Open summary CSV with UW results  ####
############################################


con <- file("~/HealthQ/RGA/CSVResults_without_notes.csv")
flines <- readLines(con)


fline_list <- sapply(flines, function(x) strsplit(x, ","))

uw <- matrix(unlist(fline_list[-1]), ncol=111, byrow=T)
colnames(uw) <- fline_list[[1]]

# Extact final overall, pref and super-pref columns
uw_overall <- uw[,grep("FINAL_overall", colnames(uw))]
uw_pref <- uw[,grep("FINAL_preferred", colnames(uw))]
uw_spref <- uw[,grep("FINAL_super-preferred", colnames(uw))]




#############################################
####  Refactor final overall UW results  ####
#############################################

# Create new refactored overall UW variable 'uw_overall2'
#   0 -- standard
#   1 -- rated
#   2 -- not accepted
#  99 -- undefined / need more info
uw_overall2 <- integer(len(uw_overall))

for(i in 1:len(uw_overall)) {
  first <- strsplit(uw_overall[i], " ")[[1]][1]
  if(first == "Standard") uw_overall2[i] <- 0
  else if(first == "Rated") uw_overall2[i] <- 1
  else if(first == "Not") uw_overall2[i] <- 2
  
  # Exceptions (standard)
  else if(uw_overall[i] == "Need more info re cancer; but otherwise standard") 
    uw_overall2[i] <- 0
  
  # Exceptions (Rated)
  else if((first == "Rateable") ||
          (uw_overall[i] == "Need more info from family doctor (high rating or risk not accepted)"))
  uw_overall2[i] <- 1
  
  # All other marked "undefined"
  else uw_overall2[i] <- 99
}



############################################
####  Refactor final preferred results  ####
############################################

# Create new refactored preferred UW variable 'uw_pref2'
#   0 -- super-preferred
#   1 -- preferred
#   2 -- no preference
#   5 -- preferred, but not super-pref due to family history
#   6 -- no pref, but not preferred due to family history
#  99 -- else / reevaluate
uw_pref2 <- integer(len(uw_pref))

for(i in 1:len(uw_pref)) {
  
  f_pref <- strsplit(uw_pref[i], " ")[[1]][1]
  f_spref <- strsplit(uw_spref[i], " ")[[1]][1]
  
  pref_family <- grep("Family", strsplit(uw_pref[i], " ")[[1]], ignore.case=T)
  spref_family <- grep("Family", strsplit(uw_spref[i], " ")[[1]], ignore.case=T)
  
  if(f_spref == "Yes")
    uw_pref2[i] <- 0
  else if((f_pref == "Yes") & (len(spref_family) > 0))
    
    uw_pref2[i] <- 5
  else if((f_pref == "Yes") & (len(spref_family) == 0))
    uw_pref2[i] <- 1
  else if((f_pref == "No") & (len(pref_family) > 0))
    uw_pref2[i] <- 6
  else if((f_pref == "No") & (len(pref_family) == 0))
    uw_pref2[i] <- 2
  
  
  # Exceptions
  else if(f_spref == "Yes;")
    uw_pref2[i] <- 0
  else if(uw_pref[i] == "Need more info re cancer; maybe accepted for preferred")
    uw_pref2[i] <- 1
  
  else
    uw_pref2[i] <- 99
  
}

# Create new refactored preferred UW variable 'uw_pref3'
#   0 -- super-preferred
#   1 -- preferred
#   2 -- no preference
uw_pref3 <- uw_pref2
uw_pref3[uw_pref3 == 5] <- 1
uw_pref3[uw_pref3 == 6] <- 2


uw_pref4 <- uw_pref2
uw_pref4[uw_pref4 == 5] <- 0
uw_pref4[uw_pref4 == 6] <- 1





