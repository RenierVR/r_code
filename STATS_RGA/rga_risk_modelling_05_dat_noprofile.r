source("rga_risk_modelling_01_dat.r")
source("rga_risk_modelling_02_model_funs.r")

library(glmnet)
library(glmnetcr)
library(randomForest)
library(ggplot2)
library(gridExtra)


#######################################
####  Data for modelling  #############
#######################################

nms_all <- names(dat_all)
nms_allnp <- names(dat_all_noprof)
nms_rga <- names(dat_rga)
nms_rganp <- names(dat_rga_noprof)


dat_np_k3 <- dat_all_noprof[, c(match(c("UW_K3", "BIO_SEX", "BIO_AGE"), nms_allnp), 
                                grep("ACTIVITY.sessionSteps_mean", nms_allnp):ncol(dat_all_noprof))] 

# Remove "HRV" features
i_hrv <- grep("HRV", names(dat_np_k3))
i_hrv <- i_hrv[1:which(diff(i_hrv) > 1)[1]]
dat_np_k3 <- dat_np_k3[, -i_hrv]

# Remove columns with more than specified nr of NAs
dat_np_k3 <- dat_np_k3[, which(count_NA(dat_np_k3) <= 111)]

# Set of 2 class data subsets
dat_np_k2sp <- cb(dat_all_noprof[grep("UW_K2_sp", nms_allnp)], dat_np_k3[, -1])
dat_np_k2p <- cb(dat_all_noprof[grep("UW_K2_p", nms_allnp)], dat_np_k3[, -1])


# All rows with missing values should automatically be removed in modelling functions
# dat_np_k3 <- dat_np_k3[!apply(dat_np_k3, 1, function(x) any(is.na(x))), ]

# sum(apply(dat_np_k3, 1, function(x) any(is.na(x))))


folds <- read.table("./stratified_folds.csv", sep=",", head=T)



##################################
####  GLMNET models  #############
##################################


# out <- runSupervisedCV(dat_np_k3, "GLMNET")

seq_alpha <- round(seq(0, 1, by=0.1), 1)

# out_glmnet_alphalist_np_k3 <- vector("list", len(seq_alpha))
# for(a in 1:len(seq_alpha))
#   out_glmnet_alphalist_np_k3[[a]] <- runSupervisedCV_repeats(n=10, dat_np_k3, model_type = "GLMNET", alpha = seq_alpha[a])


# out_glmnet_alphalist_np_k2sp <- vector("list", len(seq_alpha))
# for(a in 1:len(seq_alpha))
#   out_glmnet_alphalist_np_k2sp[[a]] <- runSupervisedCV_repeats(n=10, dat_np_k2sp, model_type = "GLMNET", alpha = seq_alpha[a])
# 
# 
# out_glmnet_alphalist_np_k2p <- vector("list", len(seq_alpha))
# for(a in 1:len(seq_alpha))
#   out_glmnet_alphalist_np_k2p[[a]] <- runSupervisedCV_repeats(n=10, dat_np_k2p, model_type = "GLMNET", alpha = seq_alpha[a])


# save(out_glmnet_alphalist_np_k3, file = "out_glmnet_alphalist_np_k3.Rdata")
# save(out_glmnet_alphalist_np_k2sp, file = "out_glmnet_alphalist_np_k2sp.Rdata")
# save(out_glmnet_alphalist_np_k2p, file = "out_glmnet_alphalist_np_k2p.Rdata")



delims <- paste0("_alpha", c(paste0("0", 0:9), "10"))
for(i in 1:len(seq_alpha)) {
  res_agg <- resultAggregator(out_glmnet_alphalist_np_k2sp[[i]])
  plotAggResults(res_agg, output_path_delim = delims[i])
}










##########################################
####  GLMNET-ORDINAL models  #############
##########################################


# out <- runSupervisedCV(dat_np_k3, "GLMNET_ORDINAL")

seq_alpha <- round(seq(0, 1, by=0.1), 1)

# out_glmnet_ord_alphalist_np_k3 <- vector("list", len(seq_alpha))
# for(a in 11:11)
#   out_glmnet_ord_alphalist_np_k3[[a]] <- runSupervisedCV_repeats(n=10, dat_np_k3, model_type = "GLMNET_ORDINAL", alpha = seq_alpha[a])


# save(out_glmnet_ord_alphalist_np_k3, file = "out_glmnet_ord_alphalist_np_k3.Rdata")


delims <- paste0("_alpha", c(paste0("0", 0:9), "10"))
for(i in 1:len(seq_alpha)) {
  res_agg <- resultAggregator(out_glmnet_ord_alphalist_np_k3[[i]])
  plotAggResults(res_agg, output_path_delim = delims[i])
}




##########################################
####  Random Forest models   #############
##########################################


seq_nodesizes <- 1:10

out_rf_nsizelist_np_k3 <- vector("list", len(seq_alpha))
for(a in 1:len(seq_alpha))
  out_rf_nsizelist_np_k3[[a]] <- runSupervisedCV_repeats(n=10, dat_np_k3, model_type = "RF", ntree = 2500, nodesize = seq_nodesizes[a])


out_rf_nsizelist_np_k2sp <- vector("list", len(seq_alpha))
for(a in 1:len(seq_alpha))
  out_rf_nsizelist_np_k2sp[[a]] <- runSupervisedCV_repeats(n=10, dat_np_k2sp, model_type = "RF", ntree = 2500, nodesize = seq_nodesizes[a])


out_rf_nsizelist_np_k2p <- vector("list", len(seq_alpha))
for(a in 1:len(seq_alpha))
  out_rf_nsizelist_np_k2p[[a]] <- runSupervisedCV_repeats(n=10, dat_np_k2p, model_type = "RF", ntree = 2500, nodesize = seq_nodesizes[a])


save(out_rf_nsizelist_np_k3, file = "out_rf_nsizelist_np_k3.Rdata")
save(out_rf_nsizelist_np_k2sp, file = "out_rf_nsizelist_np_k2sp.Rdata")
save(out_rf_nsizelist_np_k2p, file = "out_rf_nsizelist_np_k2p.Rdata")

