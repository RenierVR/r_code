source("../useful_funs.r")
library(readxl)    

#############################################
####  Open summary XLSX with UW results  ####
#############################################

# fpath <- "~/HealthQ/RGA/SUBJECT_INFO/UW_Questionnaire_Bio_results_all.xlsx"
fpath <- "~/HealthQ/RGA/SUBJECT_INFO/Summary/20170704_UW_Questionnaire_Bio_results_all_manuallycorrectedwithRGAreadiness.xlsx"

fsheets <- list(read_excel(fpath, sheet = "Underwriting"),
                read_excel(fpath, sheet = "Questionnaire", col_types=rep("text", 144)),
                read_excel(fpath, sheet = "Biometric", col_types=rep("text", 21)))



f1 <- fsheets[[1]]
f2 <- fsheets[[2]]
f3 <- fsheets[[3]]





####################################################
####  Create single-sheet CSV from summary file ####
####################################################

fcsv_df <- cb(fsheets[[3]][, match(c("Anonymous_email", 
                                     "DBN ID",
                                     "ANON ID",
                                     "Participant AGE (years):",
                                     "Participant GENDER:",
                                     "HEIGHT (in centimeters):",
                                     "WEIGHT (in kilograms)",
                                     "Blood pressure (Systolic)",
                                     "Blood pressure (Diastolic)",
                                     "Resting heart rate",
                                     "AVERAGE PeFR (L/min)"), names(fsheets[[3]]))],
              fsheets[[1]][, match(c("Anonymous_email", 
                                     "Cohort",
                                     "UW_Comment.Smoker",
                                     "FINAL_Nurse-exam",
                                     "FINAL_Blood-test",
                                     "FINAL_short-form",
                                     "FINAL_long-form",
                                     "FINAL_overall",
                                     "FINAL_preferred",
                                     "FINAL_super-preferred"), names(fsheets[[1]]))])

# fcsv_df[,1] %=% fcsv_df[,12]

# Change col order and names
fcsv_df <- fcsv_df[, c(1:3, 13, 5, 4, 6:11, 14:21)]
names(fcsv_df) <- c("ID_Anon_email", "ID_DBN", "ID_ANON", "ID_Cohort",
                    "BIOMETRIC_Gender", "BIOMETRIC_Age", "BIOMETRIC_Height", "BIOMETRIC_Weight", 
                    "BIOMETRIC_BP_systolic_pilot", "BIOMETRIC_BP_diastolic_pilot", 
                    "BIOMETRIC_RHR", "BIOMETRIC_PeFR_average", "BIOMETRIC_SmokeStatus",
                    "UWFINAL_NurseExam", "UWFINAL_BloodTest",
                    "UWFINAL_short_form", "UWFINAL_long_form",
                    "UWFINAL_overall", "UWFINAL_preferred", "UWFINAL_superpreferred")


# Replace ',' with ';' in text cols
fcsv_df[, 14:20] <- apply(fcsv_df[, 14:20], 2, function(x) gsub(",", ";", x))

# Cast numeric columns as such
fcsv_df[, 6:12] <- apply(fcsv_df[, 6:12], 2, as.numeric)

# Refactor smoking status
fcsv_df[grep("Never", fcsv_df[,13]), 13] <- "No"
fcsv_df[grep("Smoker", fcsv_df[,13]), 13] <- "Yes"
fcsv_df[grep("<1y", fcsv_df[,13]), 13] <- "Ex1"
fcsv_df[grep("1-5y", fcsv_df[,13]), 13] <- "Ex2"
fcsv_df[grep(">5y", fcsv_df[,13]), 13] <- "Ex3"



###############################
####  Read SubMax results  ####
###############################

dat_sub <- read_excel("~/HealthQ/RGA/LE/VO2Submax results pulled (from API) on 5 May 2017.xlsx")

dat_sub_id <- tolower(dat_sub[,1])
dat_sub_val <- rep(NA, nrow(dat_sub))
for(i in which(dat_sub[,2] > 0)) {
  tmp_val <- na.omit(as.numeric(dat_sub[i, -c(1:2)]))
  dat_sub_val[i] <- mean(tmp_val[tmp_val != 0])
}
dat_sub_val[c(which(dat_sub_val == 0), which(is.nan(dat_sub_val)))] <- NA

submax_res_matched <- dat_sub_val[match(fcsv_df[,1], dat_sub_id)]




####################################################
####  Add new underwriting and BP to fcsv_df  ######
####################################################

uw_updated <- read_excel("~/HealthQ/RGA/SUBJECT_INFO/RGA HQ Cohort BP retest 050617 - RAR.xlsx", 
                         sheet = "HQ Cohort BP measurements")


id_match <- match(tolower(uw_updated[,1]), fcsv_df[,1])
id_match_na <- which(is.na(id_match))

# Ensure pilot (old) diastolic/diastolic values are equal
# apply(cb(uw_updated[-id_match_na, 4], fcsv_df$`BIOMETRIC_BP-systolic`[id_match[-id_match_na]]), 1, diff) %=% 0
# apply(cb(uw_updated[-id_match_na, 5], fcsv_df$`BIOMETRIC_BP-diastolic`[id_match[-id_match_na]]), 1, diff) %=% 0

id_match2 <- match(fcsv_df[,1], tolower(uw_updated[,1]))

# Ensure matching works as intended
# aa <- rep(NA, nrow(fcsv_df))
# aa[!is.na(id_match2)] <- uw_updated[na.omit(id_match2), 1]

new_bp <- matrix(NA, nr=nrow(fcsv_df), nc=2)
new_bp[!is.na(id_match2), 1] <- uw_updated[na.omit(id_match2), 6]
new_bp[!is.na(id_match2), 2] <- uw_updated[na.omit(id_match2), 7]

new_uw <- rep(NA, nrow(fcsv_df))
new_uw[!is.na(id_match2)] <- uw_updated[na.omit(id_match2), 3]

new_uw2 <- na.omit(new_uw)


which_spref <- which(unlist(lapply(strsplit(new_uw2, " "), function(x) x[[1]])) == "Super-preferred")
which_pref <- which(unlist(lapply(strsplit(new_uw2, " "), function(x) x[[1]])) == "Preferred")
which_standard <- which(unlist(lapply(strsplit(new_uw2, " "), function(x) x[[1]])) == "Standard")
which_rated <- sort(c(which(unlist(lapply(strsplit(new_uw2, " "), function(x) (x[[1]])) %in% c("Rated", "Rated?"))),
                      c(grep("rated", new_uw2), grep("rating", new_uw2))))

# new_uw2[-c(which_spref, which_pref, which_standard, which_rated)]

# New overall UW - digitised
new_uw_overall <- rep(99, len(new_uw2))
new_uw_overall[c(which_spref, which_pref, which_standard)] <- 0
new_uw_overall[which_rated] <- 1

# New pref UW - digitised
new_uw_pref <- rep(2, len(new_uw2))
new_uw_pref[which_spref] <- 0
new_uw_pref[which_pref] <- 1


new_uw3 <- matrix(NA, nr=nrow(fcsv_df), nc=2)
new_uw3[which(!is.na(id_match2)), 1] <- new_uw_overall
new_uw3[which(!is.na(id_match2)), 2] <- new_uw_pref

# cb(fcsv_df, "BIOMETRIC_BP_systolic_retested"=new_bp[,1],
#    "BIOMETRIC_BP_diastolic_pilot"=new_bp[,2],
#    "UWFINAL_UPDATED"=gsub(",", ";", new_uw),
#    "UWDIGITISED_Overall_UPDATED"=new_uw3[,1],
#    "UWDIGITISED_Preferred_UPDATED"=new_uw3[,2])






#############################################
#############################################

f1 <- fsheets[[1]]

# Extact final overall, pref and super-pref columns
uw_overall <- f1[,grep("FINAL_overall", names(f1))]
uw_pref <- f1[,grep("FINAL_preferred", names(f1))]
uw_spref <- f1[,grep("FINAL_super-preferred", names(f1))]



#############################################
####  Refactor final overall UW results  ####
#############################################

# Create new refactored overall UW variable 'uw_overall2'
#   0 -- standard
#   1 -- rated
#   2 -- not accepted
#  99 -- undefined / need more info
uw_overall2 <- integer(len(uw_overall))

for(i in 1:len(uw_overall)) {
  first <- strsplit(uw_overall[i], " ")[[1]][1]
  second <- strsplit(uw_overall[i], " ")[[1]][2]
  if(is.na(second)) second <- ""
  
  if(uw_overall[i] == "Standard rates"){
    uw_overall2[i] <- 0
    # print(uw_overall[i])
  }
  if((first == "Standard") || (first == "?Standard") || (second == "standard")) {
    uw_overall2[i] <- 0
    # print(uw_overall[i])
  }
  else if((first == "Rated") || (first == "Rateable")) {
    uw_overall2[i] <- 1
    # print(uw_overall[i])
  }
  else if(first == "Not") {
    uw_overall2[i] <- 2
    # print(uw_overall[i])
  }
  
  # Exceptions (standard)
  else if((uw_overall[i] == "Need more info re cancer, but otherwise standard"))
    uw_overall2[i] <- 0

  # Exceptions (Rated)
  else if((uw_overall[i] == "Need to re-test BP, +75") ||
          (uw_overall[i] == "Need more info from family doctor (high rating or risk not accepted)"))
    uw_overall2[i] <- 1
  
  # All other marked "undefined"
  else {
    uw_overall2[i] <- 99
    # print(uw_overall[i])
  }

}





############################################
####  Refactor final preferred results  ####
############################################

# Create new refactored preferred UW variable 'uw_pref2'
#   0 -- super-preferred
#   1 -- preferred
#   2 -- no preference
#   5 -- preferred, but not super-pref due to family history
#   6 -- no pref, but not preferred due to family history
#  99 -- else / reevaluate
uw_pref2 <- integer(len(uw_pref))

for(i in 1:len(uw_pref)) {
  
  f_pref <- strsplit(uw_pref[i], " ")[[1]][1]
  f_spref <- strsplit(uw_spref[i], " ")[[1]][1]
  
  pref_family <- grep("Family", strsplit(uw_pref[i], " ")[[1]], ignore.case=T)
  spref_family <- grep("Family", strsplit(uw_spref[i], " ")[[1]], ignore.case=T)
  
  if(f_spref == "Yes") {
    uw_pref2[i] <- 0
    # print(uw_spref[i])
  }
  else if((f_pref == "Yes") & (len(spref_family) > 0)) {
    uw_pref2[i] <- 5
    # print(uw_pref[i])
  }
  else if((f_pref == "Yes")) {
    uw_pref2[i] <- 1
    # print(uw_pref[i])
  }
  else if((f_pref == "No") & (len(pref_family) > 0)) {
    uw_pref2[i] <- 6
    # print(uw_pref[i])
  }
  else if((f_pref == "No") & (len(pref_family) == 0)) {
    uw_pref2[i] <- 2
    # print(uw_pref[i])
  }
  
  # Exceptions
  else if(f_spref == "Yes,")
    uw_pref2[i] <- 0
  
  else if(uw_pref[i] == "Need more info re cancer, maybe accepted for preferred")
    uw_pref2[i] <- 1
  
  else {
    uw_pref2[i] <- 99
    # print(uw_pref[i])
  }
  
}
  
# Create new refactored preferred UW variable 'uw_pref3'
#   0 -- super-preferred
#   1 -- preferred
#   2 -- no preference

uw_pref3 <- uw_pref2
uw_pref3[uw_pref3 == 5] <- 1
uw_pref3[uw_pref3 == 6] <- 2


uw_pref4 <- uw_pref2
uw_pref4[uw_pref4 == 5] <- 0
uw_pref4[uw_pref4 == 6] <- 1


#############################################
#############################################



# fcsv_df <- fcsv_df[,1:20]

fcsv_df <- cb(fcsv_df, "UWDIGITISED_Overall"=uw_overall2,
              "UWDIGITISED_Preferred"=uw_pref3,
              "UWDIGITISED_Preferred_no_family"=uw_pref4)


fcsv_df <- cb(fcsv_df, "BIOMETRIC_VO2max_submax"=submax_res_matched)



fcsv_df <- cb(fcsv_df, "BIOMETRIC_BP_systolic_retested"=new_bp[,1],
              "BIOMETRIC_BP_diastolic_retested"=new_bp[,2],
              "UWFINAL_Updated"=gsub(",", ";", new_uw),
              "UWDIGITISED_Overall_UPDATED"=new_uw3[,1],
              "UWDIGITISED_Preferred_UPDATED"=new_uw3[,2])



#############################################
#############################################

is_na <- is.na(fcsv_df$BIOMETRIC_BP_systolic_retested)

# complete updated overall UW with results that weren't retested
fcsv_df$UWDIGITISED_Overall_UPDATED[is_na] <- fcsv_df$UWDIGITISED_Overall[is_na]

# complete preferred overall UW with results that weren't retested
fcsv_df$UWDIGITISED_Preferred_UPDATED[is_na] <- fcsv_df$UWDIGITISED_Preferred[is_na]

# adjust preferred-no-family for one retested participant that had family history as reason
fcsv_df$UWDIGITISED_Preferred_no_family_UPDATED <- fcsv_df$UWDIGITISED_Preferred_UPDATED
fcsv_df$UWDIGITISED_Preferred_UPDATED[71] <- 1


write.table2(as.data.frame(fcsv_df), file="~/HealthQ/RGA/SUBJECT_INFO/Summary/UW_Bio_CompactResults_old_vs_new.csv", col.names=T)


fcsv_df2 <- fcsv_df

# make one set of most up-to-date BPs
fcsv_df2$BIOMETRIC_BP_systolic <- fcsv_df2$BIOMETRIC_BP_systolic_retested
fcsv_df2$BIOMETRIC_BP_systolic[is_na] <- fcsv_df2$BIOMETRIC_BP_systolic_pilot[is_na]
fcsv_df2$BIOMETRIC_BP_diastolic <- fcsv_df2$BIOMETRIC_BP_diastolic_retested
fcsv_df2$BIOMETRIC_BP_diastolic[is_na] <- fcsv_df2$BIOMETRIC_BP_diastolic_pilot[is_na]

fcsv_df2 <- fcsv_df2[, c(1:8, 31:32, 11:13, 24, 14:20, 27:30)]

write.table2(as.data.frame(fcsv_df2), file="~/HealthQ/RGA/SUBJECT_INFO/Summary/UW_Bio_CompactResults_new.csv", col.names=T)





#############################################
#############################################


# datmatch <- match(tolower(dat_tmp[,1]), fcsv_df[,1])
# datmatch[8] <- 18
# datmatch[110] <- 67
# 
# dat_tmp2 <- cb(dat_tmp, fcsv_df[1:170, c(18, 21)][datmatch,])
# aa <- dat_tmp2[(dat_tmp2[,4] == 99) & (dat_tmp2[,2] == "I"), 1:3]
# aa <- cb(tolower(as.character(aa[,1])), as.character(aa[,3]))
# for(i in 1:nrow(aa)) cat(aa[i,1], "  -  ", aa[i,2], "\n", sep="")
# 
# dat_tmp2 <- fcsv_df[fcsv_df$ID_Cohort == "RGA", c(1, 18,21)]
# aa <- dat_tmp2[dat_tmp2[,3] == 99, c(1, 2)]
# aa <- cb(tolower(as.character(aa[,1])), as.character(aa[,2]))
# for(i in 1:nrow(aa)) cat(aa[i,1], "  -  ", aa[i,2], "\n", sep="")


dat <- read.table("~/HealthQ/RGA/SUBJECT_INFO/UW_Bio_results_compact.csv", head=T, sep=",")


h5_fpath <- "~/HealthQ/RGA/AGGREGATED/RGA_scalar_aggregation/RGA_colated_data_v2.h5"
h5_contents <- h5ls(h5_fpath)
print( h5_contents[h5_contents$group != "/", c(1, 5)] )


out_hr <- h5read(h5_fpath, "/hr", bit64conversion="double", compoundAsDataFrame=T)$table
out_hr <- data.frame(out_hr$anon_id, "wake_no_motion_hr_median_mean"=out_hr[, grep("wake_no_motion_hr_median_mean", names(out_hr))])

out_act <- h5read(h5_fpath, "/activity", bit64conversion="double", compoundAsDataFrame=T)$table
out_act <- data.frame(out_act$anon_id, "auto_vo2_mean"=out_act[, grep("auto_vo2_mean", names(out_act))])

out <- data.frame(out_hr, "auto_vo2_mean"=out_act[,2])



id_match <- match(dat$ID_ANON, out[,1])
dat2 <- data.frame(dat[-which(is.na(id_match)),], out[na.omit(id_match),2:3])


write.table2(dat2, file="~/HealthQ/RGA/20170608_biometric_wearable_data_for_life_exp.csv", col.names=T)



