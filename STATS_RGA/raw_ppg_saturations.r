
source("../useful_funs.r")
library(rhdf5)


h5_fpath <- "~/HealthQ/RGA/saturation.h5"
h5_contents <- h5ls(h5_fpath, rec=F)
h5_groups <- apply(h5_contents, 1, function(x) paste0(x[1], x[2], collapse=""))

n <- len(h5_groups)

sat_pp_list <- vector("list", n)
for(i in 1:n) {
  
  datf <- as.data.frame(h5read(h5_fpath, h5_groups[i], bit64conversion="double", compoundAsDataFrame=T)$table)
  sat_pp_list[[i]] <- c("all"=100*sum(datf[,3])/sum(datf[,2]), apply(datf, 1, function(x) round(100*x[3]/x[2], 6)))
  catProgress(i, len(h5_groups))
}



# Overall percentage of saturated samples for each participant
w() ; plot_g(1:n, unlist(lapply(sat_pp_list, function(x) x[[1]])), col="blue")



w() ; par(ask=T)
for(i in 1:n) {
  plot_g(1:len(sat_pp_list[[i]][-1]), sat_pp_list[[i]][-1], col="blue")
  abline(h=sat_pp_list[[i]][1], col="red", lty=2)
  title(main = paste("participant", i))
}
