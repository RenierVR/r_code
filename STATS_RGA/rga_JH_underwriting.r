library(readxl)

##################################################
####  Load data and extract relevant columns  ####
##################################################

fpath <- "C:/Users/reniervr/Downloads/MMI and LifeQ Pilot_Analytics data set_DOH_RUB_HA_UW_25042017_V7 - import.xlsx"

fdat <- read_excel(fpath, sheet = "IMPORT")
fdat <- fdat[-c(1, which(is.na(fdat[,1]))), ]


dat <- fdat[, sapply(c("AnonID", "DecisionType", "Gender", "Age", "Height", "Weight", "BMI", "Cholesterol", "BpSystolic", "BpDiastolic",
                       "SmokingHabitsLast6Mths",
                       "MedicalAdviceToQuitSmokingDrinking", "MedicalAdviceToQuitSmokingDrinkingDetails",
                       "MedicalAdviceToAttendRehab", "ReasonsForAdverseDecision"), 
                     function(x) grep(x, names(fdat))[1])]

## create data structures for storing JH-UW results
uw_JH <- cb("UW_outcome"=rep("super-preferred", nrow(dat)), "UW_comments"=char(nrow(dat)))




#######################################################
####  Judge demographic/biometric characteristics  ####
#######################################################

## No UW if age outside allowable bounds
ALLOWABLE_AGE_RANGE <- c(18, 70)
age <- as.num(dat$Age)
uw_JH[which((age < ALLOWABLE_AGE_RANGE[1]) | (age > ALLOWABLE_AGE_RANGE[2])), 1] <- NA
uw_JH[which((age < ALLOWABLE_AGE_RANGE[1]) | (age > ALLOWABLE_AGE_RANGE[2])), 2] <- "Age outside [18, 70]"

## smoke status
smoke_status <- rep("Yes", nrow(uw_JH))
smoke_status[dat$SmokingHabitsLast6Mths == "Have not smoked"] <- "No"

smoke_status[c(632)] <- "StoppedLessThan2" # from MedicalAdviceToQuitSmokingDrinkingDetails
smoke_status[c(432, 581)] <- "Stopped2To5" # from MedicalAdviceToQuitSmokingDrinkingDetails

for(i in 1:nrow(uw_JH)) {
  if(smoke_status[i] %in% c("Yes", "StoppedLessThan2")) {
    uw_JH[i, 1] <- "preferred"
    uw_JH[i, 2] <- paste0(uw_JH[i, 2], "; Smoker")
  }
  else if(smoke_status[i] == "Stopped2To5")
    uw_JH[i, 1] <- "preferred"
}

## Blood pressure
bp <- apply(cb(dat$BpSystolic, dat$BpDiastolic), 2, as.num)

for(i in 1:nrow(uw_JH)) {
  if(any(bp[i,] == 0)) {
    uw_JH[i, 1] <- NA
    uw_JH[i, 2] <- paste0(uw_JH[i, 2], "; Missing BP")
    next
  }
  
  if(age[i] <= 50) {
    if((bp[i,1] > 135) & (bp[i,1] <= 140) & (bp[i,2] <= 85)) {
      uw_JH[i, 1] <- "preferred"
      uw_JH[i, 2] <- paste0(uw_JH[i, 2], "; BP")
    }
    else if((bp[i,1] > 140) | (bp[i,2] > 85)) {
      uw_JH[i, 1] <- "non-preferred"
      uw_JH[i, 2] <- paste0(uw_JH[i, 2], "; BP")
    }
  }
  else {
    if((bp[i,1] > 140) & (bp[i,1] <= 145) & (bp[i,2] <= 90)) {
      uw_JH[i, 1] <- "preferred"
      uw_JH[i, 2] <- paste0(uw_JH[i, 2], "; BP")
    }
    else if((bp[i,1] > 145) | (bp[i,2] > 90)) {
      uw_JH[i, 1] <- "non-preferred"
      uw_JH[i, 2] <- paste0(uw_JH[i, 2], "; BP")
    }
  }
}


## Cholesterol
chol <- as.num(dat$Cholesterol)

for(i in 1:nrow(uw_JH)) {
  if(chol[i] == 0) {
    uw_JH[i, 1] <- NA
    uw_JH[i, 2] <- paste0(uw_JH[i, 2], "; Missing Chol")
    next 
  }
  
  if(age[i] <= 50) {
    if((chol[i] > 5.957) & (chol[i] <= 6.475)) {
      uw_JH[i, 1] <- "preferred"
      uw_JH[i, 2] <- paste0(uw_JH[i, 2], "; Chol")
    }
    else if(chol[i] > 6.475) {
      uw_JH[i, 1] <- "non-preferred"
      uw_JH[i, 2] <- paste0(uw_JH[i, 2], "; Chol")
    }
  }
  else {
    if((chol[i] > 6.475) & (chol[i] <= 6.993)) {
      uw_JH[i, 1] <- "preferred"
      uw_JH[i, 2] <- paste0(uw_JH[i, 2], "; Chol")
    }
    else if(chol[i] > 6.993) {
      uw_JH[i, 1] <- "non-preferred"
      uw_JH[i, 2] <- paste0(uw_JH[i, 2], "; Chol")
    }
  }
}


## BMI
bmichart_p <- cb(seq(142.24, 198.12, len=23), 
                 0.453592*c(142, 147, 152, 158, 164, 170, 176, 182, 186, 192, 197, 203, 208, 214, 219, 225, 231, 237, 243, 249, 255, 261, 268))

bmichart_sp <- cb(seq(152.4, 200.66, len=20), 
                  0.453592*c(145, 149, 153, 157, 162, 166, 170, 176, 182, 187, 193, 199, 205, 210, 216, 220, 223, 227, 231, 235))

bmichart <- cb(c(bmichart_p[1:4, 1], bmichart_sp[, 1]),
               c(bmichart_p[,2], tail(bmichart_p[,2], 1)),
               c(rep(bmichart_sp[1,2], 4), bmichart_sp[,2]))[-1,]



hw <- cb(as.num(dat$Height), as.num(dat$Weight))

for(i in 1:nrow(uw_JH)) {
  if(is.na(hw[i,1])) {
    uw_JH[i, 1] <- NA
    uw_JH[i, 2] <- paste0(uw_JH[i, 2], "; Missing BMI")
    next
  }
  
  if(hw[i,1] == 0) {
    uw_JH[i, 1] <- NA
    uw_JH[i, 2] <- paste0(uw_JH[i, 2], "; Missing BMI")
    next
  }
  
  if(hw[i,2]/(0.01*hw[i,1])^2 < 18) {
    uw_JH[i, 1] <- "non-preferred"
    uw_JH[i, 2] <- paste0(uw_JH[i, 2], "; BMI")
    next
  }
  
  
  chart_i <- which(hw[i, 1] <= bmichart[,1])[1]
  if(is.na(chart_i)) chart_i <- nrow(bmichart)
  
  if((hw[i,2] <= bmichart[chart_i, 2]) & (hw[i,2] >= bmichart[chart_i, 3])) {
    uw_JH[i, 1] <- "preferred"
    uw_JH[i, 2] <- paste0(uw_JH[i, 2], "; BMI")
  }
  else if(hw[i,2] > bmichart[chart_i, 2]) {
    uw_JH[i, 1] <- "non-preferred"
    uw_JH[i, 2] <- paste0(uw_JH[i, 2], "; BMI")
  }
  
  
}





#############################
####  Lifestyle factors  ####
#############################

## Participants with alcohol abuse (proxy: advised to stop drinking)

# med_advice <- dat$MedicalAdviceToQuitSmokingDrinkingDetails
# aa <- med_advice[!is.na(med_advice)]
# names(aa) <- which(!is.na(med_advice))

uw_JH[c(305, 861, 866), 1] <- "non-preferred"
uw_JH[c(305, 861, 866), 2] <- paste0(uw_JH[c(305, 861, 866), 2], "; Advised stop drinking")


## Drug abuse indications
uw_JH[which(dat$MedicalAdviceToAttendRehab == "Yes"), 1] <- "non-preferred"
uw_JH[which(dat$MedicalAdviceToAttendRehab == "Yes"), 2] <- paste0(uw_JH[which(dat$MedicalAdviceToAttendRehab == "Yes"), 2], "; Advised attend rehad")




##########################
####  Family history  ####
##########################

# no judgement due to inspecific info



############################
####  Personal history  ####
############################

med_hist <- tolower(dat$ReasonsForAdverseDecision)
adverse_health_i <- sort(unique(c(grep("diabetes", med_hist), grep("cancer", med_hist), grep("blood circulation", med_hist))))

uw_JH[adverse_health_i, 1] <- "non-preferred"
uw_JH[adverse_health_i, 2] <- paste0(uw_JH[adverse_health_i, 2], "; Personal history")



#########################
####  New data prep  ####
#########################

which_sc <- which(substr(uw_JH[,2], 1, 1) == ";")
uw_JH[which_sc, 2] <- substr(uw_JH[which_sc, 2], 3, nchar(uw_JH[which_sc, 2]))

dat_mmi_JH <- data.frame(dat[, 1:2], "BIO_AGE"=age, "BIO_HEIGHT"=hw[,1], "BIO_WEIGHT"=hw[,2],
                         "BIO_BMI"=hw[,2]/(0.01*hw[,1])^2, "BIO_BP_SYS"=bp[,1], "BIO_BP_DIA"=bp[,2],
                         "BIO_CHOL"=chol, "BIO_SEX"=dat$Gender, "BIO_SMOKE"=smoke_status,
                         uw_JH)

write.table2(dat_mmi_JH, "~/HealthQ/RGA/AGGREGATED/UW_JH_MMI.csv", col.names=T)






