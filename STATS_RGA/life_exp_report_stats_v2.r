source("../useful_funs.r")

# fcsv_df2 <- read.table("~/HealthQ/RGA/SUBJECT_INFO/Summary/UW_Bio_CompactResults_new.csv", sep=",", head=T)


SA_cohort_profile_submax <- read.table("~/HealthQ/RGA/LE/LifeQ_profile_RHR_BMI_plus_submax_VO2max_based_life_expectancies.csv", head=T, sep=",")

dat <- SA_cohort_profile_submax
dat$email <- tolower(as.character(dat$email))

id_match <- match(fcsv_df2[,1], dat$email)

aa <- cb(fcsv_df2[!is.na(id_match),8], dat[na.omit(id_match),5])
apply(aa, 1, diff)


################################################################

# le_input_wear <- read.table("~/HealthQ/RGA/20170608_biometric_wearable_data_for_life_exp.csv", head=T, sep=",")

################################################################

le1 <- read.table("~/HealthQ/RGA/LE/Life_expectancy_based_on_autovo2max_BMI_RHR_from_profile.csv", head=T, sep=",")
le2 <- read.table("~/HealthQ/RGA/LE/Life_expectancy_based_on_autovo2max_BMI_RHR.csv", head=T, sep=",")
le3 <- read.table("~/HealthQ/RGA/LE/Life_expectancy_based_on_profile_data_only.csv", head=T, sep=",")
le4 <- read.table("~/HealthQ/RGA/LE/Life_expectancy_based_on_profile_data_and_VO2submax_only.csv", head=T, sep=",")
le5 <- read.table("~/HealthQ/RGA/LE/Life_expectancy_based_on_profile_VO2max_and_wearable_RHR.csv", head=T, sep=",")


le_autovo2_rhrprofile <- le1$lifeq_Life_Expectancy
le_autovo2_rhrwearable <- le2$lifeq_Life_Expectancy
le_vo2profile_rhrprofile <- le3$lifeq_Life_Expectancy
le_vo2sub_rhrprofile <- le4$lifeq_Life_Expectancy
le_vo2profile_rhrwearable <- le5$lifeq_Life_Expectancy



le1$dbn_id <- as.character(le1$dbn_id)
le2$dbn_id <- as.character(le2$dbn_id)
le3$dbn_id <- as.character(le3$dbn_id)
le4$dbn_id <- as.character(le4$dbn_id)
le5$dbn_id <- as.character(le5$dbn_id)


id_match <- match(fcsv_df2[,2], le1$dbn_id)




#############################################
#####  Differences between RHR sources  #####
#############################################

rhr_prof <- le1$RHR
rhr_wear <- le2$RHR


w(12, 12)
plot_g(rhr_prof, rhr_wear, type="n", xlim=range(c(rhr_prof, rhr_wear)), ylim=range(c(rhr_prof, rhr_wear)), #ylim=c(44, 100)
       xlab="measured RHR (bpm)", ylab="wearable RHR (bpm)",
       main="RHR comparison: nurse exam vs wearable", sub="mean absolute difference = 8.78 bpm (13.97%)")
abline(a=0, b=1)
points(rhr_prof, rhr_wear, pch=19, col="blue", lty=2)


##############################################
#####  Compile comparison data into DFs  #####
##############################################


comp_mat <- data.frame("DBN_ID"=fcsv_df2[!is.na(id_match), 2],
                       "UW_OV"=fcsv_df2[!is.na(id_match), 23],
                       "UW_PR"=fcsv_df2[!is.na(id_match), 25],
                       "LE_POP"=le1[na.omit(id_match), 12],
                       "LE_PROF"=le_autovo2_rhrprofile[na.omit(id_match)],
                       "LE_WEAR"=le_autovo2_rhrwearable[na.omit(id_match)],
                       "LE_PROF_ALL"=le_vo2profile_rhrprofile[na.omit(id_match)],
                       "LE_WEAR_RHR"=le_vo2profile_rhrwearable[na.omit(id_match)])


comp_ov <- comp_mat[comp_mat[,2] != 99,c(2,4:6)]


comp_uw <- comp_mat$UW_PR
comp_uw[comp_mat$UW_OV == 1] <- 3
# comp_uw[(comp_uw == 0) & (comp_mat$UW_OV == 99)] <- 4
# comp_uw[(comp_uw == 2) & (comp_mat$UW_OV == 99)] <- 5

comp_pr <- data.frame("UW"=comp_uw, comp_mat[,4:8])
comp_pr <- comp_pr[comp_mat[,2] != 99,]



comp_vo2 <- cb(fcsv_df2[!is.na(id_match), c(2,14)], le1[na.omit(id_match), 10], le3[na.omit(id_match), 10])
comp_vo2 <- comp_vo2[!is.na(comp_vo2[,2]),]

# id_match_vo2 <- match(fcsv_df2[,2], le4$dbn_id)
# comp_vo2


comp_mat2 <- cb(comp_mat, "vo2max_auto"=le1[na.omit(id_match), 10],
                "bmi"=le1[na.omit(id_match), 4],
                "age"=le1[na.omit(id_match), 5],
                "gender"=le1[na.omit(id_match), 6])
comp_pr2 <- data.frame("UW"=comp_uw, comp_mat2[,4:11])
comp_pr2 <- comp_pr2[comp_mat2[,2] != 99,]


############################################
#####  Comparison of VO2max estimates  #####
############################################


w(12, 12)
plot_g(comp_vo2[,2], comp_vo2[,3], type="n", 
       xlim=range(c(comp_vo2[,2:4], comp_vo2[,2:4])), ylim=range(c(comp_vo2[,2:4], comp_vo2[,2:4])),
       xlab="VO2max - submax result (ml/kg/min)", ylab="VO2max estimate (ml/kg/min)",
       main="VO2max estimate comparison", sub="mean abs diff | wearable = 5.72 ml/kg/min (15.06%) | profile = 6.99 ml/kg/min (16.58%)")
abline(a=0, b=1, lty=2)
points(comp_vo2[,2], comp_vo2[,3], pch=19, col="red")
points(comp_vo2[,2], comp_vo2[,4], pch=19, col="blue")
legend("bottomright", pch=19, col=c("red", "blue"), leg=c("wearable estimate", "profile estimate"), bg="white")
savePlot("04.png", "png")


#######################################################
#####  Comparison of different Life Exp versions  #####
#######################################################

w(12, 12)
plot_g(comp_pr[,3], comp_pr[,4], type="n", xlim=range(comp_pr[,3:4]), ylim=range(comp_pr[,3:4]),
       xlab="LifeQ Life Expectancy - wearable VO2max, nurse exam RHR (years)", 
       ylab="LifeQ Life Expectancy - wearable VO2max and RHR (years)",
       main="Comparison of LifeQ Life Expectancy versions", sub="mean absolute difference = 1.63 years (1.93%)")
abline(a=0, b=1)
points(comp_pr[,3], comp_pr[,4], pch=19, col="brown", lty=2)
savePlot("le_comp_01.png", "png")


w(12, 12)
plot_g(comp_pr[,3], comp_pr[,5], type="n", xlim=range(comp_pr[,c(3,5)]), ylim=range(comp_pr[,c(3,5)]),
       xlab="LifeQ Life Expectancy - wearable VO2max, nurse exam RHR (years)", 
       ylab="LifeQ Life Expectancy - profile VO2max and nurse exam RHR (years)",
       main="Comparison of LifeQ Life Expectancy versions", sub="mean absolute difference = 2.73 years (3.2%)")
abline(a=0, b=1)
points(comp_pr[,3], comp_pr[,5], pch=19, col="purple", lty=2)
savePlot("le_comp_02.png", "png")




w(12, 12)
plot_g(comp_pr[,5], comp_pr[,6], type="n", xlim=range(comp_pr[,5:6]), ylim=range(comp_pr[,5:6]),
       xlab="LifeQ Life Expectancy - nurse exam RHR (years)", 
       ylab="LifeQ Life Expectancy - wearable RHR (years)",
       main="Comparison of LifeQ Life Expectancy versions", sub="mean absolute difference = 3.95 years (4.8%)")
abline(a=0, b=1)
points(comp_pr[,5], comp_pr[,6], pch=19, col="brown", lty=2)




vo2_scaled <- round(100*(comp_pr2[,6] - min(comp_pr2[,6]))/(max(comp_pr2[,6] - min(comp_pr2[,6])) + 0.000001), 0)


# VO2max-colored
w(12, 12)
plot_g(comp_pr[,3], comp_pr[,5], type="n", xlim=range(comp_pr[,c(3,5)]), ylim=range(comp_pr[,c(3,5)]),
       xlab="LifeQ Life Expectancy - wearable VO2max, nurse exam RHR (years)", 
       ylab="LifeQ Life Expectancy - profile VO2max and nurse exam RHR (years)",
       main="Comparison of LifeQ Life Expectancy versions", sub="mean absolute difference = 2.73 years (3.2%)")
abline(a=0, b=1)
points(comp_pr[,3], comp_pr[,5], pch=19, lty=2, col=rainbow(len(vo2_scaled))[vo2_scaled])
# savePlot("le_comp_02.png", "png")






##########################
#####  Scatterplots  #####
##########################


# overall UW against LE
w()
plot_g(comp_ov$LE_PROF, comp_ov$LE_PROF-comp_ov$LE_POP, col=c("blue", "red")[comp_ov$UW_OV+1],
       xlab="LifeQ Life Expectancy (years)", ylab='life years "gained" relative to population baseline')
legend("bottomright", pch=19, col=c("blue", "red"), leg=c("standard", "rated"), bg="white")



# preferred UW against LE
w()
plot_g(comp_mat$LE_WEAR, comp_mat$LE_WEAR-comp_mat$LE_POP, col=c("blue", "orange", "red")[comp_mat$UW_PR+1],
       xlab="LifeQ Life Expectancy (years)", ylab='life years "gained" relative to population baseline')
legend("bottomright", pch=19, col=c("blue", "seagreen", "red"), bg="white", 
       leg=c("super-preferred", "preferred", "non-preferred"))



# combined UW against LE (wearable VO2max and profile RHR)
w()
plot_g(comp_pr$LE_PROF, comp_pr$LE_PROF-comp_pr$LE_POP, 
       xlim=range(comp_pr[,3:5]), ylim=range(comp_pr[,3:5] - comp_pr$LE_POP),
       col=c("blue", "purple", "orange", "red")[comp_pr$UW+1],
       xlab="LifeQ Life Expectancy (years)", ylab='life years "gained" relative to population baseline',
       main="Association between LifeQ Life Expectancy and underwriting\n(wearable VO2max and nurse exam RHR)")
legend("bottomright", pch=19, col=c("blue", "purple", "orange", "red"), bg="white", 
       leg=c("super-preferred", "preferred", "non-preferred standard", "rated"))
savePlot("02.png", "png")




# combined UW against LE (wearable VO2max and RHR)
w(12, 10)
plot_g(comp_pr$LE_WEAR, comp_pr$LE_WEAR-comp_pr$LE_POP,
       xlim=range(comp_pr[,4:5]), ylim=range(comp_pr[,4:5] - comp_pr$LE_POP), 
       col=c("blue", "mediumseagreen", "orange", "red")[comp_pr$UW+1],
       xlab="LifeQ Life Expectancy (years)", ylab='life years "gained" relative to population baseline',
       main="Association between LifeQ Life Expectancy and underwriting\n(wearable VO2max and RHR estimates)")
legend("bottomright", pch=19, col=c("blue", "mediumseagreen", "orange", "red"), bg="white", 
       leg=c("super-preferred", "preferred", "non-preferred standard", "rated"))
savePlot("le_out_wear.png", "png")


# combined UW against LE (wearable VO2max and profile RHR)
w(12, 10)
plot_g(comp_pr$LE_PROF_ALL, comp_pr$LE_PROF_ALL-comp_pr$LE_POP, 
       xlim=range(comp_pr[,4:5]), ylim=range(comp_pr[,4:5] - comp_pr$LE_POP),
       col=c("blue", "mediumseagreen", "orange", "red")[comp_pr$UW+1],
       xlab="LifeQ Life Expectancy (years)", ylab='life years "gained" relative to population baseline',
       main="Association between LifeQ Life Expectancy and underwriting\n(using nurse exam RHR)")
legend("bottomright", pch=19, col=c("blue", "mediumseagreen", "orange", "red"), bg="white", 
       leg=c("super-preferred", "preferred", "non-preferred standard", "rated"))
savePlot("le_out_prof.png", "png")


# combined UW against LE (profile VO2max and wearable RHR)
w(12, 10)
plot_g(comp_pr$LE_WEAR_RHR, comp_pr$LE_WEAR_RHR-comp_pr$LE_POP,
       xlim=range(comp_pr[,4:5]), ylim=range(comp_pr[,4:5] - comp_pr$LE_POP),
       col=c("blue", "mediumseagreen", "orange", "red")[comp_pr$UW+1],
       xlab="LifeQ Life Expectancy (years)", ylab='life years "gained" relative to population estimated',
       main="Association between LifeQ Life Expectancy and underwriting\n(using wearable RHR)")
legend("bottomright", pch=19, col=c("blue", "mediumseagreen", "orange", "red"), bg="white", 
       leg=c("super-preferred", "preferred", "non-preferred standard", "rated"))






######################
#####  Boxplots  #####
######################


le_prof_split <- split(comp_pr$LE_PROF_ALL, as.factor(comp_pr$UW))
le_wear_split <- split(comp_pr$LE_WEAR, as.factor(comp_pr$UW))
le_pop_split <- split(comp_pr$LE_POP, as.factor(comp_pr$UW))
le_wearrhr_split <- split(comp_pr$LE_WEAR_RHR, as.factor(comp_pr$UW))

bcol <- c("blue", "mediumseagreen", "orange", "red")
blab <- c("super-preferred", "preferred", "non-preferred standard", "rated")

names(le_prof_split) <- names(le_wear_split) <- names(le_pop_split) <- names(le_wearrhr_split) <- blab


w(12, 12) 
plot(0, 0, type="n", ann=F, yaxt="n", xaxt="n")
rect(par("usr")[1], par("usr")[3], par("usr")[2], par("usr")[4], col="gray80", border=NA)
par(new=T)
boxplot(x=le_pop_split, col=bcol, ylab="life expectancy (years)",
        main=c("UW vs baseline population life expectancy estimate"))
savePlot("le_box_pop.png", "png")


w(12, 12) 
plot(0, 0, type="n", ann=F, yaxt="n", xaxt="n")
rect(par("usr")[1], par("usr")[3], par("usr")[2], par("usr")[4], col="gray80", border=NA)
par(new=T)
boxplot(x=le_prof_split, col=bcol, ylab="life expectancy (years)", ylim=range(comp_pr[,3:5]),
        main=c("UW vs LifeQ Life Expectancy (nurse RHR)"))
savePlot("le_box_prof.png", "png")


w(12, 12) 
plot(0, 0, type="n", ann=F, yaxt="n", xaxt="n")
rect(par("usr")[1], par("usr")[3], par("usr")[2], par("usr")[4], col="gray80", border=NA)
par(new=T)
boxplot(x=le_wear_split, col=bcol, ylab="life expectancy (years)", ylim=range(comp_pr[,3:5]),
        main=c("UW vs LifeQ Life Expectancy (wearable inputs)"))
savePlot("le_box_wear.png", "png")


w(12, 12) 
plot(0, 0, type="n", ann=F, yaxt="n", xaxt="n")
rect(par("usr")[1], par("usr")[3], par("usr")[2], par("usr")[4], col="gray80", border=NA)
par(new=T)
boxplot(x=le_wearrhr_split, col=bcol, ylab="life expectancy (years)", ylim=range(comp_pr[,3:5]),
        main=c("UW vs LifeQ Life Expectancy (wearable RHR)"))
# savePlot("le_box_wear.png", "png")








#####################
#####  Forests  #####
#####################

dat <- comp_pr[,c(1, 4)]
CV_K <- 5
CV_SEED <- 0
CV_STRATIFY_FOLDS <- FALSE
CV_TRAIN_RESAMPLE <- FALSE
RF_model_eqn = as.factor(UW) ~.



runRandomForest <- function(dat,
                            CV_K = 5,
                            CV_SEED = 0,
                            CV_STRATIFY_FOLDS = TRUE,
                            CV_TRAIN_RESAMPLE = TRUE,
                            RF_model_eqn = as.factor(UW) ~.,
                            ...)
{
  
  m <- len(unique(dat[,1]))
  
  # Partition data into folds
  ############################
  set.seed(CV_SEED)
  
  if(!CV_STRATIFY_FOLDS) {
    folds <- sample(rep(1:CV_K, each=ceiling(nrow(dat)/CV_K)))[1:nrow(dat)]
    dat$FOLDS <- folds
  }
  if(CV_STRATIFY_FOLDS) {
    nclass <- table(dat[,1])
    folds0 <- sample(rep(1:CV_K, each=ceiling(nclass[1]/CV_K)))[1:nclass[1]]
    folds1 <- sample(rep(1:CV_K, each=ceiling(nclass[2]/CV_K)))[1:nclass[2]]
    
    dat$FOLDS[dat[,1] == 0] <- folds0
    dat$FOLDS[dat[,1] == 1] <- folds1
  }
  
  
  # for(i in 1:5) print(table(dat[dat$FOLDS == i, 2])/nrow(dat[dat$FOLDS == i,]))
  
  
  # Iterate over CV folds
  ########################
  model_list <- vector("list", CV_K)
  res_list <- vector("list", CV_K)
  for(k in 1:CV_K) {
    
    # Split into test and train based on fold 'k'
    i_f <- grep("FOLDS", names(dat))
    dat_train <- dat[dat$FOLD != k, -i_f]
    dat_test <- dat[dat$FOLD == k, -i_f]
    
    
    if(CV_TRAIN_RESAMPLE) {
      # dat_train_new <- dat_train[sample(which(dat_train[,1] == 0), diff(table(dat_train[,1])), replace=T), ]
      dat_train_new <- dat_train[sample(which(dat_train[,1] == 1), abs(diff(table(dat_train[,1]))), replace=T), ]
      dat_train <- rbind(dat_train, dat_train_new)
    }
    
    # Ensure randomised observations
    dat_train <- dat_train[sample(1:nrow(dat_train)),]
    
    rf_out <- randomForest(RF_model_eqn, data=dat_train, 
                           importance=T, proximity=T)#, ...)
    
    # kappa <- calc_cohen_kappa(rf_out$confusion[,-3])
    
    # Performance
    preds <- predict(rf_out, dat_test[, -1, drop=F])
    pred_probs <-predict(rf_out, dat_test[, -1, drop=F], type="prob")
    y_pred <- as.numeric(preds)-1
    # conf_mat <- table(as.factor(y_pred), as.factor(dat_test[,1]))
    
    # Ensure confusion mat has all levels
    y_pred <- as.factor(y_pred)
    y_test <- as.factor(dat_test[,1])
    
    lvls <- as.character(1:m-1)
    levels(y_pred) <- c(levels(y_pred), lvls[which(is.na(match(lvls, levels(y_pred))))])
    levels(y_test) <- c(levels(y_test), lvls[which(is.na(match(lvls, levels(y_test))))])
    
    conf_mat <- table(y_pred, y_test)
    conf_mat <- conf_mat[order(rownames(conf_mat)), ]
    conf_mat <- conf_mat[, order(colnames(conf_mat))]
    
    # auc <- performance(prediction(pred_probs[,2], as.factor(dat_test[,1])), measure="auc")@y.values[[1]]
    
    
    model_list[[k]] <- rf_out
    res_list[[k]] <- list("CONFUSION_MATRIX"=conf_mat,
                          "PREDICTION_PROBS"=cb("pred"=pred_probs[,2], "true"=dat_test[,1]),
                          "COHEN_KAPPA"=calc_cohen_kappa(conf_mat))#,
    # "AUC"=auc)
    
    
    
    
    # catProgress(k, CV_K)
    
  }
  
  return(list("model_list"=model_list,
              "res_list"=res_list))
  
  
}




# out <- runRandomForest(comp_pr[,c(1, 4)], CV_K=5, CV_SEED=0, CV_STRATIFY_FOLDS=F, CV_TRAIN_RESAMPLE=F)

dat_rf_w <- comp_pr[,c(1, 4)]
dat_rf_w[dat_rf_w[,1] == 0, 1] <- 0
dat_rf_w[dat_rf_w[,1] == 1, 1] <- 1
dat_rf_w[dat_rf_w[,1] == 2, 1] <- 1
dat_rf_w[dat_rf_w[,1] == 3, 1] <- 1

dat_rf_p <- comp_pr[,c(1, 5)]
dat_rf_p[dat_rf_p[,1] == 0, 1] <- 0
dat_rf_p[dat_rf_p[,1] == 1, 1] <- 1
dat_rf_p[dat_rf_p[,1] == 2, 1] <- 1
dat_rf_p[dat_rf_p[,1] == 3, 1] <- 1

dat_rf_wr <- comp_pr[,c(1, 6)]
dat_rf_wr[dat_rf_wr[,1] == 0, 1] <- 0
dat_rf_wr[dat_rf_wr[,1] == 1, 1] <- 1
dat_rf_wr[dat_rf_wr[,1] == 2, 1] <- 2
dat_rf_wr[dat_rf_wr[,1] == 3, 1] <- 2


n_cv <- 50
cv_out_w <- vector("list", n_cv)
cv_out_p <- vector("list", n_cv)
cv_out_wr <- vector("list", n_cv)
for(i in 1:n_cv) {
  
  cv_seed <- round(runif(1, 0, 99999), 0)
  cv_out_w[[i]] <- runRandomForest(dat_rf_w, CV_K=3, CV_SEED=cv_seed, CV_STRATIFY_FOLDS=T, CV_TRAIN_RESAMPLE=T)
  cv_out_p[[i]] <- runRandomForest(dat_rf_p, CV_K=3, CV_SEED=cv_seed, CV_STRATIFY_FOLDS=T, CV_TRAIN_RESAMPLE=T)
  cv_out_wr[[i]] <- runRandomForest(dat_rf_wr, CV_K=3, CV_SEED=cv_seed, CV_STRATIFY_FOLDS=T, CV_TRAIN_RESAMPLE=T)
  catProgress(i, n_cv)
  
}


cv_kappa_w <- unlist(lapply(cv_out_w, function(x) unlist(lapply(x$res_list, function(y) y[[3]]))))
cv_kappa_wr <- unlist(lapply(cv_out_p, function(x) unlist(lapply(x$res_list, function(y) y[[3]]))))
cv_kappa_wr <- unlist(lapply(cv_out_wr, function(x) unlist(lapply(x$res_list, function(y) y[[3]]))))

# cv_conf_list <- lapply(cv_out, function(x) lapply(x$res_list, function(y) y[[1]]))
# cv_conf <- NULL
# for(i in 1:len(cv_conf_list))
#   cv_conf <- c(cv_conf, cv_conf_list[[i]])

cv_kappa_w_stats <- round(c(mean(cv_kappa_w), sd(cv_kappa_w), range(cv_kappa_w)), 3)
cv_kappa_p_stats <- round(c(mean(cv_kappa_p), sd(cv_kappa_p), range(cv_kappa_p)), 3)
cv_kappa_wr_stats <- round(c(mean(cv_kappa_wr), sd(cv_kappa_wr), range(cv_kappa_wr)), 3)


cat("CV Kappa (wearable):\n", "mean =", cv_kappa_w_stats[1], ", sd =", cv_kappa_w_stats[2], 
    ", min =", cv_kappa_w_stats[3], ", max =", cv_kappa_w_stats[4], "\n",
    "CV Kappa (wearable RHR):\n", "mean =", cv_kappa_wr_stats[1], ", sd =", cv_kappa_wr_stats[2], 
    ", min =", cv_kappa_wr_stats[3], ", max =", cv_kappa_wr_stats[4], "\n",
    "CV Kappa (profile):\n", "mean =", cv_kappa_p_stats[1], ", sd =", cv_kappa_p_stats[2], 
    ", min =", cv_kappa_p_stats[3], ", max =", cv_kappa_p_stats[4], "\n", sep="")











#################################
#####  Logistic regression  #####
#################################

library(nnet)


runLogRegCV <- function(dat,
                        CV_K = 3,
                        CV_SEED = 0,
                        CV_STRATIFY_FOLDS = TRUE,
                        CV_TRAIN_RESAMPLE = TRUE,
                        LR_model_eqn = as.factor(UW) ~.,
                        ...)
{
  
  m <- len(unique(dat[,1]))
  
  # if(m == 3)
    # dat[,2] <- (dat[,2] - min(dat[,2]))/(max(dat[,2] - min(dat[,2])) + 0.000001)
    
  
  # Partition data into folds
  ############################
  set.seed(CV_SEED)
  
  if(!CV_STRATIFY_FOLDS) {
    folds <- sample(rep(1:CV_K, each=ceiling(nrow(dat)/CV_K)))[1:nrow(dat)]
    dat$FOLDS <- folds
  }
  if(CV_STRATIFY_FOLDS) {
    nclass <- table(dat[,1])
    
    for(i in 1:m) 
      dat$FOLDS[dat[,1] == as.numeric(names(nclass)[i])] <- sample(rep(1:CV_K, each=ceiling(nclass[i]/CV_K)))[1:nclass[i]]
    
  }
  
  
  # for(i in 1:3) print(table(dat[dat$FOLDS == i, 1])/nrow(dat[dat$FOLDS == i,]))
  
  
  # Iterate over CV folds
  ########################
  model_list <- vector("list", CV_K)
  res_list <- vector("list", CV_K)
  for(k in 1:CV_K) {
    
    # Split into test and train based on fold 'k'
    i_f <- grep("FOLDS", names(dat))
    dat_train <- dat[dat$FOLD != k, -i_f]
    dat_test <- dat[dat$FOLD == k, -i_f]
    
    
    if(CV_TRAIN_RESAMPLE) {

      
      dat_train <- balance_data_with_SMOTE(dat_train)
      dat_train[,1] <- as.numeric(dat_train[,1])
            
      # m_tab <- table(dat_train[,1])
      # max_class <- which.max(m_tab)
      # for(i in (1:m)[-max_class]) 
      #   dat_train <- rbind(dat_train, dat_train[sample(which(dat_train[,1] == as.numeric(names(m_tab))[i]), 
      #                                                  abs(m_tab[max_class] - m_tab[i]), replace=T), ])
      
    }
    
    # Ensure randomised observations
    dat_train <- dat_train[sample(1:nrow(dat_train)),]
    
    if(m == 2)
      lr_out <- glm(LR_model_eqn, data=dat_train, family=binomial(link='logit'))#, ...)
    if(m == 3) {
      lr_out <- multinom(LR_model_eqn, data=dat_train)#, ...)
      # lr_out <- mlogit(LR_model_eqn, data=dat_train, shape="long", choice="UW")
    }
    
    
    
    # Performance
    
    if(m == 2) {
      preds <- predict(lr_out, dat_test[, -1, drop=F], type="response")
      y_pred <- round(preds, 0)
      conf_mat <- table(as.factor(dat_test[,1]), as.factor(y_pred))
      
      preds_fit <- predict(lr_out, type="response")
      y_pred_fit <- round(preds_fit, 0)
      conf_mat_fit <- table(as.factor(dat_train[,1]), as.factor(y_pred_fit))
    }
    if(m == 3) {
      y_pred <- predict(lr_out, dat_test[, -1, drop=F])
      conf_mat <- table(as.factor(dat_test[,1]), as.factor(y_pred))
      
      y_pred_fit <- predict(lr_out)
      conf_mat_fit <- table(as.factor(dat_train[,1]), as.factor(y_pred_fit))
    }
    
    conf_stats <- c(calc_cohen_kappa(conf_mat), 
                    sum(diag(conf_mat))/sum(conf_mat),
                    conf_mat[1,1]/sum(conf_mat[1,]), conf_mat[2,2]/sum(conf_mat[2,]))
    
    conf_stats_fit <- c(calc_cohen_kappa(conf_mat_fit), 
                        sum(diag(conf_mat_fit))/sum(conf_mat_fit),
                        conf_mat_fit[1,1]/sum(conf_mat_fit[1,]), conf_mat_fit[2,2]/sum(conf_mat_fit[2,]))
    
    
    
    
    
    model_list[[k]] <- lr_out
    res_list[[k]] <- list("CONFUSION_MATRIX"=conf_mat, 
                          "COHEN_KAPPA"=calc_cohen_kappa(conf_mat),
                          "CONF_STATS"=conf_stats,
                          "CONF_STATS"=conf_stats_fit)
    
    
    
    
    # catProgress(k, CV_K)
    
  }
  
  return(list("model_list"=model_list,
              "res_list"=res_list))
  
  
}






# dat_rf_w <- comp_pr[,c(1, 4)]
# dat_rf_w[dat_rf_w[,1] == 0, 1] <- 0
# dat_rf_w[dat_rf_w[,1] == 1, 1] <- 0
# dat_rf_w[dat_rf_w[,1] == 2, 1] <- 1
# dat_rf_w[dat_rf_w[,1] == 3, 1] <- 1

dat_rf_p <- comp_pr[,c(1, 5)]
dat_rf_p[dat_rf_p[,1] == 0, 1] <- 0
dat_rf_p[dat_rf_p[,1] == 1, 1] <- 1
dat_rf_p[dat_rf_p[,1] == 2, 1] <- 1
dat_rf_p[dat_rf_p[,1] == 3, 1] <- 1

dat_rf_wr <- comp_pr[,c(1, 6)]
dat_rf_wr[dat_rf_wr[,1] == 0, 1] <- 0
dat_rf_wr[dat_rf_wr[,1] == 1, 1] <- 1
dat_rf_wr[dat_rf_wr[,1] == 2, 1] <- 1
dat_rf_wr[dat_rf_wr[,1] == 3, 1] <- 1


###############################################


dat_rf_wr <- comp_pr[,c(1, 6)]
dat_rf_wr[dat_rf_wr[,1] == 0, 1] <- 0
dat_rf_wr[dat_rf_wr[,1] == 1, 1] <- 1
dat_rf_wr[dat_rf_wr[,1] == 2, 1] <- 2
dat_rf_wr[dat_rf_wr[,1] == 3, 1] <- 2
# dat_rf_wr <- dat_rf_wr[dat_rf_wr[,1] != 2, ]

dat_rf_p <- comp_pr[,c(1, 5)]
dat_rf_p[dat_rf_p[,1] == 0, 1] <- 0
dat_rf_p[dat_rf_p[,1] == 1, 1] <- 1
dat_rf_p[dat_rf_p[,1] == 2, 1] <- 2
dat_rf_p[dat_rf_p[,1] == 3, 1] <- 2




n_cv <- 100
cv_list_wr <- vector("list", 100)
cv_list_p <- vector("list", 100)
for(i in 1:n_cv) {
  
  cv_seed <- round(runif(1, 0, 99999), 0)
  
  cv_list_wr[[i]] <- runLogRegCV(dat=dat_rf_wr, 
                                 CV_K=3, CV_SEED=cv_seed, CV_STRATIFY_FOLDS=T, CV_TRAIN_RESAMPLE=T, 
                                 LR_model_eqn = as.factor(UW) ~ LE_WEAR_RHR)
  
  cv_list_p[[i]] <- runLogRegCV(dat=dat_rf_p, 
                                CV_K=3, CV_SEED=cv_seed, CV_STRATIFY_FOLDS=T, CV_TRAIN_RESAMPLE=T, 
                                LR_model_eqn = as.factor(UW) ~ LE_PROF_ALL)
  
  catProgress(i, n_cv)
  
}



###########################################


cv_stats_tmp <- lapply(cv_list_wr, function(x) matrix(unlist(lapply(x$res_list, function(y) y[[3]])), nc=4, byrow=T))
cv_stats <- cv_stats_tmp[[1]]
for(i in 2:len(cv_stats_tmp)) cv_stats <- rbind(cv_stats, cv_stats_tmp[[i]])

cv_stats_fit_tmp <- lapply(cv_list_wr, function(x) matrix(unlist(lapply(x$res_list, function(y) y[[4]])), nc=4, byrow=T))
cv_stats_fit <- cv_stats_fit_tmp[[1]]
for(i in 2:len(cv_stats_fit_tmp)) cv_stats_fit <- rbind(cv_stats_fit, cv_stats_fit_tmp[[i]])

round(apply(cv_stats, 2, mean), 2)
round(apply(cv_stats, 2, sd), 2)


round(apply(cv_stats_fit, 2, mean), 2)
round(apply(cv_stats_fit, 2, sd), 2)



###########################################


cv_stats_tmp <- lapply(cv_list_p, function(x) matrix(unlist(lapply(x$res_list, function(y) y[[3]])), nc=4, byrow=T))
cv_stats <- cv_stats_tmp[[1]]
for(i in 2:len(cv_stats_tmp)) cv_stats <- rbind(cv_stats, cv_stats_tmp[[i]])

cv_stats_fit_tmp <- lapply(cv_list_p, function(x) matrix(unlist(lapply(x$res_list, function(y) y[[4]])), nc=4, byrow=T))
cv_stats_fit <- cv_stats_fit_tmp[[1]]
for(i in 2:len(cv_stats_fit_tmp)) cv_stats_fit <- rbind(cv_stats_fit, cv_stats_fit_tmp[[i]])



round(apply(cv_stats_fit, 2, mean), 2)
round(apply(cv_stats_fit, 2, sd), 2)


round(apply(cv_stats, 2, mean), 2)
round(apply(cv_stats, 2, sd), 2)










# cv_kappa_p_stats <- round(c(mean(cv_kappa_p), sd(cv_kappa_p), range(cv_kappa_p)), 3)
# cv_kappa_wr_stats <- round(c(mean(cv_kappa_wr), sd(cv_kappa_wr), range(cv_kappa_wr)), 3)
# cv_kappa_w_stats <- round(c(mean(cv_kappa_w), sd(cv_kappa_w), range(cv_kappa_w)), 3)
# 
# 
# cat("CV Kappa (wearable):\n", "mean =", cv_kappa_w_stats[1], ", sd =", cv_kappa_w_stats[2], 
#     ", min =", cv_kappa_w_stats[3], ", max =", cv_kappa_w_stats[4], "\n",
#     "CV Kappa (wearable RHR):\n", "mean =", cv_kappa_wr_stats[1], ", sd =", cv_kappa_wr_stats[2], 
#     ", min =", cv_kappa_wr_stats[3], ", max =", cv_kappa_wr_stats[4], "\n",
#     "CV Kappa (profile):\n", "mean =", cv_kappa_p_stats[1], ", sd =", cv_kappa_p_stats[2], 
#     ", min =", cv_kappa_p_stats[3], ", max =", cv_kappa_p_stats[4], "\n", sep="")



# out1 <- glm(as.factor(UW) ~ LE_WEAR_RHR, data=dat_rf_wr, family=binomial(link='logit'))
# 
# out_fitted <- predict(out1, type="response")
# conf_fitted <- table(dat_rf_wr$UW, round(out_fitted,0))
# print(calc_cohen_kappa(conf_fitted))




# Multinomial Log Reg
#######################

# le_sc <- (comp_pr[,6] - min(comp_pr[,6]))/(max(comp_pr[,6] - min(comp_pr[,6])) + 0.000001)
# 
# dat_rf_wr <- comp_pr[,c(1, 6)]
# # dat_rf_wr[,2] <- le_sc
# dat_rf_wr[dat_rf_wr[,1] == 0, 1] <- 0
# dat_rf_wr[dat_rf_wr[,1] == 1, 1] <- 1
# dat_rf_wr[dat_rf_wr[,1] == 2, 1] <- 2
# dat_rf_wr[dat_rf_wr[,1] == 3, 1] <- 2
# 
# 
# # out2 <- multinom(as.factor(UW) ~ LE_WEAR_RHR, data=dat_rf_wr)
# 
# dat <- dat_rf_wr
# dat$UW <- as.factor(dat$UW)
# 
# out1 <- multinom(mode ~ income, data=Fishing)
# out2 <- mlogit(mode ~ 0 | income, data=Fishing, shape="wide")
# out3 <- mlogit(mode ~ 0 | income, data=mlogit.data(Fishing, varying = c(2:9), shape = "wide", choice = "mode"))
# 
# out4 <- glm(as.numeric(mode) ~ income, data=Fishing, family=poisson())
# 
# pred1 <- predict(out1)
# pred2 <- predict(out2)
# pred4 <- predict(out4, type="response")


# Support Vector Machine
#########################

library(e1071)


dat_rf_wr <- comp_pr[,c(1, 6)]
dat_rf_wr[dat_rf_wr[,1] == 0, 1] <- 0
dat_rf_wr[dat_rf_wr[,1] == 1, 1] <- 1
dat_rf_wr[dat_rf_wr[,1] == 2, 1] <- 2
dat_rf_wr[dat_rf_wr[,1] == 3, 1] <- 2


out3 <- svm(as.factor(UW) ~ LE_WEAR_RHR, data=dat_rf_wr)

pred <- predict(out3)





runSVMCV <- function(dat,
                     CV_K = 3,
                     CV_SEED = 0,
                     CV_STRATIFY_FOLDS = TRUE,
                     CV_TRAIN_RESAMPLE = TRUE,
                     SVM_model_eqn = as.factor(UW) ~.,
                     ...)
{
  
  m <- len(unique(dat[,1]))
  
  # Partition data into folds
  ############################
  set.seed(CV_SEED)
  
  if(!CV_STRATIFY_FOLDS) {
    folds <- sample(rep(1:CV_K, each=ceiling(nrow(dat)/CV_K)))[1:nrow(dat)]
    dat$FOLDS <- folds
  }
  if(CV_STRATIFY_FOLDS) {
    nclass <- table(dat[,1])
    
    for(i in 1:m) 
      dat$FOLDS[dat[,1] == as.numeric(names(nclass)[i])] <- sample(rep(1:CV_K, each=ceiling(nclass[i]/CV_K)))[1:nclass[i]]
    
  }
  
  
  # for(i in 1:5) print(table(dat[dat$FOLDS == i, 2])/nrow(dat[dat$FOLDS == i,]))
  
  
  # Iterate over CV folds
  ########################
  model_list <- vector("list", CV_K)
  res_list <- vector("list", CV_K)
  for(k in 1:CV_K) {
    
    # Split into test and train based on fold 'k'
    i_f <- grep("FOLDS", names(dat))
    dat_train <- dat[dat$FOLD != k, -i_f]
    dat_test <- dat[dat$FOLD == k, -i_f]
    
    
    if(CV_TRAIN_RESAMPLE) {
      m_tab <- table(dat_train[,1])
      max_class <- which.max(m_tab)
      
      for(i in (1:m)[-max_class]) 
        dat_train <- rbind(dat_train, dat_train[sample(which(dat_train[,1] == as.numeric(names(m_tab))[i]), 
                                                       abs(m_tab[max_class] - m_tab[i]), replace=T), ])
    }
    
    # Ensure randomised observations
    dat_train <- dat_train[sample(1:nrow(dat_train)),]
    
    svm_out <- svm(SVM_model_eqn, data=dat_train, ...)
    
    
    # Performance
    preds <- predict(svm_out, dat_test[, -1, drop=F])
    conf_mat <- table(as.factor(dat_test[,1]), as.factor(preds))
    
    preds_fit <- predict(svm_out)
    conf_mat_fit <- table(as.factor(dat_train[,1]), as.factor(preds_fit))
    
    conf_stats <- c(calc_cohen_kappa(conf_mat), 
                    sum(diag(conf_mat))/sum(conf_mat),
                    conf_mat[1,1]/sum(conf_mat[1,]), conf_mat[2,2]/sum(conf_mat[2,]))
    
    conf_stats_fit <- c(calc_cohen_kappa(conf_mat_fit), 
                        sum(diag(conf_mat_fit))/sum(conf_mat_fit),
                        conf_mat_fit[1,1]/sum(conf_mat_fit[1,]), conf_mat_fit[2,2]/sum(conf_mat_fit[2,]))
    
    
    
    
    
    model_list[[k]] <- rf_out
    res_list[[k]] <- list("CONFUSION_MATRIX"=conf_mat, 
                          "COHEN_KAPPA"=calc_cohen_kappa(conf_mat),
                          "CONF_STATS"=conf_stats,
                          "CONF_STATS"=conf_stats_fit)
    
    
    
    
    # catProgress(k, CV_K)
    
  }
  
  return(list("model_list"=model_list,
              "res_list"=res_list))
  
  
}






# dat_rf_w <- comp_pr[,c(1, 4)]
# dat_rf_w[dat_rf_w[,1] == 0, 1] <- 0
# dat_rf_w[dat_rf_w[,1] == 1, 1] <- 0
# dat_rf_w[dat_rf_w[,1] == 2, 1] <- 1
# dat_rf_w[dat_rf_w[,1] == 3, 1] <- 1

dat_rf_p <- comp_pr[,c(1, 5)]
dat_rf_p[dat_rf_p[,1] == 0, 1] <- 0
dat_rf_p[dat_rf_p[,1] == 1, 1] <- 1
dat_rf_p[dat_rf_p[,1] == 2, 1] <- 2
dat_rf_p[dat_rf_p[,1] == 3, 1] <- 2

dat_rf_wr <- comp_pr[,c(1, 6)]
dat_rf_wr[dat_rf_wr[,1] == 0, 1] <- 0
dat_rf_wr[dat_rf_wr[,1] == 1, 1] <- 1
dat_rf_wr[dat_rf_wr[,1] == 2, 1] <- 2
dat_rf_wr[dat_rf_wr[,1] == 3, 1] <- 2



n_cv <- 100
cv_list_wr <- vector("list", 100)
cv_list_p <- vector("list", 100)
for(i in 1:n_cv) {
  
  cv_seed <- round(runif(1, 0, 99999), 0)
  
  cv_list_wr[[i]] <- runSVMCV(dat=dat_rf_wr, 
                              CV_K=3, CV_SEED=cv_seed, CV_STRATIFY_FOLDS=T, CV_TRAIN_RESAMPLE=T, 
                              SVM_model_eqn = as.factor(UW) ~ LE_WEAR_RHR, kernel="radial", gamma=0.25)
  
  cv_list_p[[i]] <- runSVMCV(dat=dat_rf_p, 
                             CV_K=3, CV_SEED=cv_seed, CV_STRATIFY_FOLDS=T, CV_TRAIN_RESAMPLE=T, 
                             SVM_model_eqn = as.factor(UW) ~ LE_PROF_ALL, kernel="radial", gamma=0.25)
  
  catProgress(i, n_cv)
  
}


###########################################


cv_stats_tmp <- lapply(cv_list_wr, function(x) matrix(unlist(lapply(x$res_list, function(y) y[[3]])), nc=4, byrow=T))
cv_stats <- cv_stats_tmp[[1]]
for(i in 2:len(cv_stats_tmp)) cv_stats <- rbind(cv_stats, cv_stats_tmp[[i]])

cv_stats_fit_tmp <- lapply(cv_list_wr, function(x) matrix(unlist(lapply(x$res_list, function(y) y[[4]])), nc=4, byrow=T))
cv_stats_fit <- cv_stats_fit_tmp[[1]]
for(i in 2:len(cv_stats_fit_tmp)) cv_stats_fit <- rbind(cv_stats_fit, cv_stats_fit_tmp[[i]])

round(apply(cv_stats, 2, mean), 2)
round(apply(cv_stats, 2, sd), 2)


round(apply(cv_stats_fit, 2, mean), 2)
round(apply(cv_stats_fit, 2, sd), 2)



###########################################


cv_stats_tmp <- lapply(cv_list_p, function(x) matrix(unlist(lapply(x$res_list, function(y) y[[3]])), nc=4, byrow=T))
cv_stats <- cv_stats_tmp[[1]]
for(i in 2:len(cv_stats_tmp)) cv_stats <- rbind(cv_stats, cv_stats_tmp[[i]])

cv_stats_fit_tmp <- lapply(cv_list_p, function(x) matrix(unlist(lapply(x$res_list, function(y) y[[4]])), nc=4, byrow=T))
cv_stats_fit <- cv_stats_fit_tmp[[1]]
for(i in 2:len(cv_stats_fit_tmp)) cv_stats_fit <- rbind(cv_stats_fit, cv_stats_fit_tmp[[i]])



round(apply(cv_stats_fit, 2, mean), 2)
round(apply(cv_stats_fit, 2, sd), 2)


round(apply(cv_stats, 2, mean), 2)
round(apply(cv_stats, 2, sd), 2)










###########################################

# comp_mat_csv <- cb("Anon_ID"=fcsv_df2[which(!is.na(id_match)),1], 
#                    "LifeQ_Life_Expectancy"=comp_mat[,8],
#                    "LifeQ_Life_Adjusted"=comp_mat[,8] - comp_mat[,4])
# 
# 
# write.table2(comp_mat_csv, "~/HealthQ/RGA/20170622_RGAPilot_LifeQLifeExpectancy_Output.csv", col.names=T)


