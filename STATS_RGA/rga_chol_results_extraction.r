source("../useful_funs.r")

library(readxl)

dat_chol <- NULL


###############################
####  LIFEQ BLOOD RESULTS  ####
###############################

fdat <- read_excel("~/Documents/HealthQ/HHA/RGA_SUBJECT_INFO/SA Cohort/LifeQ cohort Blood and Urinalysis results.xlsx", sheet = "Sheet1")

# Extract cholesterol test results
# fdat <- fdat[fdat$TestName == "CHOLESTEROL-S", match(c("PatientID", "Result", "Units", "NormalRange"), names(fdat))]
fdat <- fdat[grep("CHOLESTEROL", fdat$TestName), match(c("PatientID", "TestName", "Result", "Units", "NormalRange"), names(fdat))]

names(fdat)[1] <- "Anon_email_ID"

# Omit readiness
dat_chol <- fdat[-c(1:48), ]



#########################################
####  LIFEQ READINESS BLOOD RESULTS  ####
#########################################

# dat_chol <- NULL

flist <- getDataFromDir("~/Documents/HealthQ/HHA/RGA_SUBJECT_INFO//SA Cohort/UW_readiness/", ext="xlsx", dataE=F)

for(i in 1:len(flist)) {
  
  fdat <- read_excel(flist[i], sheet = "Blood results & Urinalysis")
  fdat <- fdat[grep("CHOLESTEROL", fdat$TestName), match(c("LifeQ.ID", "TestName", "Result", "Units", "NormalRange"), names(fdat))]
  names(fdat)[1] <- "Anon_email_ID"
  
  dat_chol <- rbind(dat_chol, fdat)
  
}



#############################
####  RGA BLOOD RESULTS  ####
#############################

# dat_chol <- NULL

flist <- getDataFromDir("~/Documents/HealthQ/HHA/RGA_SUBJECT_INFO/UK Cohort/Blood/", ext="xlsx", dataE=F)

for(i in 1:len(flist)) {
  
  fdat <- read_excel(flist[i], sheet = "Sheet1")
  fdat <- fdat[grep("CHOLESTEROL", fdat$Test.Name), match(c("Patient.ID", "Test.Name", "Result", "Units", "Normal.Range"), names(fdat))]
  names(fdat) <- c("Anon_email_ID", "TestName", "Result", "Units", "NormalRange")
  
  dat_chol <- rbind(dat_chol, fdat)
  
}


#######################################
####  RGA READINESS BLOOD RESULTS  ####
#######################################

# dat_chol <- NULL

flist <- getDataFromDir("~/Documents/HealthQ/HHA/RGA_SUBJECT_INFO/UK Cohort/Blood/RGA cohort Blood and Urinalysis results_ReadinessPilot/", ext="xlsx", dataE=F)

for(i in 1:len(flist)) {
  
  fdat <- read_excel(flist[i], sheet = "Sheet1")
  fdat <- fdat[grep("CHOLESTEROL", fdat$Test.Name), match(c("Patient.ID", "Test.Name", "Result", "Units", "Normal.Range"), names(fdat))]
  names(fdat) <- c("Anon_email_ID", "TestName", "Result", "Units", "NormalRange")
  
  dat_chol <- rbind(dat_chol, fdat)
  
}

write.table2(dat_chol, file="~/Documents/HealthQ/HHA/RGA_SUBJECT_INFO/chol_temp.csv")


#####################################
####  MAP EMAIL IDS TO ANON IDS  ####
#####################################

dat_chol <- read.table("~/Documents/HealthQ/HHA/RGA_SUBJECT_INFO/chol_temp.csv", sep=",")
names(dat_chol) <- c("Anon_email_ID", "TestName", "Result", "Units", "NormalRange")

dat_chol[,1] <- tolower(dat_chol[,1])


dat_df <- t(sapply(readLines(file("~/Documents/HealthQ/HHA/RGA_SUBJECT_INFO/Summary/UW_Bio_CompactResults_new.csv")), 
                   function(x) unlist(strsplit(x, ","))))
dimnames(dat_df) <- list(NULL, dat_df[1,])
dat_df <- dat_df[-1,]

# match1 <- match(dat_df[,1], dat_chol[,1])
# match2 <- match(dat_chol[,1], dat_df[,1])
# dat_df[which(is.na(match1)), 1]
# dat_chol[which(is.na(match2)), 1]

# dat_chol[match(c("hqb90z2@gmail.com", "hqgra84@gamil.com", "hqh007hh@gmail.com ",
#                  "hqvzijl@gamil.com", "rgaukgarfield@gmail.com", "rgaukpok3r@gmail.com",
#                  "rgaukr9jya@gmail.com", "rgaukrevy@gmail.com", "rgaukv2830@gmail.com"), dat_chol[,1]), 1] <- c("hqb9oz2@gmail.com", "hqgra84@gmail.com", "hqh007hh@gmail.com",
#                                                                                                                 "hqvzijl@gmail.com", "rgukgarfield@gmail.com", "rgaukp0k3r@gmail.com", 
#                                                                                                                 "rgaukr9jy4@gmail.com",  "rgaukrevvy@gmail.com", "rgaukv283o@gmail.com")
                                                                                                                

oldnames <- c("hqb90z2@gmail.com", "hqgra84@gamil.com", "hqh007hh@gmail.com ",
              "hqvzijl@gamil.com", "rgaukgarfield@gmail.com", "rgaukpok3r@gmail.com",
              "rgaukr9jya@gmail.com", "rgaukrevy@gmail.com", "rgaukv2830@gmail.com")

newnames <- c("hqb9oz2@gmail.com", "hqgra84@gmail.com", "hqh007hh@gmail.com",
              "hqvzijl@gmail.com", "rgukgarfield@gmail.com", "rgaukp0k3r@gmail.com", 
              "rgaukr9jy4@gmail.com",  "rgaukrevvy@gmail.com", "rgaukv283o@gmail.com")

for(i in 1:len(oldnames)) {
  
  which_replace <- which(oldnames[i] == dat_chol[,1])
  print(which_replace)
  if(len(which_replace) > 0) dat_chol[which_replace, 1] <- newnames[i]
  
}



match3 <- match(dat_df[,1], dat_chol[,1])


# dat_chol <- cb(dat_df[, c(1, 3)], dat_chol[match3, -1])

anonids <- rep("", nrow(dat_chol))
for(i in 1:len(match3)) {
  
  which_i <- which(dat_chol[,1] == dat_chol[match3[i], 1])
  anonids[which_i] <- dat_df[i, 3]
  
}

dat_chol <- cb("AnonID"=anonids, dat_chol)

write.table2(dat_chol, "~/Documents/HealthQ/HHA/RGA_SUBJECT_INFO/All_cholesterol_results.csv", col.names=T)
















