# source("../R_scripts/useful_funs.r")
source("../useful_funs.r")
library(readxl)

###############################
####  LIFEQ BLOOD RESULTS  ####
###############################

fdat <- read_excel("C:/Users/reniervr/Documents/HealthQ/RGA/SUBJECT_INFO/Summary/20170704_UW_Questionnaire_Bio_results_all_manuallycorrectedwithRGAreadiness.xlsx", sheet = "Questionnaire", col_types=rep("text", 144))

qs <- c(47, 85, 87, 99, 102, 105, 106, 108, 109, 110, 112, 114, 117, 119, 120, 123, 125, 130)

fdat <- fdat[, c(1, qs-5)]
names(fdat) <- c("ID", "haz_pursuits", "alc_units", "advised_stop_drinking", "any_medical_treat_more4weeks",
                 "hosp_more2night_last5years", "family_died_heartdisease_before60", "family_other_disease",
                 "cancer", "cardiac_arrythmia", "heart_disease", "stroke_or_tia", "diabetes",
                 "mental_ill_required_hosp", "kidney_liver_panc_disease_required_hosp", "lung_disease_last5years",
                 "treated_high_BP", "treated_high_chol", "currently_antidep")

fdat[,1] <- dat_df[,3]

fdat <- cb(as.char(dat_use$UW_K3), fdat[match(dat_use$ID, fdat[,1]), ])


# haz pursuits
fdat[,3] <- as.num(as.factor(fdat[,3]))-1


# factorise alc consumption
fdat[,4] <- as.num(fdat[,4])
fdat[which(fdat[,4] <= 30), 4] <- 0
fdat[which(fdat[,4] > 30), 4] <- 1
fdat[is.na(fdat[,4]), 4] <- 0

# factorise drinking habits
i5 <- c(which(fdat[,5] == "No, but I was advised to reduce my drinking"),
        which(fdat[,5] == "Yes, I was advised to stop drinking"))
fdat[,5] <- rep(0, nrow(fdat))
fdat[i5, 5] <- 1

# factorise medical treatment
i6 <- c(which(fdat[,6] == "Yes, I have"),
        which(fdat[,6] == "Unsure"))
fdat[,6] <- rep(0, nrow(fdat))
fdat[i6, 6] <- 1

# factorise hospital stay
i7 <- which(fdat[,7] == "Yes, I have")
fdat[,7] <- rep(0, nrow(fdat))
fdat[i7, 7] <- 1

# factorise family heart disease
i8 <- which(fdat[,8] == "No immediate family members")
fdat[,8] <- 1
fdat[i8, 8] <- 0

# factorise family other disease
i9 <- which(fdat[,9] == "Yes, another inherited disease")
fdat[,9] <- 0
fdat[i9, 9] <- 1


# cancer
i10 <- which(fdat[,10] == "No, I have never had any form of cancer")
fdat[,10] <- 1
fdat[i10, 10] <- 0

# cardiac arr
i11 <- which(fdat[,11] == "No, I do not")
fdat[,11] <- 1
fdat[i11, 11] <- 0

# heart disease
i12 <- which(fdat[,12] == "No, I have never had any form of heart disease")
fdat[,12] <- 1
fdat[i12, 12] <- 0


# stroke
fdat[,13] <- as.num(as.factor(fdat[,13])) - 1

# diabetes
i14 <- which(fdat[,14] == "No, I don't have diabetes")
fdat[,14] <- 1
fdat[i14, 14] <- 0

# mental illness
i15 <- which(fdat[,15] == "No, I have not")
fdat[,15] <- 1
fdat[i15, 15] <- 0


# kidney, liver, etc disease
i16 <- which(fdat[,16] == "No, I have not")
fdat[,16] <- 1
fdat[i16, 16] <- 0


# lung disease
fdat[,17] <- as.num(as.factor(fdat[,17])) - 1


# treated high BP
fdat[,18] <- as.num(as.factor(fdat[,18])) - 1


# treated high chol
i19 <- which(fdat[,19] == "No, I have never been treated for high cholesterol")
fdat[,19] <- 1
fdat[i19, 19] <- 0


# anti dep
i20 <- which(fdat[,20] == "No, I do not")
fdat[,20] <- 1
fdat[i20, 20] <- 0









fdat[, -c(1:2)] <- apply(fdat[, -c(1:2)], 2, function(x) as.num(as.char(x)))


fdat_mod <- fdat[, -2]
names(fdat_mod)[1] <- "UW_K3"
fdat_mod[,1] <- as.factor(fdat_mod[,1])


fdat_mod2 <- balance_data_with_SMOTE(fdat_mod)
fdat_mod2 <- fdat_mod2[sample(1:nrow(fdat_mod2)), ]

out <- cv.glmnet(x=as.matrix(fdat_mod[,-1]), y=fdat_mod[,1], family="multinomial", type.multinomial="grouped")

table(as.char(fdat_mod[,1]), predict(out, newx=as.matrix(fdat_mod[,-1]), s="lambda.min", type="class"))


wl <- which(out$glmnet.fit$lambda == out$lambda.min)



aa <- out$glmnet.fit$beta[[1]]





rga_df <- data.frame("Anon_ID" = fdat[,2], "alc_problem" = as.num(fdat[,4] | fdat[,5]),
                     "hosp_stay" = as.num(fdat[,6] | fdat[,7]), "fam_hist" = as.num(fdat[,8] | fdat[,9]),
                     "cancer" = fdat[,10], "cardiac_arrythmia" = fdat[,11], "heart_disease" = fdat[,12], 
                     "stroke" = fdat[,13], "diabetes" = fdat[,14], "kidney_liver_disease" = fdat[,16],
                     "lung_disease" = fdat[,17], "treated_BP" = fdat[,18], "treated_chol" = fdat[,19], 
                     "mental_illness" = as.num(fdat[,15] | fdat[,20]))




mmi_df <- data.frame("Anon_ID" = dat[,6], "alc_problem" = as.num(mmiq1 | mmiq2),
                     "hosp_stay" = mmiq3, "fam_hist" = mmiq4, "cancer" = mmiq5, "cardiac_arrythmia" = mmiq6,
                     "heart_disease" = mmiq7, "stroke" = mmiq8, "diabetes" = mmiq9, "kidney_liver_disease" = mmi11,
                     "lung_disease" = mmi12, "treated_BP" = mmi13, "treated_chol" = mmi14, "mental_illness" = mmi15)




all_q_df <- rbind(rga_df, mmi_df)
names(all_q_df)[-1] <- paste0("QUESTION_", names(all_q_df)[-1])
write.table2(all_q_df, file="~/HealthQ/RGA/AGGREGATED/all_reformatted_questionnaire.csv", col.names = T)



############################################
####  MMI Questionnaire response  ##########
############################################

dat <- read_excel("C:/Users/reniervr/Downloads/MMI and LifeQ Pilot_Analytics data set_DOH_RUB_HA_UW_25042017_V7 - import.xlsx", sheet = "IMPORT")
head2 <- dat[1,]
dat <- dat[2:(which(is.na(dat[,3]))[1]-1) , ]

# alc units
mmiq1 <- rep(0, nrow(dat))
mmiq1[which(dat[,67] == "21 or more")] <- 1

# med advice stop drinking
mmiq2 <- rep(0, nrow(dat))
mmiq2[c(305, 861, 866)] <- 1


# personal med hist: hospital stay
mmiq3 <- rep(0, nrow(dat))
mmiq3[grep("hospital", tolower(dat[,491]))] <- 1


# family heart disease
mmifamhist <- tolower(dat[,530])

mmiq4 <- rep(0, nrow(dat))
mmiq4[grep("heart", mmifamhist)] <- 1


# cancer
mmiq5 <- rep(1, nrow(dat))
mmiq5[grep("None of these apply to me", dat[,376])] <- 0


# cardiac arr
mmiq6 <- as.num(as.factor(dat[,545]))-1


# heart disease
mmiq7 <- rep(0, nrow(dat))
mmiq7[which(!is.na(dat[,77]))] <- 1

# stroke
mmiq8 <- rep(0, nrow(dat))
mmiq8[grep("stroke", tolower(dat[,73]))] <- 1


# diabetes
mmiq9 <- rep(0, nrow(dat))
mmiq9[grep("diabetes", tolower(dat[,309]))] <- 1


# kidney, liver, etc disease
mmi11 <- rep(1, nrow(dat))
mmi11[grep("None of the above apply to me", dat[,232])] <- 0
mmi11[dat[,232] != "None of the above apply to me"] <- 1


# lung disease
mmi12 <- rep(1, nrow(dat))
mmi12[grep("None of these apply to me", dat[,273])] <- 0


# treated high BP
mmi13 <- rep(0, nrow(dat))
mmi13[grep("blood pressure", tolower(dat[,492]))] <- 1


# treated high chol
mmi14 <- rep(0, nrow(dat))
mmi14[grep("cholesterol", tolower(dat[,492]))] <- 1


# anti dep
mmimental <- tolower(dat[,125])
mmi15 <- rep(0, nrow(dat))
mmi15[c(grep("bipolar", mmimental), grep("consultation", mmimental))] <- 1



mmi_df <- data.frame("Anon_ID" = dat[,6], "alc_problem" = as.num(mmiq1 | mmiq2),
                     "hosp_stay" = mmiq3, "fam_hist" = mmiq4, "cancer" = mmiq5, "cardiac_arrythmia" = mmiq6,
                     "heart_disease" = mmiq7, "stroke" = mmiq8, "diabetes" = mmiq9, "kidney_liver_disease" = mmi11,
                     "lung_disease" = mmi12, "treated_BP" = mmi13, "treated_chol" = mmi14, "mental_illness" = mmi15)



#################################################
####  Read in and prepare bio/UW data  ##########
#################################################



dat_df <- t(sapply(readLines(file("~/HealthQ/RGA/SUBJECT_INFO/Summary/UW_Bio_CompactResults_new.csv")),
                   function(x) unlist(strsplit(x, ","))))
dimnames(dat_df) <- list(NULL, dat_df[1,])
dat_df <- dat_df[-1,]


# Check that anon ID mix up is corrected
if(dat_df[dat_df[,1] == "1406b144-eed7-45be-9052-9610fdab12f2", 3] != "b3ae4d0c-4aef-455a-b5e6-d5deaa157b5c" )
  cat("ID inconsistency detected.\n")

# Add cholesterol
dat_chol <- read.table("~/HealthQ/RGA/AGGREGATED/All_cholesterol_results.csv", head=T, sep=",")[,3]


# Configure df used for modelling
dat_use <- data.frame(dat_df[,3], dat_df[,5], apply(dat_df[,6:12], 2, as.num), dat_chol, dat_df[,13], dat_df[,23:25])
names(dat_use) <- c("ID", paste0("BIO_", c("SEX","AGE", "HEIGHT", "WEIGHT", "BP_SYS", "BP_DIA", "RHR", "PEFR", "CHOL", "SMOKE")), paste0("UW_", c("OV", "PREF", "PREF_NOFAM")))

# Add BMI
dat_use <- cb(dat_use[,1:5], "BIO_BMI"=dat_use$BIO_WEIGHT/(0.01*dat_use$BIO_HEIGHT)^2, dat_use[,6:ncol(dat_use)])

# Cast SEX as integer
dat_use$BIO_SEX <- (as.num(dat_use$BIO_SEX) - 1)

# Add summary UW columns for convenience
dat_uwk4 <- as.num(as.char(dat_use$UW_PREF_NOFAM))
dat_uwk4[which(dat_use$UW_PREF_NOFAM == 2 & dat_use$UW_OV == 0)] <- 2
dat_uwk4[which(dat_use$UW_OV == 1)] <- 3
dat_uwk4[which(dat_use$UW_OV == 99)] <- 99

dat_uwk3 <- dat_uwk4
dat_uwk3[dat_uwk4 == 3] <- 2

dat_use <- cb(dat_use, "UW_K4"=dat_uwk4, "UW_K3"=dat_uwk3)

# Omit defers from working data set
dat_use <- dat_use[dat_use$UW_K3 != 99, ]

# Make binary classification categories
dat_use$UW_K2_sp <- dat_use$UW_K3
dat_use$UW_K2_sp[dat_use$UW_K2_sp == 2] <- 1

dat_use$UW_K2_p <- dat_use$UW_K3
dat_use$UW_K2_p[dat_use$UW_K2_p == 1] <- 0
dat_use$UW_K2_p[dat_use$UW_K2_p == 2] <- 1

dat_use$UW_SS <- dat_use$UW_K4
dat_use$UW_SS[dat_use$UW_SS %in% c(1, 2)] <- 0
dat_use$UW_SS[dat_use$UW_SS == 3] <- 1
