source("../useful_funs.r")
library(readxl)

dat <- readxl::read_excel("~/Downloads/MMI and LifeQ Pilot_Analytics data set_DOH_RUB_HA_UW_25042017_V7 - import.xlsx",
                          sheet="IMPORT")

# dat1 <- as.data.frame(dat[-1, c(grep("AnonID", names(dat)), 45,64)])


dat_bio <- dat[-1, match(c("LifeQIDforDOH", "AnonID", "HHS", "Gender", "Age", "BMI", "Height", "Weight", "Cholesterol", 
                           "Glucose", "BpSystolic", "BpDiastolic", "DecisionType", "CardiacDecisionType",
                           "EndocrineCondition1", "SignificantMedicineUsage10daysplus"), names(dat))]
dat_bio <- as.data.frame(dat_bio)

dat_bio$HHS[!(dat_bio$HHS %in% c("Green", "Amber", "Red"))] <- NA
dat_bio$Age <- as.num(dat_bio$Age)

dat_bio$Height <- as.num(dat_bio$Height)
dat_bio$Height[dat_bio$Height == 0] <- NA
dat_bio$Weight <- as.num(dat_bio$Weight)
dat_bio$Weight[dat_bio$Weight == 0] <- NA

dat_bio$BMI <- as.num(dat_bio$BMI)
dat_bio$BMI[dat_bio$BMI == 0] <- NA

dat_bio$Cholesterol <- as.num(dat_bio$Cholesterol)
dat_bio$Cholesterol[dat_bio$Cholesterol == 0] <- NA

dat_bio$Glucose <- as.num(dat_bio$Glucose)
dat_bio$Glucose[dat_bio$Glucose == 0] <- NA

dat_bio$BpSystolic <- as.num(dat_bio$BpSystolic)
dat_bio$BpSystolic[dat_bio$BpSystolic == 0] <- NA

dat_bio$BpDiastolic <- as.num(dat_bio$BpDiastolic)
dat_bio$BpDiastolic[dat_bio$BpDiastolic == 0] <- NA

dat_bio$DecisionType[dat_bio$DecisionType == "Awaiting cardio report"] <- "AwaitingCardioReport"
dat_bio$DecisionType[dat_bio$DecisionType == "Not underwritten yet"] <- NA
dat_bio$DecisionType[dat_bio$DecisionType == "Standard rates"] <- "Standard"

dat_bio$CardiacDecisionType[dat_bio$CardiacDecisionType == "Awaiting cardio report"] <- "AwaitingCardioReport"
dat_bio$CardiacDecisionType[dat_bio$CardiacDecisionType == "More information required"] <- "AwaitingCardioReport"
dat_bio$CardiacDecisionType[dat_bio$CardiacDecisionType == "Not underwritten yet"] <- NA
dat_bio$CardiacDecisionType[dat_bio$CardiacDecisionType == "Standard rates"] <- "Standard"

dat_bio$QuestionDiabetes <- num(nrow(dat_bio))
dat_bio$QuestionDiabetes[grep("diabetes", tolower(dat_bio$EndocrineCondition1))] <- 1
dat_bio$EndocrineCondition1 <- NULL

dat_bio$QuestionBPTreated <- num(nrow(dat_bio))
dat_bio$QuestionBPTreated[grep("blood pressure", tolower(dat_bio$SignificantMedicineUsage10daysplus))] <- 1
dat_bio$SignificantMedicineUsage10daysplus <- NULL



multiply_smoke <- read.table("~/Downloads/MMI and LifeQ_Analytics data set_new smoking_V9.csv", head=T, sep=",")
dat_match <- match(as.char(multiply_smoke$LifeQ.ID), dat_bio$LifeQIDforDOH)

# aa <- cb(as.char(multiply_smoke$LifeQ.ID), dat_bio$LifeQIDforDOH[dat_match])

dat_bio <- cb(dat_bio[dat_match,], "MultiplySmokeStatus"=multiply_smoke$Multiply.Smoker)

write.table2(dat_bio, "~/Downloads/MMI_fullcohort_metadata_20180611.csv", col.names=T)


###############################################################################

dat2 <- read.table("~/Downloads/metadata_mmi_with_framingham_FJ_checks.csv", head=T, sep=",")
dat2 <- dat2[, match(c("ANON_ID", "Questionnaire.smoke", "Multiply.smoke"), names(dat2))]

###############################################################################

id_match <- match(dat1$AnonID, dat2$ANON_ID)
dat <- cb(dat1[-which(is.na(id_match)),], dat2[na.omit(id_match), 2:3])

names(dat) <- c("AnonID", "QUESTIONNAIRE_SmokeStatus", "QUESTIONNAIRE_SmokingHabitsLast6Mths",
                "HealthQ_digitised", "Multiply")

w_diff <- which(dat$HealthQ_digitised != dat$Multiply)

dat[w_diff,]
