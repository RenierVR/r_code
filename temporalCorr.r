


temporalCorr <- function(x, y1, y2, y3, ynames=c("1", "2", "3"), winsize=512, overlap=256, k=c(0,0,0), 
                         method=c("corr", "ccf","ccfmax", "fft", "fftmax", "fftmultmax")[1], 
                         plot_show=TRUE, plot_ask=FALSE, ccf_show=TRUE, bw_ksmooth=1)
{
  ## Function to calculate correlation between two signals 'x' and 'y' across time
  ##   using moving windows of size 'winsize' with 'overlap' shared between them.
  ## Pearson's correlation computed in windows with function stats::cor
  ## If specified with 'plot_show', plotting window is opened showing original
  ##   signals as well as correlation across time, smoothed with a Gaussian filter
  ##   with bandwidth 'ksmooth_bw' (should be 1 for no smoothing)
  
  n <- length(x)
  ref2 <- !missing(y2)
  ref3 <- !missing(y3)
  
  tempCorCalc <- function(x, y, k){
    if(length(x) != length(y)) stop("Input signals does not have equal length.")
    
    if(k >= 0){
      x <- x[(1+k):length(x)]
      y <- y[1:(length(y)-k)]
    }
    else{
      x <- x[1:(length(x)+k)]
      y <- y[(1-k):length(y)]
    }
    
    
    
    cvec <- NULL
    j <- 0
    while(TRUE){
      j = j+1
      plotind <- (overlap*(j-1)+1):(winsize + overlap*(j-1))
      if(max(plotind) > length(x))
        break
      else{
        cvec <- rbind(cvec, c(mean(plotind), cor(x[plotind], y[plotind])))
      }
    }
    
    return(list(cvec, cbind(x,y)))
  }
  
  
  
  tempCorCalc_ccf <- function(x, y, k){
    if(length(x) != length(y)) stop("Input signals does not have equal length.")
    
    cvec <- NULL
    j <- 0
    while(TRUE){
      j = j+1
      plotind <- (overlap*(j-1)+1):(winsize + overlap*(j-1))
      if(max(plotind) > length(x))
        break
      else{
        cc_out <- ccf(x[plotind], y[plotind], plot=F)
        cc_mat <- cbind(as.numeric(cc_out[[4]]), as.numeric(cc_out[[1]]))
        
        
        cvec <- rbind(cvec, c(mean(plotind), cc_mat[cc_mat[,1]==k, 2]))
      }
    }
    
    return(list(cvec, cbind(x,y)))
  }
  
  tempCorCalc_ccfmax <- function(x, y, k){
    if(length(x) != length(y)) stop("Input signals does not have equal length.")
    
    cvec <- NULL
    j <- 0
    while(TRUE){
      j = j+1
      plotind <- (overlap*(j-1)+1):(winsize + overlap*(j-1))
      if(max(plotind) > length(x))
        break
      else{
        cc_out <- ccf(x[plotind], y[plotind], plot=F)
        cc_mat <- cbind(as.numeric(cc_out[[4]]), as.numeric(cc_out[[1]]))
        
        
        cvec <- rbind(cvec, c(mean(plotind), max(cc_mat[, 2])))
        #         cat(max(plotind),"\n")
      }
    }
    
    return(list(cvec, cbind(x,y)))
  }
  
  tempCorCalc_fft <- function(x, y, k){
    if(length(x) != length(y)) stop("Input signals does not have equal length.")
    
    cvec <- NULL
    j <- 0
    while(TRUE){
      j = j+1
      plotind <- (overlap*(j-1)+1):(winsize + overlap*(j-1))
      if(max(plotind) > length(x))
        break
      else{
        x_norm <- x[plotind]
        x_norm <- (x_norm - mean(x_norm))/sd(x_norm)
        y_norm <- y[plotind]
        y_norm <- (y_norm - mean(y_norm))/sd(y_norm)
        
        fft_y <- fft(x_norm)
        fft_x <- Conj(fft(y_norm))        
        fft_mult <- fft_y * fft_x
        
        fft_cc <- Re(fft(fft_mult, inverse=T))
        #         lags <- abs(cc_mat[1,1])
        lags <- 30
        fft_cc_trunc <- cbind(c(-30:-1, 1:30), fft_cc[c((length(fft_cc)-(lags-1)):length(fft_cc), 1:lags)])
        
        cvec <- rbind(cvec, c(mean(plotind), fft_cc_trunc[fft_cc_trunc[,1]==k, 2]))
      }
    }
    
    return(list(cvec, cbind(x,y)))
  }
  
  tempCorCalc_fftmax <- function(x, y, k){
    if(length(x) != length(y)) stop("Input signals does not have equal length.")
    
    cvec <- NULL
    j <- 0
    while(TRUE){
      j = j+1
      plotind <- (overlap*(j-1)+1):(winsize + overlap*(j-1))
      if(max(plotind) > length(x))
        break
      else{
        x_norm <- x[plotind]/1000
        x_norm <- (x_norm)/sd(x_norm)
        y_norm <- y[plotind]/1000
        y_norm <- (y_norm)/sd(y_norm)
        
        fft_y <- fft(x_norm)
        fft_x <- Conj(fft(y_norm))        
        fft_mult <- fft_y * fft_x
        
        fft_cc <- Re(fft(fft_mult, inverse=T))
        #         fft_cc <- fft_cc*(var(x_norm)*var(y_norm))
        
        #         lags <- abs(cc_mat[1,1])
        lags <- 30
        fft_cc_trunc <- cbind(c(-30:-1, 1:30), fft_cc[c((length(fft_cc)-(lags-1)):length(fft_cc), 1:lags)])
        
        cvec <- rbind(cvec, c(mean(plotind), max(fft_cc_trunc)))
      }
    }
    
    return(list(cvec, cbind(x,y)))
  }
  
  tempCorCalc_fftmultmax <- function(x, y, k){
    if(length(x) != length(y)) stop("Input signals does not have equal length.")
    
    cvec <- NULL
    j <- 0
    while(TRUE){
      j = j+1
      plotind <- (overlap*(j-1)+1):(winsize + overlap*(j-1))
      if(max(plotind) > length(x))
        break
      else{
        x_norm <- x[plotind]
        x_norm <- (x_norm)/sd(x_norm/1000)
        y_norm <- y[plotind]
        y_norm <- (y_norm)/sd(y_norm/1000)
        
        fft_y <- fft(x_norm)
        fft_x <- fft(y_norm)
        
        #         fft_y_ps <- sqrt(Re(fft_y)^2 + Im(fft_y)^2)
        #         fft_x_ps <- sqrt(Re(fft_x)^2 + Im(fft_x)^2)
        #         
        #         fft_m_ps <- fft_x_ps * fft_y_ps
        
        fft_x <- complex(real=floor(Re(fft_x)/128), imag=floor(Im(fft_x)/128))
        fft_y <- complex(real=floor(Re(fft_y)/128), imag=floor(Im(fft_y)/128))
        
        fft_mult <- fft_x * Conj(fft_y)
        
        fft_m_ps <- sqrt(Re(fft_mult)^2 + Im(fft_mult)^2)[1:floor(winsize/2)]  
        
        cvec <- rbind(cvec, c(mean(plotind), max(fft_m_ps)))
      }
    }
    
    return(list(cvec, cbind(x,y)))
  }
  
  
  
  
  ###########################################################################################
  
  if(method == "corr")
    corCalc <- tempCorCalc
  else if(method == "ccf")
    corCalc <- tempCorCalc_ccf
  else if(method == "ccfmax")
    corCalc <- tempCorCalc_ccfmax
  else if(method == "fft")
    corCalc <- tempCorCalc_fft
  else if(method == "fftmax")
    corCalc <- tempCorCalc_fftmax
  else if(method == "fftmultmax")
    corCalc <- tempCorCalc_fftmultmax
  
  tc_out1 <- corCalc(x, y1, k[1])
  cvec1 <- tc_out1[[1]]
  y1_lag <- tc_out1[[2]][,2]
  
  cvec2 <- rbind(c(0,0))
  cvec3 <- rbind(c(0,0))
  if(ref2){
    tc_out2 <- corCalc(x, y2, k[2])
    cvec2 <- tc_out2[[1]]
    y2_lag <- tc_out2[[2]][,2]
  }
  if(ref3){
    tc_out3 <- corCalc(x, y3, k[3])
    cvec3 <- tc_out3[[1]]
    y3_lag <- tc_out3[[2]][,2]
  }
  
  
  if(plot_show){
    
    windows(14,9)
    par(mar=c(0,5,0,4), oma=c(5,0,1,0), ask=plot_ask, 
        cex.lab=1.9, cex.axis=1.9, cex.main=1.9, cex.sub=1.9)
    
    if(!ref2 && !ref3)
      layout(matrix(c(1,2,3), nr=3, nc=1), heights=c(1,1,2))
    if(ref2 && ref3)
      layout(matrix(c(1,2,3,4,5), nr=5, nc=1), heights=c(1,1,1,1,2.5))
    else
      layout(matrix(c(1,2,3,4), nr=4, nc=1), heights=c(1,1,1,2.5))
    
    pind <- 1:n
    #     while(length(pind) > 10000) 
    #       pind <- xplot[seq(1,length(pind),2)]
    
    plot(pind, x[pind], type="l", col="forestgreen", xaxt="n", yaxt="n", ylab="signal")
    axis(side=4)
    plot(pind, y1_lag[pind], type="l", col="blue", xaxt="n", yaxt="n", ylab=paste0("reference \n(", ynames[1], ")"))
    axis(side=4)
    #     legend("bottomleft", leg=paste0(ynames[1], " relative lag = ", k[1], "   "), cex=1.3, bg="white")
    
    if(ref2){
      plot(pind, y2_lag[pind], type="l", col="red", xaxt="n", yaxt="n", ylab=paste0("reference (", ynames[2], ")"))
      axis(side=4)
      #       legend("bottomleft", leg=paste0(ynames[2], " relative lag = ", k[2], "   "), cex=1.3, bg="white")
    }
    
    if(ref3){
      plot(pind, y3_lag[pind], type="l", col="orange", xaxt="n", yaxt="n", ylab=paste0("reference (", ynames[3], ")"))
      axis(side=4)
    }
    
    ## correlation plot
    if(method=="fft" || method=="fftmax" || method=="fftmultmax")
      ylm <- range(c(cvec1[,2], cvec2[,2]))
    else
      ylm <- c(min(cvec1[,2], cvec2[,2]), 1)
    
    plot(0,0, type="n", xlim=c(1,n), ylim=ylm, ylab="temporal correlation")
    abline(h=0, lwd=2, col="gray")
    points(cvec1, pch=19, cex=0.8, col="blue")
    lines(ksmooth(cvec1[,1], cvec1[,2], "normal", bandw=bw_ksmooth), col="blue")
    mtext("index", 1, 4, cex=1.2)
    
    if(ref2){
      points(cvec2, pch=19, cex=0.8, col="red")
      lines(ksmooth(cvec2[,1], cvec2[,2], "normal", bandw=bw_ksmooth), col="red")
    }
    
    if(ref2 && !ref3)
      legend("topleft", lwd=1, pch=19, col=c("blue", "red"), leg=paste("with", ynames[1:2]), bg="white")
    
    if(ref3){
      points(cvec3, pch=19, cex=0.8, col="orange")
      lines(ksmooth(cvec3[,1], cvec3[,2], "normal", bandw=bw_ksmooth), col="orange")
      legend("topleft", lwd=1, pch=19, col=c("blue", "red", "orange"), leg=paste("with", ynames), bg="white")
    }
    
  }
  
  
  if(missing(ccf_show) && (!missing(k)))
    ccf_show <- FALSE
  
  if(missing(ccf_show) && ((method=="ccfmax") || (method=="fftmax") || (method=="fftmultmax")))
    ccf_show <- FALSE
  
  if(ccf_show){
    
    windows(14,7)
    if(ref2) par(mfrow=c(2,1), mar=c(3,3,3,1))
    
    ccf_out <- ccf(x, y1, 
                   main=paste0("Cross-correlation Between Input Signal and Reference (", ynames[1], ")"))
    ccf_mat <- cbind(ccf_out[[4]], ccf_out[[1]])
    lines(ccf_mat, col="steelblue")
    
    source("~/HealthQ/R scripts/peakDetector.r")
    pk <- peakDetector(ccf_mat)
    points(pk, pch=19, col="red")
    abline(v=pk[,1], lty=2, col="red")
    axis(side=1, at=pk[,1], col.ticks="red")
    
    if(ref2){
      ccf_out <- ccf(x, y2, 
                     main=paste0("Cross-correlation Between Input Signal and Reference (", ynames[2], ")"))
      ccf_mat <- cbind(ccf_out[[4]], ccf_out[[1]])
      lines(ccf_mat, col="steelblue")
      
      pk <- peakDetector(ccf_mat)
      points(pk, pch=19, col="red")
      abline(v=pk[,1], lty=2, col="red")
      axis(side=1, at=pk[,1], col.ticks="red")
    }
  }
  
  #   colnames(cvec) <- c("index", "corr")
  #   return(invisible(list(temporal_corr=cvec, lagged_inputs=cbind(x,y))))
  if(!ref2){
    colnames(cvec1) <- c("index", "corr")
    retlist <- list(temporal_corr=cvec1, lagged_inputs=tc_out1[[2]])
  }
  else{
    colnames(cvec1) <- colnames(cvec2) <- c("index", "corr")
    retlist <- list(temporal_corr_y1=cvec1, lagged_inputs_y1=tc_out1[[2]],
                    temporal_corr_y2=cvec2, lagged_inputs_y2=tc_out2[[2]])
  }
  
  return(invisible(retlist))
}


# aa <- temporalCorr(grn_ch, acc_ch, yn="ACC", winsize=128, overl=64, bw=1, method="fftmultmax")








###################################################################################################

###################################################################################################


###################################################################################################




###################################################################################################






temporalCorr_patent <- function(x, y1, y2, y3, ynames=c("1", "2", "3"), winsize=512, overlap=256, k=c(0,0,0), 
                                method=c("corr", "ccf","ccfmax", "fft", "fftmax", "fftmultmax")[1], 
                                plot_show=TRUE, plot_ask=FALSE, ccf_show=TRUE, bw_ksmooth=1)
{
  ## Function to calculate correlation between two signals 'x' and 'y' across time
  ##   using moving windows of size 'winsize' with 'overlap' shared between them.
  ## Pearson's correlation computed in windows with function stats::cor
  ## If specified with 'plot_show', plotting window is opened showing original
  ##   signals as well as correlation across time, smoothed with a Gaussian filter
  ##   with bandwidth 'ksmooth_bw' (should be 1 for no smoothing)
  
  n <- length(x)
  ref2 <- !missing(y2)
  ref3 <- !missing(y3)
  
  tempCorCalc <- function(x, y, k){
    if(length(x) != length(y)) stop("Input signals does not have equal length.")
  
    if(k >= 0){
      x <- x[(1+k):length(x)]
      y <- y[1:(length(y)-k)]
    }
    else{
      x <- x[1:(length(x)+k)]
      y <- y[(1-k):length(y)]
    }
  
    
  
    cvec <- NULL
    j <- 0
    while(TRUE){
      j = j+1
      plotind <- (overlap*(j-1)+1):(winsize + overlap*(j-1))
      if(max(plotind) > length(x))
        break
      else{
        cvec <- rbind(cvec, c(mean(plotind), cor(x[plotind], y[plotind])))
      }
    }
    
    return(list(cvec, cbind(x,y)))
  }
  
  
  
  tempCorCalc_ccf <- function(x, y, k){
    if(length(x) != length(y)) stop("Input signals does not have equal length.")
      
    cvec <- NULL
    j <- 0
    while(TRUE){
      j = j+1
      plotind <- (overlap*(j-1)+1):(winsize + overlap*(j-1))
      if(max(plotind) > length(x))
        break
      else{
        cc_out <- ccf(x[plotind], y[plotind], plot=F)
        cc_mat <- cbind(as.numeric(cc_out[[4]]), as.numeric(cc_out[[1]]))
        
        
        cvec <- rbind(cvec, c(mean(plotind), cc_mat[cc_mat[,1]==k, 2]))
      }
    }
    
    return(list(cvec, cbind(x,y)))
  }
  
  tempCorCalc_ccfmax <- function(x, y, k){
    if(length(x) != length(y)) stop("Input signals does not have equal length.")
    
    cvec <- NULL
    j <- 0
    while(TRUE){
      j = j+1
      plotind <- (overlap*(j-1)+1):(winsize + overlap*(j-1))
      if(max(plotind) > length(x))
        break
      else{
        cc_out <- ccf(x[plotind], y[plotind], plot=F)
        cc_mat <- cbind(as.numeric(cc_out[[4]]), as.numeric(cc_out[[1]]))
        
        
        cvec <- rbind(cvec, c(mean(plotind), max(cc_mat[, 2])))
#         cat(max(plotind),"\n")
      }
    }
    
    return(list(cvec, cbind(x,y)))
  }
  
  tempCorCalc_fft <- function(x, y, k){
    if(length(x) != length(y)) stop("Input signals does not have equal length.")
    
    cvec <- NULL
    j <- 0
    while(TRUE){
      j = j+1
      plotind <- (overlap*(j-1)+1):(winsize + overlap*(j-1))
      if(max(plotind) > length(x))
        break
      else{
        x_norm <- x[plotind]
        x_norm <- (x_norm - mean(x_norm))/sd(x_norm)
        y_norm <- y[plotind]
        y_norm <- (y_norm - mean(y_norm))/sd(y_norm)
        
        fft_y <- fft(x_norm)
        fft_x <- Conj(fft(y_norm))        
        fft_mult <- fft_y * fft_x
        
        fft_cc <- Re(fft(fft_mult, inverse=T))
#         lags <- abs(cc_mat[1,1])
        lags <- 30
        fft_cc_trunc <- cbind(c(-30:-1, 1:30), fft_cc[c((length(fft_cc)-(lags-1)):length(fft_cc), 1:lags)])
        
        cvec <- rbind(cvec, c(mean(plotind), fft_cc_trunc[fft_cc_trunc[,1]==k, 2]))
      }
    }
    
    return(list(cvec, cbind(x,y)))
  }

  tempCorCalc_fftmax <- function(x, y, k){
    if(length(x) != length(y)) stop("Input signals does not have equal length.")
  
    cvec <- NULL
    j <- 0
    while(TRUE){
      j = j+1
      plotind <- (overlap*(j-1)+1):(winsize + overlap*(j-1))
      if(max(plotind) > length(x))
        break
      else{
        x_norm <- x[plotind]/1000
        x_norm <- (x_norm)/sd(x_norm)
        y_norm <- y[plotind]/1000
        y_norm <- (y_norm)/sd(y_norm)
      
        fft_y <- fft(x_norm)
        fft_x <- Conj(fft(y_norm))        
        fft_mult <- fft_y * fft_x
      
        fft_cc <- Re(fft(fft_mult, inverse=T))
#         fft_cc <- fft_cc*(var(x_norm)*var(y_norm))
        
        #         lags <- abs(cc_mat[1,1])
        lags <- 30
        fft_cc_trunc <- cbind(c(-30:-1, 1:30), fft_cc[c((length(fft_cc)-(lags-1)):length(fft_cc), 1:lags)])
      
        cvec <- rbind(cvec, c(mean(plotind), max(fft_cc_trunc)))
      }
    }
  
    return(list(cvec, cbind(x,y)))
  }

  tempCorCalc_fftmultmax <- function(x, y, k){
    if(length(x) != length(y)) stop("Input signals does not have equal length.")
  
    cvec <- NULL
    j <- 0
    while(TRUE){
      j = j+1
      plotind <- (overlap*(j-1)+1):(winsize + overlap*(j-1))
      if(max(plotind) > length(x))
        break
      else{
        x_norm <- x[plotind]
        x_norm <- (x_norm)/sd(x_norm/1000)
        y_norm <- y[plotind]
        y_norm <- (y_norm)/sd(y_norm/1000)
      
        fft_y <- fft(x_norm)
        fft_x <- fft(y_norm)
        
#         fft_y_ps <- sqrt(Re(fft_y)^2 + Im(fft_y)^2)
#         fft_x_ps <- sqrt(Re(fft_x)^2 + Im(fft_x)^2)
#         
#         fft_m_ps <- fft_x_ps * fft_y_ps

        fft_x <- complex(real=floor(Re(fft_x)/128), imag=floor(Im(fft_x)/128))
        fft_y <- complex(real=floor(Re(fft_y)/128), imag=floor(Im(fft_y)/128))

        fft_mult <- fft_x * Conj(fft_y)
      
        fft_m_ps <- sqrt(Re(fft_mult)^2 + Im(fft_mult)^2)[1:floor(winsize/2)]  
      
        cvec <- rbind(cvec, c(mean(plotind), max(fft_m_ps)))
      }
    }
  
    return(list(cvec, cbind(x,y)))
  }

  
  
  
  ###########################################################################################
  
  if(method == "corr")
    corCalc <- tempCorCalc
  else if(method == "ccf")
    corCalc <- tempCorCalc_ccf
  else if(method == "ccfmax")
    corCalc <- tempCorCalc_ccfmax
  else if(method == "fft")
    corCalc <- tempCorCalc_fft
  else if(method == "fftmax")
    corCalc <- tempCorCalc_fftmax
  else if(method == "fftmultmax")
    corCalc <- tempCorCalc_fftmultmax
  
  tc_out1 <- corCalc(x, y1, k[1])
  cvec1 <- tc_out1[[1]]
  y1_lag <- tc_out1[[2]][,2]
  
  cvec2 <- rbind(c(0,0))
  cvec3 <- rbind(c(0,0))
  if(ref2){
    tc_out2 <- corCalc(x, y2, k[2])
    cvec2 <- tc_out2[[1]]
    y2_lag <- tc_out2[[2]][,2]
  }
  if(ref3){
    tc_out3 <- corCalc(x, y3, k[3])
    cvec3 <- tc_out3[[1]]
    y3_lag <- tc_out3[[2]][,2]
  }
  
  
  if(plot_show){
    
    windows(14,9)
    par(mar=c(0,5.5,0,4.5), oma=c(5.5,0,1.5,0), ask=plot_ask, 
        cex.lab=1.9, cex.axis=1.9, cex.main=1.9, cex.sub=1.9)
    
    if(!ref2 && !ref3)
      layout(matrix(c(1,2,3), nr=3, nc=1), heights=c(1,1,2))
    if(ref2 && ref3)
      layout(matrix(c(1,2,3,4,5), nr=5, nc=1), heights=c(1,1,1,1,2.5))
    else
      layout(matrix(c(1,2,3,4), nr=4, nc=1), heights=c(1,1,1,2.5))
    
    pind <- 1:n
#     while(length(pind) > 10000) 
#       pind <- xplot[seq(1,length(pind),2)]
    
    plot(pind, x[pind], type="l", col="forestgreen", xaxt="n", yaxt="n", ylab="525nm")
    axis(side=4)
    plot(pind, y1_lag[pind], type="l", col="blue", xaxt="n", yaxt="n", ylab="ACC")
    axis(side=4)
#     legend("bottomleft", leg=paste0(ynames[1], " relative lag = ", k[1], "   "), cex=1.3, bg="white")

    if(ref2){
      plot(pind, y2_lag[pind], type="l", col="red", xaxt="n", yaxt="n", ylab="EMG")
      axis(side=4)
#       legend("bottomleft", leg=paste0(ynames[2], " relative lag = ", k[2], "   "), cex=1.3, bg="white")
    }

    if(ref3){
      plot(pind, y3_lag[pind], type="l", col="orange", xaxt="n", yaxt="n", ylab="950nm")
      axis(side=4)
    }
    
    ## correlation plot
    if(method=="fft" || method=="fftmax" || method=="fftmultmax")
      ylm <- range(c(cvec1[,2], cvec2[,2]))
    else
      ylm <- c(min(cvec1[,2], cvec2[,2]), 1)

    plot(0,0, type="n", xlim=c(1,n), ylim=ylm, ylab="temporal correlation")
    abline(h=0, lwd=2, col="gray")
    points(cvec1, pch=19, cex=1.3, col="blue")
    lines(ksmooth(cvec1[,1], cvec1[,2], "normal", bandw=bw_ksmooth), col="blue", lwd=2)
    mtext("index", 1, 4, cex=1.2)

    if(ref2){
      points(cvec2, pch=17, cex=1.3, col="red")
      lines(ksmooth(cvec2[,1], cvec2[,2], "normal", bandw=bw_ksmooth), col="red")
    }
    
    if(ref2 && !ref3)
      legend("topleft", lwd=2, pch=19, col=c("blue", "red"), leg=paste("with", ynames[1:2]), bg="white")
    
    if(ref3){
      points(cvec3, pch=15, cex=1.3, col="orange")
      lines(ksmooth(cvec3[,1], cvec3[,2], "normal", bandw=bw_ksmooth), col="orange")
      legend("topleft", lwd=2, pch=c(19,17,15), col=c("blue", "red", "orange"), cex=1.75,
             leg=paste("with", c("ACC", "EMG", "950nm")), bg="white")
    }
    
  }
  
  
  if(missing(ccf_show) && (!missing(k)))
    ccf_show <- FALSE

  if(missing(ccf_show) && ((method=="ccfmax") || (method=="fftmax") || (method=="fftmultmax")))
    ccf_show <- FALSE

  if(ccf_show){
    
    windows(14,7)
    if(ref2) par(mfrow=c(2,1), mar=c(3,3,3,1))
    
    ccf_out <- ccf(x, y1, 
                   main=paste0("Cross-correlation Between Input Signal and Reference (", ynames[1], ")"))
    ccf_mat <- cbind(ccf_out[[4]], ccf_out[[1]])
    lines(ccf_mat, col="steelblue")
    
    source("~/HealthQ/R scripts/peakDetector.r")
    pk <- peakDetector(ccf_mat)
    points(pk, pch=19, col="red")
    abline(v=pk[,1], lty=2, col="red")
    axis(side=1, at=pk[,1], col.ticks="red")
    
    if(ref2){
      ccf_out <- ccf(x, y2, 
                   main=paste0("Cross-correlation Between Input Signal and Reference (", ynames[2], ")"))
      ccf_mat <- cbind(ccf_out[[4]], ccf_out[[1]])
      lines(ccf_mat, col="steelblue")
    
      pk <- peakDetector(ccf_mat)
      points(pk, pch=19, col="red")
      abline(v=pk[,1], lty=2, col="red")
      axis(side=1, at=pk[,1], col.ticks="red")
    }
  }
  
#   colnames(cvec) <- c("index", "corr")
#   return(invisible(list(temporal_corr=cvec, lagged_inputs=cbind(x,y))))
  if(!ref2){
    colnames(cvec1) <- c("index", "corr")
    retlist <- list(temporal_corr=cvec1, lagged_inputs=tc_out1[[2]])
  }
  else{
    colnames(cvec1) <- colnames(cvec2) <- c("index", "corr")
    retlist <- list(temporal_corr_y1=cvec1, lagged_inputs_y1=tc_out1[[2]],
                    temporal_corr_y2=cvec2, lagged_inputs_y2=tc_out2[[2]])
  }

  return(invisible(retlist))
}


# aa <- temporalCorr(grn_ch, acc_ch, yn="ACC", winsize=128, overl=64, bw=1, method="fftmultmax")
  
