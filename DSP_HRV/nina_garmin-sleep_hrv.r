source("../useful_funs.r")
source("../DSP_RR/amendConfidencePostProcess.r")
library(rhdf5)

# enable viewing of 64-bit format timestamps
options(digits = 22)


##  VIEW HDF5 FILE CONTENTS
############################

# path to h5 file
h5list <- getDataFromDir("F:/HEALTHQ/RRI/HRV/Sleep HRV Test Data-parsed/Sleep HRV Test Data-parsed/", dataE=F, ext="h5")

i <- 1

# print group names with corresponding dimension
h5_contents <- h5ls(h5list[i])
print( h5_contents[h5_contents$group != "/", c(1, 5)] )


# out_metric_heartrate$index <- out_metric_heartrate$index/10^6
# as.POSIXlt(out_metric_heartrate$index[1]/1000, origin = "1970-01-01")




## ACCESS RR DATA
##################

rr_list <- vector("list", len(h5list))
for(i in 1:len(h5list)) {
  
  out_raw <- h5read(h5list[i], "/raw_converted", bit64conversion="double", compoundAsDataFrame=T)$table
  out_trange <- out_raw[c(1, nrow(out_raw)), 1]
  
  out_rr <- h5read(h5list[i], "/metric_rr", bit64conversion="double", compoundAsDataFrame=T)$table
  out_rrv <- h5read(h5list[i], "/metric_rr_isvalid", bit64conversion="double", compoundAsDataFrame=T)$table
  out_rrcm <- h5read(h5list[i], "/metric_rr_conf", bit64conversion="double", compoundAsDataFrame=T)$table
  
  rrcm_pp <- amendConfidencePostProcess(out_rr[,2], out_rrcm[,2])
  
  # out_rr <- cb(out_rr, out_rrv[,2], out_rrcm[,2])
  out_rr <- cb(out_rr, out_rrv[,2], rrcm_pp[,3])
  
  names(out_rr) <- c("index", "rr", "rrv", "rrcm")
  rm(out_rrv, out_rrcm, out_raw)
  
  cat(round(100*sum(out_rr[out_rr$rrcm == 1, 2])/sum(out_rr[, 2]), 2), "%  | ") 
  cat(round(sum(out_rr[,2])/1000/3600, 1), "  ")
  print(as.POSIXct(out_trange/10^9, origin = "1970-01-01"))
  
  rr_list[[i]] <- out_rr
}





w() ; for(i in 1:len(rr_list)) {
  lplot_g(rr_list[[i]][,1], main=basename(h5list[i]), ylab="RR timestamp") ; 
  savePlot(paste0("figures/figure", i, ".png"), "png")
}



for(i in 1:len(h5list)) {
  
  out_rr <- rr_list[[i]]
  
  rrcm_change <- c(which(diff(out_rr[,4]) != 0), nrow(out_rr))
  rrcm_diff <- cb(c(rrcm_change[1], diff(rrcm_change)),
                  out_rr[rrcm_change, 4])
  
  len_conf <- rrcm_diff[rrcm_diff[,2]==1,1]
  
  # print(sort(rrcm_diff[rrcm_diff[,2]==1,1], dec=T))
  print(sum(floor(len_conf/150)))
  
  cat("\n\n\n")
  
}




prop_invalid <- NULL
for(i in 1:len(h5list)) {
  
  out_rr <- rr_list[[i]]
  
  conf_count <- 0
  for(j in 1:nrow(out_rr)) {
    if(out_rr[j,4] == 1) conf_count <- conf_count+1
    
    if(out_rr[j,4] == 0) conf_count <- 0
    
    if(conf_count == 150) {
      out_rr_tmp <- out_rr[(j-150+1):j,]
      
      prop_invalid <- c(prop_invalid, 100*sum(out_rr_tmp[,3])/150)
      
      conf_count <- 0
    }
  }
  
  catProgress(i, len(h5list))
  
}



