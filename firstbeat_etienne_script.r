sdflist <- getDataFromDir("~/HealthQ/DATA/Firstbeat/", ext="sdf", dataE=F, ret="full")

j <- 3

sdfout <- scan(sdflist[j], what="character")

sdfout_date <- strsplit(sdfout[grep("TIME", sdfout)] ,"=")[[1]][2]
sdfout_time <- sdfout[grep("TIME", sdfout) + 1]


tstamp <- as.numeric(as.POSIXct( paste(paste(rev(strsplit(sdfout_date, "[.]")[[1]]), collapse="-"), 
                            paste(strsplit(sdfout_time, "[.]")[[1]], collapse=":")) ))



rr <- as.integer(sdfout[(which(sdfout == "[CUSTOM1]")[1] + 1):len(sdfout)])

tstamp <- tstamp + round(sum(rr[1:10])/1000, 0)

rr <- rr[-c(1:10)]
rr_x <- cumsum(rr)/1000
hr <- round(60/(rr/1000), 2)

# spline_out <- smooth.spline(cb(rr_x, hr), all.knots=TRUE, spar=0.1)
# sp_fit <- predict(spline_out, rr_x)$y
# zPlot(cb(sp_fit, hr))



rr_x_c <- ceiling(rr_x)
i_start <- which(rr_x_c >= 1)[1]
hr_1hz <- rbind(c(rr_x_c[i_start], hr[i_start]))

for(i in (i_start+1):len(rr_x)) {
  d <- rr_x_c[i] - rr_x_c[i-1]
  
  if(d == 0) {
    hr_1hz[nrow(hr_1hz), 2] <- hr[i]
  }
  
  else if(d == 1) 
    hr_1hz <- rbind(hr_1hz, c(rr_x_c[i], hr[i]))
  
  else if(d > 1){
    print(i)
    hr_1hz <- rbind(hr_1hz, cb((rr_x_c[i-1]+1):rr_x_c[i],
                               c(rep(hr_1hz[nrow(hr_1hz), 2], d-1), hr[i])))
  }
}


###################################################################################################

hr2 <- hr_1hz[,2]
outliers <- numeric(len(hr2))
ma_tmp_lst <- numeric(len(hr2))

MAX_JUMP <- 25
MA_ORDER <- 2
for(i in (MA_ORDER + 1):len(hr2)) {
  ma_tmp <- mean(hr2[(i - MA_ORDER):(i-1)])
  ma_tmp_lst[i] <- ma_tmp
  
  if(abs(hr2[i]-ma_tmp) >= MAX_JUMP) {
    hr2[i] <- ma_tmp
    outliers[i] <- 1
  }
}


hr2_sm <- expSmooth(hr2, alpha=0.5)

zPlot(cb("HR"=hr_1hz[,2], "HR filtered"=hr2_sm), col_f=c("blue", "red"))


###################################################################################################

tstamps <- tstamp + hr_1hz[,1] - 1

write.table2(data.frame("Unix_time"=tstamps, "Date"=as.POSIXct(tstamps, origin="1970-01-01"),
                        "HR_1hz"=hr_1hz[,2], "HR_filtered"=hr2_sm),
             file=paste0(strsplit(sdflist[j], ".sdf")[[1]], " - HR_1HZ.csv"), col.names=T)





###################################################################################################


hr1hz_from_rr <- function(rr, max_jump=25, ma_order=2, alpha=0.5)
{
  rr_x <- cumsum(rr)/1000
  
  # Compute 1Hz heart rate from RR intervals
  rr_x_c <- ceiling(rr_x)
  i_start <- which(rr_x_c >= 1)[1]
  hr_1hz <- rbind(c(rr_x_c[i_start], hr[i_start]))
  
  for(i in (i_start+1):len(rr_x)) {
    d <- rr_x_c[i] - rr_x_c[i-1]
    
    if(d == 0) {
      hr_1hz[nrow(hr_1hz), 2] <- hr[i]
    }
    
    else if(d == 1) 
      hr_1hz <- rbind(hr_1hz, c(rr_x_c[i], hr[i]))
    
    else if(d > 1){
      print(i)
      hr_1hz <- rbind(hr_1hz, cb((rr_x_c[i-1]+1):rr_x_c[i],
                                 c(rep(hr_1hz[nrow(hr_1hz), 2], d-1), hr[i])))
    }
  }
  
  # Remove outliers from 1Hz HR
  hr2 <- hr_1hz[,2]
  outliers <- numeric(len(hr2))
  ma_tmp_lst <- numeric(len(hr2))
  
  for(i in (ma_order + 1):len(hr2)) {
    ma_tmp <- mean(hr2[(i - ma_order):(i-1)])
    ma_tmp_lst[i] <- ma_tmp
    
    if(abs(hr2[i]-ma_tmp) >= max_jump) {
      hr2[i] <- ma_tmp
      outliers[i] <- 1
    }
  }
  
  # Smooth 1Hz HR
  hr2_sm <- expSmooth(hr2, alpha=alpha)
  
  return(cb(hr_1hz[,1], hr2_sm))
  
}

